jQuery(function() {

	/*
	$.ajax({
		 url: 'http://localhost:8888/pme_app/index.php/event/geteventall',

		 type: "POST",
		 success: function(json) {
			 console.log(json);

		 }
		 });
	*/

	global_starttime="";
	global_endtime="";
	actioncalendar("","#calendar1",0);
	actioncalendar("","#calendar",0);
	var tag_cal=jQuery(".calendar_get").attr("id");
	console.log(tag_cal)
	  if(tag_cal!="calendar1")
		{
			var idcat=jQuery(".calendar_get").attr("idsecteur");
			actioncalendar("","#"+tag_cal,idcat);
		}

	function checkoptioncats()
	{
	var list_cats=new Array();
	jQuery(".secteur").each(function(){
		if(jQuery(this).is( ":checked" ))
			{
				list_cats.push(jQuery(this).val());
			}

	});

	return list_cats.join();
	}


	function actioncalendar(m_get,cal,idcat)
	{
		if(idcat==0)
		  {
			var str_idcats=checkoptioncats();
		  }else
		  {
			  var str_idcats=idcat;
		  }
		var date = new Date();
		var d = date.getDate();
		var m = date.getMonth();
		var y = date.getFullYear();
		if(m_get=="")
			{
				var month=m;
			}else
			{
				var month=m_get;
			}

		// jQuery(cal).fullCalendar({
  //       	monthNames: ['Janvier','Février','Mars','Avril','Mai','Juin','Juillet','Août','Septembre','Octobre','Novembre','Décembre'],
  //       	monthNamesShort: ['Jan','Fev','Mar','Avr','Mai','Juin','Juil','Auot','Sep','Oct','Nov','Dec'],
		// 	weekdays : "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi".split("_"),
  //           weekdaysShort : "dim._lun._mar._mer._jeu._ven._sam.".split("_"),
  //           weekdaysMin : "Di_Lu_Ma_Me_Je_Ve_Sa".split("_")
  //     	});

		jQuery(cal).fullCalendar({
			lang: 'fr',
			theme: true,
            header: {


				right: 'month,agendaWeek,agendaDay,prev,next'
			},
			editable: false,
			defaultView: 'month',
			month: month,

			//events: "http://localhost:8888/pme_app/index.php/event/geteventall",
			events: function(start, end,callback) {
				var start_format=moment(start).format('YYYY-MM-DD');
				var end_format=moment(end).format('YYYY-MM-DD');
				global_starttime=start_format;
				global_endtime=end_format;
		        $.ajax({
		            url: '/extranet/pme-appl/public/geteventbyidcats',
		            type:'post',
		            dataType: 'json',
                    async: true,
		            data: {
		                // our hypothetical feed requires UNIX timestamps
		                start: start_format,
		                end: end_format,
		                idcats:str_idcats
		            },
		            success: function(doc) {
		            	console.log(doc);
		                var events = [];

		           // doc direction
		                var doc_direction=doc["direction"];
		                $(doc_direction).each(function() {

		                	events.push({
		                        title: $(this).attr('title'),
		                        start: $(this).attr('start'), // will be parsed
		                        lieu:$(this).attr('lieu') ,
		                        color:"#ec1c24"

		                       });

		                });

		              // doc assemblee_annuelle
		                var doc_assem_annuelle=doc["assemblee_annuelle"];
		                $(doc_assem_annuelle).each(function() {

		                	events.push({
		                        title: $(this).attr('title'),
		                        start: $(this).attr('start'), // will be parsed
		                        lieu:$(this).attr('lieu') ,
		                        color:"#a4cd39"

		                       });

		                });


		                // doc assemblee
		                var doc_assem=doc["assemblee"];
		                $(doc_assem).each(function() {

		                	events.push({
		                        title: $(this).attr('title'),
		                        start: $(this).attr('start'), // will be parsed
		                        lieu:$(this).attr('lieu') ,
		                        color:"#cdc08c"

		                       });

		                });


		                // doc congre
		                var doc_congre=doc["assemblee"];
		                $(doc_congre).each(function() {

		                	events.push({
		                        title: $(this).attr('title'),
		                        start: $(this).attr('start'), // will be parsed
		                        lieu:$(this).attr('lieu') ,
		                        color:"#f58020"

		                       });

		                });

		                // doc formation
		                var doc_congre=doc["formation"];
		                $(doc_congre).each(function() {

		                	events.push({
		                        title: $(this).attr('title'),
		                        start: $(this).attr('start'), // will be parsed
		                        lieu:$(this).attr('lieu') ,
		                        color:"#0088cf"

		                       });

		                });




		                callback(events);
		            }
		        });
		    },

			eventRender: function(event, element, view) {

                    if (event.allDay === 'true') {
				     event.allDay = true;
				    } else {
				     event.allDay = false;
				    }
				    element.attr('href', 'javascript:void(0);');
			        element.click(function(eventp) {
                        Y=eventp.clientY-250;
                        X=eventp.clientX-100;
                        //alert(X);
                       	jQuery("#startTime").html(moment(event.start).format('MMMM Do YYYY'));
			        	jQuery("#lieu").html(event.lieu);
			        	jQuery("#eventContent").dialog({ title: event.title, width:350,position:[X, Y]});


			        });
				   },

		});


	 }





		jQuery(".secteur").click(function(){
			//alert(global_starttime);
			date1=new Date(global_starttime)
			date2=new Date(global_endtime)
			middle=new Date(date2 - (date2-date1)/2);
			//alert(middle);
			var m = middle.getMonth();
			jQuery('#calendar1').fullCalendar( 'destroy' )
			actioncalendar(m,'#calendar1',0);
		})


// click left menu

		var page={"214":92,"206":93,"229":94,"425":95,"224":96,"231":97,'15345':15330}
		if(jQuery(".section_page").size()>0)
			{
			var idsect=jQuery(".section_page").attr("parentid");
			if(idsect!=233)
				{
					var menu_id=page[idsect];
					jQuery(".directions > .sub-menu").css("display","block");
					jQuery(".directions").addClass("open");
					jQuery(".menu-item-"+menu_id).addClass("current-menu-item");
				}

			}

	});

