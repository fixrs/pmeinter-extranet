jQuery(document).ready(function($) {

	App.init();
	$('.current-menu-parent a').trigger('click');

	$(".menu-toggle").on("click", function() {
		$(this).toggleClass("open");
		$("#primary-menu").toggleClass("open");
	});

	$("#search-handle").on("click", function() {
		$("body").toggleClass("search-open");
		$(this).parent().toggleClass("open");
	});

	$("time").each(function() {
		if ($(this).attr("datetime")) {
			$(this).html($(this).attr("datetime"));
		}
	});

	var page={"214":92,"206":93,"229":94,"425":95,"224":96,"231":97}
	if($(".section_page").size()>0 && !$("body.page-id-289").length) {
		var idsect=$(".section_page").attr("parentid");
		if(idsect!=233) {
			var menu_id=page[idsect];
			$(".directions > .sub-menu").css("display","block");
			$(".directions").addClass("open");
			$(".menu-item-"+menu_id).addClass("current-menu-item");
		}
	}


	if ($("#fep-message").length) {

		var cffeps;

		function checkForFEPSuccess() {
			if (jQuery("#fep-message").hasClass("success")) {
				jQuery("#fep-message").html("Votre appel à tous est maintenant publié!");

				clearTimeout(cffeps);
				setTimeout(function() {
					location.reload();
				}, 2000);
			} else {
				cffeps = setTimeout(function() { checkForFEPSuccess(); }, 1000);
			}
		}

		cffeps = setTimeout(function() { checkForFEPSuccess(); }, 1000);
	}



	function loadAppelATousYear() {
		$("#y").on("change", function() {
			var current_page = window.location.href;
			current_page = current_page.replace(/\?y=[0-9]+/, '');

			$(".prod-details.AppelTous").load(current_page + '?y=' + $(this).val() + ' .prod-details.AppelTous', function() {
				loadAppelATousYear();
			});

		});

		if ($("body.page-template-appels_nous_template").length) {
			//$("body.page-template-appels_nous_template .mce-toolbar-grp").hide();
			$("body.page-template-appels_nous_template #fep-featured-image").hide();
			$("body.page-template-appels_nous_template #fep-tags").hide();
			$('body.page-template-appels_nous_template label[for="fep-tags"]').hide();

			$('body.page-template-appels_nous_template label[for="fep-category"]').hide();
			$('body.page-template-appels_nous_template label[for="fep-post-title"]').html("Titre");
			$('body.page-template-appels_nous_template label[for="fep-post-content"]').html("Message");

			// var direction = $("#tab_1_1_8 li.active").attr("class").replace(/\s*(active|direction)\s*/g, '');
			// switch (direction * 1) {
			// 	case 1: $("#fep-category").val("4"); break;
			// 	case 2: $("#fep-category").val("16"); break;
			// 	case 3: $("#fep-category").val("17"); break;
			// 	case 4: $("#fep-category").val("18"); break;
			// 	case 5: $("#fep-category").val("19"); break;
			// 	case 6: $("#fep-category").val("20"); break;
			// 	default: $("#fep-category").val("23");
			// }
			$("#fep-category").val("23");

			$("#fep-category").hide();

			$("#fep-submit-post").html("Envoyer");
		}
	}


	loadAppelATousYear();

	$(window).load(function() {


		if ($("body.page-template-appels_nous_template").length) {
			//$("body.page-template-appels_nous_template .mce-toolbar-grp").hide();
			$("body.page-template-appels_nous_template #fep-featured-image").hide();
			$("body.page-template-appels_nous_template #fep-tags").hide();
			$('body.page-template-appels_nous_template label[for="fep-tags"]').hide();

			$('body.page-template-appels_nous_template label[for="fep-category"]').hide();
			$('body.page-template-appels_nous_template label[for="fep-post-title"]').html("Titre");
			$('body.page-template-appels_nous_template label[for="fep-post-content"]').html("Message");

			// var direction = $("#tab_1_1_8 li.active").attr("class").replace(/\s*(active|direction)\s*/g, '');
			// switch (direction * 1) {
			// 	case 1: $("#fep-category").val("4"); break;
			// 	case 2: $("#fep-category").val("16"); break;
			// 	case 3: $("#fep-category").val("17"); break;
			// 	case 4: $("#fep-category").val("18"); break;
			// 	case 5: $("#fep-category").val("19"); break;
			// 	case 6: $("#fep-category").val("20"); break;
			// 	default: $("#fep-category").val("23");
			// }

			$("#fep-category").val("23");

			$("#fep-category").hide();

			$("#fep-submit-post").html("Envoyer");
		}


		if($("ul.forum-titles li.bbp-topic-reply-count").length) {
			$("ul.forum-titles li.bbp-topic-reply-count").html("Réponses");
		}

		if ($("#post-81 #tab_1_1_8 ul.nav-tabs li.active").length) {
			var active_class = $("#post-81 #tab_1_1_8 ul.nav-tabs li.active").attr("class").replace(/\s?active\s?/, '');
			$("#post-81 #tab_1_1_8 ul.nav-tabs").attr("class", "nav nav-tabs nav-justified " + active_class);
		}

		$("#post-81 #tab_1_1_8 ul.nav-tabs li a").on("click", function() {
			var active_class = $(this).parent().attr("class").replace(/\s?active\s?/, '');
			$(this).parents("ul").attr("class", "nav nav-tabs nav-justified " + active_class);
		});

		if (!$("body#bdpme").length && !$("#dossiers").length) {

			$(".checker span").on("click", function() {
				var filters = jQuery.parseJSON($(".quibo-calendar-view .calendar-wrapper").attr("data-filters"));

				var index = $(this).find("input").attr("value");

				if ($(this).hasClass("checked")) {
					filters[index] = "1";
				} else {

					if (filters) {
						delete filters[index];
					}
				}

				$(".quibo-calendar-view .calendar-wrapper").attr("data-filters", JSON.stringify(filters));

				$(".quibo-calendar-view").load("?ajax=1&date=" + $(".quibo-calendar-view").find(".calendar-wrapper").attr("data-date") + "&layout=" + $(".quibo-calendar-view").find(".calendar-wrapper").attr("data-layout") + "&filters=" + encodeURIComponent($(".quibo-calendar-view").find(".calendar-wrapper").attr("data-filters")) + " .calendar-wrapper", function() { initCalendarToolbar(); });

			});

		}

		function initCalendarToolbar() {



			$(".quibo-calendar-view .fc-button-month").on("click", function() {
				$(this).parents(".quibo-calendar-view").load("?ajax=1&date=" + $(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-date") + "&layout=month&filters=" + encodeURIComponent($(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-filters")) + " .calendar-wrapper", function() { initCalendarToolbar(); });
			});

			$(".quibo-calendar-view .fc-button-agendaWeek").on("click", function() {
				$(this).parents(".quibo-calendar-view").load("?ajax=1&date=" + $(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-date") + "&layout=week&filters=" + encodeURIComponent($(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-filters")) + " .calendar-wrapper", function() { initCalendarToolbar(); });
			});

			$(".quibo-calendar-view .fc-button-agendaDay").on("click", function() {
				$(this).parents(".quibo-calendar-view").load("?ajax=1&date=" + $(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-date") + "&layout=day&filters=" + encodeURIComponent($(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-filters")) + " .calendar-wrapper", function() { initCalendarToolbar(); });
			});

			$(".quibo-calendar-view .fc-button-prev").on("click", function() {
				$(this).parents(".quibo-calendar-view").load("?ajax=1&date=" + $(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-prev-date") + "&layout=" + $(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-layout") + "&filters=" + encodeURIComponent($(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-filters")) + " .calendar-wrapper", function() { initCalendarToolbar(); });
			});

			$(".quibo-calendar-view .fc-button-next").on("click", function() {
				$(this).parents(".quibo-calendar-view").load("?ajax=1&date=" + $(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-next-date") + "&layout=" + $(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-layout") + "&filters=" + encodeURIComponent($(this).parents(".quibo-calendar-view").find(".calendar-wrapper").attr("data-filters")) + " .calendar-wrapper", function() { initCalendarToolbar(); });
			});
		}

		initCalendarToolbar();

	});

	$(".mobile-handle").on("click", function() {
		$(this).toggleClass("open");
		$(".mobile-dd").toggleClass("open");
	});


	/* -------------------------------------------------------------------------- */
	                   /** ∆∆      FUNCTIONS      ∆∆ **/
	/* -------------------------------------------------------------------------- */


	// function get_weather(location){

	// 	var days = {
	// 		"Mon" : "Lun",
	// 		"Tue" : "Mar",
	// 		"Wed" : "Mer",
	// 		"Thu" : "Jeu",
	// 		"Fri" : "Ven",
	// 		"Sat" : "Sam",
	// 		"Sun" : "Dim"
	// 	};

		// $.simpleWeather({
		// 	location: location,
		// 	unit: 'c',
		// 	success: function(weather) {
		// 		var html = '';

		// 		if ("geolocation" in navigator) {
		// 			html += '<a class="js-geolocation">Calculer ma position</a><a>';
		// 		}

		// 		html += '<a class="city">' + weather.city + ' ' + weather.temp +'&deg;<i class="icon-' + weather.code + '"></i></a>';

		// 		html += '<div class="forecast">';

		// 		html += '<div class="current"><i class="icon-' + weather.code + '"></i><span class="temp">' + weather.temp +'&deg;<span class="range">' + weather.high + '&deg; / ' + weather.low + '&deg;</span></span></div>';

		// 		for(var i=0;i<weather.forecast.length;i++) {
		// 			html += '<div class="item"><div class="day">' + days[weather.forecast[i].day] + '</div><i class="icon-' + weather.code + '"></i><div class="temp">' + weather.forecast[i].high + '&deg;</div></div>';
		// 		}

		// 		html += '</div>';

		// 		$("#weather").html(html);
		// 	},
		// 	error: function(error) {
		// 		$("#weather").html('<p>'+error+'</p>');
		// 	}
		// });
	//


	/* -------------------------------------------------------------------------- */
	                   /** ∆∆      EVENTS      ∆∆ **/
	/* -------------------------------------------------------------------------- */


	// if ( jQuery.cookie('user_position') == undefined) {
	// 	get_weather('Montreal, QC');
	// } else {
	// 	var user_position = jQuery.cookie('user_position');
	// 	get_weather(user_position);
	// }

	// $('#weather')
	// 	.on({
	// 		'click': function() {
	// 			navigator.geolocation.getCurrentPosition(function(position) {
	// 				jQuery.cookie('user_position', position.coords.latitude + ',' + position.coords.longitude, { expires: 365, path: '/' });
	// 				get_weather(position.coords.latitude + ',' + position.coords.longitude); //load weather using your lat/lng coordinates
	// 			});
	// 		}
	// 	}, '.js-geolocation' )

	// 	.on({
	// 		'click': function() {
	// 			$(".forecast").slideToggle(500);
	// 		}
	// 	}, '.city' );

});




