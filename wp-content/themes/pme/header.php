<?php
    if ( ! is_user_logged_in() && !preg_match("/\/login/", $_SERVER["REQUEST_URI"]) ) {
        //$login_url=wp_login_url( $redirect );
        header("Location: /extranet/login/?redirect_to=" . $_SERVER["REQUEST_URI"]);
    }

setlocale (LC_TIME, 'fr_FR.utf8','fra');
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 9]>
<html class="ie9" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="utf-8"/>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1" name="viewport"/>
<!--meta content="" name="description"/>
<meta content="" name="author"/-->
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGIN STYLES -->
<!--link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" type="text/css"/-->
<!--link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css"/-->
<!--link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/fullcalendar/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css"/-->
<!-- END PAGE LEVEL PLUGIN STYLES -->
<!-- BEGIN THEME STYLES -->
<!--link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/data-tables/DT_bootstrap.css" rel="stylesheet" type="text/css"/-->
<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/jquery-ui.theme.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/styles.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/print.css" rel="stylesheet" type="text/css" media="print"/>
<link href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/mobile.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
<link rel="shortcut icon" href="/extranet/favicon.ico"/>
<script type='text/javascript' src='/extranet/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/extranet/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='/extranet/wp-includes/js/tinymce/tinymce.min.js?ver=4310-20160418'></script>
</head>

<body <?php body_class("page-header-fixed"); ?>>
	<!-- BEGIN HEADER -->
	<div class="header navbar ">
		<div class="header-inner">
			<?php global $current_user;
				get_currentuserinfo();

				/*echo 'Username: ' . $current_user->user_login . "\n";
				echo 'User email: ' . $current_user->user_email . "\n";
				echo 'User first name: ' . $current_user->user_firstname . "\n";
				echo 'User last name: ' . $current_user->user_lastname . "\n";
				echo 'User ID: ' . $current_user->ID . "\n";*/
            ?>

            <div class="searchbox">
            	<a id="search-handle"><span></span></a>
            	<?php get_search_form(); ?>
            </div>

			<ul class="nav navbar-nav pull-right">
				<!--li class="searchbox"><?php get_search_form(); ?></li-->
				<!-- BEGIN USER LOGIN DROPDOWN -->

				<li class="dropdown user">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="fa fa-user"></i>
						<span class="username"><?php echo $current_user->display_name; ?></span>
						<i class="fa fa-angle-down"></i>
					</a>
					<ul class="dropdown-menu">
						<li>
							<a href="/extranet/wp-admin/profile.php">
								<i class="fa fa-user"></i> Mon Profil
							</a>
						</li>
						<!--<li>
							<a href="page_calendar.html">
								<i class="fa fa-calendar"></i> My Calendar
							</a>
						</li>
						<li>
							<a href="inbox.html">
								<i class="fa fa-envelope"></i> My Inbox
								<span class="badge badge-danger">
									 3
								</span>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-tasks"></i> My Tasks
								<span class="badge badge-success">
									 7
								</span>
							</a>
						</li>
						<li class="divider">
						</li>
						<li>
							<a href="javascript:;" id="trigger_fullscreen">
								<i class="fa fa-arrows"></i> Full Screen
							</a>
						</li>
						<li>
							<a href="extra_lock.html">
								<i class="fa fa-lock"></i> Lock Screen
							</a>
						</li>-->
                        <?php if ( is_user_logged_in() ) {  ?>
                        <li>
							<a href="<?php echo wp_logout_url(); ?>">
								<i class="fa fa-key"></i> Déconnexion
							</a>
						</li>
                        <?php }else{ ?>
                            <li>
                                <a href="<?php echo wp_login_url( $redirect ); ?> ">
                                    <i class="fa fa-key"></i> Connexion
                                </a>
                            </li>

                        <?php } ?>


					</ul>
				</li>
				<!-- END USER LOGIN DROPDOWN -->

			</ul>
		</div>
	</div>
	<!-- END HEADER -->
	<div class="clearfix"></div>

	<!-- BEGIN CONTAINER -->
	<div class="page-container">
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar-wrapper">
			<div class="navbar-no-scroll page-sidebar navbar-collapse collapse">
				<!-- add "navbar-no-scroll" class to disable the scrolling of the sidebar menu -->

				<h1 class="site-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/pme/pme-logo.png" src="<?php bloginfo( 'name' ); ?>" />
					</a>
				</h1>

				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">Primary Menu</button>

				<div id="primary-menu">
					<!-- BEGIN SIDEBAR MENU -->
					
					<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
						<?php

							wp_nav_menu(array(
								'container'       => false,
								'before'          => '',
								'after'           => '',
								'link_before'     => '<i class="fa"></i>',
								'link_after'      => '<span class="arrow"></span>',
								'items_wrap'      => '%3$s',
							));


						?>
					</ul>
					<!-- END SIDEBAR MENU -->
				</div>

			</div>
		</div>
		<!-- END SIDEBAR -->
	<div class="page-content-wrapper">
			<div class="page-content">
				<div class="locales">
					<div id="date"><?php 

						$english_date = strftime("%d %B %Y");

						
						$french_date = str_replace("January", "Janvier", $english_date);
						$french_date = str_replace("February", "Février", $french_date);
						$french_date = str_replace("March", "Mars", $french_date);
						$french_date = str_replace("April", "Avril", $french_date);
						$french_date = str_replace("May", "Mai", $french_date);
						$french_date = str_replace("June", "Juin", $french_date);
						$french_date = str_replace("July", "Juillet", $french_date);
						$french_date = str_replace("August", "Août", $french_date);
						$french_date = str_replace("September", "Septembre", $french_date);
						$french_date = str_replace("October", "Octobre", $french_date);
						$french_date = str_replace("November", "Novembre", $french_date);
						$french_date = str_replace("December", "Décembre", $french_date);

						echo $french_date; ?></div>
					<div id="weather"></div>
				</div>
