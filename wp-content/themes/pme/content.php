<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

<div class="row">
	<div class="col-md-12">

		<?php if ( is_single() ) : ?>
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
			<h3 class="page-title"><?php echo get_the_title(); ?></h3>
			<?php
				if (get_post_type() == "post") {
					echo
						"<ul class=\"page-breadcrumb breadcrumb\">
        					<li><i class=\"fa fa-home\"></i> <a href=\"/extranet/\">Extranet</a> <i class=\"fa fa-angle-right\"></i></li>
							<li><a href=\"/extranet/appel-a-tous/\">Appel à tous</a> <i class=\"fa fa-angle-right\"></i></li>
                			<li>" . get_the_title() . "</li>
                		</ul>";

				} else {
					echo get_breadcrumb();
				}

			?>
			<!-- END PAGE TITLE & BREADCRUMB-->
		<?php else : ?>
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
			<h3 class="page-title"><a href="<?php echo esc_url( get_permalink() ); ?>"><?php echo get_the_title(); ?></a></h3>
			<!-- END PAGE TITLE & BREADCRUMB-->
		<?php endif; ?>

	</div>
</div>



<div class="row">
	<div id="post-<?php the_ID(); ?>" <?php post_class("col-md-9 blog-tag-data"); ?>>

		<?php the_post_thumbnail('full', array(
			'class'	=> "attachment-$size img-responsive"
		)); ?>



<?php if ( is_search() ) : ?>
<div class="entry-summary">
	<?php the_excerpt(); ?>
</div><!-- .entry-summary -->
<?php else : ?>
<div class="entry-content">
	<?php
		the_content( __( 'Continue reading <span class="meta-nav">&rarr;</span>', 'twentyfourteen' ) );
		wp_link_pages( array(
			'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentyfourteen' ) . '</span>',
			'after'       => '</div>',
			'link_before' => '<span>',
			'link_after'  => '</span>',
		) );
	?>
</div><!-- .entry-content -->
<?php endif; ?>

<?php the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' ); ?>
</div></div><!-- #post-## -->

<?php

	if (get_post_type() == "post") {
		if ( comments_open() || get_comments_number() ) :
			comments_template();
		endif;
	}

?>





