<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php 
$page = "collaboratrices";

$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "corail",
);

$color = $colors[$page];

?>
<div id="eventContent" title="Event Details" style="display:none;">
    Start: <span id="startTime"></span><br>
    End: <span id="endTime"></span><br><br>
    <p id="eventInfo"></p>
    
</div>

<div class="<?php echo $color; ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">		
					<h3 class="page-title">Droit de la personne</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="/">Intranet</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>Droit de la personne</li>
					</ul>			
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>	
			<div class="row">
				<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-9 blog-tag-data">
					<div class="entry-content ">
						<div class="tabbable-custom nav-justified">
							<ul class="nav nav-tabs nav-justified">
								<li class="active">
									<a href="#tab_1_1_1" data-toggle="tab">Aperçu<br /></a>
								</li>
								<li><a href="#tab_1_1_2" data-toggle="tab">Calendrier<br /></a>
								</li>
								<li><a class="tab_1_1_3" href="#tab_1_1_3" data-toggle="tab">Liste des membres</a>
								</li>
								<li><a class="tab_1_1_4" href="#tab_1_1_4" data-toggle="tab">Comptes rendus</a>

								</li>
								<li><a href="#tab_1_1_5" data-toggle="tab">Documents<br /></a>
								</li>
								<li><a href="#tab_1_1_6" data-toggle="tab">Produits informatiques</a>
								</li>
								<li><a href="#tab_1_1_7" data-toggle="tab">Forum<br /></a>
								</li>
							</ul>						
						</div>
					</div><!-- .entry-content -->
				</div>
			</div><!-- #post-## -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="portlet box green-meadow calendar">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-gift"></i>Calendar
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-3 col-sm-12">
							<!-- BEGIN DRAGGABLE EVENTS PORTLET-->
							<h3 class="event-form-title">Catégories</h3>
							<div id="external-events">
								<form class="inline-form">
									<ul>
										<li><input type="checkbox" >Droit de la personne</li>
										<li><input type="checkbox" >Droit des affaires</li>
										<li><input type="checkbox" >Droit Agricole</li>
										<li><input type="checkbox" >Droit Immobilier</li>
										<li><input type="checkbox" >Comité de gestion des bureaux</li>
										<li><input type="checkbox" >Développement des affaires</li>
										<li><input type="checkbox" >Médias Sociaux</li>												
									</ul>
								</form>
								<h3 class="event-form-title">Légende</h3>
								<ul class="legende">										
									<li><span class="blue"></span>Formation</li>
									<li><span class="orange"></span>Evénements généraux</li>
									<li><span class="green"></span>Activités extérieures</li>
									<li><span class="red"></span>Direction de travail / Réunion</li>
									<li><span class="corail"></span>Collaboratrices</li>
								</ul>
							</div>
							<!-- END DRAGGABLE EVENTS PORTLET-->
						</div>
						<div class="col-md-9 col-sm-12">
							<div id="calendar1" class="has-toolbar">
							</div>
						</div>
					</div>
					<!-- END CALENDAR PORTLET-->
				</div>
			</div>
		</div>
	</div>
</div>
<?php
get_footer();
