<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php 
$page = "";

$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "corail",
	"autres"=>"autres",
);

$color = $colors[$page];

?>
<div id="eventContent" title="Event Details" style="display:none;">
    Start: <span id="startTime"></span><br>
    End: <span id="endTime"></span><br><br>
    <p id="eventInfo"></p>
    
</div>

<div class="<?php echo $color; ?> image du reseau">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">					
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title"><?php echo get_the_title(); ?></h3>
					<?php echo get_breadcrumb(); ?>
					<!-- END PAGE TITLE & BREADCRUMB-->						
				</div>
			</div>
		</div>
	</div>	
	<div class="row">
		<div class="col-md-12">			
		   <div class="container-fluid">
		     <div class="accordion" id="accordion2">
		            <div class="accordion-group">
		              <div class="accordion-heading">
		                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
		                  Cahier des normes graphiques
		                </a>
		              </div>
		              <div id="collapseOne" class="accordion-body collapse" style="height: 0px; ">
		                <div class="accordion-inner">
		                  	<table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nom</th>
											<th></th>
											<th>Format</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="icone-pdf">
												<i class="logo-frame"></i>
											</td>
											<td>
												<div class="col-md-8">
													<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
												</div>
												<div class="col-md-4">
													<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												</div>
											</td>
											<td>--</td>
											<td>.zip</td>
										</tr>
									</tbody>
							</table>
		                </div>
		              </div>
		            </div>
		            <div class="accordion-group">
		              <div class="accordion-heading">
		                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
		                 Documents
		                </a>
		              </div>
		              <div id="collapseTwo" class="accordion-body collapse">
		                <div class="accordion-inner">
		                  <table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
									<thead>
										<tr>
											<th>Nom</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<div class="col-md-8">
													<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
												</div>
												<div class="col-md-4">
												</div>
											</td>
										</tr>
										<tr>
											<td>
												<div class="col-md-8">
													<a href="http://www.pmeinter.com/extranet/download/calendrier-2016/">Calendrier 2016</a>
												</div>
												<div class="col-md-4">
												</div>
											</td>
											
										</tr>
										<tr>
											<td>
												<div class="col-md-8">
													<a href="http://www.pmeinter.com/extranet/download/fonds-ppt/">Fonds PPT</a>
												</div>
												<div class="col-md-4">
										
												</div>
											</td>
											
										</tr>
									</tbody>
							</table>
		                </div>
		              </div>
		            </div>
		            <div class="accordion-group">
		              <div class="accordion-heading">
		                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
		                  Logos
		                </a>
		              </div>
		              <div id="collapseThree" class="accordion-body collapse">
		                <div class="accordion-inner">
		                	<div class="row images-tab">
			                	<div class="col-md-3">		                	    					
	            					<img src="/wp-content/themes/pme/img/img-template/img-reseau.jpg" alt="">
	            					<p>Couleur</p>
	            					<a href= "#">EPS</a> • <a href= "#">PNG</a>
			                	</div>
			                	<div class="col-md-3">		                	    					
	            					<img src="/wp-content/themes/pme/img/img-template/img-reseau.jpg" alt="">
	            					<p>Noir et blanc</p>
	            					<a href= "#">EPS</a> • <a href= "#">PNG</a>
			                	</div>		
			                	<div class="col-md-3">		                	    					
	            					<img src="/wp-content/themes/pme/img/img-template/img-reseau.jpg" alt="">
	            					<p>Couleur</p>
	            					<a href= "#">EPS</a> • <a href= "#">PNG</a>
			                	</div>		
			                	<div class="col-md-3">		                	    					
	            					<img src="/wp-content/themes/pme/img/img-template/img-reseau.jpg" alt="">
	            					<p>Couleur</p>
	            					<a href= "#">EPS</a> • <a href= "#">PNG</a>
			                	</div>				
		                	</div>
		                  	<table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nom</th>
											<th></th>
											<th>Format</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="icone-pdf">
												<i class="logo-frame"></i>
											</td>
											<td>
												<div class="col-md-8">
													<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
												</div>
												<div class="col-md-4">
													<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												</div>
											</td>
											<td>--</td>
											<td>.zip</td>
										</tr>
									</tbody>
							</table>
		                </div>
		              </div>
		            </div>
		         
		          </div>
		    </div>
		</div>	
	</div>
</div>
<?php
get_footer();
