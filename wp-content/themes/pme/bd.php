<?php
/**
 * Template Name: BD
 */

$wrapper_before = '<article><div class="entry-content">';
$wrapper_after = '</div></article>';
$wrapper_id_before = '<div id="dossiers">';
$wrapper_id_after = '</div>';

if (isset($_GET["ajax"]) || isset($_POST["ajax"])) {
    $wrapper_before = '';
    $wrapper_after = '';
    $wrapper_id_after = '';
    $wrapper_id_before = '';
} else {
    get_header();
}


while ( have_posts() ) {
    the_post();

    echo $wrapper_before;

    // ini_set("display_errors", 1);
    // error_reporting(E_ALL);

    require_once(dirname(dirname(dirname(__DIR__))) . "/bd/conf/conf.inc.php");
    connect($DB);

    ob_start();
    the_content();
    $content = preg_replace("/^.*?(\[LOAD.*?\]).*?$/is", "$1", ob_get_contents());
    ob_end_clean();
    switch (trim($content)) {
        case '[LOAD dossier]':
            echo $wrapper_id_before;
            require_once(dirname(dirname(dirname(__DIR__))) . "/bd/dossiers/index.php");
            echo $wrapper_id_after;
            break;

    }

    echo $wrapper_after;
}

if (isset($_GET["ajax"]) || isset($_POST["ajax"])) {

} else {
    get_footer();
}

