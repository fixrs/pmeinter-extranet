<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 * Template Name: Section liste membre template
 */

get_header(); ?>

<?php
$page = "droit-affaires";
$color=get_field('color_page',get_the_ID());
/*
$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "yellow",
);
$color = $colors[$page];
*/

//$color =$get_color;

$id_section=get_the_ID();
$parentid=wp_get_post_parent_id( $id_section );
$id_cats_secteur=get_field("key",$parentid);
$color=get_field('color_page',$parentid);
$pagelink=get_permalink(get_the_ID() );
$list_dep=array("droit_affaire"=>1,"droit_immobilier"=>4,"droit_personne"=>2,"droit_agricole"=>5,"gestion_bureau"=>3,"dev_affaires"=>6,"action_des_collaboratrices"=>7);
$nom_seteur_membre=array_search($id_cats_secteur, $list_dep);
?>

<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title"><?php the_title();?></h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php get_home_url(); ?>">Intranet</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa"></i>
						<a href="<?php echo get_permalink($parentid); ?>"><?php echo get_the_title($parentid);?></a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><?php the_title();?></li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<div class="row">
			<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-12 blog-tag-data listeDesMembres">
				<div class="entry-content">
					<div class="tabbable-custom nav-justified">
						<div class="mobile-dd">
							<?php include_once 'global_nav.php';?>
							<div class="mobile-handle">
								<a class="fa fa-angle-down">
								</a>
							</div>
						</div>
						<div class="tab-content">
							<div id="tab_1_1_3" class="tab-pane" style="display: block;"><!--Nom du responsable :-->
								<strong>
								<?php

									//$nom_respo=get_field("nom_de_responsable",$id_section);
									//echo $nom_respo;

									$members = array();

									switch ($id_section) {
										case 259: $members = getMembresDirectionTravail(2); break;
										case 261: $members = getMembresDirectionTravail(1); break;
										case 257: $members = getMembresDirectionTravail(5); break;
										case 429: $members = getMembresDirectionTravail(4); break;
										case 264: $members = getMembresDirectionTravail(3); break;
										case 254: $members = getMembresDirectionTravail(6); break;
										case 252: $members = getMembresDirectionTravail(7); break;
										case 15334: $members = getMembresDirectionTravail(9); break;
									}

								?>

								</strong>
								<div class="portlet-body">
									<table class="table table-striped table-bordered table-hover" id="sample_1">
										<thead>
											<tr>
												<th class="no-bg">
												&nbsp
												</th>
												<th>
												Nom
												</th>
												<th>
												Prénom
												</th>
												<th>
												Etude
												</th>

												<th class="no-bg">
												Téléphone
												</th>
											</tr>
										</thead>
										<tbody>
										<?php

											foreach ($members as $member) {

												echo
													"<tr id=\"m" . $member["key"] . "\">\n".
													"	<td class=\"envelop\"><a href=\"mailto:" . $member["courriel"] . "\"><i class=\"fa fa-envelope\"></i></a></td>\n".
													"	<td><a href=\"?employe=" . $member["key"] . "#m" . $member["key"] . "\">" . $member["nom"] . "</a></td>\n".
													"	<td><a href=\"?employe=" . $member["key"] . "#m" . $member["key"] . "\">" . $member["prenom"] . "</a></td>\n".
													"	<td>" . $member["etude"] . "</td>\n".
													"	<td><a href=\"tel:" . $member["telephone"] . "\">" . $member["telephone"] . "</a></td>\n".
													"</tr>\n";


												if (isset($_GET["employe"]) && $member["key"] == $_GET["employe"]) {

													echo
														"<tr><td colspan=\"5\">\n".
														getMemberProfile($member["key"]).
														"</td></tr>";

												}


											}

											// if($nom_seteur_membre!=false) {
											// 	$list_membre=get_users( array( 'role' => $nom_seteur_membre ) );
											// } else {
											// 	$list_membre=array();
											// }

											// foreach ($list_membre as $lm) {

											// <tr>
											// 	<td class="envelop"><a href="mailto:<?php echo $lm->user_email; ? >"><i class="fa fa-envelope"></i></a></td>
											// 	<td class="linkedin"><a href="<?php echo the_author_meta( "linkedin", $lm->ID );  ? >"><i class="fa fa-linkedin-square"></i></a></td>
											// 	<td><?php echo $lm->last_name;? ></td>
											// 	<td><?php echo $lm->first_name;? ></td>
											// 	<td><?php echo the_author_meta( "etude", $lm->ID );  ? ></td>
											// 	<td><?php echo the_author_meta( "tel", $lm->ID );  ? ></td>
											// </tr>

											// }
										?>
										</tbody>
									</table>

								</div>

							</div>
						</div>
					</div>
				</div><!-- .entry-content -->
			</div>
		</div><!-- #post-## -->
	</div>
</div>
<?php
get_footer();
