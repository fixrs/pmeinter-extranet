<?php
	function month2french($string) {
		switch ($string) {
			case 'January': return "Janvier";
			case 'February': return "Février";
			case 'March': return "Mars";
			case 'April': return "Avril";
			case 'May': return "Mai";
			case 'June': return "Juin";
			case 'July': return "Juillet";
			case 'August': return "Août";
			case 'September': return "Septembre";
			case 'October': return "Octobre";
			case 'November': return "Novembre";
			case 'December': return "Décembre";
		}
	}
?>
<div class="archives">
	<!-- BEGIN ACCORDION PORTLET-->
	<div class="portlet box orange">
		<div class="portlet-title">
			<div class="caption">L'intermédiaire</div>
		</div>

		<div class="portlet-body">
			<div class="panel-group accordion" id="accordion1">
				<?php
					$posts_by_date = publication_by_date();
					$i = 0;
					setlocale(LC_TIME, "fr_FR");
					foreach ($posts_by_date as $year => $months) :
				?>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?php echo $i; ?>">
							 <?php echo $year; ?>
						</a></h4>
					</div>
					<div id="collapse_<?php echo $i; ?>" class="panel-collapse collapse">
						<div class="panel-body">

							<div class="portlet-body">
								<div class="panel-group accordion" id="accordion<?php echo $i; ?>1">
									<?php
									$j = 0;
									foreach($months as $month => $posts) :
									?>

									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title">
												<a class="accordion-toggle" href="/extranet/publications/?ad=<?php echo date("Y-m", strtotime($year . "-" . $month . "-01")); ?>"><?php echo month2french(ucfirst(strftime("%B", mktime(0, 0, 0, $month, 10)))); ?></a></h4>
										</div>

										<?php
											/*
											<div id="collapse_<?php echo $i; ?>_<?php echo $j; ?>" class="panel-collapse collapse">
												<div class="panel-body">
													<ul>
														< ?php foreach ($posts as $post) : ? >
															<li><a href="<?php the_permalink(); ? >"><?php the_title(); ? ></a></li>
														<?php endforeach; ? >
													</ul>
												</div>
											</div>
											*/
										?>

									</div>
									<?php $j++; ?>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php $i++; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	<!-- END ACCORDION PORTLET-->
</div>
