<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php 
$page = "collaboratrices";

$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "corail",
);

$color = $colors[$page];

?>
<div id="eventContent" title="Event Details" style="display:none;">
    Start: <span id="startTime"></span><br>
    End: <span id="endTime"></span><br><br>
    <p id="eventInfo"></p>
    
</div>

<div class="<?php echo $color; ?> prod-details">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title"><?php echo get_the_title(); ?></h3>
					<?php echo get_breadcrumb(); ?>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>			
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="note note-success">
				<h4 class="block">Mise en garde aux membres du réseau</h4>
				<p>Ce document est le fruit du travail d’analyse et de rédaction des membres du Comité de droit des aﬀaires. L’utilisation de ce document et du questionnaire qui y est relié devrait vous permettre de réaliser une économie substantielle de temps lors de la préparation d’une convention entre actionnaires. En outre, ce document permettra de personnaliser le produit ﬁnal en fonction des besoins de vos clients respectifs.</p>
				<p>Le tarif indicatif minimal recommandé, mais non obligatoire, relativement à un tel produit est d’environ neuf cent dollars. Nous vous recommandons, en outre, de recourir à une tariﬁcation horaire dans le cadre de la réalisation d’un tel mandat.</p>
				<p>
					Enﬁn, nous vous invitons à faire parvenir vos commentaires et suggestions à l’égard de ce produit à
					<a href="mailto:info@pmeinter.com">info@pmeinter.com</a>
				</p>
			</div>						
		</div>
	</div>
	<div class="row">.
		<div class="col-md-12">
			<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-12 blog-tag-data">
				<div id="tab_1_1_8" class="tab-pane sousmenu-nav">
					<div class="tab-content">
						<ul class="nav nav-tabs nav-justified">
							<li class="active">
								<a href="#tab_2_1_1" data-toggle="tab">Droit</br> de la personne</a>
							</li>
							<li>
								<a href="#tab_2_1_2" data-toggle="tab">Droit<br /> des affaires</a>
							</li>
							<li>
								<a href="#tab_2_1_3" data-toggle="tab">Droit<br /> agricole</a>
							</li>
							<li>
								<a href="#tab_2_1_4" data-toggle="tab">Droit<br /> immobilier</a>										
							</li>
							<li>
								<a href="#tab_2_1_5" data-toggle="tab">Gestion<br /> des bureaux</a>
							</li>
							<li>
								<a href="#tab_2_1_6" data-toggle="tab">Développement <br />des affaires</a>
							</li>					
						</ul>
						<div id="tab_2_1_1" class="soustab tab-pane active">
							<div class="sidebar-search-wrapper">					
								<form class="sidebar-search sidebar-search-bordered" action="extra_search.html" method="POST">
									<a href="javascript:;" class="remove">
									<i class="icon-close"></i>
									</a>
									<div class="input-group">
										<input type="text" class="form-control" placeholder="Search...">
										<span class="input-group-btn">
										<a href="javascript:;" class="btn submit"><i class="fa fa-search"></i></a>
										</span>
									</div>
								</form>					
							</div>
							   <div class="accordion" id="accordion2">
						            <div class="accordion-group">
						              	<div class="accordion-heading">
					                		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
							                  2014  <i class="fa fa-angle-down"></i>
							                </a>
								      	 </div>
							              <div id="collapseOne" class="accordion-body collapse" style="height: 0px; ">
							                <div class="accordion-inner">
							                  	<table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
														<thead>
															<tr>																
																<th>Nom</th>																
																<th class="public">Date de publication</th>
															</tr>
														</thead>
														<tbody>
															<tr>																
																<td>
																	<div class="col-md-10">
																		<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
																	</div>
																	<div class="col-md-2 stamp">
																		<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
																	</div>
																</td>
																<td class="public">22 Avril 2014</td>
															</tr>
														</tbody>
												</table>
							                </div>
							              </div>
								    </div>
						            <div class="accordion-group">
						              <div class="accordion-heading">
						                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
						                 	2013  <i class="fa fa-angle-down"></i>
						                </a>
						              </div>
						              <div id="collapseTwo" class="accordion-body collapse">
						               		<div class="accordion-inner">
							                  	<table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
														<thead>
															<tr>																
																<th>Nom</th>																
																<th class="public">Date de publication</th>
															</tr>
														</thead>
														<tbody>
															<tr>																
																<td>
																	<div class="col-md-10">
																		<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
																	</div>
																	<div class="col-md-2 stamp">
																		<a class="btn btn-xs" href="javascript:;">Nouveau</a>
																	</div>
																</td>																
																<td class="public">22 Avril 2014</td>
															</tr>
														</tbody>
												</table>
							                </div>
						              </div>
						            </div>
						            <div class="accordion-group">
						              <div class="accordion-heading">
						                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
						                  2012  <i class="fa fa-angle-down"></i>
						                </a>
						              </div>
						              <div id="collapseThree" class="accordion-body collapse">
						                <div class="accordion-inner">
							                  	<table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
														<thead>
															<tr>																
																<th>Nom</th>																
																<th class="public">Date de publication</th>
															</tr>
														</thead>
														<tbody>
															<tr>																
																<td>
																	<div class="col-md-10">
																		<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
																	</div>
																	<div class="col-md-2 stamp">
																		<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
																	</div>
																</td>																
																<td class="public">22 Avril 2014</td>
															</tr>
														</tbody>
												</table>
							                </div>
						              </div>
						            </div>
								</div>
						</div>															
						<div id="tab_2_1_2" class="soustab tab-pane">
								<div class="accordion" id="accordion3">
						            <div class="accordion-group">
						              	<div class="accordion-heading">
					                		<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#collapseFour">
							                  TESTING
							                </a>
								      	 </div>
							              <div id="collapseFour" class="accordion-body collapse" style="height: 0px; ">
							                <div class="accordion-inner">
							                  	<table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
														<thead>
															<tr>
																<th>&nbsp;</th>
																<th>Nom</th>
																<th>Dern. Màj</th>
																<th>Taille</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class="icone-pdf">
																	<i class="fa fa-picture-o">
																</td>
																<td>
																	<div class="col-md-8">
																		<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
																	</div>
																	<div class="col-md-4">
																		<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
																	</div>
																</td>
																<td>--</td>
																<td>--</td>
															</tr>
														</tbody>
												</table>
							                </div>
							              </div>
								    </div>
						            <div class="accordion-group">
						              <div class="accordion-heading">
						                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFive">
						                 Documents
						                </a>
						              </div>
						              <div id="collapseFive" class="accordion-body collapse">
						                <div class="accordion-inner">
						                  <table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
													<thead>
														<tr>
															<th>&nbsp;</th>
															<th>Nom</th>
															<th>Dern. Màj</th>
															<th>Taille</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td class="icone-pdf">
																<i class="fa fa-picture-o">
															</td>
															<td>
																<div class="col-md-8">
																	<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
																</div>
																<div class="col-md-4">
																	<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
																</div>
															</td>
															<td>--</td>
															<td>--</td>
														</tr>
													</tbody>
											</table>
						                </div>
						              </div>
						            </div>
						            <div class="accordion-group">
						              <div class="accordion-heading">
						                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseSix">
						                  Lorem
						                </a>
						              </div>
						              <div id="collapseSix" class="accordion-body collapse">
						                <div class="accordion-inner">
						                  <table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
												<thead>
													<tr>
														<th>&nbsp;</th>
														<th>Nom</th>
														<th>Dern. Màj</th>
														<th>Taille</th>
													</tr>
												</thead>
												<tbody>
													<tr>
														<td class="icone-pdf">
															<i class="fa fa-picture-o">
														</td>
														<td>
															<div class="col-md-8">
																<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
															</div>
															<div class="col-md-4">
																<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
															</div>
														</td>
														<td>--</td>
														<td>--</td>
													</tr>
												</tbody>
											</table>
						                </div>
						              </div>
						            </div>
								</div>						
						</div>						
					</div>
				</div>
			</div>
		</div>
	</div><!-- #post-## -->	
</div>
<?php
get_footer();
