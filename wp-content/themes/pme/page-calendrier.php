<?php

/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<div id="eventContent" title="Event Details" style="display:none;">
    <b>Date: </b><span id="startTime"></span><br>
    <b>Lieu:</b> <span id="lieu"></span><br><br>
</div>

<div class="calendrier-general">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<h3 class="page-title">Calendrier</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="/extranet/">Intranet</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>Calendrier</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="portlet box green-meadow calendar">
				<div class="portlet-title">
					<div class="caption">
						<i class="fa fa-calendar"></i>Calendrier
					</div>
				</div>
				<div class="portlet-body">
					<div class="row">
						<div class="col-md-3 col-sm-12">
							<!-- BEGIN DRAGGABLE EVENTS PORTLET-->
							<h3 class="event-form-title">Réunion des directions de travail</h3>
							<div id="external-events">
								<div class="inline-form">
									<ul>
										<?php getDirectionsTravail("li-checkbox", "all"); ?>
										<li class="anniversaire"><input type="checkbox" class="secteur" name="secteur" value ="999" >Anniversaires</li>
									</ul>
								</div>
								<!--h3 class="event-form-title">Légende</h3>
								<ul class="legende">
									<li><span class="blue"></span>Formation</li>
									<li><span class="orange"></span>Evénements généraux</li>
									<li><span class="green"></span>Activités extérieures</li>
									<li><span class="red"></span>Direction de travail / Réunion</li>
									<li><span class="corail"></span>Collaboratrices</li>
								</ul-->
							</div>
							<!-- END DRAGGABLE EVENTS PORTLET-->
						</div>
						<div class="col-md-9 col-sm-12">
							<!--div id="calendar1"  class="has-toolbar calendar_get"> </div-->
							<?php
								$_GET["filters"] = getDirectionsTravail("array");
								getCalendar();
							?>
						</div>
					</div>
					<!-- END CALENDAR PORTLET-->
				</div>
			</div>
		</div>
	</div>
</div>

<?php

get_footer();

