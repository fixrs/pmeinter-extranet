
<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
  * Template Name: Section produits informatiques detail template
 */

get_header(); ?>

<?php 
$id_section=get_the_ID();
$parentid=wp_get_post_parent_id( $id_section );
$grandparentid=wp_get_post_parent_id( $parentid );
$id_cats_secteur=get_field("key",$grandparentid);
$color=get_field('color_page',$grandparentid);
$pagelink=get_permalink($parentid);
//echo $pagelink;
if(!isset($_POST["info_id"]))
{
	header("Location:$pagelink");
}	
$id_info_product=$_POST["info_id"];	
//echo $id_info_product;


?>
<div id="eventContent" title="Event Details" style="display:none;">
    Start: <span id="startTime"></span><br>
    End: <span id="endTime"></span><br><br>
    <p id="eventInfo"></p>
    
</div>

<div class="<?php echo $color; ?> accordeon-general">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">		
					<h3 class="page-title"><?php echo get_the_title($parentid);   ?></h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="/">Intranet</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
                            <a href="<?php echo get_permalink($grandparentid); ?>"><?php echo get_the_title($grandparentid);   ?></a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <a href="<?php echo get_permalink($parentid); ?>"><?php echo get_the_title($parentid);   ?></a>
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li>
                            <?php echo get_the_title($id_info_product);   ?>
                        </li>
					</ul>			
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>	
			<div class="row">
				<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-12 blog-tag-data">
					<div class="entry-content ">
						<div class="tabbable-custom nav-justified">
						<ul class="nav nav-tabs nav-justified">
								
							<li>
								<a href="<?php echo get_permalink($grandparentid); ?>" >Aperçu<br /></a>
							</li>
							<?php 
								
								$args = array(
										'post_type'      => 'page',
										'posts_per_page' => -1,
										'post_parent'    => $grandparentid,
										'order'          => 'ASC',
										'orderby'        => 'menu_order'
								);
								
								
								$allpages_sections = new WP_Query( $args );
								
									if ( $allpages_sections->have_posts() ) : 
								
								  		$i=2; 
										while ( $allpages_sections->have_posts() ) : $allpages_sections->the_post(); 
								   
							
							?>					
							<li <?php if (get_the_ID()==$id_section) {?>class="active" <?php }?> class="section_page"  parentid="<?php echo $grandparentid ?>" ><a class="tab_1_1_<?php echo $i;?>" href="<?php echo get_permalink();?>" ><?php  the_title(); ?><br /></a>
							</li>
							<?php 	
										$i++;
										endwhile;
									endif;
							?>
						</ul>
						</div>
					</div><!-- .entry-content -->
				</div>
			</div><!-- #post-## -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
            <h4 class="page-title"><?php echo get_the_title($id_info_product);?></h4>
			<div class="note note-success">

				<?php //echo get_the_content($parentid);
                        echo get_post_field('post_content', $id_info_product);?>
			</div>						
		</div>
	</div>
	<?php 
			//print_r(get_field("pages_associes_info_detail",$id_info_product));
			if( have_rows('doc_produits_info',$id_info_product) ):
				
			///echo "ici";
			// loop through the rows of data
			while ( have_rows('doc_produits_info',$id_info_product) ) : the_row();
				
			// get all title and value for accordtion
			$listoptions_objet=get_sub_field_object('document_info_secteur',$id_info_product);
			
			endwhile;
				
			else :
				
			// no rows found
			
			endif;
			$options_arr=array();
			$rows_selected=array();
			foreach ($listoptions_objet['choices'] as $k => $v )
			{
			
			
				$options_arr[$k] =$v;
			}
			//print_r($options_arr);
			
			
			if(get_field('doc_produits_info', $id_info_product))
			{
				
				while(has_sub_field('doc_produits_info', $id_info_product))
					{
						$doc_name=get_sub_field('document_inofo_titre');
						$doc_file=get_sub_field('document_file');
						$doc_secteur=get_sub_field('document_info_secteur');
						$doc_nouveau=get_sub_field('nouveau_tag_info');
						$doc_date_info=get_sub_field('date_info');
						//$sectuer_label=
						$rows_selected[$doc_secteur][]=array("name"=>$doc_name,"file"=>$doc_file,"secteur"=>$doc_secteur,"nouveau"=>$doc_nouveau,"date"=>$doc_date_info);
						
					}
			}
			
			
					
				//print_r($rows_selected)  	
				
			?>
	
	
	<div class="row">
		<div class="col-md-12">			
		   <div class="container-fluid">
		     <div class="accordion" id="accordion2">
		           <?php 
		           		$class_array=array("One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten","Eleven","Twelve","Thirteen","Fourten");
		           		$i=0;
		           		foreach ($rows_selected as $key => $item)
						  {
		           			if(count($item)>=1)
		           			{		
		           ?>
		           
		            <div class="accordion-group">
		              <div class="accordion-heading detail_info">
		                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapse<?php echo $class_array[$i] ?>">
		                  <?php echo $options_arr[$key]; ?>
		                </a>
		              </div>
		              <div id="collapse<?php echo $class_array[$i] ?>" class="accordion-body collapse in" >
		                <div class="accordion-inner">
		                  	<table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nom</th>
											<th>Dern. Màj</th>
											<th>Taille</th>
										</tr>
									</thead>
									<tbody>
										<?php 
											foreach ($item as $it)
											{
										?>
										<tr>
                                            <?php
                                            $filename = $it["file"]["url"];
                                            $filetype_arr=wp_check_filetype($filename);
                                            $filetype=$filetype_arr["ext"];
                                           if(isset($GLOBALS['doc_type'][$filetype]))
                                           {
                                               $icon_ext=$GLOBALS['doc_type'][$filetype];
                                           }else
                                           {
                                               $icon_ext=$GLOBALS['doc_type']["file"];
                                           }
                                            ?>
											<td class="icone-pdf">
												<i class="fa <?php echo $icon_ext;?>"></i>
											</td>
											<td>
												<div class="col-md-8">
													
													<a target="_blank" href='<?php echo $it["file"]["url"] ?>' title="<?php echo $it["file"]["title"] ?>"><?php echo $it["name"] ?></a>
												</div>
												<div class="col-md-4">
													<?php if($it["nouveau"]){?>
													<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
													<?php } ?>
												</div>
											</td>
											<td>
											<?php 
											setlocale(LC_ALL, 'fr_CA.utf-8');
											$dt = strftime("%e %B %Y", strtotime($it["date"]));
											$dt = mb_convert_encoding($dt, 'utf-8');
											echo $dt; 
											?>
											</td>
											<td>
											<?php 

												$headers  = get_headers($filename, 1);
												$fsize    = $headers['Content-Length'];
												if($fsize<1024)
													{
														echo round($fsize/1024)."octets";
													}elseif($fsize>=1024)
														{
															echo round($fsize/1024)."K";
														}elseif($fsize>=1048576)
															{
																echo round($fsize/1048576)."M";
															}

													
												?>
											
											
											</td>
										</tr>
									<?php }?>	
									</tbody>
							</table>
		                </div>
		              </div>
		            </div>
		          
		           <?php  $i++;} }?>
		           
		           </div>
		    </div>
		</div>	
	</div>
</div>


<?php
get_footer();
