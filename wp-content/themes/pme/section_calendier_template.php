<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 * Template Name: Section calendier template
 */

get_header(); ?>

<?php
$page = "droit-affaires";
$color=get_field('color_page',get_the_ID());
/*
$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "yellow",
);
$color = $colors[$page];
*/

//$color =$get_color;

$id_section=get_the_ID();
$parentid=wp_get_post_parent_id( $id_section );
?>
<div id="eventContent" title="Event Details" style="display:none;">
    <b>Date: </b><span id="startTime"></span><br>
     <b>Lieu:</b> <span id="lieu"></span><br><br>
</div>
<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title"><?php the_title();?></h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php get_home_url(); ?>">Intranet</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa"></i>
						<a href="<?php echo get_permalink($parentid); ?>"><?php echo get_the_title($parentid);?></a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><?php the_title();?></li>
				</ul>

				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<div class="row">
			<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-12 blog-tag-data">
				<div class="entry-content">
					<div class="tabbable-custom nav-justified">
						<div class="mobile-dd">
							<?php include_once 'global_nav.php';?>
							<div class="mobile-handle">
								<a class="fa fa-angle-down">
								</a>
							</div>
						</div>
						<div class="tab-content">

							<div id="tab_1_1_2"  style="display: block;" class="tab-pane">
								<!--div id="calendar_secteur_calendar" idsecteur="<?php echo get_field("key", $parentid);?>" class="has-toolbar calendar_get"-->

								<?php
									$filters = array();
									switch ($post->post_parent) {
										case 214: $filters[2] = 1; break;
										case 206: $filters[1] = 1; break;
										case 229: $filters[5] = 1; break;
										case 425: $filters[4] = 1; break;
										case 224: $filters[3] = 1; break;
										case 231: $filters[6] = 1; break;
										case 233: $filters[7] = 1; break;
										case 15330: $filters[9] = 1; break;
									}

									$_GET["filters"] = $filters;

									getCalendar();
								?>

							</div>
						</div>

					</div>
				</div><!-- .entry-content -->
			</div>
		</div><!-- #post-## -->
	</div>
</div>
<?php
get_footer();
