<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 * Template Name: Section document template
 */

get_header(); ?>

<?php


/*
$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "yellow",
);
$color = $colors[$page];
*/

//$color =$get_color;
//echo $color;
$id_section=get_the_ID();
$parentid=wp_get_post_parent_id( $id_section );
$id_cats_secteur=get_field("key",$parentid);
$color=get_field('color_page',$parentid);
global $paged,$total_page;




?>

<div class="row <?php echo $color; ?>">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title"><?php the_title();?></h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php get_home_url(); ?>">Intranet</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa"></i>
						<a href="<?php echo get_permalink($parentid); ?>"><?php echo get_the_title($parentid);?></a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><?php the_title();?></li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<div class="row">
			<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-12 blog-tag-data documents">
				<div class="entry-content">
					<div class="tabbable-custom nav-justified">
						<div class="mobile-dd">
							<?php include_once 'global_nav.php';?>
							<div class="mobile-handle">
								<a class="fa fa-angle-down"></a>
							</div>
						</div>
						<div class="tab-content">
							<div id="tab_1_1_5"  style="display: block;" class="tab-pane">
								<div class="note note-success">
									<?php echo get_the_content(); ?>

                                    <?php
                                        $BLOG_BASE = preg_replace("/\?.*$/is", "", $_SERVER["REQUEST_URI"]);
                                        switch ($parentid) {
                                            case '214':
                                                $categories = get_categories(array('taxonomy' => 'doc-cat', 'parent' => 28));
                                                break;

                                            case '206':
                                                $categories = get_categories(array('taxonomy' => 'doc-cat', 'parent' => 29));
                                                break;

                                            case '229':
                                                $categories = get_categories(array('taxonomy' => 'doc-cat', 'parent' => 30));
                                                break;

                                            case '425':
                                                $categories = get_categories(array('taxonomy' => 'doc-cat', 'parent' => 31));
                                                break;

                                            case '224':
                                                $categories = get_categories(array('taxonomy' => 'doc-cat', 'parent' => 32));
                                                break;

                                            case '233':
                                                $categories = get_categories(array('taxonomy' => 'doc-cat', 'parent' => 34));
                                                break;

                                            case '15330':
                                                $categories = get_categories(array('taxonomy' => 'doc-cat', 'parent' => 54)) ;
                                                break;

                                            default:
                                                $categories = get_categories(array('taxonomy' => 'doc-cat'));
                                                break;
                                        }

                                        $categories_html = "";
                                        foreach ($categories as $cat_id => $cat) {

                                            if (single_cat_title('', false) == $cat->name) {
                                                $categories_html .= "<li class=\"cat " . $cat->slug . " current\"><a href=\"" . $BLOG_BASE . "?c=" . $cat->slug . "\">" . $cat->name . "</a></li>\n";
                                            } else {
                                                $categories_html .= "<li class=\"cat " . $cat->slug . "\"><a href=\"" . $BLOG_BASE . "?c=" . $cat->slug . "\">" . $cat->name . "</a></li>\n";
                                            }
                                        }
                                    ?>

                                    <ul class="nav-category">
                                        <li class="label"><?php echo __('Sélectionnez une catégorie', 'pme'); ?></li>
                                        <li class="all"><a href="<?php echo $BLOG_BASE; ?>"><?php echo __('Tous', 'pme'); ?></a></li>
                                        <?php echo $categories_html; ?>
                                    </ul>
								</div>
								<table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nom</th>
											<th>Dern. Màj</th>
											<th>Taille</th>
										</tr>
									</thead>
									<tbody>
										<?php

                                            if (isset($_GET["c"])) {

                                                $args_total = array(
                                                    'post_type'     => 'documents',
                                                    'posts_per_page'   => -1,
                                                    'tax_query' => array(
                                                        array(
                                                            'taxonomy' => 'doc-cat',
                                                            'field'    => 'slug',
                                                            'terms'    => $_GET["c"],
                                                        ),
                                                    ),
                                                    'meta_query' => array (
                                                        array (
                                                            'key' => 'pages_associes_document',
                                                            'value' => $id_cats_secteur
                                                        ))

                                                );

                                            } else {

                                                $args_total = array(
                                                    'post_type'     => 'documents',
                                                    'posts_per_page'   => -1,
                                                    'nopaging' => true,
                                                    'meta_query' => array (
                                                        array (
                                                            'key' => 'pages_associes_document',
                                                            'value' => $id_cats_secteur
                                                        ))

                                                );
                                            }

                                            $per_page=10;
                                            $posts_total = count(get_posts( $args_total ));
                                            $total_page=ceil($posts_total/$per_page);
                                            //$pages=$total_page;

                                            $detail=(get_query_var('paged')) ? get_query_var('paged') : 1;
                                            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                                            if($paged==1) {
                                                $offset_page=0;
                                            } else {
                                                $offset_page=($paged-1)*$per_page;
                                            }

                                            if (isset($_GET["c"])) {
                                                $args = array(
                                                    'post_type'     => 'documents',
                                                    'posts_per_page'   => -1,
                                                    'tax_query' => array(
                                                        array(
                                                            'taxonomy' => 'doc-cat',
                                                            'field'    => 'slug',
                                                            'terms'    => $_GET["c"],
                                                        ),
                                                    ),
                                                    'meta_query' => array (
                                                        array (
                                                            'key' => 'pages_associes_document',
                                                            'value' => $id_cats_secteur
                                                        )),
                                                );
                                            } else {

												$args = array(
    											    'post_type'		=> 'documents',
                                                    'posts_per_page'   => $per_page,
                                                    'offset'           => $offset_page,
    												'meta_query' => array (
														array (
															'key' => 'pages_associes_document',
															'value' => $id_cats_secteur
														)),
												);
                                            }

										    $documents= new WP_Query( $args );
										  	//print_r($documents)	;
										    if ( $documents->have_posts() ) :

										   	 while ( $documents->have_posts() ) : $documents->the_post();

										?>
										<tr>
                                            <?php

                                                $attachment_id = get_field('pdf_documents',get_the_ID());
                                                $filetype_arr=wp_check_filetype($attachment_id["url"]);
                                                $filetype=$filetype_arr["ext"];
                                                if(isset($GLOBALS['doc_type'][$filetype])) {
                                                    $icon_ext=$GLOBALS['doc_type'][$filetype];
                                                } else {
                                                    $icon_ext=$GLOBALS['doc_type']["file"];
                                                }

                                            ?>
											<td class="file-icon">
												<i class="fa <?php echo $icon_ext; ?>"></i>
											</td>
											<td>
												<div class="col-md-8">
												<?php
    												$attachment_id = get_field('pdf_documents',get_the_ID());
    												$url="";
    												if(isset($attachment_id["url"])) {
    												    $url = $attachment_id["url"];
    												}
												?>
													<a href='<?php echo $url?>'><?php the_title();?></a>
												</div>

												<div class="col-md-4">
												<?php
                                                        $current_time=time();
                                                        $post_time= get_the_time('U', get_the_ID());
                                                        $ecart=($current_time-$post_time)/(2 * 60 *60);
                                                        if($ecart>60)
                                                        {
                                                            update_field('tag', array(), get_the_ID());

                                                        }
                                                        $tag_arr=get_field('tag',get_the_ID());
														foreach($tag_arr as $tag)
                                                        {
														if($tag ==3)
														{
												?>
															<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												<?php 	}
                                                        if($tag ==2)
														{
												?>
															<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Modifié</a>
												<?php  } }?>
												</div>

												</div>
											</td>
											<td><?php echo  get_the_date();?></td>
											<td><?php
												$filename = $url;
												$headers  = get_headers($filename, 1);
												$fsize    = $headers['Content-Length'];
												if($fsize<1024)
													{
														echo round($fsize/1024)."octets";
													}elseif($fsize>=1024)
														{
															echo round($fsize/1024)."K";
														}elseif($fsize>=1048576)
															{
																echo round($fsize/1048576)."M";
															}


												?>
											</td>
										</tr>
										<?php
											endwhile;
											endif;
											wp_reset_postdata();

										?>
									</tbody>
								</table>
                            <?php

                            if (!isset($_GET["c"])) {
                                $args_pagination = array(
                                    'base' => @add_query_arg('paged','%#%'),
                                    'format'       => '?paged=%#%',
                                    'total'        => $total_page,
                                    'current'      => $paged,
                                    'show_all'     => False,
                                    'end_size'     => 1,
                                    'mid_size'     => 2,
                                    'prev_next'    => True,
                                    'prev_text'    => __('« Previous'),
                                    'next_text'    => __('Next »'),
                                    'type'         => 'plain',
                                    'add_args'     => False,
                                    'add_fragment' => '',
                                    'before_page_number' => '',
                                    'after_page_number' => ''
                                );
                                echo paginate_links( $args_pagination );
                            }


                            ?>

                        </div>
						</div>
					</div>
				</div><!-- .entry-content -->
			</div>
		</div><!-- #post-## -->
	</div>
</div>
<?php
get_footer();
