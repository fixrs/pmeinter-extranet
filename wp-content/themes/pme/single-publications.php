<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Twelve already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
   <?php  while ( have_posts() ) : the_post(); ?>
		
		<div class="publications">
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title"><?php echo post_type_archive_title('', false); ?></h3>
					<?php echo get_breadcrumb(); ?>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
            <?php

                    $pdf_path="";
                    $image_path="";
                    $image_title="";
                    $id_publication=get_the_ID();
                    $pdf_obj=get_field("pdf",$id_publication);
                    $image_objet=get_field("couverture",$id_publication);
                    $image_path=$image_objet["url"];
                    $image_title=$image_objet["alt"]

            ?>
			<div class="row">
				<div class="col-md-6 col-sm-9">
					<a href="<?php  echo $pdf_obj ?>" target="_blank"><img width="500" height="800" src="<?php echo $image_path?>" alt="<?php echo $image_title;  ?>"></a>
				</div>
				<div class="col-md-6 col-sm-3">
					<?php get_sidebar("publications"); ?>	
				</div>
			</div>			
		</div>
	
	</div>	
</div><!-- #content -->
<?php endwhile;?>
<?php get_footer(); ?>