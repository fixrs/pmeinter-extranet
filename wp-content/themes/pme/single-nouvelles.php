<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

	<div class="row">
		<div class="col-md-12">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php

					$new_title = get_the_title($post->ID);
					$new_content = "";

					$title_parts = preg_split("/([\.\!\?\…])/", $new_title, 0, PREG_SPLIT_DELIM_CAPTURE);

					if (count($title_parts) > 1) {

						$new_title = $title_parts[0] . $title_parts[1];
						array_shift($title_parts);
						array_shift($title_parts);

						$new_content =  "<p>" . implode($title_parts) . "</p>";

						$new_content = preg_replace("/(http:\/\/[^\s]+)/is", "<a href=\"$1\" target=\"_blank\">$1</a>", $new_content);
					}

				?>

				<div class="row">
					<div class="col-md-12">

						<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && twentyfourteen_categorized_blog() ) : ?>
						<div class="entry-meta">
							<span class="cat-links"><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ); ?></span>
						</div>
						<?php endif; ?>

						<?php if ( is_single() ) : ?>
							<!-- BEGIN PAGE TITLE & BREADCRUMB-->
							<!--h3 class="page-title"><?php echo $new_title; ?></h3-->
							<?php ob_start(); get_breadcrumb(); $new_breadcrumbs = preg_replace("/<li>[^<]*?<\/li>[^<]*?<\/ul>/is", "<li>" . $new_title . "</li></ul>", ob_get_contents()); ob_end_clean(); echo $new_breadcrumbs; ?>
							<!-- END PAGE TITLE & BREADCRUMB-->
						<?php else : ?>
							<!-- BEGIN PAGE TITLE & BREADCRUMB-->
							<h3 class="page-title"><a href="<?php echo esc_url(get_permalink()); ?>"><?php echo $new_title; ?></a></h3>
							<!-- END PAGE TITLE & BREADCRUMB-->
						<?php endif; ?>

					</div>
				</div>

				<div class="row">
					<div id="post-<?php the_ID(); ?>" <?php post_class("col-md-9 blog-tag-data"); ?>>
					<h4><?php echo $new_title; ?></h4>
						<?php the_post_thumbnail('full', array(
							'class'	=> "attachment-$size img-responsive"
						)); ?>


						<div class="row nouvelles">
							<div class="col-md-6">
								<!--ul class="list-inline blog-tags">
									<li>
										<i class="fa fa-tags"></i>
										<a href="#">
											 Technology
										</a>
										<a href="#">
											 Education
										</a>
										<a href="#">
											 Internet
										</a>
									</li>
								</ul-->
							</div>
							<div class="col-md-6 blog-tag-data-inner">
								<ul class="list-inline">
									<li>
										<i class="fa fa-calendar"></i>
										<a href="#">
											<?php echo get_the_date("d F Y"); ?>
										</a>
									</li>
									<li>
										<i class="fa fa-comments"></i>
										<a href="#">
											<?php echo get_comments_number(); ?> commentaire(s)
										</a>
									</li>
								</ul>
							</div>
						</div>



					<div class="entry-meta">
						<?php
							if ( 'post' == get_post_type() )
								twentyfourteen_posted_on();

							if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
						?>

						<?php
							endif;

							edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
						?>
					</div><!-- .entry-meta -->




				<?php

					//the_content();
					// $content_post = get_post($post->ID);
					// $content = $content_post->post_content;
					// $content = preg_replace("/(https?\S*?fbcdn\.net\S+?)(Bastien,)? /is", "<img src=\"$1\" alt=\"\" /> $2", $content . " ");
					// $content = apply_filters('the_content', $content);
					// echo $content;



					$content_post = get_post($post->ID);
					$content = $content_post->post_content;
					$content = preg_replace("/(https?\S*?fbcdn\.net\S+?)(Bastien,)? /is", "<img src=\"$1\" alt=\"\" /> $2", $content . " ");
					$content = apply_filters('the_content', $content);

					echo $new_content . " " . $content;

					the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' );

					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}

					endwhile;
				?>
			</div>
			<?php get_sidebar("nouvelles"); ?>
		</div>
	</div>
</div>

<?php
get_footer();
