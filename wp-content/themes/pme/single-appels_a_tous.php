<?php
/**
 * The Template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
	
	<div class="row">
		<div class="col-md-12">
			
			<?php while ( have_posts() ) : the_post(); ?>

				<div class="row">
					<div class="col-md-12">

						<?php if ( in_array( 'category', get_object_taxonomies( get_post_type() ) ) && twentyfourteen_categorized_blog() ) : ?>
						<div class="entry-meta">
							<span class="cat-links"><?php echo get_the_category_list( _x( ', ', 'Used between list items, there is a space after the comma.', 'twentyfourteen' ) ); ?></span>
						</div>
						<?php endif; ?>

						<?php if ( is_single() ) : ?>
							<!-- BEGIN PAGE TITLE & BREADCRUMB-->
							<h3 class="page-title"><?php echo get_the_title(); ?></h3>
							<?php echo get_breadcrumb(); ?>
							<!-- END PAGE TITLE & BREADCRUMB-->
						<?php else : ?>
							<!-- BEGIN PAGE TITLE & BREADCRUMB-->
							<h3 class="page-title"><a href="<?php echo esc_url(get_permalink()); ?>"><?php echo get_the_title(); ?></a></h3>
							<!-- END PAGE TITLE & BREADCRUMB-->
						<?php endif; ?>

					</div>
				</div>	
							
				<div class="row">
					<div id="post-<?php the_ID(); ?>" <?php post_class("col-md-11 blog-tag-data"); ?>>
						
						<?php the_post_thumbnail('full', array(
							'class'	=> "attachment-$size img-responsive"
						)); ?>
						

						<div class="row nouvelles">
							<div class="col-md-6">
							<h4>Par <?php echo get_the_author(); ?></h4>
							

							</div>
							<div class="col-md-6 blog-tag-data-inner">
								<ul class="list-inline">
									<li>
										<i class="fa fa-calendar"></i>
										<a href="#">
											<?php echo get_the_date("d F Y"); ?> 
										</a>
									</li>
									<li>
										<i class="fa fa-comments"></i>
										<a href="#">
											<?php echo get_comments_number(); ?> commentaire(s)
										</a>
									</li>
								</ul>
							</div>
						</div>



					<div class="entry-meta">
						<?php
							if ( 'post' == get_post_type() )
								twentyfourteen_posted_on();

							if ( ! post_password_required() && ( comments_open() || get_comments_number() ) ) :
						?>					
						
						<?php
							endif;

							edit_post_link( __( 'Edit', 'twentyfourteen' ), '<span class="edit-link">', '</span>' );
						?>
						<?php 

							the_content();
							the_tags( '<footer class="entry-meta"><span class="tag-links">', '', '</span></footer>' );
						
							// If comments are open or we have at least one comment, load up the comment template.
							if ( comments_open() || get_comments_number() ) {
								comments_template();
							}

							endwhile;
						?>
					</div>
			</div>	
			<?php //get_sidebar("nouvelles"); ?>
		</div>	
	</div>
</div>

<?php 
get_footer();