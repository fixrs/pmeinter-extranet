<?php
$page = "comite-gestion";

$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "corail",
	'fiducies' => 'purple'
);

$color = $colors[$page];

?>
<div class="col-md-3">
	<!-- BEGIN ACCORDION PORTLET-->
	<div class="portlet box <?php echo $color; ?>">
		<div class="portlet-title">
			<div class="caption"><i class="fa fa-book"></i> Archives</div>
		</div>

		<div class="portlet-body">
			<div class="panel-group accordion" id="accordion1">
				<?php
					$posts_by_date = posts_by_date();
					$i = 0;
					foreach ($posts_by_date as $year => $months) :
				?>

				<div class="panel panel-default">
					<div class="panel-heading">
						<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#collapse_<?php echo $i; ?>">
							 <?php echo $year; ?>
						</a></h4>
					</div>
					<div id="collapse_<?php echo $i; ?>" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="portlet-body">
								<div class="panel-group accordion" id="accordion<?php echo $i; ?>1">
									<?php
									$j = 0;
									foreach($months as $month => $posts) :
									?>
									<div class="panel panel-default">
										<div class="panel-heading">
											<h4 class="panel-title"><a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion<?php echo $i; ?>1" href="#collapse_<?php echo $i; ?>_<?php echo $j; ?>">
												 <?php echo ucfirst(strftime("%B", mktime(0, 0, 0, $month, 10))); ?>
											</a></h4>
										</div>
										<div id="collapse_<?php echo $i; ?>_<?php echo $j; ?>" class="panel-collapse collapse">
											<div class="panel-body">
												<ul>
													<?php foreach ($posts as $post) : ?>
														<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
													<?php endforeach; ?>
												</ul>
											</div>
										</div>
									</div>
									<?php $j++; ?>
									<?php endforeach; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php $i++; ?>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
	<!-- END ACCORDION PORTLET-->
</div>
