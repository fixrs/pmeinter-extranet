<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php 
$page = "collaboratrices";

$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "corail",
);

$color = $colors[$page];

?>
<div id="eventContent" title="Event Details" style="display:none;">
    Start: <span id="startTime"></span><br>
    End: <span id="endTime"></span><br><br>
    <p id="eventInfo"></p>
    
</div>

<div class="<?php echo $color; ?> accordeon-general">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">		
					<h3 class="page-title">Droit de la personne</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="/">Intranet</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>Droit de la personne</li>
					</ul>			
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>	
			<div class="row">
				<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-9 blog-tag-data">
					<div class="entry-content ">
						<div class="tabbable-custom nav-justified">
							<ul class="nav nav-tabs nav-justified">
								<li class="active">
									<a href="#tab_1_1_1" data-toggle="tab">Aperçu<br /></a>
								</li>
								<li><a href="#tab_1_1_2" data-toggle="tab">Calendrier<br /></a>
								</li>
								<li><a class="tab_1_1_3" href="#tab_1_1_3" data-toggle="tab">Liste des membres</a>
								</li>
								<li><a class="tab_1_1_4" href="#tab_1_1_4" data-toggle="tab">Comptes rendus</a>

								</li>
								<li><a href="#tab_1_1_5" data-toggle="tab">Documents<br /></a>
								</li>
								<li><a href="#tab_1_1_6" data-toggle="tab">Produits informatiques</a>
								</li>
								<li><a href="#tab_1_1_7" data-toggle="tab">Forum<br /></a>
								</li>
							</ul>						
						</div>
					</div><!-- .entry-content -->
				</div>
			</div><!-- #post-## -->
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">			
			<div class="note note-success">
				<h4 class="block">Mise en garde aux membres du réseau</h4>
				<p>Ce document est le fruit du travail d’analyse et de rédaction des membres du Comité de droit des aﬀaires. L’utilisation de ce document et du questionnaire qui y est relié devrait vous permettre de réaliser une économie substantielle de temps lors de la préparation d’une convention entre actionnaires. En outre, ce document permettra de personnaliser le produit ﬁnal en fonction des besoins de vos clients respectifs.</p>
				<p>Le tarif indicatif minimal recommandé, mais non obligatoire, relativement à un tel produit est d’environ neuf cent dollars. Nous vous recommandons, en outre, de recourir à une tariﬁcation horaire dans le cadre de la réalisation d’un tel mandat.</p>
				<p>
					Enﬁn, nous vous invitons à faire parvenir vos commentaires et suggestions à l’égard de ce produit à
					<a href="mailto:info@pmeinter.com">info@pmeinter.com</a>
				</p>
			</div>						
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">			
		   <div class="container-fluid">
		     <div class="accordion" id="accordion2">
		            <div class="accordion-group">
		              <div class="accordion-heading">
		                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
		                  Procédure
		                </a>
		              </div>
		              <div id="collapseOne" class="accordion-body collapse" style="height: 0px; ">
		                <div class="accordion-inner">
		                  	<table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nom</th>
											<th>Dern. Màj</th>
											<th>Taille</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="icone-pdf">
												<i class="fa fa-file-text"></i>
											</td>
											<td>
												<div class="col-md-8">
													<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
												</div>
												<div class="col-md-4">
													<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												</div>
											</td>
											<td>--</td>
											<td>--</td>
										</tr>
									</tbody>
							</table>
		                </div>
		              </div>
		            </div>
		            <div class="accordion-group">
		              <div class="accordion-heading">
		                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
		                 Documents
		                </a>
		              </div>
		              <div id="collapseTwo" class="accordion-body collapse">
		                <div class="accordion-inner">
		                  <table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nom</th>
											<th>Dern. Màj</th>
											<th>Taille</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="icone-pdf">
												<i class="fa fa-file-text"></i>
											</td>
											<td>
												<div class="col-md-8">
													<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
												</div>
												<div class="col-md-4">
													<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												</div>
											</td>
											<td>--</td>
											<td>--</td>
										</tr>
									</tbody>
							</table>
		                </div>
		              </div>
		            </div>
		            <div class="accordion-group">
		              <div class="accordion-heading">
		                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
		                  Lorem
		                </a>
		              </div>
		              <div id="collapseThree" class="accordion-body collapse">
		                <div class="accordion-inner">
		                  <table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nom</th>
											<th>Dern. Màj</th>
											<th>Taille</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="icone-pdf">
												<i class="fa fa-file-text"></i>
											</td>
											<td>
												<div class="col-md-8">
													<a href='#'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
												</div>
												<div class="col-md-4">
													<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												</div>
											</td>
											<td>--</td>
											<td>--</td>
										</tr>
									</tbody>
							</table>
		                </div>
		              </div>
		            </div>
		          </div>
		    </div>
		</div>	
	</div>
</div>
<?php
get_footer();
