<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Twelve already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

		
	
	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
			<h3 class="page-title"><?php echo post_type_archive_title('', false); ?></h3>
			<?php echo get_breadcrumb(); ?>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>


		<?php if ( have_posts() ) : ?>	
		<div class="row">
			<div class="col-md-12">
				<div class="fournisseurs">
		  			<ul class="listing">
	       
						<?php
						/* Start the Loop */
						$posts = query_posts($query_string . "&orderby=title&order=asc&posts_per_page=-1"); 

						$i = 1;
						while ( have_posts() ) : the_post();

							/* Include the post format-specific template for the content. If you want to
							 * this in a child theme then include a file called called content-___.php
							 * (where ___ is the post format) and that will be used instead.
							 */
							
								
								
							$logo = get_field('logo', get_the_id(), true); 
							$lien = get_field( 'f_lien', get_the_id(), true );
							$class = ($i%4) == 0 ? 'class="last"' : '';
							?>
							<li <?php echo $class; ?>>
								<a href="<?php echo $lien; ?>" target="_blank">
									<img src="<?php echo $logo; ?>" alt="<?php the_title(); ?>" />
								</a>
								<footer class="entry-meta">
									<?php edit_post_link( __( 'Edit', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?>
								</footer><!-- .entry-meta -->
							</li>

						<?php
						$i++;
						endwhile;
						?>
					</ul>
					<div class="clear"></div>

					<?php else : ?>
						<?php get_template_part( 'content', 'none' ); ?>
					<?php endif; ?>
				</div>
			</div>	
		</div>	
	</div>	
</div><!-- #content -->

<?php get_footer(); ?>