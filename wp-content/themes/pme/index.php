<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>
<?php 
$page = "droit-agricol";

$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "yellow",
);
$color = $colors[$page];
?>
<div id="main-content" class="main-content">
<?php
	if ( is_front_page() && twentyfourteen_has_featured_posts() ) {
		// Include the featured content template.
		get_template_part( 'featured-content' );
	}	
?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
			<?php
			$args = array(
					'p' => 334, // id of a page, post, or custom type
					'post_type' => 'page');
			$my_posts = new WP_Query($args);
				// Start the Loop.
				while ( $my_posts->have_posts() ) : $my_posts->the_post();

					// Include the page content template.
					get_template_part( 'content', 'accueil' );

					// If comments are open or we have at least one comment, load up the comment template.
					/*
					if ( comments_open() || get_comments_number() ) {
						comments_template();
					}
					*/
				endwhile;
				wp_reset_postdata();
			?>

		</div><!-- #content -->
	</div><!-- #primary -->
	<?php get_sidebar( 'content' ); ?>
</div><!-- #main-content -->
<div class="spacer">&nbsp;</div>
<?php
get_sidebar();
get_footer();
