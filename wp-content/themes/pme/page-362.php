<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>

<?php 
$page = "collaboratrices";

$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "corail",
);
$color = $colors[$page];
?>
<div class="row <?php echo $color; ?>">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">Droit de la personne</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="/">Intranet</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>Droit de la personne</li>
				</ul>			
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>	
		<div class="row">
			<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-9 blog-tag-data">
				<div class="entry-content ">
					<div class="tabbable-custom nav-justified">
						<ul class="nav nav-tabs nav-justified">
							<li class="active">
								<a href="#tab_1_1_1" data-toggle="tab">Aperçu<br /></a>
							</li>
							<li><a href="#tab_1_1_2" data-toggle="tab">Calendrier<br /></a>
							</li>
							<li><a class="tab_1_1_3" href="#tab_1_1_3" data-toggle="tab">Liste des membres</a>
							</li>
							<li><a class="tab_1_1_4" href="#tab_1_1_4" data-toggle="tab">Procès verbaux</a>
							</li>
							<li><a href="#tab_1_1_5" data-toggle="tab">Documents<br /></a>
							</li>
							<li><a href="#tab_1_1_6" data-toggle="tab">Produits informatiques</a>
							</li>
							<li><a href="#tab_1_1_7" data-toggle="tab">Forum<br /></a>
							</li>
							<li><a href="#tab_1_1_8" data-toggle="tab">Boite à Outils<br /></a>
							</li>
						</ul>
						  
							<div id="tab_1_1_1" class="tab-pane active">
								<div class="row">
									<div class="col-md-6">
										<div class="portlet box <?php echo $color; ?> calendar">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-calendar"></i>Calendrier
												</div>
											</div>
											<div class="portlet-body light"><div id="calendar"></div></div>
										</div>
										<div class="portlet box <?php echo $color; ?>">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-cogs"></i>Documents récents
												</div>
											</div>
											<div class="portlet-body light">
												<table class="table">
													<thead>
														<tr>
															<th class="intitule">Titre</th>
															<th>Format</th>
															<th>Taille</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td class="intitule">Résolution du conseil d'administration et des actionnaires, le cas échéant, pour la vente d'actifs d'entreprise 2013</td>
															<td>DOC</td>
															<td>28 Ko </td>
														</tr>
														<tr>
															<td class="intitule">Business checklist</td>
															<td>DOC</td>
															<td>45 Ko </td>
														</tr>
													</tbody>
												</table>
												<a href="javascript:;">Tous les documents</a>
											</div>
										</div>
										<div class="portlet box <?php echo $color; ?>">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-cogs"></i>Produits informatiques
												</div>
											</div>
											<div class="portlet-body light">
												<table class="table">
													<thead>
														<tr>
															<th class="intitule">Titre</th>
															<th>Format</th>
															<th>Taille</th>
														</tr>
													</thead>
													<tbody>
														<tr>
															<td class="intitule">Close de vente</td>
															<td>PDF</td>
															<td>28 Ko </td>
														</tr>
														<tr>
															<td class="intitule">Vente de terrain</td>
															<td>TIFF</td>
															<td>50,2 Ko </td>
														</tr>
													</tbody>
												</table>
												<a href="javascript:;">Tous les produits informatiques</a>
											</div>
											
										</div>
									</div>
									<div class="col-md-6">
										<div class="portlet box <?php echo $color; ?>">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-cogs"></i>Forum Droit de la personne
												</div>
											</div>
										
										</div>
										<div class="row <?php echo $color; ?>">
											<div class="col-md-6 comptesRendus">
												<a class="compte_rendu"><i class="fa fa-file-text"></i></br>Comptes rendus</a>
												
											</div>
											<div class="col-md-6 ListeMembres">
												<a class="liste_membre"><i class="fa fa-user"></i></br>Liste des membres</a>

											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="tab_1_1_2" class="tab-pane">
								<div class="row">
									<div class="col-md-12">
										<div class="portlet box blue calendar">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-reorder"></i>Calendar
												</div>
											</div>
											<div class="portlet-body light-grey">
												<div class="row">
													<div class="col-md-3 col-sm-12">
														<!-- BEGIN DRAGGABLE EVENTS PORTLET-->														
														<div id="external-events">
															<form class="inline-form">
																<h3>Catégories</h3>
																<span>																	
																	<input type="checkbox" name="vehicle" value="Bike">I have a bike
																	<label></label>
																<input type="checkbox" name="vehicle" value="Car">I have a car 
															</form>
															<hr/>
															<div id="event_box">
															</div>
															<label for="drop-remove">
															<input type="checkbox" id="drop-remove"/>remove after drop </label>
															<hr class="visible-xs"/>
														</div>
														<!-- END DRAGGABLE EVENTS PORTLET-->
													</div>
													<div class="col-md-9 col-sm-12">
														<div id="calendar" class="has-toolbar"></div>
													</div>
												</div>
												<!-- END CALENDAR PORTLET-->
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="tab_1_1_3" class="tab-pane">Nom du responsable : 
								<strong>Me Karine Jobin</strong>		
								<?php echo getMembersList(); ?>						
							</div>
							<div id="tab_1_1_4" class="tab-pane">
								<div class="col-md-9">
									<h1>PROCÈS-VERBAL DE LA RÉUNION DU COMITÉ DU Droit de la personne TENUE À DRUMMONDVILLE </h1>
									<p><strong>LE 21 FÉVRIER 2014</strong></p>
									<h3>LISTE DES PERSONNES PRÉSENTES :</h3>
									<div class="content">
										<div class="col-md-3">
											Karine Jobin (Sherbrooke)<br>
											Christophe Larocque (Gatineau)<br>
											Éric Tremblay (Lac Etchemin)<br>
											Jacinthe Breton (Ste-Marie)<br>
											Josée Girard (Saguenay)<br>
											Karine Francoeur (Sherbrooke/Fleurimont)<br>
										</div>
										<div class="col-md-3">
											Annie Durocher (Drummondville)<br>
											Claude Rodrigue (St-Georges)<br>
											François Sylvestre (Sherbrooke)<br>
											Jean-Pierre Aubry (Shawinigan)<br>
											Julie Hébert (Montréal/Anjou)<br>
											Mathieu Rainville (Sherbrooke/Fleurimont)<br>
										</div>
									</div>
									<p>&nbsp;</p>
									<p>La réunion est présidée par Me Karine Jobin, présidente du comité</p>
									<h3>1.  NOMINATION DU SECRÉTAIRE</h3>
									<p>Me Jean-Pierre Aubry est nommé à ce poste</p>
									<h3>2.  LECTURE ET ADOPTION DE L'ORDRE DU JOUR</h3>
									<p>L'ordre du jour est adopté à l'unanimité</p>
									<h3>3.  LECTURE ET ADOPTION DU PROCÈS-VERBAL DE LA RÉUNION DU 8 NOVEMBRE 2013</h3>
									<p>Le procès-verbal est adopté à l'unanimité.  Merci à Julie Hébert pour la rédaction du procès-verbal.</p>
								</div>
								<?php get_sidebar( 'direction' ); ?>

							</div>
							<div id="tab_1_1_5" class="tab-pane">
								<div class="note note-success">
									<h4 class="block">Mise en garde aux membres du réseau</h4>
										<p>Ce document est le fruit du travail d&rsquo;analyse et de rédaction des membres du Comité de droit des aﬀaires. L&rsquo;utilisation de ce document et du questionnaire qui y est relié devrait vous permettre de réaliser une économie substantielle de temps lors de la préparation d&rsquo;une convention entre actionnaires. En outre, ce document permettra de personnaliser le produit ﬁnal en fonction des besoins de vos clients respectifs.</p>
										<p>Le tarif indicatif minimal recommandé, mais non obligatoire, relativement à un tel produit est d&rsquo;environ neuf cent dollars. Nous vous recommandons, en outre, de recourir à une tariﬁcation horaire dans le cadre de la réalisation d&rsquo;un tel mandat.</p>
										<p>Enﬁn, nous vous invitons à faire parvenir vos commentaires et suggestions à l&rsquo;égard de ce produit à <a href="mailto:info@pmeinter.com">info@pmeinter.com</a></p>
								</div>
								<table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nom</th>
											<th>Dern. Màj</th>
											<th>Taille</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="icone-pdf">
												<i class="fa fa-file-text"></i>
											</td>
											<td>
												<div class="col-md-8">
													<a href='http://pmex.leeroy.ca/?wpdmact=process&did=Mi5ob3RsaW5r'>Business checklist</a>
												</div>
												<div class="col-md-4">
													<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												</div>
											</td>
											<td>--</td>
											<td>--</td>
										</tr>
										<tr>
											<td class="icone-pdf">
												<i class="fa fa-file-text"></i>
											</td>
											<td>
												<div class="col-md-8">
													<a href='http://pmex.leeroy.ca/?wpdmact=process&did=MS5ob3RsaW5r'>Résolution du conseil d'administration et des actionnaires, le cas échéant, pour la vente d'actifs d'entreprise 2013</a></div><div class="col-md-4"><a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												</div>
											</td>
											<td>--</td>
											<td>--</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div id="tab_1_1_6" class="tab-pane">
								<div class="note note-success">
									<p>Il est à noter que vous ne retrouverez ici que les nouvelles versions de la barre d&rsquo;outils, les macros, documents&#8230;. testées par FIL Informatique.</p>
									<p>Pour les anciennes versions, si vous devez récupérer des documents, vous devrez communiquer avec FIL Informatique par téléphone (450.530.7373) ou par courriel (<a href="mailto:mp@ﬁlinformatique.com">mp@ﬁlinformatique.com</a>).</p>
									<p><strong>Mise en garde : pour toute assistance concernant l&rsquo;installation et le soutien informatique reliés aux macros et aux assistants de PME INTER Notaires, il est impératif de communiquer avec l&rsquo;équipe de FIL Informatique.</strong></p>
									<p>Pour télécharger un ﬁchier, cliquez sur le lien avec le bouton de droite de la souris et choisissez « Enregistrer la cible sous ».</p>
								</div>
								<table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nom</th>
											<th>Dern. Màj</th>
											<th>Taille</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td class="icone-pdf">
												<i class="fa fa-file-text"></i>
											</td>
											<td>
												<div class="col-md-8">
													<a href='/mira/droit-de-la-personne/produits-informatiques/'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
												</div>
												<div class="col-md-4">
													<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												</div>
											</td>
											<td>--</td>
											<td>--</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div id="tab_1_1_7" class="tab-pane">
								<p>Forum</p>
							</div>							
							<div id="tab_1_1_8" class="tab-pane sousmenu-nav">
								<ul class="nav nav-tabs nav-justified">
									<li class="active">
										<a href="#tab_2_1_1" data-toggle="tab">Droit</br> de la personne</a>
									</li>
									<li>
										<a href="#tab_2_1_2" data-toggle="tab">Droit<br /> des affaires</a>
									</li>
									<li>
										<a href="#tab_2_1_3" data-toggle="tab">Droit<br /> agricole</a>
									</li>
									<li>
										<a href="#tab_2_1_4" data-toggle="tab">Droit<br /> immobilier</a>										
									</li>
									<li>
										<a href="#tab_2_1_5" data-toggle="tab">Gestion<br /> des bureaux</a>
									</li>
									<li>
										<a href="#tab_2_1_6" data-toggle="tab">Développement <br />des affaires</a>
									</li>
									
								</ul>
								<div id="tab_2_1_1" class="soustab tab-pane">									
									<div class="col-md-6">
										<h4>Documents</h4>
										<ul>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>											
										</ul>
									</div>
									<div class="col-md-6">
										<h4>Liens utiles</h4>
										<ul>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>											
										</ul>
									</div>
								</div>
								<div id="tab_2_1_2" class="soustab tab-pane">
									<div class="col-md-6">
										<h4>Documents</h4>
										<ul>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>											
										</ul>
									</div>
									<div class="col-md-6">
										<h4>Liens utiles</h4>
										<ul>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>											
										</ul>
									</div>
								</div>
								<div id="tab_2_1_3" class="soustab tab-pane">
									<div class="col-md-6">
										<h4>Documents</h4>
										<ul>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>
											<li><a href="#" ><i class="fa fa-file"></i>document</a></li>											
										</ul>
									</div>
									<div class="col-md-6">
										<h4>Liens utiles</h4>
										<ul>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>
											<li><a href="#" ><i class="fa fa-link"></i>lien</a></li>											
										</ul>
									</div>
								</div>
							</div>
						
						</div>
					</div>
				</div><!-- .entry-content -->
			</div>
		</div><!-- #post-## -->
	</div>
</div>
<?php
get_footer();
