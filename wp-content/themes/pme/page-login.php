<?php
/**
 * The template for displaying all login pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package ii
 */

if (is_user_logged_in()) {

    if (isset($_GET["redirect_to"])) {
        wp_redirect($_GET["redirect_to"]);
    } else {
        wp_redirect("/extranet/");
    }
}

get_header(); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main login" role="main">
            <div class="container">
                <header class="entry-header">
                    <h1 class="entry-title">Connexion</h1>
                </header><!-- .entry-header -->
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                        <h3>Extranet - Réseau PME Inter Notaires</h3>
                    </div>
                    <div class="col-sm-12 col-xs-12">
                        <?php
                            $login  = (isset($_GET['login']) ) ? $_GET['login'] : 0;

                            if ( $login === "failed" ) {
                                echo '<p class="login-msg"><strong>ERREUR :</strong> Nom d\'utilisateur ou mot de passe invalide.</p>';
                            } elseif ( $login === "empty" ) {
                                echo '<p class="login-msg"><strong>ERREUR :</strong> Nom d\'utilisateur ou mot de passe vide.</p>';
                            } elseif ( $login === "false" ) {
                                echo '<p class="login-msg"><strong>Vous êtes déconnecté.</strong></p>';
                            }

                            if (isset($_GET["redirect_to"])) {
                                ob_start();
                                wp_login_form();
                                $form = ob_get_contents();
                                ob_get_clean();

                                echo preg_replace("/(wp-login.php)/is", "$1?redirect_to=" . $_GET["redirect_to"], $form);

                            }  else {
                                wp_login_form();
                            }



                        ?>
                    </div>
                </div>
            </div>
        </main><!-- #main -->
    </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
