<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 * Template Name: Section comptes rendu template Single
 */

get_header(); ?>

<?php
$id_section=get_the_ID();
$id_cats_secteur=get_field("pages_associes",$id_section);
$parentid=wp_get_post_parent_id( $id_section );


// $args = $args = array(
//     'post_parent' => 71,
//     'post_type'   => 'page',
//     'posts_per_page' => -1,
//     'post_status' => 'publish' );

switch (get_the_ID()) {
    case 214: $id_cats_secteur = 2; break;
    case 206: $id_cats_secteur = 1; break;
    case 229: $id_cats_secteur = 5; break;
    case 425: $id_cats_secteur = 4; break;
    case 224: $id_cats_secteur = 3; break;
    case 231: $id_cats_secteur = 6; break;
    case 233; $id_cats_secteur = 7; break;
    case 5314; $id_cats_secteur = 8; break;
}

//$comptesrendu = get_children( $args );

// if($comptesrendu)
// {
//     foreach($comptesrendu as $cr)
//     {
//         if (get_field("key", $cr->ID) == $id_cats_secteur)
//         {
//             $parentid =$cr->ID;
//             break;
//         }


//     }

// }
$id_cats_secteur=get_field("pages_associes",$parentid);
$color=get_field('color_page',$parentid);



?>
    <script type="text/javascript">

        function PrintElem(elem)
        {
            Popup($(elem).html());
        }

        function Popup(data)
        {
            var mywindow = window.open('', 'my div', 'height=1024,width=800');
            mywindow.document.write('<html><head><title>my div</title>');
            /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.print();
            mywindow.close();

            return true;
        }

    </script>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">Procès verbaux</h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="<?php get_home_url(); ?>">Intranet</a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <i class="fa"></i>
                        <a href="<? echo get_permalink($parentid); ?>"><?php echo get_the_title($parentid);?></a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li><?php the_title();?></li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div class="row">
            <div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-12 blog-tag-data comptesRendusSection">
                <div class="entry-content">
                    <div class="tabbable-custom nav-justified">
                        <?php
                            $force_swithcer=$parentid;
                            include_once 'global_nav.php';
                        ?>

                        <div class="tab-content">


                            <div id="tab_1_1_4" style="display: block;" class="tab-pane">

                                <div class="col-md-9">
                                    <i class="fa fa-print"></i><input type="button" value="Imprimer" onclick="PrintElem('#mydiv')" />

                                    <?php

                                        $args = array(
                                            'post_type'     => 'comptesrendu',
                                            'offset'=>0,
                                            'posts_per_page'    => 1,
                                            'meta_query' => array (
                                                    array (
                                                            'key' => 'pages_associes',
                                                            'value' => $id_cats_secteur
                                                          )),

                                            'meta_key'      => 'date',
                                            'orderby'       => 'date',
                                            'order'         => 'DESC'
                                            );
                                            $comptes_rendu = new WP_Query( $args );

                                        while ( $comptes_rendu->have_posts() ) : $comptes_rendu->the_post(); ?>

                                           <h1><?php echo get_the_title(); ?></h1>
                                            <p><strong>
                                                   <?php
                                                    setlocale (LC_ALL, "fr_CA.UTF-8");
                                                    //echo strftime("%A %e %B %Y",get_field("date",get_the_ID()));

                                                    ECHO strftime("%A %d %B %Y",strtotime(get_field("date",get_the_ID())));
                                                    ?>
                                                </strong></p>
                                            <h3>LISTE DES PERSONNES PRÉSENTES :</h3>

                                            <?php
                                            echo get_the_content();

                                        endwhile; ?>
                                    <!-- <div class="col-md-3">Archives</div>-->


                                </div>

                                <?php get_sidebar("crdetail");
                                //echo "test";
                                //print_r(comptes_rendu_archive($id_cats_secteur));
                                ?>
                            </div>
                        </div>
                    </div><!-- .entry-content -->
                </div>
            </div><!-- #post-## -->
        </div>
    </div>
    <?php
    get_footer();

