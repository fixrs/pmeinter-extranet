<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 * Template Name: Section comptes rendu template
 */

get_header(); ?>

<?php

/*
$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "yellow",
);
$color = $colors[$page];
*/

//$color =$get_color;
$id_section=get_the_ID();
$parentid=wp_get_post_parent_id( $id_section );
$id_cats_secteur=get_field("key",$parentid);
$color=get_field('color_page',$parentid);
?>
    <script type="text/javascript">

        function PrintElem(elem)
        {
            Popup($(elem).html());
        }

        function Popup(data)
        {
            var mywindow = window.open('', 'my div', 'height=1024,width=800');
            mywindow.document.write('<html><head><title>my div</title>');
            /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
            mywindow.document.write('</head><body >');
            mywindow.document.write(data);
            mywindow.document.write('</body></html>');

            mywindow.print();
            mywindow.close();

            return true;
        }

    </script>

<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title"><?php the_title();?></h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php get_home_url(); ?>">Intranet</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa"></i>
						<a href="<?php echo get_permalink($parentid); ?>"><?php echo get_the_title($parentid);?></a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><?php the_title();?></li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<div class="row">
			<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-12 blog-tag-data comptesRendusSection">
				<div class="entry-content">
					<div class="tabbable-custom nav-justified">
						<div class="mobile-dd">
							<?php include_once 'global_nav.php';?>
							<div class="mobile-handle">
								<a class="fa fa-angle-down">
								</a>
							</div>
						</div>
						<div class="tab-content">


								<div id="tab_1_1_4" style="display: block;" class="tab-pane">

								<div class="col-md-9" id="mydiv">
                                    <!--i class="fa fa-print"></i><input class="btnPrint" type="button" value="Imprimer" onclick="PrintElem('#mydiv')" />-->
							<?php

							if (isset($_GET["ad"])) {

								$date_parts = explode("-", $_GET["ad"]);


							    $args = array(
								    'post_type'		=> 'comptesrendu',
									'posts_per_page'	=> -1,
									'meta_query' => array (
											array (
													'key' => 'pages_associes',
													'value' => $id_cats_secteur
											),
											array(
								                'key'  => 'date',
								                'value' => preg_replace("/-[0-9]+$/", "", $_GET["ad"]),
								                'compare' => 'LIKE'
								            )
										),

									'meta_key'		=> 'date',
									'orderby'		=> 'date',
									'order'			=> 'DESC'
								);

							} else {

								$args = array(
								    'post_type'		=> 'comptesrendu',
									'offset'=>0,
									'posts_per_page'	=> -1,
									'meta_query' => array (
											array (
													'key' => 'pages_associes',
													'value' => $id_cats_secteur
											      ),
											array(
								                'key'  => 'date',
								                'value' => date("Y"),
								                'compare' => 'LIKE'
								            )
										),

									'meta_key'		=> 'date',
									'orderby'		=> 'date',
									'order'			=> 'DESC'
								);

							}

						    $comptes_rendu = new WP_Query( $args );
							//print_r($comptes_rendu);

						    if($comptes_rendu->have_posts()) :
							      while($comptes_rendu->have_posts()) :
							         $comptes_rendu->the_post();
							?>

									<h1><?php echo get_the_title(); ?></h1>
									<p><strong>
									<?php ?>

									<?php
									setlocale (LC_ALL, "fr_CA.UTF-8");
									//echo strftime("%A %e %B %Y",get_field("date",get_the_ID()));

									ECHO strftime("%A %d %B %Y",strtotime(get_field("date",get_the_ID())));
									?>
									</strong></p>
									<!--h3>LISTE DES PERSONNES PRÉSENTES :</h3-->

							<?php
									$content = get_the_content();

									$content = preg_replace("/<a[^>]*?href=\"http:([^\"]*?\.pdf)\"[^>]*?>(.*?)<\/a>/is", "<h3>$2</h3><iframe style=\"width: 100%; height: 600px\" src=\"$1\"></iframe>", $content);

									echo $content;

									endwhile;endif; wp_reset_postdata(); ?>
								<!-- <div class="col-md-3">Archives</div>-->


							</div>

						<?php get_sidebar("comptesrendu");
									//echo "test";
									//print_r(comptes_rendu_archive($id_cats_secteur));
								?>
						</div>
					</div>
				</div><!-- .entry-content -->
			</div>
		</div><!-- #post-## -->
	</div>
</div>
<?php
get_footer();
