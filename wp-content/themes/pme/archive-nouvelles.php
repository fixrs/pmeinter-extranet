<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Twelve already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header();


if (isset($_GET["ad"])) {

	$date_parts = explode("-", $_GET["ad"]);

    $posts = get_posts(
    	array(
	        'orderby' => 'post_date',
	        'order' => 'DESC',
	        'post_type' => 'nouvelles',
	        'post_status' => 'publish',
	        'date_query' => array(
	            array(
	                'year'  => $date_parts[0],
	                'month' => $date_parts[1]
	            )
	        )
	   	));

} else {

	$posts =  query_posts(
		array(
			'posts_per_page' => 40,
	        'orderby' => 'post_date',
	        'order' => 'DESC',
	        'post_type' => 'nouvelles',
	        'post_status' => 'publish'
	   	)
	);

}



?>

	<div class="row">
		<div class="col-md-12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
			<h3 class="page-title"><?php echo post_type_archive_title('', false); if (isset($_GET["ad"])) { echo " (" . $_GET["ad"] . ")"; }  ?></h3>
			<?php
				 if (isset($_GET["ad"])) {
                    ob_start();
                    get_breadcrumb();
                    $breadcrumb = ob_get_contents();
                    ob_end_clean();

                    echo preg_replace("/<li>\s*Nouvelles\s*<\/li>/is", "<li><a href=\"/extranet/nouvelles/\">Nouvelles</a> <i class=\"fa fa-angle-right\"></i></li><li>" . $_GET["ad"] . "</li>", $breadcrumb);
                } else {
					get_breadcrumb();
				}

			?>
			<!-- END PAGE TITLE & BREADCRUMB-->
		</div>
	</div>

	<div class="row">
		<div class="col-md-9 blog-tag-data">
			<?php
				/* Start the Loop */

				$titles = array();
				$i = 1;
				$class = "even";
				foreach ($posts as $post) {

					//if (preg_match("/-[0-9]+\/?$/", get_the_permalink($post->ID))) { continue; }
					$new_title = str_replace('’', '\'', get_the_title($post->ID));
					if (isset($titles[$new_title])) { continue; }
					$titles[$new_title] = 1;

					if ($class == "even") { $class = "odd"; } else { $class = "even"; }
			?>

			<div class="row <?php echo $class; ?> blog-page <?php if (has_post_thumbnail($post->ID)) { echo "has_thumbnail"; } ?>">

				<?php if (has_post_thumbnail($post->ID)) { ?>
					<div class="col-md-12 blog-tag-data">
						<?php echo get_the_post_thumbnail($post->ID, 'post-thumbnail', array('class'	=> "attachment-$size img-responsive" )); ?>
					</div>
				<?php } ?>
				<div class="col-md-12">
					<ul class="list-block">
						<li>
							<i class="fa fa-calendar"></i>
							<a href="#"><?php echo get_the_date(null, $post->ID); ?></a>
						</li>
						<li>
							<i class="fa fa-comments"></i>
							<a href="<?php echo get_permalink($post->ID) . "#respond"; ?>">
								<?php
									if (get_comments_number() < 2) {
										echo get_comments_number() . " Commentaire";
									} else {
										echo get_comments_number() . " Commentaires";
									}
								?>
							</a>
						</li>
					</ul>
				</div>

				<?php

					$new_content = "";

					$title_parts = preg_split("/([\.\!\?\…])/", $new_title, 0, PREG_SPLIT_DELIM_CAPTURE);

					if (count($title_parts) > 1) {

						$new_title = $title_parts[0] . $title_parts[1];
						array_shift($title_parts);
						array_shift($title_parts);

						$new_content =  "<p>" . implode($title_parts) . " </p>";

						$new_content = preg_replace("/([^\"])(http:\/\/[^\s]+)/is", "$1<a href=\"$2\" target=\"_blank\">$2</a>", $new_content);
						$new_content = preg_replace("/>([^\s]{75})([^\s]{75})?([^\s]{75})?([^\s]*)</is", ">$1 $2 $3 $4<", $new_content);

						// $new_content = preg_replace("/<a href=\"<a href=\"/is", "<a href=\"", $new_content);
						// $new_content = preg_replace("/\" target=.*?_blank.*? >.*?<\/a>([^<]+?<\/a>)/is", "$1",  $new_content);

					}


					$content_post = get_post($post->ID);
					$content = $content_post->post_content . " ";

					$content = preg_replace("/(https?\S*?fbcdn\.net\S+?)(Bastien,)? /is", "<img src=\"$1\" alt=\"\" /> $2", $content . " ");

					$content = preg_replace("/([^\"])(http:\/\/[^\s]+)/is", "$1<a href=\"$2\" target=\"_blank\">$2</a>", $content);

					$content = preg_replace("/>([^\s]{75})([^\s]{75})?([^\s]{75})?([^\s]*)</is", ">$1 $2 $3 $4<", $content);

					$content = apply_filters('the_excerpt', $content);
					$content = str_replace(']]>', ']]&gt;', $content);

				?>


				<div class="col-md-12 blog-article">
					<h3><a href="<?php the_permalink(); ?>"><?php echo $new_title ?></a></h3>

					<div class="news-item-page">
						<?php

						echo $new_content . $content;

						?>
					</div>
				</div>
			</div>

			<?php
				}
			?>
			<div class="clear"></div>


		</div>

		<?php get_sidebar("nouvelles"); ?>
	</div>

<?php get_footer(); ?>
