<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 * Template Name: Appels à nous template
 */

get_header();

$section_id = get_field("section_page_appels_a_nous",get_the_ID());
$per_page=10;
//global $paged,$pages;
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
if ($paged==1) {
    $offset_page=0;
} else {
    $offset_page=($paged-1)*$per_page;
}


$category_name = "";
switch ($section_id) {
    case '1': $category_name = "droit-des-affaires"; break;
    case '2': $category_name = "droit-de-la-personne"; break;
    case '3': $category_name = "gestion-de-bureaux"; break;
    case '4': $category_name = "droit-immobilier"; break;
    case '5': $category_name = "droit-agricole"; break;
    case '6': $category_name = "evenements-generaux"; break;
    case '7': $category_name = "notaires"; break;
    case '9': $category_name = 'fiducies'; break;
    default: $category_name = "notaires";
}

$category_name = "notaires";


$args = array(
    'posts_per_page'   => $per_page,
    'offset'           => $offset_page,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'post_date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    //'meta_key'         => 'section_appels',
    'category_name'       => $category_name,
    //'post_type'        => 'appels_a_tous',
    'post_type'        => 'post',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'post_status'      => 'publish',
    'suppress_filters' => true );

$args_total= array(
    'posts_per_page'   => -1,
    'category'         => '',
    'category_name'    => '',
    'orderby'          => 'post_date',
    'order'            => 'DESC',
    'include'          => '',
    'exclude'          => '',
    //'meta_key'         => 'section_appels',
    'category_name'       => $category_name,
    //'post_type'        => 'appels_a_tous',
    'post_type'        => 'post',
    'post_mime_type'   => '',
    'post_parent'      => '',
    'post_status'      => 'publish',
    'suppress_filters' => true );


if (isset($_GET["y"])) {


    $args = array(
        'posts_per_page'   => $per_page,
        'offset'           => $offset_page,
        'category'         => '',
        'category_name'    => '',
        'orderby'          => 'post_date',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        //'meta_key'         => 'section_appels',
        'category_name'       => $category_name,
        //'post_type'        => 'appels_a_tous',
        'post_type'        => 'post',
        'date_query' => array(
                array(
                    'year'  => $_GET["y"],
                )
            ),
        'post_mime_type'   => '',
        'post_parent'      => '',
        'post_status'      => 'publish',
        'suppress_filters' => true );

    $args_total= array(
        'posts_per_page'   => -1,
        'category'         => '',
        'category_name'    => '',
        'orderby'          => 'post_date',
        'order'            => 'DESC',
        'include'          => '',
        'exclude'          => '',
        //'meta_key'         => 'section_appels',
        'category_name'       => $category_name,
        //'post_type'        => 'appels_a_tous',
        'post_type'        => 'post',
        'date_query' => array(
                array(
                    'year'  => $_GET["y"],
                )
            ),
        'post_mime_type'   => '',
        'post_parent'      => '',
        'post_status'      => 'publish',
        'suppress_filters' => true );

}


$posts_total = count(get_posts( $args_total ));
$posts_array = get_posts( $args );
$total_page=ceil($posts_total/$per_page);
$pages=$total_page;
//$param=add_query_arg(array("paged"=>3));
wp_reset_postdata();
?>


<div id="eventContent" title="Event Details" style="display:none;">
    Start: <span id="startTime"></span><br>
    End: <span id="endTime"></span><br><br>
    <p id="eventInfo"></p>
</div>

<div class="<?php echo $color; ?> prod-details AppelTous">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title"><?php echo get_the_title(); ?></h3>
					<?php echo get_breadcrumb(); ?>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<div class="note note-success">
                <?php
                $id=404;
                $post = get_post($id);
                $content = apply_filters('the_content', $post->post_content);
                echo $content;
                ?>
			</div>
		</div>
	</div>
	<div class="row">.
		<div class="col-md-12">
			<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-12 blog-tag-data <> ">
				<div id="tab_1_1_8" class="tab-pane sousmenu-nav">
					<div class="tab-content">
						<!--ul class="nav nav-tabs nav-justified <?php if (preg_match("/droit-de-la-personne/is", $_SERVER["REQUEST_URI"])) { echo "direction2"; } if (preg_match("/droit-des-affaires/is", $_SERVER["REQUEST_URI"])) { echo "direction1"; } if (preg_match("/droit-agricole/is", $_SERVER["REQUEST_URI"])) { echo "direction5"; } if (preg_match("/droit-immobilier/is", $_SERVER["REQUEST_URI"])) { echo "direction4"; } if (preg_match("/gestion-de-bureaux/is", $_SERVER["REQUEST_URI"])) { echo "direction3"; } if (preg_match("/evenements-generaux/is", $_SERVER["REQUEST_URI"])) { echo "direction6"; }  ?> ">
							<li class="<?php if (preg_match("/droit-de-la-personne/is", $_SERVER["REQUEST_URI"])) { echo "active"; } ?> direction2">
								<a href="/extranet/appel-a-tous/droit-de-la-personne/" >Droit</br> de la personne</a>
							</li>
							<li class="<?php if (preg_match("/droit-des-affaires/is", $_SERVER["REQUEST_URI"])) { echo "active"; } ?> direction1">
                                <a href="/extranet/appel-a-tous/droit-des-affaires/" >Droit des affaires</a>
                            </li>
                            <li class="<?php if (preg_match("/droit-agricole/is", $_SERVER["REQUEST_URI"])) { echo "active"; } ?> direction5">
                                <a href="/extranet/appel-a-tous/droit-agricole/ " >Droit<br /> agricole</a>
                            </li>
                            <li class="<?php if (preg_match("/droit-immobilier/is", $_SERVER["REQUEST_URI"])) { echo "active"; } ?> direction4">
                                <a href="/extranet/appel-a-tous/droit-immobilier/" >Droit<br /> immobilier</a>
                            </li>
                            <li class="<?php if (preg_match("/gestion-de-bureaux/is", $_SERVER["REQUEST_URI"])) { echo "active"; } ?> direction3">
                                <a href="/extranet/appel-a-tous/gestion-de-bureaux/" >Gestion<br /> des bureaux</a>
                            </li>
                            <li class="<?php if (preg_match("/evenements-generaux/is", $_SERVER["REQUEST_URI"])) { echo "active"; } ?> direction6">
								<a href="/extranet/appel-a-tous/evenements-generaux/" >Évènements<br /> généraux</a>
							</li-->


							<!--li class="grey">
								<a href="/appel-a-tous/developpement-des-affaires/" >Développement <br />des affaires</a>
							</li-->

						<!--/ul-->

                        <?php

                            echo
                                "<div id=\"year-browser\">\n".
                                "   <label for=\"y\">Archives : </label>\n".
                                "   <select id=\"y\" name=\"y\">\n".
                                "       <option value=\"\"></option>\n";

                            for ($i = 2004; $i <= date("Y"); $i++) {
                                echo "      <option value=\"" . $i . "\">" . $i . "</option>\n";
                            }

                            echo
                                "   </select>\n".
                                "</div>\n";

                        ?>

						<div id="tab_2_1_1" class="soustab tab-pane active">
                            <table class="table table-striped table-bordered table-hover table-full-width <?php echo $color; ?>" id="sample_3">
                                <thead>
                                <tr>
                                    <th class="first">Nom</th>
                                    <th>Date de Publication</th>
                                </tr>
                                </thead>
                                <tbody>
                               <?php foreach ($posts_array as $pa){ ?>

                                <tr>
                                    <td class="first">
                                        <div class="col-md-8">
                                            <a href='<?php echo get_permalink( $pa->ID ); ?>'><?php echo $pa->post_title?></a>
                                        </div>

                                        <div class="col-md-4">
                                            <?php

                                            $current_time=time();
                                            $post_time= get_the_time('U', $pa->ID);
                                            $ecart=($current_time-$post_time)/(2 * 60 *60);

                                            $tag_arr=get_field('tag',$pa->ID);
                                            foreach($tag_arr as $tag) {
                                                if($tag ==3) {
                                                    ?>
                                                    <a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
                                                <?php 	}
                                                if($tag ==2) {
                                                    ?>
                                                    <a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Modifié</a>
                                                <?php  } }?>
                                        </div>

                                    </td>
                                    <td> <?php echo  get_the_time('Y-M-d', $pa->ID);?></td>

                                </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                            <?php
                                $args_pagination = array(
                                    'base' => @add_query_arg('paged','%#%'),
                                    'format'       => '?paged=%#%',
                                    'total'        => $total_page,
                                    'current'      => $paged,
                                    'show_all'     => False,
                                    'end_size'     => 1,
                                    'mid_size'     => 2,
                                    'prev_next'    => True,
                                    'prev_text'    => __('« Previous'),
                                    'next_text'    => __('Next »'),
                                    'type'         => 'plain',
                                    'add_args'     => False,
                                    'add_fragment' => '',
                                    'before_page_number' => '',
                                    'after_page_number' => ''
                                );
                                echo paginate_links( $args_pagination );

                                wp_reset_postdata();
                            ?>

						      </div>
						</div>
					</div>
				</div>
			</div>
		</div>
    <h3>Faire un appel à tous</h3>
    <?php
        the_content();

        echo "<!--";
        wp_editor( "", "bobo");
        echo "-->";

    ?>

	</div><!-- #post-## -->
</div>
<?php
get_footer();
