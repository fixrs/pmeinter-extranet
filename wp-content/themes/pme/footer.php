<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */
?>

				</div><!-- #main -->
			</div><!-- #page -->
		</div><!-- #main -->


		<!-- BEGIN FOOTER -->
		<div class="footer">
			<div class="footer-inner">
				<div class="credits">Design par <a href="http://www.leeroy.ca" target="_blank">LEEROY</a></div>
			</div>
			<div class="footer-tools">
				<span class="go-top">
					<i class="fa fa-angle-up"></i>
				</span>
			</div>
		</div>
		<!-- END FOOTER -->


		<?php wp_footer(); ?>

		<!-- BEGIN CORE PLUGINS -->

		<!-- END FOOTER -->
		<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
		<!-- BEGIN CORE PLUGINS -->
		<!--[if lt IE 9]>
		<script src="assets/plugins/respond.min.js"></script>
		<script src="assets/plugins/excanvas.min.js"></script>
		<![endif]-->

		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery-1.10.2.min.js" type="text/javascript"></script-->
		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery-migrate-1.2.1.min.js" type="text/javascript"></script-->
		<!-- IMPORTANT! Load jquery-ui-1.10.3.custom.min.js before bootstrap.min.js to fix bootstrap tooltip conflict with jquery ui tooltip -->
		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js" type="text/javascript"></script-->
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery.blockui.min.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery.cokie.min.js" type="text/javascript"></script-->


		<?php if (!preg_match("/\/mandats\//i", $_SERVER["REQUEST_URI"])) { ?>

		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>

		<?php } ?>

		<!-- END CORE PLUGINS -->
		<!-- BEGIN PAGE LEVEL PLUGINS -->
		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jqvmap/jqvmap/jquery.vmap.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js" type="text/javascript"></script-->
		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/flot/jquery.flot.min.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/flot/jquery.flot.resize.min.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/flot/jquery.flot.categories.min.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery.pulsate.min.js" type="text/javascript"></script-->
		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/bootstrap-daterangepicker/moment.min.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script-->
		<!-- <script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/gritter/js/jquery.gritter.js" type="text/javascript"></script> -->
		<!-- IMPORTANT! fullcalendar depends on jquery-ui-1.10.3.custom.min.js for drag & drop support -->



		<!--script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/custom/moment.js"></script>

		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js" type="text/javascript"></script-->
		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery-easy-pie-chart/jquery.easy-pie-chart.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/jquery.sparkline.min.js" type="text/javascript"></script-->
		<!-- END PAGE LEVEL PLUGINS -->
		<!-- BEGIN PAGE LEVEL SCRIPTS -->
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/core/app.js" type="text/javascript"></script>
		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/custom/index.js" type="text/javascript"></script-->
		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/custom/tasks.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/data-tables/jquery.dataTables.min.js" type="text/javascript"></script-->
		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/data-tables/DT_bootstrap.js" type="text/javascript"></script-->

		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/jquery.simpleWeather/jquery.simpleWeather.min.js" type="text/javascript"></script-->
		<!--script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/plugins/select2/select2.min.js" type="text/javascript"></script>
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/custom/table-advanced.js" type="text/javascript"></script-->
		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/scripts.js" type="text/javascript"></script>


		<!--script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/wang.js"></script-->
		<!-- END PAGE LEVEL SCRIPTS -->
		<script>
		//jQuery(document).ready(function($) {
		    // initlayout and core plugins
		   // Index.init();
		   // Index.initCalendar(); // init index page's custom scripts
		   //Index.initCalendar2();
		   //TableAdvanced.init();

		//});
		// jQuery('.compte_rendu').click(function(){
		// 	jQuery('.tab_1_1_4').trigger('click');
		// return false;
		// });
		// jQuery('.liste_membre').click(function(){
		// 	jQuery('.tab_1_1_3').trigger('click');
		// return false;
		// });
		// jQuery(function(){

		// });
		</script>

		<!-- END CORE PLUGINS -->

		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-23306924-1']);
			_gaq.push(['_trackPageview']);
			(function() {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>

	</body>
</html>
