<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Twelve already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 *
 * @link http://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

<?php

if (isset($_GET["ad"])) {

    $date_parts = explode("-", $_GET["ad"]);

    $posts = get_posts(array(
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'publications',
        'post_status' => 'publish',
        'date_query' => array(
            array(
                'year'  => $date_parts[0],
                'month' => $date_parts[1]
                )
            )
    ));


} else {

    $posts = get_posts(array(
        'numberposts' => 20,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'publications',
        'post_status' => 'publish'
    ));

}

$pdf_path="";
$image_path="";
$image_title="";

?>
		<div class="publications">
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title"><?php echo post_type_archive_title('', false); if (isset($_GET["ad"])) { echo " (" . $_GET["ad"] . ")"; } ?></h3>
					<?php

                        if (isset($_GET["ad"])) {
                            ob_start();
                            get_breadcrumb();
                            $breadcrumb = ob_get_contents();
                            ob_end_clean();

                            echo preg_replace("/<li>\s*Publications\s*<\/li>/is", "<li><a href=\"/extranet/publications/\">Publications</a> <i class=\"fa fa-angle-right\"></i></li><li>" . $_GET["ad"] . "</li>", $breadcrumb);
                        } else {
                            get_breadcrumb();
                        }
                    ?>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-sm-8">
                    <div class="row">

                    <?php
                        foreach ($posts as $post) {
                            $id_dernier=$post->ID;
                            $id_publication=$id_dernier;
                            $pdf_obj=get_field("pdf",$id_publication);
                            $image_objet=get_field("couverture",$id_publication);
                            $image_path=$image_objet["url"];
                            $image_title=$image_objet["alt"];

                            echo
                                "<div class=\"col-md-6 col-sm-12 publication-item\">\n".
                                "   <a href=\"" . $pdf_obj . "\" target=\"_blank\"><img src=\"" . $image_path . "\" alt=\"" . $image_title . "\"><br /><strong>" . get_the_title($id_publication) . "</strong></a>\n".
                                "</div>\n";
                       }
                    ?>

                    </div>
				</div>
				<div class="col-sm-4 col-md-4">
					<?php get_sidebar("publications"); ?>
				</div>
			</div>
		</div>

	</div>
</div><!-- #content -->

<?php get_footer(); ?>
