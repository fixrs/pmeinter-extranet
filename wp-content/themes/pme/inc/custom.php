<?php
add_role(
    'droit_affaire',
    __( "Droit des affaires" ),
    array(
        'read' => true,
        'edit_posts'   => true,
        'delete_posts' => false
    )
);

add_role(
    'droit_immobilier',
    __( 'Droit immobilier' ),
    array(
        'read'         => true,
        'edit_posts'   => true,
        'delete_posts' => false
    )
);

add_role(
    'droit_personne',
    __( 'Droit de la personne' ),
    array(
        'read'         => true,
        'edit_posts'   => true,
        'delete_posts' => false
    )
);

add_role(
    'droit_agricole',
    __( 'Droit agricole' ),
    array(
        'read'         => true,
        'edit_posts'   => true,
        'delete_posts' => false
    )
);

add_role(
    'gestion_bureau',
    __( 'Gestion de bureau' ),
    array(
        'read'         => true,
        'edit_posts'   => true,
        'delete_posts' => false
    )
);

add_role(
    'dev_affaires',
    __( 'Événements généraux' ),
    array(
        'read'         => true,
        'edit_posts'   => true,
        'delete_posts' => false
    )
);

add_role(
'action_des_collaboratrices',
__( 'Action des collaboratrices' ),
array(
'read'         => true,
'edit_posts'   => true,
'delete_posts' => false
)
);



register_post_type( "fournisseurs", array(
    'labels'             => array(
        'name'               => 'Fournisseurs',
        'singular_name'      => 'Fournisseur',
        'add_new_item'       => 'Ajouter un fournisseur',
        'edit_item'          => "Éditer le fournisseur",
        'new_item'           => 'Nouveau forunisseur',
        'all_items'          => 'Tous les fournisseurs',
        'view_item'          => "Voir le fournisseur",
        'search_items'       => 'Rechercher des fournisseurs',
        'not_found'          => 'Aucun fournisseur trouvé',
        'not_found_in_trash' => 'Aucun fournisseur trouvé dans la corbeille',
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    //'rewrite'            => array( 'slug' => _x( 'marques', 'URL slug' ) ),
    'capabilities' => array(
        'edit_post'          => 'update_core',
        'read_post'          => 'update_core',
        'delete_post'        => 'update_core',
        'edit_posts'         => 'update_core',
        'edit_others_posts'  => 'update_core',
        'publish_posts'      => 'update_core',
        'read_private_posts' => 'update_core'
    ),
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 41,
    'supports'           => array( 'title', 'revisions' )
));

register_post_type( "nouvelles", array(
    'labels'             => array(
        'name'               => 'Nouvelles',
        'singular_name'      => 'Nouvelle',
        'add_new_item'       => 'Ajouter une nouvelle',
        'edit_item'          => "Éditer la nouvelle",
        'new_item'           => 'Nouvelle nouvelle',
        'all_items'          => 'Toutes les nouvelles',
        'view_item'          => "Voir la nouvelle",
        'search_items'       => 'Rechercher des nouvelles',
        'not_found'          => 'Aucune nouvelle trouvée',
        'not_found_in_trash' => 'Aucune mouvelle trouvée dans la corbeille',
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    //'rewrite'            => array( 'slug' => _x( 'marques', 'URL slug' ) ),
    'capabilities' => array(
        'edit_post'          => 'update_core',
        'read_post'          => 'update_core',
        'delete_post'        => 'update_core',
        'edit_posts'         => 'update_core',
        'edit_others_posts'  => 'update_core',
        'publish_posts'      => 'update_core',
        'read_private_posts' => 'update_core'
    ),
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 41,
    'supports'           => array( 'title', 'editor', 'revisions', 'thumbnail', 'comments' )
));

register_post_type( "publications", array(
    'labels'             => array(
        'name'               => 'Publications',
        'singular_name'      => 'Publication',
        'add_new_item'       => 'Ajouter une publication',
        'edit_item'          => "Éditer la publication",
        'new_item'           => 'Nouvelle publication',
        'all_items'          => 'Toutes les publications',
        'view_item'          => "Voir la publication",
        'search_items'       => 'Rechercher des publications',
        'not_found'          => 'Aucune publication trouvée',
        'not_found_in_trash' => 'Aucune publication trouvée dans la corbeille',
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    //'rewrite'            => array( 'slug' => _x( 'marques', 'URL slug' ) ),
    'capabilities' => array(
        'edit_post'          => 'update_core',
        'read_post'          => 'update_core',
        'delete_post'        => 'update_core',
        'edit_posts'         => 'update_core',
        'edit_others_posts'  => 'update_core',
        'publish_posts'      => 'update_core',
        'read_private_posts' => 'update_core'
    ),
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 41,
    'supports'           => array( 'title', 'revisions' )
));

$args = array(
    'hierarchical'          => true,
    'labels'                => array(
        'name'                       => _x( 'Catégories', 'taxonomy general name' ),
        'singular_name'              => _x( 'Catégorie', 'taxonomy singular name' ),
        'search_items'               => __( 'Rechercher une catégorie' ),
        'popular_items'              => __( 'Catégories populaires' ),
        'all_items'                  => __( 'Toutes les catégories' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Editer la catégorie' ),
        'update_item'                => __( 'Mettre à jour la catégorie' ),
        'add_new_item'               => __( 'Ajouter une nouvelle catégorie' ),
        'new_item_name'              => __( 'Nouveau nom' ),
        'separate_items_with_commas' => __( 'Séparer les catégories par des virgules' ),
        'add_or_remove_items'        => __( 'Ajouter ou supprimer des catégories' ),
        'choose_from_most_used'      => __( 'Choisir parmi les catégories les plus utilisées' ),
        'not_found'                  => __( 'Aucune catégorie trouvée.' ),
        'menu_name'                  => __( 'Catégories' ),
    ),
    'show_ui'               => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'pub-cat' ),
);

register_taxonomy( 'pub-cat', 'publications', $args );

function get_breadcrumb() {
    global $post;

    ?><ul class="page-breadcrumb breadcrumb">
        <li><i class="fa fa-home"></i>
            <?php if (!is_home()) : ?>
                <a href="/extranet/"><?php _e("Extranet", "pme"); ?></a>
            <?php else : ?>
                <?php _e("Extranet", "pme"); ?>
            <?php endif; ?>
                <i class="fa fa-angle-right"></i>
        </li>

        <?php if (is_home()) : ?>
            <li><?php _e("Accueil", "pme"); ?></li>

        <?php elseif (is_category() || is_single()) : ?>
            <?php
            /*$cat = get_the_category();

            if (!empty($cat)) : ?>
                <li><?php echo $cat[0]->cat_name; ?><i class="fa fa-angle-right"></i></li><li>
            <?php endif;*/ ?>


            <?php if (is_single()) : ?>

                <?php if( ! is_singular( array('page', 'attachment', 'post') ) ) :
                    $pt_name = get_post_type();
                    $pt = get_post_type_object($pt_name); ?>

                   <li><a href="<?php echo get_post_type_archive_link( $pt_name ); ?>"><?php echo $pt->labels->name; ?></a> <i class="fa fa-angle-right"></i></li>
                <?php endif; ?>

                <li><?php the_title(); ?></li>
            <?php endif; ?>

        <?php elseif ( is_post_type_archive() ) : ?>
            <li><?php post_type_archive_title(); ?></li>

        <?php elseif (is_page()) : ?>
            <?php if($post->post_parent) :
                $anc = get_post_ancestors( $post->ID );
                $title = get_the_title();
                foreach ( $anc as $ancestor ) : ?>
                    <li>
                        <a href="<?php echo get_permalink($ancestor) ?>"><?php echo get_the_title($ancestor) ?></a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                <?php endforeach; ?>

                <li><?php echo $title; ?></li>

            <?php else : ?>
                <li><?php echo get_the_title(); ?></li>
            <?php endif; ?>

        <?php elseif (is_tag()) : ?>
            single_tag_title();

        <?php elseif (is_day()) : ?>
            <li>Archive pour <?php the_time('F jS, Y'); ?></li>

        <?php elseif (is_month()) : ?>
            <li>Archive pour <?php the_time('F, Y'); ?></li>

        <?php elseif (is_year()) : ?>
            <li>Archive pour <?php the_time('Y'); ?></li>

        <?php elseif (is_author()) : ?>
            <li>Archive d'auteur</li>

        <?php elseif (isset($_GET['paged']) && !empty($_GET['paged'])) : ?>
            <li>Blog Archives</li>
        <?php elseif (is_search()) : ?>
            <li>Résultats de Recherche</li>
        <?php endif; ?>
    </ul><?php
}

function new_content_more($more) {
       global $post;
       return '<a href="' . get_permalink() . '#more-' . $post->ID . '" class="more-link btn blue">En savoir plus <i class="m-icon-swapright m-icon-white"></i></a>';
}
add_filter( 'the_content_more_link', 'new_content_more' );



function posts_by_date() {
  // array to use for results
  $years = array();

  // get posts from WP
  $posts = get_posts(array(
    'numberposts' => -1,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'post_type' => 'nouvelles',
    'post_status' => 'publish'
  ));

  // loop through posts, populating $years arrays
  foreach($posts as $post) {
    $time = strtotime($post->post_date);
    $years[date('Y', $time)][date('n', $time)][] = $post;
  }

  return $years;
}


//wang create
function publication_by_date() {
    // array to use for results
    $years = array();

    // get posts from WP
    $posts = get_posts(array(
        'numberposts' => -1,
        'orderby' => 'post_date',
        'order' => 'DESC',
        'post_type' => 'publications',
        'post_status' => 'publish'
    ));

    // loop through posts, populating $years arrays
    foreach($posts as $post) {
        $time = strtotime($post->post_date);
        $years[date('Y', $time)][date('n', $time)][] = $post;
    }

    return $years;
}




register_post_type( "comptesrendu", array(
'labels'             => array(
'name'               => 'Comptes rendu',
'singular_name'      => 'Comptesrendu',
'add_new_item'       => 'Ajouter une Comptesrendu',
'edit_item'          => "Éditer la Comptesrendu",
'new_item'           => 'Nouvelle Comptesrendu',
'all_items'          => 'Toutes les Comptesrendu',
'view_item'          => "Voir la Comptesrendu",
'search_items'       => 'Rechercher des Comptesrendu',
'not_found'          => 'Aucune Comptesrendu trouvée',
'not_found_in_trash' => 'Aucune Comptesrendu trouvée dans la corbeille',
'parent_item_colon'  => ''
		),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		//'rewrite'            => array( 'slug' => _x( 'marques', 'URL slug' ) ),
        'capabilities' => array(
        'edit_post'          => 'update_core',
        'read_post'          => 'update_core',
        'delete_post'        => 'update_core',
        'edit_posts'         => 'update_core',
        'edit_others_posts'  => 'update_core',
        'publish_posts'      => 'update_core',
        'read_private_posts' => 'update_core'
    ),
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 41,
		'supports'           => array( 'title', 'editor', 'revisions', 'thumbnail', 'comments' )
));

function comptes_rendu_archive($id_cats_secteur) {
	// array to use for results
	$years = array();
	$args = array(
			'post_type'		=> 'comptesrendu',
			'offset'=>0,
			'posts_per_page'	=> -1,
			'meta_query' => array (
					array (
							'key' => 'pages_associes',
							'value' => $id_cats_secteur
					)),

			'meta_key'		=> 'date',
			'orderby'		=> 'date',
			'order'			=> 'DESC',
			'post_status' => 'publish'
	);
	$comptes_rendu = get_posts( $args );

	// loop through posts, populating $years arrays
	foreach($comptes_rendu as $post) {
		$time = strtotime(get_field("date",$post->ID));
		$years[date('Y', $time)][date('n', $time)][] = $post;
	}

	return $years;

}


register_post_type( "documents", array(
'labels'             => array(
'name'               => 'Documents',
'singular_name'      => 'documents',
'add_new_item'       => 'Ajouter un Document',
'edit_item'          => "Éditer le Document",
'new_item'           => 'Nouveau Document',
'all_items'          => 'Toutes les Documents',
'view_item'          => "Voir la Documents",
'search_items'       => 'Rechercher des Documents',
'not_found'          => 'Aucun Documents trouvé',
'not_found_in_trash' => 'Aucun Documents trouvé dans la corbeille',
'parent_item_colon'  => ''
		),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		//'rewrite'            => array( 'slug' => _x( 'marques', 'URL slug' ) ),
        'capabilities' => array(
        'edit_post'          => 'update_core',
        'read_post'          => 'update_core',
        'delete_post'        => 'update_core',
        'edit_posts'         => 'update_core',
        'edit_others_posts'  => 'update_core',
        'publish_posts'      => 'update_core',
        'read_private_posts' => 'update_core'
    ),
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 41,
		'supports'           => array( 'title', 'editor', 'revisions', 'thumbnail', 'comments' )
));


$args = array(
    'hierarchical'          => true,
    'labels'                => array(
        'name'                       => _x( 'Catégories', 'taxonomy general name' ),
        'singular_name'              => _x( 'Catégorie', 'taxonomy singular name' ),
        'search_items'               => __( 'Rechercher une catégorie' ),
        'popular_items'              => __( 'Catégories populaires' ),
        'all_items'                  => __( 'Toutes les catégories' ),
        'parent_item'                => null,
        'parent_item_colon'          => null,
        'edit_item'                  => __( 'Editer la catégorie' ),
        'update_item'                => __( 'Mettre à jour la catégorie' ),
        'add_new_item'               => __( 'Ajouter une nouvelle catégorie' ),
        'new_item_name'              => __( 'Nouveau nom' ),
        'separate_items_with_commas' => __( 'Séparer les catégories par des virgules' ),
        'add_or_remove_items'        => __( 'Ajouter ou supprimer des catégories' ),
        'choose_from_most_used'      => __( 'Choisir parmi les catégories les plus utilisées' ),
        'not_found'                  => __( 'Aucune catégorie trouvée.' ),
        'menu_name'                  => __( 'Catégories' ),
    ),
    'show_ui'               => true,
    'show_admin_column'     => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var'             => true,
    'rewrite'               => array( 'slug' => 'doc-cat' ),
);

register_taxonomy( 'doc-cat', 'documents', $args );





register_post_type( "informatique", array(
'labels'             => array(
'name'               => 'Produits Informatiques',
'singular_name'      => 'informatiques',
'add_new_item'       => 'Ajouter une Produits Informatiques',
'edit_item'          => "Éditer la Produits Informatiques",
'new_item'           => 'Nouvelle Produits Informatiques',
'all_items'          => 'Toutes les Produits Informatiques',
'view_item'          => "Voir la Produits Informatiques",
'search_items'       => 'Rechercher des Produits Informatiques',
'not_found'          => 'Aucune Produits Informatiques trouvée',
'not_found_in_trash' => 'Aucune Produits Informatiques trouvée dans la corbeille',
'parent_item_colon'  => ''
		),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		//'rewrite'            => array( 'slug' => _x( 'marques', 'URL slug' ) ),
        'capabilities' => array(
        'edit_post'          => 'update_core',
        'read_post'          => 'update_core',
        'delete_post'        => 'update_core',
        'edit_posts'         => 'update_core',
        'edit_others_posts'  => 'update_core',
        'publish_posts'      => 'update_core',
        'read_private_posts' => 'update_core'
    ),
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => 41,
		'supports'           => array( 'title', 'editor', 'revisions', 'thumbnail', 'comments' )
));


register_post_type( "outils", array(
    'labels'             => array(
        'name'               => 'Bôite à outils',
        'singular_name'      => 'outils',
        'add_new_item'       => 'Ajouter une Bôite à outils',
        'edit_item'          => "Éditer une Bôite à outils",
        'new_item'           => 'Nouvelle Bôite à outils',
        'all_items'          => 'Toutes les Bôites à outils',
        'view_item'          => "Voir la Bôite à outils",
        'search_items'       => 'Rechercher des Bôite à outils',
        'not_found'          => 'Aucune Bôite à outils trouvée',
        'not_found_in_trash' => 'Aucune Bôite à outils trouvée dans la corbeille',
        'parent_item_colon'  => ''
    ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    //'rewrite'            => array( 'slug' => _x( 'marques', 'URL slug' ) ),
    'capabilities' => array(
        'edit_post'          => 'update_core',
        'read_post'          => 'update_core',
        'delete_post'        => 'update_core',
        'edit_posts'         => 'update_core',
        'edit_others_posts'  => 'update_core',
        'publish_posts'      => 'update_core',
        'read_private_posts' => 'update_core'
    ),
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 41,
    'supports'           => array( 'title', 'editor', 'revisions', 'thumbnail', 'comments' )
));



// register_post_type( "appels_a_tous", array(
//     'labels'             => array(
//         'name'               => 'Appels à tous',
//         'singular_name'      => 'appels_a_tous',
//         'add_new_item'       => 'Ajouter un appel à tous',
//         'edit_item'          => "Éditer un appel à tous",
//         'new_item'           => 'Nouveaux appels à tous',
//         'all_items'          => 'Tous les appels à tous',
//         'view_item'          => "Voir un appel à tous",
//         'search_items'       => 'Rechercher des appel à tous',
//         'not_found'          => 'Aucune appel à tous trouvé',
//         'not_found_in_trash' => 'Aucune appel à tous trouvé dans la corbeille',
//         'parent_item_colon'  => ''
//     ),
//     'public'             => true,
//     'publicly_queryable' => true,
//     'show_ui'            => true,
//     'show_in_menu'       => true,
//     'query_var'          => true,
//     //'rewrite'            => array( 'slug' => _x( 'marques', 'URL slug' ) ),
//     'capability_type'    => 'post',
//     'has_archive'        => true,
//     'hierarchical'       => false,
//     'menu_position'      => 45,
//     'supports'           => array( 'title', 'editor', 'author','revisions', 'thumbnail', 'comments' )
// ));





add_action( 'show_user_profile', 'my_show_extra_profile_fields' );
add_action( 'edit_user_profile', 'my_show_extra_profile_fields' );
add_action( 'user_new_form',     'my_show_extra_profile_fields' );

function my_show_extra_profile_fields( $user ) { ?>

	<h3>Extra profile information</h3>

	<table class="form-table">

		<tr>
			<th><label for="linkedin">Linkedin</label></th>

			<td>
				<input type="text" name="linkedin" id="linkedin" value="<?php echo esc_attr( get_the_author_meta( 'linkedin', $user->ID ) ); ?>" class="regular-text" /><br />

			</td>
		</tr>
		<tr>
			<th><label for="Tel">Téléphone</label></th>

			<td>
				<input type="text" name="tel" id="tel" value="<?php echo esc_attr( get_the_author_meta( 'tel', $user->ID ) ); ?>" class="regular-text" /><br />

			</td>
		</tr>

        <tr>
            <th><label for="Tel">Etude</label></th>

            <td>
                <input type="text" style="width: 650px" name="etude" id="tel" value="<?php echo esc_attr( get_the_author_meta( 'etude', $user->ID ) ); ?>" class="regular-text" /><br />

            </td>
        </tr>

	</table>
<?php }


add_action( 'personal_options_update', 'my_save_extra_profile_fields' );
add_action( 'edit_user_profile_update', 'my_save_extra_profile_fields' );
add_action( 'user_register', 'my_save_extra_profile_fields' );

function my_save_extra_profile_fields( $user_id ) {

	if ( !current_user_can( 'edit_user', $user_id ) )
		return false;

	/* Copy and paste this line for additional fields. Make sure to change 'twitter' to the field ID. */
	update_usermeta( $user_id, 'linkedin', $_POST['linkedin'] );
	update_usermeta( $user_id, 'tel', $_POST['tel'] );
}



$GLOBALS['page_ass']=array("s-1"=>'Droit des affaires',
    "s-2"=>"Droit de la personne",
    "s-3"=>"Gestion de bureaux",
    "s-4"=>"Droit immobilier",
    "s-5"=>"Droit agricole",
    "s-6"=>"Événements généraux",
    "s-7"=>"Action des collaboratrices");


/* documents */
add_filter('manage_documents_posts_columns', 'bs_documents_table_head');
function bs_documents_table_head( $columns ) {


       $columns['pages_associes_document']  = 'pages associés ';

    return $columns;

}
add_action( 'manage_documents_posts_custom_column', 'bs_documents_table_content', 10, 2 );


function bs_documents_table_content( $column_name, $post_id ) {


    if( $column_name == 'pages_associes_document' ) {
        $project_category = get_post_meta( $post_id, 'pages_associes_document' ,true);
        echo $GLOBALS['page_ass']["s-".$project_category];
    }
}


function sortable_columns($columns) {
    $columns['pages_associes_document'] = 'pages_associes_document';
    return $columns;
}



add_filter( "manage_edit-documents_sortable_columns", "sortable_columns" );


/** end documents */




/* comptes rendu */
add_filter('manage_comptesrendu_posts_columns', 'bs_comptesrendu_table_head');
function bs_comptesrendu_table_head( $columns ) {


    $columns['pages_associes_comptesrendu']  = 'Pages associés ';

    return $columns;

}
add_action( 'manage_comptesrendu_posts_custom_column', 'bs_comptesrendu_table_content', 10, 2 );


function bs_comptesrendu_table_content( $column_name, $post_id ) {


    if( $column_name == 'pages_associes_comptesrendu' ) {
        $project_category = get_post_meta( $post_id, 'pages_associes' ,true);
        echo $GLOBALS['page_ass']["s-".$project_category];
    }
}


function sortable_columns_comptesrendu($columns) {
    $columns['pages_associes_comptesrendu'] = 'pages_associes_comptesrendu';
    return $columns;
}



add_filter( "manage_edit-comptesrendu_sortable_columns", "sortable_columns_comptesrendu" );


/** end comptes rendu */



/* Informatique */
add_filter('manage_informatique_posts_columns', 'bs_informatique_table_head');
function bs_informatique_table_head( $columns ) {


    $columns['pages_associes_info_detail']  = 'Pages associés ';

    return $columns;

}
add_action( 'manage_informatique_posts_custom_column', 'bs_informatique_table_content', 10, 2 );


function bs_informatique_table_content( $column_name, $post_id ) {


    if( $column_name == 'pages_associes_info_detail' ) {
        $project_category = get_post_meta( $post_id, 'pages_associes_info_detail' ,true);
        echo $GLOBALS['page_ass']["s-".$project_category];
    }
}


function sortable_columns_informatique($columns) {
    $columns['pages_associes_info_detail'] = 'pages_associes_info_detail';
    return $columns;
}



add_filter( "manage_edit-informatique_sortable_columns", "sortable_columns_informatique" );


/** end Informatique */



/* Outil */
add_filter('manage_outils_posts_columns', 'bs_outils_table_head');
function bs_outils_table_head( $columns ) {


    $columns['pages_associes_outils']  = 'Pages associés ';

    return $columns;

}
add_action( 'manage_outils_posts_custom_column', 'bs_outils_table_content', 10, 2 );


function bs_outils_table_content( $column_name, $post_id ) {


    if( $column_name == 'pages_associes_outils' ) {
        $project_category = get_post_meta( $post_id, 'pages_associes_outil' ,true);
        echo $GLOBALS['page_ass']["s-".$project_category];
    }
}


function sortable_columns_outils($columns) {
    $columns['pages_associes_outils'] = 'pages_associes_outils';
    return $columns;
}



add_filter( "manage_edit-outils_sortable_columns", "sortable_columns_outils" );


/** end Outil */







/* Sorts the sector. */
/*
function my_sort_secteur( $vars ) {

    $acf_arr=array("documents"=>"pages_associes_document","informatique"=>"pages_associes_info_detail","comptesrendu"=>"pages_associes","outils"=>"pages_associes_outil");

    if ( isset( $vars['post_type'] ) && ('documents' == $vars['post_type']||'informatique' == $vars['post_type']||'comptesrendu' == $vars['post_type']||'outils' == $vars['post_type']) ) {
        $page_type_get=$vars['post_type'] ;


        if ( isset( $vars['orderby'] ) && $acf_arr[$page_type_get] == $vars['orderby'] ) {


            $vars = array_merge(
                $vars,
                array(
                    'meta_key' => '$acf_arr[$page_type_get]',
                    'orderby' => '$acf_arr[$page_type_get]'
                )
            );
        }
    }

    return $vars;
}
*/

$GLOBALS['acf_arr']=array("documents"=>"pages_associes_document","informatique"=>"pages_associes_info_detail","comptesrendu"=>"pages_associes","outils"=>"pages_associes_outil");

$GLOBALS['post_type']=array("Document"=>"documents","Informatique"=>"informatique","Comptes Rendu"=>"comptesrendu","Outils"=>"outils");
$GLOBALS['doc_type']=array("doc"=>"fa-file-word-o","docx"=>"fa-file-word-o","xls"=>"fa-file-excel-o","xlsx"=>"fa-file-excel-o","ppt"=>"fa-file-powerpoint-o","pptx"=>"fa-file-powerpoint-o","zip"=>"fa-file-archive-o","rar"=>"fa-file-archive-o","pdf"=>"fa-file-pdf-o","file"=>"fa-file");

add_filter( 'parse_query', 'ba_admin_posts_filter' );
add_action( 'restrict_manage_posts', 'ba_admin_posts_filter_restrict_manage_posts' );

function ba_admin_posts_filter( $query )
{

    global $pagenow;
    if ( is_admin() && $pagenow=='edit.php' && isset($_GET['ADMIN_FILTER_FIELD_NAME'])  != ''&& ($_GET['post_type']=='documents'|| $_GET['post_type']=='informatique'|| $_GET['post_type']=='comptesrendu'|| $_GET['post_type']=='outils')) {
        $post_type_get=$_GET['post_type'];
        $query->query_vars['meta_key'] =$GLOBALS['acf_arr'][$post_type_get];
        $query->query_vars['meta_value']=$_GET['ADMIN_FILTER_FIELD_NAME'];
        $query->query_vars['post_type'] =$_GET['post_type'];
        if (isset($_GET['ADMIN_FILTER_FIELD_VALUE']) && $_GET['ADMIN_FILTER_FIELD_VALUE'] != '')
            $query->query_vars['meta_value'] = $_GET['ADMIN_FILTER_FIELD_VALUE'];
    }
}

function ba_admin_posts_filter_restrict_manage_posts()
{
    if ( is_admin() &&  ($_GET['post_type']=='documents'|| $_GET['post_type']=='informatique'|| $_GET['post_type']=='comptesrendu'|| $_GET['post_type']=='outils')){
    global $wpdb;
    //sql = 'SELECT DISTINCT meta_key FROM '.$wpdb->postmeta.' ORDER BY 1';
  //  $fields = $wpdb->get_results($sql, ARRAY_N);
    ?>
    <select name="ADMIN_FILTER_FIELD_NAME">
        <option value="">Filter par Pages associés</option>
        <option value="">Tous</option>
        <?php
        $current = isset($_GET['ADMIN_FILTER_FIELD_NAME'])? $_GET['ADMIN_FILTER_FIELD_NAME']:'';
        foreach ($GLOBALS['page_ass'] as $k=>$v) {
        ?>
            <option value="<?php echo  str_replace("s-","",$k) ?>" <?php if ($current==str_replace("s-","",$k)){?>selected<?php }?> ><?php echo $v ?></option>

        <?php
                  }
        ?>
   <?php
}}




function add_rewrite_rules($aRules) {
    $aNewRules = array('appels/([^/]+)/?$' => 'index.php?p=msds-pif&msds_pif_cat=$matches[1]');
    $aRules = $aNewRules + $aRules;
    return $aRules;
}

// hook add_rewrite_rules function into rewrite_rules_array
add_filter('rewrite_rules_array', 'add_rewrite_rules');






