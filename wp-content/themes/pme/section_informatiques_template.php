<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 * Template Name: Section produits infomatique template
 */

get_header(); ?>

<?php
$color=get_field('color_page',get_the_ID());
/*
$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "yellow",
);
$color = $colors[$page];
*/

//$color =$get_color;
$id_section=get_the_ID();
$parentid=wp_get_post_parent_id( $id_section );
$id_cats_secteur=get_field("key",$parentid);
$color=get_field('color_page',$parentid);
$pagelink=get_permalink(get_the_ID() );
?>

<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title"><?php the_title();?></h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php get_home_url(); ?>">Intranet</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa"></i>
						<a href="<?php echo get_permalink($parentid); ?>"><?php echo get_the_title($parentid);?></a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><?php the_title();?></li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<div class="row">
			<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-12 blog-tag-data <?php echo $color; ?> produitsInfoGral">
				<div class="entry-content">
					<div class="tabbable-custom nav-justified">
						<div class="mobile-dd" style="display: none;">
							<?php
                                include_once 'global_nav.php';
                            ?>
							<div class="mobile-handle">
								<a class="fa fa-angle-down">
								</a>
							</div>
						</div>
						<div class="tab-content">
							<div id="tab_1_1_6"  style="display: block" class="tab-pane">

                                <div class="note note-success">
									<?php the_content()?>
								</div>
								<table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
									<thead>
										<tr>
											<th class="first">Nom</th>
											<th class="middle">Dern. Màj</th>
											<th class="last">Détails</th>
										</tr>
									</thead>
									<tbody>
									<?php

												$args = array(
													'post_type'		=> 'informatique',
													'posts_per_page'	=> -1,
													// 'meta_query' => array (
													// array (
													// 		'key' => 'pages_associes_info_detail',
													// 		'value' => $id_cats_secteur
													// ))

											);
											$doc_info= new WP_Query( $args );


											if ( $doc_info->have_posts() ) :

											while ( $doc_info->have_posts() ) : $doc_info->the_post();



									?>
										<tr>
											<td class="first">
												<div class="col-md-8">
													<?php echo get_the_title()?>
												</div>
                                                <div class="col-md-4">
                                                    <?php


                                                    $current_time=time();
                                                    $post_time= get_the_time('U', get_the_ID());
                                                    $ecart=($current_time-$post_time)/(2 * 60 *60);
                                                    if($ecart>60)
                                                    {
                                                        update_field('tag', array(), get_the_ID());

                                                    }
                                                    $tag_arr=get_field('tag',get_the_ID());
                                                    foreach($tag_arr as $tag)
                                                    {
                                                        if($tag ==3)
                                                        {
                                                            ?>
                                                            <a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
                                                        <?php 	}
                                                        if($tag ==2)
                                                        {
                                                            ?>
                                                            <a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Modifié</a>
                                                        <?php  } }?>
                                                </div>
                                            </td>
											<td class="middle"><?php echo get_the_date()?></td>
											<td>
													<form class="fleche" action="<?php echo  $pagelink?>produit-informatique_detail" method="post">
													   <input type="hidden" name="info_id" value="<?php echo get_the_ID();?>"/>
													   <input class="<?php echo $color; ?>" type="submit" value="envoyer" name="bt">
													</form>

											</td>
										</tr>
									<?php  endwhile;endif;wp_reset_postdata(); ?>

									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div><!-- .entry-content -->
			</div>
		</div><!-- #post-## -->
	</div>
</div>
<?php
get_footer();
