<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Leeroy 1.0
 * Template Name: Direction travail page template
 */

get_header(); ?>

<?php

/*
1, droit des affaires
2, droit de la personne
3, gestion de bureaux
4, droit immobilier
5, droit agricole
6, développement des affaires
7, action des collaboratrices

 *
 */
$form_id=array("sec-1"=>4,"sec-2"=>451,"sec-3"=>461,"sec-4"=>455,"sec-5"=>453,"sec-6"=>463,"sec-7"=>465, "sec-9" => 15341);
$page = "droit-affaires";
$color=get_field('color_page',get_the_ID());
/*
$colors = array(
	"droit-personne" => "blue",
	"droit-affaires" => "green",
	"droit-agricole" => "red",
	"droit-immobilier" => "orange",
	"comite-gestion" => "blue-dark",
	"dev-affaires" => "grey",
	"collaboratrices" => "yellow",
);
$color = $colors[$page];
*/

//$color =$get_color;
$id_cats_secteur=get_field("key",get_the_ID());
$parentid=get_the_ID();
?>
<div id="eventContent" title="Event Details" style="display:none;">
    <b>Date: </b><span id="startTime"></span><br>
     <b>Lieu:</b> <span id="lieu"></span><br><br>


</div>
<div class="row <?php echo $color; ?>">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title"><?php the_title();?></h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php get_home_url(); ?>">Intranet</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><?php the_title();?></li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<div class="row">
			<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-12 blog-tag-data">
				<div class="entry-content">
					<div class="tabbable-custom nav-justified">
						<div class="mobile-dd">
							<ul class="nav nav-tabs nav-justified menuNavGeneral">
								<li class="active">
									<a href="" >Aperçu<br /></a>
								</li>
								<?php

									$args = array(
											'post_type'      => 'page',
											'posts_per_page' => -1,
											'post_parent'    => get_the_ID(),
											'order'          => 'ASC',
											'orderby'        => 'menu_order'
									);


									$allpages_sections = new WP_Query( $args );

										if ( $allpages_sections->have_posts() ) :

								  			$i=2;
											while ( $allpages_sections->have_posts() ) : $allpages_sections->the_post();


								?>
								<li><a class="tab_1_1_<?php echo $i;?>" href="<?php echo get_permalink();?>" ><?php  the_title(); ?><br /></a>
								</li>
								<?php
											$i++;
											endwhile;
										endif;
										wp_reset_postdata();
								?>
							</ul>
							<div class="mobile-handle">
								<a class="fa fa-angle-down">
								</a>
							</div>
						</div>

						<div class="tab-content">
							<div id="tab_1_1_1" class="tab-pane active">
								<div class="row">
									<div class="col-md-6">
										<div class="portlet box <?php echo $color; ?> calendar">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-calendar"></i>Calendrier
												</div>

											</div>
											<div class="portlet-body light">
												<!--div id="apercu_calendar"  idsecteur="<?php echo $id_cats_secteur;?>"  class="calendar_get" ></div-->
												<?php
													$filters = array();
													switch (get_the_ID()) {
														case 214: $filters[2] = 1; break;
														case 206: $filters[1] = 1; break;
														case 229: $filters[5] = 1; break;
														case 425: $filters[4] = 1; break;
														case 224: $filters[3] = 1; break;
														case 231: $filters[6] = 1; break;
														case 233; $filters[7] = 1; break;
														case 15330; $filters[9] = 1; break;
													}

													$_GET["filters"] = $filters;

													getCalendar();
												?>

											</div>
										</div>
										<div class="portlet box <?php echo $color; ?> calendar">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-file-text docBlc"></i> Documents récents
												</div>

											</div>
											<div class="portlet-body light encadres">
												<table class="table encart-docs">
													<thead>
														<tr>
															<th class="first">Titre</th>
															<th class="middle">Date</th>
															<!--th class="last">Taille</th-->
														</tr>
													</thead>
													<tbody>
													<?php


												$args_doc = array(
											    'post_type'		=> 'documents',
												'posts_per_page'	=> 10,
												'meta_query' => array (
														array (
																'key' => 'pages_associes_document',
																'value' => $id_cats_secteur
														)),
														//'meta_key'		=> 'tag_documents',
														'orderby'		=> 'date ', //tag_documents
														'order'			=> 'DESC'


												);
											    $documents= new WP_Query( $args_doc );


											    if ( $documents->have_posts() ) :

											   	 while ( $documents->have_posts() ) : $documents->the_post();

												?>

														<tr class="contenu">
															<td class="first"><?php
																$attachment_id = get_field('pdf_documents',get_the_ID());
																$url="";
																if(isset($attachment_id["url"])) {
																	$url = $attachment_id["url"];
																}

																echo "<a href='" . $url . "' target='_blank'>";
																the_title();
																echo "</a>";

															?></td>
															<td class="middle"><?php echo  get_the_date();?></td>
															<!--td class="last">
																<?php
																	$filename = $url;
																	$headers  = get_headers($filename, 1);
																	$fsize    = $headers['Content-Length'];
																	if($fsize<1024)
																		{
																			echo round($fsize/1024)." octets";
																		}elseif($fsize>=1024)
																			{
																				echo round($fsize/1024)."K";
																			}elseif($fsize>=1048576)
																				{
																					echo round($fsize/1048576)."M";
																				}


																?>
															</td-->
														</tr>
														<?php
															endwhile;
															endif;
															wp_reset_postdata();
															?>
													</tbody>
												</table>
												<a href="<?php echo $_SERVER["REQUEST_URI"]; ?>documents/" class="lienTout">Tous les documents  ></a>
											</div>
										</div>
										<!--div class="portlet box <?php echo $color; ?>">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-desktop"></i> Produits informatiques
												</div>
											</div>
											<div class="portlet-body light encadres">
												<table class="table encart-docs">
													<thead>
														<tr>
															<th class="first">Titre</th>
															<th class="middle">Date</th>
															<th class="last">Taille</th>
														</tr>
													</thead>
													<tbody>
								<?php

												$args_info = array(
													'post_type'		=> 'informatique',
													'posts_per_page'	=> -1,
													'meta_query' => array (
													array (
															'key' => 'pages_associes_info_detail',
															'value' => $id_cats_secteur
													))

											);
											$doc_info= new WP_Query( $args_info );


											if ( $doc_info->have_posts() ) :

											while ( $doc_info->have_posts() ) : $doc_info->the_post();



									?>


														<tr class="contenu">
															<td class="first"><?php the_title();?></td>
															<td class="middle"><?php echo  get_the_date();?></td>
															<td class="last">

															</td>
														</tr>
														<?php
															endwhile;
															endif;
															wp_reset_postdata();
															?>
													</tbody>
												</table>
											<a href="/extranet/direction-de-travail/droit-des-affaires/documents/" class="lienTout">Tous les produits informatiques  ></a>
											</div>
										</div-->
									</div>
									<div class="col-md-6">
										<div class="portlet box <?php echo $color; ?>">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-cogs"></i>Forum <?php the_title(); ?>
												</div>

											</div>
											<div>
											<div id="bbpress-forums">
	<?php

          global $idform;
          $idform=$form_id["sec-".$id_cats_secteur];

    ?>
	<?php //bbp_breadcrumb(); ?>
    <?php //bbp_forum_subscription_link(); ?>

	<?php do_action( 'bbp_template_before_single_forum' ); ?>

	<?php if ( post_password_required() ) : ?>

		<?php //bbp_get_template_part( 'form', 'protected' ); ?>

	<?php else : ?>

		<?php //bbp_single_forum_description(); ?>

		<?php if ( bbp_has_forums() ) : ?>

			<?php //bbp_get_template_part( 'leeroyloop', 'forums' ); ?>

		<?php endif; ?>

		<?php if ( !bbp_is_forum_category() && bbp_has_topics() ) : ?>

			<?php  echo do_shortcode('[bbp-single-forum id=' . $idform . ']'); //bbp_get_template_part( 'loop', 'forums'); ?>

			<?php echo "<div class=\"forum-link\"><a href=\"" . preg_replace("/action-des-collaboratrices/", "forums/forum/action-des-collaboratrices", preg_replace("/direction-de-travail/", "forums/forum", $_SERVER["REQUEST_URI"])) . "\">Participer à ce forum</a></div>";
			//bbp_get_template_part( 'leeroyloop',       'topics'    ); ?>

			<?php //bbp_get_template_part( 'pagination', 'topics'    ); ?>

			<?php //bbp_get_template_part( 'form',       'topic'     ); ?>

		<?php elseif ( !bbp_is_forum_category() ) : ?>

			<?php bbp_get_template_part( 'feedback',   'no-topics' ); ?>

			<?php bbp_get_template_part( 'form',       'topic'     ); ?>

		<?php endif; ?>

	<?php endif; ?>

	<?php do_action( 'bbp_template_after_single_forum' ); ?>

</div>


											</div>
										</div>
                                        <div class="row <?php echo $color; ?>">
                                            <div class="col-md-6 col-sm-6 comptesRendus">
                                                <a class="compte_rendu" href="comptes-rendus/" ><i class="fa fa-file-text"></i></br><span>Procès verbaux</span></a>

                                            </div>
                                            <div class="col-md-6 col-sm-6 ListeMembres">
                                                <a class="liste_membre" href="liste-des-membres/" ><i class="fa fa-user"></i></br><span>Liste des membres</span></a>

                                            </div>
                                        </div>

                                        <?php
    										$content = get_the_content();

    										if (trim($content) != "") {
    									?>
                                        <br />
                                        <div class="portlet box <?php echo $color; ?>">
											<div class="portlet-title">
												<div class="caption">
													<i class="fa fa-external-link"></i>Liens utiles
												</div>
											</div>
											<div>
												<div id="liens-utiles">
			    									<?php
			    										echo preg_replace("/(<a)(\s[^>]*>)/is", "$1 target=\"_blank\" $2", $content);
			    									?>
												</div>
											</div>
										</div>

										<?php } ?>


									</div>
								</div>
							</div>
							<!--div id="tab_1_1_2" class="tab-pane">calendrier</div>
							<div id="tab_1_1_3" class="tab-pane">Nom du responsable :
								<strong>Me Karine Jobin</strong>
							</div>
							<div id="tab_1_1_4" class="tab-pane">
								<div class="col-md-10">
									<h1>PROCÈS-VERBAL DE LA RÉUNION DU COMITÉ DU DROIT DES AFFAIRES TENUE À DRUMMONDVILLE </h1>
									<p><strong>LE 21 FÉVRIER 2014</strong></p>
									<h3>LISTE DES PERSONNES PRÉSENTES :</h3>
									<div class="content">
										<div class="col-md-6">
											Karine Jobin (Sherbrooke)<br>
											Christophe Larocque (Gatineau)<br>
											Éric Tremblay (Lac Etchemin)<br>
											Jacinthe Breton (Ste-Marie)<br>
											Josée Girard (Saguenay)<br>
											Karine Francoeur (Sherbrooke/Fleurimont)<br>
										</div>
										<div class="col-md-6">
											Annie Durocher (Drummondville)<br>
											Claude Rodrigue (St-Georges)<br>
											François Sylvestre (Sherbrooke)<br>
											Jean-Pierre Aubry (Shawinigan)<br>
											Julie Hébert (Montréal/Anjou)<br>
											Mathieu Rainville (Sherbrooke/Fleurimont)<br>
										</div>
									</div>
									<p>&nbsp;</p>
									<p>La réunion est présidée par Me Karine Jobin, présidente du comité</p>
									<h3>1.  NOMINATION DU SECRÉTAIRE</h3>
									<p>Me Jean-Pierre Aubry est nommé à ce poste</p>
									<h3>2.  LECTURE ET ADOPTION DE L'ORDRE DU JOUR</h3>
									<p>L'ordre du jour est adopté à l'unanimité</p>
									<h3>3.  LECTURE ET ADOPTION DU PROCÈS-VERBAL DE LA RÉUNION DU 8 NOVEMBRE 2013</h3>
									<p>Le procès-verbal est adopté à l'unanimité.  Merci à Julie Hébert pour la rédaction du procès-verbal.</p>
								</div>
								<div class="col-md-2">Archives</div>
							</div>
							<div id="tab_1_1_5" class="tab-pane">
								<div class="note note-success">
									<h4 class="block">Mise en garde aux membres du réseau</h4>
										<p>Ce document est le fruit du travail d&rsquo;analyse et de rédaction des membres du Comité de droit des aﬀaires. L&rsquo;utilisation de ce document et du questionnaire qui y est relié devrait vous permettre de réaliser une économie substantielle de temps lors de la préparation d&rsquo;une convention entre actionnaires. En outre, ce document permettra de personnaliser le produit ﬁnal en fonction des besoins de vos clients respectifs.</p>
										<p>Le tarif indicatif minimal recommandé, mais non obligatoire, relativement à un tel produit est d&rsquo;environ neuf cent dollars. Nous vous recommandons, en outre, de recourir à une tariﬁcation horaire dans le cadre de la réalisation d&rsquo;un tel mandat.</p>
										<p>Enﬁn, nous vous invitons à faire parvenir vos commentaires et suggestions à l&rsquo;égard de ce produit à <a href="mailto:info@pmeinter.com">info@pmeinter.com</a></p>
								</div>
								<table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nom</th>
											<th>Dern. Màj</th>
											<th>Taille</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<img id="icon_2" src="http://pmex.leeroy.ca/wp-content/plugins/download-manager/icon/file_extension_doc.png" />
											</td>
											<td>
												<div class="col-md-8">
													<a href='http://pmex.leeroy.ca/?wpdmact=process&did=Mi5ob3RsaW5r'>Business checklist</a>
												</div>
												<div class="col-md-4">
													<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												</div>
											</td>
											<td>--</td>
											<td>--</td>
										</tr>
										<tr>
											<td>
												<img id="icon_1" src="http://pmex.leeroy.ca/wp-content/plugins/download-manager/icon/file_extension_doc.png" />
											</td>
											<td>
												<div class="col-md-8">
													<a href='http://pmex.leeroy.ca/?wpdmact=process&did=MS5ob3RsaW5r'>Résolution du conseil d'administration et des actionnaires, le cas échéant, pour la vente d'actifs d'entreprise 2013</a></div><div class="col-md-4"><a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												</div>
											</td>
											<td>--</td>
											<td>--</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div id="tab_1_1_6" class="tab-pane">
								<div class="note note-success">
									<p>Il est à noter que vous ne retrouverez ici que les nouvelles versions de la barre d&rsquo;outils, les macros, documents&#8230;. testées par FIL Informatique.</p>
									<p>Pour les anciennes versions, si vous devez récupérer des documents, vous devrez communiquer avec FIL Informatique par téléphone (450.530.7373) ou par courriel (<a href="mailto:mp@ﬁlinformatique.com">mp@ﬁlinformatique.com</a>).</p>
									<p><strong>Mise en garde : pour toute assistance concernant l&rsquo;installation et le soutien informatique reliés aux macros et aux assistants de PME INTER Notaires, il est impératif de communiquer avec l&rsquo;équipe de FIL Informatique.</strong></p>
									<p>Pour télécharger un ﬁchier, cliquez sur le lien avec le bouton de droite de la souris et choisissez « Enregistrer la cible sous ».</p>
								</div>
								<table class="table table-striped table-bordered table-hover table-full-width" id="sample_3">
									<thead>
										<tr>
											<th>&nbsp;</th>
											<th>Nom</th>
											<th>Dern. Màj</th>
											<th>Taille</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												<img id="icon_3" src="" />
											</td>
											<td>
												<div class="col-md-8">
													<a href='http://pmex.leeroy.ca/?wpdmact=process&did=My5ob3RsaW5r'>Barre d'outils PME INTER Notaires (requis pour tous les modules)</a>
												</div>
												<div class="col-md-4">
													<a class="btn btn-xs <?php echo $color; ?>" href="javascript:;">Nouveau</a>
												</div>
											</td>
											<td>--</td>
											<td>--</td>
										</tr>
									</tbody>
								</table>
							</div>
							<div id="tab_1_1_7" class="tab-pane">
								<p>Forum</p>
							</div-->
						</div>
					</div>
				</div><!-- .entry-content -->
			</div>
		</div><!-- #post-## -->
	</div>
</div>
<?php
get_footer();
