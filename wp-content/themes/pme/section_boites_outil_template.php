<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 * Template Name: Section Boites outil template
 */

get_header(); ?>

<?php
$color=get_field('color_page',get_the_ID());
$id_section=get_the_ID();
$parentid=wp_get_post_parent_id( $id_section );
$id_cats_secteur=get_field("key",$parentid);
$color=get_field('color_page',$parentid);
$pagelink=get_permalink(get_the_ID() );
$list_dep=array("droit_affaire"=>1,"droit_immobilier"=>4,"droit_personne"=>2,"droit_agricole"=>5,"gestion_bureau"=>3,"dev_affaires"=>6,"action_des_collaboratrices"=>7);
$nom_seteur_membre=array_search($id_cats_secteur, $list_dep);
?>

<div class="row">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
			<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title"><?php the_title();?></h3>
				<ul class="page-breadcrumb breadcrumb">
					<li>
						<i class="fa fa-home"></i>
						<a href="<?php get_home_url(); ?>">Intranet</a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<i class="fa"></i>
						<a href="<?php echo get_permalink($parentid); ?>"><?php echo get_the_title($parentid);?></a>
						<i class="fa fa-angle-right"></i>
					</li>
					<li><?php the_title();?></li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<div class="row">
			<div id="post-81" class="post-81 post type-post status-publish format-standard hentry category-directions-de-travail col-md-12 blog-tag-data boiteOutils">
				<div class="entry-content">
					<div class="tabbable-custom nav-justified">
                        <div class="mobile-dd">
			                <?php include_once 'global_nav.php';?>
                            <div class="mobile-handle">
                                <a class="fa fa-angle-down">
                                </a>
                          </div>
						</div>
						<div class="tab-content">
                            <div id="tab_1_1_8"  style="display: block;" class="tab-pane sousmenu-nav">
                                <ul class="nav nav-tabs nav-justified">
                                    <li class="active drt-affaires">
                                        <a href="#tab_2_1_1" data-toggle="tab">Droit<br /> des affaires</a>
                                    </li>
                                    <li class="drt-perso">
                                        <a href="#tab_2_1_2" data-toggle="tab">Droit</br> de la personne</a>
                                    </li>
                                    <li class="drt-immo">
                                        <a href="#tab_2_1_4" data-toggle="tab">Droit<br /> immobilier</a>
                                    </li>
                                    <li class="drt-agri">
                                        <a href="#tab_2_1_5" data-toggle="tab">Droit<br /> agricole</a>
                                    </li>
                                    <!--li class="drt-bure">
                                        <a href="#tab_2_1_5" data-toggle="tab">Gestion<br /> des bureaux</a>
                                    </li-->
                                    <li class="drt-dev">
                                        <a href="#tab_2_1_6" data-toggle="tab">Général</a>
                                    </li>

                                </ul>
<?php
$arr_doc=array();


foreach($GLOBALS['page_ass'] as $ps=>$pv) {
    $arr_doc[$ps]=array();
    $args = array(
        'post_type' => 'outils',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key' => 'pages_associes_outil',
                'value' => str_replace("s-","",$ps)
            )),
        'orderby' => 'title',
        'order' => 'asc'
    );
    $documents = new WP_Query($args);
//print_r($documents)	;
    if ($documents->have_posts()) :

        while ($documents->have_posts()) : $documents->the_post();

            //print_r(get_the_ID());
            $title = get_the_title();
            $attachment_id = get_field('documents_outils', get_the_ID());
            $url_file = "";
            if (isset($attachment_id["url"])) {
                $url_file = $attachment_id["url"];
            }
            $link = get_field('link_outils', get_the_ID());
            $row_doc = array("title" => $title, "url_file" => $url_file, "link" => $link);
            $arr_doc[$ps][] = $row_doc;
            //print_r($ps);
        endwhile;
//print_r($arr_doc);
    endif;
    wp_reset_postdata();
}

        ?>


                            <?php
                            $i=1;

                            foreach($GLOBALS['page_ass'] as $ps=>$pv) {  ?>
                                <div id="tab_2_1_<? echo $i ?>" class="soustab tab-pane <?php if($i==1) echo "active" ?>">
                                    <div class="col-md-6">
                                        <h4>Documents</h4>
                                        <ul>
                            <?php
                                    foreach($arr_doc[$ps] as $p) {
                                        if (trim($p["url_file"]) != "") {
                            ?>
                                            <li><a href="<?php echo $p["url_file"]; ?>" ><i class="fa fa-file-text"></i><?php echo $p["title"];?></i></a></li>
                            <?php
                                        }
                                    }
                            ?>
                                        </ul>
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Liens utiles</h4>
                                        <ul>
                            <?php
                                   foreach($arr_doc[$ps] as $pp) {
                                        if (trim($pp["link"]) != "") {
                            ?>
                                       <li><a href="<?php echo $pp["link"]?>" ><i class="fa fa-share-square"></i></i><?php echo $pp["title"];?></a></li>
                            <?php
                                        }
                                    }
                            ?>
                                        </ul>
                                    </div>
                                </div>

                            <?php
                                $i++;
                                }
                            ?>
                            </div>

                        </div>
					</div>
				</div><!-- .entry-content -->
			</div>
		</div><!-- #post-## -->
	</div>
</div>
<?php
get_footer();
