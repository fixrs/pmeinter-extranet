<form role="search" action="<?php bloginfo('siteurl'); ?>" class="search-form" method="get">
    <div class="form-container">
		<div class="input-box">
			<a class="remove" href="javascript:;"></a>
			<input class="search-field" name="s" type="text" placeholder="Recherche...">
			<input type="button" value="" class="submit">
		</div>
	</div>
</form>
