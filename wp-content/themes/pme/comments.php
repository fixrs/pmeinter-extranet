<?php
/**
 * The template for displaying Comments
 *
 * The area of the page that contains comments and the comment form.
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

/*
 * If the current post is protected by a password and the visitor has not yet
 * entered the password we will return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>










<hr>

<div class="media blog-page">

	<?php if ( have_comments() ) : ?>

	<h2 class="comments-title">Commentaire(s)</h2>

	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
	<nav id="comment-nav-above" class="navigation comment-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'twentyfourteen' ); ?></h1>
		<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'twentyfourteen' ) ); ?></div>
		<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'twentyfourteen' ) ); ?></div>
	</nav><!-- #comment-nav-above -->
	<?php endif; // Check for comment navigation. ?>


	<?php
		wp_list_comments( array(
			'style'      => 'div',
			'short_ping' => true,
			'avatar_size'=> 34
		) );


		//$comments = get_comments();
		//foreach($comments as $comment) : ?>

			<!--<span class="pull-left"><?php echo get_avatar( $comment->user_id, $size = '54'); ?></span>
			<div class="media-body">
				<h4 class="media-heading"><?php echo $comment->comment_author; ?> <span><?php echo $comment->comment_date; ?> / <a href="#">Modifier</a> / <a href="#">Répondre</a></span></h4>
				<?php echo $comment->comment_content; ?>
			</div>-->

		<?php //endforeach; ?>




	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
	<nav id="comment-nav-below" class="navigation comment-navigation" role="navigation">
		<h1 class="screen-reader-text"><?php _e( 'Comment navigation', 'twentyfourteen' ); ?></h1>
		<div class="nav-previous"><?php previous_comments_link( __( '&larr; Older Comments', 'twentyfourteen' ) ); ?></div>
		<div class="nav-next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'twentyfourteen' ) ); ?></div>
	</nav><!-- #comment-nav-below -->
	<?php endif; // Check for comment navigation. ?>


	<?php if ( ! comments_open() ) : ?>
	<p class="no-comments"><?php _e( 'Comments are closed.', 'twentyfourteen' ); ?></p>
	<?php endif; ?>

	<?php endif; // have_comments() ?>

	<div class="post-comment">
		<?php comment_form(); ?>
	</div>

</div><!-- #comments -->
