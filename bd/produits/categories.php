<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");
		$Skin->assign("errors", $html);
		$Skin->display("produits.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");
		$Skin->assign("errors", $html);
		$Skin->display("produits.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Categorie");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	

	// ------------------
	// LE TRAITEMENT HTML
	// ------------------
	
	$formString = "";
	$successString = "";
	$errorString = "";
	$titreString = "";
	$begin = getorpost('begin');
	$action = getorpost('action');
	$key = getorpost('categorie_key');

	switch ($action) {
		case "add" :
			$titreString = "<h2>Ajouter une cat&eacute;gorie de produit</h2>\n";
			if ($begin) {
				importForm("categories");
				$FormObject = new Form("", "", getForm($action));
				$formString = $FormObject->returnForm();
				$titreString = "<h2>Ajouter une cat&eacute;gorie de produit</h2>\n";

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "produits/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("categories");
					$FormObject = new Form("", getParametersFromPOST($action), getForm($action));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "modify" :
			$titreString = "<h2>Modifier une cat&eacute;gorie de produit</h2>\n";
			if ($begin) {
				importForm("categories");
				$FormObject = new Form("", "", getForm($action));
				$FormObject->setValue("categorie_key", $key);			
				$FormObject->setValue("action", $action);
				$FormObject->setParameters(getAllFieldsCategorie($key));
				$formString = $FormObject->returnFilledForm();

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "produits/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("categories");
					$parameters = getParametersFromPOST($action);
					$FormObject = new Form("", $parameters, getForm($action));
					$FormObject->setValue("categorie_key", $key);
					$FormObject->setValue("action", $action);
					$FormObject->setParameters($parameters);
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "read" :
			$titreString = "<h2>Consulter une cat&eacute;gorie de produit</h2>\n";
			importForm("categories");
			$FormObject = new Form("", "", getForm($action));
			$parameters = getAllFieldsCategorie($key, "read");
			$FormObject->setParameters($parameters);
			$FormObject->setAllHidden();
			$formString = $FormObject->returnFilledForm();			
			break;

	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");
	$Skin->assign("page", "categories");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("titre", $titreString);
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("categories.tpl");

	$DB->close();
	

	// -------------------------------------------
	// LES FONCTIONS POUR LE TRAITEMENT HTML ET DB
	// -------------------------------------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "PME Inter : Administration : Cat&eacute;gorie de produit : Ajout";
				break;
			case "modify":
				$title = "PME Inter : Administration : Cat&eacute;gorie de produit : Modification";
				break;
			case "delete":
				$title = "PME Inter : Administration : Cat&eacute;gorie de produit : Suppression";
				break;
			case "read":
				$title = "PME Inter : Administration : Cat&eacute;gorie de produit : Consultation";
				break;
			case "search":
				$title = "PME Inter : Administration : Cat&eacute;gorie de produit : Recherche";
				break;
			default:
				$title = "PME Inter : Administration : Cat&eacute;gories de produit";
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "modify":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "read":
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "modify":
				break;

			case "modify":
				break;

			case "read":
				break;
		}
		return $html;
	}

	// Fonction qui filtre les POST provenant du formulaire de catégorie
	// Retourne une string html d'erreurs OU retourne "" si aucune erreur n'est détectée
	function validateFormValues($action) {
		$errorString = "";
		global $DB;
		$blnPremiereDate = true;

		switch($action){
			case "add":
				// le nom de la catégorie doit être entré
				if (trim($_POST['nom'])==""){
					$errorString = _error("<a href=\"javascript: showErrorField('nom');\">SVP entrez le nom de la cat&eacute;gorie de produit</a><br />");
				}
				else {
					//si c'est un ajout, on vérifie si le nom de catégorie existe déjà
					if ($_POST['action']=="add"){
						if (verifyIfCategorieNameExists(trim($_POST['nom']))){
							$errorString = _error("Ce nom de cat&eacute;gorie existe d&eacute;j&agrave;, veuillez en entrer un nouveau<br />");			
						}
					}
				}
			break;
			
			case "modify":
				// le nom de la catégorie doit être entré
				if (trim($_POST['nom'])==""){
					$errorString = _error("<a href=\"javascript: showErrorField('nom');\">SVP entrez le nom de la cat&eacute;gorie de produit</a><br />");
				}
				else {
					//si c'est un ajout, on vérifie si le nom de catégorie existe déjà
					if ($_POST['action']=="add"){
						if (verifyIfCategorieNameExists(trim($_POST['nom']))){
							$errorString = _error("Ce nom de cat&eacute;gorie existe d&eacute;j&agrave;, veuillez en entrer un nouveau<br />");			
						}
					}
				}
			break;
		}
		return $errorString;
	}
	
	// Fonction qui met à jour les valeurs de la DB à partir des valeurs de POST provenant du formulaire
	// Retourne une string html d'erreurs OU retourne "" si aucune erreur n'est détectée
	function updateDbValues($action) {
		global $DB;
		$errorString = "";
		
			$Categorie = new Categorie($DB, "", "");
			$Categorie->scanfields();
			
			if ($action=="add") {
				$Categorie->insertmode();
			}
			else if ($action == "modify"){
				$Categorie->setQKey("key", trim($_POST['categorie_key']));			
			}
			$Categorie->set('nom', trim($_POST['nom']));
			$Categorie->set('actif', '1');

			if (!$Categorie->saveAll()) {
				
				if ($action=="add") {
					$errorString = _error("Une erreur s'est produite lors de l'insertion.");			
				}
				else if($action == "modify"){
					$errorString = _error("Une erreur s'est produite lors de la modification.");			
				}		
			}
		return $errorString;		
	}

	// Prend tous les champs d'une catégorie
	// retourne un array des champs de la catégorie
	function getAllFieldsCategorie($key){
		global $DB;

		$Categorie = new Categorie($DB, "", 0);
		$Categorie->scanfields();
		$Categorie->setQKey("key", $key);
		$Categorie->loadAll();
		return $Categorie->getAll();
	}
	
	// Vérifie si le nom de la catégorie existe déjà
	// retourne vrai ou faux	
	function verifyIfCategorieNameExists($name)
	{
		global $DB;
		$blnExists = false;
		$Categorie= new Categorie($DB, "", "");
		$Categorie->scanfields();
		$Categorie->setQKey("nom", $name);
		$Categorie->loadAll();
		
		if ($Categorie->get("nom")==$name && $Categorie->get("actif")=='1'){
			
			$blnExists = true;			
		}
		
		return $blnExists;
	}

	function getParametersFromPOST($action) {
		$parameters = array();
		switch ($action) {
			case "add":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;			
			case "modify":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;	
		}
		return $parameters;
	}
	
?>
