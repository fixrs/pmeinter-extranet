<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");
		$Skin->assign("errors", $html);
		$Skin->display("produits.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");
		$Skin->assign("errors", $html);
		$Skin->display("produits.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.quiboweb.logger.Logger");
	import("com.pmeinter.Produit");
	import("com.pmeinter.Categorie");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	

	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$successString = "";
	$errorString = "";
	$formString = validateJSString();
	$dir_image = "docs/produit_img/";
	$root_image = $BASEPATH . $dir_image;
	$action = getorpost('action');

	// ÉTAPE #1
	// Traitement des GET provenant de la sélection d'une action dans le PREMIER menu $menuString
	switch ($_GET['action']) {
		case "search":
			$formString .= searchProduitsString();
			break;

		case "search_cat":
			$formString .= searchCategoriesString();
			break;
		case "results":
			$formString .= searchResultsProduitsString();
			break;

		case "results_cat":
			$formString .= searchResultsCategoriesString();
			break;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");
	$Skin->assign("page", "produits");
	$Skin->assign("title", pageTitleString($action));
#	$Skin->assign("menu", menuProduitsString($action));
	$Skin->assign("menu_title", menuTitleString($action));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("produits.tpl");

	$DB->close();


	// -----------------
	// LES SOUS-ROUTINES
	// -----------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "search":
				$title = "Recherche de produits";
				break;
			case "search_cat":
				$title = "Recherche de cat&eacute;gories de produits";
				break;
			case "results":
				$title = "R&eacute;sultats de recherche de cat&eacute;gories de produits";
				break;
	
			case "results_cat":
				$title = "R&eacute;sultats de recherche de cat&eacute;gories de produits";
				break;
			default:
				$title = "Recherche de produits";
		}
		return $title;
	}

	// Fonction qui retourne le html pour le titre du sous-menu
	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "search":
				$html .= "<h1>Rechercher des produits</h1>\n";
				break;
			case "search_cat":
				$html .= "<h1>Rechercher des cat&eacute;gories de produits</h1>\n";
				break;
			case "results":
				$html .= "<h1>Rechercher des produits</h1>\n";
				break;
			case "results_cat":
				$html .= "<h1>Rechercher des cat&eacute;gories de produits</h1>\n";
				break;
		}
		return $html;
	}


	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function searchProduitsString() { 
		$motCle = "";
		$identificationReseau = "";
		$categorieKey = "";
		if (isset($_GET['search_produits'])&& trim($_GET['search_produits'])!=""){
			$motCle = trim($_GET['search_produits']);
		}
		if (isset($_GET['search_produits_reseau']) && trim($_GET['search_produits_reseau'])!=""){
			$identificationReseau = " checked=\"checked\" ";
		}
		if (isset($_GET['categories_list'])&& trim($_GET['categories_list'])!="-1" && trim($_GET['categories_list'])!=""){
			$categorieKey = trim($_GET['categories_list']);
		}

		$html =
			"<div class=\"form\">\n".
//			"	<form id=\"produits_recherche\" action=\"search.php?action=results\" method=\"get\" onsubmit=\"return validateForm('search_produits')\">\n".
			"	<form id=\"produits_recherche\" action=\"search.php?action=results\" method=\"get\">\n".
			"		<input type=\"hidden\" id=\"action\" name=\"action\" value=\"results\" />".
			"		<table class=\"searchTable\">\n" .
			"			<tr>\n".
			"				<td>Mot(s)-cl&eacute;(s) :</td> \n" .
			"		 		<td><input type=\"text\" id=\"search_produits\" name=\"search_produits\" value=\"".$motCle."\" /><td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Est-ce un produit <br/>d'identification au r&eacute;seau</td>" .
			"				<td><input type=\"checkbox\" id=\"search_produits_reseau\" name=\"search_produits_reseau\" ".$identificationReseau."value=\"oui\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Recherche par cat&eacute;gorie de produits</td>\n".
			"				<td><select id=\"categories_list\" name=\"categories_list\">\n".
			"						<option value=\"-1\">- Cat&eacute;gories de produits -</option>\n";
		$html .= getCategoriesOptionsString($categorieKey);
		$html .=
			"					</select></td>\n".
			"			</tr>\n".
			"		</table>\n".
			"		<br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>".
			"<br /><br />\n";
		return $html;
	}

	function getCategoriesOptionsString($categorie_key=""){
		global $DB;
		$html = "";
		$DB->query(
			"SELECT `key`, `nom` ".
			"FROM `produits_categories` ".
			"WHERE `actif`='1' ".
			"ORDER BY `nom` ASC"
		);
		while($DB->next_record()) {
			$selected = "";
			if ($categorie_key!="" && $categorie_key==$DB->getField("key")){
				$selected = "selected=\"selected\" ";
			}
			$html .= "<option ".$selected."value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . "</option>\n";
		}
		return $html;
	}
	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function searchCategoriesString() { 
		$motCle = "";
		if (isset($_GET['search_categories']) && trim($_GET['search_categories'])!="" ){
			$motCle = trim($_GET['search_categories']);
		}
		$html =
			"<div class=\"form\">\n".
//			"	<form id=\"produits_recherche\" action=\"search.php?action=results_cat\" method=\"get\" onsubmit=\"return validateForm('search_categories')\">\n" .
			"	<form id=\"produits_recherche\" action=\"search.php?action=results_cat\" method=\"get\">\n" .
			"		<input type=\"hidden\" id=\"action\" name=\"action\" value=\"results_cat\" />".
			"		Mot(s)-cl&eacute;(s) : <input type=\"text\" id=\"search_categories\" name=\"search_categories\" value=\"".$motCle ."\" />\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>".
			"<br /><br />\n";
		return $html;
	}

	function getProduitsOptionsString(){
		global $DB;
		$html = "";
		$DB->query(
			"SELECT `produits`.`key`, `produits`.`nom`, `produits`.`produits_categories_key`, ".
			"`produits_categories`.`nom` AS `nom_categorie` ".
			"FROM `produits`, `produits_categories` ".
			"WHERE `produits`.`produits_categories_key`=`produits_categories`.`key` ".
			"AND `produits`.`actif`='1' ".
			"ORDER BY `produits`.`nom` ASC"
		);
		while($DB->next_record()) {
			$html .= "<option value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . " - ". $DB->getField("nom_categorie") ." </option>\n";
		}
		return $html;
	}

	function searchResultsCategoriesString() {
		global $DB;
		$html = searchCategoriesString();
		$nb = 0;
		$query = "";
			
		if (trim($_GET["search_categories"]) != "") {
			$terms = split(" ", trim($_GET["search_categories"]));
			$query = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
				$query .= "AND `nom` LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		$html .=
			"<h2>R&eacute;sultats de recherche des cat&eacute;gories de produits</h2>\n";
		$compteur = 0;

		$DB->query(
				"SELECT * ".
				"FROM `produits_categories` ".
				"WHERE `actif` = '1' ".
				(trim($_GET["search_categories"]) != "" ? $query : "").
				"ORDER BY `nom` ASC "
			);
		$nb = $DB->getNumRows();

		if ($nb>0){
			$html .=
				"<table class=\"resultsTable\">\n" .
				"	<tr>\n".
				"	<th>Nom de la cat&eacute;gorie</th><th>&nbsp;</th>\n".
				"	</tr>\n";
		}
		
		while ($DB->next_record()) {
			$key = $DB->getField("key");
			$color = "";
			if ($compteur%2==0) {
				$color = " class=\"altTr\"";			
			}
			$html .=
			"    <tr". $color .">\n<td><a href=\"categories.php?action=read&begin=1&categorie_key=" . $DB->getField("key") . "\" title=\"Pour visualiser\">".$DB->getField("nom")."</a> &nbsp;</td><td><a href=\"categories.php?action=read&begin=1&categorie_key=" . $DB->getField("key") . "\" title=\"Pour visualiser\">Visualiser</a> | <a href=\"categories.php?action=modify&begin=1&categorie_key=" . $DB->getField("key") . "\" title=\"Pour modifier\">Modifier</a></td>" .
			"	</tr>\n";
			$compteur++;
		}
		if ($nb==0) {
			$html .= "<p>Aucune cat&eacute;gorie ne correspond &agrave; votre requ&ecirc;te...</p>";
		}
		else if ($nb>0) {
			$html .=
				"</table>";			
		}
	
		return $html;
	}


	function searchResultsProduitsString() {
		global $DB;
		$html = searchProduitsString();
		$nb = 0;
		$query = "";
			
		if (trim($_GET["search_produits"]) != "") {
			$terms = split(" ", trim($_GET["search_produits"]));
			$query = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$query .= "AND CONCAT_WS(' ', `produits` .`nom` , if( `produits` .`description` IS NULL , NULL , `produits` .`description` ) , if( `produits` .`numero_produit` IS NULL , NULL , `produits` .`numero_produit` ), `produits_categories`.`nom` ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		$sql = "";
		$sqlCategorieKey = "";
		if (isset($_GET['categories_list']) && $_GET['categories_list']!="-1"){
			$sqlCategorieKey =				
					"AND `produits`.`produits_categories_key` =  '".$_GET['categories_list']."'";
		}
		
		if (isset($_GET['search_produits_reseau']) && $_GET['search_produits_reseau']=="oui"){
			$sql = 
				"SELECT `produits`.*, `produits_categories`.`nom` AS `categories_nom` ".
				"FROM `produits`, `produits_categories` ".
				"WHERE `produits`.`actif` = '1' ".
				"AND `produits`.`produits_categories_key` = `produits_categories`.`key` ".
				"AND `produits_categories`.`actif` = '1' ".
				"AND `produits`.`identification_reseau` = '1' ";
			$sql .= $sqlCategorieKey;
			$sql .=				
				(trim($_GET["search_produits"]) != "" ? $query : "").
				"ORDER BY `produits`.`nom` ASC ";
		}
		else {
			$sql = 
				"SELECT `produits`.*, `produits_categories`.`nom` AS `categories_nom` ".
				"FROM `produits`, `produits_categories` ".
				"WHERE `produits`.`actif` = '1' ".
				"AND `produits`.`produits_categories_key` = `produits_categories`.`key` ".
				"AND `produits_categories`.`actif` = '1' ";
			$sql .= $sqlCategorieKey;
			$sql .=				
				(trim($_GET["search_produits"]) != "" ? $query : "").
				"ORDER BY `produits`.`nom` ASC ";	
		}

		$DB->query(
				$sql
			);
			
		$nb = $DB->getNumRows();
		$html .=
				"<h2>R&eacute;sultats de recherche de produits</h2>\n";
		if ($nb>0){
			$html .=
				"<table class=\"resultsTable\">\n" .
				"	<tr>\n".
				"	<th>Nom du produit</th><th>Cat&eacute;gorie</th><th>&nbsp;</th>\n".
				"	</tr>\n";
		}
		$compteur = 0;
		
		while ($DB->next_record()) {
			$key = $DB->getField("key");
			$color = "";
			if ($compteur%2==0) {
				$color = " class=\"altTr\"";			
			}
			$html .=
			"    <tr". $color .">\n<td><a href=\"produits.php?action=read&begin=1&produit_key=" . $DB->getField("key") . "\" title=\"Pour visualiser\">".$DB->getField("nom")."</a><td>".$DB->getField("categories_nom")."</td><td><a href=\"produits.php?action=read&begin=1&produit_key=" . $DB->getField("key") . "\" title=\"Pour visualiser\">Visualiser</a> | <a href=\"produits.php?action=modify&begin=1&produit_key=" . $DB->getField("key") . "\" title=\"Pour modifier\">Modifier</a></td></tr>\n";
			$compteur++;
		}
		if ($nb==0) {
			$html .= "<p>Aucun produit ne correspond &agrave; votre requ&ecirc;te...</p>";
		}
		else if ($nb>0) {
			$html .=
				"</table>";			
		}
	
		return $html;

	}

	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function validateJSString() { 
		$html =
			"<script type=\"text/javascript\" language=\"javascript\">\n" .
			"	String.prototype.trim = function(){ \n" .
			"		return this.replace(/^\s+|\s+$/g,'');\n" .
			"		}\n".
			
			"	function validateForm(nomChamp) {\n".
//			"			alert('Validation');\n" .
			"		var champ = document.getElementById(nomChamp);\n".
			"		var champValue = champ.value;\n".
			"		var blnOk = false;\n".
			"		if (champValue.trim()!='' && champValue!='-1'){\n".
			"			blnOk = true;\n".
			"		} \n" .
			"		else if (champValue=='-1'){\n".
			"			var nomSection = 'une catégorie';\n".
			"			if (nomChamp=='produit_key'){\n".
			"				nomSection = 'un produit';\n" .
			"			} \n" .
			"			alert('Vous devez sélectionner '+nomSection);\n" .
			"			champ.focus();\n" .
			"		} \n" .
			"		else { \n" .
			"			if (nomChamp=='search_produits'){\n".
			"				if (champValue.trim()==''){\n" .
			"					var produitReseau = document.getElementById('ignore_search_produits_reseau');\n".
			"					var categorieList = document.getElementById('categories_list');\n".
			"					if (!produitReseau.checked && categorieList.value=='-1'){\n" .
			"						alert('Vous devez soit entré un mot-clé,\\nsoit cocher la case du produit d\'identification au réseau, \\nsoit choisir une catégorie de produits.'); \n" .
			"					} \n" .
			"					else { \n" .
			"						blnOk = true;\n".
			"					} \n" .
			"				} \n" .
			"			} \n" .
			"			else {\n".
			"				alert('Vous devez entrer un mot-clé');\n" .
			"				champ.focus();\n" .
			"			} \n" .
			"		} \n" .
			"		return blnOk;\n" .

			"	}\n".
			"</script>\n" .
			"\n";
		return $html;
	}
	
?>
