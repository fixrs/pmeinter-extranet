<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");
		$Skin->assign("errors", $html);
		$Skin->display("produits.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");
		$Skin->assign("errors", $html);
		$Skin->display("produits.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Produit");
	import("com.pmeinter.Categorie");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------
	
	$formString = "";
	$successString = "";
	$errorString = "";
	$titreString = "";
	$begin = getorpost('begin');
	$action = getorpost('action');
	$key = getorpost('produit_key');
	$dir_image = "docs/produits_img/";
	$root_image = $BASEPATH . $dir_image;
	$debut_image_url = "<img src=\"" . $BASEURL . $dir_image;

	switch ($action) {
		case "add" :
			$titreString = "<h2>Ajouter un produit</h2>\n";
			if ($begin) {
				importForm("produits");
				$FormObject = new Form("", "", getForm($action));
				$formString = $FormObject->returnForm();
				$titreString = "<h2>Ajouter un produit</h2>\n";

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "produits/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("produits");
					$FormObject = new Form("", getParametersFromPOST($action), getForm($action));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "modify" :
			$fieldImageValue = getFieldProduitFromDB($key, 'url_image');
			$image_url = $debut_image_url.$fieldImageValue."\" />";
			$titreString = "<h2>Modifier un produit</h2>\n";
			if ($begin) {
				importForm("produits");
				$FormObject = new Form("", "", getForm($action));
				$FormObject->setValue("produit_key", $key);			
				$FormObject->setValue("action", $action);
				$FormObject->setValue("MAX_FILE_SIZE", "100000");
				if ($fieldImageValue!=""){
					$FormObject->setHiddenValue("url_image", $image_url);
					$FormObject->setHiddenValue("delete_image", createFormSectionDeleteImage());
				}
				$FormObject->setParameters(getAllFieldsProduitFromDB($key, "image"));
				$formString = $FormObject->returnFilledForm();

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "produits/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					$parameters = getParametersFromPOST($action);
					importForm("produits");
					$FormObject = new Form("", $parameters, getForm($action));
					$FormObject->setValue("produit_key", $key);
					$FormObject->setValue("action", $action);
					$FormObject->setValue("MAX_FILE_SIZE", "100000");
					if ($fieldImageValue!=""){
						$FormObject->setHiddenValue("url_image", $image_url);
						$FormObject->setHiddenValue("delete_image", createFormSectionDeleteImage());
					}
					$FormObject->setParameters($parameters);
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "read" :
			$titreString = "<h2>Consulter un produit</h2>\n";
			$fieldImageValue = getFieldProduitFromDB($key, 'url_image');
			$image_url = $debut_image_url.$fieldImageValue."\" />";

			importForm("produits");
			$FormObject = new Form("", "", getForm($action));
			$parameters = getAllFieldsProduitFromDB($key, "read");
			if ($fieldImageValue!=""){
				$FormObject->setHiddenValue("url_image", $image_url);			
			}
			$FormObject->setParameters($parameters);
			$FormObject->setAllHidden();
			$formString = $FormObject->returnFilledForm();			
			break;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");
	$Skin->assign("page", "produits");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("titre", $titreString);
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("produits.tpl");

	$DB->close();
	

	// -------------------------------------------
	// LES FONCTIONS POUR LE TRAITEMENT HTML ET DB
	// -------------------------------------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "PME Inter : Administration : Produits : Ajout";
				break;
			case "modify":
				$title = "PME Inter : Administration : Produits : Modification";
				break;
			case "delete":
				$title = "PME Inter : Administration : Produits : Suppression";
				break;
			case "read":
				$title = "PME Inter : Administration : Produits : Consultation";
				break;
			case "search":
				$title = "PME Inter : Administration : Produits : Recherche";
				break;
			default:
				$title = "PME Inter : Administration : Produits";
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "modify":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "read":
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "modify":
				break;

			case "modify":
				break;

			case "read":
				break;
		}
		return $html;
	}


	// Fonction qui filtre les POST provenant d'un formulaire d'évaluation
	// Retourne une string html d'erreurs OU retourne "" si aucune n'est détectée
	function validateFormValues($action) {
		$errorString = "";
		global $DB;

		switch($action){
			case "add":
				// le nom de l'événement doit être entré
				if (trim($_POST['nom'])==""){
					$errorString = _error("<a href=\"javascript: showErrorField('nom');\">SVP entrez le nom du produit</a><br />");
				}
				//la date de début doit être entrée
				if (trim($_POST['produits_categories_key'])=="0"){
					$errorString .= _error("<a href=\"javascript: showErrorField('produits_categories_key');\">SVP choisir la cat&eacute;gorie de produit</a><br />");
				}
		
				//si la date de fin a été entrée, on vérifie le format
				if (trim($_POST['prix'])!=""){
					if (numero_is_numeric(trim($_POST['prix']))!=1){
						$errorString .= _error("<a href=\"#prix_v\" onclick=\"javascript: showErrorField('prix');\">Le prix doit &ecirc;tre un nombre</a><br />");			
					}			
				}
		
			break;
			
			case "modify":
				// le nom de l'événement doit être entré
				// le nom de l'événement doit être entré
				if (trim($_POST['nom'])==""){
					$errorString = _error("<a href=\"javascript: showErrorField('nom');\">SVP entrez le nom du produit</a><br />");
				}
				//la date de début doit être entrée
				if (trim($_POST['produits_categories_key'])=="0"){
					$errorString .= _error("<a href=\"javascript: showErrorField('produits_categories_key');\">SVP choisir la cat&eacute;gorie de produit</a><br />");
				}
		
				//si la date de fin a été entrée, on vérifie le format
				if (trim($_POST['prix'])!=""){
					if (numero_is_numeric(trim($_POST['prix']))!=1){
						$errorString .= _error("<a href=\"#prix_v\" onclick=\"javascript: showErrorField('prix');\">Le prix doit &ecirc;tre un nombre</a><br />");			
					}			
				}
		
			break;
		}
		
		return $errorString;
	}
	
	// Fonction qui met à jour les valeurs dela DB à partir des valeurs de POST provenant du formulaire
	// Retourne une string html d'erreurs OU retourne "" si aucune n'est détectée
	function updateDbValues($action) {
		global $DB;
		global $root_image;
		$errorString = "";
		$old_image_value = getFieldProduitFromDB($_POST['produit_key'], 'url_image');
		
		$Produit = new Produit($DB, "", "");
		
		$Produit->scanfields();
		
		if ($action == "modify"){
			$Produit->setQKey("key", $_POST['produit_key']);
			$Produit->getFieldsArray();
			if (isset($_POST['delete_image']) && $_POST['delete_image']=="1"){
					deleteProduitImageFromDirectory($root_image.$old_image_value);
					$Produit->set("url_image", '');
			}
			
		}

		if ($action=="add") {
			$Produit->insertmode();
		}

		$Produit->set("key", trim($_POST['key']));
		$Produit->set("nom", trim($_POST['nom']));
		$Produit->set("produits_categories_key", trim($_POST['produits_categories_key']));		
		$Produit->set("prix", trim($_POST['prix']));
		$Produit->set("description", trim($_POST['description']));
		$Produit->set("url_image", '');
		$Produit->set("numero_produit", trim($_POST['numero_produit']));
		$Produit->set("actif", '1');
		$Produit->set("identification_reseau", trim($_POST['identification_reseau']));
	

		//$Produit->setFieldsArray($_POST);

		$resultat = $Produit->saveAll();
		if (!$resultat) {
			if ($action=="add"){
				$errorString .= _error("Une erreur s'est produite lors de l'insertion.");
			}
			else {
				$errorString .= _error("Une erreur s'est produite lors de la modification.");
				
			}
		}
		else {
			
			if (($_FILES["url_image"]["size"] > 0) && !(isset($_POST['delete_image']))){
				
				if($action=="add"){
					$resultat = $Produit->getInsertId();
				}
				else if($action=="modify"){
					$resultat = $_POST['produit_key'];
				}
				if ($old_image_value!="" && $action == "modify"){
					deleteProduitImageFromDirectory($root_image.$old_image_value);
				}
				$fileName = $_FILES['url_image']['name'];
				$pos = strrpos($fileName, ".");
				$extension = "";
				if($pos === false) { 
					return false;
				}
				else {
					$extension = strtolower(substr($fileName, $pos));
					if ($extension == ".gif" || $extension == ".png" || $extension == ".jpg" || $extension == ".jpeg"){
						$name = "produit_img_";
						$name .= $resultat.$extension;
						$target_path = $root_image.$name;
		
						if(!move_uploaded_file($_FILES['url_image']['tmp_name'], $target_path)) {
							$errorString .= _error("Une erreur s'est produite lors de la sauvegarde de l'image.");							    
						}
						else {
							if ($action=="add") {
								$Produit->setQKey('key', $resultat);
							}
							$Produit->set('url_image', $name);
							$Produit->save('url_image');
						}
					}
				}
			}
		}
				
		return $errorString;		
	}

	/**
	* function
	*
	* @access public
	* @param primary key value
	* 
	* @return array 
	*/		
	
	function getAllFieldsProduitFromDB($produit_key, $read=""){
		global $DB;
		
		$arrayFields = "";

		$Produit = new Produit($DB, "", 0);
		$Produit->scanfields();
		$Produit->setQKey("key", $produit_key);
		$Produit->loadAll();

		if ($Produit->get("prix")=="0.00"){
			$Produit->set("prix", "");
		}

		if ($read=="read"){

			if ($Produit->get("prix")!=""){
				$prix = $Produit->get("prix");
				$Produit->set("prix", $prix." $");
			}
			if ($Produit->get("identification_reseau")=="1"){
				$Produit->set("identification_reseau", "Oui");
			}
			else{
				$Produit->set("identification_reseau", "Non");
			}
			$categorie = getFieldCategorieName($Produit->get("produits_categories_key"));
			$Produit->set("produits_categories_key", $categorie);
		}

		return $Produit->getAll();
	}

	/**
	* function
	*
	* @access public
	* @param primary key value and the field name
	* 
	* @return value 
	*/		
	
	function getFieldProduitFromDB($produit_key, $field){
		global $DB;

		$Produit = new Produit($DB, "", 0);
		$Produit->scanfields();
		$Produit->setQKey("key", $produit_key);
		$Produit->loadAll();
		return $Produit->get($field);
	}


	/**
	* function
	* 
	* @access public
	* @param number value
	* 
	* @return 1 or empty
	*/		
	
	function numero_is_numeric($value)
	{
		return (preg_match ("/^(-){0,1}([0-9]+)(,[0-9][0-9][0-9])*([.][0-9]){0,1}([0-9]*)$/", $value) == 1);
	}
	
	function getCategoriesProduitsSelect($tab_index){
		global $DB;
		$html =	
			"		<select name=\"produits_categories_key\" id=\"produits_categories_key\" tabindex=\"".$tab_index."\">\n".
			"			<option id=\"produits_categories_key\" value=\"0\">- Choisir une cat&eacute;gorie de produit -</option>\n";
		$DB->query(
			"SELECT `key`, `nom` ".
			"FROM `produits_categories` ".
			"WHERE `actif`='1'".
			"ORDER BY `nom` ASC"
		);
		while($DB->next_record()) {
			$html .= "<option id=\"produits_categories_key\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . "</option>\n";
		}
		$html .=
			"		</select>\n";
			
		return $html;
		
	}
	
	/**
	* function
	*
	* @access public
	* @param primary key value and the field name
	* 
	* @return value 
	*/		
	
	function getFieldCategorieName($categorie_key){
		global $DB;

		$Categorie = new Categorie($DB, "", 0);
		$Categorie->scanfields();
		$Categorie->setQKey("key", $categorie_key);
		$Categorie->loadAll();
		return $Categorie->get("nom");
	}
	
	/**
	* function
	*
	* @access public
	* @param full path (with file name)
	* 
	* @return true or false
	*/		
	
	function deleteProduitImageFromDirectory($full_path){
		return unlink($full_path);
	}
	
	function createFormSectionDeleteImage(){
		$html = 	"		<div class=\"label radio\">\n".
					"			<label for=\"delete_image\">Cocher ici si vous ne d&eacute;sirez plus d'image avec ce produit&nbsp;: </label>\n".
					"		</div>\n".
					"		<div class=\"field radio\">\n".
					"			<input type=\"checkbox\" name=\"delete_image\" value=\"1\" />\n".
					"		</div>\n<br/><br/>";
		return $html;
	}
	
	function getParametersFromPOST($action) {
		$parameters = array();
		switch ($action) {
			case "add":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;			
			case "modify":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;	
		}
		return $parameters;
	}

	
?>
