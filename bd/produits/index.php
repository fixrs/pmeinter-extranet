<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");
		$Skin->assign("errors", $html);
		$Skin->display("produits.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");
		$Skin->assign("errors", $html);
		$Skin->display("produits.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.quiboweb.logger.Logger");
	import("com.pmeinter.Produit");
	import("com.pmeinter.Categorie");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	

	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$successString = "";
	$errorString = "";
	$formString = validateJSString();
	$dir_image = "docs/produit_img/";
	$root_image = $BASEPATH . $dir_image;

	// ÉTAPE #1
	// Traitement des GET provenant de la sélection d'une action dans le PREMIER menu $menuString
	switch ($_GET['action']) {
		case "add":
			// Redirection vers la page du formulaire pour ajouter un produit
			header("Location: produits.php?action=add&begin=1&produit_key=" . $produit_key);
			exit;

		case "add_cat":
			// Redirection vers la page du formulaire pour ajouter une catégorie
			header("Location: categories.php?action=add&begin=1&categorie_key=" . $cat_key);
			exit;

		case "modify":
			$formString .= modifyProduitsString();
			break;

		case "modify_cat":
			$formString .= modifyCategoriesString();
			break;

		case "delete":
			$formString .= deleteProduitsString();
			break;

		case "delete_cat":
			$formString .= deleteCategoriesString();
			break;

		case "read":
			$formString .= readProduitsString();
			break;

		case "read_cat":
			$formString .= readCategoriesString();
			break;
	}

	// ÉTAPE #2
	// Traitement des POST provenant de la sélection d'un item sélectionné dans un dropdown APRES le menu $menuString
	switch ($_POST['action']) {
		case "modify":
			// Si aucun produit sélectionné, Alors on réaffiche
			if ($_POST['produit_key'] == "-1") {
				$formString .= modifyProduitsString();
				$errorString .= _error("Veuillez choisir un produit dans la liste");
			// Autrement on redirige vers produit.php
			} else {
				header("Location: produits.php?action=modify&begin=1&produit_key=" . $_POST['produit_key'] . "");
				exit;
			}
			break;

		case "modify_cat":
			// Si aucun produit sélectionné, Alors on réaffiche
			if ($_POST['categorie_key'] == "-1") {
				$formString .= modifyCategoriesString();
				$errorString .= _error("Veuillez choisir une cat&eacute;gorie de produit dans la liste");
			// Autrement on redirige vers categories.php
			} else {
				header("Location: categories.php?action=modify&begin=1&categorie_key=" . $_POST['categorie_key'] . "");
				exit;
			}
			break;

		case "delete":
			// Si aucun produit sélectionné, Alors on réaffiche
			if ($_POST['produit_key'] == "-1") {
				$formString .= deleteProduitsString();
				$errorString .= _error("Veuillez choisir un produit dans la liste");
			// Autrement on procède à la suppression
			} else {
					deleteProduitFromDB($_POST['produit_key']);
					$formString .= deleteProduitsString();
					$successString = _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s");
			}
			break;

		case "delete_cat":
			// Si aucun produit sélectionné, Alors on réaffiche
			if ($_POST['categorie_key'] == "-1") {
				$formString .= deleteCategoriesString();
				$errorString .= _error("Veuillez choisir une cat&eacute;gorie de produit dans la liste");
			// Autrement on procède à la suppression
			} else {
				if (!verifyProduitsLinkedToCategorie($_POST['categorie_key'])){
					deleteCategorieFromDB($_POST['categorie_key']);
					$formString .= deleteCategoriesString();
					$successString = _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s");
				}
				else {
					$formString .= deleteCategoriesString();
					$errorString .= _error("Vous devez supprimer tous les produits d'une cat&eacute;gorie avant de pouvoir supprimer la cat&eacute;gorie.");				
				}
			}
			break;

		case "read":
			// Si aucun produit sélectionné, Alors on réaffiche
			if ($_POST['produit_key'] == "-1") {
				$formString .= readProduitsString();
				$errorString .= _error("Veuillez choisir un produit dans la liste");
			// Autrement on redirige vers produits.php
			} else {
				header("Location: produits.php?action=read&begin=1&produit_key=" . $_POST['produit_key'] . "");
				exit;
			}
			break;

		case "read_cat":
			// Si aucun produit sélectionné, Alors on réaffiche
			if ($_POST['categorie_key'] == "-1") {
				$formString .= readCategoriesString();
				$errorString .= _error("Veuillez choisir une cat&eacute;gorie de produit dans la liste");
			// Autrement on redirige vers produits.php
			} else {
				header("Location: categories.php?action=read&begin=1&categorie_key=" . $_POST['categorie_key'] . "");
				exit;
			}
			break;

	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "produits");	
	$Skin->assign("page", "produits");
	$Skin->assign("title", pageTitleString(getorpost('action')));
	$Skin->assign("menu", menuProduitsString());
	$Skin->assign("menu_title", menuTitleString(getorpost('action')));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("produits.tpl");

	$DB->close();


	// -----------------
	// LES SOUS-ROUTINES
	// -----------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "Ajout de produits";
				break;
			case "add_cat":
				$title = "Ajout de cat&eacute;gories de produits";
				break;
			case "modify":
				$title = "Modification de produits";
				break;
			case "modify_cat":
				$title = "Modification de cat&eacute;gories de produits";
				break;
			case "delete":
				$title = "Suppression de produits";
				break;
			case "delete_cat":
				$title = "Suppression de cat&eacute;gories de produits";
				break;
			case "read":
				$title = "Consultation de produits";
				break;
			case "read_cat":
				$title = "Consultation de cat&eacute;gories de produits";
				break;
			default:
				$title = "Gestion des produits";
		}
		return $title;
	}

	// Fonction qui retourne le html pour le menu d'actions des produits
	function menuProduitsString() { 
		$html = "<h1>Gestions des produits</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n";
		$blnCategorieExists = verifyCategorieExists();
		$blnProduitExists = verifyProduitExists();
		if ($blnCategorieExists){
			$html .= 
				"		<li><a href=\"index.php?action=add\">Ajouter un produit</a></li>\n";
			if ($blnProduitExists){
				$html .= 
					"		<li><a href=\"index.php?action=modify\">Modifier un produit</a></li>\n".
					"		<li><a href=\"index.php?action=delete\">Supprimer un produit</a></li>\n".
					"		<li><a href=\"index.php?action=read\">Consulter un produit</a></li>\n".
					"		<li><a href=\"search.php?action=search\">Rechercher des produits</a></li>\n";
			}
			
		}
		else {
			$html .= 
				"		<li>Pour ajouter ou modifier un produit, vous devez cr&eacute;er une cat&eacute;gorie</li>\n";
		}
		$html .=
			"	</ul>\n".
			"</div>\n".			
			"<div class=\"menuIn\">\n".
			"	<h1>Cat&eacute;gories</h1>\n".
			"	<ul>\n".
			"		<li><a href=\"index.php?action=add_cat\">Ajouter une cat&eacute;gorie de produit</a></li>\n";
			if ($blnCategorieExists){
				$html .=
					"		<li><a href=\"index.php?action=modify_cat\">Modifier une cat&eacute;gorie de produit</a></li>\n".
					"		<li><a href=\"index.php?action=delete_cat\">Supprimer une cat&eacute;gorie de produit</a></li>\n".
					"		<li><a href=\"index.php?action=read_cat\">Consulter une cat&eacute;gorie de produit</a></li>\n".
					"		<li><a href=\"search.php?action=search_cat\">Rechercher des cat&eacute;gories de produit</a></li>\n";
			}
		$html .=
			"	</ul>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le titre du sous-menu
	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "modify":
				$html .= "<h2>Modifier un produit</h2>\n";
				break;
			case "modify_cat":
				$html .= "<h2>Modifier une cat&eacute;gorie de produits</h2>\n";
				break;
				
			case "delete":
				$html .= "<h2>Supprimer un produit</h2>\n";
				break;
			case "delete_cat":
				$html .= "<h2>Supprimer une cat&eacute;gorie de produits</h2>\n";
				break;
			case "read":
				$html .= "<h2>Consulter un produit</h2>\n";
				break;
			case "read_cat":
				$html .= "<h2>Consulter une cat&eacute;gorie de produits</h2>\n";
				break;
		}
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'un produit à modifier
	function modifyProduitsString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\" onsubmit=\"return validateForm('produit_key');\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"		<select id=\"produit_key\" name=\"produit_key\">\n".
			"			<option value=\"-1\">- Choisir un produit -</option>\n";
		$html .=	getProduitsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'un produit à consulter
	function readProduitsString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\" onsubmit=\"return validateForm('produit_key');\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"		<select id=\"produit_key\" name=\"produit_key\">\n".
			"			<option value=\"-1\">- Choisir un produit -</option>\n";
		$html .=	getProduitsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

		// Fonction qui retourne le html pour le dropdown de la sélection d'un produit à supprimer
	function deleteProduitsString() {
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\" onsubmit=\"return validateForm('produit_key');\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"		<select id=\"produit_key\" name=\"produit_key\">\n".
			"			<option value=\"-1\">- Choisir un produit -</option>\n";
		$html .=	getProduitsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Supprimer\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

// Fonction qui retourne le html pour le dropdown de la sélection d'une catégorie à modifier
	function modifyCategoriesString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\" onsubmit=\"return validateForm('categorie_key');\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"modify_cat\" />\n".
			"		<select id=\"categorie_key\" name=\"categorie_key\">\n".
			"			<option value=\"-1\">- Choisir une cat&eacute;gorie de produit -</option>\n";
		$html .= getCategoriesOptionsString();
		$html .=
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

// Fonction qui retourne le html pour le dropdown de la sélection d'une catégorie à modifier
	function readCategoriesString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\" onsubmit=\"return validateForm('categorie_key');\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"read_cat\" />\n".
			"		<select id=\"categorie_key\" name=\"categorie_key\">\n".
			"			<option value=\"-1\">- Choisir une cat&eacute;gorie de produit -</option>\n";
		$html .= getCategoriesOptionsString();
		$html .=
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}



		// Fonction qui retourne le html pour le dropdown de la sélection d'une catégorie à supprimer
	function deleteCategoriesString() {
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\" onsubmit=\"return validateForm('categorie_key');\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"delete_cat\" />\n".
			"		<select id=\"categorie_key\" name=\"categorie_key\">\n".
			"			<option value=\"-1\">- Choisir une cat&eacute;gorie de produit -</option>\n";
		$html .= getCategoriesOptionsString();
		$html .=
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Supprimer\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	function getCategoriesOptionsString(){
		global $DB;
		$html = "";
		$DB->query(
			"SELECT `key`, `nom` ".
			"FROM `produits_categories` ".
			"WHERE `actif`='1' ".
			"ORDER BY `nom` ASC"
		);
		while($DB->next_record()) {
			$html .= "<option value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . "</option>\n";
		}
		return $html;
	}

	function getProduitsOptionsString(){
		global $DB;
		$html = "";
		$DB->query(
			"SELECT `produits`.`key`, `produits`.`nom`, `produits`.`produits_categories_key`, ".
			"`produits_categories`.`nom` AS `nom_categorie` ".
			"FROM `produits`, `produits_categories` ".
			"WHERE `produits`.`produits_categories_key`=`produits_categories`.`key` ".
			"AND `produits`.`actif`='1' ".
			"ORDER BY `produits`.`nom` ASC"
		);
		while($DB->next_record()) {
			$html .= "<option value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . " - ". $DB->getField("nom_categorie") ." </option>\n";
		}
		return $html;
	}

	// Fonction qui supprime un produit de la DB
	function deleteProduitFromDB($produit_key) {
		global $DB;
		
		//deleteProduitImageFromDB($produit_key);

		$Produit = new Produit($DB, "", 1);
		$Produit->setQKey("key", $produit_key);
		$Produit->set('actif', '0');		
	}

	// Fonction qui supprime une catégorie de la DB
	function deleteCategorieFromDB($key) {
		global $DB;
		
		$Categorie = new Categorie($DB, "", 1);
		$Categorie->setQKey("key", $key);
		$Categorie->set('actif', '0');		
	}
	
	//fonction qui vérifie si au moins une catégorie existe
	function verifyCategorieExists(){
		$blnOk = true;
		global $DB;
		$DB->query(
			"SELECT count(`key`) AS nombre ".
			"FROM `produits_categories` " .
			"WHERE `actif`='1'"
		);
		if($DB->next_record()){
			if ($DB->getField("nombre")== 0){
				$blnOk = false;
			}
		}
		return $blnOk;
	}
	
	//fonction qui vérifie si au moins une catégorie existe
	function verifyProduitExists(){
		$blnOk = true;
		global $DB;
		$DB->query(
			"SELECT count(`key`) AS nombre ".
			"FROM `produits` " .
			"WHERE `actif`='1'"
		);
		if($DB->next_record()){
			if ($DB->getField("nombre")== 0){
				$blnOk = false;
			}
		}
		return $blnOk;
	}


	//fonction qui vérifie s'il y a des produits rattachés à la catégorie
	function verifyProduitsLinkedToCategorie($key){
		$blnOk = false;
		global $DB;
		$DB->query(
			"SELECT count(`key`) AS `nombre` ".
			"FROM `produits` " .
			"WHERE `produits_categories_key`='".$key. "' " .
			"AND `actif`='1'"
			);
		if($DB->next_record()){
			if ($DB->getField("nombre")> 0){
				$blnOk = true;
			}
		}
		return $blnOk;
	}
	
	//	
	function deleteProduitImageFromDB($produit_key){
		global $DB;
		global $root_image;

		$Produit = new Produit($DB, "", 0);
		$Produit->scanfields();
		$Produit->setQKey("key", $produit_key);
		$Produit->loadAll();
		$full_path = $Produit->get("url_image");

		unlink($root_image.$full_path);
	}
	
	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function validateJSString() { 
		$html =
			"<script type=\"text/javascript\" language=\"javascript\">\n" .
			"	String.prototype.trim = function(){ \n" .
			"		return this.replace(/^\s+|\s+$/g,'');\n" .
			"		}\n".
			
			"	function validateForm(nomChamp) {\n".
//			"			alert('Validation');\n" .
			"		var champ = document.getElementById(nomChamp);\n".
			"		var champValue = champ.value;\n".
			"		var blnOk = false;\n".
			"		if (champValue.trim()!='' && champValue!='-1'){\n".
			"			blnOk = true;\n".
			"		} \n" .
			"		else if (champValue=='-1'){\n".
			"			var nomSection = 'une catégorie';\n".
			"			if (nomChamp=='produit_key'){\n".
			"				nomSection = 'un produit';\n" .
			"			} \n" .
			"			alert('Vous devez sélectionner '+nomSection);\n" .
			"			champ.focus();\n" .
			"		} \n" .
			"		return blnOk;\n" .

			"	}\n".
			"</script>\n" .
			"\n";
		return $html;
	}
	
?>
