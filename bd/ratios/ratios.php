<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "ratios");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("ratios/index.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 4)) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "ratios");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("ratios/index.tpl");
		exit;
	}

	import("com.quiboweb.form.Form");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "ratios");


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$admin = 0;
	if ($_SESSION['user_type'] == 1) { $admin = 1; }

	$action = getorpost('action');
	$currentForm = getorpost('currentForm');
	$requestForm = getorpost('requestForm');
	$etudeKey = getorpost('etudeKey');
	$annee = getorpost('annee');

	$successString = "";
	$errorString = "";
	$formString = "";

	$valuesToLoad = array();
	$valuesToSave = array();
	$noticeArray = array();
	$warningArray = array();
	$errorArray = array();

	if (!$requestForm) { $requestForm = 1; }
	if (!$annee) { $annee = 2008; }

	if (!isRatioOpen($annee, $DB) && !$admin) {
		$errorArray[] = "La saisie de donn&eacute;es est pr&eacute;sentement verrouill&eacute;e.<br/>Veuillez contacter un administrateur si vous devez modifier votre ratio.";

		//$valuesToLoad = getValuesFromDb($requestForm, $DB, $etudeKey, $annee);
		$valuesToLoad = array();

		$Skin->assign('ratioClosed', 1);

		if (isRatioComplete($etudeKey, $annee, $DB)) {
			$noticeArray[] = "Ce ratio est termin&eacute;.";
		} else {
			$warningArray[] = "Ce ratio n'est pas termin&eacute;.";
		}

		displayForm($requestForm, $DB, $Skin, $valuesToLoad, $etudeKey, $annee, $noticeArray, $warningArray, $errorArray, $admin);

	} else {

		switch (strtolower($action)) {
			case 'display':
				if (isRatioComplete($etudeKey, $annee, $DB)) {
					$warningArray[] = "Ce ratio est inscrit comme <strong>termin&eacute;</strong>.<br />Pour le modifier, vous devez d'abord aller &agrave; l'<a href=\"#\" onclick=\"submitRatiosForm('" . $currentForm . "', '6');\"><strong><u>&eacute;tape 6</u></strong></a> pour le marquer comme <strong>non-termin&eacute;</strong>.";
				}

				if ($requestForm == 6) {
					$errorArray = getErrorArray($etudeKey, $annee, $DB);
					$warningArray = getWarningArray($etudeKey, $annee, $DB);
				}

				$valuesToLoad = getValuesFromDb($requestForm, $DB, $etudeKey, $annee);
				displayForm($requestForm, $DB, $Skin, $valuesToLoad, $etudeKey, $annee, $noticeArray, $warningArray, $errorArray, $admin);
				break;

			case 'start':
				if (!ratioExists($etudeKey, $annee, $DB)) {
					createRatioIntoDb($etudeKey, $annee, $DB);

				} elseif (isRatioComplete($etudeKey, $annee, $DB)) {
					$warningArray[] = "Ce ratio est inscrit comme <strong>termin&eacute;</strong>.<br />Pour le modifier, vous devez d'abord aller &agrave; l'<a href=\"#\" onclick=\"submitRatiosForm('" . $currentForm . "', '6');\"><strong><u>&eacute;tape 6</u></strong></a> pour le marquer comme <strong>non-termin&eacute;</strong>.";
				}

				if ($requestForm == 6) {
					$errorArray = getErrorArray($etudeKey, $annee, $DB);
					$warningArray = getWarningArray($etudeKey, $annee, $DB);
				}

				$valuesToLoad = getValuesFromDb($requestForm, $DB, $etudeKey, $annee);
				displayForm(1, $DB, $Skin, $valuesToLoad, $etudeKey, $annee, $noticeArray, $warningArray, $errorArray, $admin);
				break;

			case 'autosave':
				if (isRatioComplete($etudeKey, $annee, $DB)) {
					$warningArray[] = "Ce ratio est inscrit comme <strong>termin&eacute;</strong>.<br />Pour le modifier, vous devez d'abord aller &agrave; l'<a href=\"#\" onclick=\"submitRatiosForm('" . $currentForm . "', '6');\"><strong><u>&eacute;tape 6</u></strong></a> pour le marquer comme <strong>non-termin&eacute;</strong>.";

				} else {
					if (!ratioExists($etudeKey, $annee, $DB)) {
						createRatioIntoDb($etudeKey, $annee, $DB);
					}
					$valuesToSave = getValuesFromPost();
					setValuesIntoDb($currentForm, $DB, $valuesToSave);
					if ($currentForm == 6) {
						$errorArray = getErrorArray($etudeKey, $annee, $DB);
						$warningArray = getWarningArray($etudeKey, $annee, $DB);
					}
				}

				$valuesToLoad = getValuesFromPost();
				displayForm($currentForm, $DB, $Skin, $valuesToLoad, $etudeKey, $annee, $noticeArray, $warningArray, $errorArray, $admin);
				break;

			case 'save':
				if (isRatioComplete($etudeKey, $annee, $DB)) {
					$warningArray[] = "Ce ratio est inscrit comme <strong>termin&eacute;</strong>.<br />Pour le modifier, vous devez d'abord aller &agrave; l'<a href=\"#\" onclick=\"submitRatiosForm('" . $currentForm . "', '6');\"><strong><u>&eacute;tape 6</u></strong></a> pour le marquer comme <strong>non-termin&eacute;</strong>.";

				} else {
					$valuesToSave = getValuesFromPost();
					if (!ratioExists($etudeKey, $annee, $DB)) {
						createRatioIntoDb($etudeKey, $annee, $DB);
					}
					setValuesIntoDb($currentForm, $DB, $valuesToSave);
					if ($requestForm == 6) {
						$errorArray = getErrorArray($etudeKey, $annee, $DB);
						$warningArray = getWarningArray($etudeKey, $annee, $DB);
					}
				}

				$valuesToLoad = getValuesFromDb($requestForm, $DB, $etudeKey, $annee);
				displayForm($requestForm, $DB, $Skin, $valuesToLoad, $etudeKey, $annee, $noticeArray, $warningArray, $errorArray, $admin);
				break;

			case 'complete':
				$errorArray = getErrorArray($etudeKey, $annee, $DB);
				if (count($errorArray) == 0) {
					if (setRatioAsComplete($etudeKey, $annee, $DB)) {
						$noticeArray[] = "Le ratio a &eacute;t&eacute; signal&eacute; comme <strong>termin&eacute;</strong> avec succ&egrave;s.";
					} else {
						$errorArray[] = "Une erreur s'est produite. Le ratio n'a pu &eacute;t&eacute; signal&eacute; comme termin&eacute;.";
					}
					displayForm(6, $DB, $Skin, $valuesToLoad, $etudeKey, $annee, $noticeArray, $warningArray, $errorArray, $admin);
				} else {
					displayForm(6, $DB, $Skin, $valuesToLoad, $etudeKey, $annee, $noticeArray, $warningArray, $errorArray, $admin);
				}
				break;

			case 'notcomplete':
				setRatioAsNotComplete($etudeKey, $annee, $DB);
				$noticeArray[] = "Le ratio a &eacute;t&eacute; signal&eacute; comme <strong>non termin&eacute;</strong> avec succ&egrave;s.";
				if ($requestForm == 5) {
					$errorArray = getErrorArray($etudeKey, $annee, $DB);
					$warningArray = getWarningArray($etudeKey, $annee, $DB);
				}
				$valuesToLoad = getValuesFromDb($requestForm, $DB, $etudeKey, $annee);
				displayForm($requestForm, $DB, $Skin, $valuesToLoad, $etudeKey, $annee, $noticeArray, $warningArray, $errorArray, $admin);
				break;

			default:
				header("Location: " . $BASEURL . "ratios/index.php");
				exit();
		}
	}

	closeDbLinks();
	exit();



	// -------------
	// LES FONCTIONS
	// -------------

	function ratioExists($etudeKey, $annee, $DB) {
		$SQL = "SELECT `key` FROM `ratios` WHERE `etudes_key` = '" . $etudeKey . "' AND `annee` = '" . $annee . "' LIMIT 0,1;";
		$DB->query($SQL);
		return($DB->getNumRows());
	}

	function isRatioOpen($annee, $DB) {
		$current_date = date('Y-m-d');
		$SQL = "SELECT `date_debut`, `date_fin`, `ouvert` FROM `ratios_controls` WHERE `annee` = '" . $annee . "';";
		$DB->query($SQL);
		while ($record = $DB->next_record()) {
			if (!$record['ouvert']) {
				return 0;
			}
			if ((( $current_date >= $record['date_debut'] ) && ( $current_date <= $record['date_fin'] || $record['date_fin'] == '0000-00-00' ))) {
				return 1;
			}
		}
		return 0;
	}

	function isRatioComplete($etudeKey, $annee, $DB) {
		$SQL = "SELECT `key` FROM `ratios` WHERE `etudes_key` = '" . $etudeKey . "' AND `annee` = '" . $annee . "' AND `complete` = 1 LIMIT 0,1;";
		$DB->query($SQL);
		return($DB->getNumRows());
	}

	function createRatioIntoDb($etudeKey, $annee, $DB) {
		$SQL = "INSERT INTO `ratios` (`etudes_key`, `annee`) VALUES ('" . $etudeKey . "', '" . $annee . "');";
		$DB->query($SQL);
		return($DB->getInsertId());
	}

	function setRatioAsComplete($etudeKey, $annee, $DB) {
		$SQL =
			"SELECT `complete_date` ".
			"FROM `ratios` ".
			"WHERE `etudes_key` = '" . $etudeKey . "' AND `annee` = '" . $annee . "' ".
			"LIMIT 0,1;";

		$DB->query($SQL);
		$record = $DB->next_record();
		if ($record['complete_date'] != "") {
			$dateSet = 1;
		} else {
			$dateSet = 0;
		}

		$SQL =
			"UPDATE `ratios` ".
			"SET ".
			"`complete` = '1' ";

		if (!$dateSet) { $SQL .= ",`complete_date` = CURRENT_TIMESTAMP "; }

		$SQL .= "WHERE `etudes_key` = '" . $etudeKey . "' AND `annee` = '" . $annee . "';";

		return($DB->query($SQL));
	}

	function setRatioAsNotComplete($etudeKey, $annee, $DB) {
		$SQL =
			"UPDATE `ratios` ".
			"SET ".
			"`complete` = '0' ".
			"WHERE `etudes_key` = '" . $etudeKey . "' AND `annee` = '" . $annee . "';";

		return($DB->query($SQL));
	}

	function getValuesFromPost() {
		$values = array();

		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$values[$field] = $value;
				} else {
					$values[$field] = trim($value);
				}
			}
		}

		return $values;
	}

	function getValuesFromDb($form, $DB, $etudeKey, $annee) {
		switch ($form) {
			case 'all':
			case '1':
				$SQL =
					"SELECT ".
					"`nom_utilisateur`, ".
					"`email_utilisateur`, ".
					"`phone_utilisateur`, ".
					"`nb_notaires_associes`, ".
					"`nb_notaires_salaries`, ".
					"`nb_total_notaires`, ".
					"`nb_employes`, ".
					"`nb_stagieres`, ".
					"`dossier_mandat`, ".
					"`nb_dossiers`, ".
					"`nb_minutes`, ".
					"`minutes_excluent_radiation`, ".
					"`nb_societes`, ".
					"`nb_feducies`, ".
					"`nb_dossiers_ag`, ".
					"`nb_dossiers_mc` ".
					"FROM `ratios` ".
					"WHERE `etudes_key`='" . $etudeKey . "' AND `annee`='" . $annee . "' ".
					"LIMIT 0,1;";

				$DB->query($SQL);
				$record = $DB->next_record();

				$values['nomUtilisateur'] = $record['nom_utilisateur'];
				$values['emailUtilisateur'] = $record['email_utilisateur'];
				$values['phoneUtilisateur'] = $record['phone_utilisateur'];
				$values['nbNotairesAssocies'] = $record['nb_notaires_associes'];
				$values['nbNotairesSalaries'] = $record['nb_notaires_salaries'];
				$values['nbNotairesTotal'] = $record['nb_total_notaires'];
				$values['nbEmployes'] = $record['nb_employes'];
				$values['nbStagieres'] = $record['nb_stagieres'];
				$values['dossierMandat'] = $record['dossier_mandat'];
				$values['nbDossiers'] = $record['nb_dossiers'];
				$values['nbMinutesNotaires'] = $record['nb_minutes'];
				$values['minutesExcluent'] = $record['minutes_excluent_radiation'];
				$values['nbSocietes'] = $record['nb_societes'];
				$values['nbFeducies'] = $record['nb_feducies'];
				$values['nbDossiersAG'] = $record['nb_dossiers_ag'];
				$values['nbDossiersMC'] = $record['nb_dossiers_mc'];

				if ($form != 'all') break;

			case '2':
				$SQL =
					"SELECT ".
					"`facturation`, ".
					"`chiffre_affaires`, ".
					"`debours_refactures`, ".
					"`benefices_nets`, ".
					"`depenses_totales`, ".
					"`salaires`, ".
					"`salaires_coll`, ".
					"`salaires_notaires`, ".
					"`depense_loyer`, ".
					"`depense_debourses_dossiers`, ".
					"`depense_inters_prets_marge`, ".
					"`depense_amortissement`, ".
					"`depense_telecom`, ".
					"`depense_soutien`, ".
					"`depense_publicite`, ".
					"`depense_representation`, ".
					"`autres_depenses` ".
					"FROM `ratios` ".
					"WHERE `etudes_key`='" . $etudeKey . "' AND `annee`='" . $annee . "' ".
					"LIMIT 0,1;";

				$DB->query($SQL);
				$record = $DB->next_record();

				$values['facturation'] = $record['facturation'];
				$values['chiffreAffaire'] = $record['chiffre_affaires'];
				$values['deboursRefactures'] = $record['debours_refactures'];
				$values['beneficesNets'] = $record['benefices_nets'];
				$values['depensesTotales'] = $record['depenses_totales'];
				$values['salaires'] = $record['salaires'];
				$values['salairesColl'] = $record['salaires_coll'];
				$values['salairesNotaires'] = $record['salaires_notaires'];
				$values['depenseLoyer'] = $record['depense_loyer'];
				$values['depenseDeboursesDossiers'] = $record['depense_debourses_dossiers'];
				$values['depenseInteretsPretMarge'] = $record['depense_inters_prets_marge'];
				$values['depenseAmortissement'] = $record['depense_amortissement'];
				$values['depenseTelecom'] = $record['depense_telecom'];
				$values['depenseSoutien'] = $record['depense_soutien'];

				$values['depensePublicite'] = $record['depense_publicite'];
				$values['depenseRepresentation'] = $record['depense_representation'];

				$values['depenseAutres'] = $record['autres_depenses'];

				if ($form != 'all') break;

			case '3':
				$SQL =
					"SELECT ".
					"`recevables`, ".
					"`travaux_en_cours`, ".
					"`total_actifs`, ".
					"`total_passifs`, ".
					"`capital_associes`, ".
					"`nb_heures_facturables_annee`, ".
					"`nb_heures_facturables_collaborateurs_annee`, ".
					"`secretaire_juridique_junior`, ".
					"`secretaire_juridique_senior`, ".
					"`technicien_juridique_junior`, ".
					"`technicien_juridique_senior`, ".
					"`receptionniste`, ".
					"`adjointe_administrative`, ".
					"`notaire_salarie_junior`, ".
					"`notaire_salarie_senior`, ".
					"`stagiaire_notariat`, ".
					"`stagiaire_techniques_juridiques`, ".
					"`secretaire_juridique_junior_min`, ".
					"`secretaire_juridique_senior_min`, ".
					"`technicien_juridique_junior_min`, ".
					"`technicien_juridique_senior_min`, ".
					"`receptionniste_min`, ".
					"`adjointe_administrative_min`, ".
					"`notaire_salarie_junior_min`, ".
					"`notaire_salarie_senior_min`, ".
					"`stagiaire_notariat_min`, ".
					"`stagiaire_techniques_juridiques_min`, ".
					"`nb_notaires`, ".
					"`nb_collaborateurs` ".
					"FROM `ratios` ".
					"WHERE `etudes_key`='" . $etudeKey . "' AND `annee`='" . $annee . "' ".
					"LIMIT 0,1;";

				$DB->query($SQL);
				$record = $DB->next_record();

				$values['recevables'] = $record['recevables'];
				$values['travauxEnCours'] = $record['travaux_en_cours'];
				$values['totalActifsCourtTerme'] = $record['total_actifs'];
				$values['totalPassifsCourtTerme'] = $record['total_passifs'];
				$values['capitalAssocies'] = $record['capital_associes'];
				$values['nbNotaires'] = $record['nb_notaires'];
				$values['nbCollaborateurs'] = $record['nb_collaborateurs'];
				$values['nbHeuresFacturablesParAnnee'] = $record['nb_heures_facturables_annee'];
				$values['nbHeuresFacturablesCollaborateursParAnnee'] = $record['nb_heures_facturables_collaborateurs_annee'];
				$values['salaireSecretaireJuridiqueJunior'] = $record['secretaire_juridique_junior'];
				$values['salaireSecretaireJuridiqueSenior'] = $record['secretaire_juridique_senior'];
				$values['salaireTechnicienJunior'] = $record['technicien_juridique_junior'];
				$values['salaireTechnicienSenior'] = $record['technicien_juridique_senior'];
				$values['salaireReceptionniste'] = $record['receptionniste'];
				$values['salaireAdjointeAdministrative'] = $record['adjointe_administrative'];
				$values['salaireNotaireJunior'] = $record['notaire_salarie_junior'];
				$values['salaireNotaireSenior'] = $record['notaire_salarie_senior'];
				$values['salaireStagiaireNotariat'] = $record['stagiaire_notariat'];
				$values['salaireStagiaireTechniques'] = $record['stagiaire_techniques_juridiques'];
				$values['salaireSecretaireJuridiqueJuniorMin'] = $record['secretaire_juridique_junior_min'];
				$values['salaireSecretaireJuridiqueSeniorMin'] = $record['secretaire_juridique_senior_min'];
				$values['salaireTechnicienJuniorMin'] = $record['technicien_juridique_junior_min'];
				$values['salaireTechnicienSeniorMin'] = $record['technicien_juridique_senior_min'];
				$values['salaireReceptionnisteMin'] = $record['receptionniste_min'];
				$values['salaireAdjointeAdministrativeMin'] = $record['adjointe_administrative_min'];
				$values['salaireNotaireJuniorMin'] = $record['notaire_salarie_junior_min'];
				$values['salaireNotaireSeniorMin'] = $record['notaire_salarie_senior_min'];
				$values['salaireStagiaireNotariatMin'] = $record['stagiaire_notariat_min'];
				$values['salaireStagiaireTechniquesMin'] = $record['stagiaire_techniques_juridiques_min'];

				if ($form != 'all') break;

			case '4':
				$SQL =
					"SELECT ".
					"`immobilier_residentiel`, ".
					"`immobilier_residentiel_diff`, ".
					"`immobilier_res_ce`, ".
					"`immobilier_res_cn`, ".
					"`constitution_services_co`, ".
					"`immobilier_existant`, ".
					"`immobilier_neuf`, ".
					"`commercial_corporatif`, ".
					"`commercial_corporatif_diff`, ".
					"`commercial`, ".
					"`immobilier_comm_ind`, ".
					"`corporatif`, ".
					"`service_corporatif`, ".
					"`personne_succession_familial`, ".
					"`personne_succession_familial_diff`, ".
					"`testaments_mandats`, ".
					"`donations_fiduciaires`, ".
					"`sucessions`, ".
					"`mediation_familiale`, ".
					"`procedures_non_content`, ".
					"`protection_patrimoine`, ".
					"`autres_personne_familial`, ".
					"`agricole_municipale_administratif`, ".
					"`agricole_municipale_administratif_diff`, ".
					"`agricole`, ".
					"`municipal_administratif`, ".
					"`divers_total`, ".
					"`divers`, ".
					"`divers_diff`, ".
					"`accepte_mandats_exterieurs`, ".
					"`taux_remboursement`, ".
					"`numerisation_pieces`, ".
					"`particularites_annee` ".
					"FROM `ratios` ".
					"WHERE `etudes_key`='" . $etudeKey . "' AND `annee`='" . $annee . "' ".
					"LIMIT 0,1;";

				$DB->query($SQL);
				$record = $DB->next_record();

				$values['immobilierResidentielTotal'] = $record['immobilier_residentiel'];
				$values['immobilierResidentielDiff'] = $record['immobilier_residentiel_diff'];
				$values['immobilierResidentielCondoExistant'] = $record['immobilier_res_ce'];
				$values['immobilierResidentielCondoNeuf'] = $record['immobilier_res_cn'];
				$values['constitutionServicesCondo'] = $record['constitution_services_co'];

				$values['immobilierExistant'] = $record['immobilier_existant'];
				$values['immobilierNeuf'] = $record['immobilier_neuf'];

				$values['commercialCorporatifTotal'] = $record['commercial_corporatif'];
				$values['commercialCorporatifDiff'] = $record['commercial_corporatif_diff'];
				$values['commercial'] = $record['commercial'];
				$values['immobilierComm'] = $record['immobilier_comm_ind'];
				$values['corporatif'] = $record['corporatif'];
				$values['serviceCorporatif'] = $record['service_corporatif'];
				$values['personneSuccessionFamilialTotal'] = $record['personne_succession_familial'];
				$values['personneSuccessionFamilialDiff'] = $record['personne_succession_familial_diff'];
				$values['testamentsMandats'] = $record['testaments_mandats'];
				$values['donationsFiduciaires'] = $record['donations_fiduciaires'];
				$values['successions'] = $record['sucessions'];
				$values['mediationFamiliale'] = $record['mediation_familiale'];
				$values['proceduresNonContent'] = $record['procedures_non_content'];
				$values['protectionPatrimoine'] = $record['protection_patrimoine'];
				$values['autresPersonneFamilial'] = $record['autres_personne_familial'];
				$values['agricoleMunicipalAdministratifTotal'] = $record['agricole_municipale_administratif'];
				$values['agricoleMunicipalAdministratifDiff'] = $record['agricole_municipale_administratif_diff'];
				$values['agricole'] = $record['agricole'];
				$values['municipalAdministratif'] = $record['municipal_administratif'];
				$values['diversTotal'] = $record['divers_total'];
				$values['divers'] = $record['divers'];
				$values['diversDiff'] = $record['divers_diff'];
				$values['accepteMandats'] = $record['accepte_mandats_exterieurs'];
				$values['tauxRemboursementKilo'] = $record['taux_remboursement'];
				$values['numerisationPiecesIdentite'] = $record['numerisation_pieces'];
				$values['particularitesAnnee'] = $record['particularites_annee'];

				if ($form != 'all') break;

			case '5':
				$SQL =
					"SELECT ".
					"`forfait_honoraires_residentiel`, ".
					"`forfait_honoraires_testaments`, ".
					"`forfait_honoraires_corporatif`, ".
					"`forfait_honoraires_feducie`, ".
					"`forfait_honoraires_ag`, ".
					"`forfait_honoraires_mc`, ".
					"`forfait_honoraires_hypotheque`, ".
					"`forfait_honoraires_incorporation`, ".
					"`forfait_honoraires_hmn`, ".
					"`forfait_honoraires_rpm`, ".
					"`forfait_honoraires_htn`, ".
					"`taux_horaire_notaire_moins_de_5_ans`, ".
					"`taux_horaire_notaire_5_a_10_ans`, ".
					"`taux_horaire_notaire_plus_de_10_ans`, ".
					"`taux_horaire_collaborateur` ".
					"FROM `ratios` ".
					"WHERE `etudes_key`='" . $etudeKey . "' AND `annee`='" . $annee . "' ".
					"LIMIT 0,1;";

				$DB->query($SQL);
				$record = $DB->next_record();

				$values['forfaitHonorairesResidentiel'] = $record['forfait_honoraires_residentiel'];
				$values['forfaitHonorairesTestaments'] = $record['forfait_honoraires_testaments'];
				$values['forfaitHonorairesCorporatif'] = $record['forfait_honoraires_corporatif'];
				$values['forfaitHonorairesFeducie'] = $record['forfait_honoraires_feducie'];
				$values['forfaitHonorairesAG'] = $record['forfait_honoraires_ag'];
				$values['forfaitHonorairesMC'] = $record['forfait_honoraires_mc'];
				$values['forfaitHonorairesHypotheque'] = $record['forfait_honoraires_hypotheque'];
				$values['forfaitHonorairesIncorporation'] = $record['forfait_honoraires_incorporation'];
				$values['forfaitHonorairesHMN'] = $record['forfait_honoraires_hmn'];
				$values['forfaitHonorairesRPM'] = $record['forfait_honoraires_rpm'];
				$values['forfaitHonorairesHTN'] = $record['forfait_honoraires_htn'];
				$values['tauxHoraireNotaireMoinsDe5ans'] = $record['taux_horaire_notaire_moins_de_5_ans'];
				$values['tauxHoraireNotaire5a10ans'] = $record['taux_horaire_notaire_5_a_10_ans'];
				$values['tauxHoraireNotairePlusDe10ans'] = $record['taux_horaire_notaire_plus_de_10_ans'];
				$values['tauxHoraireCollaborateur'] = $record['taux_horaire_collaborateur'];

				if ($form != 'all') break;
		}

		return $values;
	}

	function setValuesIntoDb($form, $DB, $values = array()) {
		$SQL = "";

		if ($values['etudeKey'] != '' && $values['annee'] != '') {
			switch ($form) {
				case '1':
					if ($values['minutesExcluent'] === '') {
						$minutesExcluent = "NULL";
					} else {
						$minutesExcluent = "'" . $values['minutesExcluent'] . "'";
					}
					$SQL =
						"UPDATE `ratios` SET ".
						"`nom_utilisateur`='" . $values['nomUtilisateur'] . "', ".
						"`email_utilisateur`='" . $values['emailUtilisateur'] . "', ".
						"`phone_utilisateur`='" . $values['phoneUtilisateur'] . "', ".
						"`nb_notaires_associes`='" . $values['nbNotairesAssocies'] . "', ".
						"`nb_notaires_salaries`='" . $values['nbNotairesSalaries'] . "', ".
						"`nb_total_notaires`='" . $values['nbNotairesTotal'] . "', ".
						"`nb_employes`='" . $values['nbEmployes'] . "', ".
						"`nb_stagieres`='" . $values['nbStagieres'] . "', ".
						"`dossier_mandat`='" . $values['dossierMandat'] . "', ".
						"`nb_dossiers`='" . $values['nbDossiers'] . "', ".
						"`nb_minutes`='" . $values['nbMinutesNotaires'] . "', ".
						"`minutes_excluent_radiation`=" . $minutesExcluent . ", ".
						"`nb_societes`='" . $values['nbSocietes'] . "', ".
						"`nb_feducies`='" . $values['nbFeducies'] . "', ".
						"`nb_dossiers_ag`='" . $values['nbDossiersAG'] . "', ".
						"`nb_dossiers_mc`='" . $values['nbDossiersMC'] . "' ".
						"WHERE `etudes_key`='" . $values['etudeKey'] . "' AND `annee`='" . $values['annee'] . "';";
					break;

				case '2':
					$SQL =
						"UPDATE `ratios` SET ".
						"`facturation`='" . $values['facturation'] . "', ".
						"`chiffre_affaires`='" . $values['chiffreAffaire'] . "', ".
						"`debours_refactures`='" . $values['deboursRefactures'] . "', ".
						"`benefices_nets`='" . $values['beneficesNets'] . "', ".
						"`depenses_totales`='" . $values['depensesTotales'] . "',".
						"`salaires`='" . $values['salaires'] . "', ".
						"`salaires_coll`='" . $values['salairesColl'] . "', ".
						"`salaires_notaires`='" . $values['salairesNotaires'] . "', ".
						"`depense_loyer`='" . $values['depenseLoyer'] . "', ".
						"`depense_debourses_dossiers`='" . $values['depenseDeboursesDossiers'] . "', ".
						"`depense_inters_prets_marge`='" . $values['depenseInteretsPretMarge'] . "', ".
						"`depense_amortissement`='" . $values['depenseAmortissement'] . "', ".
						"`depense_telecom`='" . $values['depenseTelecom'] . "', ".
						"`depense_soutien`='" . $values['depenseSoutien'] . "', ".
						"`depense_publicite`='" . $values['depensePublicite'] . "', ".
						"`depense_representation`='" . $values['depenseRepresentation'] . "', ".
						"`autres_depenses`='" . $values['depenseAutres'] . "' ".
						"WHERE `etudes_key`='" . $values['etudeKey'] . "' AND `annee`='" . $values['annee'] . "';";
					break;

				case '3':
					$SQL =
						"UPDATE `ratios` SET ".
						"`recevables`='" . $values['recevables'] . "',".
						"`travaux_en_cours`='" . $values['travauxEnCours'] . "', ".
						"`total_actifs`='" . $values['totalActifsCourtTerme'] . "', ".
						"`total_passifs`='" . $values['totalPassifsCourtTerme'] . "', ".
						"`capital_associes`='" . $values['capitalAssocies'] . "', ".
						"`nb_notaires`='" . $values['nbNotaires'] . "', ".
						"`nb_collaborateurs`='" . $values['nbCollaborateurs'] . "', ".
						"`nb_heures_facturables_annee`='" . $values['nbHeuresFacturablesParAnnee'] . "', ".
						"`nb_heures_facturables_collaborateurs_annee`='" . $values['nbHeuresFacturablesCollaborateursParAnnee'] . "', ".
						"`secretaire_juridique_junior`='" . $values['salaireSecretaireJuridiqueJunior'] . "', ".
						"`secretaire_juridique_junior_min`='" . $values['salaireSecretaireJuridiqueJuniorMin'] . "', ".
						"`secretaire_juridique_senior`='" . $values['salaireSecretaireJuridiqueSenior'] . "', ".
						"`secretaire_juridique_senior_min`='" . $values['salaireSecretaireJuridiqueSeniorMin'] . "', ".
						"`technicien_juridique_junior`='" . $values['salaireTechnicienJunior'] . "', ".
						"`technicien_juridique_junior_min`='" . $values['salaireTechnicienJuniorMin'] . "', ".
						"`technicien_juridique_senior`='" . $values['salaireTechnicienSenior'] . "', ".
						"`technicien_juridique_senior_min`='" . $values['salaireTechnicienSeniorMin'] . "', ".
						"`receptionniste`='" . $values['salaireReceptionniste'] . "', ".
						"`receptionniste_min`='" . $values['salaireReceptionnisteMin'] . "', ".
						"`adjointe_administrative`='" . $values['salaireAdjointeAdministrative'] . "', ".
						"`adjointe_administrative_min`='" . $values['salaireAdjointeAdministrativeMin'] . "', ".
						"`notaire_salarie_junior`='" . $values['salaireNotaireJunior'] . "', ".
						"`notaire_salarie_junior_min`='" . $values['salaireNotaireJuniorMin'] . "', ".
						"`notaire_salarie_senior`='" . $values['salaireNotaireSenior'] . "', ".
						"`notaire_salarie_senior_min`='" . $values['salaireNotaireSeniorMin'] . "', ".
						"`stagiaire_notariat`='" . $values['salaireStagiaireNotariat'] . "', ".
						"`stagiaire_notariat_min`='" . $values['salaireStagiaireNotariatMin'] . "', ".
						"`stagiaire_techniques_juridiques`='" . $values['salaireStagiaireTechniques'] . "', ".
						"`stagiaire_techniques_juridiques_min`='" . $values['salaireStagiaireTechniquesMin'] . "' ".
						"WHERE `etudes_key`='" . $values['etudeKey'] . "' AND `annee`='" . $values['annee'] . "';";
					break;

				case '4':
					if ($values['accepteMandats'] === '') {
						$accepteMandats = "NULL";
					} else {
						$accepteMandats = "'" . $values['accepteMandats'] . "'";
					}
					if ($values['numerisationPiecesIdentite'] === '') {
						$numerisationPiecesIdentite = "NULL";
					} else {
						$numerisationPiecesIdentite = "'" . $values['numerisationPiecesIdentite'] . "'";
					}
					$SQL =
						"UPDATE `ratios` SET ".
						"`immobilier_residentiel`='" . $values['immobilierResidentielTotal'] . "', ".
						"`immobilier_residentiel_diff`='" . $values['immobilierResidentielDiff'] . "', ".

						"`immobilier_res_ce`='" . $values['immobilierResidentielCondoExistant'] . "', ".
						"`immobilier_res_cn`='" . $values['immobilierResidentielCondoNeuf'] . "', ".
						"`constitution_services_co`='" . $values['constitutionServicesCondo'] . "', ".

						"`immobilier_existant`='" . $values['immobilierExistant'] . "', ".
						"`immobilier_neuf`='" . $values['immobilierNeuf'] . "', ".
						"`commercial_corporatif`='" . $values['commercialCorporatifTotal'] . "', ".
						"`commercial_corporatif_diff`='" . $values['commercialCorporatifDiff'] . "', ".
						"`commercial`='" . $values['commercial'] . "', ".
						"`immobilier_comm_ind`='" . $values['immobilierComm'] . "', ".
						"`corporatif`='" . $values['corporatif'] . "', ".
						"`service_corporatif`='" . $values['serviceCorporatif'] . "', ".
						"`personne_succession_familial`='" . $values['personneSuccessionFamilialTotal'] . "', ".
						"`personne_succession_familial_diff`='" . $values['personneSuccessionFamilialDiff'] . "', ".
						"`testaments_mandats`='" . $values['testamentsMandats'] . "', ".
						"`donations_fiduciaires`='" . $values['donationsFiduciaires'] . "', ".
						"`sucessions`='" . $values['successions'] . "', ".
						"`mediation_familiale`='" . $values['mediationFamiliale'] . "', ".
						"`procedures_non_content`='" . $values['proceduresNonContent'] . "', ".
						"`protection_patrimoine`='" . $values['protectionPatrimoine'] . "', ".
						"`autres_personne_familial`='" . $values['autresPersonneFamilial'] . "', ".
						"`agricole_municipale_administratif`='" . $values['agricoleMunicipalAdministratifTotal'] . "', ".
						"`agricole_municipale_administratif_diff`='" . $values['agricoleMunicipalAdministratifDiff'] . "', ".
						"`agricole`='" . $values['agricole'] . "', ".
						"`municipal_administratif`='" . $values['municipalAdministratif'] . "', ".
						"`divers_total`='" . $values['diversTotal'] . "', ".
						"`divers`='" . $values['divers'] . "', ".
						"`divers_diff`='" . $values['diversDiff'] . "', ".
						"`accepte_mandats_exterieurs`=" . $accepteMandats . ", ".
						"`taux_remboursement`='" . $values['tauxRemboursementKilo'] . "', ".
						"`numerisation_pieces`=" . $numerisationPiecesIdentite . ", ".
						"`particularites_annee`='" . preg_replace("/\'/", "''", $values['particularitesAnnee']) . "' ".
						"WHERE `etudes_key`='" . $values['etudeKey'] . "' AND `annee`='" . $values['annee'] . "';";
					break;

				case '5':
					$SQL =
						"UPDATE `ratios` SET ".
						"`forfait_honoraires_residentiel`='" . $values['forfaitHonorairesResidentiel'] . "',".
						"`forfait_honoraires_testaments`='" . $values['forfaitHonorairesTestaments'] . "', ".
						"`forfait_honoraires_corporatif`='" . $values['forfaitHonorairesCorporatif'] . "', ".
						"`forfait_honoraires_feducie`='" . $values['forfaitHonorairesFeducie'] . "', ".
						"`forfait_honoraires_ag`='" . $values['forfaitHonorairesAG'] . "', ".
						"`forfait_honoraires_mc`='" . $values['forfaitHonorairesMC'] . "', ".
						"`forfait_honoraires_hypotheque`='" . $values['forfaitHonorairesHypotheque'] . "', ".
						"`forfait_honoraires_incorporation`='" . $values['forfaitHonorairesIncorporation'] . "', ".
						"`forfait_honoraires_hmn`='" . $values['forfaitHonorairesHMN'] . "', ".
						"`forfait_honoraires_rpm`='" . $values['forfaitHonorairesRPM'] . "', ".
						"`forfait_honoraires_htn`='" . $values['forfaitHonorairesHTN'] . "', ".
						"`taux_horaire_notaire_moins_de_5_ans`='" . $values['tauxHoraireNotaireMoinsDe5ans'] . "', ".
						"`taux_horaire_notaire_5_a_10_ans`='" . $values['tauxHoraireNotaire5a10ans'] . "', ".
						"`taux_horaire_notaire_plus_de_10_ans`='" . $values['tauxHoraireNotairePlusDe10ans'] . "', ".
						"`taux_horaire_collaborateur`='" . $values['tauxHoraireCollaborateur'] . "' ".
						"WHERE `etudes_key`='" . $values['etudeKey'] . "' AND `annee`='" . $values['annee'] . "';";
					break;
			}

			if ($SQL != '') {
				$DB->query($SQL);
			}
		}
	}

	function toInt($str) {
		$int = (float) str_replace(" ", "", str_replace(",", ".", str_replace("&nbsp", "", $str)));
		return($int);
	}

	function getWarningArray($etudeKey, $annee, $DB) {
		$warningArray = array();

		$DB->query("SELECT * FROM `ratios` WHERE `etudes_key`='" . $etudeKey . "' AND `annee`='" . $annee . "';");
		$record = $DB->next_record();

		$nb_notaires_associes = toInt($record['nb_notaires_associes']);
		$nb_notaires_salaries  = toInt($record['nb_notaires_salaries']);
		$nb_total_notaires = toInt($record['nb_total_notaires']);
		$nb_employes = toInt($record['nb_employes']);
		$nb_dossiers  = toInt($record['nb_dossiers']);
		$nb_minutes  = toInt($record['nb_minutes']);
		$minutes_excluent_radiation  = toInt($record['minutes_excluent_radiation']);
		$facturation  = toInt($record['facturation']);
		$chiffre_affaires  = toInt($record['chiffre_affaires']);
		$debours_refactures  = toInt($record['debours_refactures']);
		$benefices_nets = toInt($record['benefices_nets']);

		$depensesTotales = toInt($record['depenses_totales']);
		$salairesTotal = toInt($record['salaires']);
		$salaires_coll = toInt($record['salaires_coll']);
		$salaires_notaires  = toInt($record['salaires_notaires']);

		$depense_loyer = toInt($record['depense_loyer']);
		$depense_debourses_dossiers = toInt($record['depense_debourses_dossiers']);
		$depense_inters_prets_marge = toInt($record['depense_inters_prets_marge']);
		$depense_amortissement = toInt($record['depense_amortissement']);
		$depense_telecom = toInt($record['depense_telecom']);
		$depense_soutien = toInt($record['depense_soutien']);
		$depense_publicite = toInt($record['depense_publicite']);
		$depense_representation = toInt($record['depense_representation']);
		$autres_depenses = toInt($record['autres_depenses']);
		$recevables = toInt($record['recevables']);
		$travaux_en_cours = toInt($record['travaux_en_cours']);
		$total_actifs = toInt($record['total_actifs']);
		$total_passifs = toInt($record['total_passifs']);
		$capital_associes = toInt($record['capital_associes']);
		$nb_heures_facturables_annee = toInt($record['nb_heures_facturables_annee']);
		$nb_heures_facturables_collaborateurs_annnee = toInt($record['nb_heures_facturables_collaborateurs_annnee']);
		$secretaire_juridique_junior = toInt($record['secretaire_juridique_junior']);
		$secretaire_juridique_senior = toInt($record['secretaire_juridique_senior']);
		$technicien_juridique_junior = toInt($record['technicien_juridique_junior']);
		$technicien_juridique_senior = toInt($record['technicien_juridique_senior']);
		$receptionniste = toInt($record['receptionniste']);
		$adjointe_administrative = toInt($record['adjointe_administrative']);
		$notaire_salarie_junior = toInt($record['notaire_salarie_junior']);
		$notaire_salarie_senior = toInt($record['notaire_salarie_senior']);
		$stagiaire_notariat = toInt($record['stagiaire_notariat']);
		$stagiaire_techniques_juridiques = toInt($record['stagiaire_techniques_juridiques']);

		$immobilierTotal = toInt($record['immobilier_residentiel']);
		$immobilierDiff = toInt($record['immobilier_residentiel_diff']);

		$immobilierResidentielCondoExistant = toInt($record['immobilier_res_ce']);
		$immobilierResidentielCondoNeuf = toInt($record['immobilier_res_cn']);
		$constitutionServicesCondo = toInt($record['constitution_services_co']);

		$immobilier_existant = toInt($record['immobilier_existant']);
		$immobilier_neuf = toInt($record['immobilier_neuf']);

		$commercialCorporatifTotal = toInt($record['commercial_corporatif']);
		$commercialCorporatifDiff = toInt($record['commercial_corporatif_diff']);
		$commercial = toInt($record['commercial']);
		$immobilier_comm_ind = toInt($record['immobilier_comm_ind']);
		$corporatif = toInt($record['corporatif']);
		$service_corporatif  = toInt($record['service_corporatif']);

		$contratTotal = toInt($record['personne_succession_familial']);
		$contratDiff = toInt($record['personne_succession_familial_diff']);
		$testaments_mandats = toInt($record['testaments_mandats']);
		$donations_fiduciaires = toInt($record['donations_fiduciaires']);
		$sucessions = toInt($record['sucessions']);
		$mediation_familiale = toInt($record['mediation_familiale']);
		$procedures_non_content = toInt($record['procedures_non_content']);
		$protection_patrimoine = toInt($record['protection_patrimoine']);
		$autres_personne_familial = toInt($record['autres_personne_familial']);

		$agricoleMunicipalTotal = toInt($record['agricole_municipale_administratif']);
		$agricoleMunicipalDiff = toInt($record['agricole_municipale_administratif_diff']);
		$agricole = toInt($record['agricole']);
		$municipal_administratif = toInt($record['municipal_administratif']);

		$divers = toInt($record['divers']);
		$diversTotal = toInt($record['divers_total']);
		$diversDiff = toInt($record['divers_diff']);
		$accepte_mandats_exterieurs = toInt($record['accepte_mandats_exterieurs']);
		$taux_remboursement = toInt($record['taux_remboursement']);
		$numerisation_pieces = toInt($record['numerisation_pieces']);
		$complete = toInt($record['complete']);

		$etape1Link = "<br /><a href=\"" . $_SERVER['PHP_SELF'] . "?annee=" . getorpost('annee') . "&amp;etudeKey=" . getorpost('etudeKey') . "&amp;action=display&amp;requestForm=1\">Cliquer ici pour sauter &agrave; l'&eacute;tape 1.</a>";
		$etape2Link = "<br /><a href=\"" . $_SERVER['PHP_SELF'] . "?annee=" . getorpost('annee') . "&amp;etudeKey=" . getorpost('etudeKey') . "&amp;action=display&amp;requestForm=2\">Cliquer ici pour sauter &agrave; l'&eacute;tape 2.</a>";
		$etape3Link = "<br /><a href=\"" . $_SERVER['PHP_SELF'] . "?annee=" . getorpost('annee') . "&amp;etudeKey=" . getorpost('etudeKey') . "&amp;action=display&amp;requestForm=3\">Cliquer ici pour sauter &agrave; l'&eacute;tape 3.</a>";
		$etape4Link = "<br /><a href=\"" . $_SERVER['PHP_SELF'] . "?annee=" . getorpost('annee') . "&amp;etudeKey=" . getorpost('etudeKey') . "&amp;action=display&amp;requestForm=4\">Cliquer ici pour sauter &agrave; l'&eacute;tape 4.</a>";
		$etape5Link = "<br /><a href=\"" . $_SERVER['PHP_SELF'] . "?annee=" . getorpost('annee') . "&amp;etudeKey=" . getorpost('etudeKey') . "&amp;action=display&amp;requestForm=5\">Cliquer ici pour sauter &agrave; l'&eacute;tape 5.</a>";

		//Nombre total des notaires doit etre egal au nombre notaires associes + nombre de notaires salaries
		$total = $nb_notaires_associes + $nb_notaires_salaries;
		if ((string) $total != (string) $nb_total_notaires && $total != '0') {
			array_push($warningArray, '<strong>Avertissement</strong> - Le nombre total de notaires doit &ecirc;tre &eacute;gal &agrave; la somme du nombre de notaires salari&eacute;s et du nombre de notaires associ&eacute;s (questions #1 à #3).' . $etape1Link);
		}

		//Verifier le total des salaires
		$total = $salaires_coll + $salaires_notaires;
		if ((string) $total != (string) $salairesTotal && $total != '0') {
			array_push($warningArray, '<strong>Avertissement</strong> - Les salaires des notaires et de leurs collaborateurs n\'&eacute;galent pas le montant sp&eacute;cifi&eacute; dans la case <i>salaires</i>.' . $etape2Link);
		}

		//Le chiffre d'affaire doit etre moins eleve que la facturation
		if ($chiffre_affaires > $facturation) {
			array_push($warningArray, '<strong>Avertissement</strong> - Le chiffre d\'affaires devrait &ecirc;tre moins &eacute;lev&eacute; que la facturation.' . $etape2Link);
		}

		//Les debours refactures doivent etre entre 10 % et 30% du CA
		$minCa = 0.1 * $chiffre_affaires;
		$maxCa = 0.3 * $chiffre_affaires;
		if ($debours_refactures > $maxCa || $debours_refactures < $minCa) {
			array_push($warningArray, '<strong>Avertissement</strong> - Les d&eacute;bours &laquo;&nbsp;refactur&eacute;s&nbsp;&raquo; (question #11) devraient repr&eacute;senter entre 10% et 30% du chiffre d\'affaires.' . $etape2Link);
		}

		//Les salaires doivent etre inferieurs a 60% du montant inscrit a la ligne >depenses totales<
		if ($salairesTotal > (0.6 * $depensesTotales)) {
			array_push($warningArray, '<strong>Avertissement</strong> - Le total de vos salaires repr&eacute;sente plus de 60% de vos d&eacute;penses totales.' . $etape2Link);
		}

		//Les depenses totales doit egaliser les 6 champs en dessous de lui
		$total = $salaires_coll + $salaires_notaires + $depense_loyer + $depense_debourses_dossiers + $depense_inters_prets_marge + $depense_amortissement + $depense_telecom + $depense_soutien + $depense_publicite + $depense_representation + $autres_depenses;
		if ((string) $total != (string) $depensesTotales && $total != '0') {
			array_push($warningArray, '<strong>Avertissement</strong> - Vos d&eacute;penses totales devraient repr&eacute;senter la somme des &eacute;l&eacute;ments suivants : le salaire des collaborateurs, le salaire des notaires, les d&eacute;penses de l\'amortissement, les d&eacute;penses pour les pr&ecirc;ts et les marges, les d&eacute;penses de d&eacute;bours&eacute;s de dossiers, les d&eacute;penses de loyer et les autres d&eacute;penses.' . $etape2Link);
		}

		//Les depenses déboursés de dossier doivent etre inferieur à 40% du montant des depenses totales
		$maxDep = 0.4 * $depensesTotales;
		if ($depense_debourses_dossiers > $maxDep) {
			array_push($warningArray, '<strong>Avertissement</strong> - Les d&eacute;penses d&eacute;bours&eacute;s de dossiers ne devraient pas repr&eacute;senter plus de 40% des d&eacute;penses totales.' . $etape2Link);
		}

		//Les recevables a la fin de la periode doivent etre inferieur a 20% de Facturation
		if ($recevables > (0.2 * $facturation)) {
			array_push($warningArray, '<strong>Avertissement</strong> - Selon les donn&eacute;es entr&eacute;es, votre montant recevable &agrave; la fin de la p&eacute;riode repr&eacute;sente plus de 20% de la facturation.' . $etape3Link);
		}
/*
		//Verifier le total de l'immobilier
		$total = $immobilier_existant + $immobilier_neuf + $immobilierDiff;
		if ((string) $total != (string) $immobilierTotal && $total != '0') {
			array_push($warningArray, '<strong>Avertissement</strong> - La valeur totale de l\'immobilier n\'&eacute;gale pas la somme de ses &eacute;l&eacute;ments.' . $etape4Link);
		}

		//Verifier le total de personne, succession & familial
		$total = $testaments_mandats + $donations_fiduciaires + $sucessions + $mediation_familiale + $procedures_non_content + $protection_patrimoine + $autres_personne_familial + $contratDiff;
		if ((string) $total != (string) $contratTotal && $total != '0') {
			array_push($warningArray, '<strong>Avertissement</strong> - La valeur totale de la section <i>Personne, succession & familial</i> n\'&eacute;galent pas la somme de ses &eacute;l&eacute;ments.' . $etape4Link);
		}

		//Verifier le total de l'agricole
		$total = $agricole + $municipal_administratif + $agricoleMunicipalDiff;
		if ((string) $total != (string) $agricoleMunicipalTotal && $total != '0') {
			array_push($warningArray, '<strong>Avertissement</strong> - La valeur totale de la section <i>Agricole, municipal & administratif</i> n\'&eacute;galent pas la somme de ses &eacute;l&eacute;ments.' . $etape4Link);
		}

		//Verifier le total du corporatif
		$total = $commercial + $immobilier_comm_ind + $corporatif + $service_corporatif + $commercialCorporatifDiff;
		if ((string) $total != (string) $commercialCorporatifTotal) {
			array_push($warningArray,'<strong>Avertissement</strong> - La valeur totale de la section <i>Commercial & corporatif</i> n\'&eacute;galent pas la somme de ses &eacute;l&eacute;ments.' . $etape4Link);
		}

		//Verifier le total du divers
		$total = $divers + $diversDiff;
		if ((string) $total != (string) $diversTotal) {
			array_push($warningArray,'<strong>Avertissement</strong> - La valeur totale de la section <i>Divers</i> n\'&eacute;galent pas la somme de ses &eacute;l&eacute;ments.' . $etape4Link);
		}
*/
		return $warningArray;
	}

	function getErrorArray($etudeKey, $annee, $DB) {
		$errorArray = array();
		$errors = array();

		$values = getValuesFromDb('all', $DB, $etudeKey, $annee);

		$champsObligatoires = array(
			// Champs Étape 1
			'nbNotairesAssocies' => 101,
			'nbNotairesSalaries' => 102,
			'nbNotairesTotal' => 103,
			'nbEmployes' => 104,
			//'nbStagieres' => 6,
			//'dossierMandat' => 7,
			'nbDossiers' => 107,
			'nbMinutesNotaires' => 108,
			//'minutesExcluent' => 10,
			//'nbSocietes' => 11,

			// Champs Étape 2
			'facturation' => 200,
			'chiffreAffaire' => 201,
			'deboursRefactures' => 202,
			'beneficesNets' => 203,
			'depensesTotales' => 204,
			'salaires' => 205,
			'salairesColl' => 206,
			'salairesNotaires' => 207,
			'depenseLoyer' => 208,
			'depenseDeboursesDossiers' => 209,
			'depenseInteretsPretMarge' => 210,
			'depenseAmortissement' => 211,
			'depenseTelecom' => 212,
			'depenseSoutien' => 213,
			'depensePublicite' => 214,
			'depenseRepresentation' => 215,
			'depenseAutres' => 216,

			// Champs Étape 3
			'recevables' => 300,
			'travauxEnCours' => 301,
			'totalActifsCourtTerme' => 302,
			'totalPassifsCourtTerme' => 303,
			'capitalAssocies' => 304,
			'nbHeuresFacturablesParAnnee' => 307,
			'nbHeuresFacturablesCollaborateursParAnnee' => 308,

			// Champs Étape 4
			//'accepteMandats' => 70,
			//'tauxRemboursementKilo' => 71,
			//'numerisationPiecesIdentite' => 72,

			// Champs Étape 5
			//'forfaitHonorairesResidentiel' => 73,
			//'forfaitHonorairesTestaments' => 74,
			//'forfaitHonorairesCorporatif' => 75,
			//'forfaitHonorairesHypotheque' => 76,
			//'forfaitHonorairesIncorporation' => 77,
			//'tauxHoraireNotaireMoinsDe5ans' => 78,
			//'tauxHoraireNotaire5a10ans' => 79,
			//'tauxHoraireNotairePlusDe10ans' => 80,
			//'tauxHoraireCollaborateur' => 81
		);

		if (is_array($values) && count($values) > 0) {
			foreach ($values as $field => $value) {
				if ($champsObligatoires[$field] && ($value === '' || $value === NULL)) {
					$errors[] = $champsObligatoires[$field];
				}
			}
		}

		if (count($errors) > 0) {
			if (count($errors) == 1) {
				$debut = "La question";
				$fin = " doit &ecirc;tre r&eacute;pondue.";
			} else {
				$debut = "Les questions";
				$fin = " doivent &ecirc;tre r&eacute;pondues.";
			}

			$questions = "";
			foreach ($errors as $index => $num) {
				if ($index == 0) {
					$questions .= " <strong>" . $num . "</strong>";
				} elseif ($index == count($errors) - 1) {
					$questions .= " et <strong>" . $num . "</strong>";
				} else {
					$questions .= ", <strong>" . $num . "</strong>";
				}
			}

			array_push($errorArray, $debut . $questions . $fin);
		}

		return $errorArray;
	}

	function displayForm($form, $DB, $Skin, $values = array(), $etudeKey, $annee, $noticeArray = array(), $warningArray = array(), $errorArray = array(), $admin = 0) {
		global $BASEURL;

		$etudeNom = getEtudeNom($etudeKey, $DB);
		$completeBool = isRatioComplete($etudeKey, $annee, $DB);
		$openBool = isRatioOpen($annee, $DB);

		$jsOnLoad = "";
		$jsOnKeydown = "";
		if (!$completeBool) {
			$jsOnLoad = "startAutoSaveTimer(); ";
			$jsOnKeydown = "resetAutoSaveTimer(); ";
			switch ($form) {
				case 1:
					$jsOnLoad .= "toggleMustAll('1'); ";
					break;
				case 2:
					$jsOnLoad .= "toggleMustAll('2'); ";
					break;
				case 3:
					$jsOnLoad .= "toggleMustAll('3'); ";
					break;
				case 4:
					$jsOnLoad .= "toggleMustAll('4'); remplirSousTotauxEtTotaux(); ";
					break;
				case 5:
					$jsOnLoad .= "toggleMustAll('5'); ";
					break;
				case 6:
					break;
			}
		}
		if ($jsOnLoad != "") {
			$onLoad = "onload=\"javascript: " . $jsOnLoad . "\" ";
		}
		if ($jsOnKeydown != "") {
			$onKeydown = "onkeydown=\"javascript: " . $jsOnKeydown . " \" ";
		}
		$bodyparams = $onLoad . $onKeydown;

		$Skin->assign('admin', $admin);
		$Skin->assign('BASEURL', $BASEURL);
		$Skin->assign('etudeKey', $etudeKey);
		$Skin->assign('annee', $annee);
		$Skin->assign('nomEtude', $etudeNom);
		$Skin->assign('currentForm', $form);
		$Skin->assign('complete', $completeBool);
		$Skin->assign('open', $openBool);

		$Skin->assign('noticeArray', $noticeArray);
		$Skin->assign('warningArray', $warningArray);
		$Skin->assign('errorArray', $errorArray);

		$Skin->assign('bodyparams', $bodyparams);
		$Skin->assign('link_stats3', $BASEURL . "ratios/stats.php?action=donnees&type=stats3&etudeKey=" . $etudeKey . "&annee=" . $annee);
		$Skin->assign('link_stats5', $BASEURL . "ratios/stats.php?action=donnees&type=stats5&etudeKey=" . $etudeKey . "&annee=" . $annee);
		$Skin->assign('link_stats16', $BASEURL . "ratios/stats.php?action=donnees&type=stats16&etudeKey=" . $etudeKey . "&annee=" . $annee);
		$Skin->assign("JS", array('ratios','feedback', 'jquery.timers-1.1.2'));//, 'jquery-1.12.4'
		$Skin->assign("CSS", array('ratios'));

		$Skin->assign('form', 'etape' . $form);

		$Skin->assign('LIVETAB', $Skin->fetch('ratios/tabEtapes.tpl'));

		assignValuesToSkin($Skin, $values);

		$Skin->display("ratios/ratios.tpl");

	}

	function assignValuesToSkin($Skin, $values = array()) {
		if (is_array($values) && count($values) > 0) {
			foreach ($values as $key => $value) {
				$Skin->assign($key, $value);
			}
		}
	}

	function getEtudeNom($etudeKey, $DB) {
		$SQL = "SELECT `nom` FROM `etudes` WHERE `key` = '" . $etudeKey . "';";
		$DB->query($SQL);
		if ($DB->getNumRows() > 0) {
			$tmp = $DB->next_record();
			$nom = $tmp['nom'];
		}
		$DB->reset();
		return($nom);
	}

	function closeDbLinks() {
		global $DB, $DB2, $DB3;
		if ($DB != "") {
			$DB->close();
		}
		if ($DB2 != "") {
			$DB2->close();
		}
		if ($DB3 != "") {
			$DB3->close();
		}
	}

?>
