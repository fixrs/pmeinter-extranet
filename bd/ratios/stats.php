<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "ratios");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("ratios/index.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1 || ($_SESSION['user_type'] == 4))) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "ratios");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("ratios/index.tpl");
		exit;
	}

	if ($_SESSION['user_type'] == 1) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "ratios");
		$Skin->assign('admin', 1);
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "ratios");
	$Stats = new Statistiques($DB, $Skin);

	$Stats->doExecute();

	class Statistiques {
		public $db;
		public $skin;

		function __construct($DB = "", $Skin = "") {
			if ($DB != "" && $Skin != "") {
				$this->db = $DB;
				$this->skin = $Skin;
				import("com.pmeinter.ratios.NotaireArray");
				import("com.pmeinter.ratios.NotaireRow");
				import("com.pmeinter.ratios.RatioObject");
			} else {
				header("Location: " . $BASEURL . "ratios/index.php");
				exit();
			}
		}

		function doExecute() {
			$action = strtolower(getorpost('action'));
			$type = strtolower(getorpost('type'));
			$etudeKey = getorpost('etudeKey');

			// Securité
			if ($_SESSION['user_type'] == 4 && $_SESSION['user_etudes_key'] != $etudeKey) {
				exit();
			}

			if (!$type) {
				echo "Vous devez choisir un type de cumulatif.";
				exit();
			}
			if (($type == 'stats11' || $type == 'stats3' || $type == 'stats5') && !$etudeKey) {
				echo "Vous devez choisir une &eacute;tude.";
				exit();
			}

			switch (strtolower($action)) {
				case 'custom':
					$tableau = explode("-", strtolower(getorpost('tableau')));
					foreach ($tableau as $item) {
						switch ($item) {
							case 'donneesgenerales':
								$arrayActions['donneesgenerales'] = $this->buildArrayDonneesGenerales();
								break;
							case 'repartitionhonoraires':
								$arrayActions['repartitionhonoraires_a'] = $this->buildArrayRepartitionHonorairesA();
								$arrayActions['repartitionhonoraires_b'] = $this->buildArrayRepartitionHonorairesB();
								$arrayActions['repartitionhonoraires_c'] = $this->buildArrayRepartitionHonorairesC();
								break;
							case 'rentabilite':
								$arrayActions['rentabilite'] = $this->buildArrayRentabilite();
								break;
							case 'recevablesalaires':
								$arrayActions['recevablesalaires'] = $this->buildArrayRecevableSalaires();
								break;
							case 'tarification':
								$arrayActions['tarification'] = $this->buildArrayTarification();
								break;
							case 'depenses':
								$arrayActions['depenses_a'] = $this->buildArrayDepensesA();
								$arrayActions['depenses_b'] = $this->buildArrayDepensesB();
								break;
							case 'production':
								$arrayActions['production_a'] = $this->buildArrayProductionA();
								$arrayActions['production_b'] = $this->buildArrayProductionB();
								break;
							case 'statsalariales':
								$arrayActions['statsalariales_a'] = $this->buildArrayStatSalarialesA();
								$arrayActions['statsalariales_b'] = $this->buildArrayStatSalarialesB();
								break;
							case 'autres':
								$arrayActions['autres_a'] = $this->buildArrayAutresA();
								$arrayActions['autres_b'] = $this->buildArrayAutresB();
								$arrayActions['autres_c'] = $this->buildArrayAutresC();
								break;
						}
					}
					break;

				case 'donneesgenerales':
					$arrayActions['donneesgenerales'] = $this->buildArrayDonneesGenerales();
					break;

				case 'repartitionhonoraires':
					$arrayActions['repartitionhonoraires_a'] = $this->buildArrayRepartitionHonorairesA();
					$arrayActions['repartitionhonoraires_b'] = $this->buildArrayRepartitionHonorairesB();
					$arrayActions['repartitionhonoraires_c'] = $this->buildArrayRepartitionHonorairesC();
					break;

				case 'rentabilite':
					$arrayActions['rentabilite'] = $this->buildArrayRentabilite();
					break;

				case 'recevablesalaires':
					$arrayActions['recevablesalaires'] = $this->buildArrayRecevableSalaires();
					break;

				case 'tarification':
					$arrayActions['tarification'] = $this->buildArrayTarification();
					break;

				case 'depenses':
					$arrayActions['depenses_a'] = $this->buildArrayDepensesA();
					$arrayActions['depenses_b'] = $this->buildArrayDepensesB();
					break;

				case 'production':
					$arrayActions['production_a'] = $this->buildArrayProductionA();
					$arrayActions['production_b'] = $this->buildArrayProductionB();
					break;

				case 'statsalariales':
					$arrayActions['statsalariales_a'] = $this->buildArrayStatSalarialesA();
					$arrayActions['statsalariales_b'] = $this->buildArrayStatSalarialesB();
					break;

				case 'autres':
					$arrayActions['autres_a'] = $this->buildArrayAutresA();
					$arrayActions['autres_b'] = $this->buildArrayAutresB();
					$arrayActions['autres_c'] = $this->buildArrayAutresC();
					break;

				case 'all':
				default:
					$arrayActions['donneesgenerales'] = $this->buildArrayDonneesGenerales();
					$arrayActions['repartitionhonoraires_a'] = $this->buildArrayRepartitionHonorairesA();
					$arrayActions['repartitionhonoraires_b'] = $this->buildArrayRepartitionHonorairesB();
					$arrayActions['repartitionhonoraires_c'] = $this->buildArrayRepartitionHonorairesC();
					$arrayActions['rentabilite'] = $this->buildArrayRentabilite();
					$arrayActions['recevablesalaires'] = $this->buildArrayRecevableSalaires();
					$arrayActions['tarification'] = $this->buildArrayTarification();
					$arrayActions['depenses_a'] = $this->buildArrayDepensesA();
					$arrayActions['depenses_b'] = $this->buildArrayDepensesB();
					$arrayActions['production_a'] = $this->buildArrayProductionA();
					$arrayActions['production_b'] = $this->buildArrayProductionB();
					$arrayActions['statsalariales_a'] = $this->buildArrayStatSalarialesA();
					$arrayActions['statsalariales_b'] = $this->buildArrayStatSalarialesB();

					if ($admin || $type == 'stats11' || $type == 'stats3' || $type == 'stats5') {
						$arrayActions['autres_a'] = $this->buildArrayAutresA();
						$arrayActions['autres_b'] = $this->buildArrayAutresB();
						$arrayActions['autres_c'] = $this->buildArrayAutresC();
					}
					break;
			}

			$this->doDisplay($arrayActions);
		}

		function buildArrayDonneesGenerales() {
			$type = strtolower(getorpost('type'));

			if ($type == 'stats11') {

				$arrayKeys = Array(
					Array('desc' => 'Nombre de notaires associ&eacute;s', 'key' => 'nbNotairesAssocies', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Nombre de notaires salari&eacute;s', 'key' => 'nbNotairesSalaries', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre total de notaires', 'key' => 'nbTotalNotaires', 'unit' => '', 'type' => 'important', 'type3' => 'odd'),
					//Array('desc' => 'Nombre d\'employ&eacute;s (notaires exclus)', 'key' => 'nbEmployes', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre de collaborateur(trice)s', 'key' => 'nbEmployes', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre de stagiaires', 'key' => 'nbStagiaires', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Soci&eacute;t&eacute;s membres du Service Corporatif', 'key' => 'nbSocietes', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Fiducies membres du Service Fiducie', 'key' => 'nbFeducies', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Nombre de dossiers en cours pour votre service Ange Gardien', 'key' => 'nbDossiersAG', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre de dossiers en cours pour votre service Ma&icirc;tre des clefs', 'key' => 'nbDossiersMC', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Nombre de dossiers', 'key' => 'nbDossiers', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre de dossiers par notaire', 'key' => 'nbDossiersParNotaire', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Nombre de minutes', 'key' => 'nbMinutes', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre de minutes par notaire', 'key' => 'nbMinutesParNotaire', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Taux de remboursement par kilom&egrave;tre', 'key' => 'tauxRemboursement', 'unit' => 'cad', 'type3' => 'even'),
					Array('desc' => 'Les minutes excluent-elles les actes de radiation&nbsp;?', 'key' => 'minutesExcluentRadiation', 'unit' => 'bool', 'type3' => 'odd'),
					Array('desc' => 'Accepte les mandats provenant de centres de traitement des dossiers immobiliers (FCT)', 'key' => 'accepteMandatsExterieurs', 'unit' => 'bool', 'type3' => 'even'),
					Array('desc' => 'Num&eacute;risation des pi&egrave;ces d\'identit&eacute; et autres documents', 'key' => 'numerisationPieces', 'unit' => 'bool', 'type3' => 'odd'),
					Array('desc' => 'Particularit&eacute;s de l\'ann&eacute;e qui influencent particuli&egrave;rement les r&eacute;sultats financiers', 'key' => 'particularitesAnnee', 'unit' => 'txt', 'type3' => 'even')
				);
			} else {
				$arrayKeys = Array(
					Array('desc' => 'Nombre de notaires associ&eacute;s', 'key' => 'nbNotairesAssocies', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Nombre de notaires salari&eacute;s', 'key' => 'nbNotairesSalaries', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre total de notaires', 'key' => 'nbTotalNotaires', 'unit' => '', 'type' => 'important', 'type3' => 'odd'),
					//Array('desc' => 'Nombre d\'employ&eacute;s (notaires exclus)', 'key' => 'nbEmployes', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre de collaborateur(trice)s', 'key' => 'nbEmployes', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre de stagiaires', 'key' => 'nbStagiaires', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Soci&eacute;t&eacute;s membres du Service Corporatif', 'key' => 'nbSocietes', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Fiducies membres du Service Fiducie', 'key' => 'nbFeducies', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Nombre de dossiers en cours pour votre service Ange Gardien', 'key' => 'nbDossiersAG', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre de dossiers en cours pour votre service Ma&icirc;tre des clefs', 'key' => 'nbDossiersMC', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Nombre de dossiers', 'key' => 'nbDossiers', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre de dossiers par notaire', 'key' => 'nbDossiersParNotaire', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Nombre de minutes', 'key' => 'nbMinutes', 'unit' => '', 'type3' => 'even'),
					Array('desc' => 'Nombre de minutes par notaire', 'key' => 'nbMinutesParNotaire', 'unit' => '', 'type3' => 'odd'),
					Array('desc' => 'Taux de remboursement par kilom&egrave;tre', 'key' => 'tauxRemboursement', 'unit' => 'cad', 'type3' => 'even'),
					Array('desc' => 'Les minutes excluent-elles les actes de radiation&nbsp;?', 'key' => 'minutesExcluentRadiation', 'unit' => 'bool', 'type3' => 'odd'),
					Array('desc' => 'Accepte les mandats provenant de centres de traitement des dossiers immobiliers (FCT)', 'key' => 'accepteMandatsExterieurs', 'unit' => 'bool', 'type3' => 'even'),
					Array('desc' => 'Num&eacute;risation des pi&egrave;ces d\'identit&eacute; et autres documents', 'key' => 'numerisationPieces', 'unit' => 'bool', 'type3' => 'odd')
				);
			}



			return($arrayKeys);
		}

		function buildArrayRepartitionHonorairesA() {
			$arrayKeys = Array(
				Array('desc' => 'Total des honoraires', 'key' => 'honoraires', 'unit' => 'cad', 'type2' => 'rowend', 'type3' => 'odd'),
				Array('desc' => '<h3>Immobilier r&eacute;sidentiel</h3>', 'key' => '', 'unit' => 'cad', 'type' => 'sectionName'),
				Array('desc' => 'Immobilier existant', 'key' => 'immobilierExistant', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Immobilier neuf', 'key' => 'immobilierNeuf', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Immobilier r&eacute;sidentiel – copropri&eacute;t&eacute; existante', 'key' => 'immobilierResidentielCondoExistant', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Immobilier r&eacute;sidentiel – copropri&eacute;t&eacute; neuve', 'key' => 'immobilierResidentielCondoNeuf', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Constitution et services – copropri&eacute;t&eacute;', 'key' => 'constitutionServicesCondo', 'unit' => 'cad', 'type3' => 'odd'),

				Array('desc' => 'TOTAL', 'key' => 'immobilierResidentiel', 'unit' => 'cad', 'type' => 'total1', 'type2' => 'rowend', 'type3' => 'even'),
				Array('desc' => 'Pourcentage', 'key' => 'immobilierResidentielPourcent', 'unit' => 'percent', 'type' => 'total2', 'type2' => 'rowend', 'type3' => 'even'),
				Array('desc' => '<h3>Affaires (commercial &amp; corporatif)</h3>', 'key' => '', 'unit' => '', 'type' => 'sectionName'),
				Array('desc' => 'Commercial', 'key' => 'commercial', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Corporatif', 'key' => 'corporatif', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Immobilier commercial et industriel', 'key' => 'immobilierCommInd', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Service corporatif', 'key' => 'serviceCorporatif', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'TOTAL', 'key' => 'commercialCorporatif', 'unit' => 'cad', 'type' => 'total1', 'type2' => 'rowend', 'type3' => 'odd'),
				Array('desc' => 'Pourcentage', 'key' => 'commercialCorporatifPourcent', 'unit' => 'percent', 'type' => 'total2', 'type2' => 'rowend', 'type3' => 'odd')
			);

			return($arrayKeys);
		}

		function buildArrayRepartitionHonorairesB() {
			$arrayKeys = Array(
				Array('desc' => 'Total des honoraires', 'key' => 'honoraires', 'unit' => 'cad', 'type2' => 'rowend', 'type3' => 'odd'),
				Array('desc' => '<h3>Personne, succession &amp; familial</h3>', 'key' => '', 'unit' => '', 'type' => 'sectionName'),
				Array('desc' => 'Testaments et mandats', 'key' => 'testamentsMandats', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Donations fiduci&egrave;res', 'key' => 'donationsFiduciaires', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Successions', 'key' => 'sucessions', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'M&eacute;diation familiale', 'key' => 'mediationFamiliale', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Proc&eacute;dures non contentieuses', 'key' => 'proceduresNonContent', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Protection du patrimoine', 'key' => 'protectionPatrimoine', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Autres - personnes et familial', 'key' => 'autresPersonneFamilial', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'TOTAL', 'key' => 'personneSuccessionFamilial', 'unit' => 'cad', 'type' => 'total1', 'type2' => 'rowend', 'type3' => 'even'),
				Array('desc' => 'Pourcentage', 'key' => 'personneSuccessionFamilialPourcent', 'unit' => 'percent', 'type' => 'total2', 'type2' => 'rowend', 'type3' => 'even')
			);

			return($arrayKeys);
		}

		function buildArrayRepartitionHonorairesC() {
			$arrayKeys = Array(
				Array('desc' => 'Total des honoraires', 'key' => 'honoraires', 'unit' => 'cad', 'type2' => 'rowend', 'type3' => 'odd'),
				Array('desc' => '<h3>Agricole, municipale &amp; administratif</h3>', 'key' => '', 'unit' => '', 'type' => 'sectionName'),
				Array('desc' => 'Agricole', 'key' => 'agricole', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Municipale et administratif', 'key' => 'municipalAdministratif', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'TOTAL', 'key' => 'agricoleMunicipaleAdministratif', 'unit' => 'cad', 'type' => 'total1', 'type2' => 'rowend', 'type3' => 'odd'),
				Array('desc' => 'Pourcentage', 'key' => 'agricoleMunicipaleAdministratifPourcent', 'unit' => 'percent', 'type' => 'total2', 'type2' => 'rowend', 'type3' => 'odd'),
				Array('desc' => '<h3>Divers</h3>', 'key' => '', 'unit' => '', 'type' => 'sectionName'),
				Array('desc' => 'Divers', 'key' => 'divers', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'TOTAL', 'key' => 'diversTotal', 'unit' => 'cad', 'type' => 'total1', 'type2' => 'rowend', 'type3' => 'even'),
				Array('desc' => 'Pourcentage', 'key' => 'diversPourcent', 'unit' => 'percent', 'type' => 'total2', 'type2' => 'rowend', 'type3' => 'even')
			);

			return($arrayKeys);
		}

		function buildArrayRentabilite() {
			$arrayKeys = Array(
				Array('desc' => '<strong>% profit (b&eacute;n&eacute;fice) avant pr&eacute;l&egrave;vement</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">B&eacute;n&eacute;fice Net (BN)</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'beneficeNetSurCAPourcent', 'unit' => 'percent', 'type3' => 'odd'),
				Array('desc' => '<strong>% profit (b&eacute;n&eacute;fice) en excluant des d&eacute;penses, les salaires notaires (SN)</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">B&eacute;n&eacute;fice Net (BN) + Sal. Notaires (SN)</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'profitAvantPrelevementAvecSalairePourcent', 'unit' => 'percent', 'type3' => 'even'),
				Array('desc' => '<strong>% profit (b&eacute;n&eacute;fice) apr&egrave;s pr&eacute;l&egrave;vement (P)</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">B&eacute;n&eacute;fice Net (BN) - Pr&eacute;l&egrave;vement (P)</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'profitApresPrelevementPourcent', 'unit' => 'percent', 'type3' => 'odd'),
				Array('desc' => '<strong>Fonds de roulement</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Actifs &agrave; court terme</td></tr><tr><td class="diviseur">Passifs &agrave; court terme</td></tr></table></td></tr></table>', 'key' => 'fondRoulement', 'unit' => '', 'type3' => 'even'),

				Array('desc' => '<strong>Seuil de rentabilit&eacute; (SR)</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">( Co&ucirc;ts fixes (CF) * Chiffre d\'affaires (CA) )</td></tr><tr><td class="diviseur">( Chiffre d\'affaires (CA) - Co&ucirc;ts varialbles (CV) )</td></tr></table></td></tr></table>', 'key' => 'seuilRentabilite', 'unit' => 'cad', 'type3' => 'odd'),

				Array('desc' => '<strong>Point mort</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Seuil de rentabilit&eacute; (SR) * 360</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'pointMort', 'unit' => 'jours', 'type3' => 'even'),

				Array('desc' => '<strong>Coefficient multiplicateur des ressources</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Chiffre d\'affaires (CA)</td></tr><tr><td class="diviseur">(Salaires totaux (S) + Prélèvements (P))</td></tr></table></td></tr></table>', 'key' => 'coefficient_multiplicateur_ressources', 'unit' => '', 'type3' => 'odd'),

				Array('desc' => '<strong>CA qui aurait permis de dégager un bénéfice net de 15% après les prélèvements</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">(Dépenses totales + Prélèvements (P))</td></tr><tr><td class="diviseur">(1 - 15%)</td></tr></table></td></tr></table>', 'key' => 'benefice_pour_15', 'unit' => 'cad', 'type3' => 'even')
			);

			return($arrayKeys);
		}

		function buildArrayRecevableSalaires() {
			$arrayKeys = Array(
				Array('desc' => '<strong>D&eacute;lai de perception</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Recevables X 365 jours</td></tr><tr><td class="diviseur">Facturation (F)</td></tr></table></td></tr></table>', 'key' => 'delaiPerception', 'unit' => '', 'type3' => 'odd'),
				Array('desc' => '<strong>% des recevables sur facturation</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Recevables</td></tr><tr><td class="diviseur">Facturation (F)</td></tr></table></td></tr></table>', 'key' => 'recevableSurFacturationPourcent', 'unit' => 'percent', 'type3' => 'even'),
				Array('desc' => '<strong>% des travaux en cours (TEC) sur facturation</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Travaux en cours (TEC)</td></tr><tr><td class="diviseur">Facturation (F)</td></tr></table></td></tr></table>', 'key' => 'tecSurFacturationPourcent', 'unit' => 'percent', 'type2' => 'rowend', 'type3' => 'odd'),
				Array('desc' => '<strong>% salaires sur chiffre d\'affaires</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Salaires (S)</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'salairesSurCAPourcent', 'unit' => 'percent', 'type3' => 'even'),
				Array('desc' => '<strong>% salaires collabo. sur chiffre d\'affaires</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Salaires Collabo. (SC)</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'salaireCollSurCAPourcent', 'unit' => 'percent', 'type3' => 'odd'),
				Array('desc' => '<strong>% salaires notaires sur chiffre d\'affaires</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Salaires Notaires (SN)</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'salaireNotSurCAPourcent', 'unit' => 'percent', 'type3' => 'even'),
				Array('desc' => '<strong>% salaires sur d&eacute;penses totales</strong>&nbsp;:<br /><table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Salaires (S)</td></tr><tr><td class="diviseur">D&eacute;penses totales</td></tr></table></td></tr></table>', 'key' => 'salairesSurDepensesPourcent', 'unit' => 'percent', 'type3' => 'odd')
			);

			return($arrayKeys);
		}

		function buildArrayTarification() {
			$arrayKeys = Array(
				Array('desc' => 'Forfait honoraires (sans frais et taxes) pour un pr&ecirc;t/vente r&eacute;sidentiel pour une propri&eacute;t&eacute; d\'une valeur de 200 000 \$ et moins', 'key' => 'forfaitHonorairesResidentiel', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Forfait honoraires (sans frais et taxes) pour deux testaments et mandats (couple) simples', 'key' => 'forfaitHonorairesTestaments', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Forfait honoraires (sans frais et taxes) pour le service corporatif de base', 'key' => 'forfaitHonorairesCorporatif', 'unit' => 'cad', 'type3' => 'odd'),

				Array('desc' => 'Forfait honoraires (sans frais et taxes) pour le service fiducie de base', 'key' => 'forfaitHonorairesFeducie', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Forfait honoraires (sans frais et taxes) pour le service Ange Gardien de base', 'key' => 'forfaitHonorairesAG', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Forfait honoraires (sans frais et taxes) pour le service Ma&icirc;tre des clefs', 'key' => 'forfaitHonorairesMC', 'unit' => 'cad', 'type3' => 'even'),


				Array('desc' => 'Forfait honoraires (sans frais et taxes) pour une quittance ou mainlev&eacute;e - hypoth&egrave;que d\'un vendeur d\'une propri&eacute;t&eacute; r&eacute;sidentiel', 'key' => 'forfaitHonorairesHypotheque', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Forfait honoraires (sans frais et taxes) pour une nouvelle incorporation', 'key' => 'forfaitHonorairesIncorporation', 'unit' => 'cad', 'type3' => 'even'),

				Array('desc' => 'Honoraires estim&eacute;s (sans frais et taxes) pour un dossier d\'homologation de mandat devant notaire', 'key' => 'forfaitHonorairesHMN', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Honoraires estim&eacute;s (sans frais et taxes) pour un dossier d\'ouverture de r&eacute;gime de protection au majeur', 'key' => 'forfaitHonorairesRPM', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Honoraires estim&eacute;s (sans frais et taxes) pour un dossier d\'homologation de testament devant notaire', 'key' => 'forfaitHonorairesHTN', 'unit' => 'cad', 'type3' => 'odd'),

				Array('desc' => 'Taux horaire moyen pour un notaire de moins de 5 ans d\'exp&eacute;rience', 'key' => 'tauxHoraireNotaireMoinsDe5ans', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Taux horaire moyen pour un notaire de 5 &agrave; 10 ans d\'exp&eacute;rience', 'key' => 'tauxHoraireNotaire5a10ans', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Taux horaire moyen pour un notaire de plus de 10 ans d\'exp&eacute;rience', 'key' => 'tauxHoraireNotairePlusDe10ans', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Taux horaire pour collaborateur(rices)s', 'key' => 'tauxHoraireCollaborateur', 'unit' => 'cad', 'type3' => 'odd')
			);

			return($arrayKeys);
		}

		function buildArrayDepensesA() {
			$arrayKeys = Array(
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Salaires (S)</td></tr><tr><td class="diviseur">D&eacute;penses totales</td></tr></table></td></tr></table>', 'key' => 'salairesSurDepenses', 'unit' => 'percent', 'type3' => 'odd'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses de loyer</td></tr><tr><td class="diviseur">D&eacute;penses totales</td></tr></table></td></tr></table>', 'key' => 'depensesLoyerSurTotal', 'unit' => 'percent', 'type3' => 'even'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;pense de d&eacute;bours de dossier</td></tr><tr><td class="diviseur">D&eacute;penses totales</td></tr></table></td></tr></table>', 'key' => 'depensesDeboursDossierSurTotal', 'unit' => 'percent', 'type3' => 'odd'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses d\'int&eacute;r&ecirc;t</td></tr><tr><td class="diviseur">D&eacute;penses totales</td></tr></table></td></tr></table>', 'key' => 'depensesInteretSurTotal', 'unit' => 'percent', 'type3' => 'even'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses d\'amortissement</td></tr><tr><td class="diviseur">D&eacute;penses totales</td></tr></table></td></tr></table>', 'key' => 'depensesAmortissementSurTotal', 'unit' => 'percent', 'type3' => 'odd'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses de t&eacute;l&eacute;communication</td></tr><tr><td class="diviseur">D&eacute;penses totales</td></tr></table></td></tr></table>', 'key' => 'depensesTelecomSurTotal', 'unit' => 'percent', 'type3' => 'even'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses de soutien technique</td></tr><tr><td class="diviseur">D&eacute;penses totales</td></tr></table></td></tr></table>', 'key' => 'depensesSoutienSurTotal', 'unit' => 'percent', 'type3' => 'odd'),

				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses de publicit&eacute;</td></tr><tr><td class="diviseur">D&eacute;penses totales</td></tr></table></td></tr></table>', 'key' => 'depensesPubliciteSurTotal', 'unit' => 'percent', 'type3' => 'even'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses de repr&eacute;sentation</td></tr><tr><td class="diviseur">D&eacute;penses totales</td></tr></table></td></tr></table>', 'key' => 'depensesRepresentationSurTotal', 'unit' => 'percent', 'type3' => 'odd'),

				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Autres d&eacute;penses</td></tr><tr><td class="diviseur">D&eacute;penses totales</td></tr></table></td></tr></table>', 'key' => 'depensesAutresSurTotal', 'unit' => 'percent', 'type2' => 'rowend', 'type3' => 'even')
			);

			return($arrayKeys);
		}

		function buildArrayDepensesB() {
			$arrayKeys = Array(
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">B&eacute;n&eacute;fice Net (BN)</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'beneficeNetSurCA', 'unit' => 'percent', 'type2' => 'rowend', 'type3' => 'odd'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Salaires (S)</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'salairesSurCA', 'unit' => 'percent', 'type3' => 'even'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses de loyer</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'depenseLoyerSurCA', 'unit' => 'percent', 'type3' => 'odd'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses de d&eacute;bours de dossier</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'depensesDeboursDossierSurCA', 'unit' => 'percent', 'type3' => 'even'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses d\'int&eacute;r&ecirc;t</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'depensesInteretsSurCA', 'unit' => 'percent', 'type3' => 'odd'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses d\'amortissement</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'depensesAmortissementSurCA', 'unit' => 'percent', 'type3' => 'even'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses de t&eacute;l&eacute;communication</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'depensesTelecomSurCA', 'unit' => 'percent', 'type3' => 'odd'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses de soutien technique</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'depensesSoutienSurCA', 'unit' => 'percent', 'type3' => 'even'),

				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses de publicit&eacute;</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'depensesPubliciteSurCA', 'unit' => 'percent', 'type3' => 'odd'),
				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">D&eacute;penses de repr&eacute;sentation</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'depensesRepresentationSurCA', 'unit' => 'percent', 'type3' => 'even'),

				Array('desc' => '<table cellpadding="0" cellspacing="0" class="division-outer-box"><tr><td><table cellpadding="0" cellspacing="0" class="division"><tr><td class="dividande">Autres d&eacute;penses</td></tr><tr><td class="diviseur">Chiffre d\'affaires (CA)</td></tr></table></td></tr></table>', 'key' => 'depensesAutreSurCA', 'unit' => 'percent', 'type2' => 'rowend', 'type3' => 'odd'),
				Array('desc' => 'Salaires et avantages - moyenne par employ&eacute;', 'key' => 'salaireMoyenEmploye', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Salaires et avantages - moyenne par collabo.', 'key' => 'salaireMoyenCollabo', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaires et avantages - moyenne par notaire salari&eacute;', 'key' => 'salaireMoyenNotaireSalarie', 'unit' => 'cad', 'type3' => 'even')
			);

			return($arrayKeys);
		}

		function buildArrayProductionA() {
			$arrayKeys = Array(
				Array('desc' => 'Dossiers / mois', 'key' => 'dossierParMois', 'unit' => '', 'type3' => 'odd'),
				Array('desc' => 'Minutes / mois', 'key' => 'minutesParMois', 'unit' => '', 'type2' => 'rowend', 'type3' => 'even'),
				Array('desc' => 'Dossiers / notaire / ann&eacute;e', 'key' => 'dossiersParNotaireParAnnee', 'unit' => '', 'type3' => 'odd'),
				Array('desc' => 'Minutes / notaire / ann&eacute;e', 'key' => 'minutesParNotaireParAnnee', 'unit' => '', 'type2' => 'rowend', 'type3' => 'even'),
				Array('desc' => 'Co&ucirc;t moyen / dossier', 'key' => 'coutMoyenParDossier', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Chiffre d\'affaires (CA) / dossier', 'key' => 'chiffreAffairesParDossier', 'unit' => 'cad', 'type2' => 'rowend', 'type3' => 'even'),
				Array('desc' => 'Co&ucirc;t moyen / minute', 'key' => 'coutMoyenParMinute', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Chiffre d\'affaires (CA) / minute', 'key' => 'chiffreAffairesParMin', 'unit' => 'cad', 'type2' => 'rowend', 'type3' => 'even')
			);

			return($arrayKeys);
		}

		function buildArrayProductionB() {
			$arrayKeys = Array(
				Array('desc' => 'Nombre de collaborateurs par notaire', 'key' => 'nbCollaborateursParNotaire', 'unit' => '', 'type2' => 'rowend', 'type3' => 'odd'),
				Array('desc' => 'Chiffre d\'affaires moyen par notaire', 'key' => 'chiffreAffairesParNotaire', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Chiffre d\'affaires moyen par collaborateur (trice)', 'key' => 'chiffreAffairesParCollabo', 'unit' => 'cad', 'type2' => 'rowend', 'type3' => 'odd'),
				Array('desc' => 'Co&ucirc;t par heure facturable', 'key' => 'coutParHeure', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Taux horaire moyen (honoraires par heure facturable)', 'key' => 'tauxHoraireMoyen', 'unit' => 'cad', 'type3' => 'odd')
			);

			return($arrayKeys);
		}

		function buildArrayStatSalarialesA() {
			$arrayKeys = Array(
				Array('desc' => 'Salaire - Secr&eacute;taire juridique junior min', 'key' => 'secretaireJuridiqueJuniorMin', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaire - Secr&eacute;taire juridique junior max', 'key' => 'secretaireJuridiqueJuniorMax', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Salaire - Secr&eacute;taire juridique s&eacute;nior min', 'key' => 'secretaireJuridiqueSeniorMin', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaire - Secr&eacute;taire juridique s&eacute;nior max', 'key' => 'secretaireJuridiqueSeniorMax', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Salaire - Technicien juridique junior min', 'key' => 'technicienJuridiqueJuniorMin', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaire - Technicien juridique junior max', 'key' => 'technicienJuridiqueJuniorMax', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Salaire - Technicien juridique senior min', 'key' => 'technicienJuridiqueSeniorMin', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaire - Technicien juridique senior max', 'key' => 'technicienJuridiqueSeniorMax', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Salaire - R&eacute;ceptionniste min', 'key' => 'receptionnisteMin', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaire - R&eacute;ceptionniste max', 'key' => 'receptionnisteMax', 'unit' => 'cad', 'type3' => 'even')
			);

			return($arrayKeys);
		}

		function buildArrayStatSalarialesB() {
			$arrayKeys = Array(
				Array('desc' => 'Salaire - Adjointe administrative min', 'key' => 'adjointeAdministrativeMin', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaire - Adjointe administrative max', 'key' => 'adjointeAdministrativeMax', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Salaire - Notaire salari&eacute; junior min', 'key' => 'notaireSalarieJuniorMin', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaire - Notaire salari&eacute; junior max', 'key' => 'notaireSalarieJuniorMax', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Salaire - Notaire salari&eacute; senior min', 'key' => 'notaireSalarieSeniorMin', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaire - Notaire salari&eacute; senior max', 'key' => 'notaireSalarieSeniorMax', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Salaire - Stagiaire en notariat min', 'key' => 'stagiaireNotariatMin', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaire - Stagiaire en notariat max', 'key' => 'stagiaireNotariatMax', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Salaire - Stagiaire en techniques juridiques min', 'key' => 'stagiaireTechniquesJuridiquesMin', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaire - Stagiaire en techniques juridiques max', 'key' => 'stagiaireTechniquesJuridiquesMax', 'unit' => 'cad', 'type3' => 'even')
			);

			return($arrayKeys);
		}

		function buildArrayAutresA() {
			$arrayKeys = Array(
				//Array('desc' => 'Nombre d\'employ&eacute;s', 'key' => 'nbEmployes', 'unit' => '', 'type3' => 'odd'),
				Array('desc' => 'Facturation', 'key' => 'facturation', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Chiffre d\'affaires', 'key' => 'chiffreAffaire', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'D&eacute;bours refactur&eacute;s', 'key' => 'deboursRefactures', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'B&eacute;n&eacute;fices nets', 'key' => 'beneficesNets', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaires collaborateurs', 'key' => 'salairesColl', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Salaires des notaires', 'key' => 'salairesNotaires', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Salaires', 'key' => 'salaires', 'unit' => 'cad', 'type' => 'total', 'type3' => 'even')
			);

			return($arrayKeys);
		}

		function buildArrayAutresB() {
			$arrayKeys = Array(
				Array('desc' => 'D&eacute;penses de loyer', 'key' => 'depenseLoyer', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'D&eacute;pense - D&eacute;bours de dossier', 'key' => 'depenseDeboursesDossiers', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'D&eacute;pense - Int&eacute;r&ecirc;ts sur pr&ecirc;ts et marge', 'key' => 'depenseIntersPretsMarge', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'D&eacute;pense - Amortissement', 'key' => 'depenseAmortissement', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'D&eacute;pense - T&eacute;l&eacute;communication', 'key' => 'depenseTelecom', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'D&eacute;pense - Soutien technique', 'key' => 'depenseSoutien', 'unit' => 'cad', 'type3' => 'odd'),

				Array('desc' => 'D&eacute;pense - Publicit&eacute;', 'key' => 'depensePublicite', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'D&eacute;pense - Repr&eacute;sentation', 'key' => 'depenseRepresentation', 'unit' => 'cad', 'type3' => 'odd'),

				Array('desc' => 'D&eacute;pense - Autres', 'key' => 'autresDepenses', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'D&eacute;penses totales', 'key' => 'depensesTotales', 'unit' => 'cad', 'type' => 'total', 'type3' => 'even')
			);

			return($arrayKeys);
		}

		function buildArrayAutresC() {
			$arrayKeys = Array(
				Array('desc' => 'Recevables &agrave; la fin de la p&eacute;riode', 'key' => 'recevables', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Travaux en cours &agrave; la fin de la p&eacute;riode', 'key' => 'travauxEnCours', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Total des actifs &agrave; court terme - fin p&eacute;riode', 'key' => 'totalActifs', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Total des passifs &agrave; court terme - fin p&eacute;riode', 'key' => 'totalPassifs', 'unit' => 'cad', 'type3' => 'even'),
				Array('desc' => 'Capital ou avoir des associ&eacute;s', 'key' => 'capitalAssocies', 'unit' => 'cad', 'type3' => 'odd'),
				Array('desc' => 'Nombre d\'heures facturables notaires/ann&eacute;e', 'key' => 'nbHeuresFacturablesAnnee', 'unit' => '', 'type3' => 'even'),
				Array('desc' => 'Nombre d\'heures facturables collaborateur/ann&eacute;e', 'key' => 'nbHeuresFacturablesCollaborateursAnnee', 'unit' => '', 'type3' => 'odd')
			);

			return($arrayKeys);
		}

		function buildArrayData($ratios, $arrayKeys) {
			$notaireArray = new NotaireArray();

			$data = array();

			if (is_array($arrayKeys)) {
				foreach ($arrayKeys as $key) {
					$row = new NotaireRow($key['desc'], $key['unit'], $key['type'], $key['type2'], $key['type3']);

					foreach ($ratios as $ratio) {
						if ($key['type'] == 'spacer' || $key['type'] == 'sectionName') {
							$row->addData("");
						} else {
							$row->addData($ratio->$key['key']);
						}
					}
					$notaireArray->addRow($row);
				}
			}

			return($notaireArray);
		}

		function getAccesStats16Bool($etudeKey = "", $annee = "") {
			if ($_SESSION['user_type'] == 1) {
				return(TRUE);
			}

			$chk1 = 0;
			$chk2 = 0;
			if ($etudeKey && $annee) {
				$SQL1 = "SELECT `key` FROM `ratios` WHERE `etudes_key` = '" . $etudeKey . "' AND `complete` = 1 LIMIT 0,1;";
				$this->db->query($SQL1);
				if ($this->db->getNumRows() > 0) {
					$chk1 = 1;
				}

				$SQL2 =	"SELECT `bool` FROM `cumul_acces` WHERE `annee` = " . $annee . ";";
				$this->db->query($SQL2);
				while ($record = $this->db->next_record()) {
					$chk2 = $record['bool'];
				}
			}

			if ($chk1 && $chk2) {
				return(TRUE);
			}

			return(FALSE);
		}

		function retrieveAllRatios($type = "stats16", $params = array()) {
			$ratios = array();
			$type = strtolower(getorpost('type'));

			switch ($type) {
				case "stats16":
					if ($this->getAccesStats16Bool($_SESSION['user_etudes_key'], $params['anneeMax'])) {
						if ($params['sizeMin'] != "" && $params['sizeMax'] != "") {
							$SQL =
								"SELECT * ".
								"FROM `ratios` AS r, `etudes` AS e ".
								"WHERE r.etudes_key IS NOT NULL AND r.etudes_key != '0' AND r.annee = '" . $params['anneeMax'] . "' AND r.complete = '1' ".
								"AND r.nb_total_notaires >= " . $params['sizeMin'] . " AND r.nb_total_notaires <= " . $params['sizeMax'] . " ".
								"AND e.key = r.etudes_key AND e.actif = '1'".
								"ORDER BY REPLACE(r.nb_total_notaires, ',', '.') + 0 ASC;";
						} else {
							$SQL =
								"SELECT * ".
								"FROM `ratios` AS r, `etudes` AS e ".
								"WHERE r.etudes_key IS NOT NULL AND r.etudes_key != '0' AND r.annee = '" . $params['anneeMax'] . "' AND r.complete = '1' ".
								"AND e.key = r.etudes_key AND e.actif = '1'".
								"ORDER BY REPLACE(r.nb_total_notaires, ',', '.') + 0 ASC;";
						}
					}
					break;

				case "stats11":
					$SQL = "SELECT * FROM `ratios` WHERE `etudes_key` = '" . $params['etudeKey'] . "' AND `annee` = '" . $params['anneeMax'] . "';";
					break;

				case "stats3":
					$SQL = "SELECT * FROM `ratios` WHERE `etudes_key` = '" . $params['etudeKey'] . "' AND `annee` >= '" . $params['anneeMin'] . "' AND `annee` <= '" . $params['anneeMax'] . "' ORDER BY `annee` DESC;";
					break;

				case "stats5":
					$SQL = "SELECT * FROM `ratios` WHERE `etudes_key` = '" . $params['etudeKey'] . "' AND `annee` >= '" . $params['anneeMin'] . "' AND `annee` <= '" . $params['anneeMax'] . "' ORDER BY `annee` DESC;";
					break;
			}

			if ($SQL != "") {
				$this->db->query($SQL);
				while ($record = $this->db->next_record()) {
					$record["db"] = $this->db;
					$tmp = new RatioObject($record);

					$SQLNom = "SELECT `nom` FROM `etudes` WHERE `key`='" . $tmp->etudeKey . "';";
					$this->db->query($SQLNom);
					$record2 = $this->db->next_record();
					if ($this->db->getNumRows() > 0) {
						$this->db->reset();
					}
					if ($record2['nom'] != '') {
						$tmp->nomEtude = $record2['nom'];
						array_push($ratios, $tmp);
					}
				}
			}

			return $ratios;
		}

		function buildRatios() {
			$params = array();
			$type = strtolower(getorpost('type'));

			$params['etudeKey'] = getorpost('etudeKey');
			$params['anneeMax'] = getorpost('annee');
			$params['anneeMin'] = getorpost('annee') - 2;

			if ($type == "stats5") {
				$params['anneeMin'] = $params['anneeMin'] - 2;
			}

			$sizeLimites = explode("-", getorpost('size'));
			$params['sizeMin'] = $sizeLimites[0];
			$params['sizeMax'] = $sizeLimites[1];

			$ratios = $this->retrieveAllRatios($type, $params);

			return $ratios;
		}

		function buildRows($ratios, $arrayKeys) {
			$notaireArrays = array();

			if (!empty($ratios)) {
				array_push($notaireArrays, $this->buildArrayData($ratios, $arrayKeys));
			}

			foreach ($notaireArrays as $array) {
				foreach ($array->rows as $row) {
					$row->setAverage();
					$row->setTotal();
				}
			}

			$rows = $notaireArrays[0]->rows;

			return $rows;
		}

		function doDisplay($arrayActions) {
			global $BASEURL, $SKIN_URL;

			$type = strtolower(getorpost('type'));

			$params = array();
			$params['etudeKey'] = getorpost('etudeKey');
			$params['anneeMax'] = getorpost('annee');
			$params['anneeMin'] = getorpost('annee') - 2;

			$sizeLimites = explode("-", getorpost('size'));
			$params['sizeMin'] = $sizeLimites[0];
			$params['sizeMax'] = $sizeLimites[1];

			$ratios = array();
			$rows = array();
			$etudes = array();

			if (is_array($arrayActions) && count($arrayActions) > 0) {
				foreach ($arrayActions as $action => $arrayKeys) {
					$ratios[$action] = $this->buildRatios();
					$rows[$action] = $this->buildRows($ratios[$action], $arrayKeys);
					if (count($etudes) == 0) {
						foreach ($ratios[$action] as $ratio) {
							$etudes[] = $ratio->nomEtude;
						}
					}
				}
			}

			$tmp = array_values($ratios);
			$tmp = $tmp[0][0];
			$etudeNom = $tmp->nomEtude;

			switch ($type) {
				case 'stats16':
					if ($params['sizeMin'] != "" && $params['sizeMax'] != "") {
						if ($params['sizeMin'] == 1) {
							$sizeMin = 3;
						} else {
							$sizeMin = $params['sizeMin'];
						}
						if ($params['sizeMax'] == 999) {
							$titre = "Cumulatif " . $params['anneeMax'] . " pour l'ensemble des &eacute;tudes (de " . $sizeMin . " notaires et plus)";
						} else {
							$titre = "Cumulatif " . $params['anneeMax'] . " pour l'ensemble des &eacute;tudes (de " . $sizeMin . " &agrave; " . $params['sizeMax'] . " notaires)";
						}
					} else {
						$titre = "Cumulatif " . $params['anneeMax'] . " pour l'ensemble des &eacute;tudes";
					}
					$defaultFontSize = 8;
					$fontClasses = array(".valeur");
					$defaultColWidth = 80;
					$colClasses = array(
						"div#legalpaper table.stats16 thead tr td.nom",
						"div#legalpaper table.stats16 thead tr td.moyenne",
						"div#legalpaper table.stats16 tfoot tr td.nom",
						"div#legalpaper table.stats16 tfoot tr td.moyenne"
					);
					break;

				case 'stats11':
					$titre = "Cumulatif " . $params['anneeMax'] . " pour " . $etudeNom;
					$ss = $ratios['statsalariales_a'][0];
					$dateDeSaisie = substr($ss->completeDate, 0, 10);
					if (!$dateDeSaisie) $dateDeSaisie = "00-00-0000";
					$titre_ss = "Statistiques salariales pour " . $etudeNom . " au " . $dateDeSaisie;
					$defaultFontSize = 10;
					$fontClasses = array(".valeur");
					$defaultColWidth = 200;
					$colClasses = array(
						"div#legalpaper table.stats11 thead tr td.nom",
						"div#legalpaper table.stats11 tfoot tr td.nom"
					);
					break;

				case 'stats3':
					$titre = "Cumulatif " . $params['anneeMin'] . " - " . $params['anneeMax'] . " pour " . $etudeNom;
					$ss = $ratios['statsalariales_a'][0];
					$dateDeSaisie = substr($ss->completeDate, 0, 10);
					if (!$dateDeSaisie) $dateDeSaisie = "00-00-0000";
					$titre_ss = "Statistiques salariales pour " . $etudeNom . " au " . $dateDeSaisie;
					$defaultFontSize = 10;
					$fontClasses = array(".valeur");
					$defaultColWidth = 200;
					$colClasses = array(
						"div#legalpaper table.stats3 thead tr td.nom",
						"div#legalpaper table.stats3 tfoot tr td.nom"
					);
					break;
				case 'stats5':
					$titre = "Cumulatif " . ($params['anneeMin']-2) . " - " . $params['anneeMax'] . " pour " . $etudeNom;
					$ss = $ratios['statsalariales_a'][0];
					$dateDeSaisie = substr($ss->completeDate, 0, 10);
					if (!$dateDeSaisie) $dateDeSaisie = "00-00-0000";
					$titre_ss = "Statistiques salariales pour " . $etudeNom . " au " . $dateDeSaisie;
					$defaultFontSize = 10;
					$fontClasses = array(".valeur");
					$defaultColWidth = 200;
					$colClasses = array(
						"div#legalpaper table.stats5 thead tr td.nom",
						"div#legalpaper table.stats5 tfoot tr td.nom"
					);
					break;
			}
			$this->skin->assign('titre', $titre);
			$this->skin->assign('titre_ss', $titre_ss);

			$soustitres = array(
				'donneesgenerales' => "Donn&eacute;es g&eacute;n&eacute;rales",
				'repartitionhonoraires_a' => "R&eacute;partition des honoraires <span style=\"font-size: smaller\">(1/3)</span>",
				'repartitionhonoraires_b' => "R&eacute;partition des honoraires <span style=\"font-size: smaller\">(2/3)</span>",
				'repartitionhonoraires_c' => "R&eacute;partition des honoraires <span style=\"font-size: smaller\">(3/3)</span>",
				'rentabilite' => "Ratios de rentabilit&eacute; et de liquidit&eacute;",
				'tarification' => "Tarification",
				'recevablesalaires' => "Ratios des recevables et des salaires",
				'depenses_a' => "Ratios des d&eacute;penses <span style=\"font-size: smaller\">(1/2)</span>",
				'depenses_b' => "Ratios des d&eacute;penses <span style=\"font-size: smaller\">(2/2)</span>",
				'production_a' => "Ratios de production <span style=\"font-size: smaller\">(1/2)</span>",
				'production_b' => "Ratios de production <span style=\"font-size: smaller\">(2/2)</span>",
				'statsalariales_a' => "Statistiques salariales <span style=\"font-size: smaller\">(1/2)</span>",
				'statsalariales_b' => "Statistiques salariales <span style=\"font-size: smaller\">(2/2)</span>",
				'autres_a' => "Rapport de la saisie des donn&eacute;es <span style=\"font-size: smaller\">(1/3)</span>",
				'autres_b' => "Rapport de la saisie des donn&eacute;es <span style=\"font-size: smaller\">(2/3)</span>",
				'autres_c' => "Rapport de la saisie des donn&eacute;es <span style=\"font-size: smaller\">(3/3)</span>"
			);
			$this->skin->assign('soustitres', $soustitres);

			if ($_SESSION['user_type'] == 1) {
				$this->skin->assign('admin', 1);
			}

			$annee = $params['anneeMax'];

			$this->skin->assign('BASEURL', $BASEURL);
			$this->skin->assign('SKINURL', $SKIN_URL);
			$this->skin->assign('DEFAULT_FONT_SIZE', $defaultFontSize);
			$this->skin->assign('FONT_CLASSES', $fontClasses);
			$this->skin->assign('DEFAULT_COL_WIDTH', $defaultColWidth);
			$this->skin->assign('COL_CLASSES', $colClasses);
			$this->skin->assign('etudes', $etudes);
			$this->skin->assign('type', $type);
			$this->skin->assign('annee', $annee);
			$this->skin->assign('actions', $arrayActions);
			$this->skin->assign('ratios', $ratios);
			$this->skin->assign('rows', $rows);
			$this->skin->assign('title', 'Statistiques - ' . $titre);

			$this->skin->display("ratios/stats.tpl");

		}
	}

	function debug($str){
		 print_r("<pre>");
		 print_r($str);
		 print_r("</pre>\n");
	}

?>
