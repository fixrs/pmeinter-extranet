<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "ratios");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("ratios/index.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 4)) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "ratios");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section."));
		$Skin->display("ratios/index.tpl");
		exit;
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "ratios");


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	if ($_SESSION['user_type'] == 1) {
		$admin = 1;
		$Skin->assign('admin', 1);
	} else {
		$admin = 0;
		$Skin->assign('admin', 0);
	}

	$etudeKey = getorpost('etudeKey');
	$annee = getorpost('annee');
	$action = getorpost('action');

	switch (strtolower($action)) {
		case 'setaccesformulaire':
			setAccesFormulaire($DB);
			break;

		case 'setaccescumul':
			setAccesCumul($DB);
			break;

		case 'setvaleursannuelles':
			setPrelevementsFormulaire($DB);
			break;
	}

	$bodyparams = "onload=\"javascript: chkType(); chkAnnee();\" ";

	$Skin->assign('bodyparams', $bodyparams);
	$Skin->assign('etudesCompleteArray', getEtudesCompleteArray($DB));
	$Skin->assign('etudesArray', getEtudesArray($DB));
	$Skin->assign('ratiosControlsArray', getRatiosControlsArray($DB));
	$Skin->assign('accesCumulArray', getAccesCumulArray($DB));
	$Skin->assign('accesCumulAnneesList', getAccesCumulAnneesList($DB));
	$Skin->assign('accesStats16AnneesList', getAccesStats16AnneesList($_SESSION['user_etudes_key'], $DB));

	$Skin->assign("title", "Ratios de gestion");
	$Skin->assign('SKINURL', $SKIN_URL);
	$Skin->assign('BASEURL', $BASEURL);
	$Skin->assign('FULLURL', $FULLURL);
	$Skin->assign('currentYear', date('Y'));
	//$Skin->assign("JS", array('jquery-1.3.2.min'));
	$Skin->display("ratios/index.tpl");

	closeDbLinks();
	exit();


	// -------------
	// LES FONCTIONS
	// -------------

	function assignFromPost($skin) {
		foreach($_POST as $key => $value){
			$skin->assign($key, format($value));
		}
	}

	function isRatioComplete($etudeKey, $annee, $DB) {
		$SQL = "SELECT `key` FROM `ratios` WHERE `etudes_key` = '" . mysql_real_escape_string($etudeKey) . "' AND `annee` = " . mysql_real_escape_string($annee) . " AND `complete` = 1 LIMIT 0,1;";
		$DB->query($SQL);
		return($DB->getNumRows());
	}

	function getAccesStats16AnneesList($etudeKey, $DB) {
		$SQL = "SELECT `annee` FROM `ratios` WHERE `etudes_key` = '" . mysql_real_escape_string($etudeKey) . "' AND `complete` = 1;";

		$DB->query($SQL);
		while ($record = $DB->next_record()) {
			$accesStats16AnneesList[] = $record['annee'];
		}

		return $accesStats16AnneesList;
	}

	function getAccesCumulArray($DB) {
		$SQL =	"SELECT `annee`, `bool` FROM `cumul_acces`;";

		$DB->query($SQL);
		while ($record = $DB->next_record()) {
			$accesCumulArray[$record['annee']] = $record['bool'];
		}

		return $accesCumulArray;
	}

	function getAccesCumulAnneesList($DB) {
		$SQL = "SELECT `annee`, `bool` FROM `cumul_acces` WHERE `bool` = 1;";

		$DB->query($SQL);
		while ($record = $DB->next_record()) {
			$accesCumulAnneesList[] = $record['annee'];
		}

		return $accesCumulAnneesList;
	}

	function setAccesCumul($DB) {
		for ($y = date('Y')-5; $y < date('Y')+5; $y++) {

			$DB->query("SELECT COUNT(*) AS nb FROM `cumul_acces` WHERE `annee` = '" . $y . "'");
			$count = 0;
			while ($DB->next_record()) { $count = $DB->getField("nb"); }

			if ($count > 0) {
				$SQL =
					"UPDATE `cumul_acces` SET ".
					"	`bool` = '" . mysql_real_escape_string(getorpost('acces_cumulatif_ensemble_' . $y)) . "' ".
					"WHERE `annee` = '" . $y . "';";
			} else {
				$SQL =
					"INSERT INTO `cumul_acces` (`bool`, `annee`) VALUES ('" . mysql_real_escape_string(getorpost('acces_cumulatif_ensemble_' . $y)) . "', '" . $y . "');";
			}

			$DB->query($SQL);
		}
	}

	function setAccesFormulaire($DB) {
		for ($y = date('Y')-5; $y < date('Y')+5; $y++) {
			$SQL =
				"UPDATE `ratios_controls` SET ".
				"	`date_debut` = '" . mysql_real_escape_string(getorpost('acces_formulaire_date_debut_' . $y)) . "', ".
				"	`date_fin` = '" . mysql_real_escape_string(getorpost('acces_formulaire_date_fin_' . $y)) . "', ".
				"	`ouvert` = '" . mysql_real_escape_string(getorpost('acces_formulaire_ouvert_' . $y)) . "' ".
				//"	`prelevements` = '" . mysql_real_escape_string(getorpost('valeurs_prelevements_' . $y)) . "' ".
				"WHERE `annee` = '" . $y . "';";
			$DB->query($SQL);
		}
	}

	function setPrelevementsFormulaire($DB) {
		for ($y = date('Y')-5; $y < date('Y')+5; $y++) {
			$SQL =
				"UPDATE `ratios_controls` SET ".
				"	`prelevements` = '" . mysql_real_escape_string(getorpost('valeurs_prelevements_' . $y)) . "' ".
				"WHERE `annee` = '" . $y . "';";
			$DB->query($SQL);
		}
	}




	function getRatiosControlsArray($DB) {
		$ratiosControlsArray = array();
		$SQL = "SELECT * FROM `ratios_controls`";
		$DB->query($SQL);
		while ($record = $DB->next_record()) {
			$ratiosControlsArray[$record['annee']]['acces_formulaire']['annee'] = $record['annee'];
			$ratiosControlsArray[$record['annee']]['acces_formulaire']['date_debut'] = $record['date_debut'];
			$ratiosControlsArray[$record['annee']]['acces_formulaire']['date_fin'] = $record['date_fin'];
			$ratiosControlsArray[$record['annee']]['acces_formulaire']['ouvert'] = $record['ouvert'];
			$ratiosControlsArray[$record['annee']]['valeurs_annuelles']['prelevements'] = $record['prelevements'];
		}

		// echo "<!--";
		// print_r($ratiosControlsArray);
		// echo "-->";

		return $ratiosControlsArray;
	}

	function getEtudesArray($DB) {
		$etudesArray = array();
		switch ($_SESSION['user_type']) {
			case 1:
				$SQL = "SELECT `key`, `nom` FROM `etudes` WHERE `actif` = '1';";
				break;
			case 2:
				$SQL = "SELECT `key`, `nom` FROM `etudes` WHERE `key` = '" . $_SESSION['user_etudes_key'] . "' AND `actif` = '1';";
				break;
			case 4:
				$SQL = "SELECT `key`, `nom` FROM `etudes` WHERE `key` = '" . $_SESSION['user_etudes_key'] . "' AND `actif` = '1';";
				break;
		}
		$DB->query($SQL);
		while ($record = $DB->next_record()) {
			$etudesArray[$record['key']] = $record['nom'];
		}

		return $etudesArray;
	}

	function getEtudesCompleteArray($DB) {
		if ($_SESSION['user_type'] == 1) {
			$SQL =
				"SELECT e.key AS etudeKey, e.nom AS etudeNom ".
				"FROM `etudes` AS e, `ratios` AS r ".
				"WHERE r.etudes_key = e.key AND r.complete = '1';";

		} elseif ($_SESSION['user_type'] == 4) {
			$SQL =
				"SELECT e.key AS etudeKey, e.nom AS etudeNom ".
				"FROM `etudes` AS e, `ratios` AS r ".
				"WHERE r.etudes_key = e.key AND e.key = '" . $_SESSION['user_etudes_key'] . "' AND r.complete = '1';";
		}
		$etudesCompleteArray = array();
		$DB->query($SQL);
		while ($DB->next_record()) {
			$etudesCompleteArray[$DB->getField('etudeKey')] = $DB->getField('etudeNom');
		}

		return $etudesCompleteArray;
	}

	function closeDbLinks() {
		global $DB, $DB2, $DB3;
		if ($DB != "") {
			$DB->close();
		}
		if ($DB2 != "") {
			$DB2->close();
		}
		if ($DB3 != "") {
			$DB3->close();
		}
	}

?>
