<?php

	//require "../conf/conf.inc.php";
	global $DB;

	/** PHPExcel */
	include 'PHPExcel.php';

	require_once 'PHPExcel/IOFactory.php';

	$path = str_replace("excel.php", "", __FILE__);

	//$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);

	/*
		key
		etudes_key
		nom_utilisateur
		nb_notaires_associes
		nb_notaires_salaries
		nb_total_notaires
	nb_employes
		nb_stagieres
	dossier_mandat
	nb_dossiers
	nb_minutes
	minutes_excluent_radiation
	nb_societes
	nb_feducies
	nb_dossiers_ag
	nb_dossiers_mc
		facturation
		chiffre_affaires
		debours_refactures
		benefices_nets
		depenses_totales
		salaires
		salaires_coll
		salaires_notaires
		depense_loyer
		depense_debourses_dossiers
		depense_inters_prets_marge
		depense_amortissement
		depense_telecom
		depense_soutien
		depense_publicite
		depense_representation
		autres_depenses
	recevables
	travaux_en_cours
	total_actifs
	total_passifs
	capital_associes
	nb_notaires
		nb_collaborateurs
	nb_heures_facturables_annee
	nb_heures_facturables_collaborateurs_annee
	secretaire_juridique_junior
	secretaire_juridique_junior_min
	secretaire_juridique_senior
	secretaire_juridique_senior_min
	technicien_juridique_junior
	technicien_juridique_junior_min
	technicien_juridique_senior
	technicien_juridique_senior_min
	receptionniste
	receptionniste_min
	adjointe_administrative
	adjointe_administrative_min
	notaire_salarie_junior
	notaire_salarie_junior_min
	notaire_salarie_senior
	notaire_salarie_senior_min
	stagiaire_notariat
	stagiaire_notariat_min
	stagiaire_techniques_juridiques
	stagiaire_techniques_juridiques_min
		immobilier_residentiel
		immobilier_residentiel_diff
		immobilier_existant
		immobilier_neuf
		commercial_corporatif
		commercial_corporatif_diff
		commercial
		immobilier_comm_ind
		corporatif
		service_corporatif
		personne_succession_familial
		personne_succession_familial_diff
		testaments_mandats
		donations_fiduciaires
		sucessions
		mediation_familiale
		procedures_non_content
		protection_patrimoine
		autres_personne_familial
		agricole_municipale_administratif
		agricole_municipale_administratif_diff
		agricole
		municipal_administratif
		divers_total
		divers
		divers_diff
	accepte_mandats_exterieurs
	taux_remboursement
	numerisation_pieces
	forfait_honoraires_residentiel
	forfait_honoraires_testaments
	forfait_honoraires_corporatif
	forfait_honoraires_feducie
	forfait_honoraires_ag
	forfait_honoraires_mc
	forfait_honoraires_hypotheque
	forfait_honoraires_incorporation
	forfait_honoraires_hmn
	forfait_honoraires_rpm
	forfait_honoraires_htn
	taux_horaire_notaire_moins_de_5_ans
	taux_horaire_notaire_5_a_10_ans
	taux_horaire_notaire_plus_de_10_ans
	taux_horaire_collaborateur
	complete
	complete_date
	annee
	*/






	$DB->query(
	 	"SELECT r.*, e.nom AS etude_nom ".
	 	"FROM ratios r INNER JOIN etudes e ON r.etudes_key = e.key ".
	 	"WHERE complete = '1' ".
	 	"AND annee = '" . (date("Y") - 1) . "' ".
	 	"AND email_sent = '0'"
	);

	$ratios_key = array();

	$etude = "";
	while ($row = $DB->next_record()) {
		$objReader = PHPExcel_IOFactory::createReader('Excel2007');

		$objPHPExcel = $objReader->load($path . "ratios-verification.xlsx");

		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->SetCellValue("C4", $DB->getField("etude_nom"));
		$etude = preg_replace("/__/", "_", preg_replace("/[^a-zA-Z]/", "_", $DB->getField("etude_nom")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C5", $DB->getField("nom_utilisateur"));

		$objPHPExcel->getActiveSheet()->SetCellValue("C9", toNumber($DB->getField("facturation")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C10", toNumber($DB->getField("chiffre_affaires")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C11", toNumber($DB->getField("debours_refactures")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C12", toNumber($DB->getField("benefices_nets")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C18", toNumber($DB->getField("salaires_notaires")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C19", toNumber($DB->getField("salaires_coll")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C20", toNumber($DB->getField("salaires")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C21", toNumber($DB->getField("depense_loyer")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C22", toNumber($DB->getField("depense_debourses_dossiers")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C23", toNumber($DB->getField("depense_inters_prets_marge")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C24", toNumber($DB->getField("depense_amortissement")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C25", toNumber($DB->getField("depense_telecom")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C26", toNumber($DB->getField("depense_soutien")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C27", toNumber($DB->getField("depense_publicite")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C28", toNumber($DB->getField("depense_representation")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C29", toNumber($DB->getField("autres_depenses")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C31", toNumber($DB->getField("depenses_totales")));

		$objPHPExcel->getActiveSheet()->SetCellValue("C36", toNumber(($DB->getField("immobilier_residentiel") + $DB->getField("commercial_corporatif") + $DB->getField("personne_succession_familial") + $DB->getField("agricole_municipale_administratif") + $DB->getField("divers_total"))));
		$objPHPExcel->getActiveSheet()->SetCellValue("C38", toNumber($DB->getField("immobilier_residentiel")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C39", toNumber($DB->getField("commercial_corporatif")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C40", toNumber($DB->getField("personne_succession_familial")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C41", toNumber($DB->getField("agricole_municipale_administratif")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C42", toNumber($DB->getField("divers_total")));

		$objPHPExcel->getActiveSheet()->SetCellValue("C47", toNumber($DB->getField("nb_notaires_associes")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C48", toNumber($DB->getField("nb_notaires_salaries")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C49", toNumber($DB->getField("nb_total_notaires")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C50", toNumber($DB->getField("nb_stagieres")));
		$objPHPExcel->getActiveSheet()->SetCellValue("C51", toNumber($DB->getField("nb_collaborateurs")));

		$objPHPExcel->getActiveSheet()->SetCellValue("B57", "Date : " . date("Y-m-d"));

		$objPHPExcel->setActiveSheetIndex(1);
		$objPHPExcel->getActiveSheet()->SetCellValue("C4", $DB->getField("etude_nom"));
		$objPHPExcel->getActiveSheet()->SetCellValue("C5", $DB->getField("nom_utilisateur"));
		$objPHPExcel->getActiveSheet()->SetCellValue("D5", $DB->getField("email_utilisateur"));
        $objPHPExcel->getActiveSheet()->SetCellValue("E5", $DB->getField("phone_utilisateur"));
		$objPHPExcel->getActiveSheet()->SetCellValue("B57", "Date : " . date("Y-m-d"));

		$filename = "Ratio-" . $etude . "-" . date("Y-m-d") . ".xlsx";

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save($path . 'xls/' . $filename);

		$ratios_key[] = $DB->getField("key");

		echo "http://bd.pmeinter.ca/ratios/xls/" . $filename . "\n";
	}

	foreach ($ratios_key as $key) {
		$DB->query("UPDATE `ratios` SET `email_sent` = 1 WHERE `key` = '" . $key . "'");
	}

	//$DB->close();


	function toNumber($string) {

		$number = str_replace(',', '.', $string);
		$number = str_replace(' ', '', $number);

		return $number;
	}
?>
