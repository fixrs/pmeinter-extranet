<?php

    require "../conf/conf.inc.php";

    /** PHPExcel */
    include 'PHPExcel.php';

    require_once 'PHPExcel/IOFactory.php';

    $path = str_replace("verif.php", "", __FILE__);

    $DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);

    $DB->query(
        "SELECT r.*, e.nom AS etude_nom ".
        "FROM ratios r INNER JOIN etudes e ON r.etudes_key = e.key ".
        "WHERE complete = '1' ".
        "AND annee = '" . $_GET["annee"] . "' ".
        "AND etudes_key = '" . $_GET["etude"] . "'"
    );

    $ratios_key = array();

    $etude = "";
    while ($row = $DB->next_record()) {
        $objReader = PHPExcel_IOFactory::createReader('Excel2007');

        $objPHPExcel = $objReader->load($path . "ratios-verification.xlsx");

        $objPHPExcel->setActiveSheetIndex(0);
        $objPHPExcel->getActiveSheet()->SetCellValue("C4", $DB->getField("etude_nom"));
        $etude = preg_replace("/__/", "_", preg_replace("/[^a-zA-Z]/", "_", $DB->getField("etude_nom")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C5", $DB->getField("nom_utilisateur"));
        $objPHPExcel->getActiveSheet()->SetCellValue("C9", toNumber($DB->getField("facturation")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C10", toNumber($DB->getField("chiffre_affaires")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C11", toNumber($DB->getField("debours_refactures")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C12", toNumber($DB->getField("benefices_nets")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C18", toNumber($DB->getField("salaires_notaires")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C19", toNumber($DB->getField("salaires_coll")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C20", toNumber($DB->getField("salaires")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C21", toNumber($DB->getField("depense_loyer")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C22", toNumber($DB->getField("depense_debourses_dossiers")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C23", toNumber($DB->getField("depense_inters_prets_marge")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C24", toNumber($DB->getField("depense_amortissement")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C25", toNumber($DB->getField("depense_telecom")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C26", toNumber($DB->getField("depense_soutien")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C27", toNumber($DB->getField("depense_publicite")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C28", toNumber($DB->getField("depense_representation")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C29", toNumber($DB->getField("autres_depenses")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C31", toNumber($DB->getField("depenses_totales")));

        $objPHPExcel->getActiveSheet()->SetCellValue("C36", toNumber($DB->getField("immobilier_residentiel")) + toNumber($DB->getField("commercial_corporatif")) + toNumber($DB->getField("personne_succession_familial")) + toNumber($DB->getField("agricole_municipale_administratif")) + toNumber($DB->getField("divers_total")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C38", toNumber($DB->getField("immobilier_residentiel")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C39", toNumber($DB->getField("commercial_corporatif")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C40", toNumber($DB->getField("personne_succession_familial")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C41", toNumber($DB->getField("agricole_municipale_administratif")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C42", toNumber($DB->getField("divers_total")));

        $objPHPExcel->getActiveSheet()->SetCellValue("C47", toNumber($DB->getField("nb_notaires_associes")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C48", toNumber($DB->getField("nb_notaires_salaries")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C49", toNumber($DB->getField("nb_total_notaires")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C50", toNumber($DB->getField("nb_stagieres")));
        //$objPHPExcel->getActiveSheet()->SetCellValue("C51", toNumber($DB->getField("nb_collaborateurs")));
        $objPHPExcel->getActiveSheet()->SetCellValue("C51", toNumber($DB->getField("nb_employes")));

        $objPHPExcel->getActiveSheet()->SetCellValue("B57", "Date : " . date("Y-m-d"));

        $objPHPExcel->setActiveSheetIndex(1);
        $objPHPExcel->getActiveSheet()->SetCellValue("C4", $DB->getField("etude_nom"));
        $objPHPExcel->getActiveSheet()->SetCellValue("C5", $DB->getField("nom_utilisateur") );
        $objPHPExcel->getActiveSheet()->SetCellValue("D5", $DB->getField("email_utilisateur"));
        $objPHPExcel->getActiveSheet()->SetCellValue("E5", $DB->getField("phone_utilisateur"));
        $objPHPExcel->getActiveSheet()->SetCellValue("B57", "Date : " . date("Y-m-d"));


        $honoraires = toNumber($DB->getField("immobilier_residentiel")) + toNumber($DB->getField("commercial_corporatif")) + toNumber($DB->getField("personne_succession_familial")) + toNumber($DB->getField("agricole_municipale_administratif")) + toNumber($DB->getField("divers_total"));

        // $objPHPExcel->getActiveSheet()->SetCellValue("J10", toNumber($honoraires - $DB->getField("debours_refactures")));
        // $objPHPExcel->getActiveSheet()->SetCellValue("J11", toNumber($DB->getField("chiffre_affaires")) - $honoraires);
        // $objPHPExcel->getActiveSheet()->SetCellValue("J12", toNumber($DB->getField("chiffre_affaires")) - toNumber($DB->getField("depenses_totales")));

        $filename = "Ratio-" . $etude . "-" . date("Y-m-d") . ".xlsx";

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($path . 'xls/' . $filename);

        $ratios_key[] = $DB->getField("key");

        //echo "http://bd.pmeinter.ca/ratios/xls/" . $filename . "\n";
    }

    // foreach ($ratios_key as $key) {
    //     $DB->query("UPDATE `ratios` SET `email_sent` = 1 WHERE `key` = '" . $key . "'");
    // }

    $DB->close();


    function toNumber($string) {

        $number = str_replace(',', '.', $string);
        $number = str_replace(' ', '', $number);

        return $number;
    }


    header("Location: http://bd.pmeinter.ca/ratios/xls/" . $filename);


