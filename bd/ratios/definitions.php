<?PHP

	$dl = $_GET['dl'];
	$dt = $_GET['dt'];
	$html = 'definitions-' . $dl . '.html';

	$class[$dt] = "active";

	echo
		"<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n".
		"<html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"fr\" lang=\"fr\">\n".
		"<head>\n".
		"	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n".
		"	<title>D&eacute;finitions - Ratios - PME INTER Notaires</title>\n".
		"	<link rel=\"stylesheet\" href=\"/extranet/skin/css/definitions.css\" type=\"text/css\" media=\"screen\" />\n".
		"	<link rel=\"stylesheet\" href=\"/extranet/skin/css/definitions.css\" type=\"text/css\" media=\"print\" />\n".
		"	<link rel=\"shortcut icon\" type=\"image/x-icon\" href=\"/favicon.ico\" />\n".
		"</head>\n".
		"<body>\n";

	//echo "<center><h1>D&eacute;finitions - Titres</h1></center>";


	if (strpos('/', $html) === false && file_exists($html)) {
		include($html);
	}

	echo
		"</body>\n".
		"</html>\n";
?>
