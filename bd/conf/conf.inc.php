<?PHP

	$DEV = 0;

	$DOMAIN			=	"bd.pmeinter.ca";
	$DOMAIN			= 	"pmeinter.com/extranet/";
	$BASEPATH		=	"/var/www/pmeinter.ca/public_html/bd/";
	$BASEPATH		=	dirname(__DIR__) . "/";
	$BASEURL		=	"/extranet/bd/";
	$FULLURL		=	"http://" . $DOMAIN . $BASEURL;

	$PAGES_URL		=	$BASEURL . "";
	$SKIN_URL		=	$BASEURL . "skin";
	$JS_URL			=	$SKIN_URL . "/js";
	$CSS_URL		=	$SKIN_URL . "/css";

	$CONFS_PATH		=	$BASEPATH . "conf";
	$FORMS_PATH		=	$BASEPATH . "forms";
	$PACKAGES_PATH		=	$BASEPATH . "src";
	$SKIN_PATH		=	$BASEPATH . "skin";

	define("SMARTY_DIR", $PACKAGES_PATH . "/net/php/Smarty/");

	require_once $PACKAGES_PATH."/com/quiboweb/util/Import.php";

	importBD("com.quiboweb.util.Security");
	importBD("com.quiboweb.util.General");
	//importBD("com.quiboweb.sql.DB");
	importBD("com.pmeinter.Skin");
	importBD("com.pmeinter.User");

	$DB_NAME		=	"pmeinter_bd";
	$DB_HOSTNAME		=	"104.131.253.35";
	// $DB_HOSTNAME		=	"localhost";
	// $DB_USER		=	"pmeinter_bd";
	// $DB_PASSWORD		=	"i4nH289WpXc2014";
	$DB_USER = "pmeinter_user";
	$DB_PASSWORD = "S0467k3F3e85y4K";
	//$DB_PASSWORD		=	"i4nH289WpXc2";

	$DBMS			=	"MySQL";
	$DB_TYPE		=	"plain";

	$_FORM = "";

	$PHONE_FORMAT_PATTERN = "/^((\d+-)?\d{3}-\d{3}-\d{4}|011-\d{4}-\d{3}-\d{4})?$/";
	$POSTAL_CODE_FORMAT_PATTERN = "/^([a-z]\d[a-z]\s\d[a-z]\d)?$/i";
	$HTTP_FORMAT_PATTERN = "/^https?:\/\//";
	$EMAIL_FORMAT_PATTERN = "/^[\\w_\\.\\-]+\\@[\\w_\\-]+\\.[\\w_\\-]+(\\.[\\w_\\-]+)?(\\.[\\w_\\-]+)?(\\.[\\w_\\-]+)?(\\.[\\w_\\-]+)?\$/";
	$PHONE_FORMAT = "1-514-123-4567, 011-1234-567-8910";
	$TOLL_FREE_FORMAT = "1-800-123-4567";
	$POSTAL_CODE_FORMAT = "H0H 0H0";

	$NB_NOUVELLES = 4;

	$DB_TABLES = array(
		"comments",
		"directions_de_travail",
		"domaines_daffaires",
		"dossiers",
		"dossiers_applications",
		"dossiers_alertes",
		"employes",
		"employes_etudes_succursales",
		"employes_expertises",
		"employes_fonctions",
		"employes_titres",
		"etudes",
		"etudes_succursales",
		"evaluations",
		"evaluations_domaines_daffaires_collaboratrices",
		"evaluations_domaines_daffaires_notaires",
		"evaluations_employes",
		"evaluations_employes_fonctions",
		"evaluations_etudes_succursales",
		"evaluations_formations_collaboratrices_domaines_daffaires",
		"evaluations_formations_notaires_domaines_daffaires",
		"evaluations_notaires",
		"evaluations_notaires_directions_de_travail",
		"evaluations_notaires_titres",
		"evaluations_produits_articles_promotionnels",
		"evaluations_produits_papeterie_utilisee",
		"evaluations_produits_services_reseau",
		"evaluations_produits_visibles",
		"evaluations_responsables_greffe",
		"evaluations_responsables_greffe_fonctions",
		"evaluations_signataires_adhesion",
		"evenements",
		"evenements_participation",
		"expertises",
		"fonctions",
		"formations",
		"formations_participation",
		"produits",
		"produits_categories",
		"ratios",
		"titres",
		"utilisateurs"
	);

	if (return_bytes(ini_get('post_max_size')) < return_bytes(ini_get('upload_max_filesize'))) {
		$UPLOAD_SIZE_LIMIT = return_bytes(ini_get('post_max_size'));
	} else {
		$UPLOAD_SIZE_LIMIT = return_bytes(ini_get('upload_max_filesize'));
	}

	$EMPLOYES_IMG_MAX_SIZE = 2000000;
	if ($EMPLOYES_IMG_MAX_SIZE > $UPLOAD_SIZE_LIMIT) {
		$EMPLOYES_IMG_MAX_SIZE = $UPLOAD_SIZE_LIMIT;
	}

	$ETUDES_IMG_MAX_SIZE = 2000000;
	if ($ETUDES_IMG_MAX_SIZE > $UPLOAD_SIZE_LIMIT) {
		$ETUDES_IMG_MAX_SIZE = $UPLOAD_SIZE_LIMIT;
	}

	$PRODUITS_IMG_MAX_SIZE = 1000000;
	if ($PRODUITS_IMG_MAX_SIZE > $UPLOAD_SIZE_LIMIT) {
		$PRODUITS_IMG_MAX_SIZE = $UPLOAD_SIZE_LIMIT;
	}

	$EVALUATIONS_DOC_MAX_SIZE = 6000000;
	if ($EVALUATIONS_DOC_MAX_SIZE > $UPLOAD_SIZE_LIMIT) {
		$EVALUATIONS_DOC_MAX_SIZE = $UPLOAD_SIZE_LIMIT;
	}

	$EVALUATIONS_SECTIONS = array(
		"1"		=>	"Coordonn&eacute;es de l'&eacute;tude",
		"2"		=>	"Coordonn&eacute;es des notaires",
		"3"		=>	"Coordonn&eacute;es du personnel de l'&eacute;tude",
		"4"		=>	"Contrat d'adh&eacute;sion",
		"5"		=>	"Identification du r&eacute;seau",
		"6"		=>	"Exploitation des trois domaines d'affaires",
		"7"		=>	"Implication au sein des directions de travail",
		"8"		=>	"Droit des affaires",
		"9"		=>	"Droit des personnes",
		"10"		=>	"Droit immobilier",
		"11"		=>	"Marketing",
		"12"		=>	"Communications",
		"13"		=>	"Participation",
		"14"		=>	"Cabinet financier",
		"15"		=>	"G&eacute;n&eacute;ral",
		"16"		=>	"Rencontre avec les notaires",
		"17"		=>	"Section d&eacute;di&eacute;e aux collaboratrices et aux techniciennes",
		"18"		=>	"Droit des affaires",
		"19"		=>	"Droit des personnes",
		"20"		=>	"Droit immobilier",
		"21"		=>	"G&eacute;n&eacute;ral",
		"22"		=>	"Communications"
	);

	$EVALUATIONS_SECTIONS_PATTERNS = array(
		"all"		=> 	"/.?/",
		"1"		=>	"/^(s1_|etudes_succursales_)/",
		"2"		=>	"/^(s2_|notaires_employes_key|notaires_nom|notaires_prenom|notaires_courriel|notaires_annee_debut_pratique|notaires_associe|notaires_present_evaluation|notaires_raison_absence|notaires_titres_employes_key|notaires_titres_titres_key)/",
		"3"		=>	"/^(s3_|employes_employes_key|employes_nom|employes_prenom|employes_courriel|employes_present_evaluation|employes_raison_absence|employes_fonctions_key)/",
		"4"		=>	"/^(s4_|signataires_adhesion_)/",
		"5"		=>	"/^(s5_|produits_visibles_|produits_papeterie_utilisee_)/",
		"6"		=>	"/^(s6_)/",
		"7"		=>	"/^(s7_|notaires_directions_de_travail_droit_des_affaires_|notaires_directions_de_travail_droit_des_personnes_|notaires_directions_de_travail_gestion_des_bureaux_)/",
		"8"		=>	"/^(s8_|domaines_daffaires_notaires_droit_des_affaires_|domaines_daffaires_collaboratrices_droit_des_affaires_|formations_notaires_domaines_daffaires_formation_greffe_corpo_|formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_|formations_notaires_domaines_daffaires_formation_assistant_corpo_|formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_)/",
		"9"		=>	"/^(s9_|domaines_daffaires_notaires_droit_des_personnes_|domaines_daffaires_collaboratrices_droit_des_personnes_|formations_notaires_domaines_daffaires_formation_testament_fiduciaire_|formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_)/",
		"10"	=>	"/^(s10_|domaines_daffaires_notaires_droit_immo_|domaines_daffaires_collaboratrices_droit_immo_|formations_notaires_domaines_daffaires_formation_assistant_immo_|formations_collaboratrices_domaines_daffaires_formation_assistant_immo_)/",
		"11"	=>	"/^(s11_|produits_articles_promotionnels_)/",
		"12"	=>	"/^(s12_)/",
		"13"	=>	"/^(s13_)/",
		"14"	=>	"/^(s14_)/",
		"15"	=>	"/^(s15_)/",
		"16"	=>	"/^(s16_)/",
		"17"	=>	"/^(s17_)/",
		"18"	=>	"/^(s18_)/",
		"19"	=>	"/^(s19_)/",
		"20"	=>	"/^(s20_)/",
		"21"	=>	"/^(s21_)/",
		"22"	=>	"/^(s22_)/"
	);

	function isAuthenticated() {
		global $DEV;
		if (!$DEV && isset($_SESSION['user_dev']) && $_SESSION['user_dev']) {
			$_SESSION = array();
		}
		if (isset($_SESSION['user_key']) && $_SESSION['user_key'] != "" && $_SESSION['user_key'] != NULL) {
			return TRUE;
		}
		return FALSE;
	}

	function connect($DB) {

		$current_user = wp_get_current_user();

		if (!isset($_SESSION['user_key']) || (isset($_SESSION['user_key']) && $_SESSION['user_key'] == "") ) {

			$employes_key_array = array();
			$utilisateurs = array();
			$DB->query("SELECT * FROM utilisateurs  ORDER BY `type` ASC");//WHERE actif = 1
			while ($row = $DB->next_record()) {
				$utilisateurs[$row["courriel"]] = array(
					"user_key" => $row["key"],
					"etudes_key" => $row["etudes_key"],
					"nom" => $row["nom"],
					"prenom" => $row["prenom"],
					"courriel" => $row["courriel"],
					"user_type" => $row["type"]
				);

				$employes_key_array[$row["employes_key"]] = $row["key"];
			}

			$sqls = array();

			$DB->query(
				"SELECT DISTINCT e.*, es.etudes_key ".
				"FROM employes e INNER JOIN employes_etudes_succursales ees ON e.key = ees.employes_key ".
				"INNER JOIN etudes_succursales es ON es.key = ees.etudes_succursales_key ".
				"WHERE es.actif = 1 AND e.actif = 1"
			);

			while ($row = $DB->next_record()) {
				if (!isset($employes_key_array[$row["key"]])) {
					$utilisateurs[$row["courriel"]] = array(
						"user_key" => '', //$row["key"],
						"etudes_key" => $row["etudes_key"],
						"nom" => $row["nom"],
						"prenom" => $row["prenom"],
						"courriel" => $row["courriel"],
						"user_type" => 2
					);

					if ($row["etudes_key"] == "") { $row["etudes_key"] = 0; }

					$sql =
						"INSERT INTO `utilisateurs` (
							`key`,
							etudes_key,
							`type`,
							`password`,
							actif,
							nom,
							prenom,
							courriel,
							employes_key
						) VALUES (
							'" . preg_replace("/[^0-9a-z]*/is", "", strtolower($row["courriel"])) . "_" . $row["key"] . "',
							" . $row["etudes_key"] . ",
							2,
							'auto-pass-2018-09!',
							1,
							'" . addslashes($row["nom"]) . "',
							'" . addslashes($row["prenom"]) . "',
							'" . addslashes(strtolower($row["courriel"])) . "',
							" . $row["key"] . "
						)";

					$sqls[] = $sql;

				}

			}

			if (count($sqls) > 0) {
				foreach ($sqls as $i => $sql) {
					$DB->query($sql);
				}
			}

		}

	    $DB->query("SELECT * FROM utilisateurs WHERE courriel = '" . $current_user->user_email . "' AND actif = 1 ORDER BY `type` ASC");

	   	while ($row = $DB->next_record()) {

		   	$User = new User($DB, "", "");
			$User->scanfields();
			$User->setQKey("key", $row["key"]);
			$User->loadAll();

			$_SESSION = array();
			$_SESSION['user_key'] = $User->get("key");
			$_SESSION['employes_key'] = $User->get("employes_key");
			$_SESSION['user_type'] = $User->get("type");
			$_SESSION['user_fullname'] = $User->get("prenom") . " " . $User->get("nom");
			$_SESSION['user_lastlogon'] = $User->get("lastLogon");
			$_SESSION['user_etudes_key'] = $User->get('etudes_key');

			$User->set("lastLogon", "CURRENT_TIMESTAMP", 1);
			$User->save("lastLogon");

	   	}


	   	// $_SESSION['user_key'] = 148;
	   	// $_SESSION['user_type'] = 2;
	   	// $_SESSION['user_fullname'] = "Louis Vincent";
	   	// $_SESSION['user_etudes_key'] = 24;
	   	//echo "Test : " . $_SESSION['employes_key'];
	}

	noMagicQuotes();


	function translateDate($date) {
		$year = preg_replace("/^([0-9][0-9][0-9][0-9])-([0-9][0-9])-([0-9][0-9])$/", "$1", $date);
		$month = preg_replace("/^([0-9][0-9][0-9][0-9])-([0-9][0-9])-([0-9][0-9])$/", "$2", $date);
		$day = preg_replace("/^([0-9][0-9][0-9][0-9])-([0-9][0-9])-([0-9][0-9])$/", "$3", $date);
		$day = preg_replace("/^0/", "", $day);
		$month = preg_replace("/^0/", "", $month); $month = $month * 1;

		$months = array("","janvier","f&eacute;vrier","mars","avril","mai","juin","juillet","ao&ucirc;t","septembre","octobre","novembre","d&eacute;cembre");


		return $day . " " . $months[$month] . " " . $year;

	}

