<?PHP
	require "../conf/conf.inc.php";

	session_start();
	
	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "ratios");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	import("com.quiboweb.form.Form");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$DB2 = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
	$Skin->assign("title", "R&eacute;seau IQ");
	$Skin->assign('BASEURL', $BASEURL);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	if ($_SESSION['user_type'] == 1) {
		$admin = 1;
		$Skin->assign('admin', 1);
	} else {
		$admin = 0;
		$Skin->assign('admin', 0);
	}

	$action = getorpost('action');
	$etudeKey = getorpost('etudeKey');
	$factureKey = getorpost('factureKey');

	$valuesToLoad = array();
	$valuesToSave = array();
	$noticeArray = array();
	$errorArray = array();

	if ($etudeKey != '') {
		if (!$admin && $etudeKey != $_SESSION['user_etudes_key']) {
			exit("Vous ne disposez pas des droits pour effectuer cette op&eacute;ration.");
		} else {
			$etudeNom = getEtudeNom($etudeKey);
		}
	} else {
		$etudeNom = '';
	}

	switch (strtolower($action)) {
		case 'ajout':
			$bodyparams = "onLoad=\"javascript: toggleMustAll();\"";
			$notaireNom = getNotaireNom($etudeKey);
			$Skin->assign('etudeKey', $etudeKey);
			$Skin->assign('etudeNom', $etudeNom);
			$Skin->assign('notaireNom', $notaireNom);
			$Skin->assign('bodyparams', $bodyparams);
			$Skin->assign("JS", array('reseauiq', 'feedback'));
			$Skin->assign("CSS", array('reseauiq'));
			$Skin->display("reseauiq/reseauiq.tpl");
			break;

		case 'save':
			$errorArray = getErrorArray(getValuesFromPost());
			if (count($errorArray) > 0) {
				$Skin->assign('errorArray', $errorArray);
				assignValuesToSkin($Skin, getValuesFromPost());
			} else {
				$save = setValuesIntoDb($DB, getValuesFromPost());
				$Skin->assign('noForm', 1);
				if ($save) {
					$noticeArray[] = "La facture a &eacute;t&eacute; enregistr&eacute;e avec succ&egrave;s.";
					$Skin->assign('link', "<a href=\"reseauiq.php?action=ajout&amp;factureKey=" . $factureKey . "&amp;etudeKey=" . $etudeKey . "\"><strong>Cliquez ici pour ajouter une nouvelle facture.</strong></a>\n");
				} else {
					$errorArray[] = "Une erreur s'est produite lors de l'enregistrement de la facture.";
					$Skin->assign('link', "<a href=\"reseauiq.php\"><strong>Cliquez ici pour revenir au menu pr&eacute;c&eacute;dent.</strong></a>\n");
				}
			}

			$bodyparams = "onLoad=\"javascript: toggleMustAll();\"";
			$notaireNom = getNotaireNom($etudeKey);
			$Skin->assign('etudeKey', $etudeKey);
			$Skin->assign('etudeNom', $etudeNom);
			$Skin->assign('notaireNom', $notaireNom);			
			$Skin->assign('bodyparams', $bodyparams);
			$Skin->assign('errorArray', $errorArray);
			$Skin->assign('noticeArray', $noticeArray);
			$Skin->assign("JS", array('reseauiq', 'feedback'));
			$Skin->assign("CSS", array('reseauiq'));
			$Skin->display("reseauiq/reseauiq.tpl");
			break;

		case 'modify':
			if ($admin) {
				assignValuesToSkin($Skin, getValuesFromDb($DB, $factureKey));
			} else {
				$Skin->assign('noRight', 1);
				$Skin->assign('noForm', 1);
			}

			$Skin->assign('etudeKey', $etudeKey);
			$Skin->assign('etudeNom', $etudeNom);
			$Skin->assign('errorArray', $errorArray);
			$Skin->assign('noticeArray', $noticeArray);
			$Skin->assign("JS", array('reseauiq', 'feedback'));
			$Skin->assign("CSS", array('reseauiq'));
			$Skin->display("reseauiq/reseauiq.tpl");
			break;

		case 'delete':
			if ($admin) {
				$delete = deleteFactureFromDb($factureKey);
				if ($delete) {
					$noticeArray[] = "La facture a &eacute;t&eacute; supprim&eacute;e avec succ&egrave;s.";
					$Skin->assign('link', "<a href=\"reseauiq.php\"><strong>Cliquez ici pour revenir au menu pr&eacute;c&eacute;dent.</strong></a>\n");
				} else {
					$errorArray[] = "Une erreur s'est produite lors de la suppression de la facture.";
					$Skin->assign('link', "<a href=\"reseauiq.php\"><strong>Cliquez ici pour revenir au menu pr&eacute;c&eacute;dent.</strong></a>\n");
				}
			} else {
				$Skin->assign('noRight', 1);
			}

			$Skin->assign('noForm', 1);
			$Skin->assign('etudeKey', $etudeKey);
			$Skin->assign('etudeNom', $etudeNom);
			$Skin->assign('errorArray', $errorArray);
			$Skin->assign('noticeArray', $noticeArray);
			$Skin->assign("JS", array('reseauiq', 'feedback'));
			$Skin->assign("CSS", array('reseauiq'));
			$Skin->display("reseauiq/reseauiq.tpl");
			break;

		case 'rapport':
			$typeRapport = trim(getorpost('type_rapport'));
			$dateDebut = trim(getorpost('date_debut'));
			$dateFin = trim(getorpost('date_fin'));

			if ((!$dateDebut || $dateDebut == '') && isDateValid($dateFin)) {
				$dateFinSplit = explode('-', $dateFin);
				$dateDebut = ($dateFinSplit[0]-1) . '-' . $dateFinSplit[1] . '-' . $dateFinSplit[2];
			}

			if (is_array(getorpost('tableau')) && count(getorpost('tableau')) > 0) {
				foreach (getorpost('tableau') as $tableau) {
					if ($tableau == 'sommaire') $tableauSommaireBool = 1;
					if ($tableau == 'details') $tableauDetailsBool = 1;
				}
			}

			if (substr($typeRapport, -7) == "_single") {
				$notaireNom = getNotaireNom($etudeKey);
				$factureDetailsForOneArray = getFactureDetailsForOneArray($etudeKey, $dateDebut, $dateFin);
				$factureSommaireForOneArray = getFactureSommaireForOneArray($etudeKey, $dateDebut, $dateFin);
				$nbFactures = count($factureDetailsForOneArray);

			} elseif (substr($typeRapport, -4) == "_all" && $admin) {
				$notaireNomArray = getNotaireNomArray();
				$factureSommaireForAllArray = getFactureSommaireForAllArray($dateDebut, $dateFin);

			} else {
				$Skin->assign('noRight', 1);
			}

			$Skin->assign('etudeKey', $etudeKey);
			$Skin->assign('etudeNom', $etudeNom);
			$Skin->assign('notaireNom', $notaireNom);
			$Skin->assign('dateDebut', $dateDebut);
			$Skin->assign('dateFin', $dateFin);
			$Skin->assign('typeRapport', $typeRapport);
			$Skin->assign('factureDetailsForOneArray', $factureDetailsForOneArray);
			$Skin->assign('factureSommaireForOneArray', $factureSommaireForOneArray);	
			$Skin->assign('factureSommaireForAllArray', $factureSommaireForAllArray);
			$Skin->assign('tableauSommaireBool', $tableauSommaireBool);
			$Skin->assign('tableauDetailsBool', $tableauDetailsBool);
			$Skin->assign('nbFactures', $nbFactures);
			$Skin->assign("CSS", array('reseauiq'));
			$Skin->display("reseauiq/rapport.tpl");
			break;

		default:
			header("Location: " . $BASEURL . "reseauiq/index.php");
			exit();
	}

	closeDbLinks();
	exit();



	// -------------
	// LES FONCTIONS
	// -------------

	function getValuesFromPost() {
		$values = array();

		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$values[$field] = $value;
				} else {
					$values[$field] = trim(stripslashes($value));
				}
			}
		}

		return $values;
	}

	function getValuesFromDb($DB, $factureKey = '') {
		$values = array();

		if (trim($factureKey) != '') {
			$query = "SELECT * FROM `reseauiq_factures` WHERE `key` = '" . mysql_real_escape_string($factureKey) . "';";

			$DB->query($query);
			$record = $DB->next_record();

			$values['factureKey'] = $record['key'];
			$values['etudeKey'] = $record['etudes_key'];
			$values['utilisateurKey'] = $record['utilisateurs_key'];
			$values['notaire_responsable'] = $record['notaire_responsable'];
			$values['date_facture'] = $record['date_facture'];
			$values['numero_client'] = $record['numero_client'];
			$values['montant_honoraires'] = $record['montant_honoraires'];
			$values['montant_debourses'] = $record['montant_debourses'];
			$values['montant_ristourne_eligible'] = $record['montant_ristourne_eligible'];
			$values['montant_ristourne'] = $record['montant_ristourne'];
		}

		return $values;
	}

	function setValuesIntoDb($DB, $values = array()) {
		$query = "";
		$factureKey = $values['factureKey'];
		$etudeKey = $values['etudeKey'];
		$utilisateurKey = $_SESSION['user_key'];

		if ($factureKey != '' && $etudeKey != '') {

			if (factureExists($DB, $factureKey) && $_SESSION['user_type'] == 1) {
				$query =
					"UPDATE `reseauiq_factures` SET ".
					"	`etudes_key` = '" . mysql_real_escape_string($etudeKey) . "', ".
					"	`utilisateurs_key` = '" . mysql_real_escape_string($utilisateurKey) . "', ".
					"	`notaire_responsable` = '" . mysql_real_escape_string($values['notaire_responsable']) . "', ".
					"	`date_facture` = '" . mysql_real_escape_string($values['date_facture']) . "', ".
					"	`numero_client` = '" . mysql_real_escape_string($values['numero_client']) . "', ".
					"	`montant_honoraires` = '" . mysql_real_escape_string($values['montant_honoraires']) . "', ".
					"	`montant_debourses` = '" . mysql_real_escape_string($values['montant_debourses']) . "', ".
					"	`montant_ristourne_eligible` = '" . mysql_real_escape_string($values['montant_ristourne_eligible']) . "', ".
					"	`montant_ristourne` = '" . mysql_real_escape_string($values['montant_ristourne']) . "' ".
					"WHERE `key` = '" . mysql_real_escape_string($factureKey) . "';";

			} else {
				$query =
					"INSERT INTO `reseauiq_factures` (".
					"	`key`, ".
					"	`etudes_key`, ".
					"	`utilisateurs_key`, ".
					"	`notaire_responsable`, ".
					"	`date_facture`, ".
					"	`numero_client`, ".
					"	`montant_honoraires`, ".
					"	`montant_debourses`, ".
					"	`montant_ristourne_eligible`, ".
					"	`montant_ristourne`".
					") VALUES (".
					"	'" . mysql_real_escape_string($factureKey) . "', ".
					"	'" . mysql_real_escape_string($etudeKey) . "', ".
					"	'" . mysql_real_escape_string($utilisateurKey) . "', ".
					"	'" . mysql_real_escape_string($values['notaire_responsable']) . "', ".
					"	'" . mysql_real_escape_string($values['date_facture']) . "', ".
					"	'" . mysql_real_escape_string($values['numero_client']) . "', ".
					"	'" . mysql_real_escape_string($values['montant_honoraires']) . "', ".
					"	'" . mysql_real_escape_string($values['montant_debourses']) . "', ".
					"	'" . mysql_real_escape_string($values['montant_ristourne_eligible']) . "', ".
					"	'" . mysql_real_escape_string($values['montant_ristourne']) . "'".
					");";
			}
			
			return $DB->query($query);
		}

		return false;
	}

	function getErrorArray($values = array()) {
		global $DB;

		$errorArray = array();

		if ($_SESSION['user_type'] != 1 && trim($values['factureKey']) != '' && factureExists($DB, $values['factureKey'])) {
			$errorArray[] = "Cette facture a d&eacute;j&agrave; &eacute;t&eacute; soumise.<br />Pour toute modification, veuillez communiquer avec un administrateur.";
		} else {
			if (
				trim($values['notaire_responsable']) == ''
				|| trim($values['date_facture']) == ''
				|| trim($values['numero_client']) == ''
				|| trim($values['factureKey']) == ''
				|| trim($values['montant_honoraires']) == ''
				|| trim($values['montant_debourses']) == ''
				|| trim($values['montant_ristourne_eligible']) == ''
				|| trim($values['montant_ristourne']) == ''
			) {
				$errorArray[] = "Tous les champs doivent &ecirc;tre remplis.";
			}

			if (!(toInt($values['montant_honoraires']) >= 0)) {
				$errorArray[] = "Le montant des honoraires doit &ecirc;tre positif ou &eacute;gale &agrave; z&eacute;ro.";
			}

			if (!(toInt($values['montant_debourses']) >= 0)) {
				$errorArray[] = "Le montant des d&eacute;bours&eacute;s doit &ecirc;tre positif ou &eacute;gale &agrave; z&eacute;ro.";
			}
			
			if (!(toInt($values['montant_ristourne_eligible']) >= 0)) {
				$errorArray[] = "Le montant &eacute;ligible &agrave; la ristourne doit &ecirc;tre positif ou &eacute;gale &agrave; z&eacute;ro.";
			}

			if (!isDateValid($values['date_facture'])) {
				$errorArray[] = "Le format de la date n'est pas valide.";
			}
		}

		return $errorArray;
	}

	function isDateValid($dateStr = '') {
			$dateRegex = '/^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/';
			if ($dateStr == '' || !preg_match($dateRegex, $dateStr, $matches)) {
				return false;
			}
			return true;
	}

	function getNotaireNom($etudeKey = '') {
		global $DB;
		
		$notaireNom = '';
		
		if (trim($etudeKey) != '') {
			$query =
				"SELECT `notaire_responsable`, `date_facture` ".
				"FROM `reseauiq_factures` ".
				"WHERE `etudes_key` = '" . mysql_real_escape_string($etudeKey) . "' ".
				"ORDER BY `date_facture` ASC ".
				"LIMIT 1;";

			$DB->query($query);

			while ($record = $DB->next_record()) {
				$notaireNom = $record['notaire_responsable'];
			}
		}
		
		return $notaireNom;
	}

	function getNotaireNomArray() {
		global $DB;
		
		$notaireNomArray = array();
		
		$query =
			"SELECT `etudes_key`, `notaire_responsable`, `date_facture` ".
			"FROM `reseauiq_factures` ".
			"WHERE `etudes_key` != '' ".
			"ORDER BY `date_facture` ASC;";

		$DB->query($query);

		while ($record = $DB->next_record()) {
			$notaireNomArray[$record['etudes_key']] = $record['notaire_responsable'];
		}

		return $notaireNomArray;
	}

	function getFactureDetailsForOneArray($etudeKey = '', $dateDebut = '1901-01-01', $dateFin = '1901-01-01') {
		global $DB;
		
		$facturesArray = array();
		
		if (trim($etudeKey) != '') {
			$query =
				"SELECT * ".
				"FROM `reseauiq_factures` ".
				"WHERE `etudes_key` = '" . mysql_real_escape_string($etudeKey) . "' ".
				"AND `date_facture` >= '" . mysql_real_escape_string($dateDebut) . "' ".
				"AND `date_facture` <= '" . mysql_real_escape_string($dateFin) . "' ".
				"ORDER BY `date_facture` DESC;";

			$DB->query($query);

			$n = 0;
			while ($record = $DB->next_record()) {
				$facturesArray[$n]['factureKey'] = $record['key'];
				$facturesArray[$n]['etudeKey'] = $record['etudes_key'];
				$facturesArray[$n]['utilisateurKey'] = $record['utilisateurs_key'];
				$facturesArray[$n]['notaire_responsable'] = $record['notaire_responsable'];
				$facturesArray[$n]['date_facture'] = $record['date_facture'];
				$facturesArray[$n]['numero_client'] = $record['numero_client'];
				$facturesArray[$n]['montant_honoraires'] = $record['montant_honoraires'];
				$facturesArray[$n]['montant_debourses'] = $record['montant_debourses'];
				$facturesArray[$n]['montant_ristourne_eligible'] = $record['montant_ristourne_eligible'];
				$facturesArray[$n]['montant_ristourne'] = $record['montant_ristourne'];
				$n++;
			}
		}
		
		return $facturesArray;
	}

	function getFactureSommaireForOneArray($etudeKey = '', $dateDebut = '1901-01-01', $dateFin = '1901-01-01') {
		global $DB;
		
		$sommaireArray = array();
		
		if (trim($etudeKey) != '') {
			$query =
				"SELECT * ".
				"FROM `reseauiq_factures` ".
				"WHERE `etudes_key` = '" . mysql_real_escape_string($etudeKey) . "' ".
				"AND `date_facture` >= '" . mysql_real_escape_string($dateDebut) . "' ".
				"AND `date_facture` <= '" . mysql_real_escape_string($dateFin) . "' ".
				"ORDER BY `date_facture` DESC;";

			$DB->query($query);

			while ($record = $DB->next_record()) {
				$sommaireArray['montant_honoraires_total'] += toInt($record['montant_honoraires']);
				$sommaireArray['montant_debourses_total'] += toInt($record['montant_debourses']);
				$sommaireArray['montant_ristourne_eligible_total'] += toInt($record['montant_ristourne_eligible']);
				$sommaireArray['montant_ristourne_total'] += toInt($record['montant_ristourne']);
			}
		}
		
		return $sommaireArray;
	}

	function getFactureSommaireForAllArray($dateDebut = '1901-01-01', $dateFin = '1901-01-01') {
		global $DB, $DB2;
		
		$sommaireArray = array();

		$queryA =
			"SELECT `key`, `nom` ".
			"FROM `etudes` ".
			"WHERE `actif` = 1 ".
			"ORDER BY `nom` ASC;";
			
		$DB->query($queryA);

		while ($recordA = $DB->next_record()) {
			$queryB =
				"SELECT ".
				"	`key`, ".
				"	`montant_ristourne_eligible`, ".			
				"	`montant_honoraires`, ".
				"	`montant_debourses`, ".
				"	`montant_ristourne` ".
				"FROM ".
				"	`reseauiq_factures` ".
				"WHERE ".
				"	`etudes_key` = '" . $recordA['key'] . "' ".
				"	AND `date_facture` >= '" . mysql_real_escape_string($dateDebut) . "' ".
				"	AND `date_facture` <= '" . mysql_real_escape_string($dateFin) . "';";

			$DB2->query($queryB);

			$sommaireArray['data'][$recordA['key']]['nom'] = $recordA['nom'];
			$sommaireArray['data'][$recordA['key']]['montant_ristourne_eligible'] = '0';
			$sommaireArray['data'][$recordA['key']]['montant_honoraires'] = '0';
			$sommaireArray['data'][$recordA['key']]['montant_debourses'] = '0';
			$sommaireArray['data'][$recordA['key']]['montant_ristourne'] = '0';

			while ($recordB = $DB2->next_record()) {
				$sommaireArray['data'][$recordA['key']]['montant_ristourne_eligible'] += toInt($recordB['montant_ristourne_eligible']);
				$sommaireArray['data'][$recordA['key']]['montant_honoraires'] += toInt($recordB['montant_honoraires']);
				$sommaireArray['data'][$recordA['key']]['montant_debourses'] += toInt($recordB['montant_debourses']);
				$sommaireArray['data'][$recordA['key']]['montant_ristourne'] += toInt($recordB['montant_ristourne']);
				$sommaireArray['total']['montant_ristourne_eligible'] += toInt($recordB['montant_ristourne_eligible']);
				$sommaireArray['total']['montant_honoraires'] += toInt($recordB['montant_honoraires']);
				$sommaireArray['total']['montant_debourses'] += toInt($recordB['montant_debourses']);
				$sommaireArray['total']['montant_ristourne'] += toInt($recordB['montant_ristourne']);
			}
		}

		return $sommaireArray;
	}

	function factureExists($DB, $factureKey = '') {
		if (trim($factureKey) != '') {
			$query = "SELECT `key` FROM `reseauiq_factures` WHERE `key` = '" . mysql_real_escape_string($factureKey) . "';";
			$DB->query($query);
			return($DB->getNumRows());
		}
		return 0;
	}

	function deleteFactureFromDb($factureKey = '') {
		global $DB;

		if (trim($factureKey) != '') {
			$query = "DELETE FROM `reseauiq_factures` WHERE `key` = '" . mysql_real_escape_string($factureKey) . "';";
			$DB->query($query);
			return true;
		}
		return false;
	}

	function toInt($str) {
		$int = (float) str_replace(" ", "", str_replace(",", ".", str_replace("&nbsp", "", $str)));
		return($int);
	}

	function assignValuesToSkin($Skin, $values = array()) {
		if (is_array($values) && count($values) > 0) {
			foreach ($values as $key => $value) {
				$Skin->assign($key, $value);
			}
		}
	}

	function getEtudeNom($etudeKey = '') {
		global $DB;
		
		$etudeNom = '';
		
		if (trim($etudeKey) != '') {
			$query = "SELECT `nom` FROM `etudes` WHERE `key` = '" . mysql_real_escape_string($etudeKey) . "';";
			$DB->query($query);
			if ($DB->getNumRows() > 0) {
				$record = $DB->next_record();
				$etudeNom = $record['nom'];
			}
			$DB->reset();
		}
		
		return($etudeNom);
	}
	
	function toMoneyFormat($str = '') {
		if (trim($str) == '') {
			return('0,00&nbsp;$');
		}

		$str = str_replace(' ', '', $str);
		$str = str_replace('&nbsp;', '', $str);
		$str = str_replace(',', '.', $str);
				
		$str = number_format($str, 2, ',', ' ');
		if (strpos($str, ',') === false) {
			$str = $str . ',';
		}

		$str = $str . str_repeat('0', 2 - strpos(strrev($str), ','));
		$str = str_replace(' ', '&nbsp;', $str);
		$str = $str . '&nbsp;$';

		return $str;
	}

	function closeDbLinks() {
		global $DB, $DB2, $DB3;
		if ($DB != "") {
			$DB->close();
		}
		if ($DB2 != "") {
			$DB2->close();
		}
		if ($DB3 != "") {
			$DB3->close();
		}
	}

?>
