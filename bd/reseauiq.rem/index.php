<?PHP
	require "../conf/conf.inc.php";

	session_start();
	
	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;	
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	if ($_SERVER['REMOTE_ADDR'] == '69.70.81.34' || $_SERVER['REMOTE_ADDR'] == '24.201.156.205') {
		$Skin->assign('quibo', 1);
	}

	if ($_SESSION['user_type'] == 1) {
		$admin = 1;
		$Skin->assign('admin', 1);
		$Skin->assign('facturesArray', getFacturesArray());
	} else {
		$admin = 0;
		$Skin->assign('admin', 0);
	}

	$etudeKey = getorpost('etudeKey');
	$factureKey = getorpost('factureKey');
	$action = getorpost('action');

	$Skin->assign('etudesArray', getEtudesArray());
	$Skin->assign("title", "R&eacute;seau IQ");
	$Skin->assign('BASEURL', $BASEURL);
	$Skin->display("reseauiq/index.tpl");

	closeDbLinks();
	exit();


	// -------------
	// LES FONCTIONS
	// -------------

	function getEtudesArray() {
		global $DB;
		
		switch ($_SESSION['user_type']) {
			case 1:
				$query = "SELECT `key`, `nom` FROM `etudes` WHERE `actif` = '1';";
				break;
			case 2:
				$query = "SELECT `key`, `nom` FROM `etudes` WHERE `key` = '" . mysql_real_escape_string($_SESSION['user_etudes_key']) . "' AND `actif` = '1';";
				break;
		}
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$etudesArray[$record['key']] = $record['nom'];
		}

		return $etudesArray;
	}

	function getFacturesArray() {
		global $DB;
		
		$facturesArray = array();
		if ($_SESSION['user_type'] == 1) {
			$query = "SELECT `key`, `etudes_key`, `date_facture` FROM `reseauiq_factures` ORDER BY `date_facture` DESC;";
		} else {
			$query = "SELECT `key`, `etudes_key`, `date_facture` FROM `reseauiq_factures` WHERE `etudes_key` = '" . mysql_real_escape_string($_SESSION['user_etudes_key']) . "' ORDER BY `date_facture` DESC;";
		}
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$facturesArray[$record['etudes_key']][$record['key']] = $record['date_facture'];
		}
		return $facturesArray;
	}

	function closeDbLinks() {
		global $DB, $DB2, $DB3;
		if ($DB != "") {
			$DB->close();
		}
		if ($DB2 != "") {
			$DB2->close();
		}
		if ($DB3 != "") {
			$DB3->close();
		}
	}

?>
