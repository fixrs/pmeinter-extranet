<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evaluations");
		$Skin->assign("errors", $html);
		$Skin->display("evaluations.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evaluations");
		$Skin->assign("errors", $html);
		$Skin->display("evaluations.tpl");
		exit;	
	}	

	import("com.quiboweb.form.Form");
	import("com.pmeinter.Evaluation");

	include_once("load.inc.php");
	include_once("save.inc.php");
	include_once("validation.inc.php");	
	
	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	
	
	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$formString = "";
	$successString = "";
	$errorString = "";

	$sid = getorpost('sid');
	$key1 = getorpost('key1');
	$key2 = getorpost('key2');
	$key3 = getorpost('key3');
	$key4 = getorpost('key4');
	$requestSID = getorpost('requestSID');
	$begin = getorpost('begin');
	$close = getorpost('close');
	$save = getorpost('save');
	$saveAll = getorpost('saveAll');
	$reload = getorpost('reload');
	$reloadAll = getorpost('reloadAll');
	$currentForm = getorpost('currentForm');
	$requestForm = getorpost('requestForm');
	$requestURL = getorpost('requestURL');

	if ($sid == "compare") {
		$action = "compare";
	} else {
		$action = getorpost('action');
		if ($action == "") {
			$action = $_SESSION['evaluations'][$sid]['action'];
		}
	}

	fixEmptySID();

	if ($sid == "" && $requestSID == "" && $requestURL == "") {
		header("Location: index.php");
		exit;
	}

	if ($sid == "" && $requestURL != "") {
		header("Location: " . $requestURL);
		exit;
	}

	if ($sid == "" && $requestSID != "" && !$close) {
		header("Location: evaluations.php?sid=" . $requestSID . "&requestForm=1");
		exit;
	}

	if ($close) {
		unset($_SESSION['evaluations'][$requestSID]);
		$backurl = $_SERVER['HTTP_REFERER'];
		if (stristr($backurl, "evaluations.php")) {
			$start_pos = strpos($backurl, "?");
			$end_pos = strlen($backurl);
			$backurl = substr($backurl, $start_pos, $end_pos);
			$backurl = str_replace($backurl, "evaluation.php", "index.php");
		}
		header("Location: " . $backurl);
		exit;
	}

	switch ($action) {
		case "modify":

			if ($reload) {
				clearSessionValues($sid, $currentForm);
				updateSessionValues(getParametersFromDB($sid, $currentForm), $sid, "modify");
				header("Location: evaluations.php?sid=" . $sid . "&action=modify&requestForm=" . $currentForm);
				exit;
			}

			if ($reloadAll) {
				clearSessionValues($sid, "all");
				header("Location: evaluations.php?sid=" . $sid . "&action=modify&begin=1&requestForm=" . $currentForm);
				exit;
			}

			if ($begin) {
				clearSessionValues($sid, "all");
				updateSessionValues(getParametersFromDB($sid, "all"), $sid, "modify");

				if ($requestForm) {
					$form = $requestForm;
					$section = $requestForm;
					importForm("eval_" . $requestForm);
					$FormObject = new Form("", getParametersFromSession($sid), getForm($action, $requestForm));
					$FormObject->setValue("currentForm", $requestForm);
					$formString = $FormObject->returnFilledForm();

				} else {
					$form = "1";
					$section = "1";
					importForm("eval_1");
					$FormObject = new Form("", getParametersFromSession($sid), getForm($action, "1"));
					$FormObject->setValue("currentForm", "1");
					$formString = $FormObject->returnFilledForm();
				}

			} else {

				if ($currentForm) {

					if (!$reloadAll && !$reload) {
						$errorString .= validateFormValues(getParametersFromPost(), $currentForm);
					}

					if ($errorString == "") {

						if (!$reloadAll && !$reload) {
							clearSessionValues($sid, $currentForm);
							updateSessionValues(getParametersFromPost(), $sid, "modify");
						}

						if ($save) {
							$errorString .= updateDBValues(getParametersFromSession($sid), $currentForm);

							if ($errorString == "") {
								$successString = _success("Modifications enregistr&eacute;es avec succ&egrave;s.");
								clearSessionValues($sid, $currentForm);
								updateSessionValues(getParametersFromDB($sid, $currentForm), $sid, "modify");
							}
						}

						if ($saveAll) {
							$errorString .= updateDBValues(getParametersFromSession($sid), "all");

							if ($errorString == "") {
								$successString = _success("Modifications enregistr&eacute;es avec succ&egrave;s.");
								clearSessionValues($sid, "all");
								updateSessionValues(getParametersFromDB($sid, "all"), $sid, "modify");
							}
						}

						if ($requestURL) {
							header("Location: " . $requestURL);
							exit;
						}

						if ($requestForm) {

							if ($requestSID) {
								header("Location: evaluations.php?sid=" . $requestSID . "&action=modify&requestForm=" . $requestForm);
								exit;
							}

							$form = $requestForm;
							$section = $requestForm;
							importForm("eval_" . $requestForm);
							$theForm = getForm("modify", $requestForm);
							$FormObject = new Form("", getParametersFromSession($sid), $theForm);
							$formString = $FormObject->returnFilledForm();

						} else {

							if ($requestSID) {
								header("Location: evaluations.php?sid=" . $requestSID . "&requestForm=" . $currentForm);
								exit;
							}

							$form = $currentForm;
							$section = $currentForm; 
							importForm("eval_" . $currentForm);
							$theForm = getForm("modify", $currentForm);
							$FormObject = new Form("", getParametersFromSession($sid), $theForm);
							$formString = $FormObject->returnFilledForm();
						}

					} else {
						importForm("eval_" . $currentForm);
						$theForm = getForm("modify", $currentForm);
						$FormObject = new Form("", getParametersFromPost(), $theForm);
						$formString = $FormObject->returnFilledForm();
						$form = $currentForm;
					}

				} else {

					if ($saveAll) {
						$errorString .= updateDBValues(getParametersFromSession($sid), "all");
			
						if ($errorString == "") {
							$successString = _success("Modifications enregistr&eacute;es avec succ&egrave;s.");
							clearSessionValues($sid, "all");
							updateSessionValues(getParametersFromDB($sid, "all"), $sid, "modify");
						}
					}

					if ($requestForm) {

						if ($requestURL) {
							header("Location: " . $requestURL);
							exit;
						}

						if ($requestSID) {
							header("Location: evaluations.php?sid=" . $requestSID . "&requestForm=" . $requestForm);
							exit;
						}

						$form = $requestForm;
						$section = $requestForm;
						importForm("eval_" . $requestForm);
						$theForm = getForm("modify", $requestForm);
						$FormObject = new Form("", getParametersFromSession($sid), $theForm);
						$formString = $FormObject->returnFilledForm();
					}
				}
			}
			break;

		case "read":
			
			if ($reload) {
				clearSessionValues($sid, $currentForm);
				updateSessionValues(getParametersFromDB($sid, $currentForm), $sid, "read");
				header("Location: evaluations.php?sid=" . $sid . "&action=read&requestForm=" . $currentForm);
				exit;
			}

			if ($reloadAll) {
				clearSessionValues($sid, "all");
				header("Location: evaluations.php?sid=" . $sid . "&action=read&begin=1&requestForm=" . $currentForm);
				exit;
			}

			if ($begin) {
				clearSessionValues($sid, "all");
				updateSessionValues(getParametersFromDB($sid, "all"), $sid, "read");

				if ($requestForm) {
					$form = $requestForm;
					$section = $requestForm;
					importForm("eval_" . $requestForm);
					$FormObject = new Form("", getParametersFromSession($sid), getForm($action, $requestForm));
					$FormObject->setValue("currentForm", $requestForm);
					$formString = $FormObject->returnFilledForm();

				} else {
					$form = "1";
					$section = "1";
					importForm("eval_1");
					$FormObject = new Form("", getParametersFromSession($sid), getForm($action, "1"));
					$FormObject->setValue("currentForm", "1");
					$formString = $FormObject->returnFilledForm();
				}

			} else {

				if ($requestURL) {
					header("Location: " . $requestURL);
					exit;
				}

				if ($requestSID) {
					header("Location: evaluations.php?sid=" . $requestSID . "&requestForm=" . $requestForm);
					exit;
				}

				if ($requestForm) {
					$form = $requestForm;
					$section = $requestForm;
					importForm("eval_" . $requestForm);
					$FormObject = new Form("", getParametersFromSession($sid), getForm($action, $requestForm));
					$FormObject->setValue("currentForm", $requestForm);
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "compare":

			if ($reload) {
				clearSessionValues("compare", $currentForm);
				updateSessionValues(getParametersFromDB("compare", $currentForm), "compare");
				header("Location: evaluations.php?sid=compare&action=compare&requestForm=" . $currentForm);
				exit;
			}

			if ($reloadAll) {
				if (is_array($_SESSION['evaluations']['compare']) && count($_SESSION['evaluations']['compare']) > 0) {
					$nb_keys = 0;
					foreach ($_SESSION['evaluations']['compare'] as $compare_sid => $useless) {
						$nb_keys++;
						$keys_url_params .= "&key" . $nb_keys . "=" . $compare_sid;
					}
				}
				clearSessionValues("compare", "all");
				header("Location: evaluations.php?&sid=compare&action=compare" . $keys_url_params . "&begin=1&requestForm=" . $currentForm);
				exit;
			}

			if ($begin) {
				clearSessionValues("compare", "all");
				if ($key1 != "") {
					$_SESSION['evaluations']['compare'][$key1] = array();
				}
				if ($key2 != "") {
					$_SESSION['evaluations']['compare'][$key2] = array();
				}
				if ($key3 != "") {
					$_SESSION['evaluations']['compare'][$key3] = array();
				}
				if ($key4 != "") {
					$_SESSION['evaluations']['compare'][$key4] = array();
				}
				updateSessionValues(getParametersFromDB("compare", "all"), "compare");

				if ($requestForm) {
					$form = $requestForm;
					$section = $requestForm;
					importForm("eval_" . $requestForm);
					$FormObject = new Form("", formatFormCompareValues(getParametersFromSession("compare")), getForm($action, $requestForm));
					$FormObject->setValue("currentForm", $requestForm);
					$formString = $FormObject->returnFilledForm();

				} else {
					$form = "1";
					$section = "1";
					importForm("eval_1");
					$FormObject = new Form("", formatFormCompareValues(getParametersFromSession("compare")), getForm($action, "1"));
					$FormObject->setValue("currentForm", "1");
					$formString = $FormObject->returnFilledForm();
				}

			} else {

				if ($requestURL) {
					header("Location: " . $requestURL);
					exit;
				}

				if ($requestSID) {
					header("Location: evaluations.php?sid=" . $requestSID . "&requestForm=" . $requestForm);
					exit;
				}

				if ($requestForm) {
					$form = $requestForm;
					$section = $requestForm;
					importForm("eval_" . $requestForm);
					$FormObject = new Form("", formatFormCompareValues(getParametersFromSession("compare")), getForm($action, $requestForm));
					$FormObject->setValue("currentForm", $requestForm);
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;
	}

	removeOldFeedbackFromSession();

	if (!$close && $sid != "compare") {
		$_SESSION['evaluations'][$sid]['feedback'] = $successString . $errorString;
	}

	fixEmptySID();

	debug(0);

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evaluations");
	$Skin->assign("page", "evaluations");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("headcontent", headString($action, $form));
	$Skin->assign("bodyparams", bodyString($action, $form));
	$Skin->assign("form", $formString);
	$Skin->assign("INDEXTAB_A", topIndexString($sid, $form));
	$Skin->assign("INDEXTAB_B", bottomIndexString($sid, $form));
	$Skin->assign("hdp", hdpString());
	$Skin->assign("bdp", bdpString());
	$Skin->display("evaluations.tpl");

	closeDBLinks();


	// --------------------------
	// LES FONCTIONS DE TAITEMENT
	// --------------------------

	function getParametersFromPost() {
		$parameters = array();
		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$parameters[$field] = $value;
				} else {
					$parameters[$field] = trim($value);
				}
			}
		}
		return $parameters;
	}

	function getParametersFromSession($sid) {
		$parameters = array();
		if ($sid == "compare") {
			if (is_array($_SESSION['evaluations']['compare']) && count($_SESSION['evaluations']['compare']) > 0) {
				foreach ($_SESSION['evaluations']['compare'] as $compare_sid => $useless) {
					if (is_array($_SESSION['evaluations']['compare'][$compare_sid]) && count($_SESSION['evaluations']['compare'][$compare_sid]) > 0) {
						foreach ($_SESSION['evaluations']['compare'][$compare_sid] as $field => $value) {
							if (is_array($value)) {
								$parameters[$compare_sid][$field] = $value;
							} else {
								$parameters[$compare_sid][$field] = trim($value);
							}
						}
					}
				}
			}			
		} else {
			if (is_array($_SESSION['evaluations'][$sid]) && count($_SESSION['evaluations'][$sid]) > 0) {
				foreach ($_SESSION['evaluations'][$sid] as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
			}
		}
		return $parameters;
	}

	function formatFormCompareValues($parameters) {
		if (is_array($parameters) && count($parameters) > 0) {
			foreach ($parameters as $compare_sid => $useless) {
				if (is_array($parameters[$compare_sid]) && count($parameters[$compare_sid]) > 0) {
					foreach ($parameters[$compare_sid] as $field => $value) {
						$new_field_name = $compare_sid . "_" . $field;
						$compare_parameters[$new_field_name] = $value;
					}
				}
			}
		}
		return $compare_parameters;
	}

	function updateSessionValues($parameters, $sid, $action = "") {
		if ($sid == "compare") {
			if (is_array($parameters) && count($parameters) > 0) {
				foreach ($parameters as $sid => $sid_parameters) {
					if (is_array($sid_parameters) && count($sid_parameters) > 0) {
						foreach ($sid_parameters as $field => $value) {
							$_SESSION['evaluations']['compare'][$sid][$field] = $value;
						}
					}
				}
			}
		} else {
			if (is_array($parameters) && count($parameters) > 0) {
				foreach ($parameters as $field => $value) {
					$_SESSION['evaluations'][$sid][$field] = $value;
				}
			}
			if ($action != "") {
				$_SESSION['evaluations'][$sid]['action'] = $action;
			}
		}
	}

	function clearSessionValues($sid, $section) {
		global $EVALUATIONS_SECTIONS_PATTERNS;
		$pregmatch = $EVALUATIONS_SECTIONS_PATTERNS[$section];
		if ($sid == "compare") {
			if (is_array($_SESSION['evaluations']['compare']) && count($_SESSION['evaluations']['compare']) > 0) {
				foreach ($_SESSION['evaluations']['compare'] as $compare_sid => $useless) {
					if (is_array($_SESSION['evaluations']['compare'][$compare_sid]) && count($_SESSION['evaluations']['compare'][$compare_sid]) > 0) {
						foreach ($_SESSION['evaluations']['compare'][$compare_sid] as $compare_sid_parameters => $compare_sid_field) {
							if (is_array($compare_sid_field)) {
								$compare_sid_field_keys = array_keys($compare_sid_field);
								$compare_sid_field = $compare_sid_field_keys[0];
							}
							if (preg_match($pregmatch, $compare_sid_field)) {
								unset($_SESSION['evaluations']['compare'][$compare_sid][$compare_sid_field]);
							}
						}
					}
				}
			}
		} else {
			if (is_array($_SESSION['evaluations'][$sid]) && count($_SESSION['evaluations'][$sid]) > 0) {
				foreach ($_SESSION['evaluations'][$sid] as $field => $value) {
					if (!preg_match("/_tmp/", $field) && preg_match($pregmatch, $field)) {
						unset($_SESSION['evaluations'][$sid][$field]);
					}
				}
			}
			if (is_array($_FILES) && count($_FILES) > 0) {
				if (is_array($_FILES) && count($_FILES) > 0) {
					foreach ($_FILES as $file => $file_array) {
						if (preg_match($pregmatch, $file)) {
							unset($_FILES[$file]);
						}
					}
				}
			}
		}
	}

	function removeOldFeedbackFromSession() {
		if (is_array($_SESSION['evaluations']) && count($_SESSION['evaluations']) > 0) {
			foreach ($_SESSION['evaluations'] as $sid => $useless) {
				if ($sid != "compare") {
					$_SESSION['evaluations'][$sid]['feedback'] = "";
				}
			}
		}
	}

	function closeDBLinks() {
		global $DB, $DB2, $DB3;
		if ($DB != "") {
			$DB->close();
		}
		if ($DB2 != "") {
			$DB2->close();
		}
		if ($DB3 != "") {
			$DB3->close();
		}
	}

	function fixEmptySID() {
		if (is_array($_SESSION['evaluations'][NULL])) {
			unset($_SESSION['evaluations'][NULL]);
		}
		if (count($_SESSION['evaluations']) == 0) {
			unset($_SESSION['evaluations']);
		}
	}

	function debug($bool) {
		if ($bool) {
			echo "<div style=\"display: none;\">";
			print_r($_SESSION);
			echo "</div>";
		}
	}


	// -------------------------
	// LES FONCTIONS D'AFFICHAGE
	// -------------------------

	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "Ajout d'une &eacute;valuation";
				break;
			case "modify":
				$title = "Modification d'une &eacute;valuation";
				break;
			case "read":
				$title = "Consultation d'une &eacute;valuation";
				break;
			case "compare":
				$title = "Comparaison d'&eacute;valuations";
				break;
			default:
				$title = "Gestion des d'une &eacute;valuations";
		}
		return $title;
	}

	function getSectionTitle($section = "") {
		global $EVALUATIONS_SECTIONS;
		$html = "";
		if ($section != "") {
			$html = $EVALUATIONS_SECTIONS[$section];
		}
		return $html;
	}

	function headString($action, $section = "") {
		global $JS_URL, $SKIN_URL;
		$html = "";
		if ($section != "") {
			switch ($action) {
				case "modify":
					$html .=
						"<link rel=\"stylesheet\" href=\"" . $SKIN_URL . "/css/evals.css\" type=\"text/css\" />\n".
						"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
					if ($section == "2") {
						$html .=
							"<script language=\"JavaScript\" src=\"" . $JS_URL . "/eval_02.js\" type=\"text/javascript\"></script>";
					}
					if ($section == "3") {
						$html .=
							"<script language=\"JavaScript\" src=\"" . $JS_URL . "/eval_03.js\" type=\"text/javascript\"></script>";
					}
					break;

				case "read":
					$html .=
						"<link rel=\"stylesheet\" href=\"" . $SKIN_URL . "/css/evals.css\" type=\"text/css\" />\n";
					break;

				case "compare":
					$html .=
						"<link rel=\"stylesheet\" href=\"" . $SKIN_URL . "/css/evals.css\" type=\"text/css\" />\n";
					break;
			}
		}
		return $html;
	}

	function bodyString($action, $section = "") {
		$html = "";
		if ($section != "") {
			switch ($action) {
				case "modify":
					if ($section == "2") {
						$html .= "onload=\"javascript: notaires_reset_addForm()\"";
					}
					if ($section == "3") {
						$html .= "onload=\"javascript: employes_reset_addForm()\"";
					}
					break;
			}
		}
		return $html;
	}

	function topIndexString($sid, $currentForm) {
		global $BASEURL, $SKIN_URL, $EVALUATIONS_SECTIONS;

		$prevform = $currentForm - 1;
		$nextform = $currentForm + 1;

		$html .=
			"<div id=\"indextab_a\">\n".
			"	<ul>\n";
		if ($currentForm != 1) {
			if ($currentForm == 14) {
				$html .=
					"<li class=\"prev\"><a href=\"javascript: submitEvalForm('" . $sid . "', '', '12');\">&lt;</a></li>\n";
			} else {
				$html .=
					"<li class=\"prev\"><a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $prevform . "');\">&lt;</a></li>\n";
			}
		} else {
			$html .=
				"<li class=\"prev\">&lt;</li>\n";
		}
		for ($i = 1; $i <= count($EVALUATIONS_SECTIONS); $i++) { 
			if ($i == $currentForm) {
				$html .=
					"<li class=\"current\">" . $i . "</li>\n";
			} else {
				if ($i == 13) {
					$html .=
						"<li>" . $i . "</li>\n";
				} else {
					$html .=
						"<li><a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $i . "');\">" . $i . "</a></li>\n";
				}
			}
		}
		if ($currentForm != count($EVALUATIONS_SECTIONS)) {
			if ($currentForm == 12) {
				$html .=
					"<li class=\"next\"><a href=\"javascript: submitEvalForm('" . $sid . "', '', '14');\">&gt;</a></li>\n";
			} else {
				$html .=
					"<li class=\"next\"><a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $nextform . "');\">&gt;</a></li>\n";
			}				
		} else {
			$html .=
				"<li class=\"next\">&gt;</li>\n";
		}
		$html .=
			"	</ul>\n".
						"<div class=\"bdp_ie\"><a href=\"#bdp\"><img src=\"" . $SKIN_URL . "/img/bdp.png\" width=\"23\" height=\"23\" alt=\"Bas de page\" title=\"Bas de page\" /></a></div>\n".
			"</div>\n";

		return $html;
	}

	function bottomIndexString($sid, $currentForm) {
		global $BASEURL, $SKIN_URL, $EVALUATIONS_SECTIONS;

		$prevform = $currentForm - 1;
		$nextform = $currentForm + 1;

		$html .=
			"<div id=\"indextab_b\">\n".
			"	<ul>\n";
		if ($currentForm != 1) {
			if ($currentForm == 14) {
				$html .=
					"<li class=\"prev\"><a href=\"javascript: submitEvalForm('" . $sid . "', '', '12');\">&lt;</a></li>\n";
			} else {
				$html .=
					"<li class=\"prev\"><a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $prevform . "');\">&lt;</a></li>\n";
			}
		} else {
			$html .=
				"<li class=\"prev\">&lt;</li>\n";
		}
		for ($i = 1; $i <= count($EVALUATIONS_SECTIONS); $i++) { 
			if ($i == $currentForm) {
				$html .=
					"<li class=\"current\">" . $i . "</li>\n";
			} else {
				if ($i == 13) {
					$html .=
						"<li>" . $i . "</li>\n";
				} else {
					$html .=
						"<li><a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $i . "');\">" . $i . "</a></li>\n";
				}
			}
		}
		if ($currentForm != count($EVALUATIONS_SECTIONS)) {
			if ($currentForm == 12) {
				$html .=
					"<li class=\"next\"><a href=\"javascript: submitEvalForm('" . $sid . "', '', '14');\">&gt;</a></li>\n";
			} else {
				$html .=
					"<li class=\"next\"><a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $nextform . "');\">&gt;</a></li>\n";
			}				
		} else {
			$html .=
				"<li class=\"next\">&gt;</li>\n";
		}
		$html .=
			"	</ul>\n".
			"	<div class=\"hdp_ie\"><a href=\"#\"><img src=\"" . $SKIN_URL . "/img/hdp.png\" width=\"23\" height=\"23\" alt=\"Haut de page\" title=\"Haut de page\" /></a></div>\n".
			"</div>\n";

		return $html;
	}

	function hdpString() {
		global $SKIN_URL;

 		$html =
			"<div class=\"hdp\"><a href=\"#\"><img src=\"" . $SKIN_URL . "/img/hdp.png\" width=\"23\" height=\"23\" alt=\"Haut de page\" title=\"Haut de page\" /></a></div>\n";
 		
 		return $html;
	}

	function bdpString() {
		global $SKIN_URL;

 		$html =
			"<div class=\"bdp\"><a href=\"#bdp\"><img src=\"" . $SKIN_URL . "/img/bdp.png\" width=\"23\" height=\"23\" alt=\"Bas de page\" title=\"Bas de page\" /></a></div>\n";
 		
 		return $html;
	}


	// ----------------------------------
	// LES FONCTIONS POUR LES FORMULAIRES
	// ----------------------------------

	function form_header($action, $section = "") {
		global $sid;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=	
					"<form name=\"evaluation\" action=\"evaluations.php\" method=\"post\" enctype=\"multipart/form-data\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_sid\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_requestSID\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_currentForm\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_requestForm\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_save\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_saveAll\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_reload\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_reloadAll\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_close\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_requestURL\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"sid\" id=\"_eval_sid\" value=\"" . $sid . "\">\n".
					"	<input type=\"hidden\" name=\"requestSID\" id=\"_eval_requestSID\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"currentForm\" id=\"_eval_currentForm\" value=\"" . $section . "\">\n".
					"	<input type=\"hidden\" name=\"requestForm\" id=\"_eval_requestForm\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"saveAll\" id=\"_eval_saveAll\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"save\" id=\"_eval_save\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"reload\" id=\"_eval_reload\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"reloadAll\" id=\"_eval_reloadAll\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"close\" id=\"_eval_close\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"requestURL\" id=\"_eval_requestURL\" value=\"\">\n".
					"	<div class=\"evalform\">\n".
					"		<h1>" . getSectionTitle($section) . "</h1>\n";
				break;

			case "read":
				$html .=
					"<form name=\"evaluation\" action=\"evaluations.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_sid\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_requestSID\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_currentForm\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_requestForm\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_save\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_saveAll\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_reload\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_reloadAll\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_close\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_requestURL\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"sid\" id=\"_eval_sid\" value=\"" . $sid . "\">\n".
					"	<input type=\"hidden\" name=\"requestSID\" id=\"_eval_requestSID\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"currentForm\" id=\"_eval_currentForm\" value=\"" . $section . "\">\n".
					"	<input type=\"hidden\" name=\"requestForm\" id=\"_eval_requestForm\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"saveAll\" id=\"_eval_saveAll\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"save\" id=\"_eval_save\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"reload\" id=\"_eval_reload\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"reloadAll\" id=\"_eval_reloadAll\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"close\" id=\"_eval_close\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"requestURL\" id=\"_eval_requestURL\" value=\"\">\n".
					"	<div class=\"evalform\">\n".
					"		<h1>" . getSectionTitle($section) . "</h1>\n";
				break;

			case "compare":
				$html .=
					"<form name=\"evaluation\" action=\"evaluations.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_sid\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_requestSID\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_currentForm\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_requestForm\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_save\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_saveAll\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_reload\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_reloadAll\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_close\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"_IGNORE_requestURL\" value=\"1\">\n".
					"	<input type=\"hidden\" name=\"sid\" id=\"_eval_sid\" value=\"" . $sid . "\">\n".
					"	<input type=\"hidden\" name=\"requestSID\" id=\"_eval_requestSID\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"currentForm\" id=\"_eval_currentForm\" value=\"" . $section . "\">\n".
					"	<input type=\"hidden\" name=\"requestForm\" id=\"_eval_requestForm\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"saveAll\" id=\"_eval_saveAll\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"save\" id=\"_eval_save\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"reload\" id=\"_eval_reload\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"reloadAll\" id=\"_eval_reloadAll\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"close\" id=\"_eval_close\" value=\"\">\n".
					"	<input type=\"hidden\" name=\"requestURL\" id=\"_eval_requestURL\" value=\"\">\n".
					"	<div class=\"evalform\">\n".
					"		<h1>" . getSectionTitle($section) . "</h1>\n";
				break;
		}
		return $html;
	}

	function form_footer($action, $section = "") {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"	</div>\n".
					"</form>\n".
					"<span id=\"bdp\"></span>\n";
				break;

			case "read":
				$html .=
					"	</div>\n".
					"</form>\n".
					"<span id=\"bdp\"></span>\n";
				break;

			case "compare":
				$html .=
					"	</div>\n".
					"</form>\n".
					"<span id=\"bdp\"></span>\n";
				break;
		}
		return $html;
	}

	function annee($diff, $sid = "", $compare_sid = "", $text = "") {
		global $DB;

		if ($sid == "") {
			global $sid;
		}
		if ($text = "") {
			$text = 0;
		}

		if ($text == 1 && $sid == "compare") {
			if ($diff == 0) {
				$year = "(ann&eacute; de l'&eacute;valuation)";
			}
			if ($diff > 0) {
				$year = "(" . abs($diff) . " ans apr&egrave;s l'ann&eacute; de l'&eacute;valuation)";
			}
			if ($diff < 0) {
				$year = "(" . abs($diff) . " ans avant l'ann&eacute; de l'&eacute;valuation)";
			}

		} else {
			if ($sid == "compare") {
				$eval_year = substr($_SESSION['evaluations']['compare'][$compare_sid]["s0_date_visite"], 0, 4);
				$year = $eval_year + $diff;
			} else {
				$eval_year = substr($_SESSION['evaluations'][$sid]["s0_date_visite"], 0, 4);
				$year = $eval_year + $diff;
			}
		}

		return $year;
	}

?>
