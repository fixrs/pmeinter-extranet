<?PHP

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evaluations");
		$Skin->assign("errors", $html);
		$Skin->display("evaluations.tpl");
		exit;	
	}

/*	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL);
		$Skin->assign("errors", $html);
		$Skin->display("evaluations.tpl");
		exit;	
	}*/


	// -------------
	// LES FONCTIONS
	// -------------

	function validateFormValues($parameters, $section) {
		global $sid, $DB, $BASEURL, $BASEPATH;
		global $PHONE_FORMAT_PATTERN, $POSTAL_CODE_FORMAT_PATTERN, $HTTP_FORMAT_PATTERN, $EMAIL_FORMAT_PATTERN, $PHONE_FORMAT, $TOLL_FREE_FORMAT, $POSTAL_CODE_FORMAT;
		global $EVALUATIONS_DOC_MAX_SIZE;

		$errorString = "";
		
		if ($_SESSION['user_type'] == 2) {
			$DB->query(
				"SELECT `etudes_key` ".
				"FROM `evaluations` ".
				"WHERE `key` = '" . $sid . "';"
			);
			while ($DB->next_record()) {
				$etudes_key = $DB->getField("etudes_key");
			}
		
			if ($_SESSION['user_etudes_key'] != $etudes_key) {
				$errorString .= _error("Vous ne disposez pas des droits requis pour effectuer cette op&eacute;ration.");
				return $errorString;
			}	
		}
		
		switch ($section) {

			// SECTION #1 //
			case "1":
				break;
			

			// SECTION #2 //
			case "2":
				break;


			// SECTION #3 //
			case "3":
				break;


			// SECTION #4 //
			case "4":
				if ($parameters['s4_entente_signee_doc_num_tmp_del'] == "1") {
					$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s4_entente_signee_doc_num_tmp'];
					if (file_exists($tmpfile_path)) {
						unlink($tmpfile_path);
					}
					$parameters['s4_entente_signee_doc_num_tmp_del'] = "";
					$parameters['s4_entente_signee_doc_num_tmp'] = "";
				}
				if ($_FILES['s4_entente_signee_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s4_entente_signee_doc_num_up']['error'];
					$file_type = $_FILES['s4_entente_signee_doc_num_up']['type'];
					$file_size = $_FILES['s4_entente_signee_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s4_entente_signee_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s4_entente_signee_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s4_entente_signee_doc_num_up']['tmp_name'];
					$name = $_FILES['s4_entente_signee_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s4_entente_signee_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s4_entente_signee_doc_num_tmp'] = $tmpname;
				}
				break;


			// SECTION #5 //
			case "5":
				if ($parameters['s5_affiche_pagesblanches_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_pagesblanches_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s5_affiche_pagesblanches_doc_num_tmp_del'] = "";
					$parameters['s5_affiche_pagesblanches_doc_num_tmp'] = "";
				}
				if ($_FILES['s5_affiche_pagesblanches_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s5_affiche_pagesblanches_doc_num_up']['error'];
					$file_type = $_FILES['s5_affiche_pagesblanches_doc_num_up']['type'];
					$file_size = $_FILES['s5_affiche_pagesblanches_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_affiche_pagesblanches_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_affiche_pagesblanches_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s5_affiche_pagesblanches_doc_num_up']['tmp_name'];
					$name = $_FILES['s5_affiche_pagesblanches_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s5_affiche_pagesblanches_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s5_affiche_pagesblanches_doc_num_tmp'] = $tmpname;
				}

				if ($parameters['s5_affiche_pagesjaunes_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_pagesjaunes_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s5_affiche_pagesjaunes_doc_num_tmp_del'] = "";
					$parameters['s5_affiche_pagesjaunes_doc_num_tmp'] = "";
				}
				if ($_FILES['s5_affiche_pagesjaunes_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s5_affiche_pagesjaunes_doc_num_up']['error'];
					$file_type = $_FILES['s5_affiche_pagesjaunes_doc_num_up']['type'];
					$file_size = $_FILES['s5_affiche_pagesjaunes_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_affiche_pagesjaunes_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_affiche_pagesjaunes_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s5_affiche_pagesjaunes_doc_num_up']['tmp_name'];
					$name = $_FILES['s5_affiche_pagesjaunes_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s5_affiche_pagesjaunes_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s5_affiche_pagesjaunes_doc_num_tmp'] = $tmpname;
				}

				if ($parameters['s5_affiche_judiciaire_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_judiciaire_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s5_affiche_judiciaire_doc_num_tmp_del'] = "";
					$parameters['s5_affiche_judiciaire_doc_num_tmp'] = "";
				}
				if ($_FILES['s5_affiche_judiciaire_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s5_affiche_judiciaire_doc_num_up']['error'];
					$file_type = $_FILES['s5_affiche_judiciaire_doc_num_up']['type'];
					$file_size = $_FILES['s5_affiche_judiciaire_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_affiche_judiciaire_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_affiche_judiciaire_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s5_affiche_judiciaire_doc_num_up']['tmp_name'];
					$name = $_FILES['s5_affiche_judiciaire_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s5_affiche_judiciaire_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s5_affiche_judiciaire_doc_num_tmp'] = $tmpname;
				}

				if ($parameters['s5_affiche_emplois_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_emplois_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s5_affiche_emplois_doc_num_tmp_del'] = "";
					$parameters['s5_affiche_emplois_doc_num_tmp'] = "";
				}
				if ($_FILES['s5_affiche_emplois_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s5_affiche_emplois_doc_num_up']['error'];
					$file_type = $_FILES['s5_affiche_emplois_doc_num_up']['type'];
					$file_size = $_FILES['s5_affiche_emplois_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_affiche_emplois_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_affiche_emplois_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s5_affiche_emplois_doc_num_up']['tmp_name'];
					$name = $_FILES['s5_affiche_emplois_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s5_affiche_emplois_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s5_affiche_emplois_doc_num_tmp'] = $tmpname;
				}

				if ($parameters['s5_affiche_publication_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_publication_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s5_affiche_publication_doc_num_tmp_del'] = "";
					$parameters['s5_affiche_publication_doc_num_tmp'] = "";
				}
				if ($_FILES['s5_affiche_publication_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s5_affiche_publication_doc_num_up']['error'];
					$file_type = $_FILES['s5_affiche_publication_doc_num_up']['type'];
					$file_size = $_FILES['s5_affiche_publication_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_affiche_publication_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_affiche_publication_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s5_affiche_publication_doc_num_up']['tmp_name'];
					$name = $_FILES['s5_affiche_publication_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s5_affiche_publication_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s5_affiche_publication_doc_num_tmp'] = $tmpname;
				}

				if ($parameters['s5_utilise_fournisseur_papier_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_utilise_fournisseur_papier_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s5_utilise_fournisseur_papier_doc_num_tmp_del'] = "";
					$parameters['s5_utilise_fournisseur_papier_doc_num_tmp'] = "";
				}
				if ($_FILES['s5_utilise_fournisseur_papier_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s5_utilise_fournisseur_papier_doc_num_up']['error'];
					$file_type = $_FILES['s5_utilise_fournisseur_papier_doc_num_up']['type'];
					$file_size = $_FILES['s5_utilise_fournisseur_papier_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_utilise_fournisseur_papier_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s5_utilise_fournisseur_papier_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s5_utilise_fournisseur_papier_doc_num_up']['tmp_name'];
					$name = $_FILES['s5_utilise_fournisseur_papier_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s5_utilise_fournisseur_papier_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s5_utilise_fournisseur_papier_doc_num_tmp'] = $tmpname;
				}
				break;


			// SECTION #6 //
			case "6":
				break;


			// SECTION #7 //
			case "7":
				break;


			// SECTION #8 //
			case "8":
				if ($parameters['s8_annexes_identifiees_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s8_annexes_identifiees_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s8_annexes_identifiees_doc_num_tmp_del'] = "";
					$parameters['s8_annexes_identifiees_doc_num_tmp'] = "";
				}
				if ($_FILES['s8_annexes_identifiees_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s8_annexes_identifiees_doc_num_up']['error'];
					$file_type = $_FILES['s8_annexes_identifiees_doc_num_up']['type'];
					$file_size = $_FILES['s8_annexes_identifiees_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s8_annexes_identifiees_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s8_annexes_identifiees_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s8_annexes_identifiees_doc_num_up']['tmp_name'];
					$name = $_FILES['s8_annexes_identifiees_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s8_annexes_identifiees_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s8_annexes_identifiees_doc_num_tmp'] = $tmpname;
				}

				if ($parameters['s8_document_logo_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s8_document_logo_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s8_document_logo_doc_num_tmp_del'] = "";
					$parameters['s8_document_logo_doc_num_tmp'] = "";
				}
				if ($_FILES['s8_document_logo_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s8_document_logo_doc_num_up']['error'];
					$file_type = $_FILES['s8_document_logo_doc_num_up']['type'];
					$file_size = $_FILES['s8_document_logo_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s8_document_logo_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s8_document_logo_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s8_document_logo_doc_num_up']['tmp_name'];
					$name = $_FILES['s8_document_logo_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s8_document_logo_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s8_document_logo_doc_num_tmp'] = $tmpname;
				}
				break;


			// SECTION #9 //
			case "9":
				if ($parameters['s9_utilise_autre_promo_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s9_utilise_autre_promo_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s9_utilise_autre_promo_doc_num_tmp_del'] = "";
					$parameters['s9_utilise_autre_promo_doc_num_tmp'] = "";
				}
				if ($_FILES['s9_utilise_autre_promo_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s9_utilise_autre_promo_doc_num_up']['error'];
					$file_type = $_FILES['s9_utilise_autre_promo_doc_num_up']['type'];
					$file_size = $_FILES['s9_utilise_autre_promo_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s9_utilise_autre_promo_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s9_utilise_autre_promo_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s9_utilise_autre_promo_doc_num_up']['tmp_name'];
					$name = $_FILES['s9_utilise_autre_promo_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s9_utilise_autre_promo_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s9_utilise_autre_promo_doc_num_tmp'] = $tmpname;
				}

				if ($parameters['s9_utilise_docs_promo_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s9_utilise_docs_promo_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s9_utilise_docs_promo_doc_num_tmp_del'] = "";
					$parameters['s9_utilise_docs_promo_doc_num_tmp'] = "";
				}
				if ($_FILES['s9_utilise_docs_promo_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s9_utilise_docs_promo_doc_num_up']['error'];
					$file_type = $_FILES['s9_utilise_docs_promo_doc_num_up']['type'];
					$file_size = $_FILES['s9_utilise_docs_promo_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s9_utilise_docs_promo_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s9_utilise_docs_promo_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s9_utilise_docs_promo_doc_num_up']['tmp_name'];
					$name = $_FILES['s9_utilise_docs_promo_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s9_utilise_docs_promo_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s9_utilise_docs_promo_doc_num_tmp'] = $tmpname;
				}

				if ($parameters['s9_affiche_conventions_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s9_affiche_conventions_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s9_affiche_conventions_doc_num_tmp_del'] = "";
					$parameters['s9_affiche_conventions_doc_num_tmp'] = "";
				}
				if ($_FILES['s9_affiche_conventions_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s9_affiche_conventions_doc_num_up']['error'];
					$file_type = $_FILES['s9_affiche_conventions_doc_num_up']['type'];
					$file_size = $_FILES['s9_affiche_conventions_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s9_affiche_conventions_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s9_affiche_conventions_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s9_affiche_conventions_doc_num_up']['tmp_name'];
					$name = $_FILES['s9_affiche_conventions_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s9_affiche_conventions_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s9_affiche_conventions_doc_num_tmp'] = $tmpname;
				}
				break;


			// SECTION #10 //
			case "10":
				if ($parameters['s10_utilise_docs_promo_immo_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s10_utilise_docs_promo_immo_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s10_utilise_docs_promo_immo_doc_num_tmp_del'] = "";
					$parameters['s10_utilise_docs_promo_immo_doc_num_tmp'] = "";
				}
				if ($_FILES['s10_utilise_docs_promo_immo_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s10_utilise_docs_promo_immo_doc_num_up']['error'];
					$file_type = $_FILES['s10_utilise_docs_promo_immo_doc_num_up']['type'];
					$file_size = $_FILES['s10_utilise_docs_promo_immo_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s10_utilise_docs_promo_immo_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s10_utilise_docs_promo_immo_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s10_utilise_docs_promo_immo_doc_num_up']['tmp_name'];
					$name = $_FILES['s10_utilise_docs_promo_immo_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s10_utilise_docs_promo_immo_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s10_utilise_docs_promo_immo_doc_num_tmp'] = $tmpname;
				}

				if ($parameters['s10_affiche_docs_promo_immo_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s10_affiche_docs_promo_immo_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s10_affiche_docs_promo_immo_doc_num_tmp_del'] = "";
					$parameters['s10_affiche_docs_promo_immo_doc_num_tmp'] = "";
				}
				if ($_FILES['s10_affiche_docs_promo_immo_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s10_affiche_docs_promo_immo_doc_num_up']['error'];
					$file_type = $_FILES['s10_affiche_docs_promo_immo_doc_num_up']['type'];
					$file_size = $_FILES['s10_affiche_docs_promo_immo_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s10_affiche_docs_promo_immo_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s10_affiche_docs_promo_immo_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s10_affiche_docs_promo_immo_doc_num_up']['tmp_name'];
					$name = $_FILES['s10_affiche_docs_promo_immo_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s10_affiche_docs_promo_immo_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s10_affiche_docs_promo_immo_doc_num_tmp'] = $tmpname;
				}

				if ($parameters['s10_logo_docs_immobiliers_doc_num_tmp_del'] == "1") {
					$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s10_logo_docs_immobiliers_doc_num_tmp'];
					if (file_exists($file_path)) {
						unlink($file_path);
					}
					$parameters['s10_logo_docs_immobiliers_doc_num_tmp_del'] = "";
					$parameters['s10_logo_docs_immobiliers_doc_num_tmp'] = "";
				}
				if ($_FILES['s10_logo_docs_immobiliers_doc_num_up']['name'] != "") {
					$file_error = $_FILES['s10_logo_docs_immobiliers_doc_num_up']['error'];
					$file_type = $_FILES['s10_logo_docs_immobiliers_doc_num_up']['type'];
					$file_size = $_FILES['s10_logo_docs_immobiliers_doc_num_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s10_logo_docs_immobiliers_doc_num_up');\">Une erreur s'est produite lors de l'envoie du document.</a>");
					}
					if ($file_size == 0 || $file_size > $EVALUATIONS_DOC_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('s10_logo_docs_immobiliers_doc_num_up');\">La grosseur du document est trop grande (maximum&nbsp;:" . $EVALUATIONS_DOC_MAX_SIZE . ").</a>");
					}
					$tmp_name = $_FILES['s10_logo_docs_immobiliers_doc_num_up']['tmp_name'];
					$name = $_FILES['s10_logo_docs_immobiliers_doc_num_up']['name'];
					$extension = substr($name, strrpos($name, "."));
					$tmpname = "tmp_s10_logo_docs_immobiliers_doc_num_" . $sid . $extension;
					move_uploaded_file($tmp_name, $BASEPATH . "docs/evaluations/" . $tmpname);
					$parameters['s10_logo_docs_immobiliers_doc_num_tmp'] = $tmpname;
				}
				break;


			// SECTION #11 //
			case "11":
				break;


			// SECTION #12 //
			case "12":
				break;


			// SECTION #14 //
			case "14":
				break;


			// SECTION #15 //
			case "15":
				break;


			// SECTION #16 //
			case "16":
				break;


			// SECTION #17 //
			case "17":
				break;


			// SECTION #18 //
			case "18":
				break;


			// SECTION #19 //
			case "19":
				break;


			// SECTION #20 //
			case "20":
				break;


			// SECTION #21 //
			case "21":
				break;


			// SECTION #22 //
			case "22":
				break;

		}

		updateSessionValues($parameters, $sid);

		return $errorString;
	}


?>
