<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evaluations");
		$Skin->assign("errors", $html);
		$Skin->display("evaluations.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evaluations");
		$Skin->assign("errors", $html);
		$Skin->display("evaluations.tpl");
		exit;	
	}
	
	import("com.pmeinter.Evaluation");
	import("com.quiboweb.form.Form");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);

	
	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$action = getorpost('action');
	$key = getorpost('key');
	$key1 = getorpost('key1');
	$key2 = getorpost('key2');
	$key3 = getorpost('key3');
	$key4 = getorpost('key4');

	$successString = "";
	$errorString = "";
	$formString = "";

	fixEmptySID();

	switch ($_GET['action']) {
		case "add":
			if (count($_SESSION['evaluations']) != 4) {
				$formString .= addEvaluationString();
			} else {
				$errorString = _error("Vous avez atteint la limite d'&eacute;valuations charg&eacute;es (4).");
			}
			break;

		case "modify":
			if (count($_SESSION['evaluations']) != 4) {
				$formString .= modifyEvaluationString();
			} else {
				$errorString = _error("Vous avez atteint la limite d'&eacute;valuations charg&eacute;es (4).");
			}
			break;

		case "delete":
			$formString .= deleteEvaluationString();
			break;

		case "read":
			if (count($_SESSION['evaluations']) != 4) {
				$formString .= readEvaluationString();
			} else {
				$errorString = _error("Vous avez atteint la limite d'&eacute;valuations charg&eacute;es (4).");
			}
			break;

		case "compare":
			if (is_array($_SESSION['evaluations']) && count($_SESSION['evaluations']) == 4) {
				$errorString = _error("Vous avez atteint la limite d'&eacute;valuations charg&eacute;es (4).");
			}
			if ($_SESSION['evaluations']['compare'] != "") {
				$errorString = _error("Vous ne pouvez charger plus d'un comparatif &agrave; la fois.");
			}
			if ($errorString == "") {
				$formString .= compareEvaluationString();
			}
			break;

		case "search":
			$formString .= searchEvaluationString();
			break;
	}


	switch ($_POST['action']) {
		case "add":
			$etudes_key = trim($_POST['etudes_key']);
			$date = trim($_POST['s0_date_visite']);
			$agent_accompagnateur = trim($_POST['s0_nom_agent_accompagnateur']);
			$values = array("etudes_key" => $etudes_key, "s0_date_visite" => $date, "s0_nom_agent_accompagnateur" => $agent_accompagnateur);
			$errorString .= validateAddFormValues("add");
			if ($errorString != "") {
				$formString .= addEvaluationString();
			} else {
				$insertId = insertEvaluationIntoDB($values);
				header("Location: evaluations.php?sid=" . $insertId . "&begin=1&action=modify&requestForm=1&&etudes_key=" . $etudes_key);
				exit;
			}
			break;

		case "modify":
			if ($key == "-1") {
				$formString .= modifyEvaluationString();
				$errorString .= _error("Veuillez choisir une &eacute;valuation dans la liste.");
			}
			if (is_array($_SESSION['evaluations'][$key])) {
				$formString .= modifyEvaluationString();
				$errorString .= _error("Cette &eacute;valuation est d&eacute;j&agrave; charg&eacute;e.");
			}
			if ($errorString == "") {
				header("Location: evaluations.php?sid=" . $key . "&action=modify&begin=1&requestForm=1");
				exit;
			}
			break;

		case "delete":
			if ($key == "-1") {
				$errorString .= _error("Veuillez choisir une &eacute;valuation dans la liste.");
			} else {
				if (!$_POST['confirmation']) {
					$errorString .= _error("Veuillez confirmer la suppression.");
				} else {		
					if (isset($_SESSION['evaluations'][$key])) {
						$errorString .= _error("Cette &eacute;valuation est pr&eacute;sentement charg&eacute;e.<br />Vous devez la fermer avant de la supprimer.");
					}
					if (isset($_SESSION['evaluations']['compare'][$key])) {
						$errorString .= _error("Cette &eacute;valuation est pr&eacute;sentement charg&eacute;e dans un comparatif.<br />Vous devez fermer le comparatif avant de supprimer l'&eacute;valuation.");
					}
					if ($errorString == "") {
						$errorString .= deleteEvaluationFromDB($key);
						if ($errorString != "") {
							$errorString = _error("La supression a &eacute;chou&eacute;.<br />Veuillez contacter votre administrateur si le probl&egrave;me persiste.");
						}
					}
				}
			}
			if ($errorString != "") {
				$formString .= deleteEvaluationString();
			} else {
				$successString .= _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s.");
			}
			break;

		case "read":
			if ($key == "-1") {
				$formString .= modifyEvaluationString();
				$errorString .= _error("Veuillez choisir une &eacute;valuation dans la liste.");
			}
			if (is_array($_SESSION['evaluations'][$key])) {
				$formString .= modifyEvaluationString();
				$errorString .= _error("Cette &eacute;valuation est d&eacute;j&agrave; charg&eacute;e.");
			}
			if ($errorString == "") {
				header("Location: evaluations.php?action=read&sid=" . $key . "&action=read&begin=1&requestForm=1");
				exit;
			}
			break;

		case "compare":
			$nb_keys = 0;
			$keys_url_params = "";
			if ($key1 != "" && $key1 != "-1") {
				$nb_keys++;
				$keys_url_params .= "&key" . $nb_keys . "=" . $key1;
			} else {
				$key1 = NULL;
			}
			if ($key2 != "" && $key2 != "-1") {
				$nb_keys++;
				$keys_url_params .= "&key" . $nb_keys . "=" . $key2;
			} else {
				$key2 = NULL;
			}
			if ($key3 != "" && $key3 != "-1") {
				$nb_keys++;
				$keys_url_params .= "&key" . $nb_keys . "=" . $key3;
			} else {
				$key3 = NULL;
			}
			if ($key4 != "" && $key4 != "-1") {
				$nb_keys++;
				$keys_url_params .= "&key" . $nb_keys . "=" . $key4;
			} else {
				$key4 = NULL;
			}
			if ($nb_keys < 2) {
				$errorString .= _error("Veuillez choisir au minimum 2 &eacute;valuations &agrave; comparer.");
			}

			if ($errorString == "") {
				header("Location: evaluations.php?action=compare&sid=compare" . $keys_url_params . "&begin=1");
				exit;

			} else {
				$formString .= compareEvaluationString();
			}				
			break;

		case "search":
			// ******** À COMPLÉTER ********
			break;
	}

	fixEmptySID();


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evaluations");	
	$Skin->assign("page", "evaluations");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("menu", menuActionString());
	$Skin->assign("menu_title", menuTitleString($action));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString . "<br />");
	$Skin->assign("form", $formString);
	$Skin->display("evaluations.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "Ajout d'&eacute;valuations";
				break;
			case "modify":
				$title = "Modification d'&eacute;valuations";
				break;
			case "delete":
				$title = "Suppression d'&eacute;valuations";
				break;
			case "read":
				$title = "Consultation d'&eacute;valuations";
				break;
			case "compare":
				$title = "Comparaison d'&eacute;valuations";
				break;
			case "search":
				$title = "Recherche d'&eacute;valuations";
				break;
			default:
				$title = "Gestion des &eacute;valuations";
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL, $SKIN_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<link rel=\"stylesheet\" href=\"" . $SKIN_URL . "/css/evals.css\" type=\"text/css\" />\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "modify":
				$html .=
					"<link rel=\"stylesheet\" href=\"" . $SKIN_URL . "/css/evals.css\" type=\"text/css\" />\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "read":
					"<link rel=\"stylesheet\" href=\"" . $SKIN_URL . "/css/evals.css\" type=\"text/css\" />\n";
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "add":
				break;

			case "modify":
				break;

			case "read":
				break;
		}
		return $html;
	}

	function menuActionString() { 
		$html =
			"<h1>Gestion des &Eacute;valuations</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n";

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .=
				"<li><a href=\"index.php?action=add\">Ajouter une &eacute;valuation</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .=
				"<li><a href=\"index.php?action=modify\">Modifier une &eacute;valuation</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .=
				"<li><a href=\"index.php?action=delete\">Supprimer une &eacute;valuation</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3) {
			$html .=
				"<li><a href=\"index.php?action=read\">Consulter une &eacute;valuation</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3) {
			$html .=
				"<li><a href=\"index.php?action=compare\">Comparer des &eacute;valuations</a></li>\n";
		}

		$html .=
			"	</ul>\n".
			"</div>\n";

		return $html;
	}

	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "add":
				$html .= "<h2>Ajouter une &eacute;valuation</h2>\n";
				break;
			case "modify":
				$html .= "<h2>Modifier une &eacute;valuation</h2>\n";
				break;
			case "delete":
				$html .= "<h2>Supprimer une &eacute;valuation</h2>\n";
				break;
			case "read":
				$html .= "<h2>Consulter une &eacute;valuation</h2>\n";
				break;
			case "compare":
				$html .= "<h2>Comparer des &eacute;valuations</h2>\n";
				break;
			case "search":
				$html .= "<h2>Rechercher des &eacute;valuations</h2>\n";
				break;
		}
		return $html;
	}

	function addEvaluationString() { 
		global $DB;
		if (trim($_POST['s1_date_visite']) != "") {
			$s1_date_visite_value = $_POST['s1_date_visite'];
		} else {
			$s1_date_visite_value = "";
		}
		if (trim($_POST['s1_nom_agent_accompagnateur']) != "") {
			$s1_nom_agent_accompagnateur_value = $_POST['s1_nom_agent_accompagnateur'];
		} else {
			$s1_nom_agent_accompagnateur_value = "";
		}

		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n";

		$html .=
			"	<div class=\"entry\">\n".
			"		<label for=\"etudes_key\">Nom de l'&eacute;tude&nbsp;: </label>\n".
			"		<select id=\"etudes_key\" name=\"etudes_key\">\n";

		if ($_SESSION['user_type'] == 1) {
			$html .= "<option class=\"default\" value=\"-1\"> - Choisir une &eacute;tude - </option>\n";
			$DB->query(
				"SELECT `key`, `nom` ".
				"FROM `etudes` ".
				"WHERE `actif` = '1' ".
				"ORDER BY `key` ASC;"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$nom = $DB->getField("nom");
				if ($_POST['etudes_key'] == $key) {
					$selected = "selected=\"selected\"";
				} else {
					$selected = "";
				}
				$html .=
					"<option value=\"" . $key . "\" " . $selected . ">" . $nom . "</option>\n";
			}
		
		} elseif ($_SESSION['user_type'] == 2) {
			$DB->query(
				"SELECT `key`, `nom` ".
				"FROM `etudes` ".
				"WHERE `key` = '" . $_SESSION['user_etudes_key'] . "' AND `actif` = '1' ".
				"ORDER BY `key` ASC;"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$nom = $DB->getField("nom");
				if ($_POST['etudes_key'] == $key) {
					$selected = "selected=\"selected\"";
				} else {
					$selected = "";
				}
				$html .=
					"<option value=\"" . $key . "\" " . $selected . ">" . $nom . "</option>\n";
			}
		}
		
		$html .=
			"		</select>\n".
			"	</div>\n";

		$html .=
			"	<div class=\"entry\">\n".
			"		<label for=\"s0_date_visite\">Date de la visite&nbsp;:</label>\n".
			"		<input type=\"text\" id=\"s0_date_visite\" name=\"s0_date_visite\" value=\"" . $s1_date_visite_value . "\" /><span class=\"note\">format : AAAA-MM-JJ</span>\n".
			"	</div>\n";

		$html .=
			"	<div class=\"entry\">\n".	
			"		<label for=\"s0_nom_agent_accompagnateur\">Nom de l'agent accompagnateur&nbsp;:</label>\n".
			"		<input type=\"text\" id=\"s0_nom_agent_accompagnateur\" name=\"s0_nom_agent_accompagnateur\" value=\"" . $s1_nom_agent_accompagnateur_value . "\" />\n".
			"	</div>\n";

		$html .=
			"	<input type=\"submit\" class=\"submit\" value=\"Poursuivre\" />\n".
			"</form>\n";

		return $html;
	}

	function modifyEvaluationString() { 
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir une &eacute;valuation -</option>\n";

		if ($_SESSION['user_type'] == 1) {
			$DB->query(
				"SELECT ev.key AS evalkey, ev.s0_date_visite AS evaldate, et.nom AS nom_etude ".
				"FROM `evaluations` AS ev, `etudes` AS et ".
				"WHERE ev.actif = '1' ".
				"AND ev.etudes_key = et.key ".
				"GROUP BY ev.key ".
				"ORDER BY ev.createdOn ASC;"
			);
			while ($DB->next_record()) {
				$option = $DB->getField("nom_etude") . " (" . $DB->getField("evaldate");
				$value = $DB->getField("evalkey");
				$html .= "<option value=\"" . $value . "\">" . $option . ")</option>\n";
			}

		} elseif ($_SESSION['user_type'] == 2) {
			$DB->query(
				"SELECT ev.key AS evalkey, ev.s0_date_visite AS evaldate, et.nom AS nom_etude ".
				"FROM `evaluations` AS ev, `etudes` AS et ".
				"WHERE ev.actif = '1' ".
				"AND ev.etudes_key = '" . $_SESSION['user_etudes_key'] . "' ".
				"AND ev.etudes_key = et.key ".
				"GROUP BY ev.key ".
				"ORDER BY ev.createdOn ASC;"
			);
			while ($DB->next_record()) {
				$option = $DB->getField("nom_etude") . " (" . $DB->getField("evaldate");
				$value = $DB->getField("evalkey");
				$html .= "<option value=\"" . $value . "\">" . $option . ")</option>\n";
			}			
		}

		$html .=
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";
		return $html;
	}

	function readEvaluationString() { 
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir une &eacute;valuation -</option>\n";

		if ($_SESSION['user_type'] == 1) {
			$DB->query(
				"SELECT ev.key AS evalkey, ev.s0_date_visite AS evaldate, et.nom AS nom_etude ".
				"FROM `evaluations` AS ev, `etudes` AS et ".
				"WHERE ev.actif = '1' ".
				"AND ev.etudes_key = et.key ".
				"GROUP BY ev.key ".
				"ORDER BY ev.createdOn ASC;"
			);
			while ($DB->next_record()) {
				$option = $DB->getField("nom_etude") . " (" . $DB->getField("evaldate");
				$value = $DB->getField("evalkey");
				$html .= "<option value=\"" . $value . "\">" . $option . ")</option>\n";
			}

		} elseif ($_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3) {
			$DB->query(
				"SELECT ev.key AS evalkey, ev.s0_date_visite AS evaldate, et.nom AS nom_etude ".
				"FROM `evaluations` AS ev, `etudes` AS et ".
				"WHERE ev.actif = '1' ".
				"AND ev.etudes_key = '" . $_SESSION['user_etudes_key'] . "' ".
				"AND ev.etudes_key = et.key ".
				"GROUP BY ev.key ".
				"ORDER BY ev.createdOn ASC;"
			);
			while ($DB->next_record()) {
				$option = $DB->getField("nom_etude") . " (" . $DB->getField("evaldate");
				$value = $DB->getField("evalkey");
				$html .= "<option value=\"" . $value . "\">" . $option . ")</option>\n";
			}			
		}

		$html .=
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function compareEvaluationString() {
		global $DB;

		if ($_SESSION['user_type'] == 1) {
			$DB->query(
				"SELECT ev.key AS evalkey, ev.s0_date_visite AS evaldate, et.nom AS nom_etude ".
				"FROM `evaluations` AS ev, `etudes` AS et ".
				"WHERE ev.actif = '1' AND ev.etudes_key = et.key ".
				"GROUP BY ev.key ".
				"ORDER BY ev.createdOn ASC;"
			);
			while ($DB->next_record()) {
				$option = $DB->getField("nom_etude") . " (" . $DB->getField("evaldate");
				$value = $DB->getField("evalkey");
				$optionString .= "<option value=\"" . $value . "\">" . $option . ")</option>\n";
			}
		
		} elseif (($_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3) && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT ev.key AS evalkey, ev.s0_date_visite AS evaldate, et.nom AS nom_etude ".
				"FROM `evaluations` AS ev, `etudes` AS et ".
				"WHERE ev.actif = '1' AND ev.etudes_key = et.key AND et.key = '" . $_SESSION['user_etudes_key'] . "' ".
				"GROUP BY ev.key ".
				"ORDER BY ev.createdOn ASC;"
			);
			while ($DB->next_record()) {
				$option = $DB->getField("nom_etude") . " (" . $DB->getField("evaldate");
				$value = $DB->getField("evalkey");
				$optionString .= "<option value=\"" . $value . "\">" . $option . ")</option>\n";
			}
		}

		$html .=
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"compare\" />\n";

		for ($i = 1; $i <= 4; $i++) {
			$html .=
				"	<div class=\"entry\">\n".
				"		<select id=\"key" . $i . "\" name=\"key" . $i . "\">\n".
				"			<option class=\"default\" value=\"-1\">- Choisir une &eacute;valuation -</option>\n".
				"			" . $optionString . "\n".
				"		</select>\n".
				"	</div>\n";
		}
		
		$html .=
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function searchEvaluationString() { 
		$html = "";
		// ******* À COMPLÉTER ******* //
		return $html;
	}

	function deleteEvaluationString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir une &eacute;valuation -</option>\n";

		if ($_SESSION['user_type'] == 1) {
			$DB->query(
				"SELECT ev.key AS evalkey, ev.s0_date_visite AS evaldate, et.nom AS nom_etude ".
				"FROM `evaluations` AS ev, `etudes` AS et ".
				"WHERE ev.actif = '1' ".
				"AND ev.etudes_key = et.key ".
				"GROUP BY ev.key ".
				"ORDER BY ev.createdOn ASC;"
			);
			while ($DB->next_record()) {
				$option = $DB->getField("nom_etude") . " (" . $DB->getField("evaldate");
				$value = $DB->getField("evalkey");
				$html .= "<option value=\"" . $value . "\">" . $option . ")</option>\n";
			}

		} elseif ($_SESSION['user_type'] == 2) {
			$DB->query(
				"SELECT ev.key AS evalkey, ev.s0_date_visite AS evaldate, et.nom AS nom_etude ".
				"FROM `evaluations` AS ev, `etudes` AS et ".
				"WHERE ev.actif = '1' ".
				"AND ev.etudes_key = '" . $_SESSION['user_etudes_key'] . "' ".
				"AND ev.etudes_key = et.key ".
				"GROUP BY ev.key ".
				"ORDER BY ev.createdOn ASC;"
			);
			while ($DB->next_record()) {
				$option = $DB->getField("nom_etude") . " (" . $DB->getField("evaldate");
				$value = $DB->getField("evalkey");
				$html .= "<option value=\"" . $value . "\">" . $option . ")</option>\n";
			}			
		}

		$html .=
			"		</select><br />\n".
			"		<br />\n".
			"		Confirmer&nbsp: <input type=\"checkbox\" class=\"checkbox\" id=\"confirmation\" name=\"confirmation\">\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Supprimer\" />\n".
			"</form>\n";

		return $html;
	}

	function validateAddFormValues() {
		$errorString = "";
		if (!($_SESSION['user_type'] == 1 || ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] == $_POST['etudes_key']))) {
			$errorString .= _error("<a href=\"javascript: showErrorField('etudes_key')\">Vous ne poss&eacute;dez pas les droit requis pour effectuer cette op&eacute;ration.</a>");			
		}
		if ($_POST['etudes_key'] == "-1") {
			$errorString .= _error("<a href=\"javascript: showErrorField('etudes_key')\">Veuillez indiquer le nom de l'&eacute;tude.</a>");
		}
		if (trim($_POST['s0_date_visite']) == "") {
			$errorString .= _error("<a href=\"javascript: showErrorField('s0_date_visite')\">Veuillez indiquer la date de l'&eacute;valuation.</a>");
		} else {
			if (!isDateValid($_POST['s0_date_visite'])) {
				$errorString .= _error("<a href=\"javascript: showErrorField('s0_date_visite')\">Le format de la date n'est pas valide.</a>");
			}
		}
		if ($_POST['s0_nom_agent_accompagnateur'] == "") {
			$errorString .= _error("<a href=\"javascript: showErrorField('s0_nom_agent_accompagnateur')\">Veuillez indiquer le nom de l'agent accompagnateur.</a>");
		}
		return $errorString;
	}

	function deleteEvaluationFromDB($key) {
		global $DB;
		$errorString = "";
		$Evaluation = new Evaluation($DB, "", 1);
		$Evaluation->setQKey("key", $key);
		if (!$Evaluation->set("actif", "0")) {
			$errorString .= _error("Une erreur s'est produite lors de la suppression des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
		}
		return $errorString;
	}

	function isDateValid($date) {
		if (strlen($date) == 10) {
			if (strpos($date, "-") === false) {
				return FALSE;
			} else {
				$arrDate = explode( "-", $date);
				if ($date != date("Y-m-d", mktime(0, 0, 0, $arrDate[1], $arrDate[2], $arrDate[0]))) {
					return FALSE;
				}
			}
		} else {
			return FALSE;
		}
		return TRUE;
	}

	function insertEvaluationIntoDB($values) {
		global $DB;
		
		$values = addslashesToValues($values);
		
		$queryFields = "`createdOn`, `createdBy`, `utilisateurs_key`, `actif`, ";
		$queryValues = "CURRENT_TIMESTAMP, '" . $_SESSION['user_key'] . "', '" . $_SESSION['user_key'] . "', '1', ";
		if (is_array($values) && count($values) > 0) {
			foreach ($values as $field => $value) {
				$queryFields .= "`" . $field . "`, ";
				$queryValues .= "'" . $value . "', ";
			}
			$queryFields = substr($queryFields, 0, strlen($queryFields) - 2);
			$queryValues = substr($queryValues, 0, strlen($queryValues) - 2);
			$query =
				"INSERT INTO `evaluations` (".
				$queryFields .
				") VALUES (".
				$queryValues .
				");";
			$DB->query($query);
	
			return $DB->getInsertId();
		}
	}

	function addslashesToValues($parameters = "") {
		if (is_array($parameters) && count($parameters) > 0) {
			foreach ($parameters as $field => $value) {
				if (is_array($value) && count($value) > 0) {
					foreach ($value as $field2 => $value2) {
						if (is_array($value2) && count($value2) > 0) {
							foreach ($value2 as $field3 => $value3) {
								$parameters[$field][$field2][$field3] = addslashes($value3);
							}
						} else {
							$parameters[$field][$field2] = addslashes($value2);
						}
					}
				} else {
					$parameters[$field] = addslashes($value);
				}
			}
		} else {
			$parameters = addslashes($parameters);
		}
		return $parameters;
	}

	function fixEmptySID() {
		if (is_array($_SESSION['evaluations'][NULL])) {
			unset($_SESSION['evaluations'][NULL]);
		}
		if (count($_SESSION['evaluations']) == 0) {
			unset($_SESSION['evaluations']);
		}
	}

?>
