<?PHP

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evaluations");
		$Skin->assign("errors", $html);
		$Skin->display("evaluations.tpl");
		exit;	
	}

/*	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$html = _error("Vous ne disposez pas des droits requis pour effectuer cette op&eacute;ration.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL);
		$Skin->assign("errors", $html);
		$Skin->display("evaluations.tpl");
		exit;	
	}*/

	
	// -------------
	// LES FONCTIONS
	// -------------
	
	function updateDBValues($parameters, $section = "") {
		global $DB, $sid;

		$errorString = "";		

		if ($_SESSION['user_type'] == 2) {
			$DB->query(
				"SELECT `etudes_key` ".
				"FROM `evaluations` ".
				"WHERE `key` = '" . $sid . "';"
			);
			while ($DB->next_record()) {
				$etudes_key = $DB->getField("etudes_key");
			}			
		}
		
		if (!($_SESSION['user_type'] == 1 || ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] == $etudes_key))) {
			$errorString = _error("Vous ne disposez pas des droits requis pour effectuer cette op&eacute;ration.");
			return $errorString;
		}
		
		if (!saveUploadedFiles(addslashesToValues(getParametersFromSession($sid)), $section)) {
			$errorString .= _error("Une erreur s'est produite lors de la sauvegarde de certains documents.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.<br />Code d'erreur&nbsp;: <em>ERROR_Eval_Save_Eval_Files</em>");
		}

		if (!saveAssociationTablesRows(addslashesToValues(getParametersFromSession($sid)), $section)) {
			$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.<br />Code d'erreur&nbsp;: <em>ERROR_Eval_Save_Eval_Assoc</em>");
		}

		if (!saveEvaluationsTableRows(addslashesToValues(getParametersFromSession($sid)), $section)) {
			$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.<br />Code d'erreur&nbsp;: <em>ERROR_Eval_Save_Eval_Main</em>");
		}

		return $errorString;
	}

	function saveEvaluationsTableRows($parameters, $section) {
		global $DB, $sid;
		$eval_key = $parameters['key'];	
		$errorBool = 0;

		$Evaluation = new Evaluation($DB, "", 0);
		$Evaluation->scanfields();				
		$Evaluation->setQKey("key", $sid);

		if ($section == "all") {
			if (is_array($parameters) && count($parameters) > 0) {
				foreach ($parameters as $field => $value) {
					if (preg_match("/^s\d+_/", $field)) {
						$Evaluation->set($field, $value);
					}
				}
			}

		} else {
			if (is_array($parameters) && count($parameters) > 0) {
				foreach ($parameters as $field => $value) {
					if (preg_match("/^s" . $section . "_/", $field)) {
						$Evaluation->set($field, $value);
					}
				}
			}
		}

		$Evaluation->set("updatedOn", "CURRENT_TIMESTAMP", 1);
		$Evaluation->set("updatedBy", $_SESSION['user_key']);

		if (!$Evaluation->saveAll()) {
			$errorBool = 1;
		}

		if (!$errorBool) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function saveUploadedFiles($parameters, $section = "") {
		global $DB, $sid, $BASEPATH;
		$eval_key = $parameters['key'];	
		$errorBool = 0;

		// SECTION #1 //

		// SECTION #2 //

		// SECTION #3 //

		// SECTION #4 //
		if ($section == "all" || $section == "4") {
			if ($parameters['s4_entente_signee_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s4_entente_signee_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s4_entente_signee_doc_num_tmp_del'] = "";
				$parameters['s4_entente_signee_doc_num_tmp'] = "";
			}
			if ($parameters['s4_entente_signee_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s4_entente_signee_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s4_entente_signee_doc_num_del'] = "";
				$parameters['s4_entente_signee_doc_num'] = "";
			}
			if ($parameters['s4_entente_signee_doc_num_tmp'] != "") {
				$tmpname = $parameters['s4_entente_signee_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s4_entente_signee_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s4_entente_signee_doc_num_tmp'] = "";
				$parameters['s4_entente_signee_doc_num'] = $newname;
			}
		}

		// SECTION #5 //
		if ($section == "all" || $section == "5") {
			if ($parameters['s5_affiche_pagesblanches_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_pagesblanches_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s5_affiche_pagesblanches_doc_num_tmp_del'] = "";
				$parameters['s5_affiche_pagesblanches_doc_num_tmp'] = "";
			}
			if ($parameters['s5_affiche_pagesblanches_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_pagesblanches_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s5_affiche_pagesblanches_doc_num_del'] = "";
				$parameters['s5_affiche_pagesblanches_doc_num'] = "";
			}
			if ($parameters['s5_affiche_pagesblanches_doc_num_tmp'] != "") {
				$tmpname = $parameters['s5_affiche_pagesblanches_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s5_affiche_pagesblanches_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s5_affiche_pagesblanches_doc_num_tmp'] = "";
				$parameters['s5_affiche_pagesblanches_doc_num'] = $newname;
			}

			if ($parameters['s5_affiche_pagesjaunes_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_pagesjaunes_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s5_affiche_pagesjaunes_doc_num_tmp_del'] = "";
				$parameters['s5_affiche_pagesjaunes_doc_num_tmp'] = "";
			}
			if ($parameters['s5_affiche_pagesjaunes_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_pagesjaunes_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s5_affiche_pagesjaunes_doc_num_del'] = "";
				$parameters['s5_affiche_pagesjaunes_doc_num'] = "";
			}
			if ($parameters['s5_affiche_pagesjaunes_doc_num_tmp'] != "") {
				$tmpname = $parameters['s5_affiche_pagesjaunes_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s5_affiche_pagesjaunes_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s5_affiche_pagesjaunes_doc_num_tmp'] = "";
				$parameters['s5_affiche_pagesjaunes_doc_num'] = $newname;
			}

			if ($parameters['s5_affiche_judiciaire_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_judiciaire_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s5_affiche_judiciaire_doc_num_tmp_del'] = "";
				$parameters['s5_affiche_judiciaire_doc_num_tmp'] = "";
			}
			if ($parameters['s5_affiche_judiciaire_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_judiciaire_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s5_affiche_judiciaire_doc_num_del'] = "";
				$parameters['s5_affiche_judiciaire_doc_num'] = "";
			}
			if ($parameters['s5_affiche_judiciaire_doc_num_tmp'] != "") {
				$tmpname = $parameters['s5_affiche_judiciaire_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s5_affiche_judiciaire_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s5_affiche_judiciaire_doc_num_tmp'] = "";
				$parameters['s5_affiche_judiciaire_doc_num'] = $newname;
			}

			if ($parameters['s5_affiche_judiciaire_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_judiciaire_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s5_affiche_judiciaire_doc_num_tmp_del'] = "";
				$parameters['s5_affiche_judiciaire_doc_num_tmp'] = "";
			}
			if ($parameters['s5_affiche_judiciaire_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_judiciaire_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s5_affiche_judiciaire_doc_num_del'] = "";
				$parameters['s5_affiche_judiciaire_doc_num'] = "";
			}
			if ($parameters['s5_affiche_judiciaire_doc_num_tmp'] != "") {
				$tmpname = $parameters['s5_affiche_judiciaire_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s5_affiche_judiciaire_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s5_affiche_judiciaire_doc_num_tmp'] = "";
				$parameters['s5_affiche_judiciaire_doc_num'] = $newname;
			}

			if ($parameters['s5_affiche_publication_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_publication_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s5_affiche_publication_doc_num_tmp_del'] = "";
				$parameters['s5_affiche_publication_doc_num_tmp'] = "";
			}
			if ($parameters['s5_affiche_publication_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_affiche_publication_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s5_affiche_publication_doc_num_del'] = "";
				$parameters['s5_affiche_publication_doc_num'] = "";
			}
			if ($parameters['s5_affiche_publication_doc_num_tmp'] != "") {
				$tmpname = $parameters['s5_affiche_publication_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s5_affiche_publication_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s5_affiche_publication_doc_num_tmp'] = "";
				$parameters['s5_affiche_publication_doc_num'] = $newname;
			}

			if ($parameters['s5_utilise_fournisseur_papier_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_utilise_fournisseur_papier_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s5_utilise_fournisseur_papier_doc_num_tmp_del'] = "";
				$parameters['s5_utilise_fournisseur_papier_doc_num_tmp'] = "";
			}
			if ($parameters['s5_utilise_fournisseur_papier_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s5_utilise_fournisseur_papier_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s5_utilise_fournisseur_papier_doc_num_del'] = "";
				$parameters['s5_utilise_fournisseur_papier_doc_num'] = "";
			}
			if ($parameters['s5_utilise_fournisseur_papier_doc_num_tmp'] != "") {
				$tmpname = $parameters['s5_utilise_fournisseur_papier_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s5_utilise_fournisseur_papier_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s5_utilise_fournisseur_papier_doc_num_tmp'] = "";
				$parameters['s5_utilise_fournisseur_papier_doc_num'] = $newname;
			}
		}

		// SECTION #6 //

		// SECTION #7 //

		// SECTION #8 //
		if ($section == "all" || $section == "8") {
			if ($parameters['s8_annexes_identifiees_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s8_annexes_identifiees_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s8_annexes_identifiees_doc_num_tmp_del'] = "";
				$parameters['s8_annexes_identifiees_doc_num_tmp'] = "";
			}
			if ($parameters['s8_annexes_identifiees_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s8_annexes_identifiees_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s8_annexes_identifiees_doc_num_del'] = "";
				$parameters['s8_annexes_identifiees_doc_num'] = "";
			}
			if ($parameters['s8_annexes_identifiees_doc_num_tmp'] != "") {
				$tmpname = $parameters['s8_annexes_identifiees_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s8_annexes_identifiees_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s8_annexes_identifiees_doc_num_tmp'] = "";
				$parameters['s8_annexes_identifiees_doc_num'] = $newname;
			}

			if ($parameters['s8_document_logo_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s8_document_logo_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s8_document_logo_doc_num_tmp_del'] = "";
				$parameters['s8_document_logo_doc_num_tmp'] = "";
			}
			if ($parameters['s8_document_logo_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s8_document_logo_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s8_document_logo_doc_num_del'] = "";
				$parameters['s8_document_logo_doc_num'] = "";
			}
			if ($parameters['s8_document_logo_doc_num_tmp'] != "") {
				$tmpname = $parameters['s8_document_logo_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s8_document_logo_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s8_document_logo_doc_num_tmp'] = "";
				$parameters['s8_document_logo_doc_num'] = $newname;
			}
		}

		// SECTION #9 //
		if ($section == "all" || $section == "9") {
			if ($parameters['s9_utilise_autre_promo_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s9_utilise_autre_promo_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s9_utilise_autre_promo_doc_num_tmp_del'] = "";
				$parameters['s9_utilise_autre_promo_doc_num_tmp'] = "";
			}
			if ($parameters['s9_utilise_autre_promo_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s9_utilise_autre_promo_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s9_utilise_autre_promo_doc_num_del'] = "";
				$parameters['s9_utilise_autre_promo_doc_num'] = "";
			}
			if ($parameters['s9_utilise_autre_promo_doc_num_tmp'] != "") {
				$tmpname = $parameters['s9_utilise_autre_promo_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s9_utilise_autre_promo_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s9_utilise_autre_promo_doc_num_tmp'] = "";
				$parameters['s9_utilise_autre_promo_doc_num'] = $newname;
			}

			if ($parameters['s9_utilise_docs_promo_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s9_utilise_docs_promo_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s9_utilise_docs_promo_doc_num_tmp_del'] = "";
				$parameters['s9_utilise_docs_promo_doc_num_tmp'] = "";
			}
			if ($parameters['s9_utilise_docs_promo_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s9_utilise_docs_promo_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s9_utilise_docs_promo_doc_num_del'] = "";
				$parameters['s9_utilise_docs_promo_doc_num'] = "";
			}
			if ($parameters['s9_utilise_docs_promo_doc_num_tmp'] != "") {
				$tmpname = $parameters['s9_utilise_docs_promo_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s9_utilise_docs_promo_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s9_utilise_docs_promo_doc_num_tmp'] = "";
				$parameters['s9_utilise_docs_promo_doc_num'] = $newname;
			}

			if ($parameters['s9_affiche_conventions_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s9_affiche_conventions_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s9_affiche_conventions_doc_num_tmp_del'] = "";
				$parameters['s9_affiche_conventions_doc_num_tmp'] = "";
			}
			if ($parameters['s9_affiche_conventions_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s9_affiche_conventions_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s9_affiche_conventions_doc_num_del'] = "";
				$parameters['s9_affiche_conventions_doc_num'] = "";
			}
			if ($parameters['s9_affiche_conventions_doc_num_tmp'] != "") {
				$tmpname = $parameters['s9_affiche_conventions_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s9_affiche_conventions_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s9_affiche_conventions_doc_num_tmp'] = "";
				$parameters['s9_affiche_conventions_doc_num'] = $newname;
			}
		}

		// SECTION #10 //
		if ($section == "all" || $section == "10") {
			if ($parameters['s10_utilise_docs_promo_immo_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s10_utilise_docs_promo_immo_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s10_utilise_docs_promo_immo_doc_num_tmp_del'] = "";
				$parameters['s10_utilise_docs_promo_immo_doc_num_tmp'] = "";
			}
			if ($parameters['s10_utilise_docs_promo_immo_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s10_utilise_docs_promo_immo_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s10_utilise_docs_promo_immo_doc_num_del'] = "";
				$parameters['s10_utilise_docs_promo_immo_doc_num'] = "";
			}
			if ($parameters['s10_utilise_docs_promo_immo_doc_num_tmp'] != "") {
				$tmpname = $parameters['s10_utilise_docs_promo_immo_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s10_utilise_docs_promo_immo_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s10_utilise_docs_promo_immo_doc_num_tmp'] = "";
				$parameters['s10_utilise_docs_promo_immo_doc_num'] = $newname;
			}

			if ($parameters['s10_affiche_docs_promo_immo_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s10_affiche_docs_promo_immo_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s10_affiche_docs_promo_immo_doc_num_tmp_del'] = "";
				$parameters['s10_affiche_docs_promo_immo_doc_num_tmp'] = "";
			}
			if ($parameters['s10_affiche_docs_promo_immo_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s10_affiche_docs_promo_immo_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s10_affiche_docs_promo_immo_doc_num_del'] = "";
				$parameters['s10_affiche_docs_promo_immo_doc_num'] = "";
			}
			if ($parameters['s10_affiche_docs_promo_immo_doc_num_tmp'] != "") {
				$tmpname = $parameters['s10_affiche_docs_promo_immo_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s10_affiche_docs_promo_immo_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s10_affiche_docs_promo_immo_doc_num_tmp'] = "";
				$parameters['s10_affiche_docs_promo_immo_doc_num'] = $newname;
			}

			if ($parameters['s10_logo_docs_immobiliers_doc_num_tmp_del'] == "1") {
				$tmpfile_path = $BASEPATH . "docs/evaluations/" . $parameters['s10_logo_docs_immobiliers_doc_num_tmp'];
				if (file_exists($tmpfile_path)) {
					unlink($tmpfile_path);
				}
				$parameters['s10_logo_docs_immobiliers_doc_num_tmp_del'] = "";
				$parameters['s10_logo_docs_immobiliers_doc_num_tmp'] = "";
			}
			if ($parameters['s10_logo_docs_immobiliers_doc_num_del'] == "1") {
				$file_path = $BASEPATH . "docs/evaluations/" . $parameters['s10_logo_docs_immobiliers_doc_num'];
				if (file_exists($file_path)) {
					unlink($file_path);
				}
				$parameters['s10_logo_docs_immobiliers_doc_num_del'] = "";
				$parameters['s10_logo_docs_immobiliers_doc_num'] = "";
			}
			if ($parameters['s10_logo_docs_immobiliers_doc_num_tmp'] != "") {
				$tmpname = $parameters['s10_logo_docs_immobiliers_doc_num_tmp'];
				$tmpname_path = $BASEPATH . "docs/evaluations/" . $tmpname;
				$newname = substr($parameters['s10_logo_docs_immobiliers_doc_num_tmp'], 4);
				$newname_path = $BASEPATH . "docs/evaluations/" . $newname;
				if (file_exists($tmpname_path)) {
					copy($tmpname_path, $newname_path);
				}
				if (!file_exists($newname_path)) {
					$errorBool = 1;
				}
				if (file_exists($tmpname_path)) {
					unlink($tmpname_path);
				}
				$parameters['s10_logo_docs_immobiliers_doc_num_tmp'] = "";
				$parameters['s10_logo_docs_immobiliers_doc_num'] = $newname;
			}
		}

		// SECTION #12 //

		// SECTION #14 //

		// SECTION #15 //

		// SECTION #16 //

		// SECTION #17 //

		// SECTION #18 //

		// SECTION #19 //

		// SECTION #20 //

		// SECTION #21 //

		// SECTION #22 //

		updateSessionValues($parameters, $sid);

		if (!$errorBool) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function saveAssociationTablesRows($parameters, $section = "") {
		global $DB, $sid, $BASEPATH;
		$eval_key = $parameters['key'];
		$errorBool = 0;

		// SECTION #1 //
		if ($section == "all" || $section == "1") {
			// Cleanup de la table `evaluations_etudes_succursales`
			$DB->query(
				"DELETE FROM `evaluations_etudes_succursales` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			// Remplissage de la table `evaluations_etudes_succursales`
			if (is_array($parameters['etudes_succursales_key']) && count($parameters['etudes_succursales_key']) > 0) {
				foreach ($parameters['etudes_succursales_key'] as $etudes_succursales_key) {
					if ($etudes_succursales_key != "" && $etudes_succursales_key != "-1" && $etudes_succursales_key != "-2" && preg_replace("/(\n|\r|\t)/", "", $parameters['etudes_succursales_coordonnees'][$etudes_succursales_key]) != preg_replace("/(\n|\r|\t)/", "", $parameters['etudes_succursales_coordonnees_originales'][$etudes_succursales_key])) {
						$coordonnees = $parameters['etudes_succursales_coordonnees'][$etudes_succursales_key];
						$DB->query(
							"INSERT INTO `evaluations_etudes_succursales` ".
							"(`evaluations_key`, `etudes_succursales_key`, `coordonnees`) ".
							"VALUES ('" . $eval_key . "', '" . $etudes_succursales_key . "', '" . $coordonnees . "')"
						);
					}
				}
			}
		}

		// SECTION #2 //
		if ($section == "all" || $section == "2") {
			// Cleanup de la table `evaluations_notaires`
			$DB->query( 
				"DELETE FROM `evaluations_notaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			// Cleanup de la table `evaluations_notaires_titres`
			$DB->query( 
				"DELETE FROM `evaluations_notaires_titres` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			// Remplissage de la table `evaluations_notaires`
			if (is_array($parameters['notaires_employes_key']) && count($parameters['notaires_employes_key']) > 0) {
				foreach ($parameters['notaires_employes_key'] as $employes_key) {
					if ($employes_key != "" && $employes_key != "-1") {
						$nom = $parameters['notaires_nom'][$employes_key];
						$prenom = $parameters['notaires_prenom'][$employes_key];
						$courriel = $parameters['notaires_courriel'][$employes_key];
						$annee_debut_pratique = $parameters['notaires_annee_debut_pratique'][$employes_key];
						$associe = $parameters['notaires_associe'][$employes_key];
						$present_evaluation = $parameters['notaires_present_evaluation'][$employes_key];
						$raison_absence = $parameters['notaires_raison_absence'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_notaires` ".
							"(`evaluations_key`, `employes_key`, `nom`, `prenom`, `courriel`, `annee_debut_pratique`, `associe`, `present_evaluation`, `raison_absence`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $nom . "', '" . $prenom . "', '" . $courriel . "', '" . $annee_debut_pratique . "', '" . $associe . "', '" . $present_evaluation . "', '" . $raison_absence . "');"
						);
					}
				}
			}
			// Remplissage de la table `evaluations_notaires_titres`
			if (is_array($parameters['notaires_titres_employes_key']) && count($parameters['notaires_titres_employes_key']) > 0) {
				foreach ($parameters['notaires_titres_employes_key'] as $employes_key) {
					if (is_array($parameters['notaires_titres_employes_key'][$employes_key]) && count($parameters['notaires_titres_employes_key'][$employes_key]) > 0) {
						foreach ($parameters['notaires_titres_titres_key'][$employes_key] as $nb => $titres_key) {
							$DB->query(
								"INSERT INTO `evaluations_notaires_titres` ".
								"(`evaluations_key`, `employes_key`, `titres_key`) ".
								"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $titres_key . "');"
							);
						}
					}
				}
			}
		}

		// SECTION #3 //
		if ($section == "all" || $section == "3") {
			// Cleanup de la table `evaluations_employes`
			$DB->query( 
				"DELETE FROM `evaluations_employes` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			// Cleanup de la table `evaluations_employes_fonctions`
			$DB->query( 
				"DELETE FROM `evaluations_employes_fonctions` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			// Remplissage de la table `evaluations_employes`
			if (is_array($parameters['employes_employes_key']) && count($parameters['employes_employes_key']) > 0) {
				foreach ($parameters['employes_employes_key'] as $employes_key) {
					if ($employes_key != "" && $employes_key != "-1") {
						$nom = $parameters['employes_nom'][$employes_key];
						$prenom = $parameters['employes_prenom'][$employes_key];
						$courriel = $parameters['employes_courriel'][$employes_key];
						$present_evaluation = $parameters['employes_present_evaluation'][$employes_key];
						$raison_absence = $parameters['employes_raison_absence'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_employes` ".
							"(`evaluations_key`, `employes_key`, `nom`, `prenom`, `courriel`, `present_evaluation`, `raison_absence`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $nom . "', '" . $prenom . "', '" . $courriel . "', '" . $present_evaluation . "', '" . $raison_absence . "');"
						);
					}
				}
			}
			// Remplissage de la table `evaluations_employes_fonctions`
			if (is_array($parameters['employes_employes_key']) && count($parameters['employes_employes_key']) > 0) {
				foreach ($parameters['employes_employes_key'] as $employes_key) {
					if (is_array($parameters['employes_fonctions_key'][$employes_key]) && count($parameters['employes_fonctions_key'][$employes_key]) > 0) {
						foreach ($parameters['employes_fonctions_key'][$employes_key] as $nb => $fonctions_key) {
							$DB->query(
								"INSERT INTO `evaluations_employes_fonctions` ".
								"(`evaluations_key`, `employes_key`, `fonctions_key`) ".
								"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $fonctions_key . "');"
							);
						}
					}
				}
			}
		}

		// SECTION #4 //
		if ($section == "all" || $section == "4") {
			// Cleanup de la table `evaluations_signataires_adhesion`
			$DB->query( 
				"DELETE FROM `evaluations_signataires_adhesion` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			// Remplissage de la table `evaluations_signataires_adhesion`
			if (is_array($parameters['signataires_adhesion_employes_key']) && count($parameters['signataires_adhesion_employes_key']) > 0) {
				foreach ($parameters['signataires_adhesion_employes_key'] as $nb => $employes_key) {
					if ($employes_key != "") {
						$DB->query(
							"INSERT INTO `evaluations_signataires_adhesion` ".
							"(`evaluations_key`, `employes_key`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "');"
						);
					}
				}
			}
		}

		// SECTION #5 //
		if ($section == "all" || $section == "5") {
			// Cleanup de la table `evaluations_produits_visibles`
			$DB->query( 
				"DELETE FROM `evaluations_produits_visibles` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			// Cleanup de la table `evaluations_produits_papeterie_utilisee`
			$DB->query( 
				"DELETE FROM `evaluations_produits_papeterie_utilisee` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			// Remplissage de la table `evaluations_produits_visibles`
			if (is_array($parameters['produits_visibles_produits_key']) && count($parameters['produits_visibles_produits_key']) > 0) {
				foreach ($parameters['produits_visibles_produits_key'] as $produits_key) {
					if ($parameters['produits_visibles_visible'][$produits_key] == "1") {
						$nom = $parameters['produits_visibles_nom'][$produits_key];
						$DB->query(
							"INSERT INTO `evaluations_produits_visibles` ".
							"(`evaluations_key`, `produits_key`, `nom`) ".
							"VALUES ('" . $eval_key . "', '" . $produits_key . "', '" . $nom . "');"
						);
					}
				}
			}
			// Remplissage de la table `evaluations_produits_papeterie_utilisee`
			if (is_array($parameters['produits_papeterie_utilisee_produits_key']) && count($parameters['produits_papeterie_utilisee_produits_key']) > 0) {
				foreach ($parameters['produits_papeterie_utilisee_produits_key'] as $produits_key) {
					if ($produits_key != "") {
						$nom = $parameters['produits_papeterie_utilisee_nom'][$produits_key];
						$qte_2003 = $parameters['produits_papeterie_utilisee_qte_2003'][$produits_key];
						$qte_2004 = $parameters['produits_papeterie_utilisee_qte_2004'][$produits_key];
						$utilisation = $parameters['produits_papeterie_utilisee_utilisation'][$produits_key];
						$raisons = $parameters['produits_papeterie_utilisee_raisons'][$produits_key];
	
						$DB->query(
							"INSERT INTO `evaluations_produits_papeterie_utilisee` ".
							"(`evaluations_key`, `produits_key`, `nom`, `qte_2003`, `qte_2004`, `utilisation`, `raisons`) ".
							"VALUES ('" . $eval_key . "', '" . $produits_key . "', '" . $nom . "', '" . $qte_2003 . "', '" . $qte_2004 . "', '" . $utilisation . "', '" . $raisons . "');"
						);
					}
				}
			}
		}

		// SECTION #6 //

		// SECTION #7 //
		if ($section == "all" || $section == "7") {
			// Cleanup de la table `evaluations_notaires_directions_de_travail`
			$DB->query( 
				"DELETE FROM `evaluations_notaires_directions_de_travail` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			// Remplissage de la table `evaluations_notaires_directions_de_travail` ( directions_de_travail_key = 1 )
			if (is_array($parameters['notaires_directions_de_travail_droit_des_affaires_employes_key']) && count($parameters['notaires_directions_de_travail_droit_des_affaires_employes_key']) > 0) { 
				foreach ($parameters['notaires_directions_de_travail_droit_des_affaires_employes_key'] as $nb => $employes_key) {
					if ($employes_key != "") {
						$DB->query(
							"INSERT INTO `evaluations_notaires_directions_de_travail` ".
							"(`evaluations_key`, `employes_key`, `directions_de_travail_key`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '1');"
						);
					}
				}
			}
			// Remplissage de la table `evaluations_notaires_directions_de_travail` ( directions_de_travail_key = 2 )
			if (is_array($parameters['notaires_directions_de_travail_droit_des_personnes_employes_key']) && count($parameters['notaires_directions_de_travail_droit_des_personnes_employes_key']) > 0) {
				foreach ($parameters['notaires_directions_de_travail_droit_des_personnes_employes_key'] as $nb => $employes_key) {
					if ($employes_key != "") {
						$DB->query(
							"INSERT INTO `evaluations_notaires_directions_de_travail` ".
							"(`evaluations_key`, `employes_key`, `directions_de_travail_key`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '2');"
						);
					}
				}
			}
			// Remplissage de la table `evaluations_notaires_directions_de_travail` ( directions_de_travail_key = 3 )
			if (is_array($parameters['notaires_directions_de_travail_gestion_des_bureaux_employes_key']) && count($parameters['notaires_directions_de_travail_gestion_des_bureaux_employes_key']) > 0) {
				foreach ($parameters['notaires_directions_de_travail_gestion_des_bureaux_employes_key'] as $nb => $employes_key) {
					if ($employes_key != "") {
						$DB->query(
							"INSERT INTO `evaluations_notaires_directions_de_travail` ".
							"(`evaluations_key`, `employes_key`, `directions_de_travail_key`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '3');"
						);
					}
				}
			}
		}

		// SECTION #8 //
		if ($section == "all" || $section == "8") {
			// Cleanup de la table `evaluations_domaines_daffaires_notaires` ( domaines_daffaires_key = 1 )
			$DB->query( 
				"DELETE FROM `evaluations_domaines_daffaires_notaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '1';"
			);
			// Remplissage de la table `evaluations_domaines_daffaires_notaires` ( domaines_daffaires_key = 1 )
			if (is_array($parameters['domaines_daffaires_notaires_droit_des_affaires_employes_key']) && count($parameters['domaines_daffaires_notaires_droit_des_affaires_employes_key']) > 0) {
				foreach ($parameters['domaines_daffaires_notaires_droit_des_affaires_employes_key'] as $employes_key) {
					if ($parameters['domaines_daffaires_notaires_droit_des_affaires_selectionne'][$employes_key] == "1") {
						$temps_consacre_pourcent = $parameters['domaines_daffaires_notaires_droit_des_affaires_temps_consacre_pourcent'][$employes_key];
						$nom = $parameters['domaines_daffaires_notaires_droit_des_affaires_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_domaines_daffaires_notaires` ".
							"(`evaluations_key`, `employes_key`, `domaines_daffaires_key`, `temps_consacre_pourcent`, `nom`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '1', '" . $temps_consacre_pourcent . "', '" . $nom . "');"
						);
					}
				}
			}
			// Cleanup de la table `evaluations_domaines_daffaires_collaboratrices` ( domaines_daffaires_key = 1 )
			$DB->query( 
				"DELETE FROM `evaluations_domaines_daffaires_collaboratrices` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '1';"
			);
			// Remplissage de la table `evaluations_domaines_daffaires_collaboratrices` ( domaines_daffaires_key = 1 )
			if (is_array($parameters['domaines_daffaires_collaboratrices_droit_des_affaires_employes_key']) && count($parameters['domaines_daffaires_collaboratrices_droit_des_affaires_employes_key']) > 0) {
				foreach ($parameters['domaines_daffaires_collaboratrices_droit_des_affaires_employes_key'] as $employes_key) {
					if ($parameters['domaines_daffaires_collaboratrices_droit_des_affaires_selectionne'][$employes_key] == "1") {
						$temps_consacre_pourcent = $parameters['domaines_daffaires_collaboratrices_droit_des_affaires_temps_consacre_pourcent'][$employes_key];
						$nom = $parameters['domaines_daffaires_collaboratrices_droit_des_affaires_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_domaines_daffaires_collaboratrices` ".
							"(`evaluations_key`, `employes_key`, `domaines_daffaires_key`, `temps_consacre_pourcent`, `nom`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '1', '" . $temps_consacre_pourcent . "', '" . $nom . "');"
						);
					}
				}
			}
			// Cleanup de la table `evaluations_formations_notaires_domaines_daffaires` ( formation_greffe_corpo = 1 )
			$DB->query( 
				"DELETE FROM `evaluations_formations_notaires_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_greffe_corpo` = '1';"
			);
			// Remplissage de la table `evaluations_formations_notaires_domaines_daffaires` ( formation_greffe_corpo = 1 )
			if (is_array($parameters['formations_notaires_domaines_daffaires_formation_greffe_corpo_employes_key']) && count($parameters['formations_notaires_domaines_daffaires_formation_greffe_corpo_employes_key']) > 0) {
				foreach ($parameters['formations_notaires_domaines_daffaires_formation_greffe_corpo_employes_key'] as $employes_key) {
					if ($parameters['formations_notaires_domaines_daffaires_formation_greffe_corpo_selectionne'][$employes_key] == "1") {
						$nom = $parameters['formations_notaires_domaines_daffaires_formation_greffe_corpo_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_formations_notaires_domaines_daffaires` ".
							"(`evaluations_key`, `employes_key`, `nom`, `formation_greffe_corpo`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $nom . "', '1');"
						);
					}
				}
			}
			// Cleanup de la table `evaluations_formations_collaboratrices_domaines_daffaires` ( formation_greffe_corpo = 1 )
			$DB->query( 
				"DELETE FROM `evaluations_formations_collaboratrices_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_greffe_corpo` = '1';"
			);
			// Remplissage de la table `evaluations_formations_collaboratrices_domaines_daffaires` ( formation_greffe_corpo = 1 )
			if (is_array($parameters['formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_employes_key']) && count($parameters['formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_employes_key']) > 0) {
				foreach ($parameters['formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_employes_key'] as $employes_key) {
					if ($parameters['formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_selectionne'][$employes_key] == "1") {
						$nom = $parameters['formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_formations_collaboratrices_domaines_daffaires` ".
							"(`evaluations_key`, `employes_key`, `nom`, `formation_greffe_corpo`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $nom . "', '1');"
						);
					}
				}
			}
			// Cleanup de la table `evaluations_formations_notaires_domaines_daffaires` ( formation_assistant_corpo = 1 )
			$DB->query( 
				"DELETE FROM `evaluations_formations_notaires_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_assistant_corpo` = '1';"
			);
			// Remplissage de la table `evaluations_formations_notaires_domaines_daffaires` ( formation_assistant_corpo = 1 )
			if (is_array($parameters['formations_notaires_domaines_daffaires_formation_assistant_corpo_employes_key']) && count($parameters['formations_notaires_domaines_daffaires_formation_assistant_corpo_employes_key']) > 0) {
				foreach ($parameters['formations_notaires_domaines_daffaires_formation_assistant_corpo_employes_key'] as $employes_key) {
					if ($parameters['formations_notaires_domaines_daffaires_formation_assistant_corpo_selectionne'][$employes_key] == "1") {
						$nom = $parameters['formations_notaires_domaines_daffaires_formation_assistant_corpo_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_formations_notaires_domaines_daffaires` ".
							"(`evaluations_key`, `employes_key`, `nom`, `formation_assistant_corpo`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $nom . "', '1');"
						);
					}
				}
			}
			// Cleanup de la table `evaluations_formations_collaboratrices_domaines_daffaires` ( formation_assistant_corpo = 1 )
			$DB->query( 
				"DELETE FROM `evaluations_formations_collaboratrices_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_assistant_corpo` = '1';"
			);
			// Remplissage de la table `evaluations_formations_collaboratrices_domaines_daffaires` ( formation_assistant_corpo = 1 )
			if (is_array($parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_employes_key']) && count($parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_employes_key']) > 0) {
				foreach ($parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_employes_key'] as $employes_key) {
					if ($parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_selectionne'][$employes_key] == "1") {
						$nom = $parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_formations_collaboratrices_domaines_daffaires` ".
							"(`evaluations_key`, `employes_key`, `nom`, `formation_assistant_corpo`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $nom . "', '1');"
						);
					}
				}
			}
		}

		// SECTION #9 //
		if ($section == "all" || $section == "9") {
			// Cleanup de la table `evaluations_domaines_daffaires_notaires` ( domaines_daffaires_key = 2 )
			$DB->query( 
				"DELETE FROM `evaluations_domaines_daffaires_notaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '2';"
			);
			// Remplissage de la table `evaluations_domaines_daffaires_notaires` ( domaines_daffaires_key = 2 )
			if (is_array($parameters['domaines_daffaires_notaires_droit_des_personnes_employes_key']) && count($parameters['domaines_daffaires_notaires_droit_des_personnes_employes_key']) > 0) {
				foreach ($parameters['domaines_daffaires_notaires_droit_des_personnes_employes_key'] as $employes_key) {
					if ($parameters['domaines_daffaires_notaires_droit_des_personnes_selectionne'][$employes_key] == "1") {
						$temps_consacre_pourcent = $parameters['domaines_daffaires_notaires_droit_des_personnes_temps_consacre_pourcent'][$employes_key];
						$nom = $parameters['domaines_daffaires_notaires_droit_des_personnes_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_domaines_daffaires_notaires` ".
							"(`evaluations_key`, `employes_key`, `domaines_daffaires_key`, `temps_consacre_pourcent`, `nom`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '2', '" . $temps_consacre_pourcent . "', '" . $nom . "');"
						);
					}
				}
			}
			// Cleanup de la table `evaluations_domaines_daffaires_collaboratrices` ( domaines_daffaires_key = 2 )
			$DB->query( 
				"DELETE FROM `evaluations_domaines_daffaires_collaboratrices` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '2';"
			);
			// Remplissage de la table `evaluations_domaines_daffaires_collaboratrices` ( domaines_daffaires_key = 2 )
			if (is_array($parameters['domaines_daffaires_collaboratrices_droit_des_personnes_employes_key']) && count($parameters['domaines_daffaires_collaboratrices_droit_des_personnes_employes_key']) > 0) {
				foreach ($parameters['domaines_daffaires_collaboratrices_droit_des_personnes_employes_key'] as $employes_key) {
					if ($parameters['domaines_daffaires_collaboratrices_droit_des_personnes_selectionne'][$employes_key] == "1") {
						$temps_consacre_pourcent = $parameters['domaines_daffaires_collaboratrices_droit_des_personnes_temps_consacre_pourcent'][$employes_key];
						$nom = $parameters['domaines_daffaires_collaboratrices_droit_des_personnes_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_domaines_daffaires_collaboratrices` ".
							"(`evaluations_key`, `employes_key`, `domaines_daffaires_key`, `temps_consacre_pourcent`, `nom`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '2', '" . $temps_consacre_pourcent . "', '" . $nom . "');"
						);
					}
				}
			}
			// Cleanup de la table `evaluations_formations_notaires_domaines_daffaires` ( formation_testament_fiduciaire = 1 )
			$DB->query( 
				"DELETE FROM `evaluations_formations_notaires_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_testament_fiduciaire` = '1';"
			);
			// Remplissage de la table `evaluations_formations_notaires_domaines_daffaires` ( formation_testament_fiduciaire = 1 )
			if (is_array($parameters['formations_notaires_domaines_daffaires_formation_testament_fiduciaire_employes_key']) && count($parameters['formations_notaires_domaines_daffaires_formation_testament_fiduciaire_employes_key']) > 0) {
				foreach ($parameters['formations_notaires_domaines_daffaires_formation_testament_fiduciaire_employes_key'] as $employes_key) {
					if ($parameters['formations_notaires_domaines_daffaires_formation_testament_fiduciaire_selectionne'][$employes_key] == "1") {
						$nom = $parameters['formations_notaires_domaines_daffaires_formation_testament_fiduciaire_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_formations_notaires_domaines_daffaires` ".
							"(`evaluations_key`, `employes_key`, `nom`, `formation_testament_fiduciaire`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $nom . "', '1');"
						);
					}
				}
			}
			// Cleanup de la table `evaluations_formations_collaboratrices_domaines_daffaires` ( formation_testament_fiduciaire = 1 )
			$DB->query( 
				"DELETE FROM `evaluations_formations_collaboratrices_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_testament_fiduciaire` = '1';"
			);
			// Remplissage de la table `evaluations_formations_collaboratrices_domaines_daffaires` ( formation_testament_fiduciaire = 1 )
			if (is_array($parameters['formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_employes_key']) && count($parameters['formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_employes_key']) > 0) {
				foreach ($parameters['formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_employes_key'] as $employes_key) {
					if ($parameters['formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_selectionne'][$employes_key] == "1") {
						$nom = $parameters['formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_formations_collaboratrices_domaines_daffaires` ".
							"(`evaluations_key`, `employes_key`, `nom`, `formation_testament_fiduciaire`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $nom . "', '1');"
						);
					}
				}
			}
		}

		// SECTION #10 //
		if ($section == "all" || $section == "10") {
			// Cleanup de la table `evaluations_domaines_daffaires_notaires` ( domaines_daffaires_key = 3 )
			$DB->query( 
				"DELETE FROM `evaluations_domaines_daffaires_notaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '3';"
			);
			// Remplissage de la table `evaluations_domaines_daffaires_notaires` ( domaines_daffaires_key = 3 )
			if (is_array($parameters['domaines_daffaires_notaires_droit_immo_employes_key']) && count($parameters['domaines_daffaires_notaires_droit_immo_employes_key']) > 0) {
				foreach ($parameters['domaines_daffaires_notaires_droit_immo_employes_key'] as $employes_key) {
					if ($parameters['domaines_daffaires_notaires_droit_immo_selectionne'][$employes_key] == "1") {
						$temps_consacre_pourcent = $parameters['domaines_daffaires_notaires_droit_immo_temps_consacre_pourcent'][$employes_key];
						$nom = $parameters['domaines_daffaires_notaires_droit_immo_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_domaines_daffaires_notaires` ".
							"(`evaluations_key`, `employes_key`, `domaines_daffaires_key`, `temps_consacre_pourcent`, `nom`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '3', '" . $temps_consacre_pourcent . "', '" . $nom . "');"
						);
					}
				}
			}
			// Cleanup de la table `evaluations_domaines_daffaires_collaboratrices` ( domaines_daffaires_key = 3 )
			$DB->query( 
				"DELETE FROM `evaluations_domaines_daffaires_collaboratrices` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '3';"
			);
			// Remplissage de la table `evaluations_domaines_daffaires_collaboratrices` ( domaines_daffaires_key = 3 )
			if (is_array($parameters['domaines_daffaires_collaboratrices_droit_immo_employes_key']) && count($parameters['domaines_daffaires_collaboratrices_droit_immo_employes_key']) > 0) {
				foreach ($parameters['domaines_daffaires_collaboratrices_droit_immo_employes_key'] as $employes_key) {
					if ($parameters['domaines_daffaires_collaboratrices_droit_immo_selectionne'][$employes_key] == "1") {
						$temps_consacre_pourcent = $parameters['domaines_daffaires_collaboratrices_droit_immo_temps_consacre_pourcent'][$employes_key];
						$nom = $parameters['domaines_daffaires_collaboratrices_droit_immo_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_domaines_daffaires_collaboratrices` ".
							"(`evaluations_key`, `employes_key`, `domaines_daffaires_key`, `temps_consacre_pourcent`, `nom`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '3', '" . $temps_consacre_pourcent . "', '" . $nom . "');"
						);
					}
				}
			}
			// Cleanup de la table `evaluations_formations_notaires_domaines_daffaires` ( formation_assistant_immo = 1 )
			$DB->query( 
				"DELETE FROM `evaluations_formations_notaires_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_assistant_immo` = '1';"
			);
			// Remplissage de la table `evaluations_formations_notaires_domaines_daffaires` ( formation_assistant_immo = 1 )
			if (is_array($parameters['formations_notaires_domaines_daffaires_formation_assistant_immo_employes_key']) && count($parameters['formations_notaires_domaines_daffaires_formation_assistant_immo_employes_key']) > 0) {
				foreach ($parameters['formations_notaires_domaines_daffaires_formation_assistant_immo_employes_key'] as $employes_key) {
					if ($parameters['formations_notaires_domaines_daffaires_formation_assistant_immo_selectionne'][$employes_key] == "1") {
						$nom = $parameters['formations_notaires__domaines_daffaires_formation_assistant_immo_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_formations_notaires_domaines_daffaires` ".
							"(`evaluations_key`, `employes_key`, `nom`, `formation_assistant_immo`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $nom . "', '1');"
						);
					}
				}
			}
			// Cleanup de la table `evaluations_formations_collaboratrices_domaines_daffaires` ( formation_assistant_immo = 1 )
			$DB->query( 
				"DELETE FROM `evaluations_formations_collaboratrices_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_assistant_immo` = '1';"
			);
			// Remplissage de la table `evaluations_formations_collaboratrices_domaines_daffaires` ( formation_assistant_immo = 1 )
			if (is_array($parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_immo_employes_key']) && count($parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_immo_employes_key']) > 0) {
				foreach ($parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_immo_employes_key'] as $employes_key) {
					if ($parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_immo_selectionne'][$employes_key] == "1") {
						$nom = $parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_immo_nom'][$employes_key];
						$DB->query(
							"INSERT INTO `evaluations_formations_collaboratrices_domaines_daffaires` ".
							"(`evaluations_key`, `employes_key`, `nom`, `formation_assistant_immo`) ".
							"VALUES ('" . $eval_key . "', '" . $employes_key . "', '" . $nom . "', '1');"
						);
					}
				}
			}
		}

		// SECTION #11 //
		if ($section == "all" || $section == "11") {
			// Cleanup de la table `evaluations_produits_articles_promotionnels`
			$DB->query( 
				"DELETE FROM `evaluations_produits_articles_promotionnels` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			// Remplissage de la table `evaluations_produits_articles_promotionnels`
			if (is_array($parameters['produits_articles_promotionnels_produits_key']) && count($parameters['produits_articles_promotionnels_produits_key']) > 0) {
				foreach ($parameters['produits_articles_promotionnels_produits_key'] as $produits_key) {
					$qte = $parameters['produits_articles_promotionnels_qte'][$produits_key];
					$nom = $parameters['produits_articles_promotionnels_nom'][$produits_key];
					$DB->query(
						"INSERT INTO `evaluations_produits_articles_promotionnels` ".
						"(`evaluations_key`, `produits_key`, `nom`, `qte`) ".
						"VALUES ('" . $eval_key . "', '" . $produits_key . "', '" . $nom . "', '" . $qte . "');"
					);
				}
			}
		}

		// SECTION #12 //

		// SECTION #14 //

		// SECTION #15 //

		// SECTION #16 //

		// SECTION #17 //

		// SECTION #18 //

		// SECTION #19 //

		// SECTION #20 //

		// SECTION #21 //

		// SECTION #22 //

		if (!$errorBool) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	function addslashesToValues($parameters = "") {
		if (is_array($parameters) && count($parameters) > 0) {
			foreach ($parameters as $field => $value) {
				if (is_array($value) && count($value) > 0) {
					foreach ($value as $field2 => $value2) {
						if (is_array($value2) && count($value2) > 0) {
							foreach ($value2 as $field3 => $value3) {
								$parameters[$field][$field2][$field3] = addslashes($value3);
							}
						} else {
							$parameters[$field][$field2] = addslashes($value2);
						}
					}
				} else {
					$parameters[$field] = addslashes($value);
				}
			}
		} else {
			$parameters = addslashes($parameters);
		}
		return $parameters;
	}

?>
