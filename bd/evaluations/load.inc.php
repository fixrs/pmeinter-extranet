<?PHP

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evaluations");
		$Skin->assign("errors", $html);
		$Skin->display("evaluations.tpl");
		exit;	
	}

/*	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3)) {
		$html = _error("Vous ne disposez pas des droits requis pour effectuer cette op&eacute;ration.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL);
		$Skin->assign("errors", $html);
		$Skin->display("evaluations.tpl");
		exit;	
	}*/

	
	// -------------
	// LES FONCTIONS
	// -------------
	
	function getParametersFromDB($sid, $section) {
		global $DB;
		if ($sid == "compare") {
			if (is_array($_SESSION['evaluations']['compare']) && count($_SESSION['evaluations']['compare']) > 0) {
				foreach ($_SESSION['evaluations']['compare'] as $sid_key => $useless) {
					$params0 = loadEvalInfoValues($sid_key);
					$params1 = loadEvaluationTableRows($sid_key, $section);
					$params2 = loadAssociationTablesRows($params0, $section);
					$parameters[$sid_key] = array_merge($params0, $params1, $params2);
				}
			}
		} else {
			$params0 = loadEvalInfoValues($sid);
			$params1 = loadEvaluationTableRows($sid, $section);
			$params2 = loadAssociationTablesRows($params0, $section);
			$parameters = array_merge($params0, $params1, $params2);
		}

		return $parameters;
	}

	function loadEvalInfoValues($key) {
		global $DB;
		$parameters = array();
		$Evaluation = new Evaluation($DB, "", 0);
		$Evaluation->scanfields();
		$Evaluation->setQKey("key", $key);
		$pregmatch = "/^(key|actif|createdOn|createdBy|updatedOn|updatedBy|utilisateurs_key|etudes_key|s0_date_visite|s0_nom_agent_accompagnateur)$/";
		$Evaluation->loadAll($pregmatch);
		$parameters = $Evaluation->getAll($pregmatch);

		$DB->query(
			"SELECT et.nom AS etudes_nom ".
			"FROM `evaluations` AS ev, `etudes` AS et ".
			"WHERE ev.key = '" . $key . "' AND ev.etudes_key = et.key;"
		);
		while ($DB->next_record()) {
			$parameters['etudes_nom'] = $DB->getField("etudes_nom");
		}

		return $parameters;
	}

	function loadEvaluationTableRows($key, $section) {
		global $DB;
		if ($section == "all") {
			$pregmatch = "";
		} else {
			$pregmatch = "/^s" . $section . "_/";
		}

		$Evaluation = new Evaluation($DB, "", 0);
		$Evaluation->scanfields();
		$Evaluation->setQKey("key", $key);
		$Evaluation->loadAll();

		$parameters = $Evaluation->getAll($pregmatch);
		
		return $parameters;
	}

	function loadAssociationTablesRows($parameters, $section) {
		global $DB;
		$eval_key = $parameters['key'];
		$etudes_key = $parameters['etudes_key'];

		// SECTION #1 //
		if ($section == "all" || $section == "1") {
			$DB->query( 
				"SELECT * ".
				"FROM `evaluations_etudes_succursales` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			while ($DB->next_record()) {
				$etudes_succursales_key = $DB->getField("etudes_succursales_key");
				$parameters['etudes_succursales_key'][$etudes_succursales_key] = $etudes_succursales_key;
				$parameters['etudes_succursales_coordonnees'][$etudes_succursales_key] = $DB->getField("coordonnees");
			}
		}

		// SECTION #2 //
		if ($section == "all" || $section == "2") {
			$DB->query(
				"SELECT * ".
				"FROM `evaluations_notaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$parameters['notaires_employes_key'][$employes_key] = $employes_key;
				$parameters['notaires_nom'][$employes_key] = $DB->getField("nom");
				$parameters['notaires_prenom'][$employes_key] = $DB->getField("prenom");
				$parameters['notaires_courriel'][$employes_key] = $DB->getField("courriel");
				$parameters['notaires_annee_debut_pratique'][$employes_key] = $DB->getField("annee_debut_pratique");
				$parameters['notaires_associe'][$employes_key] = $DB->getField("associe");
				$parameters['notaires_present_evaluation'][$employes_key] = $DB->getField("present_evaluation");
				$parameters['notaires_raison_absence'][$employes_key] = $DB->getField("raison_absence");
			}
	
			$DB->query( 
				"SELECT * ".
				"FROM `evaluations_notaires_titres` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' ".
				"ORDER BY `employes_key` ASC;"
			);
			$employes_key = "";
			$i = 0;
			while ($DB->next_record()) {
				if ($employes_key != $DB->getField("employes_key")) {
					$i = 0;
					$employes_key = $DB->getField("employes_key");
					$parameters['notaires_titres_employes_key'][$employes_key] = $employes_key;
					$parameters['notaires_titres_titres_key'][$employes_key][$i] = $DB->getField("titres_key");
				} else {
					$parameters['notaires_titres_titres_key'][$employes_key][$i] = $DB->getField("titres_key");
				}					
				$i++;
			}
		}
		
		// SECTION #3 //
		if ($section == "all" || $section == "3") {
			$DB->query( 
				"SELECT * ".
				"FROM `evaluations_employes` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$parameters['employes_employes_key'][$employes_key] = $employes_key;
				$parameters['employes_nom'][$employes_key] = $DB->getField("nom");
				$parameters['employes_prenom'][$employes_key] = $DB->getField("prenom");
				$parameters['employes_courriel'][$employes_key] = $DB->getField("courriel");
				$parameters['employes_present_evaluation'][$employes_key] = $DB->getField("present_evaluation");
				$parameters['employes_raison_absence'][$employes_key] = $DB->getField("raison_absence");
			}
			$DB->query( 
				"SELECT * ".
				"FROM `evaluations_employes_fonctions` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' ".
				"ORDER BY `employes_key` ASC;"
			);
			$employes_key = "";
			$i = 0;
			while ($DB->next_record()) {
				if ($employes_key != $DB->getField("employes_key")) {
					$i = 0;
					$employes_key = $DB->getField("employes_key");
					$parameters['employes_fonctions_key'][$employes_key][$i] = $DB->getField("fonctions_key");
				} else {
					$parameters['employes_fonctions_key'][$employes_key][$i] = $DB->getField("fonctions_key");
				}					
				$i++;
			}
		}

		// SECTION #4 //
		if ($section == "all" || $section == "4") {
			$DB->query(
				"SELECT * ".
				"FROM `evaluations_signataires_adhesion` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			$i = 0;
			while ($DB->next_record()) {
				$parameters['signataires_adhesion_employes_key'][$i] = $DB->getField("employes_key");
				$i++;
			}
		}

		// SECTION #5 //
		if ($section == "all" || $section == "5") {
			$DB->query(
				"SELECT * ".
				"FROM `evaluations_produits_visibles` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			while ($DB->next_record()) {
				$produits_key = $DB->getField("produits_key");
				$parameters['produits_visibles_visible'][$produits_key] = "1";
				$parameters['produits_visibles_produits_key'][$produits_key] = $produits_key;
				$parameters['produits_visibles_nom'][$produits_key] = $DB->getField("nom");
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_produits_papeterie_utilisee` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			while ($DB->next_record()) {
				$produits_key = $DB->getField("produits_key");
				$parameters['produits_papeterie_utilisee_nom'][$produits_key] = "1";
				$parameters['produits_papeterie_utilisee_qte_2003'][$produits_key] = $DB->getField("qte_2003");
				$parameters['produits_papeterie_utilisee_qte_2004'][$produits_key] = $DB->getField("qte_2004");
				$parameters['produits_papeterie_utilisee_utilisation'][$produits_key] = $DB->getField("utilisation");
				$parameters['produits_papeterie_utilisee_raisons'][$produits_key] = $DB->getField("raisons");
			}
		}

		// SECTION #6 //

		// SECTION #7 //
		if ($section == "all" || $section == "7") {
			$DB->query( 
				"SELECT * ".
				"FROM `evaluations_notaires_directions_de_travail` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' ".
				"ORDER BY `directions_de_travail_key` ASC;"
			);
			$a = 0;
			$b = 0;
			$c = 0;
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$directions_de_travail_key = $DB->getField("directions_de_travail_key");
				switch ($directions_de_travail_key) {
					case '1':
						$parameters['notaires_directions_de_travail_droit_des_affaires_employes_key'][$a] = $DB->getField("employes_key");
						$a++;					
						break;
					case '2':
						$parameters['notaires_directions_de_travail_droit_des_personnes_employes_key'][$b] = $DB->getField("employes_key");
						$b++;					
						break;
					case '3':
						$parameters['notaires_directions_de_travail_gestion_des_bureaux_employes_key'][$c] = $DB->getField("employes_key");
						$c++;					
						break;
				}
			}
		}

		// SECTION #8 //
		if ($section == "all" || $section == "8") {
			$DB->query(
				"SELECT * ".
				"FROM `evaluations_domaines_daffaires_notaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '1';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$parameters['domaines_daffaires_notaires_droit_des_affaires_employes_key'][$employes_key] = $employes_key;
				$parameters['domaines_daffaires_notaires_droit_des_affaires_temps_consacre_pourcent'][$employes_key] = $DB->getField("temps_consacre_pourcent");
				$parameters['domaines_daffaires_notaires_droit_des_affaires_selectionne'][$employes_key] = "1";
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_domaines_daffaires_collaboratrices` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '1';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$parameters['domaines_daffaires_collaboratrices_droit_des_affaires_employes_key'][$employes_key] = $employes_key;
				$parameters['domaines_daffaires_collaboratrices_droit_des_affaires_temps_consacre_pourcent'][$employes_key] = $DB->getField("temps_consacre_pourcent");
				$parameters['domaines_daffaires_collaboratrices_droit_des_affaires_selectionne'][$employes_key] = "1";
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_formations_notaires_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_greffe_corpo` = '1';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$nom = $DB->getField("nom");
				$parameters['formations_notaires_domaines_daffaires_formation_greffe_corpo_employes_key'][$employes_key] = $employes_key;
				$parameters['formations_notaires_domaines_daffaires_formation_greffe_corpo_nom'][$employes_key] = $nom;
				$parameters['formations_notaires_domaines_daffaires_formation_greffe_corpo_selectionne'][$employes_key] = "1";
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_formations_collaboratrices_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_greffe_corpo` = '1';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$nom = $DB->getField("nom");
				$parameters['formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_employes_key'][$employes_key] = $employes_key;
				$parameters['formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_nom'][$employes_key] = $nom;
				$parameters['formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_selectionne'][$employes_key] = "1";
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_formations_notaires_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_assistant_corpo` = '1';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$nom = $DB->getField("nom");
				$parameters['formations_notaires_domaines_daffaires_formation_assistant_corpo_employes_key'][$employes_key] = $employes_key;
				$parameters['formations_notaires_domaines_daffaires_formation_assistant_corpo_nom'][$employes_key] = $nom;
				$parameters['formations_notaires_domaines_daffaires_formation_assistant_corpo_selectionne'][$employes_key] = "1";
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_formations_collaboratrices_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_assistant_corpo` = '1';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$nom = $DB->getField("nom");
				$parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_employes_key'][$employes_key] = $employes_key;
				$parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_nom'][$employes_key] = $nom;
				$parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_selectionne'][$employes_key] = "1";
			}
		}

		// SECTION #9 //
		if ($section == "all" || $section == "9") {
			$DB->query(
				"SELECT * ".
				"FROM `evaluations_domaines_daffaires_notaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '2';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$parameters['domaines_daffaires_notaires_droit_des_personnes_employes_key'][$employes_key] = $employes_key;
				$parameters['domaines_daffaires_notaires_droit_des_personnes_temps_consacre_pourcent'][$employes_key] = $DB->getField("temps_consacre_pourcent");
				$parameters['domaines_daffaires_notaires_droit_des_personnes_selectionne'][$employes_key] = "1";
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_domaines_daffaires_collaboratrices` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '2';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$parameters['domaines_daffaires_collaboratrices_droit_des_personnes_employes_key'][$employes_key] = $employes_key;
				$parameters['domaines_daffaires_collaboratrices_droit_des_personnes_temps_consacre_pourcent'][$employes_key] = $DB->getField("temps_consacre_pourcent");
				$parameters['domaines_daffaires_collaboratrices_droit_des_personnes_selectionne'][$employes_key] = "1";
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_formations_notaires_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_testament_fiduciaire` = '1';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$nom = $DB->getField("nom");
				$parameters['formations_notaires_domaines_daffaires_formation_testament_fiduciaire_employes_key'][$employes_key] = $employes_key;
				$parameters['formations_notaires_domaines_daffaires_formation_testament_fiduciaire_nom'][$employes_key] = $nom;
				$parameters['formations_notaires_domaines_daffaires_formation_testament_fiduciaire_selectionne'][$employes_key] = "1";
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_formations_collaboratrices_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_testament_fiduciaire` = '1';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$nom = $DB->getField("nom");
				$parameters['formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_employes_key'][$employes_key] = $employes_key;
				$parameters['formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_nom'][$employes_key] = $nom;
				$parameters['formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_selectionne'][$employes_key] = "1";
			}
		}

		// SECTION #10 //
		if ($section == "all" || $section == "10") {
			$DB->query(
				"SELECT * ".
				"FROM `evaluations_domaines_daffaires_notaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '3';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$parameters['domaines_daffaires_notaires_droit_immo_employes_key'][$employes_key] = $employes_key;
				$parameters['domaines_daffaires_notaires_droit_immo_temps_consacre_pourcent'][$employes_key] = $DB->getField("temps_consacre_pourcent");
				$parameters['domaines_daffaires_notaires_droit_immo_selectionne'][$employes_key] = "1";
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_domaines_daffaires_collaboratrices` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `domaines_daffaires_key` = '3';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$parameters['domaines_daffaires_collaboratrices_droit_immo_employes_key'][$employes_key] = $employes_key;
				$parameters['domaines_daffaires_collaboratrices_droit_immo_temps_consacre_pourcent'][$employes_key] = $DB->getField("temps_consacre_pourcent");
				$parameters['domaines_daffaires_collaboratrices_droit_immo_selectionne'][$employes_key] = "1";
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_formations_notaires_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_assistant_immo` = '1';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$nom = $DB->getField("nom");
				$parameters['formations_notaires_domaines_daffaires_formation_assistant_immo_employes_key'][$employes_key] = $employes_key;
				$parameters['formations_notaires_domaines_daffaires_formation_assistant_immo_nom'][$employes_key] = $nom;
				$parameters['formations_notaires_domaines_daffaires_formation_assistant_immo_selectionne'][$employes_key] = "1";
			}

			$DB->query(
				"SELECT * ".
				"FROM `evaluations_formations_collaboratrices_domaines_daffaires` ".
				"WHERE `evaluations_key` = '" . $eval_key . "' AND `formation_assistant_immo` = '1';"
			);
			while ($DB->next_record()) {
				$employes_key = $DB->getField("employes_key");
				$nom = $DB->getField("nom");
				$parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_immo_employes_key'][$employes_key] = $employes_key;
				$parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_immo_nom'][$employes_key] = $nom;
				$parameters['formations_collaboratrices_domaines_daffaires_formation_assistant_immo_selectionne'][$employes_key] = "1";
			}
		}

		// SECTION #11 //
		if ($section == "all" || $section == "11") {
			$DB->query(
				"SELECT * ".
				"FROM `evaluations_produits_articles_promotionnels` ".
				"WHERE `evaluations_key` = '" . $eval_key . "';"
			);
			while ($DB->next_record()) {
				$produits_key = $DB->getField("produits_key");
				$parameters['produits_articles_promotionnels_produits_key'][$produits_key] = $produits_key;
				$parameters['produits_articles_promotionnels_qte'][$produits_key] = $DB->getField("qte");
				$parameters['produits_articles_promotionnels_nom'][$produits_key] = $DB->getField("nom");
			}
		}

		// SECTION #12 //

		// SECTION #14 //

		// SECTION #15 //

		// SECTION #16 //

		// SECTION #17 //

		// SECTION #18 //

		// SECTION #19 //

		// SECTION #20 //

		// SECTION #21 //

		// SECTION #22 //

		return $parameters;
	}

?>
