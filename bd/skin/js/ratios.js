function autoSave() {
	document.getElementById('action').value = 'autoSave';
	document.getElementById('ratiosForm').submit();
}

function startAutoSaveTimer() {
	if (document.getElementById('currentForm').value != '5') {
		$(this).everyTime('30s', 'saveTimer', function() {autoSave();}, 0, false);
	}
}

function stopAutoSaveTimer() {
	$(this).stopTime('saveTimer');
}

function resetAutoSaveTimer() {
	stopAutoSaveTimer();
	startAutoSaveTimer();
}

function submitComplete() {
	document.getElementById('action').value = 'complete';
	document.getElementById('ratiosForm').submit();
}

function submitNotComplete() {
	document.getElementById('action').value = 'notcomplete';
	document.getElementById('ratiosForm').submit();
}

function submitRatiosForm(currentForm, requestForm) {
	document.getElementById('currentForm').value = currentForm;
	document.getElementById('requestForm').value = requestForm;
	document.getElementById('action').value = 'save';
	document.getElementById('ratiosForm').submit();
}

function popupDef(URL) {
	window.open(URL, '', 'toolbar=0, scrollbars=1, location=0, statusbar=0, menubar=0, resizable=1, width=750px, height=500px');
}

function toggleMust(fieldId) {
	var fieldElement = document.getElementById(fieldId);
	if (fieldElement.type == 'select') {
		var fieldValue = getSelectValue(fieldElement);
	} else {
		var fieldValue = fieldElement.value;
	}
	if (trim(fieldValue) == '' || fieldValue === undefined) {
		fieldElement.style.borderColor = '#FF0000';
	} else {
		fieldElement.style.borderColor = '#000000';
	}
}

function toggleMustAll(form) {
	switch (form) {
		case '1':
			toggleMust('nbNotairesAssocies');
			toggleMust('nbNotairesSalaries');
			toggleMust('nbNotairesTotal');
			toggleMust('nbEmployes');
			//toggleMust('nbStagieres');
			//toggleMust('dossierMandat');
			toggleMust('nbDossiers');
			toggleMust('nbMinutesNotaires');
			//toggleMust('minutesExcluent');
			//toggleMust('nbSocietes');
			break;
		case '2':
			toggleMust('facturation');
			toggleMust('chiffreAffaire');
			toggleMust('deboursRefactures');
			toggleMust('beneficesNets');
			toggleMust('depensesTotales');
			toggleMust('salaires');
			toggleMust('salairesColl');
			toggleMust('salairesNotaires');
			toggleMust('depenseLoyer');
			toggleMust('depenseDeboursesDossiers');
			toggleMust('depenseInteretsPretMarge');
			toggleMust('depenseAmortissement');
			toggleMust('depenseTelecom');
			toggleMust('depenseSoutien');
			toggleMust('depensePublicite');
			toggleMust('depenseRepresentation');
			toggleMust('depenseAutres');
			break;
		case '3':
			toggleMust('recevables');
			toggleMust('travauxEnCours');
			toggleMust('totalActifsCourtTerme');
			toggleMust('totalPassifsCourtTerme');
			toggleMust('capitalAssocies');
			toggleMust('nbHeuresFacturablesParAnnee');
			toggleMust('nbHeuresFacturablesCollaborateursParAnnee');
			break;
		case '4':
			//toggleMust('accepteMandats');
			//toggleMust('tauxRemboursementKilo');
			//toggleMust('numerisationPiecesIdentite');
			break;
		case '5':
			//toggleMust('forfaitHonorairesResidentiel');
			//toggleMust('forfaitHonorairesTestaments');
			//toggleMust('forfaitHonorairesCorporatif');
			//toggleMust('forfaitHonorairesHypotheque');
			//toggleMust('forfaitHonorairesIncorporation');
			//toggleMust('tauxHoraireNotaireMoinsDe5ans');
			//toggleMust('tauxHoraireNotaire5a10ans');
			//toggleMust('tauxHoraireNotairePlusDe10ans');
			//toggleMust('tauxHoraireCollaborateur');
			break;
	}
}

function adjustDateField(fieldId) {
	var fieldElement = document.getElementById(fieldId);
	var fieldValue = fieldElement.value;
	fieldElement.value = fieldValue.replace(/\//g, '-');
}

function adjustNumericField(fieldId, allowDecimal, maxDecimal, maxUnit, allowNegative) {
	var fieldElement = document.getElementById(fieldId);
	var fieldValue = fieldElement.value;
	fieldElement.value = formatFrenchNumber(fieldValue, allowDecimal, maxDecimal, maxUnit, allowNegative);
}

function formatFrenchNumber(num, allowDecimal, maxDecimal, maxUnit, allowNegative) {
	if (num.length == 0) {
		return('');
	}

	// On force le type en string.
	str = num.toString();

	// On prend note si le nombre est negatif
	neg = 0;
	if (str.charAt(0) === '-') {
		neg = 1;
	}

	// On strip toutes les chars AUTRES que les chiffres, le point et la virgule. 
	str2 = str;
	str = '';
	for (k = 0; k < str2.length; k++) {
		if (
			((str2.charAt(k) === ',' || str2.charAt(k) === '.') && allowDecimal)
			|| str2.charAt(k) === '0' || str2.charAt(k) === '1'
			|| str2.charAt(k) === '2' || str2.charAt(k) === '3'
			|| str2.charAt(k) === '4' || str2.charAt(k) === '5'
			|| str2.charAt(k) === '6' || str2.charAt(k) === '7'
			|| str2.charAt(k) === '8' || str2.charAt(k) === '9'
		) {
			str = str + str2.charAt(k);
		}
	}
	
	// Si, apres la transformation, la string ne contient plus de chars, alors inutile de continuer plus loin.
	if (str.length == 0) {
		return('');
	}

	// Si on permet les decimales, on supprime toutes les points et virgules que contient la string SAUF le/la dernier(e).
	if (allowDecimal) {
		str2 = str;
		str = '';
		sep = false;
		for (j = str2.length-1; j >= 0; j--) {
			if (str2.charAt(j) === ',' || str2.charAt(j) === '.') {
				if (!sep) {
					str = str2.charAt(j) + str;
				}
				sep = true;
			} else {
				str = str2.charAt(j) + str;
			}
		}
	}
	
	// On change le point pour une virgule
	str2 = str;
	str = '';
	for (k = 0; k < str2.length; k++) {
		if (str2.charAt(k) === '.') {
			str = str + ',';
		} else {
			str = str + str2.charAt(k);
		}
	}

	// Si, apres la transformation, la string ne contient plus de chars, alors inutile de continuer plus loin.
	if (str.length == 0) {
		return('');
	}

	// On ajuste le nombre de décimales authorisées selon maxDecimal
	if (allowDecimal) {
		a = str.split(',');
		x = a[0];
		y = a[1];
		z = '';
		if (y !== undefined) {
			if (y.length < maxDecimal) {
				diff = 1 * maxDecimal - y.length;
				fill = '';
				for (var d = 1; d <= diff; d++) {
					fill = fill + '0';
				}
				z = str + fill;
			} else {
				if (y.length > maxDecimal) {
					z = x + ',' + y.substr(0, maxDecimal);
				}
			}
		
		// S'il n'y avait pas de décimales, on les ajoute selon maxDecimal
		} else {
			fill = '';
			for (var d = 1; d <= maxDecimal; d++) {
				fill = fill + '0';
			}
			z = str + ',' + fill;
		}
		if (z.length > 0) {
			str = z;
		}
	}

	// On ajuste le nombre d'unités authorisées selon maxUnit
	if (maxUnit > 0) {
		a = str.split(',');
		x = a[0];
		y = a[1];
		z = '';
		if (x !== undefined && x.length > maxUnit) {
			z = x.substr(0, maxUnit) + ',' + y;
		}
		if (z.length > 0) {
			str = z;
		}
	}

	// On sépare les milles par une espace
	a = str.split(',');
	if (typeof(a[1]) === 'undefined') {
		a = str.split('.');
	}

	x = a[0];
	y = a[1]; 
	z = '';
	
	if (typeof(x) != 'undefined') {
		for (i = x.length-1; i >= 0; i--) {
			if (x.charAt(i) != ',' && x.charAt(i) != ' ') {
				z += x.charAt(i);
			}
		}

		x = '';

		for (i = z.length-1; i >= 0; i--) {
			if (i%3 == 0) {
	 			x += z.charAt(i) + ' ';
	 		} else {
				x += z.charAt(i);
			}
		}

		if (typeof(y) == 'undefined') { 
			y = '00';
		}

		if (y.length == 1) {
			y = y + '0';
		}

		if (allowDecimal) { 
			x = x.slice(0, (x.length-1)) + ',' + y;
		} else {
			x = x.slice(0, (x.length-1));
		}

		str = x;
	}

	// On trim les zéros et espaces inutiles à gauche
	tocut = 0;
	for (k = 0; k < str.length; k++) {
		if (str.charAt(k+1) != ',' && (str.charAt(k) === '0' || str.charAt(k) === ' ')) {
			tocut++;
		} else {
			break;
		}
	}
	if (tocut > 0) {
		str = str.substr(tocut, 1*str.length-tocut);
	}

	// Si le nombre était negatif et qu'on permet qu'il le soit, on lui remet son signe
	if (neg && allowNegative) {
		str = '-' + str;
	}

 	return str;
}

function sanitizeString(inputString) {
	inputString = inputString.replace(/\s+/g, '');
	inputString = inputString.replace(',', '.');
	return inputString;
}

function string2Float(inputString) {
	newFloat = parseFloat(inputString);
	if (!isNumeric(newFloat) || isNaN(newFloat)) {
		newFloat = 0;
	}
	return newFloat;
}

function isNumeric(strString) {
	var strValidChars = "-0123456789., ";
	var strChar;
	var blnResult = true;
	if (strString.length == 0) {
		return true;
	}
	for (i = 0; i < strString.length && blnResult == true; i++) {
		strChar = strString.charAt(i);
		if (strValidChars.indexOf(strChar) == -1) {
			blnResult = false;
		}
	}
	return blnResult;
}

function afficherHeuresFacturables(inputId, multiplicatif, outputId){
	var inputInField = document.getElementById(inputId);
	var inputInValue = inputInField.value;
	var outputField = document.getElementById(outputId);
	var outputValue = multiplicatif * inputInValue;
	outputField.value= outputValue;
}

function remplirSousTotauxEtTotaux() {
	afficherSousTotal('residentiel');
	afficherTotal('residentiel');
	afficherSousTotal('commercial');
	afficherTotal('commercial');
	afficherSousTotal('personne');
	afficherTotal('personne');
	afficherSousTotal('agricole');
	afficherTotal('agricole');
	afficherSousTotal('divers');
	afficherTotal('divers');
}

function afficherSousTotal(section) {
	switch (section) {
		case 'residentiel':
			sousTotal = 
				string2Float(sanitizeString(document.getElementById('immobilierExistant').value)) + 
				string2Float(sanitizeString(document.getElementById('immobilierNeuf').value)) +
				string2Float(sanitizeString(document.getElementById('immobilierResidentielCondoExistant').value)) +
				string2Float(sanitizeString(document.getElementById('immobilierResidentielCondoNeuf').value)) +
				string2Float(sanitizeString(document.getElementById('constitutionServicesCondo').value));

			sousTotal = sousTotal.toFixed(2);
			document.getElementById('immobilierResidentielSousTotal').value = sousTotal;
			adjustNumericField('immobilierResidentielSousTotal', 1, 2, 0, 1);
			break;

		case 'commercial':
			sousTotal = string2Float(sanitizeString(document.getElementById('commercial').value)) + string2Float(sanitizeString(document.getElementById('immobilierComm').value)) + string2Float(sanitizeString(document.getElementById('corporatif').value)) + string2Float(sanitizeString(document.getElementById('serviceCorporatif').value));
			sousTotal = sousTotal.toFixed(2);
			document.getElementById('commercialCorporatifSousTotal').value = sousTotal;
			adjustNumericField('commercialCorporatifSousTotal', 1, 2, 0, 1);
			break;

		case 'personne':
			sousTotal = string2Float(sanitizeString(document.getElementById('testamentsMandats').value)) + string2Float(sanitizeString(document.getElementById('donationsFiduciaires').value)) + string2Float(sanitizeString(document.getElementById('successions').value)) + string2Float(sanitizeString(document.getElementById('mediationFamiliale').value)) + string2Float(sanitizeString(document.getElementById('proceduresNonContent').value)) + string2Float(sanitizeString(document.getElementById('protectionPatrimoine').value)) + string2Float(sanitizeString(document.getElementById('autresPersonneFamilial').value));
			sousTotal = sousTotal.toFixed(2);
			document.getElementById('personneSuccessionFamilialSousTotal').value = sousTotal;
			adjustNumericField('personneSuccessionFamilialSousTotal', 1, 2, 0, 1);
			break;

		case 'agricole':
			sousTotal = string2Float(sanitizeString(document.getElementById('agricole').value)) + string2Float(sanitizeString(document.getElementById('municipalAdministratif').value));
			sousTotal = sousTotal.toFixed(2);
			document.getElementById('agricoleMunicipalAdministratifSousTotal').value = sousTotal;
			adjustNumericField('agricoleMunicipalAdministratifSousTotal', 1, 2, 0, 1);
			break;

		case 'divers':
			sousTotal = string2Float(sanitizeString(document.getElementById('divers').value));
			sousTotal = sousTotal.toFixed(2);
			document.getElementById('diversSousTotal').value = sousTotal;
			adjustNumericField('diversSousTotal', 1, 2, 0, 1);
			break;
	}
}

function afficherDiff(section) {
	switch (section) {
		case 'residentiel':
			diff = string2Float(sanitizeString(document.getElementById('immobilierResidentielTotal').value)) - string2Float(sanitizeString(document.getElementById('immobilierResidentielSousTotal').value));
			diff = diff.toFixed(2);
			document.getElementById('immobilierResidentielDiff').value = diff;
			adjustNumericField('immobilierResidentielDiff', 1, 2, 0, 1);
			break;

		case 'residentiel':
			diff = string2Float(sanitizeString(document.getElementById('commercialCorporatifTotal').value)) - string2Float(sanitizeString(document.getElementById('commercialCorporatifSousTotal').value));
			diff = diff.toFixed(2);
			document.getElementById('commercialCorporatifDiff').value = diff;
			adjustNumericField('commercialCorporatifDiff', 1, 2, 0, 1);
			break;

		case 'personne':
			diff = string2Float(sanitizeString(document.getElementById('personneSuccessionFamilialTotal').value)) - string2Float(sanitizeString(document.getElementById('personneSuccessionFamilialSousTotal').value));
			diff = diff.toFixed(2);
			document.getElementById('personneSuccessionFamilialDiff').value = diff;
			adjustNumericField('personneSuccessionFamilialDiff', 1, 2, 0, 1);
			break;

		case 'agricole':
			diff = string2Float(sanitizeString(document.getElementById('agricoleMunicipalAdministratifTotal').value)) - string2Float(sanitizeString(document.getElementById('agricoleMunicipalAdministratifSousTotal').value));
			diff = diff.toFixed(2);
			document.getElementById('agricoleMunicipalAdministratifDiff').value = diff;
			adjustNumericField('agricoleMunicipalAdministratifDiff', 1, 2, 0, 1);
			break;

		case 'divers':
			diff = string2Float(sanitizeString(document.getElementById('diversTotal').value)) - string2Float(sanitizeString(document.getElementById('diversSousTotal').value));
			diff = diff.toFixed(2);
			document.getElementById('diversDiff').value = diff;
			adjustNumericField('diversDiff', 1, 2, 0, 1);
			break;
	}
}

function afficherTotal(section) {
	switch (section) {
		case 'residentiel':
			total = string2Float(sanitizeString(document.getElementById('immobilierResidentielSousTotal').value)) + string2Float(sanitizeString(document.getElementById('immobilierResidentielDiff').value));
			total = total.toFixed(2);
			document.getElementById('immobilierResidentielTotal').value = total;
			adjustNumericField('immobilierResidentielTotal', 1, 2, 0, 1);
			break;

		case 'commercial':
			total = string2Float(sanitizeString(document.getElementById('commercialCorporatifSousTotal').value)) + string2Float(sanitizeString(document.getElementById('commercialCorporatifDiff').value));
			total = total.toFixed(2);
			document.getElementById('commercialCorporatifTotal').value = total;
			adjustNumericField('commercialCorporatifTotal', 1, 2, 0, 1);
			break;

		case 'personne':
			total = string2Float(sanitizeString(document.getElementById('personneSuccessionFamilialSousTotal').value)) + string2Float(sanitizeString(document.getElementById('personneSuccessionFamilialDiff').value));
			total = total.toFixed(2);
			document.getElementById('personneSuccessionFamilialTotal').value = total;
			adjustNumericField('personneSuccessionFamilialTotal', 1, 2, 0, 1);
			break;

		case 'agricole':
			total = string2Float(sanitizeString(document.getElementById('agricoleMunicipalAdministratifSousTotal').value)) + string2Float(sanitizeString(document.getElementById('agricoleMunicipalAdministratifDiff').value));
			total = total.toFixed(2);
			document.getElementById('agricoleMunicipalAdministratifTotal').value = total;
			adjustNumericField('agricoleMunicipalAdministratifTotal', 1, 2, 0, 1);
			break;

		case 'divers':
			total = string2Float(sanitizeString(document.getElementById('diversSousTotal').value)) + string2Float(sanitizeString(document.getElementById('diversDiff').value));
			total = total.toFixed(2);
			document.getElementById('diversTotal').value = total;
			adjustNumericField('diversTotal', 1, 2, 0, 1);
			break;
	}
}
