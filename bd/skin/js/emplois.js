function adjustDateField(fieldId) {
	var fieldElement = document.getElementById(fieldId);
	var fieldValue = fieldElement.value;
	fieldElement.value = fieldValue.replace(/\//g, '-');
}

function showChrCount(fieldId, spanId) {
	var txt = document.getElementById(fieldId).value;
	document.getElementById(spanId).innerHTML = txt.length;
}

function initShowChrCount() {
	if (
		document.getElementById('expertises_droit_affaires') !== null
		&& document.getElementById('expertises_droit_personne') !== null	
		&& document.getElementById('expertises_droit_immobilier') !== null
		&& document.getElementById('expertises_sectorielles') !== null
	) {	
		var expertises_droit_affaires_str = document.getElementById('expertises_droit_affaires').value;
		var expertises_droit_personne_str = document.getElementById('expertises_droit_personne').value;
		var expertises_droit_immobilier_str = document.getElementById('expertises_droit_immobilier').value;
		var expertises_sectorielles_str = document.getElementById('expertises_sectorielles').value;
		document.getElementById('expertises_droit_affaires_chrcount').innerHTML = expertises_droit_affaires_str.length;
		document.getElementById('expertises_droit_personne_chrcount').innerHTML = expertises_droit_personne_str.length;
		document.getElementById('expertises_droit_immobilier_chrcount').innerHTML = expertises_droit_immobilier_str.length;
		document.getElementById('expertises_sectorielles_chrcount').innerHTML = expertises_sectorielles_str.length;
	}
}