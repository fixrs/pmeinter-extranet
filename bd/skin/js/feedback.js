function showErrorField(fieldId) {
	var field = document.getElementById(fieldId);
	if (field.style.backgroundColor != '#FA8072') {
		var originalBkgColor = '';
		changeBkgColor(fieldId, '#FA8072');
		var string = "changeBkgColor('" + fieldId + "', '" + originalBkgColor + "');";
		setTimeout(string, 2000);
		window.location = '#' + fieldId;
	}
}

function changeBkgColor(fieldId, color) {
	var field = document.getElementById(fieldId);
	field.style.backgroundColor = color;
}
