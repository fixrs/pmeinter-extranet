function showNotaireFields(action) {
	if (action == 'read') {
		if (document.getElementById('is_notaire').innerHTML == '1') {
			document.getElementById('notaire_only').style.display = 'block';
		}
	} else if (document.getElementById('fonctions_key') !== null) {
		var isNotaire = 0;
		for (var selectIndex = this.document.employe.fonctions_key.length - 1; selectIndex >= 0; selectIndex--) {
			if (this.document.employe.fonctions_key.options[selectIndex].selected == true && (this.document.employe.fonctions_key.options[selectIndex].value == '8' || this.document.employe.fonctions_key.options[selectIndex].value == '15')) {
				isNotaire = 1;
			}
		}
		if (isNotaire == 1) {
			document.getElementById('notaire_only').style.display = 'block';
		} else {
			document.getElementById('notaire_only').style.display = 'none';
		}
	}
}



function showChrCount(fieldId, spanId) {
	var txt = document.getElementById(fieldId).value;
	document.getElementById(spanId).innerHTML = txt.length;
}

function initShowChrCount() {
	if (
		document.getElementById('expertises_droit_affaires') !== null
		&& document.getElementById('expertises_droit_personne') !== null
		&& document.getElementById('expertises_droit_immobilier') !== null
		&& document.getElementById('expertises_sectorielles') !== null
	) {
		var expertises_droit_affaires_str = document.getElementById('expertises_droit_affaires').value;
		var expertises_droit_personne_str = document.getElementById('expertises_droit_personne').value;
		var expertises_droit_immobilier_str = document.getElementById('expertises_droit_immobilier').value;
		var expertises_sectorielles_str = document.getElementById('expertises_sectorielles').value;
		document.getElementById('expertises_droit_affaires_chrcount').innerHTML = expertises_droit_affaires_str.length;
		document.getElementById('expertises_droit_personne_chrcount').innerHTML = expertises_droit_personne_str.length;
		document.getElementById('expertises_droit_immobilier_chrcount').innerHTML = expertises_droit_immobilier_str.length;
		document.getElementById('expertises_sectorielles_chrcount').innerHTML = expertises_sectorielles_str.length;
	}
}

jQuery(document).ready(function($){
	$("#secteur").css({border:"none"});
	$("#secteur_select").change(function() {
		if ($("#secteur_select").val() == "") {
			$("#secteur").css({border:"1px solid #253767"});
		} else {
			$("#secteur").css({border:"none"});
		}
		$("#secteur").val($("#secteur_select").val());
	});

	if ($("#fonctions_key_8").length && $("#fonctions_key_15").length) {
		if ($("#fonctions_key_8").prop("checked") || $("#fonctions_key_15").prop("checked")) {
				document.getElementById('notaire_only').style.display = 'block';
		}

		$("#fonctions_key_8, #fonctions_key_15").change(function() {
			if ($("#fonctions_key_8").prop("checked") || $("#fonctions_key_15").prop("checked")) {
				document.getElementById('notaire_only').style.display = 'block';
			} else {
				document.getElementById('notaire_only').style.display = 'none';
			}
		});

	}


});
