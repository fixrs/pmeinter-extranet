function showEtudesField() {
	document.utilisateur.etudes_key.disabled = true;
	if (getRadioValue(document.utilisateur.type) == 2 || getRadioValue(document.utilisateur.type) == 3 || getRadioValue(document.utilisateur.type) == 4) {
		document.utilisateur.etudes_key.disabled = false;
	}
}

function limitTypes(user_type) {
	if (user_type == '2') {
		document.utilisateur.type[0].disabled = true;
		document.utilisateur.type[1].disabled = true;
		document.utilisateur.type[3].disabled = true;
	}
}