function popupDef(URL) {
	window.open(URL, '', 'toolbar=0, scrollbars=1, location=0, statusbar=0, menubar=0, resizable=1, width=816, height=716');
}

function submitComplete() {
	document.getElementById('action').value = 'complete';
	document.getElementById('ratiosForm').submit();
}

function submitForm() {
	errorMsg = getErrorMsg();
	if (errorMsg == '') {
		document.getElementById('reseauiqForm').submit();
	} else {
		alert(errorMsg);
	}
}

function getErrorMsg() {
	var errorMsg = '';

	var notaireResponsable = document.getElementById('notaire_responsable').value;
	var dateFacture = document.getElementById('date_facture').value;
	var numeroClient = document.getElementById('numero_client').value;
	var factureKey = document.getElementById('factureKey').value;
	var montantHonoraires = document.getElementById('montant_honoraires').value;
	var montantDebourses = document.getElementById('montant_debourses').value;
	var montantRistourneEligible = document.getElementById('montant_ristourne_eligible').value;

	if (
		trim(notaireResponsable) == ''
		|| trim(dateFacture) == ''
		|| trim(numeroClient) == ''
		|| trim(factureKey) == ''
		|| trim(montantHonoraires) == ''
		|| trim(montantDebourses) == ''
		|| trim(montantRistourneEligible) == ''
	) {
		errorMsg += 'Tous les champs doivent \352tre remplis.';

	} else {
		if (!(string2Float(sanitizeString(montantHonoraires)) >= 0)) {
			errorMsg += 'Le montant des honoraires doit \352tre positif ou \351gale \340 z\351ro.\n';
		}
		if (!(string2Float(sanitizeString(montantDebourses)) >= 0)) {
			errorMsg += '\nLe montant des d\351bours\351s doit \352tre positif ou \351gale \340 z\351ro.\n';
		}
		if (!(string2Float(sanitizeString(montantRistourneEligible)) >= 0)) {
			errorMsg += '\nLe montant \351ligible \340 la ristourne doit \352tre positif ou \351gale \340 z\351ro.';
		}
	}

	return errorMsg;
}

function toggleMust(fieldId) {
	var fieldElement = document.getElementById(fieldId);
	if (fieldElement != 'undefined' && fieldElement != '') {
		if (fieldElement.type == 'select') {
			var fieldValue = getSelectValue(fieldElement);
		} else {
			var fieldValue = fieldElement.value;
		}
		if (trim(fieldValue) == '' || fieldValue === undefined) {
			fieldElement.style.borderColor = '#FF0000';
		} else {
			fieldElement.style.borderColor = '#000000';
		}
	}
}

function toggleMustAll() {
	toggleMust('notaire_responsable');
	toggleMust('date_facture');
	toggleMust('numero_client');
	toggleMust('factureKey');
	toggleMust('montant_honoraires');
	toggleMust('montant_debourses');
	toggleMust('montant_ristourne_eligible');
}

function afficherMontantRistourne() {
	var montantRistourneEligible = document.getElementById('montant_ristourne_eligible');
	var montantRistourne = document.getElementById('montant_ristourne');

	montantRistourneValue = string2Float(sanitizeString(montantRistourneEligible.value)) * 0.05;
	montantRistourneValue = montantRistourneValue.toFixed(2);
	montantRistourne.value = montantRistourneValue;
	adjustNumericField('montant_ristourne', 1, 2, 0, 0);
}

function adjustDateField(fieldId) {
	var fieldElement = document.getElementById(fieldId);
	var fieldValue = fieldElement.value;
	fieldElement.value = fieldValue.replace(/\//g, '-');
}

function adjustNumericField(fieldId, allowDecimal, maxDecimal, maxUnit, allowNegative) {
	var fieldElement = document.getElementById(fieldId);
	var fieldValue = fieldElement.value;
	fieldElement.value = formatFrenchNumber(fieldValue, allowDecimal, maxDecimal, maxUnit, allowNegative);
}

function formatFrenchNumber(num, allowDecimal, maxDecimal, maxUnit, allowNegative) {
	if (num.length == 0) {
		return('');
	}

	// On force le type en string.
	str = num.toString();

	// On prend note si le nombre est negatif
	neg = 0;
	if (str.charAt(0) === '-') {
		neg = 1;
	}

	// On strip toutes les chars AUTRES que les chiffres, le point et la virgule. 
	str2 = str;
	str = '';
	for (k = 0; k < str2.length; k++) {
		if (
			((str2.charAt(k) === ',' || str2.charAt(k) === '.') && allowDecimal)
			|| str2.charAt(k) === '0' || str2.charAt(k) === '1'
			|| str2.charAt(k) === '2' || str2.charAt(k) === '3'
			|| str2.charAt(k) === '4' || str2.charAt(k) === '5'
			|| str2.charAt(k) === '6' || str2.charAt(k) === '7'
			|| str2.charAt(k) === '8' || str2.charAt(k) === '9'
		) {
			str = str + str2.charAt(k);
		}
	}
	
	// Si, apres la transformation, la string ne contient plus de chars, alors inutile de continuer plus loin.
	if (str.length == 0) {
		return('');
	}

	// Si on permet les decimales, on supprime toutes les points et virgules que contient la string SAUF le/la dernier(e).
	if (allowDecimal) {
		str2 = str;
		str = '';
		sep = false;
		for (j = str2.length-1; j >= 0; j--) {
			if (str2.charAt(j) === ',' || str2.charAt(j) === '.') {
				if (!sep) {
					str = str2.charAt(j) + str;
				}
				sep = true;
			} else {
				str = str2.charAt(j) + str;
			}
		}
	}
	
	// On change le point pour une virgule
	str2 = str;
	str = '';
	for (k = 0; k < str2.length; k++) {
		if (str2.charAt(k) === '.') {
			str = str + ',';
		} else {
			str = str + str2.charAt(k);
		}
	}

	// Si, apres la transformation, la string ne contient plus de chars, alors inutile de continuer plus loin.
	if (str.length == 0) {
		return('');
	}

	// On ajuste le nombre de décimales authorisées selon maxDecimal
	if (allowDecimal) {
		a = str.split(',');
		x = a[0];
		y = a[1];
		z = '';
		if (y !== undefined) {
			if (y.length < maxDecimal) {
				diff = 1 * maxDecimal - y.length;
				fill = '';
				for (var d = 1; d <= diff; d++) {
					fill = fill + '0';
				}
				z = str + fill;
			} else {
				if (y.length > maxDecimal) {
					z = x + ',' + y.substr(0, maxDecimal);
				}
			}
		
		// S'il n'y avait pas de décimales, on les ajoute selon maxDecimal
		} else {
			fill = '';
			for (var d = 1; d <= maxDecimal; d++) {
				fill = fill + '0';
			}
			z = str + ',' + fill;
		}
		if (z.length > 0) {
			str = z;
		}
	}

	// On ajuste le nombre d'unités authorisées selon maxUnit
	if (maxUnit > 0) {
		a = str.split(',');
		x = a[0];
		y = a[1];
		z = '';
		if (x !== undefined && x.length > maxUnit) {
			z = x.substr(0, maxUnit) + ',' + y;
		}
		if (z.length > 0) {
			str = z;
		}
	}

	// On sépare les milles par une espace
	a = str.split(',');
	if (typeof(a[1]) === 'undefined') {
		a = str.split('.');
	}

	x = a[0];
	y = a[1]; 
	z = '';
	
	if (typeof(x) != 'undefined') {
		for (i = x.length-1; i >= 0; i--) {
			if (x.charAt(i) != ',' && x.charAt(i) != ' ') {
				z += x.charAt(i);
			}
		}

		x = '';

		for (i = z.length-1; i >= 0; i--) {
			if (i%3 == 0) {
	 			x += z.charAt(i) + ' ';
	 		} else {
				x += z.charAt(i);
			}
		}

		if (typeof(y) == 'undefined') { 
			y = '00';
		}

		if (y.length == 1) {
			y = y + '0';
		}

		if (allowDecimal) { 
			x = x.slice(0, (x.length-1)) + ',' + y;
		} else {
			x = x.slice(0, (x.length-1));
		}

		str = x;
	}

	// On trim les zéros et espaces inutiles à gauche
	tocut = 0;
	for (k = 0; k < str.length; k++) {
		if (str.charAt(k+1) != ',' && (str.charAt(k) === '0' || str.charAt(k) === ' ')) {
			tocut++;
		} else {
			break;
		}
	}
	if (tocut > 0) {
		str = str.substr(tocut, 1*str.length-tocut);
	}

	// Si le nombre était negatif et qu'on permet qu'il le soit, on lui remet son signe
	if (neg && allowNegative) {
		str = '-' + str;
	}

 	return str;
}

function sanitizeString(inputString) {
	inputString = inputString.replace(/\s+/g, '');
	inputString = inputString.replace(',', '.');
	return inputString;
}

function string2Float(inputString) {
	newFloat = parseFloat(inputString);
	if (!isNumeric(newFloat) || isNaN(newFloat)) {
		newFloat = 0;
	}
	return newFloat;
}

function isNumeric(strString) {
	var strValidChars = "-0123456789., ";
	var strChar;
	var blnResult = true;
	if (strString.length == 0) {
		return true;
	}
	for (i = 0; i < strString.length && blnResult == true; i++) {
		strChar = strString.charAt(i);
		if (strValidChars.indexOf(strChar) == -1) {
			blnResult = false;
		}
	}
	return blnResult;
}
