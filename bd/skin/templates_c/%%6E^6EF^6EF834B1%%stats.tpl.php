<?php /* Smarty version 2.6.13, created on 2009-04-09 18:06:23
         compiled from ratios2/stats.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php echo $this->_tpl_vars['title']; ?>
</title>
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SKIN_URL']; ?>
/css/stats.css" type="text/css" media="screen"/>
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SKIN_URL']; ?>
/css/print-stats.css" media="print" type="text/css" />
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
	</head>

	<body>

		<div id="legalpaper">

			<?php $_from = $this->_tpl_vars['actions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['actionList'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['actionList']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['nomAction'] => $this->_tpl_vars['action']):
        $this->_foreach['actionList']['iteration']++;
?>

				<?php if ($this->_tpl_vars['nomAction'] == 'statsalariales' && $this->_tpl_vars['type'] != 'stats16'): ?>
					<h1><?php echo $this->_tpl_vars['titre_ss']; ?>
</h1>
				<?php else: ?>
					<h1><?php echo $this->_tpl_vars['titre']; ?>
</h1>
				<?php endif; ?>

				<?php if ($this->_tpl_vars['nomAction'] == 'autres'): ?>
					<?php if ($this->_tpl_vars['admin'] || $this->_tpl_vars['type'] == 'stats3' || $this->_tpl_vars['type'] == 'stats11'): ?>
						<?php $this->assign('ratioz', $this->_tpl_vars['ratios'][$this->_tpl_vars['nomAction']]); ?>
						<?php $this->assign('rowz', $this->_tpl_vars['rows'][$this->_tpl_vars['nomAction']]); ?>
						<?php $this->assign('soustitre', $this->_tpl_vars['soustitres'][$this->_tpl_vars['nomAction']]); ?>
						<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "ratios/".($this->_tpl_vars['type']).".tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					<?php endif; ?>
				<?php else: ?>
					<?php $this->assign('ratioz', $this->_tpl_vars['ratios'][$this->_tpl_vars['nomAction']]); ?>
					<?php $this->assign('rowz', $this->_tpl_vars['rows'][$this->_tpl_vars['nomAction']]); ?>
					<?php $this->assign('soustitre', $this->_tpl_vars['soustitres'][$this->_tpl_vars['nomAction']]); ?>
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "ratios/".($this->_tpl_vars['type']).".tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				<?php endif; ?>

				<?php if ($this->_tpl_vars['type'] == 'stats16'): ?>
					<?php if (($this->_foreach['actionList']['iteration'] == $this->_foreach['actionList']['total'])): ?>
						<?php $this->assign('legClass', 'last'); ?>
					<?php else: ?>
						<?php $this->assign('legClass', 'notlast'); ?>
					<?php endif; ?>
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "ratios/statsLegende.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
		</div>
	</body>
</html>
