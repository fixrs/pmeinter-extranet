<?php /* Smarty version 2.6.13, created on 2013-04-04 21:47:09
         compiled from ratios/etape3.tpl */ ?>
<h2>Renseignements - &eacute;tats financiers</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n">300.&nbsp;</td>
		<td class="l">
			<label for="recevables">Recevables &agrave; la fin de la p&eacute;riode&nbsp;:</label>
		</td>
		<td class="m" width="100px">
			<input type="text" value="<?php echo $this->_tpl_vars['recevables']; ?>
" name="recevables" onchange="javascript: adjustNumericField('recevables', 1, 2, 0, 1); toggleMust('recevables');" id="recevables" class="argent" />
		</td>
		<td class="r" width="40px">$</td>
	</tr>

	<tr>
		<td class="n">301.&nbsp;</td>
		<td class="l">
			<label for="travauxEnCours">Travaux en cours &agrave; la fin de la p&eacute;riode (TEC)&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['travauxEnCours']; ?>
" name="travauxEnCours" onchange="javascript: adjustNumericField('travauxEnCours', 1, 2, 0, 1); toggleMust('travauxEnCours');" id="travauxEnCours" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">302.&nbsp;</td>
		<td class="l">
			<label for="totalActifsCourtTerme">Total des actifs &agrave; court terme - fin de p&eacute;riode&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['totalActifsCourtTerme']; ?>
" name="totalActifsCourtTerme" onchange="javascript: adjustNumericField('totalActifsCourtTerme', 1, 2, 0, 1); toggleMust('totalActifsCourtTerme');" id="totalActifsCourtTerme" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">303.&nbsp;</td>
		<td class="l">
			<label for="totalPassifsCourtTerme">Total des passifs &agrave; court terme - fin de p&eacute;riode&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['totalPassifsCourtTerme']; ?>
" name="totalPassifsCourtTerme" onchange="javascript: adjustNumericField('totalPassifsCourtTerme', 1, 2, 0, 1); toggleMust('totalPassifsCourtTerme');" id="totalPassifsCourtTerme" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">304.&nbsp;</td>
		<td class="l">
			<label for="capitalAssocies">Capital ou avoir des associ&eacute;s&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['capitalAssocies']; ?>
" name="capitalAssocies" onchange="javascript: adjustNumericField('capitalAssocies', 1, 2, 0, 1); toggleMust('capitalAssocies');" id="capitalAssocies" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>
</table>

<br clear="all" />
<br clear="all" />

<h2>Temps productif (facturable)</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td colspan="4">
			<p style="width:508px;">
				Pour les &eacute;tudes qui ne comptabilisent pas le temps productif (facturable) de leurs ressources, veuillez indiquer le nombre de notaires et le nombre de collaborateur(rice)s qui ont pour principales
				t&acirc;ches la production des dossiers clients (exclure les r&eacute;ceptionnistes, les comptables, contr&ocirc;leurs et administrateurs). 
				Le nombre d'heures productives par d&eacute;faut sera attribu&eacute; aux notaires et collaborateur(rice)s que vous aurez inscrits, le tout bas&eacute; sur une semaine normale de travail 
				de 40 heures pour les notaires et de 35 heures pour les collaborateur(rice)s. 
				Ce nombre d'heures annuelles (1008 heures pour les notaires et 1030 heures pour les collaboratrices) tient compte des vacances, des jours f&eacute;ri&eacute;s, des r&eacute;unions, de la formation, d'un certain nombre de jours d'absences pour cong&eacute;s personnels, etc.
			</p>
			<p style="width:508px;">
				Pour les &eacute;tudes qui comptabilisent le temps productif (facturable), sautez
				les deux prochaines questions et inscrivez directement le résultat de la
				compilation de vos données aux questions 32 et 33.
			</p>
		</td>
	</tr>

	<tr>
		<td class="n">305.&nbsp;</td>
		<td class="l" width="390px">
			<label for="nbNotaires">Nombre de notaires&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" id="nbNotaires" name="nbNotaires" value="<?php echo $this->_tpl_vars['nbNotaires']; ?>
" onchange="javascript: afficherHeuresFacturables('nbNotaires', '1008', 'nbHeuresFacturablesParAnnee');" class="numerique" />
		</td>
		<td class="r" width="30px">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">306.&nbsp;</td>
		<td class="l">
			<label for="nbCollaborateurs">Nombre de collaborateur(rice)s&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" id="nbCollaborateurs" name="nbCollaborateurs" value="<?php echo $this->_tpl_vars['nbCollaborateurs']; ?>
" onchange="javascript: afficherHeuresFacturables('nbCollaborateurs', '1030', 'nbHeuresFacturablesCollaborateursParAnnee');" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">307.&nbsp;</td>
		<td class="l">
			<label for="nbHeuresFacturablesParAnnee">Nombre d'heures facturables pour les notaires / ann&eacute;e&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['nbHeuresFacturablesParAnnee']; ?>
" name="nbHeuresFacturablesParAnnee" onchange="javascript: adjustNumericField('nbHeuresFacturablesParAnnee', 1, 2, 0, 0); toggleMust('nbHeuresFacturablesParAnnee');" id="nbHeuresFacturablesParAnnee" class="numerique" />
		</td>
		<td class="r">heures</td>
	</tr>

	<tr>
		<td class="n">308.&nbsp;</td>
		<td class="l">
			<label for="nbHeuresFacturablesCollaborateursParAnnee">Nombre d'heures facturables pour les collaborateurs / ann&eacute;e&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['nbHeuresFacturablesCollaborateursParAnnee']; ?>
" name="nbHeuresFacturablesCollaborateursParAnnee" onchange="javascript: adjustNumericField('nbHeuresFacturablesCollaborateursParAnnee', 1, 2, 0, 0); toggleMust('nbHeuresFacturablesCollaborateursParAnnee');" id="nbHeuresFacturablesCollaborateursParAnnee" class="numerique" />
		</td>
		<td class="r">heures</td>
	</tr>
</table>

<br clear="all" />
<br clear="all" />

<h2>Statistiques salariales</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td colspan="4">
			<p style="width:502px;">
				<span class="highlight">En date d'aujourd'hui</span>, quels sont les taux horaires minimums et maximums pay&eacute;s &agrave;
				vos employ&eacute;s juniors (moins de 5 ans d'exp&eacute;rience) et seniors (5 ans d'exp&eacute;rience et plus)&nbsp;:
			</p>
		</td>
	</tr>

	<tr>
		<td class="n">&nbsp;</td>
		<td class="l">
			&nbsp;
		</td>
		<td class="m" width="240px">
			Minimum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maximum&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		</td>
		<td class="r" width="30px">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">309.&nbsp;</td>
		<td class="l">
			<label for="salaireSecretaireJuridiqueJunior">Secr&eacute;taire juridique junior&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaireSecretaireJuridiqueJuniorMin']; ?>
" name="salaireSecretaireJuridiqueJuniorMin" onchange="javascript: adjustNumericField('salaireSecretaireJuridiqueJuniorMin', 1, 2, 3, 0);" id="salaireSecretaireJuridiqueJuniorMin" class="numerique" />
			<input type="text" value="<?php echo $this->_tpl_vars['salaireSecretaireJuridiqueJunior']; ?>
" name="salaireSecretaireJuridiqueJunior" onchange="javascript: adjustNumericField('salaireSecretaireJuridiqueJunior', 1, 2, 3, 0);" id="salaireSecretaireJuridiqueJunior" class="numerique" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">310.&nbsp;</td>
		<td class="l">
			<label for="salaireSecretaireJuridiqueSenior">Secr&eacute;taire juridique senior&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaireSecretaireJuridiqueSeniorMin']; ?>
" name="salaireSecretaireJuridiqueSeniorMin" onchange="javascript: adjustNumericField('salaireSecretaireJuridiqueSeniorMin', 1, 2, 3, 0);" id="salaireSecretaireJuridiqueSeniorMin" class="numerique" />
			<input type="text" value="<?php echo $this->_tpl_vars['salaireSecretaireJuridiqueSenior']; ?>
" name="salaireSecretaireJuridiqueSenior" onchange="javascript: adjustNumericField('salaireSecretaireJuridiqueSenior', 1, 2, 3, 0);" id="salaireSecretaireJuridiqueSenior" class="numerique" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">311.&nbsp;</td>
		<td class="l">
			<label for="salaireTechnicienJunior">Technicien(ne) juridique junior&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaireTechnicienJuniorMin']; ?>
" name="salaireTechnicienJuniorMin" onchange="javascript: adjustNumericField('salaireTechnicienJuniorMin', 1, 2, 3, 0);" id="salaireTechnicienJuniorMin" class="numerique" />
			<input type="text" value="<?php echo $this->_tpl_vars['salaireTechnicienJunior']; ?>
" name="salaireTechnicienJunior" onchange="javascript: adjustNumericField('salaireTechnicienJunior', 1, 2, 3, 0);" id="salaireTechnicienJunior" class="numerique" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">312.&nbsp;</td>
		<td class="l">
			<label for="salaireTechnicienSenior">Technicien(ne) juridique senior&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaireTechnicienSeniorMin']; ?>
" name="salaireTechnicienSeniorMin" onchange="javascript: adjustNumericField('salaireTechnicienSeniorMin', 1, 2, 3, 0);" id="salaireTechnicienSeniorMin" class="numerique" />
			<input type="text" value="<?php echo $this->_tpl_vars['salaireTechnicienSenior']; ?>
" name="salaireTechnicienSenior" onchange="javascript: adjustNumericField('salaireTechnicienSenior', 1, 2, 3, 0);" id="salaireTechnicienSenior" class="numerique" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">313.&nbsp;</td>
		<td class="l">
			<label for="salaireReceptionniste">R&eacute;ceptionniste&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaireReceptionnisteMin']; ?>
" name="salaireReceptionnisteMin" onchange="javascript: adjustNumericField('salaireReceptionnisteMin', 1, 2, 3, 0);" id="salaireReceptionnisteMin" class="numerique" />
			<input type="text" value="<?php echo $this->_tpl_vars['salaireReceptionniste']; ?>
" name="salaireReceptionniste" onchange="javascript: adjustNumericField('salaireReceptionniste', 1, 2, 3, 0);" id="salaireReceptionniste" class="numerique" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">314.&nbsp;</td>
		<td class="l">
			<label for="salaireAdjointeAdministrative">Adjointe administrative&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaireAdjointeAdministrativeMin']; ?>
" name="salaireAdjointeAdministrativeMin" onchange="javascript: adjustNumericField('salaireAdjointeAdministrativeMin', 1, 2, 3, 0);" id="salaireAdjointeAdministrativeMin" class="numerique" />
			<input type="text" value="<?php echo $this->_tpl_vars['salaireAdjointeAdministrative']; ?>
" name="salaireAdjointeAdministrative" onchange="javascript: adjustNumericField('salaireAdjointeAdministrative', 1, 2, 3, 0);" id="salaireAdjointeAdministrative" class="numerique" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">315.&nbsp;</td>
		<td class="l">
			<label for="salaireNotaireJunior">Notaire salari&eacute;(e) junior&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaireNotaireJuniorMin']; ?>
" name="salaireNotaireJuniorMin" onchange="javascript: adjustNumericField('salaireNotaireJuniorMin', 1, 2, 3, 0);" id="salaireNotaireJuniorMin" class="numerique" />
			<input type="text" value="<?php echo $this->_tpl_vars['salaireNotaireJunior']; ?>
" name="salaireNotaireJunior" onchange="javascript: adjustNumericField('salaireNotaireJunior', 1, 2, 3, 0);" id="salaireNotaireJunior" class="numerique" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">316.&nbsp;</td>
		<td class="l">
			<label for="salaireNotaireSenior">Notaire salari&eacute;(e) senior&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaireNotaireSeniorMin']; ?>
" name="salaireNotaireSeniorMin" onchange="javascript: adjustNumericField('salaireNotaireSeniorMin', 1, 2, 3, 0);" id="salaireNotaireSeniorMin" class="numerique" />
			<input type="text" value="<?php echo $this->_tpl_vars['salaireNotaireSenior']; ?>
" name="salaireNotaireSenior" onchange="javascript: adjustNumericField('salaireNotaireSenior', 1, 2, 3, 0);" id="salaireNotaireSenior" class="numerique" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">317.&nbsp;</td>
		<td class="l">
			<label for="salaireStagiaireNotariat">Stagiaire en notariat&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaireStagiaireNotariatMin']; ?>
" name="salaireStagiaireNotariatMin" onchange="javascript: adjustNumericField('salaireStagiaireNotariatMin', 1, 2, 3, 0);" id="salaireStagiaireNotariatMin" class="numerique" />
			<input type="text" value="<?php echo $this->_tpl_vars['salaireStagiaireNotariat']; ?>
" name="salaireStagiaireNotariat" onchange="javascript: adjustNumericField('salaireStagiaireNotariat', 1, 2, 3, 0);" id="salaireStagiaireNotariat" class="numerique" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">318.&nbsp;</td>
		<td class="l">
			<label for="salaireStagiaireTechniques">Stagiaire en techniques juridiques&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaireStagiaireTechniquesMin']; ?>
" name="salaireStagiaireTechniquesMin" onchange="javascript: adjustNumericField('salaireStagiaireTechniquesMin', 1, 2, 3, 0);" id="salaireStagiaireTechniquesMin" class="numerique" />
			<input type="text" value="<?php echo $this->_tpl_vars['salaireStagiaireTechniques']; ?>
" name="salaireStagiaireTechniques" onchange="javascript: adjustNumericField('salaireStagiaireTechniques', 1, 2, 3, 0);" id="salaireStagiaireTechniques" class="numerique" />
		</td>
		<td class="r">$/heure</td>
	</tr>
</table>