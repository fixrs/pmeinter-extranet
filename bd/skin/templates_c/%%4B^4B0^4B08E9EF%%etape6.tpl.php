<?php /* Smarty version 2.6.13, created on 2011-05-11 12:50:39
         compiled from ratios/etape6.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'ratios/etape6.tpl', 3, false),)), $this); ?>
<?php if ($this->_tpl_vars['open'] || $this->_tpl_vars['admin']): ?>
	<?php if (! $this->_tpl_vars['complete']): ?>
		<?php if (count($this->_tpl_vars['errorArray']) == 0): ?>
			<?php if (count($this->_tpl_vars['warningArray']) > 0): ?>
				Le formulaire peut tout de m&ecirc;me &ecirc;tre signal&eacute; comme termin&eacute; malgr&eacute; les avertissements.<br />
				Nous vous demandons simplement de v&eacute;rifier et d'&ecirc;tre certains des chiffres entr&eacute;s.<br />
			<?php else: ?>
				Le formulaire est pr&ecirc;t &agrave; &ecirc;tre signal&eacute; comme termin&eacute;.<br />
			<?php endif; ?>
		<?php else: ?>
			&Agrave; cause d'&eacute;l&eacute;ments de validation non-conformes, vous ne pouvez signaler ce formulaire comme termin&eacute;. Veuillez revoir les &eacute;tapes 1 &agrave; 5 afin de corriger ces erreurs, puis revenez &agrave; l'&eacute;tape 6 afin de signaler le formulaire comme termin&eacute;.
		<?php endif; ?>

		<br clear="all" />

		<?php if (count($this->_tpl_vars['errorArray']) == 0): ?>
			<input type="button" class="submit" value="Signaler le formulaire comme termin&eacute;" onclick="javascript: submitComplete();"><br />
		<?php endif; ?>

	<?php else: ?>

		<br clear="all" />

		<input type="button" class="submit" value="Signaler le formulaire comme non termin&eacute;" onclick="javascript: submitNotComplete();"><br />

	<?php endif; ?>

<?php endif; ?>

