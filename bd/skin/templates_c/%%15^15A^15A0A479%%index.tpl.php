<?php /* Smarty version 2.6.13, created on 2014-04-17 07:56:19
         compiled from ratios/index.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'ratios/index.tpl', 5, false),array('function', 'math', 'ratios/index.tpl', 6, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo $this->_tpl_vars['errors']; ?>


<?php $this->assign('annee_c0', ((is_array($_tmp=time())) ? $this->_run_mod_handler('date_format', true, $_tmp, '%Y') : smarty_modifier_date_format($_tmp, '%Y')));  echo smarty_function_math(array('equation' => "x-y",'x' => $this->_tpl_vars['annee_c0'],'y' => '3','assign' => 'annee_p3'), $this);?>

<?php echo smarty_function_math(array('equation' => "x-y",'x' => $this->_tpl_vars['annee_c0'],'y' => '2','assign' => 'annee_p2'), $this);?>

<?php echo smarty_function_math(array('equation' => "x-y",'x' => $this->_tpl_vars['annee_c0'],'y' => '1','assign' => 'annee_p1'), $this);?>

<?php echo smarty_function_math(array('equation' => "x+y",'x' => $this->_tpl_vars['annee_c0'],'y' => '1','assign' => 'annee_s1'), $this);?>

<?php echo smarty_function_math(array('equation' => "x+y",'x' => $this->_tpl_vars['annee_c0'],'y' => '2','assign' => 'annee_s2'), $this);?>


<?php if ($this->_tpl_vars['nologin'] != 1): ?>

	<?php echo '
	<script language="JavaScript">

	function adjustDateField(fieldId) {
		var fieldElement = document.getElementById(fieldId);
		var fieldValue = fieldElement.value;
		fieldElement.value = fieldValue.replace(/\\//g, \'-\');
	}

	function submitRatios() {
		var errorMsg = \'\';
		var action = \'start\';
		var etudeKey = document.getElementById(\'etudeField\').value;
		var annee = document.getElementById(\'anneeField\').value;

		if (etudeKey == \'\' || etudeKey == \'0\' || etudeKey == \'undefined\') {
			errorMsg += \'Vous devez choisir une \\351tude.\\n\';
		}
		if (annee == \'\' || annee == \'0\' || annee == \'undefined\') {
			errorMsg += \'Vous devez choisir une ann\\351e.\\n\';
		}

		if (errorMsg != \'\') {
			alert(errorMsg);
			return false;
		}

		var baseurl = '; ?>
'<?php echo $this->_tpl_vars['BASEURL']; ?>
';<?php echo '
		var url = baseurl + \'ratios/ratios.php?action=\' + action + \'&etudeKey=\' + etudeKey + \'&annee=\' + annee;

		window.location = url;
	}

	var tableau = \'\';
	function submitStats() {
		var errorMsg = \'\';
		var action = \'custom\';
		var type = document.getElementById(\'type\').value;
		var etudeKey = document.getElementById(\'etudeKey2\').value;
		var annee = document.getElementById(\'annee\').value;
		var size = document.getElementById(\'size\').value;
		tableau = \'\';

		$(".tableauStats").each(function() {
			if ($(this).attr("checked") == true) {	
				tableau = tableau + $(this).val() + "-";
			}
		});

		if (type == \'\' || type == \'0\' || type == \'undefined\') {
			errorMsg += \'Vous devez sp\\351cifier un type de cumulatif\\n\';
		}
		if ((type == \'stats3\' || type == \'stats11\') && (etudeKey == \'\' || etudeKey == \'0\' || etudeKey == \'undefined\')) {
			errorMsg += \'Vous devez choisir une \\351tude.\\n\';
		}
		if (annee == \'\' || annee == \'0\' || annee == \'undefined\') {
			errorMsg += \'Vous devez choisir une ann\\351e.\\n\';
		}
		if (tableau == \'\'){
			errorMsg += \'Vous devez choisir un moins un tableau \\340 afficher.\\n\';
		}

		if (errorMsg != \'\') {
			alert(errorMsg);
			return false;
		}

		var baseurl = '; ?>
'<?php echo $this->_tpl_vars['BASEURL']; ?>
';<?php echo '
		var url = baseurl + \'ratios/stats.php?action=\' + action + \'&type=\' + type + \'&etudeKey=\' + etudeKey + \'&annee=\' + annee + \'&size=\' + size+ \'&tableau=\' + tableau;

		window.open(url, \'statistiques\');
	}

	function submitCumulAcces() {
		var baseurl = '; ?>
'<?php echo $this->_tpl_vars['BASEURL']; ?>
';<?php echo '
		var url = baseurl + \'ratios/index.php?action=updateCumulAcces&cumul[';  echo $this->_tpl_vars['annee_p2'];  echo ']=\' + getSelectValue(document.getElementById(\'cumul_';  echo $this->_tpl_vars['annee_p2'];  echo '\')) + \'&cumul[';  echo $this->_tpl_vars['annee_p1'];  echo ']=\' + getSelectValue(document.getElementById(\'cumul_';  echo $this->_tpl_vars['annee_p1'];  echo '\')) + \'&cumul[';  echo $this->_tpl_vars['annee_c0'];  echo ']=\' + getSelectValue(document.getElementById(\'cumul_';  echo $this->_tpl_vars['annee_c0'];  echo '\')) + \'&cumul[';  echo $this->_tpl_vars['annee_s1'];  echo ']=\' + getSelectValue(document.getElementById(\'cumul_';  echo $this->_tpl_vars['annee_s1'];  echo '\'));

		window.location = url;		
	}

	function submitAdminControls(actionValue) {
		var action = document.getElementById(\'action\');
		action.value = actionValue;
		document.getElementById(\'ratioAdminForm\').submit();
	}

	function chkType() {
		var typeElement = document.getElementById(\'type\');
		var sizeElement = document.getElementById(\'size\');
		var etudeElement = document.getElementById(\'etudeKey2\');
		var autresElement = document.getElementById(\'autres\');

		var adminBool = ';  echo $this->_tpl_vars['admin'];  echo ';

		if (getSelectValue(typeElement) == \'stats16\') {
			sizeElement.disabled = false;
		} else {
			sizeElement.disabled = true;
		}

		if (getSelectValue(typeElement) == \'stats3\' || getSelectValue(typeElement) == \'stats11\') {
			etudeElement.disabled = false;
		} else {
			etudeElement.disabled = true;
		}

		if (adminBool) {
			autresElement.disabled = false;
		} else {
			if (getSelectValue(typeElement) == \'stats3\' || getSelectValue(typeElement) == \'stats11\') {
				autresElement.disabled = false;
			} else {
				autresElement.disabled = true;
			}
		}
	}

	function chkAnnee() {
		var adminBool = ';  echo $this->_tpl_vars['admin'];  echo ';

		if (!adminBool) {
			var typeElement = document.getElementById(\'type\');
			var anneeElement = document.getElementById(\'annee\');
			
			var accesCumulAnneesList = new Array(';  $_from = $this->_tpl_vars['accesCumulAnneesList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['_accesCumulAnneesList'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['_accesCumulAnneesList']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['annee']):
        $this->_foreach['_accesCumulAnneesList']['iteration']++;
?>'<?php echo $this->_tpl_vars['annee']; ?>
'<?php if (! ($this->_foreach['_accesCumulAnneesList']['iteration'] == $this->_foreach['_accesCumulAnneesList']['total'])): ?>,<?php endif;  endforeach; endif; unset($_from);  echo ');
			var accesStats16AnneesList = new Array(';  $_from = $this->_tpl_vars['accesStats16AnneesList']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['_accesStats16AnneesList'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['_accesStats16AnneesList']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['annee']):
        $this->_foreach['_accesStats16AnneesList']['iteration']++;
?>'<?php echo $this->_tpl_vars['annee']; ?>
'<?php if (! ($this->_foreach['_accesStats16AnneesList']['iteration'] == $this->_foreach['_accesStats16AnneesList']['total'])): ?>,<?php endif;  endforeach; endif; unset($_from);  echo ');

			bool = 0;
			for (var y in accesCumulAnneesList) {
				if (getSelectValue(anneeElement) == accesCumulAnneesList[y]) {
					bool = 1;
				}
			}

			if (bool) {
				bool = 0;
				for (var y in accesStats16AnneesList) {
					if (getSelectValue(anneeElement) == accesStats16AnneesList[y]) {
						bool = 1;
					}
				}
			}

			var optionExistsBool = 0;
			var o;
			for (o = typeElement.length - 1; o >= 0; o--) {
				if (typeElement.options[o].value == \'stats16\') {
					optionExistsBool = 1;
				}
			}

			if (!bool) {
				if (optionExistsBool) {
					var t;
					for (t = typeElement.length - 1; t >= 0; t--) {
						if (typeElement.options[t].value == \'stats16\') {
							typeElement.remove(t);
						}
					}
				}

			} else {
				if (!optionExistsBool) {
					var newOptionElement = document.createElement(\'option\');
					newOptionElement.text = \'Cumulatif de 1 an pour l\\\'ensemble des \\351tudes\';
					newOptionElement.value = \'stats16\';
					try {
						typeElement.add(newOptionElement, typeElement.options[1]); // standards compliant; doesn\'t work in IE
					} catch(ex) {
						typeElement.add(newOptionElement, 1); // IE only
					}
				}
			}
		}
	}

	</script>
	'; ?>


	<h1>Saisie de donn&eacute;es financi&egrave;res</h1>
	
	<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
	<select name="etudeKey" id="etudeField">
	<?php if ($this->_tpl_vars['admin']): ?>
		<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
		<?php $_from = $this->_tpl_vars['etudesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
			<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
		<?php endforeach; endif; unset($_from); ?>
	<?php else: ?>
				<?php $_from = $this->_tpl_vars['etudesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
			<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
		<?php endforeach; endif; unset($_from); ?>
	<?php endif; ?>
	</select>
	
	<br clear="all" />
	<br clear="all" />
	
	<label>Donn&eacute;es pour la p&eacute;riode financi&egrave;re se terminant en&nbsp;:</label>

	<?php if ($this->_tpl_vars['admin']): ?>
		<select name="anneeField" id="anneeField">
			<option value="2007">2007</option>
			<option value="2008">2008</option>
			<option value="2009">2009</option>
			<option value="2010">2010</option>
			<option value="2011">2011</option>
			<option value="2012">2012</option>
			<option value="2013">2013</option>
		</select>
	<?php else: ?>
		<select name="anneeField" id="anneeField">
			<option value="<?php echo $this->_tpl_vars['annee_p3']; ?>
"><?php echo $this->_tpl_vars['annee_p3']; ?>
</option>
			<option value="<?php echo $this->_tpl_vars['annee_p2']; ?>
"><?php echo $this->_tpl_vars['annee_p2']; ?>
</option>
			<option value="<?php echo $this->_tpl_vars['annee_p1']; ?>
" selected="selected"><?php echo $this->_tpl_vars['annee_p1']; ?>
</option>
			
			<?php if ($_SESSION['user_etudes_key'] == 7): ?>
			<option value="<?php echo $this->_tpl_vars['annee_c0']; ?>
"><?php echo $this->_tpl_vars['annee_c0']; ?>
</option>
			<?php endif; ?>
								</select>
	<?php endif; ?>
	
	<br clear="all"/>
	<br clear="all"/>
	
	<input type="button" class="submit" value="D&eacute;buter la saisie de donn&eacute;es" onclick="javascript: submitRatios();" />
	
	
	<br clear="all" />
	<br clear="all" />

	<a href="<?php echo $this->_tpl_vars['BASEURL']; ?>
docs/ratios/ratios_formulaire2014.pdf" title="Formulaire imprimable"><img src="<?php echo $this->_tpl_vars['SKINURL']; ?>
/img/printer.jpg" alt="formulaire imprimable" class="print" />Cliquez ici pour obtenir une version imprimable du formulaire vierge.</a>
	
	<br clear="all" />
	<br clear="all" />
	<br clear="all" />
	<br clear="all" />

	<h1>Consultation de ratios</h1>

	<label>Type de cumulatif&nbsp;:</label>

	<select name="type" id="type" onchange="javascript: chkType();">
		<option value="0">- Choisir le type de cumulatif -</option>
		<?php if ($this->_tpl_vars['admin']): ?>
			<option value="stats16">Cumulatif de 1 an pour l'ensemble des &eacute;tudes</option>
		<?php endif; ?>
		<option value="stats11">Cumulatif de 1 an pour une &eacute;tude sp&eacute;cifique</option>
		<option value="stats3">Cumulatif de 3 ans pour une &eacute;tude sp&eacute;cifique</option>
	</select>

	<?php if (! $this->_tpl_vars['admin']): ?>
		<br clear="all" />
		<br clear="all" />
		<span class="note">* Vous devez avoir compl&eacute;t&eacute; le ratio de votre &eacute;tude <strong>pour l'ann&eacute;e s&eacute;lectionn&eacute;e</strong> afin d'avoir acc&egrave;s au comparatif de l'ensemble des &eacute;tudes.</span>
	<?php endif; ?>

	<br clear="all" />
	<br clear="all" />

	<?php if ($this->_tpl_vars['admin']): ?>
		<label for="etudeKey">Veuillez choisir l'&eacute;tude&nbsp;:</label>
		<select name="etudeKey" id="etudeKey2" disabled="disabled">
			<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
			<?php $_from = $this->_tpl_vars['etudesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
				<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
		</select>
	<?php else: ?>
		<label for="etudeKey">Veuillez choisir l'&eacute;tude&nbsp;:</label>
		<select name="etudeKey" id="etudeKey2" disabled="disabled">
			<?php $_from = $this->_tpl_vars['etudesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
				<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
		</select>
	<?php endif; ?>

	<?php if ($this->_tpl_vars['admin']): ?>
		<br clear="all" />
		<br clear="all" />
		<label for="size">Nombre de notaires&nbsp;:</label>
		<select name="size" id="size" disabled="disabled">
			<option selected="selected" value="0">- Choisir le nombre de notaires -</option>
			<option value="1-4">De 3 &agrave; 4 notaires</option>
			<option value="5-8">De 5 &agrave; 8 notaires</option>
			<option value="9-999">De 9 notaires et plus</option>
		</select>
	<?php else: ?>
		<br clear="all" />
		<br clear="all" />
		<label for="size">Nombre de notaires&nbsp;:</label>
		<select name="size" id="size" disabled="disabled">
			<option selected="selected" value="0">- Choisir le nombre de notaires -</option>
			<option value="1-4">De 3 &agrave; 4 notaires</option>
			<option value="5-8">De 5 &agrave; 8 notaires</option>
			<option value="9-999">De 9 notaires et plus</option>
		</select>
	<?php endif; ?>

	<br clear="all" />
	<br clear="all" />

	
	<label for="annee">Donn&eacute;es pour la p&eacute;riode financi&egrave;re se terminant en&nbsp;:</label>

	<?php if ($this->_tpl_vars['admin']): ?>
		<select name="annee" id="annee" onchange="javascript: chkAnnee();">
			<option value="2007">2007</option>
			<option value="2008">2008</option>
			<option value="2009">2009</option>
			<option value="2010">2010</option>
			<option value="2011">2011</option>
			<option value="2012">2012</option>
			<option value="2013">2013</option>
		</select>
	<?php else: ?>
		<select name="annee" id="annee" onchange="javascript: chkAnnee();">
			<option value="<?php echo $this->_tpl_vars['annee_p2']; ?>
"><?php echo $this->_tpl_vars['annee_p2']; ?>
</option>
			<option value="<?php echo $this->_tpl_vars['annee_p1']; ?>
" selected="selected"><?php echo $this->_tpl_vars['annee_p1']; ?>
</option>
			<option value="<?php echo $this->_tpl_vars['annee_c0']; ?>
"><?php echo $this->_tpl_vars['annee_c0']; ?>
</option>
			<option value="<?php echo $this->_tpl_vars['annee_s1']; ?>
"><?php echo $this->_tpl_vars['annee_s1']; ?>
</option>
		</select>
	<?php endif; ?>

	<br clear="all" />
	<br clear="all" />

	<label>Donn&eacute;es &agrave; afficher&nbsp;:</label>
	<input type="checkbox" id="donneesGenerales" value="donneesGenerales" name="tableau[]" checked="checked" class="tableauStats checkbox"/> <label for="donneesGenerales" style="display:inline;">Donn&eacute;es g&eacute;n&eacute;rales</label><br />
	<input type="checkbox" id="repartitionHonoraires" value="repartitionHonoraires" name="tableau[]" checked="checked" class="tableauStats checkbox"/> <label for="repartitionHonoraires" style="display:inline;">R&eacute;partition des honoraires</label><br />
	<input type="checkbox" id="rentabilite" value="rentabilite" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="rentabilite" style="display:inline;">Ratios de rentabilit&eacute; et de liquidit&eacute;</label><br />
	<input type="checkbox" id="recevableSalaires" value="recevableSalaires" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="recevableSalaires" style="display:inline;">Ratios des recevables et des salaires</label><br />
	<input type="checkbox" id="tarification" value="tarification" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="tarification" style="display:inline;">Ratios de tarification</label><br />
	<input type="checkbox" id="depenses" value="depenses" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="depenses" style="display:inline;">Ratios des d&eacute;penses</label><br />
	<input type="checkbox" id="production" value="production" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="production" style="display:inline;">Ratios de production</label><br />
	<input type="checkbox" id="statSalariales" value="statSalariales" name="tableau[]" checked="checked" class="tableauStats checkbox"/> <label for="statSalariales" style="display:inline;">Statistiques salariales</label><br />
	<input type="checkbox" id="autres" value="autres" disabled="disabled" name="tableau[]" class="tableauStats checkbox"/> <label for="autres" style="display:inline;">Rapport de la saisie de donn&eacute;es de votre &eacute;tude</label><br />

	<br clear="all" />

	<input type="button" class="submit" value="Consulter les donn&eacute;es" onclick="javascript: submitStats();" />
	

	<?php if ($this->_tpl_vars['admin']): ?>
		<form action="index.php" method="post" id="ratioAdminForm" name="ratioAdminForm">
			<input type="hidden" name="action" id="action" value="" />

		<br clear="all" />
		<br clear="all" />

		<hr />

		<br clear="all" />
		<br clear="all" />

		<h1>Options aux supers administrateurs</h1>

		<h2>Acc&egrave;s au formulaire</h2>

		<table cellpadding="0" cellspacing="0" border="0" class="noborder">
			<tr>
				<td width="75px;"><strong>Ann&eacute;e</strong></td>
				<td width="75px;"><strong>Ouvert&nbsp;?</strong></td>
				<td width="150px;"><strong>Date d&eacute;but (AAAA-MM-JJ)</strong></td>
				<td><strong>Date fin (AAAA-MM-JJ)</strong></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_p3']; ?>
</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_p3']; ?>
" id="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_p3']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p3']]['acces_formulaire']['ouvert']): ?>checked="checked"<?php endif; ?>/></td>
				<td><input type="text" name="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_p3']; ?>
" id="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_p3']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_p3']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p3']]['acces_formulaire']['date_debut']; ?>
" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_p3']; ?>
" id="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_p3']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_p3']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p3']]['acces_formulaire']['date_fin']; ?>
" /></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_p2']; ?>
</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_p2']; ?>
" id="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_p2']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p2']]['acces_formulaire']['ouvert']): ?>checked="checked"<?php endif; ?>/></td>
				<td><input type="text" name="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_p2']; ?>
" id="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_p2']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_p2']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p2']]['acces_formulaire']['date_debut']; ?>
" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_p2']; ?>
" id="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_p2']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_p2']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p2']]['acces_formulaire']['date_fin']; ?>
" /></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_p1']; ?>
</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_p1']; ?>
" id="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_p1']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p1']]['acces_formulaire']['ouvert']): ?>checked="checked"<?php endif; ?>/></td>
				<td><input type="text" name="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_p1']; ?>
" id="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_p1']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_p1']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p1']]['acces_formulaire']['date_debut']; ?>
" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_p1']; ?>
" id="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_p1']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_p1']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p1']]['acces_formulaire']['date_fin']; ?>
" /></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_c0']; ?>
</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_c0']; ?>
" id="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_c0']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_c0']]['acces_formulaire']['ouvert']): ?>checked="checked"<?php endif; ?>/></td>
				<td><input type="text" name="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_c0']; ?>
" id="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_c0']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_c0']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_c0']]['acces_formulaire']['date_debut']; ?>
" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_c0']; ?>
" id="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_c0']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_c0']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_c0']]['acces_formulaire']['date_fin']; ?>
" /></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_s1']; ?>
</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_s1']; ?>
" id="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_s1']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_s1']]['acces_formulaire']['ouvert']): ?>checked="checked"<?php endif; ?>/></td>
				<td><input type="text" name="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_s1']; ?>
" id="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_s1']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_s1']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_s1']]['acces_formulaire']['date_debut']; ?>
" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_s1']; ?>
" id="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_s1']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_s1']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_s1']]['acces_formulaire']['date_fin']; ?>
" /></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_s2']; ?>
</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_s2']; ?>
" id="acces_formulaire_ouvert_<?php echo $this->_tpl_vars['annee_s2']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_s2']]['acces_formulaire']['ouvert']): ?>checked="checked"<?php endif; ?>/></td>
				<td><input type="text" name="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_s2']; ?>
" id="acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_s2']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_debut_<?php echo $this->_tpl_vars['annee_s2']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_s2']]['acces_formulaire']['date_debut']; ?>
" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_s2']; ?>
" id="acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_s2']; ?>
" onchange="javascript: adjustDateField('acces_formulaire_date_fin_<?php echo $this->_tpl_vars['annee_s2']; ?>
');" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_s2']]['acces_formulaire']['date_fin']; ?>
" /></td>
			</tr>
		</table>

		<input type="button" class="submit" value="Mettre &agrave; jour" onclick="javascript: submitAdminControls('setAccesFormulaire');" />


		<br clear="all" />
		<br clear="all" />
		<br clear="all" />

		<h2>Acc&egrave;s au cumulatif d'ensemble</h2>

		<table cellpadding="0" cellspacing="0" border="0" class="noborder">
			<tr>
				<td width="75px;"><strong>Ann&eacute;e</strong></td>
				<td><strong>Ouvert&nbsp;?</strong></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_p3']; ?>
</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_p3']; ?>
" id="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_p3']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['accesCumulArray'][$this->_tpl_vars['annee_p3']]): ?>checked="checked"<?php endif; ?>/></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_p2']; ?>
</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_p2']; ?>
" id="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_p2']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['accesCumulArray'][$this->_tpl_vars['annee_p2']]): ?>checked="checked"<?php endif; ?>/></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_p1']; ?>
</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_p1']; ?>
" id="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_p1']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['accesCumulArray'][$this->_tpl_vars['annee_p1']]): ?>checked="checked"<?php endif; ?>/></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_c0']; ?>
</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_c0']; ?>
" id="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_c0']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['accesCumulArray'][$this->_tpl_vars['annee_c0']]): ?>checked="checked"<?php endif; ?>/></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_s1']; ?>
</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_s1']; ?>
" id="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_s1']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['accesCumulArray'][$this->_tpl_vars['annee_s1']]): ?>checked="checked"<?php endif; ?>/></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_s2']; ?>
</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_s2']; ?>
" id="acces_cumulatif_ensemble_<?php echo $this->_tpl_vars['annee_s2']; ?>
" value="1" class="checkbox" <?php if ($this->_tpl_vars['accesCumulArray'][$this->_tpl_vars['annee_s2']]): ?>checked="checked"<?php endif; ?>/></td>
			</tr>
		</table>
		
		<input type="button" class="submit" value="Mettre &agrave; jour" onclick="javascript: submitAdminControls('setAccesCumul');" />

		<br clear="all" />
		<br clear="all" />
		<br clear="all" />

		<h2>Valeurs annuelles</h2>

		<table cellpadding="0" cellspacing="0" border="0" class="noborder">
			<tr>
				<td width="75px;"><strong>Ann&eacute;e</strong></td>
				<td><strong>Pr&eacute;l&egrave;vements (P)</strong></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_p3']; ?>
</td>
				<td><input type="text" name="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_p3']; ?>
" id="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_p3']; ?>
" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p3']]['valeurs_annuelles']['prelevements']; ?>
" /></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_p2']; ?>
</td>
				<td><input type="text" name="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_p2']; ?>
" id="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_p2']; ?>
" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p2']]['valeurs_annuelles']['prelevements']; ?>
" /></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_p1']; ?>
</td>
				<td><input type="text" name="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_p1']; ?>
" id="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_p1']; ?>
" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_p1']]['valeurs_annuelles']['prelevements']; ?>
" /></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_c0']; ?>
</td>
				<td><input type="text" name="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_c0']; ?>
" id="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_c0']; ?>
" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_c0']]['valeurs_annuelles']['prelevements']; ?>
" /></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_s1']; ?>
</td>
				<td><input type="text" name="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_s1']; ?>
" id="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_s1']; ?>
" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_s1']]['valeurs_annuelles']['prelevements']; ?>
" /></td>
			</tr>
			<tr>
				<td><?php echo $this->_tpl_vars['annee_s2']; ?>
</td>
				<td><input type="text" name="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_s2']; ?>
" id="valeurs_prelevements_<?php echo $this->_tpl_vars['annee_s2']; ?>
" value="<?php echo $this->_tpl_vars['ratiosControlsArray'][$this->_tpl_vars['annee_s2']]['valeurs_annuelles']['prelevements']; ?>
" /></td>
			</tr>
		</table>
		
		<input type="button" class="submit" value="Mettre &agrave; jour" onclick="javascript: submitAdminControls('setValeursAnnuelles');" />

	</form>
		
	<?php endif;  endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>