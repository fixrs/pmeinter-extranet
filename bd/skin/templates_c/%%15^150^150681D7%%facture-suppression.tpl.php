<?php /* Smarty version 2.6.13, created on 2011-01-14 11:20:46
         compiled from facturation/facture-suppression.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

	<?php echo $this->_tpl_vars['errors']; ?>


	<?php if ($this->_tpl_vars['nologin'] != 1): ?>

		<?php echo '
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = \'\';
			var action = document.getElementById(\'action\');
			var etudeKey = document.getElementById(\'etudeKey\');
			var factureKey = document.getElementById(\'factureKey\');

			switch (thisAction) {
				case \'supprimer\':
					var etudeKeyDelValue = getSelectValue(document.getElementById(\'etudeKeyDel\'));
					var factureKeyDelValue = getSelectValue(document.getElementById(\'factureKeyFieldDel_\' + etudeKeyDelValue));
					if (factureKeyDelValue == \'\' || factureKeyDelValue == \'0\' || factureKeyDelValue == \'undefined\' || factureKeyDelValue == null) {
						errorMsg += \'Vous devez choisir une facture.\\n\';
					}
					action.value = \'supprimer\';
					etudeKey.value = etudeKeyDelValue;
					factureKey.value = factureKeyDelValue;
					break;
			}

			if (errorMsg == \'\') {
				document.getElementById(\'factureForm\').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function loadFactures(form) {
			var factureKeyFields = new Array();
			';  $_from = $this->_tpl_vars['facturesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['factureForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['factureForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['etudeKey'] => $this->_tpl_vars['null']):
        $this->_foreach['factureForeach']['iteration']++;
 echo '
				factureKeyFields[';  echo ($this->_foreach['factureForeach']['iteration']-1);  echo '] = \'factureKeyField\' + form + \'_';  echo $this->_tpl_vars['etudeKey'];  echo '\';
			';  endforeach; endif; unset($_from);  echo '

			for (i = 0; i < factureKeyFields.length; i++) {
				document.getElementById(factureKeyFields[i]).style.display = \'none\';
				document.getElementById(factureKeyFields[i]).disabled = true;
			}

			var etudeKeyValue = getSelectValue(document.getElementById(\'etudeKey\' + form));
			var factureKeyField = document.getElementById(\'factureKeyField\' + form + \'_\' + etudeKeyValue);
			var factureKeyFieldDefault = document.getElementById(\'factureKeyField\' + form + \'_default\');

			if (
				etudeKeyValue != \'\' && etudeKeyValue != \'0\' && etudeKeyValue != \'undefined\' &&
				factureKeyField != \'\' && factureKeyField != \'undefined\' && factureKeyField != null
			) {
				factureKeyFieldDefault.style.display = \'none\';
				factureKeyField.style.display = \'inline\';
				factureKeyField.disabled = false;
			} else {
				factureKeyFieldDefault.style.display = \'inline\';
			}
		}
		
		
		</script>
		'; ?>


		<form action="facture.php" method="post" id="factureForm">
			<input type="hidden" name="form" id="form" value="suppression" />
			<input type="hidden" name="action" id="action" value="" />
			<input type="hidden" name="etudeKey" id="etudeKey" value="" />
			<input type="hidden" name="factureKey" id="factureKey" value="" />

			<h1>Suppression d'une facture administrative</h1>

			<div class="noticeDiv">
				<?php $_from = $this->_tpl_vars['noticeArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['notices'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['notices']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['notice']):
        $this->_foreach['notices']['iteration']++;
?>
					<?php if (($this->_foreach['notices']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="noticeBox"><?php echo $this->_tpl_vars['notice']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
				<div class="errorDiv">
				<?php $_from = $this->_tpl_vars['errorArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['errors'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['errors']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['erreur']):
        $this->_foreach['errors']['iteration']++;
?>
					<?php if (($this->_foreach['errors']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="errorBox"><?php echo $this->_tpl_vars['erreur']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
				<br clear="all" />
	
			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKeyDel" id="etudeKeyDel" onchange="javascript: loadFactures('Del');">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				<?php $_from = $this->_tpl_vars['etudesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
					<?php if ($this->_tpl_vars['clef'] == $this->_tpl_vars['etudeKey2']): ?>
						<option value="<?php echo $this->_tpl_vars['clef']; ?>
" selected="selected"><?php echo $this->_tpl_vars['etude']; ?>
</option>
					<?php else: ?>
						<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
					<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
			</select><br />
	
			<br />

			<label>Facture&nbsp;:</label>

			<select name="factureKeyFieldDel_default" id="factureKeyFieldDel_default" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
			</select>

			<?php $_from = $this->_tpl_vars['facturesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['etudeClef'] => $this->_tpl_vars['null']):
?>
			<select name="factureKeyFieldDel_<?php echo $this->_tpl_vars['etudeClef']; ?>
" id="factureKeyFieldDel_<?php echo $this->_tpl_vars['etudeClef']; ?>
" style="display:none;" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
				<?php $_from = $this->_tpl_vars['facturesArray'][$this->_tpl_vars['etudeClef']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['factureClef'] => $this->_tpl_vars['factureInfo']):
?>
					<option value="<?php echo $this->_tpl_vars['factureClef']; ?>
"><?php echo $this->_tpl_vars['factureInfo']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
			<?php endforeach; endif; unset($_from); ?>

			<br />
			<br />

			<input type="button" class="submit" value="Supprimer la facture" onclick="javascript: submitForm('supprimer');" /><br />

			<br />
			<br />
			<br />

		</form>

	<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>