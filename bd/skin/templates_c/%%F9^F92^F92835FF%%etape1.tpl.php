<?php /* Smarty version 2.6.13, created on 2013-04-04 21:35:58
         compiled from ratios/etape1.tpl */ ?>
<h2>Informations g&eacute;n&eacute;rales</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n">100.&nbsp;</td>
		<td class="l">
			<label for="nbNotairesAssocies">Nom de la personne responsable de la saisie des donn&eacute;es pour l'&eacute;tude&nbsp;:</label>
		</td>
		<td class="m" width="100px">
			<input type="text" name="nomUtilisateur" value="<?php echo $this->_tpl_vars['nomUtilisateur']; ?>
" onchange="" id="nomUtilisateur" />
		</td>
		<td class="r" width="30px">&nbsp;</td>
	</tr>

	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>

	<tr>
		<td class="n">101.&nbsp;</td>
		<td class="l">
			<label for="nbNotairesAssocies">Nombre de notaires associ&eacute;(e)s <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=titres&amp;dt=notairesAssocies#notairesAssocies');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbNotairesAssocies" value="<?php echo $this->_tpl_vars['nbNotairesAssocies']; ?>
" onchange="javascript: adjustNumericField('nbNotairesAssocies', 1, 2, 2, 0); toggleMust('nbNotairesAssocies');" id="nbNotairesAssocies" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">102.&nbsp;</td>
		<td class="l">
			<label for="nbNotairesSalaries">Nombre de notaires salari&eacute;(e)s <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=titres&amp;dt=notairesSalaries#notairesSalaries');" class="pop" />&nbsp;:</label>	
		</td>
		<td class="m">
			<input type="text" name="nbNotairesSalaries" value="<?php echo $this->_tpl_vars['nbNotairesSalaries']; ?>
" onchange="javascript: adjustNumericField('nbNotairesSalaries', 1, 2, 2, 0); toggleMust('nbNotairesSalaries');" id="nbNotairesSalaries" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">103.&nbsp;</td>
		<td class="l">
			<label for="nbNotairesTotal">Nombre total de notaires dans l'organisation&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbNotairesTotal" value="<?php echo $this->_tpl_vars['nbNotairesTotal']; ?>
" onchange="javascript: adjustNumericField('nbNotairesTotal', 1, 2, 2, 0); toggleMust('nbNotairesTotal');" id="nbNotairesTotal" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">104.&nbsp;</td>
		<td class="l">
			<label for="nbEmployes">Nombre de collaborateur(rice)s <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=titres&amp;dt=employesNonNotaires#employesNonNotaires');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbEmployes" value="<?php echo $this->_tpl_vars['nbEmployes']; ?>
" onchange="javascript: adjustNumericField('nbEmployes', 1, 2, 2, 0); toggleMust('nbEmployes');" id="nbEmployes" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">105.&nbsp;</td>
		<td class="l">
			<label for="nbStagieres">Nombre de stagiaires en notariat <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=titres&amp;dt=stagiairesEnNotariat#stagiairesEnNotariat');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbStagieres" value="<?php echo $this->_tpl_vars['nbStagieres']; ?>
" onchange="javascript: adjustNumericField('nbStagieres', 1, 2, 2, 0);" id="nbStagieres" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>

	<tr>
		<td class="n">106.&nbsp;</td>
		<td class="l">
			<label for="dossierMandat">De fa&ccedil;on g&eacute;n&eacute;rale, ouvrez-vous un nouveau dossier par mandat&nbsp;?</label>
		</td>
		<td class="m">
			<select name="dossierMandat" id="dossierMandat" onchange="">
				<option value="">--</option>
				<option value="1" <?php if ($this->_tpl_vars['dossierMandat'] === '1'): ?>selected="selected"<?php endif; ?>>Oui</option>
				<option value="0" <?php if ($this->_tpl_vars['dossierMandat'] === '0'): ?>selected="selected"<?php endif; ?>>Non</option>
			</select>
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">107.&nbsp;</td>
		<td class="l">
			<label for="nbDossiers">Nombre de dossiers trait&eacute;s par l'&eacute;tude&nbsp;:</label> 
		</td>
		<td class="m">
			<input type="text" name="nbDossiers" value="<?php echo $this->_tpl_vars['nbDossiers']; ?>
" onchange="javascript: adjustNumericField('nbDossiers', 0, 0, 5, 0); toggleMust('nbDossiers');" id="nbDossiers" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">108.&nbsp;</td>
		<td class="l">
			<label for="nbMinutesNotaires">Nombre de minutes sign&eacute;es par les notaires&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbMinutesNotaires" value="<?php echo $this->_tpl_vars['nbMinutesNotaires']; ?>
" onchange="javascript: adjustNumericField('nbMinutesNotaires', 0, 0, 5, 0); toggleMust('nbMinutesNotaires');" id="nbMinutesNotaires" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">109.&nbsp;</td>
		<td class="l">
			<label for="minutesExcluent">Les minutes excluent-elles les actes de radiation&nbsp;?</label>
		</td>
		<td class="m">
			<select name="minutesExcluent" id="minutesExcluent" onchange="">
				<option value="">--</option>
				<option value="1" <?php if ($this->_tpl_vars['minutesExcluent'] === '1'): ?>selected="selected"<?php endif; ?>>Oui</option>
				<option value="0" <?php if ($this->_tpl_vars['minutesExcluent'] === '0'): ?>selected="selected"<?php endif; ?>>Non</option>
			</select>
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>
	
	<tr>
		<td class="n">110.&nbsp;</td>
		<td class="l">
			<label for="nbSocietes">Combien de soci&eacute;t&eacute;s sont membres de votre service corporatif&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" name="nbSocietes" value="<?php echo $this->_tpl_vars['nbSocietes']; ?>
" onchange="javascript: adjustNumericField('nbSocietes', 0, 0, 4, 0);" id="nbSocietes" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>
	<tr>
		<td class="n">111.&nbsp;</td>
		<td class="l">
			<label for="nbFeducies">Combien de fiducies sont membres de votre service fiducie&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" name="nbFeducies" value="<?php echo $this->_tpl_vars['nbFeducies']; ?>
" onchange="javascript: adjustNumericField('nbFeducies', 0, 0, 4, 0);" id="nbFeducies" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>
	<tr>
		<td class="n">112.&nbsp;</td>
		<td class="l">
			<label for="nbDossiersAG">Quel est le nombre de dossiers en cours pour votre service Ange Gardien&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" name="nbDossiersAG" value="<?php echo $this->_tpl_vars['nbDossiersAG']; ?>
" onchange="javascript: adjustNumericField('nbDossiersAG', 0, 0, 4, 0);" id="nbDossiersAG" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>
	<tr>
		<td class="n">113.&nbsp;</td>
		<td class="l">
			<label for="nbDossiersMC">Quel est le nombre de dossiers en cours pour votre service Ma&icirc;tre des clefs&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" name="nbDossiersMC" value="<?php echo $this->_tpl_vars['nbDossiersMC']; ?>
" onchange="javascript: adjustNumericField('nbDossiersMC', 0, 0, 4, 0);" id="nbDossiersMC" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>
</table>