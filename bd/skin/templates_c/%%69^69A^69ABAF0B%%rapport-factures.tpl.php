<?php /* Smarty version 2.6.13, created on 2011-04-05 08:58:34
         compiled from facturation/rapport-factures.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'facturation/rapport-factures.tpl', 19, false),array('modifier', 'toMoneyFormat', 'facturation/rapport-factures.tpl', 56, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['nologin'] != 1): ?>

	<center>
		<h1>Rapport administratif</h1>
		<strong><?php echo $this->_tpl_vars['dateDebut']; ?>
</strong> au <strong><?php echo $this->_tpl_vars['dateFin']; ?>
</strong><br />
	</center>

	<div id="printpage"><a href="javascript: window.print();"><img class="print" src="<?php echo $this->_tpl_vars['SKIN_URL']; ?>
/img/printer.jpg" alt="Imprimer" /> Imprimer le rapport</a></div>

	<br />
	<br />

	<?php $this->assign('ln', '0'); ?>
	
	<?php $_from = $this->_tpl_vars['reportArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['reportForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['reportForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['n'] => $this->_tpl_vars['etude']):
        $this->_foreach['reportForeach']['iteration']++;
?>
	
	<?php if (count($this->_tpl_vars['etude']['factures']) > 0): ?>
	
		<?php $this->assign('lx', count($this->_tpl_vars['etude']['factures'])); ?>

		<?php $this->assign('ln', $this->_tpl_vars['ln']+$this->_tpl_vars['lx']+3); ?>

		<?php if ($this->_tpl_vars['ln'] > 40): ?>

			<?php $this->assign('ln', '0'); ?>

			<hr class="pagebreak" />

			<div class="printblock">
				<center>
					<h1>Rapport administratif</h1>
					<strong><?php echo $this->_tpl_vars['dateDebut']; ?>
</strong> au <strong><?php echo $this->_tpl_vars['dateFin']; ?>
</strong><br />
				</center>
				<br />
				<br />
			</div>
		<?php endif; ?>

		<table cellpadding="0" cellspacing="0" border="0" class="rapport_factures">

			<tr>
				<td class="etude_info_nom" style="text-align:left;" colspan="2"><strong><?php echo $this->_tpl_vars['etude']['info']['etude_nom']; ?>
</strong></td>
			</tr>

			<tr>
				<td class="facture_label" style="text-align:left;">Num&eacute;ro de la facture&nbsp;</td>
				<td class="facture_label" style="text-align:right; width:140px;">Montant de la facture&nbsp;</td>
			</tr>
	
			<?php $_from = $this->_tpl_vars['etude']['factures']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['factureForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['factureForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['f'] => $this->_tpl_vars['facture']):
        $this->_foreach['factureForeach']['iteration']++;
?>

				<tr>
					<td class="facture_value" style="text-align:left;"><?php echo $this->_tpl_vars['facture']['numero_facture']; ?>
</td>
					<td class="facture_value" style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['facture']['montant_facture'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
				</tr>
				
			<?php endforeach; endif; unset($_from); ?>

		</table><br />

	<?php endif; ?>

	<?php endforeach; endif; unset($_from); ?>

<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>