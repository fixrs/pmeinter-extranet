<?php /* Smarty version 2.6.13, created on 2009-04-21 11:47:36
         compiled from ratios/2008/etape1.tpl */ ?>
<h2>Informations g&eacute;n&eacute;rales</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="l" width="320px">
			<label for="nbNotairesAssocies">1.&nbsp;Nom de la personne responsable de la saisie des donn&eacute;es pour l'&eacute;tude&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nomUtilisateur" value="<?php echo $this->_tpl_vars['nomUtilisateur']; ?>
" onchange="" id="nomUtilisateur" />
		</td>
		<td class="r" width="30px">&nbsp;</td>
	</tr>

	<tr>
		<td class="l">
			<label for="nbNotairesAssocies">2.&nbsp;Nombre de notaires associ&eacute;(e)s&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbNotairesAssocies" value="<?php echo $this->_tpl_vars['nbNotairesAssocies']; ?>
" onchange="javascript: adjustNumericField('nbNotairesAssocies', 1, 2, 2, 0); toggleMust('nbNotairesAssocies');" id="nbNotairesAssocies" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="l">
			<label for="nbNotairesSalaries">3.&nbsp;Nombre de notaires salari&eacute;(e)s&nbsp;:</label>	
		</td>
		<td class="m">
			<input type="text" name="nbNotairesSalaries" value="<?php echo $this->_tpl_vars['nbNotairesSalaries']; ?>
" onchange="javascript: adjustNumericField('nbNotairesSalaries', 1, 2, 2, 0); toggleMust('nbNotairesSalaries');" id="nbNotairesSalaries" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="l">
			<label for="nbNotairesTotal">4.&nbsp;Nombre total de notaires dans l'organisation&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbNotairesTotal" value="<?php echo $this->_tpl_vars['nbNotairesTotal']; ?>
" onchange="javascript: adjustNumericField('nbNotairesTotal', 1, 2, 2, 0); toggleMust('nbNotairesTotal');" id="nbNotairesTotal" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="l">
			<label for="nbEmployes">5.&nbsp;Nombre d'employ&eacute;s (notaires exclus)&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbEmployes" value="<?php echo $this->_tpl_vars['nbEmployes']; ?>
" onchange="javascript: adjustNumericField('nbEmployes', 1, 2, 2, 0); toggleMust('nbEmployes');" id="nbEmployes" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="l">
			<label for="nbDossiers">6.&nbsp;Nombre de dossiers trait&eacute;s par l'&eacute;tude&nbsp;:</label> 
		</td>
		<td class="m">
			<input type="text" name="nbDossiers" value="<?php echo $this->_tpl_vars['nbDossiers']; ?>
" onchange="javascript: adjustNumericField('nbDossiers', 0, 0, 5, 0); toggleMust('nbDossiers');" id="nbDossiers" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="l">
			<label for="nbMinutesNotaires">7.&nbsp;Nombre de minutes sign&eacute;es par les notaires&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbMinutesNotaires" value="<?php echo $this->_tpl_vars['nbMinutesNotaires']; ?>
" onchange="javascript: adjustNumericField('nbMinutesNotaires', 0, 0, 5, 0); toggleMust('nbMinutesNotaires');" id="nbMinutesNotaires" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="l">
			<label for="minutesExcluent">8.&nbsp;Les minutes excluent-elles les actes de radiation&nbsp;?</label>
		</td>
		<td class="m">
			<select name="minutesExcluent" id="minutesExcluent" onchange="javascript: toggleMust('minutesExcluent');">
				<option value="">--</option>
				<option value="1" <?php if ($this->_tpl_vars['minutesExcluent'] === '1'): ?>selected="selected"<?php endif; ?>>Oui</option>
				<option value="0" <?php if ($this->_tpl_vars['minutesExcluent'] === '0'): ?>selected="selected"<?php endif; ?>>Non</option>
			</select>
		</td>
		<td class="r">&nbsp;</td>
	</tr>
</table>