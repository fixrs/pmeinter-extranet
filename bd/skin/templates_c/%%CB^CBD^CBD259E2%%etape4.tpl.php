<?php /* Smarty version 2.6.13, created on 2009-04-21 13:34:35
         compiled from ratios/2008/etape4.tpl */ ?>
<h2>R&eacute;partition des honoraires (H)</h2>

<h3>Immobilier r&eacute;sidentiel&nbsp;:</h3>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="l" width="400px">
			<label for="immobilierExistant">41.&nbsp;Immobilier existant&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['immobilierExistant']; ?>
" name="immobilierExistant" onchange="javascript: adjustNumericField('immobilierExistant', 1, 2, 0, 0); afficherSousTotal('residentiel'); afficherTotal('residentiel');" class="total" id="immobilierExistant" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="immobilierNeuf">42.&nbsp;Immobilier neuf&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['immobilierNeuf']; ?>
" name="immobilierNeuf" onchange="javascript: adjustNumericField('immobilierNeuf', 1, 2, 0, 0); afficherSousTotal('residentiel'); afficherTotal('residentiel');" class="total" id="immobilierNeuf" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="immobilierResidentielSousTotal" class="total">43.&nbsp;Sous-total&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['immobilierResidentielSousTotal']; ?>
" name="immobilierResidentielSousTotal" onchange="" id="immobilierResidentielSousTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l diff">
			<label for="immobilierResidentielDiff">Si ce montant ne correspond pas au total des honoraires,<br />veuillez inscrire la diff&eacute;rence&nbsp;:</label>
		</td>
		<td class="m diff">
			<input type="text" value="<?php echo $this->_tpl_vars['immobilierResidentielDiff']; ?>
" name="immobilierResidentielDiff" onchange="javascript: adjustNumericField('immobilierResidentielDiff', 1, 2, 0, 1); afficherTotal('residentiel');" class="total" id="immobilierResidentielDiff" />
		</td>
		<td class="r diff">$</td>
	</tr>

	<tr>
		<td class="l total">
			<label for="immobilierResidentielTotal" class="total">44.&nbsp;Total</label>
		</td>
		<td class="m total">
			<input type="text" value="<?php echo $this->_tpl_vars['immobilierResidentielTotal']; ?>
" name="immobilierResidentielTotal" onchange="" id="immobilierResidentielTotal" class="total readonly" readonly="readonly"  />
		</td>
		<td class="r total">$</td>
	</tr>
</table>

<br clear="all" />

<h3>Commercial & corporatif&nbsp;:</h3>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="l" width="420px">
			<label for="commercial">45.&nbsp;Commercial&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['commercial']; ?>
" name="commercial" onchange="javascript: adjustNumericField('commercial', 1, 2, 0, 0); afficherSousTotal('commercial'); afficherTotal('commercial');" id="commercial" class="total" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="immobilierComm">46.&nbsp;Immobilier commercial & industriel&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['immobilierComm']; ?>
" name="immobilierComm" onchange="javascript: adjustNumericField('immobilierComm', 1, 2, 0, 0); afficherSousTotal('commercial'); afficherTotal('commercial');" id="immobilierComm" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="corporatif">47.&nbsp;Corporatif&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['corporatif']; ?>
" name="corporatif" onchange="javascript: adjustNumericField('corporatif', 1, 2, 0, 0); afficherSousTotal('commercial'); afficherTotal('commercial');" id="corporatif" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="serviceCorporatif">48.&nbsp;Service Corporatif&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['serviceCorporatif']; ?>
" name="serviceCorporatif" onchange="javascript: adjustNumericField('serviceCorporatif', 1, 2, 0, 0); afficherSousTotal('commercial'); afficherTotal('commercial');" id="serviceCorporatif" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="commercialCorporatifSousTotal" class="total">49.&nbsp;Sous-total&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['commercialCorporatifSousTotal']; ?>
" name="commercialCorporatifSousTotal" onchange="" id="commercialCorporatifSousTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l diff">
			<label for="commercialCorporatifDiff">Si ce montant ne correspond pas au total des honoraires,<br />veuillez inscrire la diff&eacute;rence&nbsp;:</label>
		</td>
		<td class="m diff">
			<input type="text" value="<?php echo $this->_tpl_vars['commercialCorporatifDiff']; ?>
" name="commercialCorporatifDiff" onchange="javascript: adjustNumericField('commercialCorporatifDiff', 1, 2, 0, 1); afficherTotal('commercial');" id="commercialCorporatifDiff" class="total" />
		</td>
		<td class="r diff">$</td>
	</tr>

	<tr>
		<td class="l total">
			<label for="commercialCorporatifTotal" class="total">50.&nbsp;Total</label>
		</td>
		<td class="m total">
			<input type="text" value="<?php echo $this->_tpl_vars['commercialCorporatifTotal']; ?>
" name="commercialCorporatifTotal" onchange="" id="commercialCorporatifTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r total">$</td>
	</tr>
</table>

<br clear="all" />

<h3>Personne, succession &amp; famille&nbsp;:</h3>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="l" width="400px">
			<label for="testamentsMandats">51.&nbsp;Testaments &amp; mandats&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['testamentsMandats']; ?>
" name="testamentsMandats" onchange="javascript: adjustNumericField('testamentsMandats', 1, 2, 0, 0); afficherSousTotal('personne'); afficherTotal('personne');" id="testamentsMandats" class="total" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="donationsFiduciaires">52.&nbsp;Donations fiduciaires :</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['donationsFiduciaires']; ?>
" name="donationsFiduciaires" onchange="javascript: adjustNumericField('donationsFiduciaires', 1, 2, 0, 0); afficherSousTotal('personne'); afficherTotal('personne');" id="donationsFiduciaires" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="successions">53.&nbsp;Successions</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['successions']; ?>
" name="successions" onchange="javascript: adjustNumericField('successions', 1, 2, 0, 0); afficherSousTotal('personne'); afficherTotal('personne');" id="successions" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="mediationFamiliale">54.&nbsp;M&eacute;diation familiale&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['mediationFamiliale']; ?>
" name="mediationFamiliale" onchange="javascript: adjustNumericField('mediationFamiliale', 1, 2, 0, 0); afficherSousTotal('personne'); afficherTotal('personne');" id="mediationFamiliale" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="proceduresNonContent">55.&nbsp;Proc&eacute;dures non contentieuses&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['proceduresNonContent']; ?>
" name="proceduresNonContent" onchange="javascript: adjustNumericField('proceduresNonContent', 1, 2, 0, 0); afficherSousTotal('personne'); afficherTotal('personne');" id="proceduresNonContent" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="protectionPatrimoine">56.&nbsp;Protection du patrimoine&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['protectionPatrimoine']; ?>
" name="protectionPatrimoine" onchange="javascript: adjustNumericField('protectionPatrimoine', 1, 2, 0, 0); afficherSousTotal('personne'); afficherTotal('personne');" id="protectionPatrimoine" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="autresPersonneFamilial">57.&nbsp;Autres - personne & familial&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['autresPersonneFamilial']; ?>
" name="autresPersonneFamilial" onchange="javascript: adjustNumericField('autresPersonneFamilial', 1, 2, 0, 0); afficherSousTotal('personne'); afficherTotal('personne');" id="autresPersonneFamilial" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="personneSuccessionFamilialSousTotal" class="total">58.&nbsp;Sous-total&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['personneSuccessionFamilialSousTotal']; ?>
" name="personneSuccessionFamilialSousTotal" onchange="" id="personneSuccessionFamilialSousTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l diff">
			<label for="<?php echo $this->_tpl_vars['personneSuccessionFamilialDiff']; ?>
">Si ce montant ne correspond pas au total des honoraires,<br />veuillez inscrire la diff&eacute;rence&nbsp;:</label>
		</td>
		<td class="m diff">
			<input type="text" value="<?php echo $this->_tpl_vars['personneSuccessionFamilialDiff']; ?>
" name="personneSuccessionFamilialDiff" onchange="javascript: adjustNumericField('personneSuccessionFamilialDiff', 1, 2, 0, 1); afficherTotal('personne');" id="personneSuccessionFamilialDiff" class="total" />
		</td>
		<td class="r diff">$</td>
	</tr>

	<tr>
		<td class="l total">
			<label for="personneSuccessionFamilialTotal" class="total">59.&nbsp;Total</label>
		</td>
		<td class="m total">
			<input type="text" value="<?php echo $this->_tpl_vars['personneSuccessionFamilialTotal']; ?>
" name="personneSuccessionFamilialTotal" onchange="" id="personneSuccessionFamilialTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r total">$</td>
	</tr>
</table>

<br clear="all" />

<h3>Agricole, municipal & administratif&nbsp;:</h3>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="l" width="400px">
			<label for="agricole">60.&nbsp;Agricole&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['agricole']; ?>
" name="agricole" onchange="javascript: adjustNumericField('agricole', 1, 2, 0, 0); afficherSousTotal('agricole'); afficherTotal('agricole');" id="agricole" class="total" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="municipalAdministratif">61.&nbsp;Municipal & administratif :</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['municipalAdministratif']; ?>
" name="municipalAdministratif" onchange="javascript: adjustNumericField('municipalAdministratif', 1, 2, 0, 0); afficherSousTotal('agricole'); afficherTotal('agricole');" id="municipalAdministratif" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="agricoleMunicipalAdministratifSousTotal" class="total">62.&nbsp;Sous-total&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['agricoleMunicipalAdministratifSousTotal']; ?>
" name="agricoleMunicipalAdministratifSousTotal" onchange="" id="agricoleMunicipalAdministratifSousTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l diff">
			<label for="agricoleMunicipalAdministratifDiff" style="width:90%;">Si ce montant ne correspond pas au total des honoraires,<br />veuillez inscrire la diff&eacute;rence&nbsp;:</label>
		</td>
		<td class="m diff">
			<input type="text" value="<?php echo $this->_tpl_vars['agricoleMunicipalAdministratifDiff']; ?>
" name="agricoleMunicipalAdministratifDiff" onchange="javascript: adjustNumericField('agricoleMunicipalAdministratifDiff', 1, 2, 0, 1); afficherTotal('agricole');" id="agricoleMunicipalAdministratifDiff" class="total" />
		</td>
		<td class="r diff">$</td>
	</tr>

	<tr>
		<td class="l total">
			<label for="agricoleMunicipalAdministratifTotal" class="total">63.&nbsp;Total</label>
		</td>
		<td class="m total">
			<input type="text" value="<?php echo $this->_tpl_vars['agricoleMunicipalAdministratifTotal']; ?>
" name="agricoleMunicipalAdministratifTotal" onchange="" id="agricoleMunicipalAdministratifTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r total">$</td>
	</tr>
</table>

<br clear="all" />

<h3>Divers</h3>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="l" width="400px">
			<label for="divers">64.&nbsp;Divers&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['divers']; ?>
" name="divers" onchange="javascript: adjustNumericField('divers', 1, 2, 0, 0); afficherSousTotal('divers'); afficherTotal('divers');" id="divers" class="total" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="diversSousTotal" class="total">65.&nbsp;Sous-total&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['diversSousTotal']; ?>
" name="diversSousTotal" onchange="" id="diversSousTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l diff">
			<label for="diversDiff" style="width:90%;">Si ce montant ne correspond pas au total des honoraires,<br />veuillez inscrire la diff&eacute;rence&nbsp;:</label>
		</td>
		<td class="m diff">
			<input type="text" value="<?php echo $this->_tpl_vars['diversDiff']; ?>
" name="diversDiff" onchange="javascript: adjustNumericField('diversDiff', 1, 2, 0, 1); afficherTotal('divers');" id="diversDiff" class="total" />
		</td>
		<td class="r diff">$</td>
	</tr>

	<tr>
		<td class="l total">
			<label for="diversTotal" class="total">66.&nbsp;Total</label>
		</td>
		<td class="m total">
			<input type="text" value="<?php echo $this->_tpl_vars['diversTotal']; ?>
" name="diversTotal" onchange="" id="diversTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r total">$</td>
	</tr>

</table>

<br clear="all" />
<br clear="all" />

<h2>Autres renseignements</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="l" width="400px">
			<label for="accepteMandats">67.&nbsp;Acceptez-vous des mandats en provenance de centres de traitement de dossiers immobiliers (ex. : FCT)&nbsp;?</label>
		</td>
		<td class="m">
			<select name="accepteMandats" id="accepteMandats" onchange="javascript: toggleMust('accepteMandats');">
				<option value="">--</option>
				<option value="1" <?php if ($this->_tpl_vars['accepteMandats'] === '1'): ?>selected="selected"<?php endif; ?>>Oui</option>
				<option value="0" <?php if ($this->_tpl_vars['accepteMandats'] === '0'): ?>selected="selected"<?php endif; ?>>Non</option>
			</select><br clear="all" />
		</td>
		<td class="r" width="30px">&nbsp;</td>
	</tr>

	<tr>
		<td class="l">
			<label for="tauxRemboursementKilo">68.&nbsp;Taux de remboursement - kilom&eacute;trage&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['tauxRemboursementKilo']; ?>
" name="tauxRemboursementKilo" onchange="javascript: adjustNumericField('tauxRemboursementKilo', 1, 2, 1, 0); toggleMust('tauxRemboursementKilo');" id="tauxRemboursementKilo" class="argent" />
		</td>
		<td class="r">$/km</td>
	</tr>

	<tr>
		<td class="l">
			<label for="numerisationPiecesIdentite">69.&nbsp;Num&eacute;risation des pi&egrave;ces d'identit&eacute; et autres documents&nbsp;:</label>
		</td>
		<td class="m">
			<select name="numerisationPiecesIdentite" id="numerisationPiecesIdentite" onchange="javascript: toggleMust('numerisationPiecesIdentite');">
				<option value="">--</option>
				<option value="1" <?php if ($this->_tpl_vars['numerisationPiecesIdentite'] === '1'): ?>selected="selected"<?php endif; ?>>Oui</option>
				<option value="0" <?php if ($this->_tpl_vars['numerisationPiecesIdentite'] === '0'): ?>selected="selected"<?php endif; ?>>Non</option>
			</select>
		</td>
		<td class="r">&nbsp;</td>
	</tr>
</table>