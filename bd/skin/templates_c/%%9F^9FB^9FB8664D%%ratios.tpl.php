<?php /* Smarty version 2.6.13, created on 2011-07-19 16:26:42
         compiled from ratios/ratios.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'ratios/ratios.tpl', 17, false),array('modifier', 'cat', 'ratios/ratios.tpl', 44, false),array('function', 'math', 'ratios/ratios.tpl', 65, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="ratios">
	<form action="ratios.php" method="post" id="ratiosForm">
		<input type="hidden" name="action" id="action" value="display" />
		<input type="hidden" name="etudeKey" id="etudeKey" value="<?php echo $this->_tpl_vars['etudeKey']; ?>
" />
		<input type="hidden" name="annee" id="annee" value="<?php echo $this->_tpl_vars['annee']; ?>
" />
		<input type="hidden" name="currentForm" id="currentForm" value="<?php echo $this->_tpl_vars['currentForm']; ?>
" />
		<input type="hidden" name="requestForm" id="requestForm" value="" />

		<div id="infosEtude"><?php echo $this->_tpl_vars['nomEtude']; ?>
&nbsp;-&nbsp;<?php echo $this->_tpl_vars['annee']; ?>
</div>

		<div id="etapes">&Eacute;tape <?php echo $this->_tpl_vars['currentForm']; ?>
/6</div>

		<br clear="all" />

		<?php if (count($this->_tpl_vars['noticeArray']) > 0): ?>
		<div class="noticeDiv">
			<?php $_from = $this->_tpl_vars['noticeArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['notice']):
?>
				<div class="noticeBox"><?php echo $this->_tpl_vars['notice']; ?>
</div>
			<?php endforeach; endif; unset($_from); ?>
		</div>
		<?php endif; ?>

		<?php if (count($this->_tpl_vars['errorArray']) > 0): ?>
		<div class="errorDiv">
			<?php $_from = $this->_tpl_vars['errorArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['erreur']):
?>
				<div class="errorBox"><?php echo $this->_tpl_vars['erreur']; ?>
</div>
			<?php endforeach; endif; unset($_from); ?>
		</div>
		<?php endif; ?>

		<?php if (count($this->_tpl_vars['warningArray']) > 0): ?>
		<div class="warningDiv">
			<?php $_from = $this->_tpl_vars['warningArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['warning']):
?>
				<div class="warningBox"><?php echo $this->_tpl_vars['warning']; ?>
</div>
			<?php endforeach; endif; unset($_from); ?>
		</div>
		<?php endif; ?>

		<br clear="all" />

		<?php if (! $this->_tpl_vars['ratioClosed']): ?>
			<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => ((is_array($_tmp=((is_array($_tmp="ratios/")) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['form']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['form'])))) ? $this->_run_mod_handler('cat', true, $_tmp, ".tpl") : smarty_modifier_cat($_tmp, ".tpl")), 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['currentForm'] == 1): ?>

			<div id="navigation">
				<br clear="all" />
				<input type="button" value="&Eacute;tape suivante &raquo;" class="navigation submit" onclick="submitRatiosForm('1', '2');" />
			</div>

		<?php elseif ($this->_tpl_vars['currentForm'] == 6): ?>

			<div id="navigation">
				<br clear="all" />
				<input type="button" value="&laquo; &Eacute;tape pr&eacute;c&eacute;dente" class="navigation submit" onclick="submitRatiosForm('6', '5');" />
			</div>

		<?php else: ?>

			<div id="navigation">
				<br clear="all" />
				<input type="button" value="&laquo; &Eacute;tape pr&eacute;c&eacute;dente" class="navigation submit" onclick="submitRatiosForm('<?php echo $this->_tpl_vars['currentForm']; ?>
', '<?php echo smarty_function_math(array('equation' => "x-y",'x' => $this->_tpl_vars['currentForm'],'y' => '1'), $this);?>
');" />
				<input type="button" value="&Eacute;tape suivante &raquo;" class="navigation submit" onclick="submitRatiosForm('<?php echo $this->_tpl_vars['currentForm']; ?>
', '<?php echo smarty_function_math(array('equation' => "x+y",'x' => $this->_tpl_vars['currentForm'],'y' => '1'), $this);?>
');" />
			</div>
		
		<?php endif; ?>
	</form>
</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>