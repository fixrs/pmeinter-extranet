<?php /* Smarty version 2.6.13, created on 2011-01-14 11:20:32
         compiled from facturation/paiement-suppression.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

	<?php echo $this->_tpl_vars['errors']; ?>


	<?php if ($this->_tpl_vars['nologin'] != 1): ?>

		<?php echo '
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = \'\';
			var action = document.getElementById(\'action\');
			var etudeKey = document.getElementById(\'etudeKey\');
			var paiementKey = document.getElementById(\'paiementKey\');

			switch (thisAction) {
				case \'supprimer\':
					var etudeKeyDelValue = getSelectValue(document.getElementById(\'etudeKeyDel\'));
					var paiementKeyDelValue = getSelectValue(document.getElementById(\'paiementKeyFieldDel_\' + etudeKeyDelValue));
					if (paiementKeyDelValue == \'\' || paiementKeyDelValue == \'0\' || paiementKeyDelValue == \'undefined\' || paiementKeyDelValue == null) {
						errorMsg += \'Vous devez choisir un paiement.\\n\';
					}
					action.value = \'supprimer\';
					etudeKey.value = etudeKeyDelValue;
					paiementKey.value = paiementKeyDelValue;
					break;
			}

			if (errorMsg == \'\') {
				document.getElementById(\'paiementForm\').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function loadPaiements(form) {
			var paiementKeyFields = new Array();
			';  $_from = $this->_tpl_vars['paiementsArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['paiementForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['paiementForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['etudeKey'] => $this->_tpl_vars['null']):
        $this->_foreach['paiementForeach']['iteration']++;
 echo '
				paiementKeyFields[';  echo ($this->_foreach['paiementForeach']['iteration']-1);  echo '] = \'paiementKeyField\' + form + \'_';  echo $this->_tpl_vars['etudeKey'];  echo '\';
			';  endforeach; endif; unset($_from);  echo '

			for (i = 0; i < paiementKeyFields.length; i++) {
				document.getElementById(paiementKeyFields[i]).style.display = \'none\';
				document.getElementById(paiementKeyFields[i]).disabled = true;
			}

			var etudeKeyValue = getSelectValue(document.getElementById(\'etudeKey\' + form));
			var paiementKeyField = document.getElementById(\'paiementKeyField\' + form + \'_\' + etudeKeyValue);
			var paiementKeyFieldDefault = document.getElementById(\'paiementKeyField\' + form + \'_default\');

			if (
				etudeKeyValue != \'\' && etudeKeyValue != \'0\' && etudeKeyValue != \'undefined\' &&
				paiementKeyField != \'\' && paiementKeyField != \'undefined\' && paiementKeyField != null
			) {
				paiementKeyFieldDefault.style.display = \'none\';
				paiementKeyField.style.display = \'inline\';
				paiementKeyField.disabled = false;
			} else {
				paiementKeyFieldDefault.style.display = \'inline\';
			}
		}
		
		
		</script>
		'; ?>


		<form action="paiement.php" method="post" id="paiementForm">
			<input type="hidden" name="form" id="form" value="suppression" />
			<input type="hidden" name="action" id="action" value="" />
			<input type="hidden" name="etudeKey" id="etudeKey" value="" />
			<input type="hidden" name="paiementKey" id="paiementKey" value="" />			

			<h1>Suppression d'un paiement</h1>

			<div class="noticeDiv">
				<?php $_from = $this->_tpl_vars['noticeArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['notices'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['notices']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['notice']):
        $this->_foreach['notices']['iteration']++;
?>
					<?php if (($this->_foreach['notices']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="noticeBox"><?php echo $this->_tpl_vars['notice']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
				<div class="errorDiv">
				<?php $_from = $this->_tpl_vars['errorArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['errors'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['errors']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['erreur']):
        $this->_foreach['errors']['iteration']++;
?>
					<?php if (($this->_foreach['errors']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="errorBox"><?php echo $this->_tpl_vars['erreur']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
				<br clear="all" />
	
			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKeyDel" id="etudeKeyDel" onchange="javascript: loadPaiements('Del');">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				<?php $_from = $this->_tpl_vars['etudesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
					<?php if ($this->_tpl_vars['clef'] == $this->_tpl_vars['etudeKey2']): ?>
						<option value="<?php echo $this->_tpl_vars['clef']; ?>
" selected="selected"><?php echo $this->_tpl_vars['etude']; ?>
</option>
					<?php else: ?>
						<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
					<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
			</select><br />
	
			<br />

			<label>Paiement&nbsp;:</label>

			<select name="paiementKeyFieldDel_default" id="paiementKeyFieldDel_default" disabled="disabled">
				<option selected="selected" value="0">- Choisir un paiement -</option>
			</select>

			<?php $_from = $this->_tpl_vars['paiementsArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['etudeClef'] => $this->_tpl_vars['null']):
?>
			<select name="paiementKeyFieldDel_<?php echo $this->_tpl_vars['etudeClef']; ?>
" id="paiementKeyFieldDel_<?php echo $this->_tpl_vars['etudeClef']; ?>
" style="display:none;" disabled="disabled">
				<option selected="selected" value="0">- Choisir un paiement -</option>
				<?php $_from = $this->_tpl_vars['paiementsArray'][$this->_tpl_vars['etudeClef']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['paiementClef'] => $this->_tpl_vars['paiementInfo']):
?>
					<option value="<?php echo $this->_tpl_vars['paiementClef']; ?>
"><?php echo $this->_tpl_vars['paiementInfo']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
			<?php endforeach; endif; unset($_from); ?>

			<br />
			<br />

			<input type="button" class="submit" value="Supprimer le paiement" onclick="javascript: submitForm('supprimer');" /><br />

			<br />
			<br />
			<br />

		</form>

	<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>