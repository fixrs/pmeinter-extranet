<?php /* Smarty version 2.6.13, created on 2014-06-02 22:08:57
         compiled from ratios/etape2.tpl */ ?>
<h2>Revenus</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n">200.&nbsp;</td>
		<td class="l">
			<label for="facturation">Facturation (F) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=<?php echo $this->_tpl_vars['annee']; ?>
&amp;dt=facturation#facturation');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m" width="100px">
			<input type="text" value="<?php echo $this->_tpl_vars['facturation']; ?>
" name="facturation" onchange="javascript: adjustNumericField('facturation', 1, 2, 0, 0); toggleMust('facturation');" id="facturation" class="argent" tabindex="1" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="n">201.&nbsp;</td>
		<td class="l">
			<label for="chiffreAffaire">Chiffre d'affaires (CA) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=<?php echo $this->_tpl_vars['annee']; ?>
&amp;dt=chiffreAffaires#chiffreAffaires');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['chiffreAffaire']; ?>
" name="chiffreAffaire" onchange="javascript: adjustNumericField('chiffreAffaire', 1, 2, 0, 0); toggleMust('chiffreAffaire');" id="chiffreAffaire" class="argent" tabindex="2" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">202.&nbsp;</td>
		<td class="l">
			<label for="deboursRefactures">D&eacute;bours &laquo;&nbsp;refactur&eacute;s&nbsp;&raquo; (DR) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=<?php echo $this->_tpl_vars['annee']; ?>
&amp;dt=deboursRefactures#deboursRefactures');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['deboursRefactures']; ?>
" name="deboursRefactures" onchange="javascript: adjustNumericField('deboursRefactures', 1, 2, 0, 0); toggleMust('deboursRefactures');" id="deboursRefactures" class="argent" tabindex="3" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">203.&nbsp;</td>
		<td class="l">
			<label for="beneficesNets">B&eacute;n&eacute;fices nets (BN) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=<?php echo $this->_tpl_vars['annee']; ?>
&amp;dt=beneficesNets#beneficesNets}');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['beneficesNets']; ?>
" name="beneficesNets" onchange="javascript: adjustNumericField('beneficesNets', 1, 2, 0, 1); toggleMust('beneficesNets');" id="beneficesNets" class="argent" tabindex="4" />
		</td>
		<td class="r">$</td>
	</tr>
</table>

<br clear="all" />
<br clear="all" />

<h2>D&eacute;penses</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n">204.&nbsp;</td>
		<td class="l">
			<label for="depensesTotales">D&eacute;penses totales&nbsp;:</label>
		</td>
		<td class="m" width="100px">
			<input type="text" value="<?php echo $this->_tpl_vars['depensesTotales']; ?>
" name="depensesTotales" onchange="javascript: adjustNumericField('depensesTotales', 1, 2, 0, 0); toggleMust('depensesTotales');" id="depensesTotales" class="argent" tabindex="5" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="n">205.&nbsp;</td>
		<td class="l">
			<label for="salaires">D&eacute;pense (s) - Salaires (S) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=<?php echo $this->_tpl_vars['annee']; ?>
&amp;dt=salaires#salaires');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaires']; ?>
" name="salaires" onchange="javascript: adjustNumericField('salaires', 1, 2, 0, 0); toggleMust('salaires');" id="salaires" class="argent" tabindex="6" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">206.&nbsp;</td>
		<td class="l">
			<label for="salairesColl">D&eacute;pense (s) - Salaires - collaborateur(rice)s (SC) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=<?php echo $this->_tpl_vars['annee']; ?>
&amp;dt=salairesCollaborateurs#salairesCollaborateurs');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salairesColl']; ?>
" name="salairesColl" onchange="javascript: adjustNumericField('salairesColl', 1, 2, 0, 0); toggleMust('salairesColl');" id="salairesColl" class="sous_total" tabindex="7" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">207.&nbsp;</td>
		<td class="l">
			<label for="salairesNotaires">D&eacute;pense (s) - Salaires - notaires (SN) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=<?php echo $this->_tpl_vars['annee']; ?>
&amp;dt=salairesNotaires#salairesNotaires');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salairesNotaires']; ?>
" name="salairesNotaires" onchange="javascript: adjustNumericField('salairesNotaires', 1, 2, 0, 0); toggleMust('salairesNotaires');" id="salairesNotaires" class="sous_total" tabindex="8" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">208.&nbsp;</td>
		<td class="l">
			<label for="depenseLoyer">D&eacute;pense (s) de loyer <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/definitions.php?dl=<?php echo $this->_tpl_vars['annee']; ?>
&amp;dt=loyer#loyer');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseLoyer']; ?>
" name="depenseLoyer" onchange="javascript: adjustNumericField('depenseLoyer', 1, 2, 0, 0); toggleMust('depenseLoyer');" id="depenseLoyer" class="argent" tabindex="9" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">209.&nbsp;</td>
		<td class="l">
			<label for="depenseDeboursesDossiers">D&eacute;pense (s) - d&eacute;bours de dossiers&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseDeboursesDossiers']; ?>
" name="depenseDeboursesDossiers" onchange="javascript: adjustNumericField('depenseDeboursesDossiers', 1, 2, 0, 0); toggleMust('depenseDeboursesDossiers');" id="depenseDeboursesDossiers" class="argent" tabindex="10" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">210.&nbsp;</td>
		<td class="l">
			<label for="depenseInteretsPretMarge">D&eacute;pense (s) - int&eacute;r&ecirc;ts sur pr&ecirc;ts et marge (s)&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseInteretsPretMarge']; ?>
" name="depenseInteretsPretMarge" onchange="javascript: adjustNumericField('depenseInteretsPretMarge', 1, 2, 0, 0); toggleMust('depenseInteretsPretMarge');" id="depenseInteretsPretMarge" class="argent" tabindex="11" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">211.&nbsp;</td>
		<td class="l">
			<label for="depenseAmortissement">D&eacute;pense (s) - amortissement&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseAmortissement']; ?>
" name="depenseAmortissement" onchange="javascript: adjustNumericField('depenseAmortissement', 1, 2, 0, 0); toggleMust('depenseAmortissement');" id="depenseAmortissement" class="argent" tabindex="12" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">212.&nbsp;</td>
		<td class="l">
			<label for="depenseTelecom">D&eacute;pense(s) t&eacute;l&eacute;communication (t&eacute;l&eacute;phonie, Internet, cellulaire)&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseTelecom']; ?>
" name="depenseTelecom" onchange="javascript: adjustNumericField('depenseTelecom', 1, 2, 0, 0); toggleMust('depenseTelecom');" id="depenseTelecom" class="argent" tabindex="13" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">213.&nbsp;</td>
		<td class="l">
			<label for="depenseSoutien">D&eacute;pense(s) soutien technique informatique (incluant contrat de service)&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseSoutien']; ?>
" name="depenseSoutien" onchange="javascript: adjustNumericField('depenseSoutien', 1, 2, 0, 0); toggleMust('depenseSoutien');" id="depenseSoutien" class="argent" tabindex="14" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">214.&nbsp;</td>
		<td class="l">
			<label for="depensePublicite">D&eacute;pense(s) publicit&eacute;&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depensePublicite']; ?>
" name="depensePublicite" onchange="javascript: adjustNumericField('depensePublicite', 1, 2, 0, 0); toggleMust('depensePublicite');" id="depensePublicite" class="argent" tabindex="14" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">215.&nbsp;</td>
		<td class="l">
			<label for="depenseRepresentation">D&eacute;pense(s) de repr&eacute;sentation&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseRepresentation']; ?>
" name="depenseRepresentation" onchange="javascript: adjustNumericField('depenseRepresentation', 1, 2, 0, 0); toggleMust('depenseRepresentation');" id="depenseRepresentation" class="argent" tabindex="14" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">216.&nbsp;</td>
		<td class="l">
			<label for="depenseAutres">Autres d&eacute;penses (R&eacute;sultat de la soustraction des d&eacute;penses totales inscrites au num&eacute;ro 204. moins les d&eacute;penses consign&eacute;es aux num&eacute;ros 206 &agrave; 215 (204 – (206 @ 215))&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseAutres']; ?>
" name="depenseAutres" onchange="javascript: adjustNumericField('depenseAutres', 1, 2, 0, 0); toggleMust('depenseAutres');" id="depenseAutres" class="argent" tabindex="15" />
		</td>
		<td class="r">$</td>
	</tr>
</table>