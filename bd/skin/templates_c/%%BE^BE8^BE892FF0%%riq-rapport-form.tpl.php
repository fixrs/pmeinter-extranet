<?php /* Smarty version 2.6.13, created on 2011-06-16 14:16:37
         compiled from facturation/riq-rapport-form.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['nologin'] != 1): ?>

	<?php echo '
	<script language="JavaScript">

	function submitForm(thisAction) {
		var errorMsg = \'\';

		var typeValue = getSelectValue(document.getElementById(\'type\'));
		var dateDebutValue = document.getElementById(\'date_debut\').value;
		var dateFinValue = document.getElementById(\'date_fin\').value;
		var taxeTpsValue = document.getElementById(\'taxe_tps\').value;
		var taxeTvqValue = document.getElementById(\'taxe_tvq\').value;
		
		if (typeValue == \'\' || typeValue == \'0\' || typeValue == \'undefined\') {
			errorMsg += \'Vous devez choisir un type de rapport.\\n\';
		}
		if (!isDate(dateDebutValue)) {
			errorMsg += \'Vous devez entrer une date de d\\351but valide.\\n\';
		}
		if (!isDate(dateFinValue)) {
			errorMsg += \'Vous devez entrer une date de fin valide.\\n\';
		}
		if (taxeTpsValue == \'\' || taxeTpsValue == \'0\' || taxeTpsValue == \'undefined\') {
			errorMsg += \'Vous devez sp\\351cifier la taxe TPS.\\n\';
		}
		if (taxeTvqValue == \'\' || taxeTvqValue == \'0\' || taxeTvqValue == \'undefined\') {
			errorMsg += \'Vous devez sp\\351cifier la taxe TVQ.\\n\';
		}

		if (errorMsg == \'\') {
			document.getElementById(\'rapportForm\').submit();
		} else {
			alert(errorMsg);
			return false;
		}
	}

	function isDate(strValue) {
		var objRegExp = /^\\d{4}-\\d{2}-\\d{2}$/
		if (objRegExp.test(strValue)) {
			return true;
		}
		return false;
	}

	</script>
	'; ?>


	<h1>Consultation de rapports des ristournes &agrave; verser</h1>

	<div class="noticeDiv">
		<?php $_from = $this->_tpl_vars['noticeArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['notices'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['notices']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['notice']):
        $this->_foreach['notices']['iteration']++;
?>
			<?php if (($this->_foreach['notices']['iteration'] <= 1)): ?><br /><?php endif; ?>
			<div class="noticeBox"><?php echo $this->_tpl_vars['notice']; ?>
</div>
		<?php endforeach; endif; unset($_from); ?>
	</div>
		<div class="errorDiv">
		<?php $_from = $this->_tpl_vars['errorArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['errors'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['errors']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['erreur']):
        $this->_foreach['errors']['iteration']++;
?>
			<?php if (($this->_foreach['errors']['iteration'] <= 1)): ?><br /><?php endif; ?>
			<div class="errorBox"><?php echo $this->_tpl_vars['erreur']; ?>
</div>
		<?php endforeach; endif; unset($_from); ?>
	</div>
	<br clear="all" />


	<form action="riq-rapport.php" method="post" id="rapportForm">
		<input type="hidden" name="action" id="action" value="display" />

		<label>Type de rapport&nbsp;:</label>
		<select name="type" id="type">
			<option selected="selected" value="0">- Choisir un type de rapport -</option>
			<option value="global">P&eacute;riode pour l'ensemble des &eacute;tudes</option>
		</select><br />

		<br />

		<label>Pour la p&eacute;riode&nbsp;:</label>
		<input type="text" name="date_debut" id="date_debut" onchange="javascript: adjustDateField('date_debut');" value="<?php echo $this->_tpl_vars['date_debut']; ?>
" /> au&nbsp;&nbsp;<input type="text" name="date_fin" id="date_fin" onchange="javascript: adjustDateField('date_fin');" value="<?php echo $this->_tpl_vars['date_fin']; ?>
" /><span class="note">AAAA-MM-JJ</span><br />

		<br />

		<label>Taxe TPS (%)&nbsp;:</label>
		<input type="text" name="taxe_tps" id="taxe_tps" onchange="javascript: adjustNumericField('taxe_tps', 1, 3, 0, 0);" value="5,000" /><br />

		<br />

		<label>Taxe TVQ (%)&nbsp;:</label>
		<input type="text" name="taxe_tvq" id="taxe_tvq" onchange="javascript: adjustNumericField('taxe_tvq', 1, 3, 0, 0);" value="8,500" /><br />
		
		<br />

		<label>Donn&eacute;es &agrave; afficher&nbsp;:</label>
		<input type="checkbox" name="cols[]" id="cols_honoraires_eligibles" class="radio" value="honoraires_eligibles" checked="checked" /> Honoraires &eacute;ligibles <br />
		<input type="checkbox" name="cols[]" id="cols_honoraires_non_eligibles" class="radio" value="honoraires_non_eligibles" checked="checked" /> Honoraires non &eacute;ligibles <br />
		<input type="checkbox" name="cols[]" id="cols_debourses" class="radio" value="debourses" checked="checked" /> D&eacute;bours&eacute;s <br />
		<input type="checkbox" name="cols[]" id="cols_ristourne_a_recevoir" class="radio" value="ristourne_a_recevoir" checked="checked" /> Ristourne &agrave; recevoir <br />
		<input type="checkbox" name="cols[]" id="cols_ristourne_a_verser" class="radio" value="ristourne_a_verser" checked="checked" /> Ristourne &agrave; verser <br />
		<input type="checkbox" name="rows[]" id="rows_paiements_recus" class="radio" value="paiements_recus" checked="checked" /> Paiements re&ccedil;us <br />
		<input type="checkbox" name="rows[]" id="rows_paiements_emis" class="radio" value="paiements_emis" checked="checked" /> Paiements &eacute;mis <br />

		<br />

		<input type="button" class="submit" value="Afficher le rapport" onclick="javascript: submitForm('display');" /><br />

	</form>

	<br />
	<br />
	<br />

<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
  