<?php /* Smarty version 2.6.13, created on 2011-03-10 00:02:59
         compiled from ratios/stats16.rh.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'ratios/stats16.rh.tpl', 11, false),array('function', 'math', 'ratios/stats16.rh.tpl', 72, false),)), $this); ?>
<!--<h2><?php echo $this->_tpl_vars['soustitre']; ?>
</h2>-->

<table class="stats stats16 <?php echo $this->_tpl_vars['nomAction']; ?>
" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<td class="description l"><span class="soustitre"><?php echo $this->_tpl_vars['soustitre']; ?>
</span></td>
			<?php $_from = $this->_tpl_vars['ratioz']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['ratiosTop'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['ratiosTop']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['ratio']):
        $this->_foreach['ratiosTop']['iteration']++;
?>
				<td class="nom"><?php echo ($this->_foreach['ratiosTop']['iteration']-1)+1; ?>
</td>
			<?php endforeach; endif; unset($_from); ?>
			<td class="moyenne">Total</td>
			<?php if (count($this->_tpl_vars['ratioz']) < 12): ?> <td class="tampon">&nbsp;</td> <?php endif; ?>
		</tr>
	</thead>
	<tbody>
		<?php $_from = $this->_tpl_vars['rowz']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['lesRows'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['lesRows']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['row']):
        $this->_foreach['lesRows']['iteration']++;
?>
			<tr>
				<?php if ($this->_tpl_vars['row']->description == ''): ?>
					<td class="description <?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
 l">&nbsp;</td>
				<?php else: ?>
					<td class="description <?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
 l"><?php echo $this->_tpl_vars['row']->description; ?>
</td>
				<?php endif; ?>
				<?php $this->assign('mesCells', $this->_tpl_vars['row']->data); ?>
				<?php $_from = $this->_tpl_vars['mesCells']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['lesCells'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['lesCells']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['uneCell']):
        $this->_foreach['lesCells']['iteration']++;
?>
					<?php if (($this->_foreach['lesCells']['iteration']-1) == 0): ?>
						<td class="valeur <?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
 l2">
					<?php else: ?>
						<td class="valeur <?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
">
					<?php endif; ?>

					<?php if ($this->_tpl_vars['row']->type == 'sectionName'): ?>
						&nbsp;
					<?php else: ?>
						<?php if ($this->_tpl_vars['row']->unit === 'bool'): ?>
							<?php if ($this->_tpl_vars['uneCell']): ?>
								Oui
							<?php else: ?>
								-
							<?php endif; ?>
						<?php elseif ($this->_tpl_vars['row']->unit === 'cad'): ?>
							<?php echo $this->_tpl_vars['row']->toFrenchFormatHtml($this->_tpl_vars['uneCell']); ?>
&nbsp;$
						<?php elseif ($this->_tpl_vars['row']->unit === 'percent'): ?>
							<?php echo $this->_tpl_vars['row']->toFrenchFormatHtml($this->_tpl_vars['uneCell']); ?>
&nbsp;%
						<?php else: ?>
							<?php if ($this->_tpl_vars['uneCell'] === null || $this->_tpl_vars['uneCell'] === false || $this->_tpl_vars['uneCell'] === ''): ?>
								-
							<?php else: ?>
								<?php echo $this->_tpl_vars['row']->toFrenchFormatHtml($this->_tpl_vars['uneCell']); ?>

							<?php endif; ?>
						<?php endif; ?>
					<?php endif; ?>
					</td>
					
					<?php if (($this->_foreach['lesRows']['iteration'] <= 1) && ($this->_foreach['lesCells']['iteration'] == $this->_foreach['lesCells']['total'])): ?>
						<?php $this->assign('honorairesTotal', $this->_tpl_vars['row']->getTotal()); ?>
					<?php endif; ?>
					
				<?php endforeach; endif; unset($_from); ?>
				<?php if (count($this->_tpl_vars['ratioz']) < 12): ?>
					<td class="valeur <?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
 total">
				<?php else: ?>
					<td class="valeur <?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
 total r">
				<?php endif; ?>
					<?php if ($this->_tpl_vars['row']->type == 'sectionName'): ?>
						&nbsp;
					<?php else: ?>
						<?php if ($this->_tpl_vars['row']->getTotal() === null): ?>
							-
						<?php elseif ($this->_tpl_vars['row']->unit === 'cad'): ?>
							<?php echo $this->_tpl_vars['row']->toFrenchFormatHtml($this->_tpl_vars['row']->getTotal()); ?>
&nbsp;$
							<?php $this->assign('sectionTotal', $this->_tpl_vars['row']->getTotal()); ?>
						<?php elseif ($this->_tpl_vars['row']->unit === 'percent'): ?>
								<?php echo smarty_function_math(array('assign' => 'totalPourcent','equation' => "y / x * 100",'x' => $this->_tpl_vars['honorairesTotal'],'y' => $this->_tpl_vars['sectionTotal']), $this);?>

								<?php echo $this->_tpl_vars['row']->toFrenchFormatHtml($this->_tpl_vars['totalPourcent']); ?>
&nbsp;%
						<?php elseif ($this->_tpl_vars['row']->unit === 'bool'): ?>
								<?php if ($this->_tpl_vars['row']->getTotal()): ?>
									Oui
								<?php else: ?>
									-
								<?php endif; ?>
						<?php else: ?>
							<?php echo $this->_tpl_vars['row']->toFrenchFormatHtml($this->_tpl_vars['row']->getTotal()); ?>

						<?php endif; ?>
					<?php endif; ?>
				</td>
				<?php if (count($this->_tpl_vars['ratioz']) < 12): ?><td class="<?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
 total tampon r">&nbsp;</td> <?php endif; ?>
			</tr>
		<?php endforeach; endif; unset($_from); ?>
	</tbody>
</table>