<?php /* Smarty version 2.6.13, created on 2009-04-08 10:09:46
         compiled from ratios/etapeFinale.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'ratios/etapeFinale.tpl', 40, false),)), $this); ?>
<?php if ($this->_tpl_vars['termine'] == '1'): ?>

	<div class="navigation_notes">Navigation : Vous pouvez naviguer parmi les champs en appuyant sur la touche &laquo; Tab &raquo; de votre clavier (ou Maj+Tab pour aller au champ pr&eacute;c&eacute;dent).</div>

	<div id="lblErrors" class="error"></div>

	<br clear="all" />

	<?php echo $this->_tpl_vars['success']; ?>


	<br clear="all" />

	<input id="button_a" class="submit" type="button" value="Rapport des ratios de gestion de l’&eacute;tude" onclick="javascript: alert('Cette fonctionnalit\351 ne sera pas disponible tant que les \351tudes du r\351seau n\'auront pas termin\351 la compilation de leurs donn\351es financi\350res. Veuillez r\351essayer plus tard.'); return false;" /><br />
	<input id="button_b" class="submit" type="button" value="Rapport cumulatif des ratios de gestion des &eacute;tudes du r&eacute;seau" onclick="javascript: alert('Cette fonctionnalit\351 ne sera pas disponible tant que les \351tudes du r\351seau n\'auront pas termin\351 la compilation de leurs donn\351es financi\350res. Veuillez r\351essayer plus tard.'); return false;" /><br />

<?php else: ?>

	<div id="etapes">&Eacute;tape 5/5</div>

	<br clear="all" />

	<div class="navigation_notes">Navigation : Vous pouvez naviguer parmi les champs en appuyant sur la touche &laquo; Tab &raquo; de votre clavier (ou Maj+Tab pour aller au champ pr&eacute;c&eacute;dent).</div>

	<div id="lblErrors" class="error"></div>
	
	<?php $_from = $this->_tpl_vars['errors']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['erreur']):
?>
		<?php echo $this->_tpl_vars['erreur']; ?>

	<?php endforeach; endif; unset($_from); ?>
	
	<br clear="all" />
	
	<?php $_from = $this->_tpl_vars['warnings']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['warning']):
?>
		<div class="warnings">
			<?php echo $this->_tpl_vars['warning']; ?>

		</div>
	<?php endforeach; endif; unset($_from); ?>
	
	<br clear="all" />
	
	<?php if (((is_array($_tmp=$this->_tpl_vars['errors'])) ? $this->_run_mod_handler('count', true, $_tmp) : count($_tmp)) == 0): ?>
		<?php if (((is_array($_tmp=$this->_tpl_vars['warnings'])) ? $this->_run_mod_handler('count', true, $_tmp) : count($_tmp)) > 0): ?>
			Le formulaire peut tout de m&ecirc;me &ecirc;tre signal&eacute; comme termin&eacute; malgr&eacute; les avertissements. Nous vous demandons simplement de v&eacute;rifier et d'&ecirc;tre certains des chiffres entr&eacute;s.<br />
		<?php else: ?>
			Le formulaire est pr&ecirc;t &agrave; &ecirc;tre signal&eacute; comme termin&eacute;.<br />
		<?php endif; ?>
	
		<br clear="all" />

		<input id="button_a" class="submit" type="button" value="Rapport des ratios de gestion de l’&eacute;tude" onclick="javascript: window.open('<?php echo $this->_tpl_vars['link_stats3']; ?>
', 'statistiques');" /><br />
		<input id="button_b" class="submit" type="button" value="Rapport cumulatif des ratios de gestion des &eacute;tudes du r&eacute;seau" onclick="javascript: window.open('<?php echo $this->_tpl_vars['link_stats16']; ?>
', 'statistiques');" /><br />

		<input type="hidden" name="complete4real" id="complete4real" value="0" />

		<input type="button" class="submit" value="Signaler le formulaire comme termin&eacute;" onclick="javascript: document.getElementById('complete4real').value='1'; changeAction('doComplete','action','ratiosForm');">

	
	<?php else: ?>
		&Agrave; cause d'&eacute;l&eacute;ments de validation non-conformes, vous ne pouvez signaler ce formulaire comme termin&eacute;. Veuillez revoir les &eacute;tapes 2 &agrave; 5 afin de corriger ces erreurs, puis revenez &agrave; l'&eacute;tape 5 afin de signaler le formulaire comme termin&eacute;.
	<?php endif;  endif; ?>