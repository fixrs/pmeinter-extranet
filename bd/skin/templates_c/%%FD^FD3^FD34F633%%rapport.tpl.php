<?php /* Smarty version 2.6.13, created on 2010-12-07 07:39:45
         compiled from reseauiq/rapport.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'toMoneyFormat', 'reseauiq/rapport.tpl', 49, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['nologin'] != 1): ?>

	<?php if ($this->_tpl_vars['noRight'] != 1): ?>

		<h2>Rapport de facturation</h2>
		
		<br />
		
		<?php if ($this->_tpl_vars['typeRapport'] == 'annuel_single'): ?>
			<strong>Cumulatif</strong>&nbsp;: Annuel finissant le <?php echo $this->_tpl_vars['dateFin']; ?>
<br />
			<br />
			<strong>&Eacute;tude</strong>&nbsp;: <?php echo $this->_tpl_vars['etudeNom']; ?>
<br />

		<?php elseif ($this->_tpl_vars['typeRapport'] == 'annuel_all'): ?>
			<strong>Cumulatif</strong>&nbsp;: Annuel finissant le <?php echo $this->_tpl_vars['dateFin']; ?>
<br />

		<?php elseif ($this->_tpl_vars['typeRapport'] == 'periode_single'): ?>
			<strong>Cumulatif</strong>&nbsp;: P&eacute;riode du <?php echo $this->_tpl_vars['dateDebut']; ?>
 au <?php echo $this->_tpl_vars['dateFin']; ?>
<br />
			<br />
			<strong>&Eacute;tude</strong>&nbsp;: <?php echo $this->_tpl_vars['etudeNom']; ?>
<br />

		<?php elseif ($this->_tpl_vars['typeRapport'] == 'periode_all'): ?>
			<strong>Cumulatif</strong>&nbsp;: P&eacute;riode du <?php echo $this->_tpl_vars['dateDebut']; ?>
 au <?php echo $this->_tpl_vars['dateFin']; ?>
<br />
		<?php endif; ?>
	
		<br />
		
		<?php if ($this->_tpl_vars['typeRapport'] == 'annuel_single' || $this->_tpl_vars['typeRapport'] == 'periode_single'): ?>

			<strong>Notaire responsable</strong>&nbsp;: <?php echo $this->_tpl_vars['notaireNom']; ?>
<br />
			
			<br />
			<br />

			<?php if ($this->_tpl_vars['tableauSommaireBool'] == 1): ?>

				<strong>Sommaire de la facturation&nbsp;:</strong> <br />
				
				<br />
				
				<table cellpadding="0" cellspacing="0" border="0" class="rapportSommaireForOne">
					<tr>
						<td width="200px" class="l">
							Montant des honoraires&nbsp;:
						</td>
						<td class="r">
							<?php echo ((is_array($_tmp=$this->_tpl_vars['factureSommaireForOneArray']['montant_honoraires_total'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>

						</td>
					</tr>
					<tr>
						<td class="l">
							Montant des d&eacute;bours&eacute;s&nbsp;:
						</td>
						<td class="r">
							<?php echo ((is_array($_tmp=$this->_tpl_vars['factureSommaireForOneArray']['montant_debourses_total'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>

						</td>
					</tr>
					<tr>
						<td class="l">
							Montant &eacute;ligible &agrave; la ristourne&nbsp;:
						</td>
						<td class="r">
							<?php echo ((is_array($_tmp=$this->_tpl_vars['factureSommaireForOneArray']['montant_ristourne_eligible_total'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>

						</td>
					</tr>
					<tr>
						<td class="l">
							Montant de la ristourne &agrave; verser&nbsp;:
						</td>
						<td class="r">
							<?php echo ((is_array($_tmp=$this->_tpl_vars['factureSommaireForOneArray']['montant_ristourne_total'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>

						</td>
					</tr>
				</table><br />
				
				<br />

			<?php endif; ?>
		
			<?php if ($this->_tpl_vars['tableauDetailsBool'] == 1): ?>

				<strong>D&eacute;tails des factures (<?php echo $this->_tpl_vars['nbFactures']; ?>
)&nbsp;:</strong><br />
				
				<br />

				<?php if ($this->_tpl_vars['nbFactures'] == 0): ?>

					Aucune facture enregistr&eacute;e pour cette p&eacute;riode.

				<?php else: ?>
			
					<?php $_from = $this->_tpl_vars['factureDetailsForOneArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['factureForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['factureForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['n'] => $this->_tpl_vars['i']):
        $this->_foreach['factureForeach']['iteration']++;
?>
						<table cellpadding="0" cellspacing="0" border="0" class="rapportDetails">
							<tr>
								<td class="l">
									Date de la facture&nbsp;:
								</td>
								<td class="r">
									<?php echo $this->_tpl_vars['i']['date_facture']; ?>

								</td>
							</tr>
							<tr>
								<td class="l">
									Num&eacute;ro de la facture&nbsp;:
								</td>
								<td class="r">
									<?php echo $this->_tpl_vars['i']['factureKey']; ?>

								</td>
							</tr>
							<tr>
								<td class="l">
									Num&eacute;ro du client&nbsp;:
								</td>
								<td class="r">
									<?php echo $this->_tpl_vars['i']['numero_client']; ?>

								</td>
							</tr>
							<tr>
								<td class="l">
									Montant des honoraires&nbsp;:
								</td>
								<td class="r">
									<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['montant_honoraires'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>

								</td>
							</tr>
							<tr>
								<td class="l">
									Montant des d&eacute;bours&eacute;s&nbsp;:
								</td>
								<td class="r">
									<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['montant_debourses'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>

								</td>
							</tr>
							<tr>
								<td class="l">
									Montant &eacute;ligible &agrave; la ristourne&nbsp;:
								</td>
								<td class="r">
									<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['montant_ristourne_eligible'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>

								</td>
							</tr>
							<tr>
								<td class="l">
									Montant de la ristourne &agrave; verser&nbsp;:
								</td>
								<td class="r">
									<?php echo ((is_array($_tmp=$this->_tpl_vars['i']['montant_ristourne'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>

								</td>
							</tr>
							<tr>
								<td class="l">
									Facture enregistr&eacute;e par&nbsp;:
								</td>
								<td class="r">
									<?php echo $this->_tpl_vars['i']['utilisateurKey']; ?>

								</td>
							</tr>						
						</table><br />
					<?php endforeach; endif; unset($_from); ?>
				<?php endif; ?>
			<?php endif; ?>
		
		<?php elseif ($this->_tpl_vars['typeRapport'] == 'annuel_all' || $this->_tpl_vars['typeRapport'] == 'periode_all'): ?>

			<br />

			<?php if ($this->_tpl_vars['tableauSommaireBool'] == 1): ?>

				<strong>Sommaire de la facturation&nbsp;:</strong> <br />
				
				<br />
				
					<table cellpadding="0" cellspacing="0" border="0" class="rapportSommaireForAll">
						<tr>
							<td class="titre" width="260px">Nom de l'&eacute;tude</td>
							<td class="titre" width="80px">Honoraires &eacute;ligibles</td>
							<td class="titre" width="80px">Honoraires non &eacute;ligibles</td>
							<td class="titre" width="80px">D&eacute;bours&eacute;s</td>
							<td class="titre" width="80px">Ristourne</td>
						</tr>

						<?php $_from = $this->_tpl_vars['factureSommaireForAllArray']['data']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['factureForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['factureForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['n'] => $this->_tpl_vars['i']):
        $this->_foreach['factureForeach']['iteration']++;
?>
						<?php if ((1 & ($this->_tpl_vars['j']++ / 1))): ?>
							<tr class="even">
						<?php else: ?>
							<tr class="odd">
						<?php endif; ?>
							<td class="etude"><?php echo $this->_tpl_vars['i']['nom']; ?>
</td>
							<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['montant_ristourne_eligible'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
							<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['montant_honoraires'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
							<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['montant_debourses'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
							<td><?php echo ((is_array($_tmp=$this->_tpl_vars['i']['montant_ristourne'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
						</tr>
						<?php endforeach; endif; unset($_from); ?>

						<tr>
							<th class="total">Total&nbsp;:</th>
							<td class="total"><?php echo ((is_array($_tmp=$this->_tpl_vars['factureSommaireForAllArray']['total']['montant_ristourne_eligible'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
							<td class="total"><?php echo ((is_array($_tmp=$this->_tpl_vars['factureSommaireForAllArray']['total']['montant_honoraires'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
							<td class="total"><?php echo ((is_array($_tmp=$this->_tpl_vars['factureSommaireForAllArray']['total']['montant_debourses'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
							<td class="total"><?php echo ((is_array($_tmp=$this->_tpl_vars['factureSommaireForAllArray']['total']['montant_ristourne'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
						</tr>
					</table>
			<?php endif; ?>

		<?php endif; ?>

	<?php else: ?>
		Vous ne disposez pas des droits pour effectuer cette op&eacute;ration.
	<?php endif; ?>

<?php else: ?>
	Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>