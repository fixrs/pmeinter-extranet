<?php /* Smarty version 2.6.13, created on 2011-01-14 14:18:01
         compiled from facturation/riq-paiement-ajout.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

	<?php echo $this->_tpl_vars['errors']; ?>


	<?php if ($this->_tpl_vars['nologin'] != 1): ?>

		<?php echo '
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = \'\';
			var action = document.getElementById(\'action\');

			switch (thisAction) {
				case \'ajouter\':
					var dateValue = document.getElementById(\'date\').value;
					if (!isDate(dateValue)) {
						errorMsg += \'La date n\\\'est pas valide.\\n\';
					}
					var montantPaiementValue = document.getElementById(\'montant_paiement\').value;
					if (trim(montantPaiementValue) == \'\') {
						errorMsg += \'Vous devez entrer le montant du paiement.\\n\';
					}
					action.value = \'ajouter\';
					break;
			}

			if (errorMsg == \'\') {
				document.getElementById(\'paiementForm\').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function isDate(strValue) {
			var objRegExp = /^\\d{4}-\\d{2}-\\d{2}$/
			if (objRegExp.test(strValue)) {
				return true;
			}
			return false;
		}
		
		</script>
		'; ?>


		<form action="riq-paiement.php" method="post" id="paiementForm">
			<input type="hidden" name="form" id="form" value="ajout" />
			<input type="hidden" name="action" id="action" value="" />
			<input type="hidden" name="paiementKey" id="paiementKey" value="" />			

			<h1>Ajout d'un paiement RIQ</h1>

			<div class="noticeDiv">
				<?php $_from = $this->_tpl_vars['noticeArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['notices'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['notices']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['notice']):
        $this->_foreach['notices']['iteration']++;
?>
					<?php if (($this->_foreach['notices']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="noticeBox"><?php echo $this->_tpl_vars['notice']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
				<div class="errorDiv">
				<?php $_from = $this->_tpl_vars['errorArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['errors'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['errors']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['erreur']):
        $this->_foreach['errors']['iteration']++;
?>
					<?php if (($this->_foreach['errors']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="errorBox"><?php echo $this->_tpl_vars['erreur']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
			<br clear="all" />

			<label>Date du paiement&nbsp;:</label>
			<input type="text" name="date" id="date" onchange="javascript: adjustDateField('date');" value="<?php echo $this->_tpl_vars['date']; ?>
" /> <span class="note">AAAA-MM-JJ</span><br />

			<br />
			
			<label for="montant_paiement">Montant du paiement&nbsp;:</label>
			<input type="text" name="montant_paiement" id="montant_paiement" onchange="javascript: adjustNumericField('montant_paiement', 1, 2, 0, 0);" value="<?php echo $this->_tpl_vars['montant_paiement']; ?>
" /><br />

			<br />

			<label for="numero_cheque">Num&eacute;ro de la facture (facultatif)&nbsp;:</label>
			<input type="text" name="numero_facture" id="numero_facture" value="<?php echo $this->_tpl_vars['numero_facture']; ?>
" /><br />

			<br />

			<label for="numero_cheque">Num&eacute;ro du ch&egrave;que (facultatif)&nbsp;:</label>
			<input type="text" name="numero_cheque" id="numero_cheque" value="<?php echo $this->_tpl_vars['numero_cheque']; ?>
" /><br />

			<br />

			<input type="button" class="submit" value="Ajouter le paiement" onclick="javascript: submitForm('ajouter');" /><br />

			<br />
			<br />
			<br />

		</form>

	<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>