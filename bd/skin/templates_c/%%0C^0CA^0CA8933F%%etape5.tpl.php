<?php /* Smarty version 2.6.13, created on 2013-04-16 08:28:59
         compiled from ratios/etape5.tpl */ ?>
<h2>Tarification</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n">500.&nbsp;</td>
		<td class="l">
			<label for="forfaitHonorairesResidentiel">Quel est votre forfait honoraires (sans frais et taxes) pour un pr&ecirc;t/vente r&eacute;sidentiel pour une propri&eacute;t&eacute; d'une valeur de 200&nbsp;000&nbsp;$ et moins&nbsp;?</label>
		</td>
		<td class="m" width="100px">
			<input type="text" value="<?php echo $this->_tpl_vars['forfaitHonorairesResidentiel']; ?>
" name="forfaitHonorairesResidentiel" onchange="javascript: adjustNumericField('forfaitHonorairesResidentiel', 1, 2, 0, 1);" id="forfaitHonorairesResidentiel" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>

	<tr>
		<td class="n">501.&nbsp;</td>
		<td class="l">
			<label for="forfaitHonorairesTestaments">Quel est votre forfait honoraires (sans frais et taxes) pour deux testaments et mandats (couple) simples&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['forfaitHonorairesTestaments']; ?>
" name="forfaitHonorairesTestaments" onchange="javascript: adjustNumericField('forfaitHonorairesTestaments', 1, 2, 0, 1);" id="forfaitHonorairesTestaments" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>

	<tr>
		<td class="n">502.&nbsp;</td>
		<td class="l">
			<label for="forfaitHonorairesCorporatif">Quel est votre forfait honoraires (sans frais et taxes) pour le service corporatif de base&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['forfaitHonorairesCorporatif']; ?>
" name="forfaitHonorairesCorporatif" onchange="javascript: adjustNumericField('forfaitHonorairesCorporatif', 1, 2, 0, 1);" id="forfaitHonorairesCorporatif" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>
	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td class="n">503.&nbsp;</td>
		<td class="l">
			<label for="forfaitHonorairesFeducie">Quel est votre forfait honoraire (sans frais et taxes) pour le service fiducie de base&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['forfaitHonorairesFeducie']; ?>
" name="forfaitHonorairesFeducie" onchange="javascript: adjustNumericField('forfaitHonorairesFeducie', 1, 2, 0, 1);" id="forfaitHonorairesFeducie" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>
	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td class="n">504.&nbsp;</td>
		<td class="l">
			<label for="forfaitHonorairesAG">Quel est votre forfait honoraire (sans frais et taxes) pour le service Ange Gardien de base&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['forfaitHonorairesAG']; ?>
" name="forfaitHonorairesAG" onchange="javascript: adjustNumericField('forfaitHonorairesAG', 1, 2, 0, 1);" id="forfaitHonorairesAG" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>
	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>
	<tr>
		<td class="n">505.&nbsp;</td>
		<td class="l">
			<label for="forfaitHonorairesMC">Quel est votre forfait honoraire (sans frais et taxes) pour le service Ma&icirc;tre des clefs&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['forfaitHonorairesMC']; ?>
" name="forfaitHonorairesMC" onchange="javascript: adjustNumericField('forfaitHonorairesMC', 1, 2, 0, 1);" id="forfaitHonorairesMC" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>

	<tr>
		<td class="n">506.&nbsp;</td>
		<td class="l">
			<label for="forfaitHonorairesHypotheque">Quel est votre forfait honoraires (sans frais et taxes) pour une quittance ou mainlev&eacute;e - hypoth&egrave;que d'un vendeur d'une propri&eacute;t&eacute; r&eacute;sidentiel&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['forfaitHonorairesHypotheque']; ?>
" name="forfaitHonorairesHypotheque" onchange="javascript: adjustNumericField('forfaitHonorairesHypotheque', 1, 2, 0, 1);" id="forfaitHonorairesHypotheque" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="spacer"><td colspan="4"><p>&nbsp;</p></td></tr>

	
	<tr>
		<td class="n">507.&nbsp;</td>
		<td class="l">
			<label for="forfaitHonorairesIncorporation">Quel est votre forfait honoraires (sans frais et taxes) pour une nouvelle incorporation&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['forfaitHonorairesIncorporation']; ?>
" name="forfaitHonorairesIncorporation" onchange="javascript: adjustNumericField('forfaitHonorairesIncorporation', 1, 2, 0, 1);" id="forfaitHonorairesIncorporation" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="spacer"><td colspan="4"><p>&nbsp;</p></td></tr>

	<tr>
		<td class="n">508.&nbsp;</td>
		<td class="l">
			<label for="forfaitHonorairesHMN">Quels sont vos honoraires estim&eacute;s (sans frais et taxes) pour un dossier d'homologation de mandat devant notaire&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['forfaitHonorairesHMN']; ?>
" name="forfaitHonorairesHMN" onchange="javascript: adjustNumericField('forfaitHonorairesHMN', 1, 2, 0, 1);" id="forfaitHonorairesHMN" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>
	
	<tr class="spacer"><td colspan="4"><p>&nbsp;</p></td></tr>

	<tr>
		<td class="n">509.&nbsp;</td>
		<td class="l">
			<label for="forfaitHonorairesRPM">Quels sont vos honoraires estim&eacute;s (sans frais et taxes) pour un dossier d'ouverture de r&eacute;gime de protection au majeur&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['forfaitHonorairesRPM']; ?>
" name="forfaitHonorairesRPM" onchange="javascript: adjustNumericField('forfaitHonorairesRPM', 1, 2, 0, 1);" id="forfaitHonorairesRPM" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>
	<tr class="spacer"><td colspan="4"><p>&nbsp;</p></td></tr>
	<tr>
		<td class="n">510.&nbsp;</td>
		<td class="l">
			<label for="forfaitHonorairesHTN">Quels sont vos honoraires estim&eacute;s (sans frais et taxes) pour un dossier d'homologation de testament devant notaire&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['forfaitHonorairesHTN']; ?>
" name="forfaitHonorairesHTN" onchange="javascript: adjustNumericField('forfaitHonorairesHTN', 1, 2, 0, 1);" id="forfaitHonorairesHTN" class="argent" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>

	<tr>
		<td colspan="4">
			<p style="width:508px;">
				Lorsque vous facturez &agrave; l'heure, quel est votre taux horaire moyen pour&nbsp;:
			</p>
		</td>
	</tr>

	<tr>
		<td class="n">511.&nbsp;</td>
		<td class="l">
			<label for="tauxHoraireNotaireMoinsDe5ans">Notaire de moins de 5 ans d'exp&eacute;rience&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['tauxHoraireNotaireMoinsDe5ans']; ?>
" name="tauxHoraireNotaireMoinsDe5ans" onchange="javascript: adjustNumericField('tauxHoraireNotaireMoinsDe5ans', 1, 2, 0, 1);" id="tauxHoraireNotaireMoinsDe5ans" class="argent" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">512.&nbsp;</td>
		<td class="l">
			<label for="tauxHoraireNotaire5a10ans">Notaire de 5 &agrave; 10 ans d'exp&eacute;rience&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['tauxHoraireNotaire5a10ans']; ?>
" name="tauxHoraireNotaire5a10ans" onchange="javascript: adjustNumericField('tauxHoraireNotaire5a10ans', 1, 2, 0, 1);" id="tauxHoraireNotaire5a10ans" class="argent" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">513.&nbsp;</td>
		<td class="l">
			<label for="tauxHoraireNotairePlusDe10ans">Notaire de plus de 10 ans d'exp&eacute;rience&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['tauxHoraireNotairePlusDe10ans']; ?>
" name="tauxHoraireNotairePlusDe10ans" onchange="javascript: adjustNumericField('tauxHoraireNotairePlusDe10ans', 1, 2, 0, 1);" id="tauxHoraireNotairePlusDe10ans" class="argent" />
		</td>
		<td class="r">$/heure</td>
	</tr>

	<tr>
		<td class="n">514.&nbsp;</td>
		<td class="l">
			<label for="tauxHoraireCollaborateur">Collaborateur(rices)s&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['tauxHoraireCollaborateur']; ?>
" name="tauxHoraireCollaborateur" onchange="javascript: adjustNumericField('tauxHoraireCollaborateur', 1, 2, 0, 1);" id="tauxHoraireCollaborateur" class="argent" />
		</td>
		<td class="r">$/heure</td>
	</tr>
</table>