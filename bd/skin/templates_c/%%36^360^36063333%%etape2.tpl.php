<?php /* Smarty version 2.6.13, created on 2009-04-21 11:40:51
         compiled from ratios/2007/etape2.tpl */ ?>
<h2>Revenus</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="l" width="400px">
			<label for="facturation">9.&nbsp;Facturation (F) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/<?php echo $this->_tpl_vars['annee']; ?>
/definitions.html#facturation');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['facturation']; ?>
" name="facturation" onchange="javascript: adjustNumericField('facturation', 1, 2, 0, 0); toggleMust('facturation');" id="facturation" class="argent" tabindex="1" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="chiffreAffaire">10.&nbsp;Chiffre d'affaires (CA) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/<?php echo $this->_tpl_vars['annee']; ?>
/definitions.html#chiffreAffaires');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['chiffreAffaire']; ?>
" name="chiffreAffaire" onchange="javascript: adjustNumericField('chiffreAffaire', 1, 2, 0, 0); toggleMust('chiffreAffaire');" id="chiffreAffaire" class="argent" tabindex="2" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="deboursRefactures">11.&nbsp;D&eacute;bours &laquo;&nbsp;refactur&eacute;s&nbsp;&raquo; (DR) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/<?php echo $this->_tpl_vars['annee']; ?>
/definitions.html#debours');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['deboursRefactures']; ?>
" name="deboursRefactures" onchange="javascript: adjustNumericField('deboursRefactures', 1, 2, 0, 0); toggleMust('deboursRefactures');" id="deboursRefactures" class="argent" tabindex="3" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="beneficesNets">12.&nbsp;B&eacute;n&eacute;fices nets (BN) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/<?php echo $this->_tpl_vars['annee']; ?>
/definitions.html#beneficesNets');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['beneficesNets']; ?>
" name="beneficesNets" onchange="javascript: adjustNumericField('beneficesNets', 1, 2, 0, 1); toggleMust('beneficesNets');" id="beneficesNets" class="argent" tabindex="4" />
		</td>
		<td class="r">$</td>
	</tr>
</table>

<br clear="all" />
<br clear="all" />

<h2>D&eacute;penses</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="l" width="400px">
			<label for="depensesTotales">13.&nbsp;D&eacute;penses totales&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depensesTotales']; ?>
" name="depensesTotales" onchange="javascript: adjustNumericField('depensesTotales', 1, 2, 0, 0); toggleMust('depensesTotales');" id="depensesTotales" class="argent" tabindex="5" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="salaires">14.&nbsp;D&eacute;pense (s) - Salaires (S) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/<?php echo $this->_tpl_vars['annee']; ?>
/definitions.html#salaires');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salaires']; ?>
" name="salaires" onchange="javascript: adjustNumericField('salaires', 1, 2, 0, 0); toggleMust('salaires');" id="salaires" class="argent" tabindex="6" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="salairesColl">15.&nbsp;D&eacute;pense (s) - Salaires - collaborateur (trice) s (SC) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/<?php echo $this->_tpl_vars['annee']; ?>
/definitions.html#salairesColl');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salairesColl']; ?>
" name="salairesColl" onchange="javascript: adjustNumericField('salairesColl', 1, 2, 0, 0); toggleMust('salairesColl');" id="salairesColl" class="sous_total" tabindex="7" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="salairesNotaires">16.&nbsp;D&eacute;pense (s) - Salaires - notaires (SN) <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/<?php echo $this->_tpl_vars['annee']; ?>
/definitions.html#salairesNot');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['salairesNotaires']; ?>
" name="salairesNotaires" onchange="javascript: adjustNumericField('salairesNotaires', 1, 2, 0, 0); toggleMust('salairesNotaires');" id="salairesNotaires" class="sous_total" tabindex="8" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="depenseLoyer">17.&nbsp;D&eacute;pense (s) de loyer <img src="<?php echo $this->_tpl_vars['BASEURL']; ?>
skin/img/question.gif" border="0" onclick="popupDef('<?php echo $this->_tpl_vars['BASEURL']; ?>
ratios/<?php echo $this->_tpl_vars['annee']; ?>
/definitions.html#loyer');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseLoyer']; ?>
" name="depenseLoyer" onchange="javascript: adjustNumericField('depenseLoyer', 1, 2, 0, 0); toggleMust('depenseLoyer');" id="depenseLoyer" class="argent" tabindex="9" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="depenseDeboursesDossiers">18.&nbsp;D&eacute;pense (s) - d&eacute;bours de dossiers&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseDeboursesDossiers']; ?>
" name="depenseDeboursesDossiers" onchange="javascript: adjustNumericField('depenseDeboursesDossiers', 1, 2, 0, 0); toggleMust('depenseDeboursesDossiers');" id="depenseDeboursesDossiers" class="argent" tabindex="10" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="depenseInteretsPretMarge">19.&nbsp;D&eacute;pense (s) - int&eacute;r&ecirc;ts sur pr&ecirc;ts et marge (s)&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseInteretsPretMarge']; ?>
" name="depenseInteretsPretMarge" onchange="javascript: adjustNumericField('depenseInteretsPretMarge', 1, 2, 0, 0); toggleMust('depenseInteretsPretMarge');" id="depenseInteretsPretMarge" class="argent" tabindex="11" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="depenseAmortissement">20.&nbsp;D&eacute;pense (s) - amortissement&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseAmortissement']; ?>
" name="depenseAmortissement" onchange="javascript: adjustNumericField('depenseAmortissement', 1, 2, 0, 0); toggleMust('depenseAmortissement');" id="depenseAmortissement" class="argent" tabindex="12" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="l">
			<label for="depenseAutres">21.&nbsp;Autres d&eacute;penses (excluant salaires, loyer, d&eacute;bours&eacute;s dossiers, int&eacute;r&ecirc;ts et amortissement)&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="<?php echo $this->_tpl_vars['depenseAutres']; ?>
" name="depenseAutres" onchange="javascript: adjustNumericField('depenseAutres', 1, 2, 0, 0); toggleMust('depenseAutres');" id="depenseAutres" class="argent" tabindex="13" />
		</td>
		<td class="r">$</td>
	</tr>
</table>