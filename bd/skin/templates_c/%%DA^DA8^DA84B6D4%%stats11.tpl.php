<?php /* Smarty version 2.6.13, created on 2010-07-20 16:06:39
         compiled from ratios/stats11.tpl */ ?>
<!--<h2><?php echo $this->_tpl_vars['soustitre']; ?>
</h2>-->

<table class="stats stats11 <?php echo $this->_tpl_vars['nomAction']; ?>
" cellpadding="0" cellspacing="0" style="<?php echo $this->_tpl_vars['pagebreak']; ?>
">
	<thead>
		<tr>
			<td class="description l"><span class="soustitre"><?php echo $this->_tpl_vars['soustitre']; ?>
</span></td>
			<?php $_from = $this->_tpl_vars['ratioz']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['ratiosTop'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['ratiosTop']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['ratio']):
        $this->_foreach['ratiosTop']['iteration']++;
?>
				<td class="nom"><?php echo $this->_tpl_vars['ratio']->annee; ?>
</td>
			<?php endforeach; endif; unset($_from); ?>
						<td class="tampon">&nbsp;</td>
		</tr>
	</thead>
		<tbody>
		<?php $_from = $this->_tpl_vars['rowz']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['row']):
?>
			<tr>
				<?php if ($this->_tpl_vars['row']->description == ''): ?>
					<td class="description <?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
 l">&nbsp;</td>
				<?php else: ?>
					<td class="description <?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
 l"><?php echo $this->_tpl_vars['row']->description; ?>
</td>
				<?php endif; ?>
				<?php $this->assign('mesCells', $this->_tpl_vars['row']->data); ?>
				<?php $_from = $this->_tpl_vars['mesCells']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['lesCells'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['lesCells']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['uneCell']):
        $this->_foreach['lesCells']['iteration']++;
?>
					<?php if (($this->_foreach['lesCells']['iteration']-1) == 0): ?>
						<td class="valeur <?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
 l2">
					<?php else: ?>
						<td class="valeur <?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
">
					<?php endif; ?>
						<?php if ($this->_tpl_vars['row']->type == 'sectionName'): ?>
							&nbsp;
						<?php else: ?>
							<?php if ($this->_tpl_vars['row']->unit === 'bool'): ?>
								<?php if ($this->_tpl_vars['uneCell']): ?>
									Oui
								<?php elseif ($this->_tpl_vars['uneCell'] === "" || $this->_tpl_vars['uneCell'] === NULL): ?>
									-
								<?php else: ?>
																		-
								<?php endif; ?>
							<?php elseif ($this->_tpl_vars['row']->unit === 'cad'): ?>
								<?php echo $this->_tpl_vars['row']->toFrenchFormatHtml($this->_tpl_vars['uneCell']); ?>
&nbsp;$
							<?php elseif ($this->_tpl_vars['row']->unit === 'percent'): ?>
								<?php echo $this->_tpl_vars['row']->toFrenchFormatHtml($this->_tpl_vars['uneCell']); ?>
&nbsp;%
							<?php else: ?>
								<?php if ($this->_tpl_vars['uneCell'] === null || $this->_tpl_vars['uneCell'] === false || $this->_tpl_vars['uneCell'] === ''): ?>
									-
								<?php else: ?>
									<?php echo $this->_tpl_vars['row']->toFrenchFormatHtml($this->_tpl_vars['uneCell']); ?>

								<?php endif; ?>
							<?php endif; ?>
						<?php endif; ?>
					</td>
				<?php endforeach; endif; unset($_from); ?>
								<td class="<?php echo $this->_tpl_vars['row']->type; ?>
 <?php echo $this->_tpl_vars['row']->type2; ?>
 <?php echo $this->_tpl_vars['row']->type3; ?>
 tampon r">&nbsp;</td>
			</tr>
		<?php endforeach; endif; unset($_from); ?>
	</tbody>
</table>