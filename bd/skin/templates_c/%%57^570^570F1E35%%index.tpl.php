<?php /* Smarty version 2.6.13, created on 2009-04-10 17:36:39
         compiled from ratios2/index.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php echo $this->_tpl_vars['errors']; ?>


<?php if ($this->_tpl_vars['nologin'] != 1): ?>

	<?php echo '
	<script language="JavaScript">

	function submitRatios() {
		var errorMsg = \'\';
		var action = \'start\';
		var etudeKey = document.getElementById(\'etudeField\').value;
		var annee = document.getElementById(\'anneeField\').value;

		if (etudeKey == \'\' || etudeKey == \'0\' || etudeKey == \'undefined\') {
			errorMsg += \'Vous devez choisir une étude.\\n\';
		}
		if (annee == \'\' || annee == \'0\' || annee == \'undefined\') {
			errorMsg += \'Vous devez choisir une année.\\n\';
		}

		if (errorMsg != \'\') {
			alert(errorMsg);
			return false;
		}

		var baseurl = '; ?>
'<?php echo $this->_tpl_vars['BASEURL']; ?>
';<?php echo '
		var url = baseurl + \'ratios2/ratios.php?action=\' + action + \'&etudeKey=\' + etudeKey + \'&annee=\' + annee;

		window.location = url;
	}

	var tableau = \'\';
	function submitStats() {
		var errorMsg = \'\';
		var action = \'custom\';
		var type = document.getElementById(\'type\').value;
		var etudeKey = document.getElementById(\'etudeKey2\').value;
		var annee = document.getElementById(\'annee\').value;
		var size = document.getElementById(\'size\').value;
		tableau = \'\';

		$(".tableauStats").each(function() {
			if ($(this).attr("checked") == true) {	
				tableau = tableau + $(this).val() + "-";
			}
		});

		if (type == \'\' || type == \'0\' || type == \'undefined\') {
			errorMsg += \'Vous devez spécifier un type de cumulatif\\n\';
		}
		if ((type == \'stats3\' || type == \'stats11\') && (etudeKey == \'\' || etudeKey == \'0\' || etudeKey == \'undefined\')) {
			errorMsg += \'Vous devez choisir une étude.\\n\';
		}
		if (annee == \'\' || annee == \'0\' || annee == \'undefined\') {
			errorMsg += \'Vous devez choisir une année.\\n\';
		}
		if (tableau == \'\'){
			errorMsg += \'Vous devez choisir un moins un tableau à afficher.\\n\';
		}

		if (errorMsg != \'\') {
			alert(errorMsg);
			return false;
		}

		var baseurl = '; ?>
'<?php echo $this->_tpl_vars['BASEURL']; ?>
';<?php echo '
		var url = baseurl + \'ratios2/stats.php?action=\' + action + \'&type=\' + type + \'&etudeKey=\' + etudeKey + \'&annee=\' + annee + \'&size=\' + size+ \'&tableau=\' + tableau;

		window.open(url, \'statistiques\');
	}

	function chkType() {
		typeElement = document.getElementById(\'type\');
		sizeElement = document.getElementById(\'size\');
		etudeElement = document.getElementById(\'etudeKey2\');
		autresElement = document.getElementById(\'autres\');

		adminBool = ';  echo $this->_tpl_vars['admin'];  echo ';
		stats16Bool = ';  echo $this->_tpl_vars['accesStats16'];  echo ';

		if (getSelectValue(typeElement) == \'stats16\') {
			sizeElement.disabled = false;
		} else {
			sizeElement.disabled = true;
		}

		if (getSelectValue(typeElement) == \'stats3\' || getSelectValue(typeElement) == \'stats11\') {
			etudeElement.disabled = false;
		} else {
			etudeElement.disabled = true;
		}

		if (adminBool) {
			autresElement.disabled = false;
		} else {
			if (getSelectValue(typeElement) == \'stats3\' || getSelectValue(typeElement) == \'stats11\') {
				autresElement.disabled = false;
			} else {
				autresElement.disabled = true;
			}
		}
	}

	function submitPrint() {
		var msg = \'Proc\\351dure pour imprimer les tableaux :\\n\\nL\\\'impression se fait \\340 partir du bouton \\253Consulter les donn\\351es\\273. Une fois la page des tableaux ouverte et active, cliquez sur le bouton d\\\'impression dans votre navigateur.\\n\\n- Si vous utilisez Internet Explorer :\\nVous devez imprimer UN tableau \\340 la fois en s\\351lectionnant les donn\\351es \\340 afficher.\\n\\n- Si vous utilisez Mozilla Firefox :\\nVous pouvez imprimer tous les tableaux d\\\'un seul coup.\\n\\n* Attention particuli\\350re :\\nDans les deux cas, vous devez vous assurer de r\\351gler votre impression\\nsur papier \\253FORMAT L\\311GAL\\273 (8.5x14) avec \\253ORIENTATION PAYSAGE\\273.\';

		alert(msg);
		return false;
	}

	</script>
	'; ?>


	<h1>Saisie de donn&eacute;es financi&egrave;res</h1>
	
	<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
	<select name="etudeKey" id="etudeField">
	<?php if ($this->_tpl_vars['admin']): ?>
		<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
		<?php $_from = $this->_tpl_vars['etudes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
			<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
		<?php endforeach; endif; unset($_from); ?>
	<?php else: ?>
				<?php $_from = $this->_tpl_vars['etudes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
			<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
		<?php endforeach; endif; unset($_from); ?>
	<?php endif; ?>
	</select>
	
	<br clear="all" />
	<br clear="all" />
	
	<label>Donn&eacute;es pour la p&eacute;riode financi&egrave;re se terminant en&nbsp;:</label>
	<select name="anneeField" id="anneeField">
		<option value="2005">2005</option>
		<option value="2006">2006</option>
		<option value="2007" selected="selected">2007</option>
		<option value="2008">2008</option>
		<option value="2009">2009</option>
		<option value="2010">2010</option>
	</select>
	
	<br clear="all"/>
	<br clear="all"/>
	
	<input type="button" class="submit" value="D&eacute;buter la saisie de donn&eacute;es" onclick="javascript: submitRatios();" />
	
	
	<br clear="all" />
	<br clear="all" />

	<a href="<?php echo $this->_tpl_vars['BASEURL']; ?>
docs/ratios/ratios_formulaire.pdf" title="Formulaire imprimable"><img src="<?php echo $this->_tpl_vars['SKINURL']; ?>
/img/printer.jpg" alt="formulaire imprimable" class="print" />Cliquez ici pour obtenir une version imprimable du formulaire vierge.</a>
	
	<br clear="all" />
	<br clear="all" />
	<br clear="all" />

	<h1>Consultation de ratios</h1>

	<label>Type de cumulatif&nbsp;:</label>

	<select name="type" id="type" onchange="javascript: chkType();">
		<option value="0">- Choisir le type de cumulatif -</option>
		<?php if ($this->_tpl_vars['admin'] || $this->_tpl_vars['accesStats16']): ?><option value="stats16">Cumulatif de 1 an pour l'ensemble des &eacute;tudes</option><?php endif; ?>
		<option value="stats11">Cumulatif de 1 an pour une &eacute;tude sp&eacute;cifique</option>
		<option value="stats3">Cumulatif de 3 ans pour une &eacute;tude sp&eacute;cifique</option>
	</select>

	<?php if (! $this->_tpl_vars['admin'] && ! $this->_tpl_vars['accesStats16']): ?>
		<br clear="all" />
		<br clear="all" />
		<span class="note">* Vous devez avoir compl&eacute;t&eacute; et signal&eacute; comme termin&eacute; le ratio de votre &eacute;tude pour avoir acc&egrave;s au comparatif de l'ensemble des &eacute;tudes.</span>
	<?php endif; ?>

	<br clear="all" />
	<br clear="all" />

	<?php if ($this->_tpl_vars['admin']): ?>
		<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
		<select name="etudeKey" id="etudeKey2" disabled="disabled">
			<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
			<?php $_from = $this->_tpl_vars['etudes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
				<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
		</select>
	<?php else: ?>
		<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
		<select name="etudeKey" id="etudeKey2" disabled="disabled">
			<?php $_from = $this->_tpl_vars['etudes']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
				<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
		</select>
	<?php endif; ?>

	<?php if ($this->_tpl_vars['admin']): ?>
		<br clear="all" />
		<br clear="all" />
		<label>Nombre de notaires&nbsp;:</label>
		<select name="size" id="size" disabled="disabled">
			<option selected="selected" value="0">- Choisir le nombre de notaires -</option>
			<option value="1-4">De 3 &agrave; 4 notaires</option>
			<option value="5-8">De 5 &agrave; 8 notaires</option>
			<option value="9-999">De 9 notaires et plus</option>
		</select>
	<?php else: ?>
		<br clear="all" />
		<br clear="all" />
		<label>Nombre de notaires&nbsp;:</label>
		<select name="size" id="size" disabled="disabled">
			<option selected="selected" value="0">- Choisir le nombre de notaires -</option>
			<option value="1-4">De 3 &agrave; 4 notaires</option>
			<option value="5-8">De 5 &agrave; 8 notaires</option>
			<option value="9-999">De 9 notaires et plus</option>
		</select>
	<?php endif; ?>

	<br clear="all" />
	<br clear="all" />

	
	<label>Donn&eacute;es pour la p&eacute;riode financi&egrave;re se terminant en&nbsp;:</label>
	<select name="annee" id="annee">
		<option value="2005">2005</option>
		<option value="2006">2006</option>
		<option value="2007" selected="selected">2007</option>
		<option value="2008">2008</option>
		<option value="2009">2009</option>
		<option value="2010">2010</option>
	</select>

	<br clear="all" />
	<br clear="all" />

	<label>Donn&eacute;es &agrave; afficher&nbsp;:</label>
	<input type="checkbox" id="donnees" value="donnees" name="tableau[]" checked="checked" class="tableauStats checkbox"/> <label for="donnees" style="display:inline;">Donn&eacute;es g&eacute;n&eacute;rales</label><br />
	<input type="checkbox" id="repartitionHonoraires" value="repartitionHonoraires" name="tableau[]" checked="checked" class="tableauStats checkbox"/> <label for="repartitionHonoraires" style="display:inline;">R&eacute;partition des honoraires</label><br />
	<input type="checkbox" id="rentabilite" value="rentabilite" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="rentabilite" style="display:inline;">Ratios de rentabilit&eacute; et de liquidit&eacute;</label><br />
	<input type="checkbox" id="recevableSalaires" value="recevableSalaires" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="recevableSalaires" style="display:inline;">Ratios des recevables et des salaires</label><br />
	<input type="checkbox" id="depenses" value="depenses" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="depenses" style="display:inline;">Ratios des d&eacute;penses</label><br />
	<input type="checkbox" id="production" value="production" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="production" style="display:inline;">Ratios de production</label><br />
	<input type="checkbox" id="autres" value="autres" disabled="disabled" name="tableau[]" class="tableauStats checkbox"/> <label for="autres" style="display:inline;">Autres statistiques</label><br />
	<input type="checkbox" id="statSalariales" value="statSalariales" name="tableau[]" checked="checked" class="tableauStats checkbox"/> <label for="statSalariales" style="display:inline;">Statistiques salariales</label><br />

	<br clear="all" />
	<br clear="all" />

	<input type="button" class="submit" value="Consulter les donn&eacute;es" onclick="javascript: submitStats();" /> &nbsp;&nbsp;&nbsp;&nbsp; <input type="button" class="submit" value="Imprimer les donn&eacute;es" onclick="javascript: submitPrint();" />
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>