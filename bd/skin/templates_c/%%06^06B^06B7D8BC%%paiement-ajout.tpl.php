<?php /* Smarty version 2.6.13, created on 2011-01-14 14:17:50
         compiled from facturation/paiement-ajout.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

	<?php echo $this->_tpl_vars['errors']; ?>


	<?php if ($this->_tpl_vars['nologin'] != 1): ?>

		<?php echo '
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = \'\';
			var action = document.getElementById(\'action\');
			var etudeKey = document.getElementById(\'etudeKey\');
			var factureKey = document.getElementById(\'factureKey\');

			switch (thisAction) {
				case \'ajouter\':
					var etudeKeyValue = getSelectValue(document.getElementById(\'etudeKeyField\'));
					if (etudeKeyValue == \'\' || etudeKeyValue == \'0\' || etudeKeyValue == \'undefined\') {
						errorMsg += \'Vous devez choisir une \\351tude.\\n\';
					}
					var factureKeyValue = getSelectValue(document.getElementById(\'factureKeyField_\' + etudeKeyValue));
					if (factureKeyValue == \'\' || factureKeyValue == \'0\' || factureKeyValue == \'undefined\' || factureKeyValue == null) {
						errorMsg += \'Vous devez choisir une facture.\\n\';
					}
					var dateDebutValue = document.getElementById(\'date_debut\').value;
					if (!isDate(dateDebutValue)) {
						errorMsg += \'La date de d\\351but n\\\'est pas valide.\\n\';
					}
					var dateFinValue = document.getElementById(\'date_fin\').value;
					if (!isDate(dateFinValue)) {
						errorMsg += \'La date de fin n\\\'est pas valide.\\n\';
					}
					var numeroChequeValue = document.getElementById(\'numero_cheque\').value;
					if (trim(numeroChequeValue) == \'\') {
						errorMsg += \'Vous devez entrer le num\\351ro du ch\\350que.\\n\';
					}
					var montantPaiementValue = document.getElementById(\'montant_paiement\').value;
					if (trim(montantPaiementValue) == \'\') {
						errorMsg += \'Vous devez entrer le montant du paiement.\\n\';
					}
					action.value = \'ajouter\';
					etudeKey.value = etudeKeyValue;
					factureKey.value = factureKeyValue;
					break;
			}

			if (errorMsg == \'\') {
				document.getElementById(\'paiementForm\').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function loadFactures() {
			var factureKeyFields = new Array();
			';  $_from = $this->_tpl_vars['facturesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['factureForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['factureForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['etudeKey'] => $this->_tpl_vars['null']):
        $this->_foreach['factureForeach']['iteration']++;
 echo '
				factureKeyFields[';  echo ($this->_foreach['factureForeach']['iteration']-1);  echo '] = \'factureKeyField_';  echo $this->_tpl_vars['etudeKey'];  echo '\';
			';  endforeach; endif; unset($_from);  echo '

			for (i = 0; i < factureKeyFields.length; i++) {
				document.getElementById(factureKeyFields[i]).style.display = \'none\';
				document.getElementById(factureKeyFields[i]).disabled = true;
			}

			var etudeKeyValue = getSelectValue(document.getElementById(\'etudeKeyField\'));
			var factureKeyField = document.getElementById(\'factureKeyField_\' + etudeKeyValue);
			var factureKeyFieldDefault = document.getElementById(\'factureKeyField_default\');

			if (
				etudeKeyValue != \'\' && etudeKeyValue != \'0\' && etudeKeyValue != \'undefined\' &&
				factureKeyField != \'\' && factureKeyField != \'undefined\' && factureKeyField != null
			) {
				factureKeyFieldDefault.style.display = \'none\';
				factureKeyField.style.display = \'inline\';
				factureKeyField.disabled = false;
			} else {
				factureKeyFieldDefault.style.display = \'inline\';
			}
		}

		function loadDates() {
			var etudeKeyValue = getSelectValue(document.getElementById(\'etudeKeyField\'));
			var factureKeyValue = getSelectValue(document.getElementById(\'factureKeyField_\' + etudeKeyValue));
			var dateDebutField = document.getElementById(\'date_debut\');
			var dateDebutArray = new Array();
			var dateFinField = document.getElementById(\'date_fin\');
			var dateFinArray = new Array();

			';  $_from = $this->_tpl_vars['datesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['datesForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['datesForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['factureClef'] => $this->_tpl_vars['dates']):
        $this->_foreach['datesForeach']['iteration']++;
 echo '
				dateDebutArray[\'';  echo $this->_tpl_vars['factureClef'];  echo '\'] = \'';  echo $this->_tpl_vars['dates']['date_debut'];  echo '\';
				dateFinArray[\'';  echo $this->_tpl_vars['factureClef'];  echo '\'] = \'';  echo $this->_tpl_vars['dates']['date_fin'];  echo '\';
			';  endforeach; endif; unset($_from);  echo '

			dateDebutField.value = dateDebutArray[factureKeyValue];
			dateFinField.value = dateFinArray[factureKeyValue];
		}

		function isDate(strValue) {
			var objRegExp = /^\\d{4}-\\d{2}-\\d{2}$/
			if (objRegExp.test(strValue)) {
				return true;
			}
			return false;
		}
		
		</script>
		'; ?>


		<form action="paiement.php" method="post" id="paiementForm">
			<input type="hidden" name="form" id="form" value="ajout" />
			<input type="hidden" name="action" id="action" value="" />
			<input type="hidden" name="etudeKey" id="etudeKey" value="" />
			<input type="hidden" name="factureKey" id="factureKey" value="" />
			<input type="hidden" name="paiementKey" id="paiementKey" value="" />			

			<h1>Ajout d'un paiement</h1>

			<div class="noticeDiv">
				<?php $_from = $this->_tpl_vars['noticeArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['notices'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['notices']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['notice']):
        $this->_foreach['notices']['iteration']++;
?>
					<?php if (($this->_foreach['notices']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="noticeBox"><?php echo $this->_tpl_vars['notice']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
				<div class="errorDiv">
				<?php $_from = $this->_tpl_vars['errorArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['errors'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['errors']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['erreur']):
        $this->_foreach['errors']['iteration']++;
?>
					<?php if (($this->_foreach['errors']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="errorBox"><?php echo $this->_tpl_vars['erreur']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
			<br clear="all" />

			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKeyField" id="etudeKeyField" onchange="javascript: loadFactures();">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				<?php $_from = $this->_tpl_vars['etudesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
					<?php if ($this->_tpl_vars['clef'] == $this->_tpl_vars['etudeKey2']): ?>
						<option value="<?php echo $this->_tpl_vars['clef']; ?>
" selected="selected"><?php echo $this->_tpl_vars['etude']; ?>
</option>
					<?php else: ?>
						<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
					<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
			</select><br />
	
			<br />

			<label>Num&eacute;ro de la facture (facultatif)&nbsp;:</label>

			<select name="factureKeyField_default" id="factureKeyField_default" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
			</select>

			<?php $_from = $this->_tpl_vars['facturesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['etudeClef'] => $this->_tpl_vars['null']):
?>
			<select name="factureKeyField_<?php echo $this->_tpl_vars['etudeClef']; ?>
" id="factureKeyField_<?php echo $this->_tpl_vars['etudeClef']; ?>
" style="display:none;" onchange="javascript: loadDates(this);" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
				<?php $_from = $this->_tpl_vars['facturesArray'][$this->_tpl_vars['etudeClef']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['factureClef'] => $this->_tpl_vars['factureInfo']):
?>
					<option value="<?php echo $this->_tpl_vars['factureClef']; ?>
"><?php echo $this->_tpl_vars['factureInfo']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
			<?php endforeach; endif; unset($_from); ?>

			<br />
			<br />
			
			<label>P&eacute;riode&nbsp;:</label>
			<input type="text" name="date_debut" id="date_debut" onchange="javascript: adjustDateField('date_debut');" value="<?php echo $this->_tpl_vars['date_debut']; ?>
" readonly="readonly" /> au&nbsp;&nbsp;<input type="text" name="date_fin" id="date_fin" onchange="javascript: adjustDateField('date_fin');" value="<?php echo $this->_tpl_vars['date_fin']; ?>
" readonly="readonly" /><br />

			<br />
			
			<label for="numero_cheque">Num&eacute;ro du ch&egrave;que&nbsp;:</label>
			<input type="text" name="numero_cheque" id="numero_cheque" value="<?php echo $this->_tpl_vars['numero_cheque']; ?>
" /><br />

			<br />
			
			<label for="montant_paiement">Montant du paiement&nbsp;:</label>
			<input type="text" name="montant_paiement" id="montant_paiement" onchange="javascript: adjustNumericField('montant_paiement', 1, 2, 0, 0);" value="<?php echo $this->_tpl_vars['montant_paiement']; ?>
" /><br />

			<br />

			<input type="button" class="submit" value="Ajouter le paiement" onclick="javascript: submitForm('ajouter');" /><br />

			<br />
			<br />
			<br />

		</form>

	<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>