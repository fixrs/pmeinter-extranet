<?php /* Smarty version 2.6.13, created on 2009-04-21 11:50:19
         compiled from ratios/2007/stats.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><?php echo $this->_tpl_vars['title']; ?>
</title>
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SKIN_URL']; ?>
/css/stats.css" type="text/css" media="screen"/>
		<link rel="stylesheet" href="<?php echo $this->_tpl_vars['SKIN_URL']; ?>
/css/print-stats.css" media="print" type="text/css" />
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
		<script type="text/javascript" src="<?php echo $this->_tpl_vars['SKIN_URL']; ?>
/js/jquery-1.3.2.min.js"></script>
	</head>

	<body>

		<?php echo '
		<script language="JavaScript">

		var minFontSize = 6;
		var maxFontSize = 24;
		var defFontSize = ';  echo $this->_tpl_vars['DEFAULT_FONT_SIZE'];  echo ';
		var fontClasses = new Array(';  $_from = $this->_tpl_vars['FONT_CLASSES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['fontClassList'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['fontClassList']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['font_class']):
        $this->_foreach['fontClassList']['iteration']++;
?>'<?php echo $this->_tpl_vars['font_class']; ?>
'<?php if (! ($this->_foreach['fontClassList']['iteration'] == $this->_foreach['fontClassList']['total'])): ?>,<?php endif;  endforeach; endif; unset($_from);  echo ');


		var minColWidth = 60;
		var maxColWidth = 600;
		var defColWidth = ';  echo $this->_tpl_vars['DEFAULT_COL_WIDTH'];  echo ';
		var colClasses = new Array(';  $_from = $this->_tpl_vars['COL_CLASSES']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['colClassList'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['colClassList']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['col_class']):
        $this->_foreach['colClassList']['iteration']++;
?>'<?php echo $this->_tpl_vars['col_class']; ?>
'<?php if (! ($this->_foreach['colClassList']['iteration'] == $this->_foreach['colClassList']['total'])): ?>,<?php endif;  endforeach; endif; unset($_from);  echo ');

		function defaultFontSize() {
			for (var i in fontClasses) {
				var text = $(fontClasses[i]);
				var unit = \'pt\';
				var num = parseFloat(defFontSize, 10);
				text.css(\'font-size\', num + unit);
			}
		}

		function increaseFontSize() {
			for (var i in fontClasses) {
				var text = $(fontClasses[i]);
				var size = text.css(\'font-size\');
				var unit = size.slice(-2);
				var num = parseFloat(size, 10);
				var newSize = num + 1;
				if (newSize < maxFontSize) {
					text.css(\'font-size\', newSize + unit);
				}
			}
		}

		function decreaseFontSize() {
			for (var i in fontClasses) {
				var text = $(fontClasses[i]);
				var size = text.css(\'font-size\');
				var unit = size.slice(-2);
				var num = parseFloat(size, 10);
				var newSize = num - 1;
				if (newSize > minFontSize) {
					text.css(\'font-size\', newSize + unit);
				}
			}
		}

		function defaultColWidth() {
			for (var i in colClasses) {
				var col = $(colClasses[i]);
				var unit = \'px\';
				var num = parseFloat(defColWidth, 10);
				col.css(\'width\', num + unit);
			}
		}

		function increaseColWidth() {
			for (var i in colClasses) {
				var col = $(colClasses[i]);
				var width = col.css(\'width\');
				var unit = width.slice(-2);
				var num = parseFloat(width, 10);
				var newWidth = num + 5;
				if (newWidth < maxColWidth) {
					col.css(\'width\', newWidth + unit);
				}
			}
		}

		function decreaseColWidth() {
			for (var i in colClasses) {
				var col = $(colClasses[i]);
				var width = col.css(\'width\');
				var unit = width.slice(-2);
				var num = parseFloat(width, 10);
				var newWidth = num - 5;
				if (newWidth > minColWidth) {
					col.css(\'width\', newWidth + unit);
				}
			}
		}

		function printNow() {
			var msg = \'Assurez-vous de r\\351gler votre impression sur papier \\253FORMAT L\\311GAL\\273 (8.5x14) avec \\253ORIENTATION PAYSAGE\\273.\';
			alert(msg);
			window.print();
		}

		</script>
		'; ?>


		<div id="printbar">
			<span style="float: left;">
				&nbsp;&nbsp;&nbsp;&nbsp;
				Ajuster la taille du texte&nbsp;:
				<input name="sizedown" id="sizedown" type="button" class="minus" onclick="javascript: decreaseFontSize();" value="-" />&nbsp;
				<input name="sizeup" id="sizeup" type="button" class="plus" onclick="javascript: increaseFontSize();" value="+" />&nbsp;
				<input name="sizedefault" id="sizedefault" type="button" class="reset" onclick="javascript: defaultFontSize();" value="R&eacute;initialiser" />&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Ajuster la largeur des colonnes&nbsp;:
				<input name="widthdown" id="widthdown" type="button" class="minus" onclick="javascript: decreaseColWidth();" value="-" />&nbsp;
				<input name="widthup" id="widthup" type="button" class="plus" onclick="javascript: increaseColWidth();" value="+" />&nbsp;
				<input name="widthdefault" id="widthdefault" type="button" class="reset" onclick="javascript: defaultColWidth();" value="R&eacute;initialiser" />&nbsp;
			</span>
			<span style="float: right;">
				<input name="printnow" id="printnow" type="button" class="printnow" onclick="javascript: printNow();" value="Imprimer" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</span>
		</div>

		<br  clear="all" />

		<div id="legalpaper">

			<?php $_from = $this->_tpl_vars['actions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['actionList'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['actionList']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['nomAction'] => $this->_tpl_vars['action']):
        $this->_foreach['actionList']['iteration']++;
?>

				<?php if (( $this->_tpl_vars['nomAction'] == 'statsalariales_a' || $this->_tpl_vars['nomAction'] == 'statsalariales_b' ) && $this->_tpl_vars['type'] != 'stats16'): ?>
					<h1><?php echo $this->_tpl_vars['titre_ss']; ?>
</h1>
				<?php else: ?>
					<h1><?php echo $this->_tpl_vars['titre']; ?>
</h1>
				<?php endif; ?>

				<?php if ($this->_tpl_vars['nomAction'] == 'autres_a' || $this->_tpl_vars['nomAction'] == 'autres_b' || $this->_tpl_vars['nomAction'] == 'autres_c'): ?>
					<?php if ($this->_tpl_vars['admin'] || $this->_tpl_vars['type'] == 'stats3' || $this->_tpl_vars['type'] == 'stats11'): ?>
						<?php $this->assign('ratioz', $this->_tpl_vars['ratios'][$this->_tpl_vars['nomAction']]); ?>
						<?php $this->assign('rowz', $this->_tpl_vars['rows'][$this->_tpl_vars['nomAction']]); ?>
						<?php $this->assign('soustitre', $this->_tpl_vars['soustitres'][$this->_tpl_vars['nomAction']]); ?>
						<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "ratios/".($this->_tpl_vars['annee'])."/".($this->_tpl_vars['type']).".tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
					<?php endif; ?>
				<?php else: ?>
					<?php $this->assign('ratioz', $this->_tpl_vars['ratios'][$this->_tpl_vars['nomAction']]); ?>
					<?php $this->assign('rowz', $this->_tpl_vars['rows'][$this->_tpl_vars['nomAction']]); ?>
					<?php $this->assign('soustitre', $this->_tpl_vars['soustitres'][$this->_tpl_vars['nomAction']]); ?>
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "ratios/".($this->_tpl_vars['annee'])."/".($this->_tpl_vars['type']).".tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				<?php endif; ?>

				<?php if ($this->_tpl_vars['type'] == 'stats16'): ?>
					<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "ratios/".($this->_tpl_vars['annee'])."/statsLegende.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
				<?php endif; ?>

				<?php if (! ($this->_foreach['actionList']['iteration'] == $this->_foreach['actionList']['total'])): ?>
					<hr class="pagebreak" />
				<?php endif; ?>
			<?php endforeach; endif; unset($_from); ?>
		</div>
	</body>
</html>
