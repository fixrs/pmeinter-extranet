<?php /* Smarty version 2.6.13, created on 2010-12-09 11:42:28
         compiled from reseauiq/reseauiq.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['nologin'] != 1): ?>

	<?php if ($this->_tpl_vars['noRight'] != 1): ?>

		<h2>Ajout d'une facture</h2>
	
		<div id="reseauiq">
			<div id="infosEtude"><?php echo $this->_tpl_vars['etudeNom']; ?>
</div>
	
			<br clear="all" />
	
			<div class="noticeDiv">
				<?php $_from = $this->_tpl_vars['noticeArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['notices'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['notices']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['notice']):
        $this->_foreach['notices']['iteration']++;
?>
					<?php if (($this->_foreach['notices']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="noticeBox"><?php echo $this->_tpl_vars['notice']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
	
			<div class="errorDiv">
				<?php $_from = $this->_tpl_vars['errorArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['errors'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['errors']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['erreur']):
        $this->_foreach['errors']['iteration']++;
?>
					<?php if (($this->_foreach['errors']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="errorBox"><?php echo $this->_tpl_vars['erreur']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
	
			<br clear="all" />

			<?php echo $this->_tpl_vars['link']; ?>

	
			<?php if ($this->_tpl_vars['noForm'] != 1): ?>
				<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "reseauiq/form.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
			<?php endif; ?>
	
		</div>

	<?php else: ?>
		Vous ne disposez pas des droits pour effectuer cette op&eacute;ration.
	<?php endif; ?>

<?php else: ?>
	Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.
<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>