<?php /* Smarty version 2.6.13, created on 2010-11-18 16:22:09
         compiled from facturation/rapport-bilan-global.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'toMoneyFormat', 'facturation/rapport-bilan-global.tpl', 29, false),array('modifier', 'toFloat', 'facturation/rapport-bilan-global.tpl', 37, false),array('modifier', 'truncate', 'facturation/rapport-bilan-global.tpl', 43, false),array('function', 'math', 'facturation/rapport-bilan-global.tpl', 37, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['nologin'] != 1): ?>

	<h2>Bilan transactionnel pour l'ensemble des &eacute;tudes</h2>

	<strong>P&eacute;riode</strong>&nbsp;: <?php echo $this->_tpl_vars['dateDebut']; ?>
 au <?php echo $this->_tpl_vars['dateFin']; ?>
<br />

	<br />
			
	<table cellpadding="0" cellspacing="0" border="0" class="bilan_global">
		<tr>
			<th style="text-align:left;">Date</th>
			<th style="text-align:left;">Nom de l'&eacute;tude&nbsp;</th>
			<th style="text-align:left;"># de facture&nbsp;</th>
			<th style="text-align:left;"># du ch&egrave;que&nbsp;</th>
			<th style="text-align:right;">Paiement&nbsp;</th>
			<th style="text-align:right;">Montant d&ucirc;&nbsp;</th>
			<th style="text-align:right;">Balance&nbsp;</th>
		</tr>

		<tr>
			<td style="text-align:left;">&nbsp;</td>
			<td style="text-align:left;">&nbsp;</td>
			<td style="text-align:left;">&nbsp;</td>
			<td style="text-align:left;">&nbsp;</td>
			<td style="text-align:right;">&nbsp;</td>
			<td style="text-align:right;">&nbsp;</td>
			<td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['initialBalance'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
		</tr>

		<?php $_from = $this->_tpl_vars['entriesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['entriesForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['entriesForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['n'] => $this->_tpl_vars['entry']):
        $this->_foreach['entriesForeach']['iteration']++;
?>
		<?php if (($this->_foreach['entriesForeach']['iteration'] <= 1)): ?>
			<?php $this->assign('balance', $this->_tpl_vars['initialBalance']); ?>
		<?php endif; ?>
		<?php if ($this->_tpl_vars['entry']['type'] == 'paiement'): ?>
			<?php echo smarty_function_math(array('equation' => "x + y",'x' => $this->_tpl_vars['balance'],'y' => ((is_array($_tmp=$this->_tpl_vars['entry']['montant_paiement'])) ? $this->_run_mod_handler('toFloat', true, $_tmp) : toFloat($_tmp)),'assign' => 'balance'), $this);?>

		<?php else: ?>
			<?php echo smarty_function_math(array('equation' => "x - y",'x' => $this->_tpl_vars['balance'],'y' => ((is_array($_tmp=$this->_tpl_vars['entry']['montant_facture'])) ? $this->_run_mod_handler('toFloat', true, $_tmp) : toFloat($_tmp)),'assign' => 'balance'), $this);?>

		<?php endif; ?>
		<tr class="<?php echo $this->_tpl_vars['entry']['type']; ?>
">
			<td style="text-align:left;"><?php echo $this->_tpl_vars['entry']['date']; ?>
</td>
			<td style="text-align:left;"><?php echo ((is_array($_tmp=$this->_tpl_vars['entry']['etude_nom'])) ? $this->_run_mod_handler('truncate', true, $_tmp, 30, '...', 1) : smarty_modifier_truncate($_tmp, 30, '...', 1)); ?>
</td>
			<?php if ($this->_tpl_vars['entry']['facture_key']): ?>
				<td style="text-align:left;"><?php echo $this->_tpl_vars['entry']['facture_key']; ?>
</td>
			<?php else: ?>
				<td style="text-align:left;">&nbsp;</td>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['entry']['numero_cheque']): ?>
				<td style="text-align:left;"><?php echo $this->_tpl_vars['entry']['numero_cheque']; ?>
</td>
			<?php else: ?>
				<td style="text-align:left;">&nbsp;</td>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['entry']['montant_paiement']): ?>
				<td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['entry']['montant_paiement'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
			<?php else: ?>
				<td style="text-align:right;">&nbsp;</td>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['entry']['montant_facture']): ?>
				<td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['entry']['montant_facture'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
			<?php else: ?>
				<td style="text-align:right;">&nbsp;</td>
			<?php endif; ?>
			<td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['balance'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
		</tr>
		<?php endforeach; endif; unset($_from); ?>

	</table><br />

<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>