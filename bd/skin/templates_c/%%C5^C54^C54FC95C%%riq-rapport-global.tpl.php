<?php /* Smarty version 2.6.13, created on 2011-05-19 00:03:42
         compiled from facturation/riq-rapport-global.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'toMoneyFormat', 'facturation/riq-rapport-global.tpl', 36, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['nologin'] != 1): ?>

	<center>
		<h1>Rapport des ristournes &agrave; verser</h1>
		<strong><?php echo $this->_tpl_vars['dateDebut']; ?>
</strong> au <strong><?php echo $this->_tpl_vars['dateFin']; ?>
</strong><br />
	</center>

	<div id="printpage"><a href="javascript: window.print();"><img class="print" src="<?php echo $this->_tpl_vars['SKIN_URL']; ?>
/img/printer.jpg" alt="Imprimer" /> Imprimer le rapport</a></div>

	<br />
	<br />

	<?php $this->assign('ln', '0'); ?>
	
	<table cellpadding="0" cellspacing="0" border="0" class="rapport_global">

		<tr class="titre">
			<td width="260px" style="text-align:left;">Nom de l'&eacute;tude</td>
			<?php if ($this->_tpl_vars['cols']['honoraires_eligibles']): ?><td width="80px" style="text-align:center;">Honoraires &eacute;ligibles</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['honoraires_non_eligibles']): ?><td width="80px" style="text-align:center;">Honoraires non &eacute;ligibles</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['debourses']): ?><td width="80px" style="text-align:center;">D&eacute;bours&eacute;s</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_recevoir']): ?><td width="80px" style="text-align:center;">Ristourne &agrave; recevoir</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_verser']): ?><td width="80px" style="text-align:center;">Ristourne &agrave; verser</td><?php endif; ?>
		</tr>

		<?php $_from = $this->_tpl_vars['entriesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['entriesForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['entriesForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['etudeKey'] => $this->_tpl_vars['etude']):
        $this->_foreach['entriesForeach']['iteration']++;
?>

			<?php if ((1 & ($this->_tpl_vars['j']++ / 1))): ?>
				<tr class="etude even">
			<?php else: ?>
				<tr class="etude odd">
			<?php endif; ?>
				<td style="text-align:left;"><?php echo $this->_tpl_vars['etude']['etude_nom']; ?>
</td>
				<?php if ($this->_tpl_vars['cols']['honoraires_eligibles']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['etude']['honoraires_eligibles'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
				<?php if ($this->_tpl_vars['cols']['honoraires_non_eligibles']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['etude']['honoraires_non_eligibles'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
				<?php if ($this->_tpl_vars['cols']['debourses']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['etude']['debourses'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
				<?php if ($this->_tpl_vars['cols']['ristourne_a_recevoir']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['etude']['ristourne_a_recevoir'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>				
				<?php if ($this->_tpl_vars['cols']['ristourne_a_verser']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['etude']['ristourne_a_verser'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			</tr>

		<?php endforeach; endif; unset($_from); ?>

		<tr class="total_ristournes">
			<td style="text-align:left;">Sous-Total&nbsp;:</td>
			<?php if ($this->_tpl_vars['cols']['honoraires_eligibles']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['sousTotalArray']['honoraires_eligibles'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['honoraires_non_eligibles']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['sousTotalArray']['honoraires_non_eligibles'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['debourses']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['sousTotalArray']['debourses'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_recevoir']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['sousTotalArray']['ristourne_a_recevoir'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_verser']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['sousTotalArray']['ristourne_a_verser'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
		</tr>

		<tr class="total_ristournes">
			<td style="text-align:left;">TPS (&nbsp;<?php echo $this->_tpl_vars['taxeTPS']; ?>
%&nbsp;)&nbsp;</td>
			<?php if ($this->_tpl_vars['cols']['honoraires_eligibles']): ?><td style="text-align:right;">&nbsp;</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['honoraires_non_eligibles']): ?><td style="text-align:right;">&nbsp;</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['debourses']): ?><td style="text-align:right;">&nbsp;</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_recevoir']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['taxesArray']['TPS']['ristourne_a_recevoir'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_verser']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['taxesArray']['TPS']['ristourne_a_verser'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
		</tr>

		<tr class="total_ristournes">
			<td style="text-align:left;">TVQ (&nbsp;<?php echo $this->_tpl_vars['taxeTVQ']; ?>
%&nbsp;)&nbsp;</td>
			<?php if ($this->_tpl_vars['cols']['honoraires_eligibles']): ?><td style="text-align:right;">&nbsp;</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['honoraires_non_eligibles']): ?><td style="text-align:right;">&nbsp;</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['debourses']): ?><td style="text-align:right;">&nbsp;</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_recevoir']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['taxesArray']['TVQ']['ristourne_a_recevoir'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_verser']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['taxesArray']['TVQ']['ristourne_a_verser'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
		</tr>

		<tr class="total_ristournes">
			<td style="text-align:left;">Total&nbsp;:</td>
			<?php if ($this->_tpl_vars['cols']['honoraires_eligibles']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['sousTotalArray']['honoraires_eligibles'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['honoraires_non_eligibles']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['sousTotalArray']['honoraires_non_eligibles'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['debourses']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['sousTotalArray']['debourses'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_recevoir']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['totalArray']['ristourne_a_recevoir'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_verser']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['totalArray']['ristourne_a_verser'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
		</tr>

		<?php if ($this->_tpl_vars['rows']['paiements_recus']): ?>
		<?php $_from = $this->_tpl_vars['paiementsRecusArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['paiementsRecusForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['paiementsRecusForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['p'] => $this->_tpl_vars['paiement']):
        $this->_foreach['paiementsRecusForeach']['iteration']++;
?>

			<tr class="paiement">
				<td colspan="<?php echo $this->_tpl_vars['colspan']; ?>
" style="text-align:left;">Paiement re&ccedil;u durant la p&eacute;riode (<?php echo $this->_tpl_vars['paiement']['date_debut']; ?>
 au <?php echo $this->_tpl_vars['paiement']['date_fin']; ?>
)</td>
				<?php if ($this->_tpl_vars['cols']['ristourne_a_recevoir']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['paiement']['montant'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
				<?php if ($this->_tpl_vars['cols']['ristourne_a_verser']): ?><td style="text-align:right;">&nbsp;</td><?php endif; ?>
			</tr>

		<?php endforeach; endif; unset($_from); ?>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['rows']['paiements_emis']): ?>
		<?php $_from = $this->_tpl_vars['paiementsEmisArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['paiementsEmisForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['paiementsEmisForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['p'] => $this->_tpl_vars['paiement']):
        $this->_foreach['paiementsEmisForeach']['iteration']++;
?>

			<tr class="paiement">
				<td colspan="<?php echo $this->_tpl_vars['colspan']; ?>
" style="text-align:left;">
					Paiement &eacute;mis en date du <?php echo $this->_tpl_vars['paiement']['date']; ?>

					<?php if ($this->_tpl_vars['paiement']['numero_facture'] != "" || $this->_tpl_vars['paiement']['numero_cheque'] != ""): ?>
						(<?php if ($this->_tpl_vars['paiement']['numero_facture'] != ""):  echo $this->_tpl_vars['paiement']['numero_facture'];  endif;  if ($this->_tpl_vars['paiement']['numero_cheque'] != ""): ?> <?php echo $this->_tpl_vars['paiement']['numero_cheque'];  endif; ?>)
					<?php endif; ?>
				</td>
				<?php if ($this->_tpl_vars['cols']['ristourne_a_recevoir']): ?><td style="text-align:right;">&nbsp;</td><?php endif; ?>
				<?php if ($this->_tpl_vars['cols']['ristourne_a_verser']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['paiement']['montant'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php endif; ?>
			</tr>

		<?php endforeach; endif; unset($_from); ?>
		<?php endif; ?>

		<?php if ($this->_tpl_vars['cols']['ristourne_a_recevoir'] || $this->_tpl_vars['cols']['ristourne_a_verser']): ?>
		<tr class="total_paiements">
			<td colspan="<?php echo $this->_tpl_vars['colspan']; ?>
" style="text-align:left;">Total des paiements&nbsp;:</td>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_recevoir']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['totalArray']['paiements_recus'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php else: ?><td>&nbsp;</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_verser']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['totalArray']['paiements_emis'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php else: ?><td>&nbsp;</td><?php endif; ?>
		</tr>
		<?php endif; ?>

		<tr class="solde">
			<td colspan="<?php echo $this->_tpl_vars['colspan']; ?>
" style="text-align:left;">Solde&nbsp;:</td>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_recevoir']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['solde_a_recevoir'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php else: ?><td>&nbsp;</td><?php endif; ?>
			<?php if ($this->_tpl_vars['cols']['ristourne_a_verser']): ?><td style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['solde_a_verser'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td><?php else: ?><td>&nbsp;</td><?php endif; ?>
		</tr>

	</table><br />

<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>