<?php /* Smarty version 2.6.13, created on 2010-11-15 04:38:48
         compiled from facturation/paiement.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

	<?php echo $this->_tpl_vars['errors']; ?>


	<?php if ($this->_tpl_vars['nologin'] != 1): ?>

		<?php echo '
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = \'\';
			var action = document.getElementById(\'action\');
			var etudeKey = document.getElementById(\'etudeKey\');
			var factureKey = document.getElementById(\'factureKey\');
			var paiementKey = document.getElementById(\'paiementKey\');

			switch (thisAction) {
				case \'ajout\':
					var etudeKeyAddValue = getSelectValue(document.getElementById(\'etudeKeyAdd\'));
					if (etudeKeyAddValue == \'\' || etudeKeyAddValue == \'0\' || etudeKeyAddValue == \'undefined\') {
						errorMsg += \'Vous devez choisir une \\351tude.\\n\';
					}
					var factureKeyAddValue = getSelectValue(document.getElementById(\'factureKeyAdd_\' + etudeKeyAddValue));
					/*if (factureKeyAddValue == \'\' || factureKeyAddValue == \'0\' || factureKeyAddValue == \'undefined\' || factureKeyAddValue == null) {
						errorMsg += \'Vous devez choisir une facture.\\n\';
					}*/
					var datePaiementValue = document.getElementById(\'date_paiement\').value;
					if (!isDate(datePaiementValue)) {
						errorMsg += \'La date du paiement n\\\'est pas valide.\\n\';
					}
					var numeroChequeValue = document.getElementById(\'numero_cheque\').value;
					if (trim(numeroChequeValue) == \'\') {
						errorMsg += \'Vous devez entrer le num\\351ro du ch\\350que.\\n\';
					}
					var montantPaiementValue = document.getElementById(\'montant_paiement\').value;
					if (trim(montantPaiementValue) == \'\') {
						errorMsg += \'Vous devez entrer le montant du paiement.\\n\';
					}
					action.value = \'ajout\';
					etudeKey.value = etudeKeyAddValue;
					factureKey.value = factureKeyAddValue;
					break;

				case \'supprimer\':
					var etudeKeyDelValue = getSelectValue(document.getElementById(\'etudeKeyDel\'));
					var paiementKeyDelValue = getSelectValue(document.getElementById(\'paiementKeyDel_\' + etudeKeyDelValue));
					if (paiementKeyDelValue == \'\' || paiementKeyDelValue == \'0\' || paiementKeyDelValue == \'undefined\' || paiementKeyDelValue == null) {
						errorMsg += \'Vous devez choisir un paiement.\\n\';
					}
					action.value = \'supprimer\';
					paiementKey.value = paiementKeyDelValue;
					break;
			}

			if (errorMsg == \'\') {
				document.getElementById(\'paiementForm\').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function loadFactures(form) {
			var factureKeyFields = new Array();
			';  $_from = $this->_tpl_vars['facturesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['factureForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['factureForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['etudeKey'] => $this->_tpl_vars['null']):
        $this->_foreach['factureForeach']['iteration']++;
 echo '
				factureKeyFields[';  echo ($this->_foreach['factureForeach']['iteration']-1);  echo '] = \'factureKeyField\' + form + \'_';  echo $this->_tpl_vars['etudeKey'];  echo '\';
			';  endforeach; endif; unset($_from);  echo '

			for (i = 0; i < factureKeyFields.length; i++) {
				document.getElementById(factureKeyFields[i]).style.display = \'none\';
				document.getElementById(factureKeyFields[i]).disabled = true;
			}

			var etudeKeyValue = getSelectValue(document.getElementById(\'etudeKey\' + form));
			var factureKeyField = document.getElementById(\'factureKeyField\' + form + \'_\' + etudeKeyValue);
			var factureKeyFieldDefault = document.getElementById(\'factureKeyField\' + form + \'_default\');

			if (
				etudeKeyValue != \'\' && etudeKeyValue != \'0\' && etudeKeyValue != \'undefined\' &&
				factureKeyField != \'\' && factureKeyField != \'undefined\' && factureKeyField != null
			) {
				factureKeyFieldDefault.style.display = \'none\';
				factureKeyField.style.display = \'inline\';
				factureKeyField.disabled = false;
			} else {
				factureKeyFieldDefault.style.display = \'inline\';
			}
		}

		function loadPaiements(form) {
			var paiementKeyFields = new Array();
			';  $_from = $this->_tpl_vars['paiementssArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['paiementForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['paiementForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['etudeKey'] => $this->_tpl_vars['null']):
        $this->_foreach['paiementForeach']['iteration']++;
 echo '
				paiementKeyFields[';  echo ($this->_foreach['paiementForeach']['iteration']-1);  echo '] = \'paiementKeyField\' + form + \'_';  echo $this->_tpl_vars['etudeKey'];  echo '\';
			';  endforeach; endif; unset($_from);  echo '

			for (i = 0; i < paiementKeyFields.length; i++) {
				document.getElementById(paiementKeyFields[i]).style.display = \'none\';
				document.getElementById(paiementKeyFields[i]).disabled = true;
			}

			var etudeKeyValue = getSelectValue(document.getElementById(\'etudeKey\' + form));
			var paiementKeyField = document.getElementById(\'paiementKeyField\' + form + \'_\' + etudeKeyValue);
			var paiementKeyFieldDefault = document.getElementById(\'paiementKeyField\' + form + \'_default\');

			if (
				etudeKeyValue != \'\' && etudeKeyValue != \'0\' && etudeKeyValue != \'undefined\' &&
				paiementKeyField != \'\' && paiementKeyField != \'undefined\' && paiementKeyField != null
			) {
				paiementKeyFieldDefault.style.display = \'none\';
				paiementKeyField.style.display = \'inline\';
				paiementKeyField.disabled = false;
			} else {
				paiementKeyFieldDefault.style.display = \'inline\';
			}
		}
		
		function isDate(strValue) {
			var objRegExp = /^\\d{4}-\\d{2}-\\d{2}$/
			if (objRegExp.test(strValue)) {
				return true;
			}
			return false;
		}
		
		</script>
		'; ?>


		<form action="paiement.php" method="post" id="paiementForm">
			<input type="hidden" name="action" id="action" value="" />
			<input type="hidden" name="etudeKey" id="etudeKey" value="" />
			<input type="hidden" name="factureKey" id="factureKey" value="" />
			<input type="hidden" name="paiementKey" id="paiementKey" value="" />			

			<h1>Ajout d'un paiement</h1>

			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKeyAdd" id="etudeKeyAdd" onchange="javascript: loadFactures('Add');">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				<?php $_from = $this->_tpl_vars['etudesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
					<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select><br />
	
			<br />

			<label>Num&eacute;ro de la facture (facultatif)&nbsp;:</label>

			<select name="factureKeyFieldAdd_default" id="factureKeyFieldAdd_default" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
			</select>

			<?php $_from = $this->_tpl_vars['facturesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['etudeKey'] => $this->_tpl_vars['null']):
?>
			<select name="factureKeyFieldAdd_<?php echo $this->_tpl_vars['etudeKey']; ?>
" id="factureKeyFieldAdd_<?php echo $this->_tpl_vars['etudeKey']; ?>
" style="display:none;" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
				<?php $_from = $this->_tpl_vars['facturesArray'][$this->_tpl_vars['etudeKey']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['factureKey'] => $this->_tpl_vars['factureDate']):
?>
					<option value="<?php echo $this->_tpl_vars['factureKey']; ?>
"><?php echo $this->_tpl_vars['factureKey']; ?>
 (<?php echo $this->_tpl_vars['factureDate']; ?>
)</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
			<?php endforeach; endif; unset($_from); ?>

			<br />
			<br />
			
			<label for="date_paiement">Date du paiement&nbsp;:</label>
			<input type="text" name="date_paiement" id="date_paiement" value="<?php echo $this->_tpl_vars['date_paiement']; ?>
" /><span class="note">AAAA-MM-JJ</span><br />

			<br />
			
			<label for="numero_cheque">Num&eacute;ro du ch&egrave;que&nbsp;:</label>
			<input type="text" name="numero_cheque" id="numero_cheque" value="<?php echo $this->_tpl_vars['numero_cheque']; ?>
" /><br />

			<br />
			
			<label for="montant_paiement">Montant du paiement&nbsp;:</label>
			<input type="text" name="montant_paiement" id="montant_paiement" onchange="javascript: adjustNumericField('montant_paiement', 1, 2, 0, 0);" value="<?php echo $this->_tpl_vars['montant_paiement']; ?>
" /><br />

			<br />

			<input type="button" class="submit" value="Ajouter le paiement" onclick="javascript: submitForm('ajout');" /><br />

			<br />
			<br />
			<br />

			<h1>Suppression d'un paiement</h1>

			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKeyDel" id="etudeKeyDel" onchange="javascript: loadPaiements('Del');">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				<?php $_from = $this->_tpl_vars['etudesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
					<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select><br />
	
			<br />

			<label>Paiement&nbsp;:</label>

			<select name="paiementKeyFieldDel_default" id="paiementKeyFieldDel_default" disabled="disabled">
				<option selected="selected" value="0">- Choisir un paiement -</option>
			</select>

			<?php $_from = $this->_tpl_vars['paiementsArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['etudeKey'] => $this->_tpl_vars['null']):
?>
			<select name="paiementKeyFieldDel_<?php echo $this->_tpl_vars['etudeKey']; ?>
" id="paiementKeyFieldDel_<?php echo $this->_tpl_vars['etudeKey']; ?>
" style="display:none;" disabled="disabled">
				<option selected="selected" value="0">- Choisir un paiement -</option>
				<?php $_from = $this->_tpl_vars['paiementsArray'][$this->_tpl_vars['etudeKey']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['paiementKey'] => $this->_tpl_vars['paiementInfo']):
?>
					<option value="<?php echo $this->_tpl_vars['paiementKey']; ?>
"><?php echo $this->_tpl_vars['paiementInfo']; ?>
</option>
				<?php endforeach; endif; unset($_from); ?>
			</select>
			<?php endforeach; endif; unset($_from); ?>

			<br />
			<br />

			<input type="button" class="submit" value="Supprimer le paiement" onclick="javascript: submitForm('supprimer');" /><br />

			<br />
			<br />
			<br />

		</form>

	<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>