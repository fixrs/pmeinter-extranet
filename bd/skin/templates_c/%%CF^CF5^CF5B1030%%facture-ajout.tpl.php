<?php /* Smarty version 2.6.13, created on 2011-05-20 17:53:13
         compiled from facturation/facture-ajout.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

	<?php echo $this->_tpl_vars['errors']; ?>


	<?php if ($this->_tpl_vars['nologin'] != 1): ?>

		<?php echo '
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = \'\';
			var action = document.getElementById(\'action\');

			switch (thisAction) {
				case \'ajouter\':
					var etudeKeyValue = getSelectValue(document.getElementById(\'etudeKey\'));
					if (etudeKeyValue == \'\' || etudeKeyValue == \'0\' || etudeKeyValue == \'undefined\') {
						errorMsg += \'Vous devez choisir une \\351tude.\\n\';
					}
					var factureKeyValue = document.getElementById(\'factureKey\').value;
					if (trim(factureKeyValue) == \'\') {
						errorMsg += \'Vous devez inscrire un num\\351ro de facture.\\n\';
					}
					var dateDebutValue = document.getElementById(\'date_debut\').value;
					if (!isDate(dateDebutValue)) {
						errorMsg += \'La date de d\\351but n\\\'est pas valide.\\n\';
					}
					var dateFinValue = document.getElementById(\'date_fin\').value;
					if (!isDate(dateFinValue)) {
						errorMsg += \'La date de fin n\\\'est pas valide.\\n\';
					}
					var montantFactureValue = document.getElementById(\'montant_facture\').value;
					if (trim(montantFactureValue) == \'\') {
						errorMsg += \'Vous devez entrer le montant de la facture.\\n\';
					}
					action.value = \'ajouter\';
					break;
			}

			if (errorMsg == \'\') {
				document.getElementById(\'factureForm\').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function isDate(strValue) {
			var objRegExp = /^\\d{4}-\\d{2}-\\d{2}$/
			if (objRegExp.test(strValue)) {
				return true;
			}
			return false;
		}
		
		</script>
		'; ?>


		<form action="facture.php" method="post" id="factureForm">
			<input type="hidden" name="form" id="form" value="ajout" />
			<input type="hidden" name="action" id="action" value="" />

			<h1>Ajout d'une facture administrative</h1>

			<div class="noticeDiv">
				<?php $_from = $this->_tpl_vars['noticeArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['notices'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['notices']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['notice']):
        $this->_foreach['notices']['iteration']++;
?>
					<?php if (($this->_foreach['notices']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="noticeBox"><?php echo $this->_tpl_vars['notice']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
				<div class="errorDiv">
				<?php $_from = $this->_tpl_vars['errorArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['errors'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['errors']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['erreur']):
        $this->_foreach['errors']['iteration']++;
?>
					<?php if (($this->_foreach['errors']['iteration'] <= 1)): ?><br /><?php endif; ?>
					<div class="errorBox"><?php echo $this->_tpl_vars['erreur']; ?>
</div>
				<?php endforeach; endif; unset($_from); ?>
			</div>
			<br clear="all" />

			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKey" id="etudeKey">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				<?php $_from = $this->_tpl_vars['etudesArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['clef'] => $this->_tpl_vars['etude']):
?>
					<?php if ($this->_tpl_vars['clef'] == $this->_tpl_vars['etudeKey2']): ?>
						<option value="<?php echo $this->_tpl_vars['clef']; ?>
" selected="selected"><?php echo $this->_tpl_vars['etude']; ?>
</option>
					<?php else: ?>
						<option value="<?php echo $this->_tpl_vars['clef']; ?>
"><?php echo $this->_tpl_vars['etude']; ?>
</option>
					<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?>
			</select><br />
	
			<br />

			<label>P&eacute;riode&nbsp;:</label>
			<input type="text" name="date_debut" id="date_debut" onchange="javascript: adjustDateField('date_debut');" value="<?php echo $this->_tpl_vars['date_debut']; ?>
" /> au&nbsp;&nbsp;<input type="text" name="date_fin" id="date_fin" onchange="javascript: adjustDateField('date_fin');" value="<?php echo $this->_tpl_vars['date_fin']; ?>
" /><span class="note">AAAA-MM-JJ</span><br />

			<br />
			
			<label for="factureKey">Num&eacute;ro de la facture&nbsp;:</label>
			<input type="text" name="factureKey" id="factureKey" value="<?php echo $this->_tpl_vars['factureKey']; ?>
" /><br />

			<br />
			
			<label for="montant_facture">Montant de la facture&nbsp;:</label>
			<input type="text" name="montant_facture" id="montant_facture" onchange="javascript: adjustNumericField('montant_facture', 1, 2, 0, 0);" value="<?php echo $this->_tpl_vars['montant_facture']; ?>
" /><br />

			<br />

			<input type="button" class="submit" value="Ajouter la facture" onclick="javascript: submitForm('ajouter');" /><br />

			<br />
			<br />
			<br />

		</form>

	<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>