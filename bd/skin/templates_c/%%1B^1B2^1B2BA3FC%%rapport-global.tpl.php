<?php /* Smarty version 2.6.13, created on 2011-05-19 15:46:50
         compiled from facturation/rapport-global.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'facturation/rapport-global.tpl', 21, false),array('modifier', 'toMoneyFormat', 'facturation/rapport-global.tpl', 62, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<?php if ($this->_tpl_vars['nologin'] != 1): ?>

	<center>
		<h1>Rapport administratif</h1>
		<strong><?php echo $this->_tpl_vars['dateDebut']; ?>
</strong> au <strong><?php echo $this->_tpl_vars['dateFin']; ?>
</strong><br />
	</center>

	<div id="printpage"><a href="javascript: window.print();"><img class="print" src="<?php echo $this->_tpl_vars['SKIN_URL']; ?>
/img/printer.jpg" alt="Imprimer" /> Imprimer le rapport</a></div>

	<br />
	<br />

	<?php $this->assign('ln', '0'); ?>
	
	<?php $_from = $this->_tpl_vars['reportArray']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['reportForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['reportForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['n'] => $this->_tpl_vars['etude']):
        $this->_foreach['reportForeach']['iteration']++;
?>
	
	<?php if ($this->_tpl_vars['etude']['info']['solde_final'] != 0): ?>
	
		<?php $this->assign('lx', count($this->_tpl_vars['etude']['paiements'])); ?>

		<?php $this->assign('ln', $this->_tpl_vars['ln']+$this->_tpl_vars['lx']+10); ?>

		<?php if ($this->_tpl_vars['ln'] > 40): ?>

			<?php $this->assign('ln', '0'); ?>

			<hr class="pagebreak" />

			<div class="printblock">
				<center>
					<h1>Rapport administratif</h1>
					<strong><?php echo $this->_tpl_vars['dateDebut']; ?>
</strong> au <strong><?php echo $this->_tpl_vars['dateFin']; ?>
</strong><br />
				</center>
				<br />
				<br />
			</div>
		<?php endif; ?>

		<table cellpadding="0" cellspacing="0" border="0" class="rapport_global">

			<tr>
				<td class="etude_info_nom" style="text-align:left;" colspan="4"><strong><?php echo $this->_tpl_vars['etude']['info']['etude_nom']; ?>
</strong></td>
			</tr>

			<?php $this->assign('cellClass', ""); ?>
			<?php if ($this->_tpl_vars['etude']['info']['solde_precedent'] < 0): ?>
				<?php $this->assign('cellClass', 'neg'); ?>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['etude']['info']['solde_precedent'] > 0): ?>
				<?php $this->assign('cellClass', 'pos'); ?>
			<?php endif; ?>

			
			<tr>
				<td class="etude_info_label" style="text-align:left;" colspan="3">Total des ristournes cumul&eacute;es&nbsp;:</td>
				<td class="etude_info_value" style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['etude']['info']['soustotal_ristournes'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
			</tr>

			<tr>
				<td class="etude_info_label" style="text-align:left;" colspan="3">TPS (&nbsp;<?php echo $this->_tpl_vars['etude']['info']['tps_pourcent']; ?>
%&nbsp;)&nbsp;:</td>
				<td class="etude_info_value" style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['etude']['info']['tps_amount'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
			</tr>

			<tr>
				<td class="etude_info_label" style="text-align:left;" colspan="3">TVQ (&nbsp;<?php echo $this->_tpl_vars['etude']['info']['tvq_pourcent']; ?>
%&nbsp;)&nbsp;:</td>
				<td class="etude_info_value" style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['etude']['info']['tvq_amount'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
			</tr>

			<tr>
				<td class="etude_info_label" style="text-align:left;" colspan="3">Total &agrave; facturer&nbsp;:</td>
				<td class="etude_info_value" style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['etude']['info']['total_ristournes'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
			</tr>

			<tr>
				<td class="paiement_label" style="text-align:left;"># de la Facture&nbsp;</td>
				<td class="paiement_label" style="text-align:left;"># du Ch&egrave;que&nbsp;</td>
				<td class="paiement_label" style="text-align:right;">Paiement re&ccedil;u&nbsp;</td>
				<td class="paiement_label" style="text-align:right; width:120px;">Solde&nbsp;</td>
			</tr>
	
			<?php $_from = $this->_tpl_vars['etude']['paiements']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['paiementForeach'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['paiementForeach']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['p'] => $this->_tpl_vars['paiement']):
        $this->_foreach['paiementForeach']['iteration']++;
?>

				<tr>
					<td class="paiement_value" style="text-align:left;"><?php echo $this->_tpl_vars['paiement']['numero_facture']; ?>
</td>
					<td class="paiement_value" style="text-align:left;"><?php echo $this->_tpl_vars['paiement']['numero_cheque']; ?>
</td>
					<td class="paiement_value" style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['paiement']['montant_paiement'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
					<td class="paiement_value" style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['paiement']['solde'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
				</tr>
				
			<?php endforeach; endif; unset($_from); ?>

			<?php $this->assign('cellClass', ""); ?>
			<?php if ($this->_tpl_vars['etude']['info']['solde_final'] < 0): ?>
				<?php $this->assign('cellClass', 'neg'); ?>
			<?php endif; ?>
			<?php if ($this->_tpl_vars['etude']['info']['solde_final'] > 0): ?>
				<?php $this->assign('cellClass', 'pos'); ?>
			<?php endif; ?>

			<tr>
				<td class="etude_info_label" style="text-align:left;" colspan="3"></td>
				<td class="etude_info_value <?php echo $this->_tpl_vars['cellClass']; ?>
" style="text-align:right;"><?php echo ((is_array($_tmp=$this->_tpl_vars['etude']['info']['solde_final'])) ? $this->_run_mod_handler('toMoneyFormat', true, $_tmp) : toMoneyFormat($_tmp)); ?>
</td>
			</tr>

		</table><br />

	<?php endif; ?>

	<?php endforeach; endif; unset($_from); ?>

<?php endif; ?>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>