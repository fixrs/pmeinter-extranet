{include file="header.tpl"}

	{$errors}

	{if $nologin != 1}

		{literal}
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = '';
			var action = document.getElementById('action');

			switch (thisAction) {
				case 'ajouter':
					var dateValue = document.getElementById('date').value;
					if (!isDate(dateValue)) {
						errorMsg += 'La date n\'est pas valide.\n';
					}
					var montantPaiementValue = document.getElementById('montant_paiement').value;
					if (trim(montantPaiementValue) == '') {
						errorMsg += 'Vous devez entrer le montant du paiement.\n';
					}
					action.value = 'ajouter';
					break;
			}

			if (errorMsg == '') {
				document.getElementById('paiementForm').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function isDate(strValue) {
			var objRegExp = /^\d{4}-\d{2}-\d{2}$/
			if (objRegExp.test(strValue)) {
				return true;
			}
			return false;
		}
		
		</script>
		{/literal}

		<form action="riq-paiement.php" method="post" id="paiementForm">
			<input type="hidden" name="form" id="form" value="ajout" />
			<input type="hidden" name="action" id="action" value="" />
			<input type="hidden" name="paiementKey" id="paiementKey" value="" />			

			<h1>Ajout d'un paiement RIQ</h1>

			<div class="noticeDiv">
				{foreach name=notices from=$noticeArray item=notice}
					{if $smarty.foreach.notices.first}<br />{/if}
					<div class="noticeBox">{$notice}</div>
				{/foreach}
			</div>
				<div class="errorDiv">
				{foreach name=errors from=$errorArray item=erreur}
					{if $smarty.foreach.errors.first}<br />{/if}
					<div class="errorBox">{$erreur}</div>
				{/foreach}
			</div>
			<br clear="all" />

			<label>Date du paiement&nbsp;:</label>
			<input type="text" name="date" id="date" onchange="javascript: adjustDateField('date');" value="{$date}" /> <span class="note">AAAA-MM-JJ</span><br />

			<br />
			
			<label for="montant_paiement">Montant du paiement&nbsp;:</label>
			<input type="text" name="montant_paiement" id="montant_paiement" onchange="javascript: adjustNumericField('montant_paiement', 1, 2, 0, 0);" value="{$montant_paiement}" /><br />

			<br />

			<label for="numero_cheque">Num&eacute;ro de la facture (facultatif)&nbsp;:</label>
			<input type="text" name="numero_facture" id="numero_facture" value="{$numero_facture}" /><br />

			<br />

			<label for="numero_cheque">Num&eacute;ro du ch&egrave;que (facultatif)&nbsp;:</label>
			<input type="text" name="numero_cheque" id="numero_cheque" value="{$numero_cheque}" /><br />

			<br />

			<input type="button" class="submit" value="Ajouter le paiement" onclick="javascript: submitForm('ajouter');" /><br />

			<br />
			<br />
			<br />

		</form>

	{/if}

{include file="footer.tpl"}
