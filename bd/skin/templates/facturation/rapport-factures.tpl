{include file="header.tpl"}

{if $nologin != 1}

	<center>
		<h1>Rapport administratif</h1>
		<strong>{$dateDebut}</strong> au <strong>{$dateFin}</strong><br />
	</center>

	<div id="printpage"><a href="javascript: window.print();"><img class="print" src="{$SKIN_URL}/img/printer.jpg" alt="Imprimer" /> Imprimer le rapport</a></div>

	<br />
	<br />

	{assign var="ln" value="0"}
	
	{foreach name=reportForeach from=$reportArray key=n item=etude}
	
	{if $etude.factures|@count gt 0}
	
		{assign var="lx" value=$etude.factures|@count}

		{assign var="ln" value=$ln+$lx+3}

		{if $ln gt 40}

			{assign var="ln" value="0"}

			<hr class="pagebreak" />

			<div class="printblock">
				<center>
					<h1>Rapport administratif</h1>
					<strong>{$dateDebut}</strong> au <strong>{$dateFin}</strong><br />
				</center>
				<br />
				<br />
			</div>
		{/if}

		<table cellpadding="0" cellspacing="0" border="0" class="rapport_factures">

			<tr>
				<td class="etude_info_nom" style="text-align:left;" colspan="2"><strong>{$etude.info.etude_nom}</strong></td>
			</tr>

			<tr>
				<td class="facture_label" style="text-align:left;">Num&eacute;ro de la facture&nbsp;</td>
				<td class="facture_label" style="text-align:right; width:140px;">Montant de la facture&nbsp;</td>
			</tr>
	
			{foreach name=factureForeach from=$etude.factures key=f item=facture}

				<tr>
					<td class="facture_value" style="text-align:left;">{$facture.numero_facture}</td>
					<td class="facture_value" style="text-align:right;">{$facture.montant_facture|toMoneyFormat}</td>
				</tr>
				
			{/foreach}

		</table><br />

	{/if}

	{/foreach}

{/if}

{include file="footer.tpl"}
