{include file="header.tpl"}

	{$errors}

	{if $nologin != 1}

		{literal}
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = '';
			var action = document.getElementById('action');
			var etudeKey = document.getElementById('etudeKey');
			var paiementKey = document.getElementById('paiementKey');

			switch (thisAction) {
				case 'supprimer':
					var etudeKeyDelValue = getSelectValue(document.getElementById('etudeKeyDel'));
					var paiementKeyDelValue = getSelectValue(document.getElementById('paiementKeyFieldDel_' + etudeKeyDelValue));
					if (paiementKeyDelValue == '' || paiementKeyDelValue == '0' || paiementKeyDelValue == 'undefined' || paiementKeyDelValue == null) {
						errorMsg += 'Vous devez choisir un paiement.\n';
					}
					action.value = 'supprimer';
					etudeKey.value = etudeKeyDelValue;
					paiementKey.value = paiementKeyDelValue;
					break;
			}

			if (errorMsg == '') {
				document.getElementById('paiementForm').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function loadPaiements(form) {
			var paiementKeyFields = new Array();
			{/literal}{foreach name=paiementForeach from=$paiementsArray key=etudeKey item=null}{literal}
				paiementKeyFields[{/literal}{$smarty.foreach.paiementForeach.index}{literal}] = 'paiementKeyField' + form + '_{/literal}{$etudeKey}{literal}';
			{/literal}{/foreach}{literal}

			for (i = 0; i < paiementKeyFields.length; i++) {
				document.getElementById(paiementKeyFields[i]).style.display = 'none';
				document.getElementById(paiementKeyFields[i]).disabled = true;
			}

			var etudeKeyValue = getSelectValue(document.getElementById('etudeKey' + form));
			var paiementKeyField = document.getElementById('paiementKeyField' + form + '_' + etudeKeyValue);
			var paiementKeyFieldDefault = document.getElementById('paiementKeyField' + form + '_default');

			if (
				etudeKeyValue != '' && etudeKeyValue != '0' && etudeKeyValue != 'undefined' &&
				paiementKeyField != '' && paiementKeyField != 'undefined' && paiementKeyField != null
			) {
				paiementKeyFieldDefault.style.display = 'none';
				paiementKeyField.style.display = 'inline';
				paiementKeyField.disabled = false;
			} else {
				paiementKeyFieldDefault.style.display = 'inline';
			}
		}
		
		
		</script>
		{/literal}

		<form action="paiement.php" method="post" id="paiementForm">
			<input type="hidden" name="form" id="form" value="suppression" />
			<input type="hidden" name="action" id="action" value="" />
			<input type="hidden" name="etudeKey" id="etudeKey" value="" />
			<input type="hidden" name="paiementKey" id="paiementKey" value="" />			

			<h1>Suppression d'un paiement</h1>

			<div class="noticeDiv">
				{foreach name=notices from=$noticeArray item=notice}
					{if $smarty.foreach.notices.first}<br />{/if}
					<div class="noticeBox">{$notice}</div>
				{/foreach}
			</div>
				<div class="errorDiv">
				{foreach name=errors from=$errorArray item=erreur}
					{if $smarty.foreach.errors.first}<br />{/if}
					<div class="errorBox">{$erreur}</div>
				{/foreach}
			</div>
				<br clear="all" />
	
			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKeyDel" id="etudeKeyDel" onchange="javascript: loadPaiements('Del');">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				{foreach from=$etudesArray item=etude key=clef}
					{if $clef == $etudeKey2}
						<option value="{$clef}" selected="selected">{$etude}</option>
					{else}
						<option value="{$clef}">{$etude}</option>
					{/if}
				{/foreach}
			</select><br />
	
			<br />

			<label>Paiement&nbsp;:</label>

			<select name="paiementKeyFieldDel_default" id="paiementKeyFieldDel_default" disabled="disabled">
				<option selected="selected" value="0">- Choisir un paiement -</option>
			</select>

			{foreach from=$paiementsArray key=etudeClef item=null}
			<select name="paiementKeyFieldDel_{$etudeClef}" id="paiementKeyFieldDel_{$etudeClef}" style="display:none;" disabled="disabled">
				<option selected="selected" value="0">- Choisir un paiement -</option>
				{foreach from=$paiementsArray.$etudeClef key=paiementClef item=paiementInfo }
					<option value="{$paiementClef}">{$paiementInfo}</option>
				{/foreach}
			</select>
			{/foreach}

			<br />
			<br />

			<input type="button" class="submit" value="Supprimer le paiement" onclick="javascript: submitForm('supprimer');" /><br />

			<br />
			<br />
			<br />

		</form>

	{/if}

{include file="footer.tpl"}
