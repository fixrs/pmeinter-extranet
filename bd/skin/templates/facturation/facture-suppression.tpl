{include file="header.tpl"}

	{$errors}

	{if $nologin != 1}

		{literal}
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = '';
			var action = document.getElementById('action');
			var etudeKey = document.getElementById('etudeKey');
			var factureKey = document.getElementById('factureKey');

			switch (thisAction) {
				case 'supprimer':
					var etudeKeyDelValue = getSelectValue(document.getElementById('etudeKeyDel'));
					var factureKeyDelValue = getSelectValue(document.getElementById('factureKeyFieldDel_' + etudeKeyDelValue));
					if (factureKeyDelValue == '' || factureKeyDelValue == '0' || factureKeyDelValue == 'undefined' || factureKeyDelValue == null) {
						errorMsg += 'Vous devez choisir une facture.\n';
					}
					action.value = 'supprimer';
					etudeKey.value = etudeKeyDelValue;
					factureKey.value = factureKeyDelValue;
					break;
			}

			if (errorMsg == '') {
				document.getElementById('factureForm').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function loadFactures(form) {
			var factureKeyFields = new Array();
			{/literal}{foreach name=factureForeach from=$facturesArray key=etudeKey item=null}{literal}
				factureKeyFields[{/literal}{$smarty.foreach.factureForeach.index}{literal}] = 'factureKeyField' + form + '_{/literal}{$etudeKey}{literal}';
			{/literal}{/foreach}{literal}

			for (i = 0; i < factureKeyFields.length; i++) {
				document.getElementById(factureKeyFields[i]).style.display = 'none';
				document.getElementById(factureKeyFields[i]).disabled = true;
			}

			var etudeKeyValue = getSelectValue(document.getElementById('etudeKey' + form));
			var factureKeyField = document.getElementById('factureKeyField' + form + '_' + etudeKeyValue);
			var factureKeyFieldDefault = document.getElementById('factureKeyField' + form + '_default');

			if (
				etudeKeyValue != '' && etudeKeyValue != '0' && etudeKeyValue != 'undefined' &&
				factureKeyField != '' && factureKeyField != 'undefined' && factureKeyField != null
			) {
				factureKeyFieldDefault.style.display = 'none';
				factureKeyField.style.display = 'inline';
				factureKeyField.disabled = false;
			} else {
				factureKeyFieldDefault.style.display = 'inline';
			}
		}
		
		
		</script>
		{/literal}

		<form action="facture.php" method="post" id="factureForm">
			<input type="hidden" name="form" id="form" value="suppression" />
			<input type="hidden" name="action" id="action" value="" />
			<input type="hidden" name="etudeKey" id="etudeKey" value="" />
			<input type="hidden" name="factureKey" id="factureKey" value="" />

			<h1>Suppression d'une facture administrative</h1>

			<div class="noticeDiv">
				{foreach name=notices from=$noticeArray item=notice}
					{if $smarty.foreach.notices.first}<br />{/if}
					<div class="noticeBox">{$notice}</div>
				{/foreach}
			</div>
				<div class="errorDiv">
				{foreach name=errors from=$errorArray item=erreur}
					{if $smarty.foreach.errors.first}<br />{/if}
					<div class="errorBox">{$erreur}</div>
				{/foreach}
			</div>
				<br clear="all" />
	
			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKeyDel" id="etudeKeyDel" onchange="javascript: loadFactures('Del');">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				{foreach from=$etudesArray item=etude key=clef}
					{if $clef == $etudeKey2}
						<option value="{$clef}" selected="selected">{$etude}</option>
					{else}
						<option value="{$clef}">{$etude}</option>
					{/if}
				{/foreach}
			</select><br />
	
			<br />

			<label>Facture&nbsp;:</label>

			<select name="factureKeyFieldDel_default" id="factureKeyFieldDel_default" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
			</select>

			{foreach from=$facturesArray key=etudeClef item=null}
			<select name="factureKeyFieldDel_{$etudeClef}" id="factureKeyFieldDel_{$etudeClef}" style="display:none;" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
				{foreach from=$facturesArray.$etudeClef key=factureClef item=factureInfo }
					<option value="{$factureClef}">{$factureInfo}</option>
				{/foreach}
			</select>
			{/foreach}

			<br />
			<br />

			<input type="button" class="submit" value="Supprimer la facture" onclick="javascript: submitForm('supprimer');" /><br />

			<br />
			<br />
			<br />

		</form>

	{/if}

{include file="footer.tpl"}
