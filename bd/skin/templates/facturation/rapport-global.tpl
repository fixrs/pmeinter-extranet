{include file="header.tpl"}

{if $nologin != 1}

	<center>
		<h1>Rapport administratif</h1>
		<strong>{$dateDebut}</strong> au <strong>{$dateFin}</strong><br />
	</center>

	<div id="printpage"><a href="javascript: window.print();"><img class="print" src="{$SKIN_URL}/img/printer.jpg" alt="Imprimer" /> Imprimer le rapport</a></div>

	<br />
	<br />

	{assign var="ln" value="0"}
	
	{foreach name=reportForeach from=$reportArray key=n item=etude}
	
	{if $etude.info.solde_final != 0}
	
		{assign var="lx" value=$etude.paiements|@count}

		{assign var="ln" value=$ln+$lx+10}

		{if $ln gt 40}

			{assign var="ln" value="0"}

			<hr class="pagebreak" />

			<div class="printblock">
				<center>
					<h1>Rapport administratif</h1>
					<strong>{$dateDebut}</strong> au <strong>{$dateFin}</strong><br />
				</center>
				<br />
				<br />
			</div>
		{/if}

		<table cellpadding="0" cellspacing="0" border="0" class="rapport_global">

			<tr>
				<td class="etude_info_nom" style="text-align:left;" colspan="4"><strong>{$etude.info.etude_nom}</strong></td>
			</tr>

			{assign var=cellClass value=""}
			{if $etude.info.solde_precedent lt 0}
				{assign var=cellClass value="neg"}
			{/if}
			{if $etude.info.solde_precedent gt 0}
				{assign var=cellClass value="pos"}
			{/if}

			{*<tr>
				<td class="etude_info_label" style="text-align:left;" colspan="3">Solde pr&eacute;c&eacute;dent&nbsp;:</td>
				<td class="etude_info_value {$cellClass}" style="text-align:right;">{$etude.info.solde_precedent|toMoneyFormat}</td>
			</tr>*}

			<tr>
				<td class="etude_info_label" style="text-align:left;" colspan="3">Total des ristournes cumul&eacute;es&nbsp;:</td>
				<td class="etude_info_value" style="text-align:right;">{$etude.info.soustotal_ristournes|toMoneyFormat}</td>
			</tr>

			<tr>
				<td class="etude_info_label" style="text-align:left;" colspan="3">TPS (&nbsp;{$etude.info.tps_pourcent}%&nbsp;)&nbsp;:</td>
				<td class="etude_info_value" style="text-align:right;">{$etude.info.tps_amount|toMoneyFormat}</td>
			</tr>

			<tr>
				<td class="etude_info_label" style="text-align:left;" colspan="3">TVQ (&nbsp;{$etude.info.tvq_pourcent}%&nbsp;)&nbsp;:</td>
				<td class="etude_info_value" style="text-align:right;">{$etude.info.tvq_amount|toMoneyFormat}</td>
			</tr>

			<tr>
				<td class="etude_info_label" style="text-align:left;" colspan="3">Total &agrave; facturer&nbsp;:</td>
				<td class="etude_info_value" style="text-align:right;">{$etude.info.total_ristournes|toMoneyFormat}</td>
			</tr>

			<tr>
				<td class="paiement_label" style="text-align:left;"># de la Facture&nbsp;</td>
				<td class="paiement_label" style="text-align:left;"># du Ch&egrave;que&nbsp;</td>
				<td class="paiement_label" style="text-align:right;">Paiement re&ccedil;u&nbsp;</td>
				<td class="paiement_label" style="text-align:right; width:120px;">Solde&nbsp;</td>
			</tr>
	
			{foreach name=paiementForeach from=$etude.paiements key=p item=paiement}

				<tr>
					<td class="paiement_value" style="text-align:left;">{$paiement.numero_facture}</td>
					<td class="paiement_value" style="text-align:left;">{$paiement.numero_cheque}</td>
					<td class="paiement_value" style="text-align:right;">{$paiement.montant_paiement|toMoneyFormat}</td>
					<td class="paiement_value" style="text-align:right;">{$paiement.solde|toMoneyFormat}</td>
				</tr>
				
			{/foreach}

			{assign var=cellClass value=""}
			{if $etude.info.solde_final lt 0}
				{assign var=cellClass value="neg"}
			{/if}
			{if $etude.info.solde_final gt 0}
				{assign var=cellClass value="pos"}
			{/if}

			<tr>
				<td class="etude_info_label" style="text-align:left;" colspan="3"></td>
				<td class="etude_info_value {$cellClass}" style="text-align:right;">{$etude.info.solde_final|toMoneyFormat}</td>
			</tr>

		</table><br />

	{/if}

	{/foreach}

{/if}

{include file="footer.tpl"}
