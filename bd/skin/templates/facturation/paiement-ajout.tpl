{include file="header.tpl"}

	{$errors}

	{if $nologin != 1}

		{literal}
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = '';
			var action = document.getElementById('action');
			var etudeKey = document.getElementById('etudeKey');
			var factureKey = document.getElementById('factureKey');

			switch (thisAction) {
				case 'ajouter':
					var etudeKeyValue = getSelectValue(document.getElementById('etudeKeyField'));
					if (etudeKeyValue == '' || etudeKeyValue == '0' || etudeKeyValue == 'undefined') {
						errorMsg += 'Vous devez choisir une \351tude.\n';
					}
					var factureKeyValue = getSelectValue(document.getElementById('factureKeyField_' + etudeKeyValue));
					if (factureKeyValue == '' || factureKeyValue == '0' || factureKeyValue == 'undefined' || factureKeyValue == null) {
						errorMsg += 'Vous devez choisir une facture.\n';
					}
					var dateDebutValue = document.getElementById('date_debut').value;
					if (!isDate(dateDebutValue)) {
						errorMsg += 'La date de d\351but n\'est pas valide.\n';
					}
					var dateFinValue = document.getElementById('date_fin').value;
					if (!isDate(dateFinValue)) {
						errorMsg += 'La date de fin n\'est pas valide.\n';
					}
					var numeroChequeValue = document.getElementById('numero_cheque').value;
					if (trim(numeroChequeValue) == '') {
						errorMsg += 'Vous devez entrer le num\351ro du ch\350que.\n';
					}
					var montantPaiementValue = document.getElementById('montant_paiement').value;
					if (trim(montantPaiementValue) == '') {
						errorMsg += 'Vous devez entrer le montant du paiement.\n';
					}
					action.value = 'ajouter';
					etudeKey.value = etudeKeyValue;
					factureKey.value = factureKeyValue;
					break;
			}

			if (errorMsg == '') {
				document.getElementById('paiementForm').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function loadFactures() {
			var factureKeyFields = new Array();
			{/literal}{foreach name=factureForeach from=$facturesArray key=etudeKey item=null}{literal}
				factureKeyFields[{/literal}{$smarty.foreach.factureForeach.index}{literal}] = 'factureKeyField_{/literal}{$etudeKey}{literal}';
			{/literal}{/foreach}{literal}

			for (i = 0; i < factureKeyFields.length; i++) {
				document.getElementById(factureKeyFields[i]).style.display = 'none';
				document.getElementById(factureKeyFields[i]).disabled = true;
			}

			var etudeKeyValue = getSelectValue(document.getElementById('etudeKeyField'));
			var factureKeyField = document.getElementById('factureKeyField_' + etudeKeyValue);
			var factureKeyFieldDefault = document.getElementById('factureKeyField_default');

			if (
				etudeKeyValue != '' && etudeKeyValue != '0' && etudeKeyValue != 'undefined' &&
				factureKeyField != '' && factureKeyField != 'undefined' && factureKeyField != null
			) {
				factureKeyFieldDefault.style.display = 'none';
				factureKeyField.style.display = 'inline';
				factureKeyField.disabled = false;
			} else {
				factureKeyFieldDefault.style.display = 'inline';
			}
		}

		function loadDates() {
			var etudeKeyValue = getSelectValue(document.getElementById('etudeKeyField'));
			var factureKeyValue = getSelectValue(document.getElementById('factureKeyField_' + etudeKeyValue));
			var dateDebutField = document.getElementById('date_debut');
			var dateDebutArray = new Array();
			var dateFinField = document.getElementById('date_fin');
			var dateFinArray = new Array();

			{/literal}{foreach name=datesForeach from=$datesArray key=factureClef item=dates}{literal}
				dateDebutArray['{/literal}{$factureClef}{literal}'] = '{/literal}{$dates.date_debut}{literal}';
				dateFinArray['{/literal}{$factureClef}{literal}'] = '{/literal}{$dates.date_fin}{literal}';
			{/literal}{/foreach}{literal}

			dateDebutField.value = dateDebutArray[factureKeyValue];
			dateFinField.value = dateFinArray[factureKeyValue];
		}

		function isDate(strValue) {
			var objRegExp = /^\d{4}-\d{2}-\d{2}$/
			if (objRegExp.test(strValue)) {
				return true;
			}
			return false;
		}
		
		</script>
		{/literal}

		<form action="paiement.php" method="post" id="paiementForm">
			<input type="hidden" name="form" id="form" value="ajout" />
			<input type="hidden" name="action" id="action" value="" />
			<input type="hidden" name="etudeKey" id="etudeKey" value="" />
			<input type="hidden" name="factureKey" id="factureKey" value="" />
			<input type="hidden" name="paiementKey" id="paiementKey" value="" />			

			<h1>Ajout d'un paiement</h1>

			<div class="noticeDiv">
				{foreach name=notices from=$noticeArray item=notice}
					{if $smarty.foreach.notices.first}<br />{/if}
					<div class="noticeBox">{$notice}</div>
				{/foreach}
			</div>
				<div class="errorDiv">
				{foreach name=errors from=$errorArray item=erreur}
					{if $smarty.foreach.errors.first}<br />{/if}
					<div class="errorBox">{$erreur}</div>
				{/foreach}
			</div>
			<br clear="all" />

			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKeyField" id="etudeKeyField" onchange="javascript: loadFactures();">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				{foreach from=$etudesArray item=etude key=clef}
					{if $clef == $etudeKey2}
						<option value="{$clef}" selected="selected">{$etude}</option>
					{else}
						<option value="{$clef}">{$etude}</option>
					{/if}
				{/foreach}
			</select><br />
	
			<br />

			<label>Num&eacute;ro de la facture (facultatif)&nbsp;:</label>

			<select name="factureKeyField_default" id="factureKeyField_default" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
			</select>

			{foreach from=$facturesArray key=etudeClef item=null}
			<select name="factureKeyField_{$etudeClef}" id="factureKeyField_{$etudeClef}" style="display:none;" onchange="javascript: loadDates(this);" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
				{foreach from=$facturesArray.$etudeClef key=factureClef item=factureInfo }
					<option value="{$factureClef}">{$factureInfo}</option>
				{/foreach}
			</select>
			{/foreach}

			<br />
			<br />
			
			<label>P&eacute;riode&nbsp;:</label>
			<input type="text" name="date_debut" id="date_debut" onchange="javascript: adjustDateField('date_debut');" value="{$date_debut}" readonly="readonly" /> au&nbsp;&nbsp;<input type="text" name="date_fin" id="date_fin" onchange="javascript: adjustDateField('date_fin');" value="{$date_fin}" readonly="readonly" /><br />

			<br />
			
			<label for="numero_cheque">Num&eacute;ro du ch&egrave;que&nbsp;:</label>
			<input type="text" name="numero_cheque" id="numero_cheque" value="{$numero_cheque}" /><br />

			<br />
			
			<label for="montant_paiement">Montant du paiement&nbsp;:</label>
			<input type="text" name="montant_paiement" id="montant_paiement" onchange="javascript: adjustNumericField('montant_paiement', 1, 2, 0, 0);" value="{$montant_paiement}" /><br />

			<br />

			<input type="button" class="submit" value="Ajouter le paiement" onclick="javascript: submitForm('ajouter');" /><br />

			<br />
			<br />
			<br />

		</form>

	{/if}

{include file="footer.tpl"}
