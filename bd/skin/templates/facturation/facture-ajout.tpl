{include file="header.tpl"}

	{$errors}

	{if $nologin != 1}

		{literal}
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = '';
			var action = document.getElementById('action');

			switch (thisAction) {
				case 'ajouter':
					var etudeKeyValue = getSelectValue(document.getElementById('etudeKey'));
					if (etudeKeyValue == '' || etudeKeyValue == '0' || etudeKeyValue == 'undefined') {
						errorMsg += 'Vous devez choisir une \351tude.\n';
					}
					var factureKeyValue = document.getElementById('factureKey').value;
					if (trim(factureKeyValue) == '') {
						errorMsg += 'Vous devez inscrire un num\351ro de facture.\n';
					}
					var dateDebutValue = document.getElementById('date_debut').value;
					if (!isDate(dateDebutValue)) {
						errorMsg += 'La date de d\351but n\'est pas valide.\n';
					}
					var dateFinValue = document.getElementById('date_fin').value;
					if (!isDate(dateFinValue)) {
						errorMsg += 'La date de fin n\'est pas valide.\n';
					}
					var montantFactureValue = document.getElementById('montant_facture').value;
					if (trim(montantFactureValue) == '') {
						errorMsg += 'Vous devez entrer le montant de la facture.\n';
					}
					action.value = 'ajouter';
					break;
			}

			if (errorMsg == '') {
				document.getElementById('factureForm').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function isDate(strValue) {
			var objRegExp = /^\d{4}-\d{2}-\d{2}$/
			if (objRegExp.test(strValue)) {
				return true;
			}
			return false;
		}
		
		</script>
		{/literal}

		<form action="facture.php" method="post" id="factureForm">
			<input type="hidden" name="form" id="form" value="ajout" />
			<input type="hidden" name="action" id="action" value="" />

			<h1>Ajout d'une facture administrative</h1>

			<div class="noticeDiv">
				{foreach name=notices from=$noticeArray item=notice}
					{if $smarty.foreach.notices.first}<br />{/if}
					<div class="noticeBox">{$notice}</div>
				{/foreach}
			</div>
				<div class="errorDiv">
				{foreach name=errors from=$errorArray item=erreur}
					{if $smarty.foreach.errors.first}<br />{/if}
					<div class="errorBox">{$erreur}</div>
				{/foreach}
			</div>
			<br clear="all" />

			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKey" id="etudeKey">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				{foreach from=$etudesArray item=etude key=clef}
					{if $clef == $etudeKey2}
						<option value="{$clef}" selected="selected">{$etude}</option>
					{else}
						<option value="{$clef}">{$etude}</option>
					{/if}
				{/foreach}
			</select><br />
	
			<br />

			<label>P&eacute;riode&nbsp;:</label>
			<input type="text" name="date_debut" id="date_debut" onchange="javascript: adjustDateField('date_debut');" value="{$date_debut}" /> au&nbsp;&nbsp;<input type="text" name="date_fin" id="date_fin" onchange="javascript: adjustDateField('date_fin');" value="{$date_fin}" /><span class="note">AAAA-MM-JJ</span><br />

			<br />
			
			<label for="factureKey">Num&eacute;ro de la facture&nbsp;:</label>
			<input type="text" name="factureKey" id="factureKey" value="{$factureKey}" /><br />

			<br />
			
			<label for="montant_facture">Montant de la facture&nbsp;:</label>
			<input type="text" name="montant_facture" id="montant_facture" onchange="javascript: adjustNumericField('montant_facture', 1, 2, 0, 0);" value="{$montant_facture}" /><br />

			<br />

			<input type="button" class="submit" value="Ajouter la facture" onclick="javascript: submitForm('ajouter');" /><br />

			<br />
			<br />
			<br />

		</form>

	{/if}

{include file="footer.tpl"}
