{include file="header.tpl"}

{if $nologin != 1}

	{literal}
	<script language="JavaScript">

	function submitForm(thisAction) {
		var errorMsg = '';

		var typeValue = getSelectValue(document.getElementById('type'));
		var dateDebutValue = document.getElementById('date_debut').value;
		var dateFinValue = document.getElementById('date_fin').value;
		var taxeTpsValue = document.getElementById('taxe_tps').value;
		var taxeTvqValue = document.getElementById('taxe_tvq').value;
		
		if (typeValue == '' || typeValue == '0' || typeValue == 'undefined') {
			errorMsg += 'Vous devez choisir un type de rapport.\n';
		}
		if (!isDate(dateDebutValue)) {
			errorMsg += 'Vous devez entrer une date de d\351but valide.\n';
		}
		if (!isDate(dateFinValue)) {
			errorMsg += 'Vous devez entrer une date de fin valide.\n';
		}
		if (typeValue == 'global') {
			if (taxeTpsValue == '' || taxeTpsValue == '0' || taxeTpsValue == 'undefined') {
				errorMsg += 'Vous devez sp\351cifier la taxe TPS.\n';
			}
			if (taxeTvqValue == '' || taxeTvqValue == '0' || taxeTvqValue == 'undefined') {
				errorMsg += 'Vous devez sp\351cifier la taxe TVQ.\n';
			}
		}

		if (errorMsg == '') {
			document.getElementById('rapportForm').submit();
		} else {
			alert(errorMsg);
			return false;
		}
	}

	function typeRapportToggle() {
		var typeValue = getSelectValue(document.getElementById('type'));
		var taxeTps = document.getElementById('taxe_tps');
		var taxeTvq = document.getElementById('taxe_tvq');

		switch (typeValue) {
			case 'global':
				taxeTps.disabled = false;
				taxeTvq.disabled = false;
				break;

			case 'factures':
				taxeTps.disabled = true;
				taxeTvq.disabled = true;
				break;

			default:
				taxeTps.disabled = true;
				taxeTvq.disabled = true;
		}
	}

	function isDate(strValue) {
		var objRegExp = /^\d{4}-\d{2}-\d{2}$/
		if (objRegExp.test(strValue)) {
			return true;
		}
		return false;
	}

	</script>
	{/literal}

	<h1>Consultation de rapports</h1>

	<div class="noticeDiv">
		{foreach name=notices from=$noticeArray item=notice}
			{if $smarty.foreach.notices.first}<br />{/if}
			<div class="noticeBox">{$notice}</div>
		{/foreach}
	</div>
		<div class="errorDiv">
		{foreach name=errors from=$errorArray item=erreur}
			{if $smarty.foreach.errors.first}<br />{/if}
			<div class="errorBox">{$erreur}</div>
		{/foreach}
	</div>
	<br clear="all" />


	<form action="rapport.php" method="post" id="rapportForm">
		<input type="hidden" name="action" id="action" value="display" />

		<label>Type de rapport&nbsp;:</label>
		<select name="type" id="type" onchange="javascript: typeRapportToggle();">
			<option selected="selected" value="0">- Choisir un type de rapport -</option>
			<option value="global">Rapport global pour chaque &eacute;tude</option>
			<option value="factures">Rapport des factures pour chaque &eacute;tude</option>
		</select><br />

		<br />

		<label>Pour la p&eacute;riode&nbsp;:</label>
		<input type="text" name="date_debut" id="date_debut" onchange="javascript: adjustDateField('date_debut');" value="{$date_debut}" /> au&nbsp;&nbsp;<input type="text" name="date_fin" id="date_fin" onchange="javascript: adjustDateField('date_fin');" value="{$date_fin}" /><span class="note">AAAA-MM-JJ</span><br />

		<br />

		<label>Taxe TPS (%)&nbsp;:</label>
		<input type="text" name="taxe_tps" id="taxe_tps" onchange="javascript: adjustNumericField('taxe_tps', 1, 3, 0, 0);" value="5,000" disabled="disabled" /><br />

		<br />

		<label>Taxe TVQ (%)&nbsp;:</label>
		<input type="text" name="taxe_tvq" id="taxe_tvq" onchange="javascript: adjustNumericField('taxe_tvq', 1, 3, 0, 0);" value="8,500" disabled="disabled" /><br />
		
		<br />

		<input type="button" class="submit" value="Afficher le rapport" onclick="javascript: submitForm('display');" /><br />

	</form>

	<br />
	<br />
	<br />

{/if}

{include file="footer.tpl"}
  
