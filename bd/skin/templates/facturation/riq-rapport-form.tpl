{include file="header.tpl"}

{if $nologin != 1}

	{literal}
	<script language="JavaScript">

	function submitForm(thisAction) {
		var errorMsg = '';

		var typeValue = getSelectValue(document.getElementById('type'));
		var dateDebutValue = document.getElementById('date_debut').value;
		var dateFinValue = document.getElementById('date_fin').value;
		var taxeTpsValue = document.getElementById('taxe_tps').value;
		var taxeTvqValue = document.getElementById('taxe_tvq').value;
		
		if (typeValue == '' || typeValue == '0' || typeValue == 'undefined') {
			errorMsg += 'Vous devez choisir un type de rapport.\n';
		}
		if (!isDate(dateDebutValue)) {
			errorMsg += 'Vous devez entrer une date de d\351but valide.\n';
		}
		if (!isDate(dateFinValue)) {
			errorMsg += 'Vous devez entrer une date de fin valide.\n';
		}
		if (taxeTpsValue == '' || taxeTpsValue == '0' || taxeTpsValue == 'undefined') {
			errorMsg += 'Vous devez sp\351cifier la taxe TPS.\n';
		}
		if (taxeTvqValue == '' || taxeTvqValue == '0' || taxeTvqValue == 'undefined') {
			errorMsg += 'Vous devez sp\351cifier la taxe TVQ.\n';
		}

		if (errorMsg == '') {
			document.getElementById('rapportForm').submit();
		} else {
			alert(errorMsg);
			return false;
		}
	}

	function isDate(strValue) {
		var objRegExp = /^\d{4}-\d{2}-\d{2}$/
		if (objRegExp.test(strValue)) {
			return true;
		}
		return false;
	}

	</script>
	{/literal}

	<h1>Consultation de rapports des ristournes &agrave; verser</h1>

	<div class="noticeDiv">
		{foreach name=notices from=$noticeArray item=notice}
			{if $smarty.foreach.notices.first}<br />{/if}
			<div class="noticeBox">{$notice}</div>
		{/foreach}
	</div>
		<div class="errorDiv">
		{foreach name=errors from=$errorArray item=erreur}
			{if $smarty.foreach.errors.first}<br />{/if}
			<div class="errorBox">{$erreur}</div>
		{/foreach}
	</div>
	<br clear="all" />


	<form action="riq-rapport.php" method="post" id="rapportForm">
		<input type="hidden" name="action" id="action" value="display" />

		<label>Type de rapport&nbsp;:</label>
		<select name="type" id="type">
			<option selected="selected" value="0">- Choisir un type de rapport -</option>
			<option value="global">P&eacute;riode pour l'ensemble des &eacute;tudes</option>
		</select><br />

		<br />

		<label>Pour la p&eacute;riode&nbsp;:</label>
		<input type="text" name="date_debut" id="date_debut" onchange="javascript: adjustDateField('date_debut');" value="{$date_debut}" /> au&nbsp;&nbsp;<input type="text" name="date_fin" id="date_fin" onchange="javascript: adjustDateField('date_fin');" value="{$date_fin}" /><span class="note">AAAA-MM-JJ</span><br />

		<br />

		<label>Taxe TPS (%)&nbsp;:</label>
		<input type="text" name="taxe_tps" id="taxe_tps" onchange="javascript: adjustNumericField('taxe_tps', 1, 3, 0, 0);" value="5,000" /><br />

		<br />

		<label>Taxe TVQ (%)&nbsp;:</label>
		<input type="text" name="taxe_tvq" id="taxe_tvq" onchange="javascript: adjustNumericField('taxe_tvq', 1, 3, 0, 0);" value="8,500" /><br />
		
		<br />

		<label>Donn&eacute;es &agrave; afficher&nbsp;:</label>
		<input type="checkbox" name="cols[]" id="cols_honoraires_eligibles" class="radio" value="honoraires_eligibles" checked="checked" /> Honoraires &eacute;ligibles <br />
		<input type="checkbox" name="cols[]" id="cols_honoraires_non_eligibles" class="radio" value="honoraires_non_eligibles" checked="checked" /> Honoraires non &eacute;ligibles <br />
		<input type="checkbox" name="cols[]" id="cols_debourses" class="radio" value="debourses" checked="checked" /> D&eacute;bours&eacute;s <br />
		<input type="checkbox" name="cols[]" id="cols_ristourne_a_recevoir" class="radio" value="ristourne_a_recevoir" checked="checked" /> Ristourne &agrave; recevoir <br />
		<input type="checkbox" name="cols[]" id="cols_ristourne_a_verser" class="radio" value="ristourne_a_verser" checked="checked" /> Ristourne &agrave; verser <br />
		<input type="checkbox" name="rows[]" id="rows_paiements_recus" class="radio" value="paiements_recus" checked="checked" /> Paiements re&ccedil;us <br />
		<input type="checkbox" name="rows[]" id="rows_paiements_emis" class="radio" value="paiements_emis" checked="checked" /> Paiements &eacute;mis <br />

		<br />

		<input type="button" class="submit" value="Afficher le rapport" onclick="javascript: submitForm('display');" /><br />

	</form>

	<br />
	<br />
	<br />

{/if}

{include file="footer.tpl"}
  
