{include file="header.tpl"}

	{$errors}

	{if $nologin != 1}

		{literal}
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = '';
			var action = document.getElementById('action');
			var annee = document.getElementById('annee');
			var paiementKey = document.getElementById('paiementKey');

			switch (thisAction) {
				case 'supprimer':
					var anneeDelValue = getSelectValue(document.getElementById('anneeDel'));
					var paiementKeyDelValue = getSelectValue(document.getElementById('paiementKeyFieldDel_' + anneeDelValue));
					if (paiementKeyDelValue == '' || paiementKeyDelValue == '0' || paiementKeyDelValue == 'undefined' || paiementKeyDelValue == null) {
						errorMsg += 'Vous devez choisir un paiement.\n';
					}
					action.value = 'supprimer';
					paiementKey.value = paiementKeyDelValue;
					break;
			}

			if (errorMsg == '') {
				document.getElementById('paiementForm').submit();
			} else {
				alert(errorMsg);
				return false;
			}
		}

		function loadPaiements(form) {
			var paiementKeyFields = new Array();
			{/literal}{foreach name=paiementForeach from=$paiementsArray key=annee item=null}{literal}
				paiementKeyFields[{/literal}{$smarty.foreach.paiementForeach.index}{literal}] = 'paiementKeyField' + form + '_{/literal}{$annee}{literal}';
			{/literal}{/foreach}{literal}

			for (i = 0; i < paiementKeyFields.length; i++) {
				document.getElementById(paiementKeyFields[i]).style.display = 'none';
				document.getElementById(paiementKeyFields[i]).disabled = true;
			}

			var anneeValue = getSelectValue(document.getElementById('annee' + form));
			var paiementKeyField = document.getElementById('paiementKeyField' + form + '_' + anneeValue);
			var paiementKeyFieldDefault = document.getElementById('paiementKeyField' + form + '_default');

			if (
				anneeValue != '' && anneeValue != '0' && anneeValue != 'undefined' &&
				paiementKeyField != '' && paiementKeyField != 'undefined' && paiementKeyField != null
			) {
				paiementKeyFieldDefault.style.display = 'none';
				paiementKeyField.style.display = 'inline';
				paiementKeyField.disabled = false;
			} else {
				paiementKeyFieldDefault.style.display = 'inline';
			}
		}
		
		
		</script>
		{/literal}

		<form action="riq-paiement.php" method="post" id="paiementForm">
			<input type="hidden" name="form" id="form" value="suppression" />
			<input type="hidden" name="action" id="action" value="" />
			<input type="hidden" name="paiementKey" id="paiementKey" value="" />			

			<h1>Suppression d'un paiement RIQ</h1>

			<div class="noticeDiv">
				{foreach name=notices from=$noticeArray item=notice}
					{if $smarty.foreach.notices.first}<br />{/if}
					<div class="noticeBox">{$notice}</div>
				{/foreach}
			</div>
				<div class="errorDiv">
				{foreach name=errors from=$errorArray item=erreur}
					{if $smarty.foreach.errors.first}<br />{/if}
					<div class="errorBox">{$erreur}</div>
				{/foreach}
			</div>
				<br clear="all" />
	
			<label>Veuillez choisir l'ann&eacute;e&nbsp;:</label>
			<select name="anneeDel" id="anneeDel" onchange="javascript: loadPaiements('Del');">
				<option selected="selected" value="0">- Choisir une ann&eacute;e -</option>
				{foreach from=$anneesArray item=annee key=clef}
					<option value="{$clef}">{$annee}</option>
				{/foreach}
			</select><br />
	
			<br />

			<label>Paiement&nbsp;:</label>

			<select name="paiementKeyFieldDel_default" id="paiementKeyFieldDel_default" disabled="disabled">
				<option selected="selected" value="0">- Choisir un paiement -</option>
			</select>

			{foreach from=$paiementsArray key=annee item=null}
			<select name="paiementKeyFieldDel_{$annee}" id="paiementKeyFieldDel_{$annee}" style="display:none;" disabled="disabled">
				<option selected="selected" value="0">- Choisir un paiement -</option>
				{foreach from=$paiementsArray.$annee key=paiementClef item=paiementInfo }
					<option value="{$paiementClef}">{$paiementInfo}</option>
				{/foreach}
			</select>
			{/foreach}

			<br />
			<br />

			<input type="button" class="submit" value="Supprimer le paiement" onclick="javascript: submitForm('supprimer');" /><br />

			<br />
			<br />
			<br />

		</form>

	{/if}

{include file="footer.tpl"}
