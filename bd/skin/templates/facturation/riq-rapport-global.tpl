{include file="header.tpl"}

{if $nologin != 1}

	<center>
		<h1>Rapport des ristournes &agrave; verser</h1>
		<strong>{$dateDebut}</strong> au <strong>{$dateFin}</strong><br />
	</center>

	<div id="printpage"><a href="javascript: window.print();"><img class="print" src="{$SKIN_URL}/img/printer.jpg" alt="Imprimer" /> Imprimer le rapport</a></div>

	<br />
	<br />

	{assign var="ln" value="0"}
	
	<table cellpadding="0" cellspacing="0" border="0" class="rapport_global">

		<tr class="titre">
			<td width="260px" style="text-align:left;">Nom de l'&eacute;tude</td>
			{if $cols.honoraires_eligibles}<td width="80px" style="text-align:center;">Honoraires &eacute;ligibles</td>{/if}
			{if $cols.honoraires_non_eligibles}<td width="80px" style="text-align:center;">Honoraires non &eacute;ligibles</td>{/if}
			{if $cols.debourses}<td width="80px" style="text-align:center;">D&eacute;bours&eacute;s</td>{/if}
			{if $cols.ristourne_a_recevoir}<td width="80px" style="text-align:center;">Ristourne &agrave; recevoir</td>{/if}
			{if $cols.ristourne_a_verser}<td width="80px" style="text-align:center;">Ristourne &agrave; verser</td>{/if}
		</tr>

		{foreach name=entriesForeach from=$entriesArray key=etudeKey item=etude}

			{if $j++ is odd by 1}
				<tr class="etude even">
			{else}
				<tr class="etude odd">
			{/if}
				<td style="text-align:left;">{$etude.etude_nom}</td>
				{if $cols.honoraires_eligibles}<td style="text-align:right;">{$etude.honoraires_eligibles|toMoneyFormat}</td>{/if}
				{if $cols.honoraires_non_eligibles}<td style="text-align:right;">{$etude.honoraires_non_eligibles|toMoneyFormat}</td>{/if}
				{if $cols.debourses}<td style="text-align:right;">{$etude.debourses|toMoneyFormat}</td>{/if}
				{if $cols.ristourne_a_recevoir}<td style="text-align:right;">{$etude.ristourne_a_recevoir|toMoneyFormat}</td>{/if}				
				{if $cols.ristourne_a_verser}<td style="text-align:right;">{$etude.ristourne_a_verser|toMoneyFormat}</td>{/if}
			</tr>

		{/foreach}

		<tr class="total_ristournes">
			<td style="text-align:left;">Sous-Total&nbsp;:</td>
			{if $cols.honoraires_eligibles}<td style="text-align:right;">{$sousTotalArray.honoraires_eligibles|toMoneyFormat}</td>{/if}
			{if $cols.honoraires_non_eligibles}<td style="text-align:right;">{$sousTotalArray.honoraires_non_eligibles|toMoneyFormat}</td>{/if}
			{if $cols.debourses}<td style="text-align:right;">{$sousTotalArray.debourses|toMoneyFormat}</td>{/if}
			{if $cols.ristourne_a_recevoir}<td style="text-align:right;">{$sousTotalArray.ristourne_a_recevoir|toMoneyFormat}</td>{/if}
			{if $cols.ristourne_a_verser}<td style="text-align:right;">{$sousTotalArray.ristourne_a_verser|toMoneyFormat}</td>{/if}
		</tr>

		<tr class="total_ristournes">
			<td style="text-align:left;">TPS (&nbsp;{$taxeTPS}%&nbsp;)&nbsp;</td>
			{if $cols.honoraires_eligibles}<td style="text-align:right;">&nbsp;</td>{/if}
			{if $cols.honoraires_non_eligibles}<td style="text-align:right;">&nbsp;</td>{/if}
			{if $cols.debourses}<td style="text-align:right;">&nbsp;</td>{/if}
			{if $cols.ristourne_a_recevoir}<td style="text-align:right;">{$taxesArray.TPS.ristourne_a_recevoir|toMoneyFormat}</td>{/if}
			{if $cols.ristourne_a_verser}<td style="text-align:right;">{$taxesArray.TPS.ristourne_a_verser|toMoneyFormat}</td>{/if}
		</tr>

		<tr class="total_ristournes">
			<td style="text-align:left;">TVQ (&nbsp;{$taxeTVQ}%&nbsp;)&nbsp;</td>
			{if $cols.honoraires_eligibles}<td style="text-align:right;">&nbsp;</td>{/if}
			{if $cols.honoraires_non_eligibles}<td style="text-align:right;">&nbsp;</td>{/if}
			{if $cols.debourses}<td style="text-align:right;">&nbsp;</td>{/if}
			{if $cols.ristourne_a_recevoir}<td style="text-align:right;">{$taxesArray.TVQ.ristourne_a_recevoir|toMoneyFormat}</td>{/if}
			{if $cols.ristourne_a_verser}<td style="text-align:right;">{$taxesArray.TVQ.ristourne_a_verser|toMoneyFormat}</td>{/if}
		</tr>

		<tr class="total_ristournes">
			<td style="text-align:left;">Total&nbsp;:</td>
			{if $cols.honoraires_eligibles}<td style="text-align:right;">{$sousTotalArray.honoraires_eligibles|toMoneyFormat}</td>{/if}
			{if $cols.honoraires_non_eligibles}<td style="text-align:right;">{$sousTotalArray.honoraires_non_eligibles|toMoneyFormat}</td>{/if}
			{if $cols.debourses}<td style="text-align:right;">{$sousTotalArray.debourses|toMoneyFormat}</td>{/if}
			{if $cols.ristourne_a_recevoir}<td style="text-align:right;">{$totalArray.ristourne_a_recevoir|toMoneyFormat}</td>{/if}
			{if $cols.ristourne_a_verser}<td style="text-align:right;">{$totalArray.ristourne_a_verser|toMoneyFormat}</td>{/if}
		</tr>

		{if $rows.paiements_recus}
		{foreach name=paiementsRecusForeach from=$paiementsRecusArray key=p item=paiement}

			<tr class="paiement">
				<td colspan="{$colspan}" style="text-align:left;">Paiement re&ccedil;u durant la p&eacute;riode ({$paiement.date_debut} au {$paiement.date_fin})</td>
				{if $cols.ristourne_a_recevoir}<td style="text-align:right;">{$paiement.montant|toMoneyFormat}</td>{/if}
				{if $cols.ristourne_a_verser}<td style="text-align:right;">&nbsp;</td>{/if}
			</tr>

		{/foreach}
		{/if}

		{if $rows.paiements_emis}
		{foreach name=paiementsEmisForeach from=$paiementsEmisArray key=p item=paiement}

			<tr class="paiement">
				<td colspan="{$colspan}" style="text-align:left;">
					Paiement &eacute;mis en date du {$paiement.date}
					{if $paiement.numero_facture != "" || $paiement.numero_cheque != ""}
						({if $paiement.numero_facture != ""}{$paiement.numero_facture}{/if}{if $paiement.numero_cheque != ""} {$paiement.numero_cheque}{/if})
					{/if}
				</td>
				{if $cols.ristourne_a_recevoir}<td style="text-align:right;">&nbsp;</td>{/if}
				{if $cols.ristourne_a_verser}<td style="text-align:right;">{$paiement.montant|toMoneyFormat}</td>{/if}
			</tr>

		{/foreach}
		{/if}

		{if $cols.ristourne_a_recevoir || $cols.ristourne_a_verser}
		<tr class="total_paiements">
			<td colspan="{$colspan}" style="text-align:left;">Total des paiements&nbsp;:</td>
			{if $cols.ristourne_a_recevoir}<td style="text-align:right;">{$totalArray.paiements_recus|toMoneyFormat}</td>{else}<td>&nbsp;</td>{/if}
			{if $cols.ristourne_a_verser}<td style="text-align:right;">{$totalArray.paiements_emis|toMoneyFormat}</td>{else}<td>&nbsp;</td>{/if}
		</tr>
		{/if}

		<tr class="solde">
			<td colspan="{$colspan}" style="text-align:left;">Solde&nbsp;:</td>
			{if $cols.ristourne_a_recevoir}<td style="text-align:right;">{$solde_a_recevoir|toMoneyFormat}</td>{else}<td>&nbsp;</td>{/if}
			{if $cols.ristourne_a_verser}<td style="text-align:right;">{$solde_a_verser|toMoneyFormat}</td>{else}<td>&nbsp;</td>{/if}
		</tr>

	</table><br />

{/if}

{include file="footer.tpl"}
