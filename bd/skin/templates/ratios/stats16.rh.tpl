<!--<h2>{$soustitre}</h2>-->

<table class="stats stats16 {$nomAction}" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<td class="description l"><span class="soustitre">{$soustitre}</span></td>
			{foreach from=$ratioz item=ratio name=ratiosTop}
				<td class="nom">{$smarty.foreach.ratiosTop.index+1}</td>
			{/foreach}
			<td class="moyenne">Total</td>
			{if $ratioz|@count lt 12} <td class="tampon">&nbsp;</td> {/if}
		</tr>
	</thead>
	<tbody>
		{foreach from=$rowz item=row name=lesRows}
			<tr>
				{if $row->description == ''}
					<td class="description {$row->type} {$row->type2} {$row->type3} l">&nbsp;</td>
				{else}
					<td class="description {$row->type} {$row->type2} {$row->type3} l">{$row->description}</td>
				{/if}
				{assign var=mesCells value=$row->data}
				{foreach from=$mesCells item=uneCell name=lesCells}
					{if $smarty.foreach.lesCells.index == 0}
						<td class="valeur {$row->type} {$row->type2} {$row->type3} l2">
					{else}
						<td class="valeur {$row->type} {$row->type2} {$row->type3}">
					{/if}

					{if $row->type == 'sectionName'}
						&nbsp;
					{else}
						{if $row->unit === 'bool'}
							{if $uneCell}
								Oui
							{else}
								-
							{/if}
						{elseif $row->unit === 'cad'}
							{$row->toFrenchFormatHtml($uneCell)}&nbsp;$
						{elseif $row->unit === 'percent'}
							{$row->toFrenchFormatHtml($uneCell)}&nbsp;%
						{else}
							{if $uneCell === null || $uneCell === false || $uneCell === ''}
								-
							{else}
								{$row->toFrenchFormatHtml($uneCell)}
							{/if}
						{/if}
					{/if}
					</td>
					
					{if $smarty.foreach.lesRows.first && $smarty.foreach.lesCells.last}
						{assign var="honorairesTotal" value=$row->getTotal()}
					{/if}
					
				{/foreach}
				{if $ratioz|@count lt 12}
					<td class="valeur {$row->type} {$row->type2} {$row->type3} total">
				{else}
					<td class="valeur {$row->type} {$row->type2} {$row->type3} total r">
				{/if}
					{if $row->type == 'sectionName'}
						&nbsp;
					{else}
						{if $row->getTotal() === null}
							-
						{elseif $row->unit === 'cad'}
							{$row->toFrenchFormatHtml($row->getTotal())}&nbsp;$
							{assign var="sectionTotal" value=$row->getTotal()}
						{elseif $row->unit === 'percent'}
								{math assign="totalPourcent" equation="y / x * 100" x=$honorairesTotal y=$sectionTotal}
								{$row->toFrenchFormatHtml($totalPourcent)}&nbsp;%
						{elseif $row->unit === 'bool'}
								{if $row->getTotal()}
									Oui
								{else}
									-
								{/if}
						{else}
							{$row->toFrenchFormatHtml($row->getTotal())}
						{/if}
					{/if}
				</td>
				{if $ratioz|@count lt 12}<td class="{$row->type} {$row->type2} {$row->type3} total tampon r">&nbsp;</td> {/if}
			</tr>
		{/foreach}
	</tbody>
</table>
