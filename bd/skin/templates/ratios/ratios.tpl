{include file="header.tpl"}

<div id="ratios">
	<form action="ratios.php" method="post" id="ratiosForm">
		<input type="hidden" name="action" id="action" value="display" />
		<input type="hidden" name="etudeKey" id="etudeKey" value="{$etudeKey}" />
		<input type="hidden" name="annee" id="annee" value="{$annee}" />
		<input type="hidden" name="currentForm" id="currentForm" value="{$currentForm}" />
		<input type="hidden" name="requestForm" id="requestForm" value="" />

		<div id="infosEtude">{$nomEtude}&nbsp;-&nbsp;{$annee}</div>

		<div id="etapes">&Eacute;tape {$currentForm}/6</div>

		<br clear="all" />

		{if $noticeArray|@count gt 0}
		<div class="noticeDiv">
			{foreach from=$noticeArray item=notice}
				<div class="noticeBox">{$notice}</div>
			{/foreach}
		</div>
		{/if}

		{if $errorArray|@count gt 0}
		<div class="errorDiv">
			{foreach from=$errorArray item=erreur}
				<div class="errorBox">{$erreur}</div>
			{/foreach}
		</div>
		{/if}

		{if $warningArray|@count gt 0}
		<div class="warningDiv">
			{foreach from=$warningArray item=warning}
				<div class="warningBox">{$warning}</div>
			{/foreach}
		</div>
		{/if}

		<br clear="all" />

		{if !$ratioClosed}
			{include file="ratios/"|cat:$form|cat:".tpl"}
		{/if}

		{if $currentForm == 1}

			<div id="navigation">
				<br clear="all" />
				<input type="button" value="&Eacute;tape suivante &raquo;" class="navigation submit" onclick="submitRatiosForm('1', '2');" />
			</div>

		{elseif $currentForm == 6}

			<div id="navigation">
				<br clear="all" />
				<input type="button" value="&laquo; &Eacute;tape pr&eacute;c&eacute;dente" class="navigation submit" onclick="submitRatiosForm('6', '5');" />
			</div>

		{else}

			<div id="navigation">
				<br clear="all" />
				<input type="button" value="&laquo; &Eacute;tape pr&eacute;c&eacute;dente" class="navigation submit" onclick="submitRatiosForm('{$currentForm}', '{math equation="x-y" x=$currentForm y="1"}');" />
				<input type="button" value="&Eacute;tape suivante &raquo;" class="navigation submit" onclick="submitRatiosForm('{$currentForm}', '{math equation="x+y" x=$currentForm y="1"}');" />
			</div>
		
		{/if}
	</form>
</div>

{include file="footer.tpl"}
