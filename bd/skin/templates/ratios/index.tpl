{include file="header.tpl"}

{$errors}

{assign var='annee_c0' value=$smarty.now|date_format:'%Y'}
{math equation="x-y" x=$annee_c0 y="5" assign='annee_p5'}
{math equation="x-y" x=$annee_c0 y="4" assign='annee_p4'}
{math equation="x-y" x=$annee_c0 y="3" assign='annee_p3'}
{math equation="x-y" x=$annee_c0 y="2" assign='annee_p2'}
{math equation="x-y" x=$annee_c0 y="1" assign='annee_p1'}
{math equation="x+y" x=$annee_c0 y="1" assign='annee_s1'}
{math equation="x+y" x=$annee_c0 y="2" assign='annee_s2'}
{math equation="x+y" x=$annee_c0 y="3" assign='annee_s3'}
{math equation="x+y" x=$annee_c0 y="4" assign='annee_s4'}
{math equation="x+y" x=$annee_c0 y="5" assign='annee_s5'}

{if $nologin != 1}

	{literal}
	<script language="JavaScript">

	function adjustDateField(fieldId) {
		var fieldElement = document.getElementById(fieldId);
		var fieldValue = fieldElement.value;
		fieldElement.value = fieldValue.replace(/\//g, '-');
	}

	function submitRatios() {
		var errorMsg = '';
		var action = 'start';
		var etudeKey = document.getElementById('etudeField').value;
		var annee = document.getElementById('anneeField').value;

		if (etudeKey == '' || etudeKey == '0' || etudeKey == 'undefined') {
			errorMsg += 'Vous devez choisir une \351tude.\n';
		}
		if (annee == '' || annee == '0' || annee == 'undefined') {
			errorMsg += 'Vous devez choisir une ann\351e.\n';
		}

		if (errorMsg != '') {
			alert(errorMsg);
			return false;
		}

		var baseurl = {/literal}'{$BASEURL}';{literal}
		var url = baseurl + 'ratios/ratios.php?action=' + action + '&etudeKey=' + etudeKey + '&annee=' + annee;

		window.location = url;
	}

	var tableau = '';
	function submitStats() {
		var errorMsg = '';
		var action = 'custom';
		var type = document.getElementById('type').value;
		var etudeKey = document.getElementById('etudeKey2').value;
		var annee = document.getElementById('annee').value;
		var size = document.getElementById('size').value;
		tableau = '';

		$(".tableauStats").each(function() {
			console.log("test=" + $(this).is(':checked'));
			if ($(this).is(':checked') == true) {
				tableau = tableau + $(this).val() + "-";
			}
		});

		if (type == '' || type == '0' || type == 'undefined') {
			errorMsg += 'Vous devez sp\351cifier un type de cumulatif\n';
		}
		if ((type == 'stats3' || type == 'stats11' || type == 'stats5') && (etudeKey == '' || etudeKey == '0' || etudeKey == 'undefined')) {
			errorMsg += 'Vous devez choisir une \351tude.\n';
		}
		if (annee == '' || annee == '0' || annee == 'undefined') {
			errorMsg += 'Vous devez choisir une ann\351e.\n';
		}
		if (tableau == ''){
			errorMsg += 'Vous devez choisir un moins un tableau \340 afficher.\n';
		}

		if (errorMsg != '') {
			alert(errorMsg);
			return false;
		}

		var baseurl = {/literal}'{$BASEURL}';{literal}
		var url = baseurl + 'ratios/stats.php?action=' + action + '&type=' + type + '&etudeKey=' + etudeKey + '&annee=' + annee + '&size=' + size+ '&tableau=' + tableau;

		window.open(url, 'statistiques');
	}

	function submitCumulAcces() {
		var baseurl = {/literal}'{$BASEURL}';{literal}
		var url = baseurl + 'ratios/index.php?action=updateCumulAcces&cumul[{/literal}{$annee_p2}{literal}]=' + getSelectValue(document.getElementById('cumul_{/literal}{$annee_p2}{literal}')) + '&cumul[{/literal}{$annee_p1}{literal}]=' + getSelectValue(document.getElementById('cumul_{/literal}{$annee_p1}{literal}')) + '&cumul[{/literal}{$annee_c0}{literal}]=' + getSelectValue(document.getElementById('cumul_{/literal}{$annee_c0}{literal}')) + '&cumul[{/literal}{$annee_s1}{literal}]=' + getSelectValue(document.getElementById('cumul_{/literal}{$annee_s1}{literal}'));

		window.location = url;
	}

	function submitAdminControls(actionValue) {
		var action = document.getElementById('action');
		action.value = actionValue;
		document.getElementById('ratioAdminForm').submit();
	}

	function chkType() {
		var typeElement = document.getElementById('type');
		var sizeElement = document.getElementById('size');
		var etudeElement = document.getElementById('etudeKey2');
		var autresElement = document.getElementById('autres');

		var adminBool = {/literal}{$admin}{literal};

		if (getSelectValue(typeElement) == 'stats16') {
			sizeElement.disabled = false;
		} else {
			sizeElement.disabled = true;
		}

		if (getSelectValue(typeElement) == 'stats3' || getSelectValue(typeElement) == 'stats11' || getSelectValue(typeElement) == 'stats5') {
			etudeElement.disabled = false;
		} else {
			etudeElement.disabled = true;
		}

		if (adminBool) {
			autresElement.disabled = false;
		} else {
			if (getSelectValue(typeElement) == 'stats3' || getSelectValue(typeElement) == 'stats11' || getSelectValue(typeElement) == 'stats5') {
				autresElement.disabled = false;
			} else {
				autresElement.disabled = true;
			}
		}
	}

	function chkAnnee() {
		var adminBool = {/literal}{$admin}{literal};

		if (!adminBool) {
			var typeElement = document.getElementById('type');
			var anneeElement = document.getElementById('annee');

			var accesCumulAnneesList = new Array({/literal}{foreach from=$accesCumulAnneesList item=annee name=_accesCumulAnneesList}'{$annee}'{if !$smarty.foreach._accesCumulAnneesList.last},{/if}{/foreach}{literal});
			var accesStats16AnneesList = new Array({/literal}{foreach from=$accesStats16AnneesList item=annee name=_accesStats16AnneesList}'{$annee}'{if !$smarty.foreach._accesStats16AnneesList.last},{/if}{/foreach}{literal});

			bool = 0;
			for (var y in accesCumulAnneesList) {
				if (getSelectValue(anneeElement) == accesCumulAnneesList[y]) {
					bool = 1;
				}
			}

			if (bool) {
				bool = 0;
				for (var y in accesStats16AnneesList) {
					if (getSelectValue(anneeElement) == accesStats16AnneesList[y]) {
						bool = 1;
					}
				}
			}

			var optionExistsBool = 0;
			var o;
			for (o = typeElement.length - 1; o >= 0; o--) {
				if (typeElement.options[o].value == 'stats16') {
					optionExistsBool = 1;
				}
			}

			if (!bool) {
				if (optionExistsBool) {
					var t;
					for (t = typeElement.length - 1; t >= 0; t--) {
						if (typeElement.options[t].value == 'stats16') {
							typeElement.remove(t);
						}
					}
				}

			} else {
				if (!optionExistsBool) {
					var newOptionElement = document.createElement('option');
					newOptionElement.text = 'Cumulatif de 1 an pour l\'ensemble des \351tudes';
					newOptionElement.value = 'stats16';
					try {
						typeElement.add(newOptionElement, typeElement.options[1]); // standards compliant; doesn't work in IE
					} catch(ex) {
						typeElement.add(newOptionElement, 1); // IE only
					}
				}
			}
		}
	}

	</script>
	{/literal}

	<h1>Saisie de donn&eacute;es financi&egrave;res</h1>

	<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
	<select name="etudeKey" id="etudeField">
	{if $admin}
		<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
		{foreach from=$etudesArray item=etude key=clef}
			<option value="{$clef}">{$etude}</option>
		{/foreach}
	{else}
		{*<option selected="selected" value="0">- Choisir une &eacute;tude -</option>*}
		{foreach from=$etudesArray item=etude key=clef}
			<option value="{$clef}">{$etude}</option>
		{/foreach}
	{/if}
	</select>

	<br clear="all" />
	<br clear="all" />

	<label>Donn&eacute;es pour la p&eacute;riode financi&egrave;re se terminant en&nbsp;:</label>

	{if $admin}
		<select name="anneeField" id="anneeField">
			<option value="2007">2007</option>
			<option value="2008">2008</option>
			<option value="2009">2009</option>
			<option value="2010">2010</option>
			<option value="2011">2011</option>
			<option value="2012">2012</option>
			<option value="2013">2013</option>
			<option value="2014">2014</option>
			<option value="2015">2015</option>
			<option value="2016">2016</option>
			<option value="2017">2017</option>
			<option value="2018">2018</option>
			<option value="2019">2019</option>
			<option value="2020">2020</option>
		</select>
	{else}
		<select name="anneeField" id="anneeField">
			<option value="{$annee_p3}">{$annee_p3}</option>
			<option value="{$annee_p2}">{$annee_p2}</option>
			<option value="{$annee_p1}" selected="selected">{$annee_p1}</option>

			{if $smarty.session.user_etudes_key == 7}
			<option value="{$annee_c0}">{$annee_c0}</option>
			{/if}
			{*<option value="{$annee_c0}">{$annee_c0}</option>*}
			{*<option value="{$annee_s1}">{$annee_s1}</option>*}
		</select>
	{/if}

	<br clear="all"/>
	<br clear="all"/>

	<input type="button" class="submit" value="D&eacute;buter la saisie de donn&eacute;es" onclick="javascript: submitRatios();" />

	{*if $admin}
		<input type="button" class="submit" onclick="genererLien('txtLien','etudeField','anneeField','{$FULLURL}ratios/ratio.php','doEtape1')" value="G&eacute;n&eacute;rer le lien" />
		<br clear="all" />
		<textarea style="display:none; width:100%;" id="txtLien"></textarea>
	{/if*}

	<br clear="all" />
	<br clear="all" />

	<a href="{$BASEURL}docs/ratios/ratios_formulaire2015.pdf" title="Formulaire imprimable"><img src="{$SKINURL}/img/printer.jpg" alt="formulaire imprimable" class="print" />Cliquez ici pour obtenir une version imprimable du formulaire vierge.</a>

	<br clear="all" />
	<br clear="all" />
	<br clear="all" />
	<br clear="all" />

	<h1>Consultation de ratios</h1>

	<label>Type de cumulatif&nbsp;:</label>

	<select name="type" id="type" onchange="javascript: chkType();">
		<option value="0">- Choisir le type de cumulatif -</option>
		{if $admin}
			<option value="stats16">Cumulatif de 1 an pour l'ensemble des &eacute;tudes</option>
		{/if}
		<option value="stats11">Cumulatif de 1 an pour une &eacute;tude sp&eacute;cifique</option>
		<option value="stats3">Cumulatif de 3 ans pour une &eacute;tude sp&eacute;cifique</option>
		<option value="stats5">Cumulatif de 5 ans pour une &eacute;tude sp&eacute;cifique</option>
	</select>

	{if !$admin}
		<br clear="all" />
		<br clear="all" />
		<span class="note">* Vous devez avoir compl&eacute;t&eacute; le ratio de votre &eacute;tude <strong>pour l'ann&eacute;e s&eacute;lectionn&eacute;e</strong> afin d'avoir acc&egrave;s au comparatif de l'ensemble des &eacute;tudes.</span>
	{/if}

	<br clear="all" />
	<br clear="all" />

	{if $admin}
		<label for="etudeKey">Veuillez choisir l'&eacute;tude&nbsp;:</label>
		<select name="etudeKey" id="etudeKey2" disabled="disabled">
			<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
			{foreach from=$etudesArray item=etude key=clef}
				<option value="{$clef}">{$etude}</option>
			{/foreach}
		</select>
	{else}
		<label for="etudeKey">Veuillez choisir l'&eacute;tude&nbsp;:</label>
		<select name="etudeKey" id="etudeKey2" disabled="disabled">
			{foreach from=$etudesArray item=etude key=clef}
				<option value="{$clef}">{$etude}</option>
			{/foreach}
		</select>
	{/if}

	{if $admin}
		<br clear="all" />
		<br clear="all" />
		<label for="size">Nombre de notaires&nbsp;:</label>
		<select name="size" id="size" disabled="disabled">
			<option selected="selected" value="0">- Choisir le nombre de notaires -</option>
			<option value="1-4">De 3 &agrave; 4 notaires</option>
			<option value="5-8">De 5 &agrave; 8 notaires</option>
			<option value="9-999">De 9 notaires et plus</option>
		</select>
	{else}
		<br clear="all" />
		<br clear="all" />
		<label for="size">Nombre de notaires&nbsp;:</label>
		<select name="size" id="size" disabled="disabled">
			<option selected="selected" value="0">- Choisir le nombre de notaires -</option>
			<option value="1-4">De 3 &agrave; 4 notaires</option>
			<option value="5-8">De 5 &agrave; 8 notaires</option>
			<option value="9-999">De 9 notaires et plus</option>
		</select>
	{/if}

	<br clear="all" />
	<br clear="all" />


	<label for="annee">Donn&eacute;es pour la p&eacute;riode financi&egrave;re se terminant en&nbsp;:</label>

	{if $admin}
		<select name="annee" id="annee" onchange="javascript: chkAnnee();">
			<option value="2007">2007</option>
			<option value="2008">2008</option>
			<option value="2009">2009</option>
			<option value="2010">2010</option>
			<option value="2011">2011</option>
			<option value="2012">2012</option>
			<option value="2013">2013</option>
			<option value="2014">2014</option>
			<option value="2015">2015</option>
			<option value="2016">2016</option>
			<option value="2017">2017</option>
			<option value="2018">2018</option>
			<option value="2019">2019</option>
			<option value="2020">2020</option>
		</select>
	{else}
		<select name="annee" id="annee" onchange="javascript: chkAnnee();">
			<option value="{$annee_p5}">{$annee_p5}</option>
			<option value="{$annee_p4}">{$annee_p4}</option>
			<option value="{$annee_p3}">{$annee_p3}</option>
			<option value="{$annee_p2}">{$annee_p2}</option>
			<option value="{$annee_p1}" selected="selected">{$annee_p1}</option>
			<option value="{$annee_c0}">{$annee_c0}</option>
			<option value="{$annee_s1}">{$annee_s1}</option>
		</select>
	{/if}

	<br clear="all" />
	<br clear="all" />

	<label>Donn&eacute;es &agrave; afficher&nbsp;:</label><br />
	<input type="checkbox" id="donneesGenerales" value="donneesGenerales" name="tableau[]" checked="checked" class="tableauStats checkbox"/> <label for="donneesGenerales" style="display:inline;">Donn&eacute;es g&eacute;n&eacute;rales</label><br />
	<input type="checkbox" id="repartitionHonoraires" value="repartitionHonoraires" name="tableau[]" checked="checked" class="tableauStats checkbox"/> <label for="repartitionHonoraires" style="display:inline;">R&eacute;partition des honoraires</label><br />
	<input type="checkbox" id="rentabilite" value="rentabilite" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="rentabilite" style="display:inline;">Ratios de rentabilit&eacute; et de liquidit&eacute;</label><br />
	<input type="checkbox" id="recevableSalaires" value="recevableSalaires" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="recevableSalaires" style="display:inline;">Ratios des recevables et des salaires</label><br />
	<input type="checkbox" id="tarification" value="tarification" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="tarification" style="display:inline;">Ratios de tarification</label><br />
	<input type="checkbox" id="depenses" value="depenses" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="depenses" style="display:inline;">Ratios des d&eacute;penses</label><br />
	<input type="checkbox" id="production" value="production" checked="checked" name="tableau[]" class="tableauStats checkbox"/> <label for="production" style="display:inline;">Ratios de production</label><br />
	<input type="checkbox" id="statSalariales" value="statSalariales" name="tableau[]" checked="checked" class="tableauStats checkbox"/> <label for="statSalariales" style="display:inline;">Statistiques salariales</label><br />
	<input type="checkbox" id="autres" value="autres" disabled="disabled" name="tableau[]" class="tableauStats checkbox"/> <label for="autres" style="display:inline;">Rapport de la saisie de donn&eacute;es de votre &eacute;tude</label><br />

	<br clear="all" />

	<input type="button" class="submit" value="Consulter les donn&eacute;es" onclick="javascript: submitStats();" />
	{*&nbsp;&nbsp;&nbsp;&nbsp; <input type="button" class="submit" value="Imprimer les donn&eacute;es" onclick="javascript: submitPrint();" />*}


	{if $admin}
		<br clear="all" />
		<br clear="all" />

		<h1>Validation de ratios</h1>

		<form action="verif.php" method="get">
			<label for="etude">Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etude" id="etude">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				{foreach from=$etudesArray item=etude key=clef}
					<option value="{$clef}">{$etude}</option>
				{/foreach}
			</select>

			<br clear="all" />
			<br clear="all" />

			<label for="annee">Donn&eacute;es pour la p&eacute;riode financi&egrave;re se terminant en&nbsp;:</label>
			<select name="annee" id="annee" onchange="javascript: chkAnnee();">
				<option value="2007">2007</option>
				<option value="2008">2008</option>
				<option value="2009">2009</option>
				<option value="2010">2010</option>
				<option value="2011">2011</option>
				<option value="2012">2012</option>
				<option value="2013">2013</option>
				<option value="2014">2014</option>
				<option value="2015">2015</option>
				<option value="2016">2016</option>
				<option value="2017">2017</option>
				<option value="2018">2018</option>
				<option value="2019">2019</option>
				<option value="2020">2020</option>
			</select>

			<br clear="all" />
			<br clear="all" />

			<input type="submit" class="submit" value="Télécharger" />
		</form>






		<form action="index.php" method="post" id="ratioAdminForm" name="ratioAdminForm">
			<input type="hidden" name="action" id="action" value="" />

		<br clear="all" />
		<br clear="all" />

		<hr />

		<br clear="all" />
		<br clear="all" />

		<h1>Options aux supers administrateurs</h1>

		<h2>Acc&egrave;s au formulaire</h2>

		<table cellpadding="0" cellspacing="0" border="0" class="noborder">
			<tr>
				<td width="75px;"><strong>Ann&eacute;e</strong></td>
				<td width="75px;"><strong>Ouvert&nbsp;?</strong></td>
				<td width="150px;"><strong>Date d&eacute;but (AAAA-MM-JJ)</strong></td>
				<td><strong>Date fin (AAAA-MM-JJ)</strong></td>
			</tr>
			<tr>
				<td>{$annee_p3}</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_{$annee_p3}" id="acces_formulaire_ouvert_{$annee_p3}" value="1" class="checkbox" {if $ratiosControlsArray.$annee_p3.acces_formulaire.ouvert}checked="checked"{/if}/></td>
				<td><input type="text" name="acces_formulaire_date_debut_{$annee_p3}" id="acces_formulaire_date_debut_{$annee_p3}" onchange="javascript: adjustDateField('acces_formulaire_date_debut_{$annee_p3}');" value="{$ratiosControlsArray.$annee_p3.acces_formulaire.date_debut}" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_{$annee_p3}" id="acces_formulaire_date_fin_{$annee_p3}" onchange="javascript: adjustDateField('acces_formulaire_date_fin_{$annee_p3}');" value="{$ratiosControlsArray.$annee_p3.acces_formulaire.date_fin}" /></td>
			</tr>
			<tr>
				<td>{$annee_p2}</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_{$annee_p2}" id="acces_formulaire_ouvert_{$annee_p2}" value="1" class="checkbox" {if $ratiosControlsArray.$annee_p2.acces_formulaire.ouvert}checked="checked"{/if}/></td>
				<td><input type="text" name="acces_formulaire_date_debut_{$annee_p2}" id="acces_formulaire_date_debut_{$annee_p2}" onchange="javascript: adjustDateField('acces_formulaire_date_debut_{$annee_p2}');" value="{$ratiosControlsArray.$annee_p2.acces_formulaire.date_debut}" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_{$annee_p2}" id="acces_formulaire_date_fin_{$annee_p2}" onchange="javascript: adjustDateField('acces_formulaire_date_fin_{$annee_p2}');" value="{$ratiosControlsArray.$annee_p2.acces_formulaire.date_fin}" /></td>
			</tr>
			<tr>
				<td>{$annee_p1}</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_{$annee_p1}" id="acces_formulaire_ouvert_{$annee_p1}" value="1" class="checkbox" {if $ratiosControlsArray.$annee_p1.acces_formulaire.ouvert}checked="checked"{/if}/></td>
				<td><input type="text" name="acces_formulaire_date_debut_{$annee_p1}" id="acces_formulaire_date_debut_{$annee_p1}" onchange="javascript: adjustDateField('acces_formulaire_date_debut_{$annee_p1}');" value="{$ratiosControlsArray.$annee_p1.acces_formulaire.date_debut}" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_{$annee_p1}" id="acces_formulaire_date_fin_{$annee_p1}" onchange="javascript: adjustDateField('acces_formulaire_date_fin_{$annee_p1}');" value="{$ratiosControlsArray.$annee_p1.acces_formulaire.date_fin}" /></td>
			</tr>
			<tr>
				<td>{$annee_c0}</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_{$annee_c0}" id="acces_formulaire_ouvert_{$annee_c0}" value="1" class="checkbox" {if $ratiosControlsArray.$annee_c0.acces_formulaire.ouvert}checked="checked"{/if}/></td>
				<td><input type="text" name="acces_formulaire_date_debut_{$annee_c0}" id="acces_formulaire_date_debut_{$annee_c0}" onchange="javascript: adjustDateField('acces_formulaire_date_debut_{$annee_c0}');" value="{$ratiosControlsArray.$annee_c0.acces_formulaire.date_debut}" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_{$annee_c0}" id="acces_formulaire_date_fin_{$annee_c0}" onchange="javascript: adjustDateField('acces_formulaire_date_fin_{$annee_c0}');" value="{$ratiosControlsArray.$annee_c0.acces_formulaire.date_fin}" /></td>
			</tr>
			<tr>
				<td>{$annee_s1}</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_{$annee_s1}" id="acces_formulaire_ouvert_{$annee_s1}" value="1" class="checkbox" {if $ratiosControlsArray.$annee_s1.acces_formulaire.ouvert}checked="checked"{/if}/></td>
				<td><input type="text" name="acces_formulaire_date_debut_{$annee_s1}" id="acces_formulaire_date_debut_{$annee_s1}" onchange="javascript: adjustDateField('acces_formulaire_date_debut_{$annee_s1}');" value="{$ratiosControlsArray.$annee_s1.acces_formulaire.date_debut}" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_{$annee_s1}" id="acces_formulaire_date_fin_{$annee_s1}" onchange="javascript: adjustDateField('acces_formulaire_date_fin_{$annee_s1}');" value="{$ratiosControlsArray.$annee_s1.acces_formulaire.date_fin}" /></td>
			</tr>
			<tr>
				<td>{$annee_s2}</td>
				<td><input type="checkbox" name="acces_formulaire_ouvert_{$annee_s2}" id="acces_formulaire_ouvert_{$annee_s2}" value="1" class="checkbox" {if $ratiosControlsArray.$annee_s2.acces_formulaire.ouvert}checked="checked"{/if}/></td>
				<td><input type="text" name="acces_formulaire_date_debut_{$annee_s2}" id="acces_formulaire_date_debut_{$annee_s2}" onchange="javascript: adjustDateField('acces_formulaire_date_debut_{$annee_s2}');" value="{$ratiosControlsArray.$annee_s2.acces_formulaire.date_debut}" /></td>
				<td><input type="text" name="acces_formulaire_date_fin_{$annee_s2}" id="acces_formulaire_date_fin_{$annee_s2}" onchange="javascript: adjustDateField('acces_formulaire_date_fin_{$annee_s2}');" value="{$ratiosControlsArray.$annee_s2.acces_formulaire.date_fin}" /></td>
			</tr>
		</table>

		<input type="button" class="submit" value="Mettre &agrave; jour" onclick="javascript: submitAdminControls('setAccesFormulaire');" />


		<br clear="all" />
		<br clear="all" />
		<br clear="all" />

		<h2>Acc&egrave;s au cumulatif d'ensemble</h2>

		<table cellpadding="0" cellspacing="0" border="0" class="noborder">
			<tr>
				<td width="75px;"><strong>Ann&eacute;e</strong></td>
				<td><strong>Ouvert&nbsp;?</strong></td>
			</tr>
			<tr>
				<td>{$annee_p3}</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_{$annee_p3}" id="acces_cumulatif_ensemble_{$annee_p3}" value="1" class="checkbox" {if $accesCumulArray.$annee_p3}checked="checked"{/if}/></td>
			</tr>
			<tr>
				<td>{$annee_p2}</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_{$annee_p2}" id="acces_cumulatif_ensemble_{$annee_p2}" value="1" class="checkbox" {if $accesCumulArray.$annee_p2}checked="checked"{/if}/></td>
			</tr>
			<tr>
				<td>{$annee_p1}</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_{$annee_p1}" id="acces_cumulatif_ensemble_{$annee_p1}" value="1" class="checkbox" {if $accesCumulArray.$annee_p1}checked="checked"{/if}/></td>
			</tr>
			<tr>
				<td>{$annee_c0}</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_{$annee_c0}" id="acces_cumulatif_ensemble_{$annee_c0}" value="1" class="checkbox" {if $accesCumulArray.$annee_c0}checked="checked"{/if}/></td>
			</tr>
			<tr>
				<td>{$annee_s1}</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_{$annee_s1}" id="acces_cumulatif_ensemble_{$annee_s1}" value="1" class="checkbox" {if $accesCumulArray.$annee_s1}checked="checked"{/if}/></td>
			</tr>
			<tr>
				<td>{$annee_s2}</td>
				<td><input type="checkbox" name="acces_cumulatif_ensemble_{$annee_s2}" id="acces_cumulatif_ensemble_{$annee_s2}" value="1" class="checkbox" {if $accesCumulArray.$annee_s2}checked="checked"{/if}/></td>
			</tr>
		</table>

		<input type="button" class="submit" value="Mettre &agrave; jour" onclick="javascript: submitAdminControls('setAccesCumul');" />

		<br clear="all" />
		<br clear="all" />
		<br clear="all" />

		<h2>Valeurs annuelles</h2>

		<table cellpadding="0" cellspacing="0" border="0" class="noborder">
			<tr>
				<td width="75px;"><strong>Ann&eacute;e</strong></td>
				<td><strong>Pr&eacute;l&egrave;vements (P)</strong></td>
			</tr>
			<tr>
				<td>{$annee_p3}</td>
				<td><input type="text" name="valeurs_prelevements_{$annee_p3}" id="valeurs_prelevements_{$annee_p3}" value="{$ratiosControlsArray.$annee_p3.valeurs_annuelles.prelevements}" /></td>
			</tr>
			<tr>
				<td>{$annee_p2}</td>
				<td><input type="text" name="valeurs_prelevements_{$annee_p2}" id="valeurs_prelevements_{$annee_p2}" value="{$ratiosControlsArray.$annee_p2.valeurs_annuelles.prelevements}" /></td>
			</tr>
			<tr>
				<td>{$annee_p1}</td>
				<td><input type="text" name="valeurs_prelevements_{$annee_p1}" id="valeurs_prelevements_{$annee_p1}" value="{$ratiosControlsArray.$annee_p1.valeurs_annuelles.prelevements}" /></td>
			</tr>
			<tr>
				<td>{$annee_c0}</td>
				<td><input type="text" name="valeurs_prelevements_{$annee_c0}" id="valeurs_prelevements_{$annee_c0}" value="{$ratiosControlsArray.$annee_c0.valeurs_annuelles.prelevements}" /></td>
			</tr>
			<tr>
				<td>{$annee_s1}</td>
				<td><input type="text" name="valeurs_prelevements_{$annee_s1}" id="valeurs_prelevements_{$annee_s1}" value="{$ratiosControlsArray.$annee_s1.valeurs_annuelles.prelevements}" /></td>
			</tr>
			<tr>
				<td>{$annee_s2}</td>
				<td><input type="text" name="valeurs_prelevements_{$annee_s2}" id="valeurs_prelevements_{$annee_s2}" value="{$ratiosControlsArray.$annee_s2.valeurs_annuelles.prelevements}" /></td>
			</tr>
		</table>

		<input type="button" class="submit" value="Mettre &agrave; jour" onclick="javascript: submitAdminControls('setValeursAnnuelles');" />

	</form>

	{/if}
{/if}

{include file="footer.tpl"}
