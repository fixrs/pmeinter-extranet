<h2>R&eacute;partition des honoraires (H)</h2>

<h3>Immobilier r&eacute;sidentiel&nbsp;:</h3>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n" width="24px">400.&nbsp;</td>
		<td class="l" width="300px">
			<label for="immobilierExistant">Immobilier existant&nbsp;:</label>
		</td>
		<td class="m" width="100px">
			<input type="text" value="{$immobilierExistant}" name="immobilierExistant" onchange="javascript: adjustNumericField('immobilierExistant', 1, 2, 0, 1); afficherSousTotal('residentiel'); afficherTotal('residentiel');" class="total" id="immobilierExistant" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="n">401.&nbsp;</td>
		<td class="l">
			<label for="immobilierNeuf">Immobilier neuf&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$immobilierNeuf}" name="immobilierNeuf" onchange="javascript: adjustNumericField('immobilierNeuf', 1, 2, 0, 1); afficherSousTotal('residentiel'); afficherTotal('residentiel');" class="total" id="immobilierNeuf" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">429.&nbsp;</td>
		<td class="l">
			<label for="immobilierResidentielCondoExistant">Immobilier r&eacute;sidentiel – copropri&eacute;t&eacute; existante&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$immobilierResidentielCondoExistant}" name="immobilierResidentielCondoExistant" onchange="javascript: adjustNumericField('immobilierResidentielCondoExistant', 1, 2, 0, 1); afficherSousTotal('residentiel'); afficherTotal('residentiel');" class="total" id="immobilierResidentielCondoExistant" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">430.&nbsp;</td>
		<td class="l">
			<label for="immobilierResidentielCondoNeuf">Immobilier r&eacute;sidentiel – copropri&eacute;t&eacute; neuve&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$immobilierResidentielCondoNeuf}" name="immobilierResidentielCondoNeuf" onchange="javascript: adjustNumericField('immobilierResidentielCondoNeuf', 1, 2, 0, 1); afficherSousTotal('residentiel'); afficherTotal('residentiel');" class="total" id="immobilierResidentielCondoNeuf" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">431.&nbsp;</td>
		<td class="l">
			<label for="constitutionServicesCondo">Constitution et services – copropri&eacute;t&eacute;&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$constitutionServicesCondo}" name="constitutionServicesCondo" onchange="javascript: adjustNumericField('constitutionServicesCondo', 1, 2, 0, 1); afficherSousTotal('residentiel'); afficherTotal('residentiel');" class="total" id="constitutionServicesCondo" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">402.&nbsp;</td>
		<td class="l">
			<label for="immobilierResidentielSousTotal" class="total">Sous-total&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$immobilierResidentielSousTotal}" name="immobilierResidentielSousTotal" onchange="" id="immobilierResidentielSousTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="diff">
		<td colspan="2" class="l">
			<label for="immobilierResidentielDiff">Si ce montant ne correspond pas au total des honoraires,<br />veuillez inscrire la diff&eacute;rence&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$immobilierResidentielDiff}" name="immobilierResidentielDiff" onchange="javascript: adjustNumericField('immobilierResidentielDiff', 1, 2, 0, 1); afficherTotal('residentiel');" class="total" id="immobilierResidentielDiff" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="total">
		<td class="n">403.&nbsp;</td>
		<td class="l">
			<label for="immobilierResidentielTotal" class="total">Total</label>
		</td>
		<td class="m">
			<input type="text" value="{$immobilierResidentielTotal}" name="immobilierResidentielTotal" onchange="" id="immobilierResidentielTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>
</table>

<br clear="all" />

<h3>Affaires (commercial &amp; corporatif)&nbsp;:</h3>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n" width="24px">404.&nbsp;</td>
		<td class="l" width="300px">
			<label for="commercial">Commercial&nbsp;:</label>
		</td>
		<td class="m" width="100px">
			<input type="text" value="{$commercial}" name="commercial" onchange="javascript: adjustNumericField('commercial', 1, 2, 0, 1); afficherSousTotal('commercial'); afficherTotal('commercial');" id="commercial" class="total" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="n">405.</td>
		<td class="l">
			<label for="immobilierComm">Immobilier commercial & industriel&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$immobilierComm}" name="immobilierComm" onchange="javascript: adjustNumericField('immobilierComm', 1, 2, 0, 1); afficherSousTotal('commercial'); afficherTotal('commercial');" id="immobilierComm" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">406.&nbsp;</td>
		<td class="l">
			<label for="corporatif">Corporatif&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$corporatif}" name="corporatif" onchange="javascript: adjustNumericField('corporatif', 1, 2, 0, 1); afficherSousTotal('commercial'); afficherTotal('commercial');" id="corporatif" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">407.&nbsp;</td>
		<td class="l">
			<label for="serviceCorporatif">Service Corporatif&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$serviceCorporatif}" name="serviceCorporatif" onchange="javascript: adjustNumericField('serviceCorporatif', 1, 2, 0, 1); afficherSousTotal('commercial'); afficherTotal('commercial');" id="serviceCorporatif" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">408.&nbsp;</td>
		<td class="l">
			<label for="commercialCorporatifSousTotal" class="total">Sous-total&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$commercialCorporatifSousTotal}" name="commercialCorporatifSousTotal" onchange="" id="commercialCorporatifSousTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="diff">
		<td colspan="2" class="l">
			<label for="commercialCorporatifDiff">Si ce montant ne correspond pas au total des honoraires,<br />veuillez inscrire la diff&eacute;rence&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$commercialCorporatifDiff}" name="commercialCorporatifDiff" onchange="javascript: adjustNumericField('commercialCorporatifDiff', 1, 2, 0, 1); afficherTotal('commercial');" id="commercialCorporatifDiff" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="total">
		<td class="n">409.&nbsp;</td>
		<td class="l">
			<label for="commercialCorporatifTotal" class="total">Total</label>
		</td>
		<td class="m">
			<input type="text" value="{$commercialCorporatifTotal}" name="commercialCorporatifTotal" onchange="" id="commercialCorporatifTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>
</table>

<br clear="all" />

<h3>Personne, succession &amp; famille&nbsp;:</h3>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n" width="24px">410.&nbsp;</td>
		<td class="l" width="300px">
			<label for="testamentsMandats">Testaments &amp; mandats&nbsp;:</label>
		</td>
		<td class="m" width="100px">
			<input type="text" value="{$testamentsMandats}" name="testamentsMandats" onchange="javascript: adjustNumericField('testamentsMandats', 1, 2, 0, 1); afficherSousTotal('personne'); afficherTotal('personne');" id="testamentsMandats" class="total" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="n">411.&nbsp;</td>
		<td class="l">
			<label for="donationsFiduciaires">Donations fiduciaires :</label>
		</td>
		<td class="m">
			<input type="text" value="{$donationsFiduciaires}" name="donationsFiduciaires" onchange="javascript: adjustNumericField('donationsFiduciaires', 1, 2, 0, 1); afficherSousTotal('personne'); afficherTotal('personne');" id="donationsFiduciaires" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">412.&nbsp;</td>
		<td class="l">
			<label for="successions">Successions</label>
		</td>
		<td class="m">
			<input type="text" value="{$successions}" name="successions" onchange="javascript: adjustNumericField('successions', 1, 2, 0, 1); afficherSousTotal('personne'); afficherTotal('personne');" id="successions" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">413.&nbsp;</td>
		<td class="l">
			<label for="mediationFamiliale">M&eacute;diation familiale&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$mediationFamiliale}" name="mediationFamiliale" onchange="javascript: adjustNumericField('mediationFamiliale', 1, 2, 0, 1); afficherSousTotal('personne'); afficherTotal('personne');" id="mediationFamiliale" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">414.&nbsp;</td>
		<td class="l">
			<label for="proceduresNonContent">Proc&eacute;dures non contentieuses&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$proceduresNonContent}" name="proceduresNonContent" onchange="javascript: adjustNumericField('proceduresNonContent', 1, 2, 0, 1); afficherSousTotal('personne'); afficherTotal('personne');" id="proceduresNonContent" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">415.&nbsp;</td>
		<td class="l">
			<label for="protectionPatrimoine">Protection du patrimoine&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$protectionPatrimoine}" name="protectionPatrimoine" onchange="javascript: adjustNumericField('protectionPatrimoine', 1, 2, 0, 1); afficherSousTotal('personne'); afficherTotal('personne');" id="protectionPatrimoine" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">416.&nbsp;</td>
		<td class="l">
			<label for="autresPersonneFamilial">Autres - personne & familial&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$autresPersonneFamilial}" name="autresPersonneFamilial" onchange="javascript: adjustNumericField('autresPersonneFamilial', 1, 2, 0, 1); afficherSousTotal('personne'); afficherTotal('personne');" id="autresPersonneFamilial" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">417.&nbsp;</td>
		<td class="l">
			<label for="personneSuccessionFamilialSousTotal" class="total">Sous-total&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$personneSuccessionFamilialSousTotal}" name="personneSuccessionFamilialSousTotal" onchange="" id="personneSuccessionFamilialSousTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="diff">
		<td colspan="2" class="l">
			<label for="{$personneSuccessionFamilialDiff}">Si ce montant ne correspond pas au total des honoraires,<br />veuillez inscrire la diff&eacute;rence&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$personneSuccessionFamilialDiff}" name="personneSuccessionFamilialDiff" onchange="javascript: adjustNumericField('personneSuccessionFamilialDiff', 1, 2, 0, 1); afficherTotal('personne');" id="personneSuccessionFamilialDiff" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="total">
		<td class="n">418.&nbsp;</td>
		<td class="l">
			<label for="personneSuccessionFamilialTotal" class="total">Total</label>
		</td>
		<td class="m">
			<input type="text" value="{$personneSuccessionFamilialTotal}" name="personneSuccessionFamilialTotal" onchange="" id="personneSuccessionFamilialTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>
</table>

<br clear="all" />

<h3>Agricole, municipal &amp; administratif&nbsp;:</h3>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n" width="24px">419.&nbsp;</td>
		<td class="l" width="300px">
			<label for="agricole">Agricole&nbsp;:</label>
		</td>
		<td class="m" width="100px">
			<input type="text" value="{$agricole}" name="agricole" onchange="javascript: adjustNumericField('agricole', 1, 2, 0, 1); afficherSousTotal('agricole'); afficherTotal('agricole');" id="agricole" class="total" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="n">420.&nbsp;</td>
		<td class="l">
			<label for="municipalAdministratif">Municipal & administratif :</label>
		</td>
		<td class="m">
			<input type="text" value="{$municipalAdministratif}" name="municipalAdministratif" onchange="javascript: adjustNumericField('municipalAdministratif', 1, 2, 0, 1); afficherSousTotal('agricole'); afficherTotal('agricole');" id="municipalAdministratif" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr>
		<td class="n">421.&nbsp;</td>
		<td class="l">
			<label for="agricoleMunicipalAdministratifSousTotal" class="total">Sous-total&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$agricoleMunicipalAdministratifSousTotal}" name="agricoleMunicipalAdministratifSousTotal" onchange="" id="agricoleMunicipalAdministratifSousTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="diff">
		<td colspan="2" class="l">
			<label for="agricoleMunicipalAdministratifDiff">Si ce montant ne correspond pas au total des honoraires,<br />veuillez inscrire la diff&eacute;rence&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$agricoleMunicipalAdministratifDiff}" name="agricoleMunicipalAdministratifDiff" onchange="javascript: adjustNumericField('agricoleMunicipalAdministratifDiff', 1, 2, 0, 1); afficherTotal('agricole');" id="agricoleMunicipalAdministratifDiff" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="total">
		<td class="n">422.&nbsp;</td>
		<td class="l">
			<label for="agricoleMunicipalAdministratifTotal" class="total">Total</label>
		</td>
		<td class="m">
			<input type="text" value="{$agricoleMunicipalAdministratifTotal}" name="agricoleMunicipalAdministratifTotal" onchange="" id="agricoleMunicipalAdministratifTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>
</table>

<br clear="all" />

<h3>Divers</h3>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n" width="24px">423.&nbsp;</td>
		<td class="l" width="300px">
			<label for="divers">Divers&nbsp;:</label>
		</td>
		<td class="m" width="100px">
			<input type="text" value="{$divers}" name="divers" onchange="javascript: adjustNumericField('divers', 1, 2, 0, 1); afficherSousTotal('divers'); afficherTotal('divers');" id="divers" class="total" />
		</td>
		<td class="r" width="30px">$</td>
	</tr>

	<tr>
		<td class="n">424.&nbsp;</td>
		<td class="l">
			<label for="diversSousTotal" class="total">Sous-total&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$diversSousTotal}" name="diversSousTotal" onchange="" id="diversSousTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="diff">
		<td colspan="2" class="l">
			<label for="diversDiff">Si ce montant ne correspond pas au total des honoraires,<br />veuillez inscrire la diff&eacute;rence&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$diversDiff}" name="diversDiff" onchange="javascript: adjustNumericField('diversDiff', 1, 2, 0, 1); afficherTotal('divers');" id="diversDiff" class="total" />
		</td>
		<td class="r">$</td>
	</tr>

	<tr class="total">
		<td class="n">425.&nbsp;</td>
		<td class="l">
			<label for="diversTotal" class="total">Total</label>
		</td>
		<td class="m">
			<input type="text" value="{$diversTotal}" name="diversTotal" onchange="" id="diversTotal" class="total readonly" readonly="readonly" />
		</td>
		<td class="r">$</td>
	</tr>

</table>

<br clear="all" />
<br clear="all" />

<h2>Autres renseignements</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n" width="24px">426.&nbsp;</td>
		<td class="l">
			<label for="accepteMandats">Acceptez-vous des mandats en provenance de centres de traitement de dossiers immobiliers (ex. : FCT)&nbsp;?</label>
		</td>
		<td class="m">
			<select name="accepteMandats" id="accepteMandats" onchange="">
				<option value="">--</option>
				<option value="1" {if $accepteMandats==='1'}selected="selected"{/if}>Oui</option>
				<option value="0" {if $accepteMandats==='0'}selected="selected"{/if}>Non</option>
			</select><br clear="all" />
		</td>
		<td class="r" width="30px">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">427.&nbsp;</td>
		<td class="l">
			<label for="tauxRemboursementKilo">Taux de remboursement - kilom&eacute;trage&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" value="{$tauxRemboursementKilo}" name="tauxRemboursementKilo" onchange="javascript: adjustNumericField('tauxRemboursementKilo', 1, 2, 1, 1);" id="tauxRemboursementKilo" class="argent" />
		</td>
		<td class="r">$/km</td>
	</tr>

	<tr>
		<td class="n">428.&nbsp;</td>
		<td class="l">
			<label for="numerisationPiecesIdentite">Num&eacute;risation des pi&egrave;ces d'identit&eacute; et autres documents&nbsp;:</label>
		</td>
		<td class="m">
			<select name="numerisationPiecesIdentite" id="numerisationPiecesIdentite" onchange="">
				<option value="">--</option>
				<option value="1" {if $numerisationPiecesIdentite==='1'}selected="selected"{/if}>Oui</option>
				<option value="0" {if $numerisationPiecesIdentite==='0'}selected="selected"{/if}>Non</option>
			</select>
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">432.&nbsp;</td>
		<td class="l" colspan="2">
			<label for="particularitesAnnee">Particularit&eacute;s de l’ann&eacute;e (le cas &eacute;ch&eacute;ant) qui influencent particuli&egrave;rement les r&eacute;sultats financiers :&nbsp;:</label>
		<!--/td>
		<td class="m"-->
			<textarea name="particularitesAnnee" id="particularitesAnnee" >{$particularitesAnnee}</textarea>
		</td>
		<td class="r">&nbsp;</td>
	</tr>
</table>
