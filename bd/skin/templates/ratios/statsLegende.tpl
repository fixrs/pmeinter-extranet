<div class="legende">
	<span class="legende">Liste des &Eacute;tudes&nbsp;:</span><br />
	<table class="legende">
		<tr>
			<td width="33%">
				<ul class="legende">
				{foreach from=$etudes item=uneEtude name=etudeListe}
					{if $smarty.foreach.etudeListe.index <= 4}
						<li><span class="num">{$smarty.foreach.etudeListe.index+1}</span>&nbsp;:&nbsp;{$uneEtude}</li>
					{/if}
				{/foreach}
				</ul>
			</td>
			<td width="33%">
				<ul class="legende">
				{foreach from=$etudes item=uneEtude name=etudeListe}
					{if $smarty.foreach.etudeListe.index > 4 && $smarty.foreach.etudeListe.index <= 9}
						<li><span class="num">{$smarty.foreach.etudeListe.index+1}</span>&nbsp;:&nbsp;{$uneEtude}</li>
					{/if}
				{/foreach}
				</ul>
			</td>
			<td width="33%">
				<ul class="legende">
				{foreach from=$etudes item=uneEtude name=etudeListe}
					{if $smarty.foreach.etudeListe.index > 9}
						<li><span class="num">{$smarty.foreach.etudeListe.index+1}</span>&nbsp;:&nbsp;{$uneEtude}</li>
					{/if}
				{/foreach}
				</ul>
			</td>
		</tr>
	</table>
</div>
