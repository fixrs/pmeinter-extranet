<!--<h2>{$soustitre}</h2>-->

<table class="stats stats16 {$nomAction}" cellpadding="0" cellspacing="0">
	<thead>
		<tr>
			<td class="description l"><span class="soustitre">{$soustitre}</span></td>
			{foreach from=$ratioz item=ratio name=ratiosTop}
				<td class="nom">{$smarty.foreach.ratiosTop.index+1}</td>
			{/foreach}
			<td class="moyenne">Moyenne</td>
			{if $ratioz|@count lt 12} <td class="tampon">&nbsp;</td> {/if}
		</tr>
	</thead>
	{*<tfoot>
		<tr>
			<td class="description l"><span class="soustitre">{$soustitre}</span></td>
			{foreach from=$ratioz item=ratio name=ratiosTop}
				<td class="nom">{$smarty.foreach.ratiosTop.index+1}</td>
			{/foreach}
			<td class="moyenne">Moyenne</td>
			{if $ratioz|@count lt 12} <td class="tampon">&nbsp;</td> {/if}
		</tr>
	</tfoot>*}
	<tbody>
		{foreach from=$rowz item=row}
			<tr>
				{if $row->description == ''}
					<td class="description {$row->type} {$row->type2} {$row->type3} l">&nbsp;</td>
				{else}
					<td class="description {$row->type} {$row->type2} {$row->type3} l">{$row->description}</td>
				{/if}
				{assign var=mesCells value=$row->data}
				{foreach from=$mesCells item=uneCell name=lesCells}
					{if $smarty.foreach.lesCells.index == 0}
						<td class="valeur {$row->type} {$row->type2} {$row->type3} l2">
					{else}
						<td class="valeur {$row->type} {$row->type2} {$row->type3}">
					{/if}

					{if $row->type == 'sectionName'}
						&nbsp;
					{else}
						{if $row->unit === 'bool'}
							{if $uneCell}
								Oui
							{elseif $uneCell === "" || $uneCell === NULL}
								-
							{else}
								Non
							{/if}
						{elseif $row->unit === 'cad'}
							{$row->toFrenchFormatHtml($uneCell)}&nbsp;$
						{elseif $row->unit === 'percent'}
							{$row->toFrenchFormatHtml($uneCell)}&nbsp;%
						{else}
							{if $uneCell === null || $uneCell === false || $uneCell === ''}
								-
							{else}
								{$row->toFrenchFormatHtml($uneCell)}
							{/if}
						{/if}
					{/if}
					</td>
				{/foreach}
				{if $ratioz|@count lt 12}
					<td class="valeur {$row->type} {$row->type2} {$row->type3}">
				{else}
					<td class="valeur {$row->type} {$row->type2} {$row->type3} r">
				{/if}
					{if $row->type == 'sectionName'}
						&nbsp;
					{else}
						{if $row->getAverage() === true}
							Oui
						{elseif $row->getAverage() === false}
							-
						{elseif $row->unit === 'cad'}
							{$row->toFrenchFormatHtml($row->getAverage())}&nbsp;$
						{elseif $row->unit === 'percent'}
							{$row->toFrenchFormatHtml($row->getAverage())}&nbsp;%
						{elseif $row->unit === 'bool'}
							{if $row->getAverage()}
								Oui
							{elseif $row->getAverage() === "" || $row->getAverage() === NULL}
								-
							{else}
								Non
							{/if}
						{else}
							{$row->toFrenchFormatHtml($row->getAverage())}
						{/if}
					{/if}
				</td>
				{if $ratioz|@count lt 12}<td class="{$row->type} {$row->type2} {$row->type3} tampon r">&nbsp;</td> {/if}
			</tr>
		{/foreach}
	</tbody>
</table>
