<h2>Informations g&eacute;n&eacute;rales</h2>

<table cellpadding="0" cellspacing="0" border="0" class="ratio_form">
	<tr>
		<td class="n">100.&nbsp;</td>
		<td class="l">
			<label for="nbNotairesAssocies">Nom de la personne responsable de la saisie des donn&eacute;es pour l'&eacute;tude&nbsp;:</label>
		</td>
		<td class="m" width="100px">
			<input type="text" name="nomUtilisateur" value="{$nomUtilisateur}" onchange="" id="nomUtilisateur" />
		</td>
		<td class="r" width="30px">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">100.1&nbsp;</td>
		<td class="l">
			<label for="emailUtilisateur">Courriel de la personne responsable :</label>
		</td>
		<td class="m" width="100px">
			<input type="text" name="emailUtilisateur" value="{$emailUtilisateur}" onchange="" id="emailUtilisateur" />
		</td>
		<td class="r" width="30px">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">100.2&nbsp;</td>
		<td class="l">
			<label for="phoneUtilisateur">T&eacute;l&eacute;phone de la personne responsable &nbsp;:</label>
		</td>
		<td class="m" width="100px">
			<input type="text" name="phoneUtilisateur" value="{$phoneUtilisateur}" onchange="" id="phoneUtilisateur" />
		</td>
		<td class="r" width="30px">&nbsp;</td>
	</tr>

	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>

	<tr>
		<td class="n">101.&nbsp;</td>
		<td class="l">
			<label for="nbNotairesAssocies">Nombre de notaires associ&eacute;(e)s et/ou actionnaires <img src="{$BASEURL}skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('{$BASEURL}ratios/definitions.php?dl=titres&amp;dt=notairesAssocies#notairesAssocies');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbNotairesAssocies" value="{$nbNotairesAssocies}" onchange="javascript: adjustNumericField('nbNotairesAssocies', 1, 2, 2, 0); toggleMust('nbNotairesAssocies');" id="nbNotairesAssocies" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">102.&nbsp;</td>
		<td class="l">
			<label for="nbNotairesSalaries">Nombre de notaires salari&eacute; qui pratiquent <img src="{$BASEURL}skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('{$BASEURL}ratios/definitions.php?dl=titres&amp;dt=notairesSalaries#notairesSalaries');" class="pop" />&nbsp;:</label>	
		</td>
		<td class="m">
			<input type="text" name="nbNotairesSalaries" value="{$nbNotairesSalaries}" onchange="javascript: adjustNumericField('nbNotairesSalaries', 1, 2, 2, 0); toggleMust('nbNotairesSalaries');" id="nbNotairesSalaries" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">103.&nbsp;</td>
		<td class="l">
			<label for="nbNotairesTotal">Nombre total de notaires dans l'organisation (excluant les notaires collaborateur(rice)s&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbNotairesTotal" value="{$nbNotairesTotal}" onchange="javascript: adjustNumericField('nbNotairesTotal', 1, 2, 2, 0); toggleMust('nbNotairesTotal');" id="nbNotairesTotal" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">104.&nbsp;</td>
		<td class="l">
			<label for="nbEmployes">Nombre de collaborateur(rice)s incluant les notaires collaborateur(trice)s <img src="{$BASEURL}skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('{$BASEURL}ratios/definitions.php?dl=titres&amp;dt=employesNonNotaires#employesNonNotaires');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbEmployes" value="{$nbEmployes}" onchange="javascript: adjustNumericField('nbEmployes', 1, 2, 2, 0); toggleMust('nbEmployes');" id="nbEmployes" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">105.&nbsp;</td>
		<td class="l">
			<label for="nbStagieres">Nombre de stagiaires en notariat <img src="{$BASEURL}skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('{$BASEURL}ratios/definitions.php?dl=titres&amp;dt=stagiairesEnNotariat#stagiairesEnNotariat');" class="pop" />&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbStagieres" value="{$nbStagieres}" onchange="javascript: adjustNumericField('nbStagieres', 1, 2, 2, 0);" id="nbStagieres" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>

	<tr>
		<td class="n">106.&nbsp;</td>
		<td class="l">
			<label for="dossierMandat">De fa&ccedil;on g&eacute;n&eacute;rale, ouvrez-vous un nouveau dossier par mandat&nbsp;?</label>
		</td>
		<td class="m">
			<select name="dossierMandat" id="dossierMandat" onchange="">
				<option value="">--</option>
				<option value="1" {if $dossierMandat==='1'}selected="selected"{/if}>Oui</option>
				<option value="0" {if $dossierMandat==='0'}selected="selected"{/if}>Non</option>
			</select>
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">107.&nbsp;</td>
		<td class="l">
			<label for="nbDossiers">Nombre de dossiers trait&eacute;s par l'&eacute;tude&nbsp;:</label> 
		</td>
		<td class="m">
			<input type="text" name="nbDossiers" value="{$nbDossiers}" onchange="javascript: adjustNumericField('nbDossiers', 0, 0, 5, 0); toggleMust('nbDossiers');" id="nbDossiers" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">108.&nbsp;</td>
		<td class="l">
			<label for="nbMinutesNotaires">Nombre de minutes sign&eacute;es par les notaires&nbsp;:</label>
		</td>
		<td class="m">
			<input type="text" name="nbMinutesNotaires" value="{$nbMinutesNotaires}" onchange="javascript: adjustNumericField('nbMinutesNotaires', 0, 0, 5, 0); toggleMust('nbMinutesNotaires');" id="nbMinutesNotaires" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr>
		<td class="n">109.&nbsp;</td>
		<td class="l">
			<label for="minutesExcluent">Les minutes excluent-elles les actes de radiation&nbsp;?</label>
		</td>
		<td class="m">
			<select name="minutesExcluent" id="minutesExcluent" onchange="">
				<option value="">--</option>
				<option value="1" {if $minutesExcluent==='1'}selected="selected"{/if}>Oui</option>
				<option value="0" {if $minutesExcluent==='0'}selected="selected"{/if}>Non</option>
			</select>
		</td>
		<td class="r">&nbsp;</td>
	</tr>

	<tr class="spacer"><td colspan="4">&nbsp;</td></tr>
	
	<tr>
		<td class="n">110.&nbsp;</td>
		<td class="l">
			<label for="nbSocietes">Combien de soci&eacute;t&eacute;s sont membres de votre service corporatif&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" name="nbSocietes" value="{$nbSocietes}" onchange="javascript: adjustNumericField('nbSocietes', 0, 0, 4, 0);" id="nbSocietes" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>
	<tr>
		<td class="n">111.&nbsp;</td>
		<td class="l">
			<label for="nbFeducies">Combien de fiducies sont membres de votre service fiducie&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" name="nbFeducies" value="{$nbFeducies}" onchange="javascript: adjustNumericField('nbFeducies', 0, 0, 4, 0);" id="nbFeducies" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>
	<tr>
		<td class="n">112.&nbsp;</td>
		<td class="l">
			<label for="nbDossiersAG">Quel est le nombre de dossiers en cours pour votre service Ange Gardien&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" name="nbDossiersAG" value="{$nbDossiersAG}" onchange="javascript: adjustNumericField('nbDossiersAG', 0, 0, 4, 0);" id="nbDossiersAG" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>
	<tr>
		<td class="n">113.&nbsp;</td>
		<td class="l">
			<label for="nbDossiersMC">Quel est le nombre de dossiers en cours pour votre service Ma&icirc;tre des clefs&nbsp;?</label>
		</td>
		<td class="m">
			<input type="text" name="nbDossiersMC" value="{$nbDossiersMC}" onchange="javascript: adjustNumericField('nbDossiersMC', 0, 0, 4, 0);" id="nbDossiersMC" class="numerique" />
		</td>
		<td class="r">&nbsp;</td>
	</tr>
</table>
