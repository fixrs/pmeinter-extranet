<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>{$title}</title>
		<link rel="stylesheet" href="{$SKIN_URL}/css/stats.css" type="text/css" media="screen"/>
		<link rel="stylesheet" href="{$SKIN_URL}/css/print-stats.css" media="print" type="text/css" />
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
		<script type="text/javascript" src="{$SKIN_URL}/js/jquery-1.3.2.min.js"></script>
	</head>

	<body>

		{literal}
		<script language="JavaScript">

		var minFontSize = 6;
		var maxFontSize = 24;
		var defFontSize = {/literal}{$DEFAULT_FONT_SIZE}{literal};
		var fontClasses = new Array({/literal}{foreach from=$FONT_CLASSES item=font_class name=fontClassList}'{$font_class}'{if !$smarty.foreach.fontClassList.last},{/if}{/foreach}{literal});


		var minColWidth = 60;
		var maxColWidth = 600;
		var defColWidth = {/literal}{$DEFAULT_COL_WIDTH}{literal};
		var colClasses = new Array({/literal}{foreach from=$COL_CLASSES item=col_class name=colClassList}'{$col_class}'{if !$smarty.foreach.colClassList.last},{/if}{/foreach}{literal});

		function defaultFontSize() {
			for (var i in fontClasses) {
				var text = $(fontClasses[i]);
				var unit = 'pt';
				var num = parseFloat(defFontSize, 10);
				text.css('font-size', num + unit);
			}
		}

		function increaseFontSize() {
			for (var i in fontClasses) {
				var text = $(fontClasses[i]);
				var size = text.css('font-size');
				var unit = size.slice(-2);
				var num = parseFloat(size, 10);
				var newSize = num + 1;
				if (newSize < maxFontSize) {
					text.css('font-size', newSize + unit);
				}
			}
		}

		function decreaseFontSize() {
			for (var i in fontClasses) {
				var text = $(fontClasses[i]);
				var size = text.css('font-size');
				var unit = size.slice(-2);
				var num = parseFloat(size, 10);
				var newSize = num - 1;
				if (newSize > minFontSize) {
					text.css('font-size', newSize + unit);
				}
			}
		}

		function defaultColWidth() {
			for (var i in colClasses) {
				var col = $(colClasses[i]);
				var unit = 'px';
				var num = parseFloat(defColWidth, 10);
				col.css('width', num + unit);
			}
		}

		function increaseColWidth() {
			for (var i in colClasses) {
				var col = $(colClasses[i]);
				var width = col.css('width');
				if (width != undefined) {
					var unit = width.slice(-2);
					var num = parseFloat(width, 10);
					var newWidth = num + 5;
					if (newWidth < maxColWidth) {
						col.css('width', newWidth + unit);
					}
				}
			}
		}

		function decreaseColWidth() {
			for (var i in colClasses) {
				var col = $(colClasses[i]);
				var width = col.css('width');
				if (width != undefined) {
					var unit = width.slice(-2);
					var num = parseFloat(width, 10);
					var newWidth = num - 5;
					if (newWidth > minColWidth) {
						col.css('width', newWidth + unit);
					}
				}
			}
		}

		function printNow() {
			/*var msg = 'Veuillez v\351rifier si votre version d\'Internet Explorer est \351gale ou sup\351rieure \340 7. Dans la n\351gative, veuillez la t\351l\351charger gratuitement \340 partir du site de Microsoft (voir lien fourni au bas du bouton d\'impression).\n\nAssurez-vous de r\351gler votre impression sur papier \253FORMAT L\311GAL\273 (8.5x14) avec \253ORIENTATION PAYSAGE\273.';*/
			var msg = 'Assurez-vous de r\351gler votre impression sur papier \253FORMAT L\311GAL\273 (8.5x14) avec \253ORIENTATION PAYSAGE\273.';
			alert(msg);
			window.print();
		}

		</script>
		{/literal}

		<div id="printbar">
			<!--[if lt IE 7]><div style="display: none;"><![endif]-->
			<span style="float: left;">
				&nbsp;&nbsp;&nbsp;&nbsp;
				Ajuster la taille du texte&nbsp;:
				<input name="sizedown" id="sizedown" type="button" class="minus" onclick="javascript: decreaseFontSize();" value="-" />&nbsp;
				<input name="sizeup" id="sizeup" type="button" class="plus" onclick="javascript: increaseFontSize();" value="+" />&nbsp;
				<input name="sizedefault" id="sizedefault" type="button" class="reset" onclick="javascript: defaultFontSize();" value="R&eacute;initialiser" />&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				Ajuster la largeur des colonnes&nbsp;:
				<input name="widthdown" id="widthdown" type="button" class="minus" onclick="javascript: decreaseColWidth();" value="-" />&nbsp;
				<input name="widthup" id="widthup" type="button" class="plus" onclick="javascript: increaseColWidth();" value="+" />&nbsp;
				<input name="widthdefault" id="widthdefault" type="button" class="reset" onclick="javascript: defaultColWidth();" value="R&eacute;initialiser" />&nbsp;
			</span>
			<span style="float: right;">
				<input name="printnow" id="printnow" type="button" class="printnow" onclick="javascript: printNow();" value="Imprimer" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</span>
			<!--[if lt IE 7]>
			</div>
			<div style="margin-top: -8px; padding: 0px; width:100%; text-align: center; color: red; font-weight: bolder;">
				Nous avons d&eacute;tect&eacute; que votre version d'Internet Explorer est inf&eacute;rieure &agrave; 7.<br />
				Afin de pouvoir imprimer les tableaux correctement, vous devez utiliser au mininum Internet Explorer 7 ou Mozilla Firefox 3.<br />
				<a href="http://www.microsoft.com/windows/internet-explorer/worldwide-sites.aspx" title="Internet Explorer 7" target="_blank">Vous pouvez t&eacute;l&eacute;charger gratuitement la derni&egrave;re version d'Internet Explorer ici.</a>
			</div>
			<![endif]-->
		</div>

		<br  clear="all" />

		<div id="legalpaper">

			{foreach from=$actions item=action name=actionList key=nomAction}

				{if $type != 'stats16' && ($nomAction == 'statsalariales_a' || $nomAction == 'statsalariales_b')}
					<h1>{$titre_ss}</h1>
				{else}
					<h1>{$titre}</h1>
				{/if}

				{assign var=ratioz value=$ratios.$nomAction}
				{assign var=rowz value=$rows.$nomAction}
				{assign var=soustitre value=$soustitres.$nomAction}

				{if $type == 'stats16' && ($nomAction == 'repartitionhonoraires_a' || $nomAction == 'repartitionhonoraires_b' || $nomAction == 'repartitionhonoraires_c')}
					{include file=ratios/stats16.rh.tpl}
				{else}
					{include file=ratios/$type.tpl}
				{/if}

				{include file="ratios/statsLegende.tpl"}

				{if !$smarty.foreach.actionList.last}
					<hr class="pagebreak" />
				{/if}
			{/foreach}
		</div>
	</body>
</html>

