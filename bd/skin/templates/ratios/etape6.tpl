{if $open || $admin}
	{if !$complete}
		{if $errorArray|@count==0}
			{if $warningArray|@count>0}
				Le formulaire peut tout de m&ecirc;me &ecirc;tre signal&eacute; comme termin&eacute; malgr&eacute; les avertissements.<br />
				Nous vous demandons simplement de v&eacute;rifier et d'&ecirc;tre certains des chiffres entr&eacute;s.<br />
			{else}
				Le formulaire est pr&ecirc;t &agrave; &ecirc;tre signal&eacute; comme termin&eacute;.<br />
			{/if}
		{else}
			&Agrave; cause d'&eacute;l&eacute;ments de validation non-conformes, vous ne pouvez signaler ce formulaire comme termin&eacute;. Veuillez revoir les &eacute;tapes 1 &agrave; 5 afin de corriger ces erreurs, puis revenez &agrave; l'&eacute;tape 6 afin de signaler le formulaire comme termin&eacute;.
		{/if}

		<br clear="all" />

		{if $errorArray|@count==0}
			<input type="button" class="submit" value="Signaler le formulaire comme termin&eacute;" onclick="javascript: submitComplete();"><br />
		{/if}

	{else}

		<br clear="all" />

		<input type="button" class="submit" value="Signaler le formulaire comme non termin&eacute;" onclick="javascript: submitNotComplete();"><br />

	{/if}

{/if}


{*
<input id="button_a" class="submit" type="button" value="Rapport des ratios de gestion de l’&eacute;tude" onclick="javascript: window.open('{$link_stats3}', 'statistiques');" /><br />
<input id="button_b" class="submit" type="button" value="Rapport cumulatif des ratios de gestion des &eacute;tudes du r&eacute;seau" onclick="javascript: window.open('{$link_stats16}', 'statistiques');" /><br />
*}
