{include file="header.tpl"}

	{$errors}

	{if $nologin != 1}

		{literal}
		<script language="JavaScript">

		function submitForm(thisAction) {
			var errorMsg = '';
			var action = document.getElementById('action');
			var etudeKey = document.getElementById('etudeKey');
			var factureKey = document.getElementById('factureKey');

			switch (thisAction) {
				case 'ajout':
					var etudeKey1Value = getSelectValue(document.getElementById('etudeField1'));
					if (etudeKey1Value == '' || etudeKey1Value == '0' || etudeKey1Value == 'undefined') {
						errorMsg += 'Vous devez choisir une \351tude.';
					}
					action.value = 'ajout';
					etudeKey.value = etudeKey1Value;
					break;

				case 'rapport':
					var etudeKey2Value = getSelectValue(document.getElementById('etudeField2'));
					var typeRapportValue = getSelectValue(document.getElementById('type_rapport'));
					var tableauSommaireValue = document.getElementById('tableau_sommaire').checked;
					var tableauDetailsValue = document.getElementById('tableau_details').checked;
					var dateDebutValue = document.getElementById('date_debut').value;
					var dateFinValue = document.getElementById('date_fin').value;
					
					if (typeRapportValue == '' || typeRapportValue == '0' || typeRapportValue == 'undefined') {
						errorMsg += 'Vous devez choisir un type de rapport.\n\n';
					}
					if (
						(typeRapportValue == 'annuel_single' || typeRapportValue == 'periode_single')
						&& (etudeKey2Value == '' || etudeKey2Value == '0' || etudeKey2Value == 'undefined')
					) {
						errorMsg += 'Vous devez choisir une \351tude.\n\n';
					}
					if ((typeRapportValue == 'annuel_single' || typeRapportValue == 'annuel_all') && !isDate(dateFinValue)) {
						errorMsg += 'Vous devez entrer une date de fin valide.\n';
					}
					if ((typeRapportValue == 'periode_single' || typeRapportValue == 'periode_all') && (!isDate(dateDebutValue) || !isDate(dateFinValue))) {
						errorMsg += 'Vous devez entrer des dates valides.\n';
					}
					if (tableauSommaireValue == false && tableauDetailsValue == false) {
						errorMsg += 'Vous devez choisir un minimum de donn\351es \340 afficher.\n\n';
					}
					action.value = 'rapport';
					etudeKey.value = etudeKey2Value;
					break;

				case 'modify':
					var etudeKey3Value = getSelectValue(document.getElementById('etudeField3'));
					var factureKeyValue = getSelectValue(document.getElementById('factureField_' + etudeKey3Value));
					if (factureKeyValue == '' || factureKeyValue == '0' || factureKeyValue == 'undefined' || factureKeyValue == null) {
						errorMsg += 'Vous devez choisir une facture.';
					}
					action.value = 'modify';
					etudeKey.value = etudeKey3Value;
					factureKey.value = factureKeyValue;
					break;

				case 'delete':
					var etudeKey3Value = getSelectValue(document.getElementById('etudeField3'));
					var factureKeyValue = getSelectValue(document.getElementById('factureField_' + etudeKey3Value));
					if (factureKeyValue == '' || factureKeyValue == '0' || factureKeyValue == 'undefined' || factureKeyValue == null) {
						errorMsg += 'Vous devez choisir une facture.\n';
					} else {
						if (!confirm('\312tes-vous certain de vouloir supprimer cette facture ?')) {
							return false;
						}
					}
					action.value = 'delete';
					etudeKey.value = etudeKey3Value;
					factureKey.value = factureKeyValue;
					break;

				default:
					errorMsg += 'Veuillez choisir une action...';
			}

			if (errorMsg == '') {
				document.getElementById('reseauiqForm').submit();
			} else {
				alert(errorMsg);
				return false;
			}

		}

		function typeRapportToggle() {
			var typeRapportValue = getSelectValue(document.getElementById('type_rapport'));
			var etudeKey2 = document.getElementById('etudeField2');
			var dateDebut = document.getElementById('date_debut');
			var dateFin = document.getElementById('date_fin');
			var tableauSommaire = document.getElementById('tableau_sommaire');
			var tableauDetails = document.getElementById('tableau_details');

			switch (typeRapportValue) {
				case 'annuel_single':
					etudeKey2.disabled = false;
					etudeKey2.style.background = '#FFFFFF';
					dateDebut.disabled = true;
					dateDebut.style.background = '#D3D3D3';
					dateFin.disabled = false;
					dateFin.style.background = '#FFFFFF';
					tableauSommaire.disabled = false;
					tableauDetails.disabled = false;
					break;

				case 'annuel_all':
					etudeKey2.disabled = true;
					etudeKey2.style.background = '#D3D3D3';
					dateDebut.disabled = true;
					dateDebut.style.background = '#D3D3D3';
					dateFin.disabled = false;
					dateFin.style.background = '#FFFFFF';
					tableauSommaire.disabled = false;
					tableauSommaire.checked = true;
					tableauDetails.disabled = true;
					tableauDetails.checked = false;
					break;

				case 'periode_single':
					etudeKey2.disabled = false;
					etudeKey2.style.background = '#FFFFFF';
					dateDebut.disabled = false;
					dateDebut.style.background = '#FFFFFF';
					dateFin.disabled = false;
					dateFin.style.background = '#FFFFFF';
					tableauSommaire.disabled = false;
					tableauDetails.disabled = false;
					break;

				case 'periode_all':
					etudeKey2.disabled = true;
					etudeKey2.style.background = '#D3D3D3';
					dateDebut.disabled = false;
					dateDebut.style.background = '#FFFFFF';
					dateFin.disabled = false;
					dateFin.style.background = '#FFFFFF';
					tableauSommaire.disabled = false;
					tableauSommaire.checked = true;
					tableauDetails.disabled = true;
					tableauDetails.checked = false;
					break;

				default:
					etudeKey2.disabled = true;
					etudeKey2.style.background = '#D3D3D3';
					dateDebut.disabled = true;
					dateDebut.style.background = '#D3D3D3';
					dateFin.disabled = true;
					dateFin.style.background = '#D3D3D3';
					tableauSommaire.disabled = true;
					tableauDetails.disabled = true;
			}
		}

		function isDate(strValue) {
			var objRegExp = /^\d{4}-\d{2}-\d{2}$/
			if (objRegExp.test(strValue)) {
				return true;
			}
			return false;
		}

		function loadFactures() {
			var factureFields = new Array();
			{/literal}{foreach name=factureForeach from=$facturesArray key=etudeKey item=null}{literal}
				factureFields[{/literal}{$smarty.foreach.factureForeach.index}{literal}] = 'factureField_{/literal}{$etudeKey}{literal}';
			{/literal}{/foreach}{literal}

			for (i = 0; i < factureFields.length; i++) {
				document.getElementById(factureFields[i]).style.display = 'none';
				document.getElementById(factureFields[i]).disabled = true;
			}

			var etudeKey = getSelectValue(document.getElementById('etudeField3'));
			var factureField = document.getElementById('factureField_' + etudeKey);
			var factureFieldDefault = document.getElementById('factureField_default');

			if (
				etudeKey != '' && etudeKey != '0' && etudeKey != 'undefined' &&
				factureField != '' && factureField != 'undefined' && factureField != null
			) {
				factureFieldDefault.style.display = 'none';
				factureField.style.display = 'inline';
				factureField.disabled = false;
			} else {
				factureFieldDefault.style.display = 'inline';
			}
		}

		function adjustDateField(fieldId) {
			var fieldElement = document.getElementById(fieldId);
			var fieldValue = fieldElement.value;
			fieldElement.value = fieldValue.replace(/\//g, '-');
		}

		</script>
		{/literal}

		<form action="reseauiq.php" method="post" id="reseauiqForm">
			<input type="hidden" name="action" id="action" value="" />
			<input type="hidden" name="etudeKey" id="etudeKey" value="" />
			<input type="hidden" name="factureKey" id="factureKey" value="" />

			<h1>Ajout d'une facture (&eacute;tude)</h1>

			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKey1" id="etudeField1">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				{foreach from=$etudesArray item=etude key=clef}
					<option value="{$clef}">{$etude}</option>
				{/foreach}
			</select><br />

			<input type="button" class="submit" value="Ajouter une facture" onclick="javascript: submitForm('ajout');" /><br />

			<br />
			<br />
			<br />

			{if $admin == 1}
			<h1>Modification d'une facture (&eacute;tude)</h1>

			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKey3" id="etudeField3" onchange="javascript: loadFactures();">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				{foreach from=$etudesArray item=etude key=clef}
					<option value="{$clef}">{$etude}</option>
				{/foreach}
			</select><br />
	
			<br />

			<label>Veuillez choisir la facture&nbsp;:</label>

			<select name="factureField_default" id="factureField_default" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
			</select>

			{foreach from=$facturesArray key=etudeKey item=null}
			<select name="factureKey_{$etudeKey}" id="factureField_{$etudeKey}" style="display:none;" disabled="disabled">
				<option selected="selected" value="0">- Choisir une facture -</option>
				{foreach from=$facturesArray.$etudeKey key=factureKey item=factureDate }
					<option value="{$factureKey}">{$factureKey} ({$factureDate})</option>
				{/foreach}
			</select>
			{/foreach}

			<br />

			<input type="button" class="submit" value="Modifier la facture" onclick="javascript: submitForm('modify');" />&nbsp;&nbsp;&nbsp;&nbsp;
			<input type="button" class="submit" value="Supprimer la facture" onclick="javascript: submitForm('delete');" /><br />

			{/if}

			<br />
			<br />
			<br />

			<h1>Consultation des rapports de ristourne</h1>

			<label>Type de cumulatif&nbsp;:</label>
			<select name="type_rapport" id="type_rapport" onchange="javascript: typeRapportToggle();">
				<option selected="selected" value="0">- Choisir un type de cumulatif -</option>
				{if $admin == 1}
					<optgroup label="Annuel">
						<option value="annuel_single">Pour une &eacute;tude sp&eacute;cifique</option>
						<option value="annuel_all">Pour l'ensemble des &eacute;tudes</option>
					</optgroup>
					<optgroup label="P&eacute;riode">
						<option value="periode_single">Pour une &eacute;tude sp&eacute;cifique</option>
						<option value="periode_all">Pour l'ensemble des &eacute;tudes</option>
					</optgroup>
				{else}
					<option value="annuel_single">Annuel pour une &eacute;tude sp&eacute;cifique</option>
					<option value="periode_single">P&eacute;riode pour une &eacute;tude sp&eacute;cifique</option>
				{/if}
			</select><br />

			<br />

			<label>Veuillez choisir l'&eacute;tude&nbsp;:</label>
			<select name="etudeKey2" id="etudeField2" disabled="disabled" style="background:#D3D3D3;">
				<option selected="selected" value="0">- Choisir une &eacute;tude -</option>
				{foreach from=$etudesArray item=etude key=clef}
					<option value="{$clef}">{$etude}</option>
				{/foreach}
			</select><br />

			<br />

			<table cellpadding="0" cellspacing="0" border="0" class="noborder">
				<tr>
					<td style="font-size:12px; padding-left:0" width="100px">D&eacute;butant le&nbsp;:</td>
					<td style="font-size:12px; padding-left:0"><input type="text" name="date_debut" id="date_debut" onchange="javascript: adjustDateField('date_debut');" value="{$date_debut}" disabled="disabled" style="background:#D3D3D3;" /><span class="note">AAAA-MM-JJ</span></td>
				</tr>
				<tr>
					<td style="font-size:12px; padding-left:0">Finissant le&nbsp;:</td>
					<td style="font-size:12px; padding-left:0"><input type="text" name="date_fin" id="date_fin" onchange="javascript: adjustDateField('date_fin');" value="{$date_fin}" disabled="disabled" style="background:#D3D3D3;" /><span class="note">AAAA-MM-JJ</span></td>
				</tr>
			</table>

			<br />

			Donn&eacute;es &agrave; afficher&nbsp;:
			&nbsp;&nbsp;
			<input type="checkbox" name="tableau[]" id="tableau_sommaire" class="radio" value="sommaire" disabled="disabled" /> Sommaire
			&nbsp;&nbsp;
			<input type="checkbox" name="tableau[]" id="tableau_details" class="radio" value="details" disabled="disabled" /> D&eacute;tails

			<br />

			<input type="button" class="submit" value="Afficher le rapport" onclick="javascript: submitForm('rapport');" /><br />

			<br />
			<br />
			<br />

			{if $admin == 1}
			<h1>Gestion des factures (Administration)</h1>

			<div class="menuIn">
			<ul>
				<li><a href="{$BASEURL}facturation/facture.php?form=ajout">Ajouter une facture</a></li>
				<li><a href="{$BASEURL}facturation/facture.php?form=suppression">Supprimer une facture</a></li>
				<li><a href="{$BASEURL}facturation/paiement.php?form=ajout">Ajouter un paiement</a></li>
				<li><a href="{$BASEURL}facturation/paiement.php?form=suppression">Supprimer un paiement</a></li>
				<li><a href="{$BASEURL}facturation/rapport.php">Consultation de rapports</a></li>
			</ul>
			</div>
			{/if}

			{if $admin == 1}
			<h1>Gestion des paiements RIQ (Administration)</h1>

			<div class="menuIn">
			<ul>
				<li><a href="{$BASEURL}facturation/riq-paiement.php?form=ajout">Ajouter un paiement</a></li>
				<li><a href="{$BASEURL}facturation/riq-paiement.php?form=suppression">Supprimer un paiement</a></li>
				<li><a href="{$BASEURL}facturation/riq-rapport.php">Consultation de rapports</a></li>
			</ul>
			</div>
			{/if}


		</form>

	{/if}

{include file="footer.tpl"}
