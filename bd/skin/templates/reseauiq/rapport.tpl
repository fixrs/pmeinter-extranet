{include file="header.tpl"}

{if $nologin != 1}

	{if $noRight != 1}

		<h2>Rapport de facturation</h2>
		
		<br />
		
		{if $typeRapport == 'annuel_single'}
			<strong>Cumulatif</strong>&nbsp;: Annuel finissant le {$dateFin}<br />
			<br />
			<strong>&Eacute;tude</strong>&nbsp;: {$etudeNom}<br />

		{elseif $typeRapport == 'annuel_all'}
			<strong>Cumulatif</strong>&nbsp;: Annuel finissant le {$dateFin}<br />

		{elseif $typeRapport == 'periode_single'}
			<strong>Cumulatif</strong>&nbsp;: P&eacute;riode du {$dateDebut} au {$dateFin}<br />
			<br />
			<strong>&Eacute;tude</strong>&nbsp;: {$etudeNom}<br />

		{elseif $typeRapport == 'periode_all'}
			<strong>Cumulatif</strong>&nbsp;: P&eacute;riode du {$dateDebut} au {$dateFin}<br />
		{/if}
	
		<br />
		
		{if $typeRapport == 'annuel_single' || $typeRapport == 'periode_single'}

			<strong>Notaire responsable</strong>&nbsp;: {$notaireNom}<br />
			
			<br />
			<br />

			{if $tableauSommaireBool == 1}

				<strong>Sommaire de la facturation&nbsp;:</strong> <br />
				
				<br />
				
				<table cellpadding="0" cellspacing="0" border="0" class="rapportSommaireForOne">
					<tr>
						<td width="200px" class="l">
							Montant des honoraires&nbsp;:
						</td>
						<td class="r">
							{$factureSommaireForOneArray.montant_honoraires_total|toMoneyFormat}
						</td>
					</tr>
					<tr>
						<td class="l">
							Montant des d&eacute;bours&eacute;s&nbsp;:
						</td>
						<td class="r">
							{$factureSommaireForOneArray.montant_debourses_total|toMoneyFormat}
						</td>
					</tr>
					<tr>
						<td class="l">
							Montant &eacute;ligible &agrave; la ristourne&nbsp;:
						</td>
						<td class="r">
							{$factureSommaireForOneArray.montant_ristourne_eligible_total|toMoneyFormat}
						</td>
					</tr>
					<tr>
						<td class="l">
							Montant de la ristourne &agrave; verser&nbsp;:
						</td>
						<td class="r">
							{$factureSommaireForOneArray.montant_ristourne_total|toMoneyFormat}
						</td>
					</tr>
				</table><br />
				
				<br />

			{/if}
		
			{if $tableauDetailsBool == 1}

				<strong>D&eacute;tails des factures ({$nbFactures})&nbsp;:</strong><br />
				
				<br />

				{if $nbFactures == 0}

					Aucune facture enregistr&eacute;e pour cette p&eacute;riode.

				{else}
			
					{foreach name=factureForeach from=$factureDetailsForOneArray key=n item=i}
						<table cellpadding="0" cellspacing="0" border="0" class="rapportDetails">
							<tr>
								<td class="l">
									Date de la facture&nbsp;:
								</td>
								<td class="r">
									{$i.date_facture}
								</td>
							</tr>
							<tr>
								<td class="l">
									Num&eacute;ro de la facture&nbsp;:
								</td>
								<td class="r">
									{$i.factureKey}
								</td>
							</tr>
							<tr>
								<td class="l">
									Num&eacute;ro du client&nbsp;:
								</td>
								<td class="r">
									{$i.numero_client}
								</td>
							</tr>
							<tr>
								<td class="l">
									Montant des honoraires&nbsp;:
								</td>
								<td class="r">
									{$i.montant_honoraires|toMoneyFormat}
								</td>
							</tr>
							<tr>
								<td class="l">
									Montant des d&eacute;bours&eacute;s&nbsp;:
								</td>
								<td class="r">
									{$i.montant_debourses|toMoneyFormat}
								</td>
							</tr>
							<tr>
								<td class="l">
									Montant &eacute;ligible &agrave; la ristourne&nbsp;:
								</td>
								<td class="r">
									{$i.montant_ristourne_eligible|toMoneyFormat}
								</td>
							</tr>
							<tr>
								<td class="l">
									Montant de la ristourne &agrave; verser&nbsp;:
								</td>
								<td class="r">
									{$i.montant_ristourne|toMoneyFormat}
								</td>
							</tr>
							<tr>
								<td class="l">
									Facture enregistr&eacute;e par&nbsp;:
								</td>
								<td class="r">
									{$i.utilisateurKey}
								</td>
							</tr>						
						</table><br />
					{/foreach}
				{/if}
			{/if}
		
		{elseif $typeRapport == 'annuel_all' || $typeRapport == 'periode_all'}

			<br />

			{if $tableauSommaireBool == 1}

				<strong>Sommaire de la facturation&nbsp;:</strong> <br />
				
				<br />
				
					<table cellpadding="0" cellspacing="0" border="0" class="rapportSommaireForAll">
						<tr>
							<td class="titre" width="260px">Nom de l'&eacute;tude</td>
							<td class="titre" width="80px">Honoraires &eacute;ligibles</td>
							<td class="titre" width="80px">Honoraires non &eacute;ligibles</td>
							<td class="titre" width="80px">D&eacute;bours&eacute;s</td>
							<td class="titre" width="80px">Ristourne</td>
						</tr>

						{foreach name=factureForeach from=$factureSommaireForAllArray.data key=n item=i}
						{if $j++ is odd by 1}
							<tr class="even">
						{else}
							<tr class="odd">
						{/if}
							<td class="etude">{$i.nom}</td>
							<td>{$i.montant_ristourne_eligible|toMoneyFormat}</td>
							<td>{$i.montant_honoraires|toMoneyFormat}</td>
							<td>{$i.montant_debourses|toMoneyFormat}</td>
							<td>{$i.montant_ristourne|toMoneyFormat}</td>
						</tr>
						{/foreach}

						<tr>
							<th class="total">Total&nbsp;:</th>
							<td class="total">{$factureSommaireForAllArray.total.montant_ristourne_eligible|toMoneyFormat}</td>
							<td class="total">{$factureSommaireForAllArray.total.montant_honoraires|toMoneyFormat}</td>
							<td class="total">{$factureSommaireForAllArray.total.montant_debourses|toMoneyFormat}</td>
							<td class="total">{$factureSommaireForAllArray.total.montant_ristourne|toMoneyFormat}</td>
						</tr>
					</table>
			{/if}

		{/if}

	{else}
		Vous ne disposez pas des droits pour effectuer cette op&eacute;ration.
	{/if}

{else}
	Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.
{/if}

{include file="footer.tpl"}
