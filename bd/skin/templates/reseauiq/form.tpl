<form action="reseauiq.php" method="post" id="reseauiqForm">
	<input type="hidden" name="action" id="action" value="save" />
	<input type="hidden" name="etudeKey" id="etudeKey" value="{$etudeKey}" />

	<table cellpadding="0" cellspacing="0" border="0" class="reseauiq_form">

		<tr>
			<td class="l" width="280px">
				<label for="notaire_responsable">Nom du notaire responsable&nbsp;:</label>
			</td>
			<td class="m">
				{if $admin == 1 && $notaireNom == ''}
					<input type="text" name="notaire_responsable" value="{$notaire_responsable}" onchange="javascript: toggleMust('notaire_responsable');" id="notaire_responsable" style="width:172px;" />
				{elseif $admin == 1 && $notaireNom != '' && $notaire_responsable == ''}
					<input type="text" name="notaire_responsable" value="{$notaireNom}" onchange="javascript: toggleMust('notaire_responsable');" id="notaire_responsable" style="width:172px;" />
				{elseif $admin == 1 && $notaireNom != '' && $notaire_responsable != ''}
					<input type="text" name="notaire_responsable" value="{$notaire_responsable}" onchange="javascript: toggleMust('notaire_responsable');" id="notaire_responsable" style="width:172px;" />
				{elseif $admin != 1 && $notaireNom == ''}
					<input type="text" name="notaire_responsable" value="{$notaire_responsable}" onchange="javascript: toggleMust('notaire_responsable');" id="notaire_responsable" style="width:172px;" />
				{else}
					<input type="text" name="notaire_responsable" value="{$notaireNom}" id="notaire_responsable" style="width:172px;" readonly="readonly" />
				{/if}

			</td>
			<td class="r">&nbsp;</td>
		</tr>
	
		<tr>
			<td class="l">
				<label for="date_facture">Date de la facture&nbsp;:</label>
			</td>
			<td class="m">
				<input type="text" name="date_facture" value="{$date_facture}" onchange="javascript: adjustDateField('date_facture'); toggleMust('date_facture');" id="date_facture" class="argent" />
			</td>
			<td class="r"><span class="note">AAAA-MM-JJ</span></td>
		</tr>

		<tr>
			<td class="l">
				<label for="numero_client">Num&eacute;ro du client&nbsp;:</label>
			</td>
			<td class="m">
				<input type="text" name="numero_client" value="{$numero_client}" onchange="javascript: toggleMust('numero_client');" id="numero_client" class="argent" />
			</td>
			<td class="r"><span class="note">&nbsp;</span></td>
		</tr>

		<tr>
			<td class="l">
				<label for="factureKey">Num&eacute;ro de la facture&nbsp;:</label>	
			</td>
			<td class="m">
				<input type="text" name="factureKey" value="{$factureKey}" onchange="javascript: toggleMust('factureKey');" id="factureKey" class="argent" />
			</td>
			<td class="r"><span class="note">&nbsp;</span></td>
		</tr>

		<tr>
			<td class="l">
				<label for="montant_ristourne_eligible">Montant des honoraires&nbsp;:</label><span style="font-size:10px;">(montant apr&egrave;s l'application de la r&eacute;duction de 10%)&nbsp;<img src="{$BASEURL}skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('{$BASEURL}reseauiq/ristourne.html');" class="pop" /></span>
			</td>
			<td class="m">
				<input type="text" name="montant_ristourne_eligible" value="{$montant_ristourne_eligible}" onchange="javascript: adjustNumericField('montant_ristourne_eligible', 1, 2, 0, 0); afficherMontantRistourne(); toggleMust('montant_ristourne_eligible');" id="montant_ristourne_eligible" class="argent" />
			</td>
			<td class="r">$</td>
		</tr>

		<tr>
			<td class="l">
				<label for="montant_honoraires">Montant des honoraires&nbsp;:</label><span style="font-size:10px;">(non &eacute;ligible &agrave; la r&eacute;duction de 10%)&nbsp;<img src="{$BASEURL}skin/img/question.gif" border="0" alt="Plus d'information" align="absmiddle" onclick="popupDef('{$BASEURL}reseauiq/ristourne.html');" class="pop" /></span>
			</td>
			<td class="m">
				<input type="text" name="montant_honoraires" value="{$montant_honoraires}" onchange="javascript: adjustNumericField('montant_honoraires', 1, 2, 0, 0); toggleMust('montant_honoraires');" id="montant_honoraires" class="argent" />
			</td>
			<td class="r">$</td>
		</tr>

		<tr>
			<td class="l">
				<label for="montant_debourses">Montant des d&eacute;bours&eacute;s&nbsp;:</label>
			</td>
			<td class="m">
				<input type="text" name="montant_debourses" value="{$montant_debourses}" onchange="javascript: adjustNumericField('montant_debourses', 1, 2, 0, 0); toggleMust('montant_debourses');" id="montant_debourses" class="argent" />
			</td>
			<td class="r">$</td>
		</tr>
	
		<tr>
			<td class="l total">
				<label for="montant_ristourne" class="total">Montant de la ristourne &agrave; verser&nbsp;:</label>
			</td>
			<td class="m total">
				<input type="text" name="montant_ristourne" value="{$montant_ristourne}" id="montant_ristourne" class="argent total readonly" readonly="readonly "/>
			</td>
			<td class="r total">$</td>
		</tr>
	
	</table>

	<div id="navigation">
		<br clear="all" />
		<input type="button" value="Soumettre" class="navigation submit" onclick="submitForm();" />
	</div>
	
</form>
