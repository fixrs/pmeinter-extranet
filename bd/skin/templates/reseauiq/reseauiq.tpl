{include file="header.tpl"}

{if $nologin != 1}

	{if $noRight != 1}

		<h2>Ajout d'une facture</h2>
	
		<div id="reseauiq">
			<div id="infosEtude">{$etudeNom}</div>
	
			<br clear="all" />
	
			<div class="noticeDiv">
				{foreach name=notices from=$noticeArray item=notice}
					{if $smarty.foreach.notices.first}<br />{/if}
					<div class="noticeBox">{$notice}</div>
				{/foreach}
			</div>
	
			<div class="errorDiv">
				{foreach name=errors from=$errorArray item=erreur}
					{if $smarty.foreach.errors.first}<br />{/if}
					<div class="errorBox">{$erreur}</div>
				{/foreach}
			</div>
	
			<br clear="all" />

			{$link}
	
			{if $noForm != 1}
				{include file="reseauiq/form.tpl"}
			{/if}
	
		</div>

	{else}
		Vous ne disposez pas des droits pour effectuer cette op&eacute;ration.
	{/if}

{else}
	Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.
{/if}

{include file="footer.tpl"}
