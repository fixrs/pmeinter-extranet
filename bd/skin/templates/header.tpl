
<link rel="stylesheet" href="{$SKIN_URL}/css/styles.css" type="text/css" media="screen"/>
<link rel="stylesheet" href="{$SKIN_URL}/css/ratios-print.css" media="print" type="text/css" />
{foreach from=$CSS item=css_file}
	<link rel="stylesheet" href="{$SKIN_URL}/css/{$css_file}.css" type="text/css" media="screen"/>
{/foreach}
{foreach from=$CSS_PRINT item=css_file}
	<link rel="stylesheet" href="{$SKIN_URL}/css/{$css_file}.css" type="text/css" media="print"/>
{/foreach}

<link href="{$SKIN_URL}/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>

<script type="text/javascript" src="{$BASEURL}skin/js/form.js"></script>
{foreach from=$JS item=js_file}
	<script type="text/javascript" src="{$BASEURL}skin/js/{$js_file}.js"></script>
{/foreach}






