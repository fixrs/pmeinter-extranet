<?php

	require "../conf/conf.inc.php";

	session_start();

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;
	}


	if ($_GET["table"] != "" && $_GET["user"] != "" && $_GET["event"] != "") {

		if ($_GET["val"] == 1) {
			$DB->query(
				"UPDATE `inscriptions` ".
				"SET `actif` = '0' ".
				"WHERE `table` = '" . $_GET["table"] . "' ".
				"AND `event_key` = '" . $_GET["event"] . "' ".
				"AND `employes_key` = '" . $_GET["user"] . "' "
			);

			$DB->query(
				"INSERT INTO `inscriptions` (".
				"`table`, ".
				"`event_key`, ".
				"`employes_key`, ".
				"`user`, ".
				"`actif`".
				") VALUES (".
				"'" . $_GET["table"] . "', ".
				"'" . $_GET["event"] . "', ".
				"'" . $_GET["user"] . "', ".
				"'" . $_SESSION["user_key"] . "', ".
				"'1'".
				")"
			);

		} else {
			$DB->query(
				"UPDATE `inscriptions` ".
				"SET `actif` = '0' ".
				"WHERE `table` = '" . $_GET["table"] . "' ".
				"AND `event_key` = '" . $_GET["event"] . "' ".
				"AND `employes_key` = '" . $_GET["user"] . "' "
			);

			$DB->query(
				"INSERT INTO `inscriptions` (".
				"`table`, ".
				"`event_key`, ".
				"`employes_key`, ".
				"`user`, ".
				"`actif`".
				") VALUES (".
				"'" . $_GET["table"] . "', ".
				"'" . $_GET["event"] . "', ".
				"'" . $_GET["user"] . "', ".
				"'" . $_SESSION["user_key"] . "', ".
				"'0'".
				")"
			);
		}

		exit;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");

	if ($_GET["excel"] == 1) {
		header("Pragma: public");
	    header("Expires: 0");
	    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	    header("Content-Type: application/force-download");
	    header("Content-Type: application/octet-stream");
	    header("Content-Type: application/download");;
	    header("Content-Disposition: attachment;filename=conference-" . $_GET["table"] . ".xls ");
	    header("Content-Transfer-Encoding: binary ");
	}

	$content = "";
	switch ($_GET["table"]) {
		case "direction_de_travail":
			$DB->query(
				"SELECT dtea.status, e.prenom, e.nom, e.courriel ".
				"FROM `directions_de_travail_event_assists` dtea INNER JOIN `employes` e ON dtea.utilisateurs_key = e.key ".
				"WHERE dtea.directions_de_travail_event_id = '" . $_GET["id"] . "' ".
				"ORDER BY e.nom, e.prenom"
			);

			$content .=
				"<table>".
				"	<tr>".
				"		<td>Nom</td>".
				"		<td>Prenom</td>".
				"		<td>Courriel</td>".
				"		<td>Present</td>".
				"	</tr>";
			while ($DB->next_record()) {
				$content .=
					"<tr>".
					"	<td>" . $DB->getField("nom") . "</td>".
					"	<td>" . $DB->getField("prenom") . "</td>".
					"	<td>" . $DB->getField("courriel") . "</td>".
					"	<td>";

				switch ($DB->getField("status")) {
					case 1:
						$content .= "AM & PM"; break;
					case 2:
						$content .= "AM"; break;
					case 3:
						$content .= "PM"; break;
					case 4:
						$content .= "Go to Meeting"; break;
					default:
						$content .= "Non";
				}

				$content .=
					"	</td>".
					"</tr>";
			}
			$content .= "</table>";

			break;

		case "convocation_aga":
			$DB->query(
				"SELECT e.prenom, e.nom, e.courriel ".
				"FROM `inscriptions` i INNER JOIN `employes` e ON e.key = i.employes_key ".
				"WHERE i.table = 'convocation_aga_event' ".
				"AND i.event_key = '" . $_GET["id"] . "' ".
				"AND i.actif = '1' ".
				"ORDER BY e.nom, e.prenom"
			);

			$content .=
				"<table>".
				"	<tr>".
				"		<td>Nom</td>".
				"		<td>Prenom</td>".
				"		<td>Courriel</td>".
				"		<td>Present</td>".
				"	</tr>";
			while ($DB->next_record()) {
				$content .=
					"<tr>".
					"	<td>" . $DB->getField("nom") . "</td>".
					"	<td>" . $DB->getField("prenom") . "</td>".
					"	<td>" . $DB->getField("courriel") . "</td>".
					"	<td>Oui</td>".
					"</tr>";
			}
			$content .= "</table>";

			break;

		case "convocation_ag":
			$DB->query(
				"SELECT e.prenom, e.nom, e.courriel ".
				"FROM `inscriptions` i INNER JOIN `employes` e ON e.key = i.employes_key ".
				"WHERE i.table = 'convocation_ag_event' ".
				"AND i.event_key = '" . $_GET["id"] . "' ".
				"AND i.actif = '1' ".
				"ORDER BY e.nom, e.prenom"
			);

			$content .=
				"<table>".
				"	<tr>".
				"		<td>Nom</td>".
				"		<td>Prenom</td>".
				"		<td>Courriel</td>".
				"		<td>Present</td>".
				"	</tr>";
			while ($DB->next_record()) {
				$content .=
					"<tr>".
					"	<td>" . $DB->getField("nom") . "</td>".
					"	<td>" . $DB->getField("prenom") . "</td>".
					"	<td>" . $DB->getField("courriel") . "</td>".
					"	<td>Oui</td>".
					"</tr>";
			}
			$content .= "</table>";

			break;

		case "journee_formation":
			$DB->query(
				"SELECT e.prenom, e.nom, e.courriel ".
				"FROM `inscriptions` i INNER JOIN `employes` e ON e.key = i.employes_key ".
				"WHERE i.table = 'journee_formation_event' ".
				"AND i.event_key = '" . $_GET["id"] . "' ".
				"AND i.actif = '1' ".
				"ORDER BY e.nom, e.prenom"
			);

			$content .=
				"<table>".
				"	<tr>".
				"		<td>Nom</td>".
				"		<td>Prenom</td>".
				"		<td>Courriel</td>".
				"		<td>Present</td>".
				"	</tr>";
			while ($DB->next_record()) {
				$content .=
					"<tr>".
					"	<td>" . $DB->getField("nom") . "</td>".
					"	<td>" . $DB->getField("prenom") . "</td>".
					"	<td>" . $DB->getField("courriel") . "</td>".
					"	<td>Oui</td>".
					"</tr>";
			}
			$content .= "</table>";

			break;


	}

	if ($_GET["excel"] == 1) {
		echo $content;
	} else {
		$Skin->assign("form", $content);
		$Skin->display("evenements.tpl");
	}

	$DB->close();



?>
