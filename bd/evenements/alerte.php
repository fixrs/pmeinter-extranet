<?php

	require "../conf/conf.inc.php";

	session_start();

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;
	}

	import("com.quiboweb.form.Form");
	import("com.quiboweb.email.Email");
	import("com.pmeinter.Convocation_AG_event");
	import("com.pmeinter.Convocation_AGA_event");
	import("com.pmeinter.Direction_de_travail_event");
	import("com.pmeinter.Congres_event");
	import("com.pmeinter.Convocation_AGA_event");

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");

	if (count($_GET) > 0) {

		$content = "";

		if ($_GET["sent"] == 1) {
			$content = "<h1>MESSAGES ENVOY&Eacute;S</h1>";
			$_GET["table"] = ".......";
		}

		switch ($_GET["table"]) {
			case "direction_de_travail":

				$Evenement = new Direction_de_travail_event($DB, "", 0);
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_GET["id"]);
				$Evenement->loadAll();

				$description = $Evenement->get("description");

				$DB->query(
					"SELECT * FROM `directions_de_travail` WHERE `key` = '" . $Evenement->get("directions_de_travail_key") . "'"
				);

				while ($DB->next_record()) {
					$comite = $DB->getField("nom");
				}

				$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
				$description = preg_replace("/\[COMITE\]/", $comite, $description);
				$description = preg_replace("/\[NOTE\]/", $Evenement->get("note"), $description);
				$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);

				$description = preg_replace("/\[DATE_INSCRIPTION\]/is", translateDate($Evenement->get("date_inscription")), $description);
				$description = preg_replace("/\[HEURE_DEBUT\]/is", $Evenement->get("heure_debut"), $description);
				$description = preg_replace("/\[HEURE_FIN\]/is", $Evenement->get("heure_fin"), $description);
				$description = preg_replace("/\[CONFERENCIER\]/is", $Evenement->get("conferencier"), $description);
				$description = preg_replace("/\[SUJET\]/is", $Evenement->get("sujet"), $description);

				$description = preg_replace("/\[INSCRIPTIONS\]/", "", $description);

				$fichiers = "";
				for ($i = 1; $i < 6; $i++) {
					if (trim($Evenement->get("fichier" . $i)) != "" && file_exists($BASEPATH . "docs/evenements/" . $Evenement->get("fichier" . $i))) {
						$fichiers .= "<li><a href=\"http://bd.pmeinter.ca/docs/evenements/" . $Evenement->get("fichier" . $i) . "\" target=\"_blank\">Pi&egrave;ce jointe #" . $i . "</a></li>\n";
					}
				}

				if (trim($fichiers) != "") {
					$fichiers = "<ul>" . $fichiers . "</ul>";
				}

				$description = preg_replace("/\[FICHIERS\]/", $fichiers, $description);

				$content .= $description;

				$DB->query(
					"SELECT e.* ".
					"FROM `employes` e ".
					"INNER JOIN `employes_directions_de_travail` edt ON edt.directions_de_travail_key = '" . $Evenement->get("directions_de_travail_key") . "' AND edt.employes_key = e.key ".
					"WHERE e.actif = '1' ".
					"ORDER BY nom ASC"
				);

				$content .=
					"<form action=\"alerte.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"table\" value=\"" . $_GET["table"] . "\" />\n".
					"	<input type=\"hidden\" name=\"id\" value=\"" . $_GET["id"] . "\" />\n";

				$content .=
						"<p>\n".
						"	<label>TEST</label>\n".
						"	<input type=\"checkbox\" checked=\"checked\" name=\"to[]\" value=\"test\" />\n".
						"</p>\n";

				while ($DB->next_record()) {
					$content .=
						"<p>\n".
						"	<label>" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</label>\n".
						"	<input type=\"checkbox\" checked=\"checked\" name=\"to[]\" value=\"" . $DB->getField("courriel") . "\" />\n".
						"</p>\n";
				}

				$content .=
					"	<input type=\"submit\" value=\"Envoyer\" />\n".
					"</form>\n";

				break;

			case "convocation_aga":
				$Evenement = new Convocation_AGA_event($DB, "", 0);
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_GET["id"]);
				$Evenement->loadAll();

				$description = $Evenement->get("description");
				$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
				$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);
				$description = preg_replace("/\[DATE_LIMITE\]/", $Evenement->get("date_limite"), $description);
				$description = preg_replace("/\[INSCRIPTIONS\]/", "", $description);

				$content .= $description;

				$DB->query(
					"SELECT e.* ".
					"FROM `employes` e ".
					"INNER JOIN `employes_titres` et ON et.employes_key = e.key AND et.titres_key = '1' ".
					"WHERE e.actif = '1' ".
					"ORDER BY e.nom ASC"
				);

				$content .=
						"<p>\n".
						"	<label>TEST</label>\n".
						"	<input type=\"checkbox\" checked=\"checked\" name=\"to[]\" value=\"test\" />\n".
						"</p>\n";

				while ($DB->next_record()) {
					$content .=
						"<p>\n".
						"	<label>" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</label>\n".
						"	<input type=\"checkbox\" checked=\"checked\" name=\"to[]\" value=\"" . $DB->getField("courriel") . "\" />\n".
						"</p>\n";
				}

				break;

			case "convocation_ag":

				$Evenement = new Convocation_AG_event($DB, "", 0);
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_GET["id"]);
				$Evenement->loadAll();

				$description = $Evenement->get("description");
				$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
				$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);
				$description = preg_replace("/\[DATE_LIMITE\]/", $Evenement->get("date_limite"), $description);
				$description = preg_replace("/\[INSCRIPTIONS\]/", "", $description);

				$content .= $description;

				$DB->query(
					"SELECT e.* ".
					"FROM `employes` e ".
					"INNER JOIN `employes_titres` et ON et.employes_key = e.key AND et.titres_key = '1' ".
					"WHERE e.actif = '1' ".
					"ORDER BY e.nom ASC"
				);

				$content .=
						"<p>\n".
						"	<label>TEST</label>\n".
						"	<input type=\"checkbox\" checked=\"checked\" name=\"to[]\" value=\"test\" />\n".
						"</p>\n";

				while ($DB->next_record()) {
					$content .=
						"<p>\n".
						"	<label>" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</label>\n".
						"	<input type=\"checkbox\" checked=\"checked\" name=\"to[]\" value=\"" . $DB->getField("courriel") . "\" />\n".
						"</p>\n";
				}

				break;

			case "congres":

				$Evenement = new Congres_event($DB, "", 0);
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_GET["id"]);
				$Evenement->loadAll();

				$description = $Evenement->get("description");

				$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
				$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);
				$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($Evenement->get("date_limite")), $description);
				$description = preg_replace("/\[SUJET1\]/", translateDate($Evenement->get("sujet1")), $description);
				$description = preg_replace("/\[SUJET2\]/", translateDate($Evenement->get("sujet2")), $description);
				$description = preg_replace("/\[SUJET3\]/", translateDate($Evenement->get("sujet3")), $description);
				$description = preg_replace("/\[SUJET4\]/", translateDate($Evenement->get("sujet4")), $description);

				$description = preg_replace("/\[PRIX_NOTAIRE\]/", translateDate($Evenement->get("prix_notaire")), $description);
				$description = preg_replace("/\[PRIX_COLLABORATRICE\]/", translateDate($Evenement->get("prix_collaboratrice")), $description);

				$description = preg_replace("/\[NOTE\]/", translateDate($Evenement->get("note")), $description);

				$description = preg_replace("/\[HEBERGEMENT_LIMITE\]/", translateDate($Evenement->get("hebergement_limite")), $description);
				$description = preg_replace("/\[HEBERGEMENT_NOTE\]/", translateDate($Evenement->get("hebergement_note")), $description);
				$description = preg_replace("/\[HEBERGEMENT_COORDONNEES\]/", translateDate($Evenement->get("hebergement_coordonnees")), $description);
				$description = preg_replace("/\[HEBERGEMENT_DETAILS\]/", translateDate($Evenement->get("hebergement_details")), $description);
				$description = preg_replace("/\[HEBERGEMENT_OCCUPATION_SIMPLE\]/", translateDate($Evenement->get("hebergement_occupation_simple")), $description);
				$description = preg_replace("/\[HEBERGEMENT_OCCUPATION_DOUBLE\]/", translateDate($Evenement->get("hebergement_occupation_double")), $description);

				$description = preg_replace("/\[INSCRIPTIONS_NOTAIRES\]/", "", $description);
				$description = preg_replace("/\[INSCRIPTIONS_COLLABORATRICES\]/", "", $description);
				$description = preg_replace("/\[LISTE_ACTIVITES\]/", "", $description);
				$description = preg_replace("/\[INSCRIPTIONS_ACTIVITES\]/", "", $description);


				$content .= $description;

				break;

			case "journee_formation":

				$Evenement = new Journee_Formation_event($DB, "", 0);
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_GET["id"]);
				$Evenement->loadAll();

				$description = $Evenement->get("description");
				$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
				$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);
				$description = preg_replace("/\[COUT\]/", $Evenement->get("cout"), $description);
				$description = preg_replace("/\[SUJET\]/", $Evenement->get("sujet"), $description);
				$description = preg_replace("/\[DEPARTEMENT\]/", $Evenement->get("departement"), $description);
				$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($Evenement->get("date_limite")), $description);
				$description = preg_replace("/\[NOTE\]/", $Evenement->get("note"), $description);
				$description = preg_replace("/\[INSCRIPTIONS\]/", "", $description);

				$content .= $description;

				break;


		}
	} else if (count($_POST)) {

		switch ($_POST["table"]) {
			case "direction_de_travail":

				$Evenement = new Direction_de_travail_event($DB, "", 0);
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_POST["id"]);
				$Evenement->loadAll();

				$description = $Evenement->get("description");


				$DB->query(
					"SELECT * FROM `directions_de_travail` WHERE `key` = '" . $Evenement->get("directions_de_travail_key") . "'"
				);

				while ($DB->next_record()) {
					$comite = $DB->getField("nom");
				}

				$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
				$description = preg_replace("/\[COMITE\]/", $comite, $description);
				$description = preg_replace("/\[NOTE\]/", $Evenement->get("note"), $description);
				$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);

				$description = preg_replace("/\[DATE_INSCRIPTION\]/is", translateDate($Evenement->get("date_inscription")), $description);
				$description = preg_replace("/\[HEURE_DEBUT\]/is", $Evenement->get("heure_debut"), $description);
				$description = preg_replace("/\[HEURE_FIN\]/is", $Evenement->get("heure_fin"), $description);
				$description = preg_replace("/\[CONFERENCIER\]/is", $Evenement->get("conferencier"), $description);
				$description = preg_replace("/\[SUJET\]/is", $Evenement->get("sujet"), $description);


				//$description = preg_replace("/\[INSCRIPTIONS\]/", "", $description);

				$fichiers = "";
				for ($i = 1; $i < 6; $i++) {
					if (trim($Evenement->get("fichier" . $i)) != "" && file_exists($BASEPATH . "docs/evenements/" . $Evenement->get("fichier" . $i))) {
						$fichiers .= "<li><a href=\"http://bd.pmeinter.ca/docs/evenements/" . $Evenement->get("fichier" . $i) . "\">Pi&egrave;ce jointe #" . $i . "</a></li>\n";
					}
				}

				if (trim($fichiers) != "") {
					$fichiers = "<ul>" . $fichiers . "</ul>";
				}

				$description = preg_replace("/\[FICHIERS\]/", $fichiers, $description);

				if (in_array("test", $_POST["to"])) {

					$_POST["to"] = array("lgravel@pmeinter.com", "alex_thib10@hotmail.com", "info@pmeinter.com");

				}

				foreach ($_POST["to"] as $to) {

					$json = json_encode(
						array(
						"table" => 'direction_de_travail',
						"id" => $_POST["id"],
						"courriel" => $to
						)
					);

					$lien = "http://bd.pmeinter.ca/?i=" . base64_encode($json);

					//$to = "viensf@gmail.com";
					$email = new Email(
						"info@pmeinter.com",
						$to,
						"PME INTER : AVIS DE CONVOCATION : " . ucfirst(utf8_decode($comite)) . " " . $to,
						utf8_decode(
							preg_replace("/\[INSCRIPTIONS\]/", "<p>Veuillez suivre le lien suivant pour confirmer votre présence : <a href=\"" . $lien . "\">Confirmation d'intention</a></p>", $description)
						));
					$email->build();
					$email->send();
				}


				$DB->close();
				header("Location: alerte.php?table=" . $_POST["table"] . "&id=" . $_POST["id"] . "&sent=1");
				exit;


				break;

			case "convocation_aga":
				$Evenement = new Convocation_AGA_event($DB, "", 0);
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_GET["id"]);
				$Evenement->loadAll();

				$description = $Evenement->get("description");
				$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
				$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);
				$description = preg_replace("/\[DATE_LIMITE\]/", $Evenement->get("date_limite"), $description);
				$description = preg_replace("/\[INSCRIPTIONS\]/", "", $description);

				$content = $description;

				$DB->query(
					"SELECT e.* ".
					"FROM `employes` e ".
					"INNER JOIN `employes_titres` et ON et.employes_key = e.key AND et.titres_key = '1' ".
					"WHERE e.actif = '1' ".
					"ORDER BY e.nom ASC"
				);

				while ($DB->next_record()) {
					$content .=
						"<p>\n".
						"	<label>" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</label>\n".
						"	<input type=\"checkbox\" checked=\"checked\" name=\"to[]\" value=\"" . $DB->getField("courriel") . "\" />\n".
						"</p>\n";
				}

				break;

			case "convocation_ag":

				$Evenement = new Convocation_AG_event($DB, "", 0);
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_GET["id"]);
				$Evenement->loadAll();

				$description = $Evenement->get("description");
				$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
				$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);
				$description = preg_replace("/\[DATE_LIMITE\]/", $Evenement->get("date_limite"), $description);
				$description = preg_replace("/\[INSCRIPTIONS\]/", "", $description);

				$content = $description;

				$DB->query(
					"SELECT e.* ".
					"FROM `employes` e ".
					"INNER JOIN `employes_titres` et ON et.employes_key = e.key AND et.titres_key = '1' ".
					"WHERE e.actif = '1' ".
					"ORDER BY e.nom ASC"
				);

				while ($DB->next_record()) {
					$content .=
						"<p>\n".
						"	<label>" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</label>\n".
						"	<input type=\"checkbox\" checked=\"checked\" name=\"to[]\" value=\"" . $DB->getField("courriel") . "\" />\n".
						"</p>\n";
				}

				break;

			case "congres":

				$Evenement = new Congres_event($DB, "", 0);
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_GET["id"]);
				$Evenement->loadAll();

				$description = $Evenement->get("description");

				$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
				$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);
				$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($Evenement->get("date_limite")), $description);
				$description = preg_replace("/\[SUJET1\]/", translateDate($Evenement->get("sujet1")), $description);
				$description = preg_replace("/\[SUJET2\]/", translateDate($Evenement->get("sujet2")), $description);
				$description = preg_replace("/\[SUJET3\]/", translateDate($Evenement->get("sujet3")), $description);
				$description = preg_replace("/\[SUJET4\]/", translateDate($Evenement->get("sujet4")), $description);

				$description = preg_replace("/\[PRIX_NOTAIRE\]/", translateDate($Evenement->get("prix_notaire")), $description);
				$description = preg_replace("/\[PRIX_COLLABORATRICE\]/", translateDate($Evenement->get("prix_collaboratrice")), $description);

				$description = preg_replace("/\[NOTE\]/", translateDate($Evenement->get("note")), $description);

				$description = preg_replace("/\[HEBERGEMENT_LIMITE\]/", translateDate($Evenement->get("hebergement_limite")), $description);
				$description = preg_replace("/\[HEBERGEMENT_NOTE\]/", translateDate($Evenement->get("hebergement_note")), $description);
				$description = preg_replace("/\[HEBERGEMENT_COORDONNEES\]/", translateDate($Evenement->get("hebergement_coordonnees")), $description);
				$description = preg_replace("/\[HEBERGEMENT_DETAILS\]/", translateDate($Evenement->get("hebergement_details")), $description);
				$description = preg_replace("/\[HEBERGEMENT_OCCUPATION_SIMPLE\]/", translateDate($Evenement->get("hebergement_occupation_simple")), $description);
				$description = preg_replace("/\[HEBERGEMENT_OCCUPATION_DOUBLE\]/", translateDate($Evenement->get("hebergement_occupation_double")), $description);

				$description = preg_replace("/\[INSCRIPTIONS_NOTAIRES\]/", "", $description);
				$description = preg_replace("/\[INSCRIPTIONS_COLLABORATRICES\]/", "", $description);
				$description = preg_replace("/\[LISTE_ACTIVITES\]/", "", $description);
				$description = preg_replace("/\[INSCRIPTIONS_ACTIVITES\]/", "", $description);


				$content = $description;

				break;

			case "journee_formation":

				$Evenement = new Journee_Formation_event($DB, "", 0);
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_GET["id"]);
				$Evenement->loadAll();

				$description = $Evenement->get("description");
				$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
				$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);
				$description = preg_replace("/\[COUT\]/", $Evenement->get("cout"), $description);
				$description = preg_replace("/\[SUJET\]/", $Evenement->get("sujet"), $description);
				$description = preg_replace("/\[DEPARTEMENT\]/", $Evenement->get("departement"), $description);
				$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($Evenement->get("date_limite")), $description);
				$description = preg_replace("/\[NOTE\]/", $Evenement->get("note"), $description);
				$description = preg_replace("/\[INSCRIPTIONS\]/", "", $description);

				$content = $description;

				break;


		}


	}


	$Skin->assign("form", $content);
	$Skin->display("evenements.tpl");

	$DB->close();



?>
