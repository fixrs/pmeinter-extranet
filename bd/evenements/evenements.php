<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Evenement");
	
	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------
	
	$formString = "";
	$successString = "";
	$errorString = "";
	$titreString = "";
	$begin = getorpost('begin');
	$action = getorpost('action');
	$key = getorpost('event_key');


	switch ($action) {
		case "add" :
			$titreString = "<h2>Ajouter un &eacute;v&eacute;nement</h2>\n";
			if ($begin) {
				importForm("evenements");
				$FormObject = new Form("", "", getForm($action));
				$formString = $FormObject->returnForm();

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "evenements/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("evenements");
					$FormObject = new Form("", $_POST, getForm($action));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "modify" :
			$titreString = "<h2>Modifier un &eacute;v&eacute;nement</h2>\n";

			if ($begin) {
				importForm("evenements");
				$FormObject = new Form("", "", getForm($action));
				$FormObject->setValue("event_key", $key);			
				$FormObject->setValue("action", $action);
				$FormObject->setParameters(getAllFieldsEvenement($key));
				$formString = $FormObject->returnFilledForm();

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "evenements/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("evenements");
					$FormObject = new Form("", $_POST, getForm($action));
					$FormObject->setValue("event_key", $key);
					$FormObject->setValue("action", $action);
					$FormObject->setParameters($_POST);
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "read" :
			$titreString = "<h2>Consulter un &eacute;v&eacute;nement</h2>\n";
			importForm("evenements");
			$FormObject = new Form("", $_POST, getForm($action));
			$parameters = getAllFieldsEvenement($key, "read");
			$FormObject->setParameters($parameters);
			$FormObject->setAllHidden();
			$formString = $FormObject->returnFilledForm();			
			break;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");
	$Skin->assign("page", "evenements");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("titre", $titreString);
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("evenements.tpl");

	$DB->close();
	

	// -------------
	// LES FONCTIONS
	// -------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "PME Inter : Administration : &Eacute;v&eacute;nements : Ajout";
				break;
			case "modify":
				$title = "PME Inter : Administration : &Eacute;v&eacute;nements : Modification";
				break;
			case "delete":
				$title = "PME Inter : Administration : &Eacute;v&eacute;nements : Suppression";
				break;
			case "read":
				$title = "PME Inter : Administration : &Eacute;v&eacute;nements : Consultation";
				break;
			case "search":
				$title = "PME Inter : Administration : &Eacute;v&eacute;nements : Recherche";
				break;
			default:
				$title = "PME Inter : Administration : &Eacute;v&eacute;nements";
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "modify":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "read":
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "modify":
				break;

			case "modify":
				break;

			case "read":
				break;
		}
		return $html;
	}

	// Fonction qui filtre les POST provenant d'un formulaire d'évaluation
	// Retourne une string html d'erreurs OU retourne "" si aucune n'est détectée
	function validateFormValues($action) {
		$errorString = "";
		global $DB;
		$blnPremiereDate = true;
		$blnNomManquant = false;

		switch ($action) {
			case "add":
				if (trim($_POST['nom'])==""){
					$errorString = _error("<a href=\"javascript: showErrorField('nom');\">SVP entrez le nom de l'&eacute;v&eacute;nement</a><br />");
					$blnNomManquant = true;
				}
				if (trim($_POST['date_fin_inscription'])==""){
					$errorString .= _error("<a href=\"javascript: showErrorField('date_fin_inscription');\">SVP entrez la date de fin d'inscription &agrave; cet &eacute;v&eacute;nement</a><br />");
				}
				else {
					//on vérifie si la date est une vraie date
					if (!verifyDate(trim($_POST['date_fin_inscription']))){
						$errorString .= _error("<a href=\"javascript: showErrorField('date_fin_inscription');\">Le format de la date limite d'inscription doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						$blnPremiereDate = false;
					}
				}
				if (trim($_POST['date_debut'])==""){
					$errorString .= _error("<a href=\"#date_debut_v\" onclick=\"javascript: showErrorField('date_debut');\">SVP entrez la date de l'&eacute;v&eacute;nement</a><br />");
				}
				else {
					//on vérifie si la date est une vraie date
					if (!verifyDate(trim($_POST['date_debut']))){
						$errorString .= _error("<a href=\"#date_debut_v\" onclick=\"javascript: showErrorField('date_debut');\">Le format de la date doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						$blnPremiereDate = false;
					}
				}
				if (trim($_POST['date_fin'])!=""){
		
					if ($blnPremiereDate) {
						if (!verifyDate(trim($_POST['date_fin']))){
							$errorString .= _error("<a  href=\"#date_fin_v\" onclick=\"javascript: showErrorField('date_fin');\">Le format de la date doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						}
					}
				}
		
				if (trim($_POST['nombre_jours'])!=""){
					if (numero_is_numeric(trim($_POST['nombre_jours']))!=1){
						$errorString .= _error("<a href=\"#nombre_jours_v\" onclick=\"javascript: showErrorField('nombre_jours');\">Le nombre de jours doit &ecirc;tre un chiffre</a><br />");			
					}
				}
		
				if (trim($_POST['prix'])!=""){
					if (numero_is_numeric(trim($_POST['prix']))!=1){
						$errorString .= _error("<a href=\"#prix_v\" onclick=\"javascript: showErrorField('prix');\">Le prix doit &ecirc;tre un nombre</a><br />");			
					}			
				}
		
				if ( !$blnNomManquant && $action=="add" && verifyIfEventNameAndDateExists (trim($_POST['nom']), trim($_POST['date_debut']) ) ){
					$errorString .= _error("<a href=\"javascript: showErrorField('nom');\">Le nom de l'&eacute;v&eacute;nement et la date de d&eacute;but sont identiques &agrave; un &eacute;v&eacute;nement d&eacute;j&agrave; sauvegard&eacute;</a><br />");			
				}
			break;

			case "modify":
				if (trim($_POST['nom'])==""){
					$errorString = _error("<a href=\"javascript: showErrorField('nom');\">SVP entrez le nom de l'&eacute;v&eacute;nement</a><br />");
					$blnNomManquant = true;
				}
				if (trim($_POST['date_fin_inscription'])==""){
					$errorString .= _error("<a href=\"javascript: showErrorField('date_fin_inscription');\">SVP entrez la date de fin d'inscription &agrave; cet &eacute;v&eacute;nement</a><br />");
				}
				else {
					//on vérifie si la date est une vraie date
					if (!verifyDate(trim($_POST['date_fin_inscription']))){
						$errorString .= _error("<a href=\"javascript: showErrorField('date_fin_inscription');\">Le format de la date limite d'inscription doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						$blnPremiereDate = false;
					}
				}
				if (trim($_POST['date_debut'])==""){
					$errorString .= _error("<a href=\"#date_debut_v\" onclick=\"javascript: showErrorField('date_debut');\">SVP entrez la date de l'&eacute;v&eacute;nement</a><br />");
				}
				else {
					//on vérifie si la date est une vraie date
					if (!verifyDate(trim($_POST['date_debut']))){
						$errorString .= _error("<a href=\"#date_debut_v\" onclick=\"javascript: showErrorField('date_debut');\">Le format de la date doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						$blnPremiereDate = false;
					}
				}
				if (trim($_POST['date_fin'])!=""){
		
					if ($blnPremiereDate) {
						if (!verifyDate(trim($_POST['date_fin']))){
							$errorString .= _error("<a  href=\"#date_fin_v\" onclick=\"javascript: showErrorField('date_fin');\">Le format de la date doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						}
					}
				}
		
				if (trim($_POST['nombre_jours'])!=""){
					if (numero_is_numeric(trim($_POST['nombre_jours']))!=1){
						$errorString .= _error("<a href=\"#nombre_jours_v\" onclick=\"javascript: showErrorField('nombre_jours');\">Le nombre de jours doit &ecirc;tre un chiffre</a><br />");			
					}
				}
		
				if (trim($_POST['prix'])!=""){
					if (numero_is_numeric(trim($_POST['prix']))!=1){
						$errorString .= _error("<a href=\"#prix_v\" onclick=\"javascript: showErrorField('prix');\">Le prix doit &ecirc;tre un nombre</a><br />");			
					}			
				}
		
				if ( !$blnNomManquant && $action=="add" && verifyIfEventNameAndDateExists (trim($_POST['nom']), trim($_POST['date_debut']) ) ){
					$errorString .= _error("<a href=\"javascript: showErrorField('nom');\">Le nom de l'&eacute;v&eacute;nement et la date de d&eacute;but sont identiques &agrave; un &eacute;v&eacute;nement d&eacute;j&agrave; sauvegard&eacute;</a><br />");			
				}
			break;
		}

			
		return $errorString;
	}
	
	// Fonction qui met à jour les valeurs dela DB à partir des valeurs de POST provenant du formulaire
	// Retourne une string html d'erreurs OU retourne "" si aucune n'est détectée
	function updateDbValues($action) {
		global $DB;
		$errorString = "";

		switch ($action) {
			case "add":
				$Evenement = new Evenement($DB, "", "");
				
				$Evenement->scanfields();
				$Evenement->insertmode();

				$Evenement->set("key", trim($_POST['key']));
				$Evenement->set("nom", trim($_POST['nom']));
				$Evenement->set("date_fin_inscription", trim($_POST['date_fin_inscription']));		
				$Evenement->set("date_debut", trim($_POST['date_debut']));		
		
				if (trim($_POST['date_fin'])!="") {
					$Evenement->set("date_fin", trim($_POST['date_fin']));			
				}
				$Evenement->set("date_precision", trim($_POST['date_precision']));
				$Evenement->set("heure", trim($_POST['heure']));
				$Evenement->set("nombre_jours", trim($_POST['nombre_jours']));
				$Evenement->set("coordonnees", trim($_POST['coordonnees']));
				$Evenement->set("salle", trim($_POST['salle']));
				$Evenement->set("nom_responsable", trim($_POST['nom_responsable']));		
				$Evenement->set("telephone_responsable", trim($_POST['telephone_responsable']));
				$Evenement->set("description", trim($_POST['description']));
				$Evenement->set("conferencier", trim($_POST['conferencier']));
				$Evenement->set("prix", trim($_POST['prix']));
				$Evenement->set("notes", trim($_POST['notes']));
				$Evenement->set("actif", '1');
	
				if (!$Evenement->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion.");
				}
			break;
						
			case "modify":
	
				$Evenement = new Evenement($DB, "", "");
				
				$Evenement->scanfields();
				$Evenement->setQKey("key", $_POST['event_key']);			
	
				$Evenement->set("key", trim($_POST['key']));
				$Evenement->set("nom", trim($_POST['nom']));
				$Evenement->set("date_fin_inscription", trim($_POST['date_fin_inscription']));		
				$Evenement->set("date_debut", trim($_POST['date_debut']));		
		
				if (trim($_POST['date_fin'])!="") {
					$Evenement->set("date_fin", trim($_POST['date_fin']));			
				}
				$Evenement->set("date_precision", trim($_POST['date_precision']));
				$Evenement->set("heure", trim($_POST['heure']));
				$Evenement->set("nombre_jours", trim($_POST['nombre_jours']));
				$Evenement->set("coordonnees", trim($_POST['coordonnees']));
				$Evenement->set("salle", trim($_POST['salle']));
				$Evenement->set("nom_responsable", trim($_POST['nom_responsable']));		
				$Evenement->set("telephone_responsable", trim($_POST['telephone_responsable']));
				$Evenement->set("description", trim($_POST['description']));
				$Evenement->set("conferencier", trim($_POST['conferencier']));
				$Evenement->set("prix", trim($_POST['prix']));
				$Evenement->set("notes", trim($_POST['notes']));
				$Evenement->set("actif", '1');

				if (!$Evenement->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de la modification.");
				}
			break;
		}
		
				
		return $errorString;		
	}

	/**
	* function
	*
	* Description: to verify if the date is a real date
	*	
	* @access public
	* @param $date the date entered in the form (has to be YYYY-MM-DD)
	* 
	* @return true or false
	*/		
	
	function verifyDate($date){
		$blnOk = true;
		if (strlen($date)==10){

			if (strpos($date, "-")=== false){
				$blnOk = false;
			}
			else {
				$arrDate = explode( "-", $date);
				if ($date != date("Y-m-d", mktime(0, 0, 0, $arrDate[1], $arrDate[2], $arrDate[0]))){
					
					$blnOk = false;
				}
			}
		}
		else {
			$blnOk = false;
		}
		return $blnOk;
	}
	
	/**
	* function
	*
	* @access public
	* @param primary key value
	* 
	* @return array 
	*/		
	
	function getAllFieldsEvenement($event_key, $read=""){
		global $DB;

		$Evenement = new Evenement($DB, "", 0);
		$Evenement->scanfields();
		$Evenement->setQKey("key", $event_key);
		$Evenement->loadAll();
		if ($Evenement->get("nombre_jours")=="0"){
			$Evenement->set("nombre_jours", "");
		}
		if ($Evenement->get("date_fin")=="0000-00-00"){
			$Evenement->set("date_fin", "");
		}
		if ($Evenement->get("prix")=="0.00"){
				$Evenement->set("prix", "");
		}
		if ($read=="read"){
			
			if ($Evenement->get("prix")!=""){
				
				$prix = $Evenement->get("prix");
				$Evenement->set("prix", $prix." $");			
			}
		}
		
		return $Evenement->getAll();
	}


	/**
	* function
	* 
	* @access public
	* @param number value
	* 
	* @return 1 or empty
	*/		
	
	function numero_is_numeric($value)
	{
		return (preg_match ("/^(-){0,1}([0-9]+)(,[0-9][0-9][0-9])*([.][0-9]){0,1}([0-9]*)$/", $value) == 1);
	}

	/**
	* function
	* 
	* @access public
	* @param name of the event and starting date of the event 
	* 
	* @return true (if both exists) or false
	*/		
	
	function verifyIfEventNameAndDateExists($name, $event_date)
	{
		global $DB;
		
		$blnBothExists = false;
		
		$Evenement = new Evenement($DB, "", "");
		$Evenement->scanfields();
		$Evenement->setQKey("nom", $name);
		$Evenement->loadAll();

		if ($Evenement->get('date_debut')==$event_date && $Evenement->get('actif')=='1'){
			$blnBothExists = true;			
		}
		return $blnBothExists;
	}

	function getParametersFromPOST($action) {
		$parameters = array();
		switch ($action) {
			case "add":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;			
			case "modify":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;	
		}
		return $parameters;
	}

?>
