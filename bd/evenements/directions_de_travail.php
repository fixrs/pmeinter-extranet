<?php

	require "../conf/conf.inc.php";

	import("com.quiboweb.email.Email");

	session_start();

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");


	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	if ($_GET["e"] != "" && $_GET["s"] != "" && $_GET["ek"] != "") {

		if (isset($_GET["i"])) {
			$json = json_decode(base64_decode($_GET["i"]), true);
			$json_email = $json["courriel"];

			$DB->query(
				"SELECT e.key ".
				"FROM `employes` e ".
				"WHERE e.courriel = '" . $json_email . "'"
			);

			while ($DB->next_record()) {
				$employe_key = $DB->getField("key");
			}

			if ($employe_key != $_GET["ek"]) {
				$DB->close();
				exit;
			}
		}

		//Présent via Go to Meeting, Présent physiquement, et de pouvoir cocher am, pm ou les 2.
		$DB->query(
			"SELECT * ".
			"FROM directions_de_travail_event_assists ".
			"WHERE directions_de_travail_event_id = '" . $_GET["e"] . "' ".
			"AND utilisateurs_key = '" . $_GET["ek"] . "'"
		);

		$assist_key = "";

		if ($DB->getNumRows() > 0) {
			$DB->query(
				"UPDATE `directions_de_travail_event_assists` ".
				"SET `status` = '" . $_GET["s"] . "' ".
				"WHERE `directions_de_travail_event_id` = '" . $_GET["e"] . "' ".
				"AND `utilisateurs_key` = '" . $_GET["ek"] . "'"
			);
		} else {
			$DB->query(
				"INSERT INTO `directions_de_travail_event_assists` ".
				"(`status`, `directions_de_travail_event_id`, `utilisateurs_key`) VALUES (".
				"'" . $_GET["s"] . "', ".
				"'" . $_GET["e"] . "', ".
				"'" . $_GET["ek"] . "')"
			);
		}

		if ($_GET["s"] == 1) {

			echo
				"	<li class=\"present\"><strong>Pr&eacute;sent(e) &agrave; la r&eacute;union AM et PM</strong></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=2&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM</a></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=3&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union PM</a></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=4&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) via Go to Meeting</a></li>\n".
				"	<li class=\"absent\"><a href=\"/evenements/directions_de_travail.php?e=" .  $_GET["e"] . "&s=0&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Absent(e) &agrave; la r&eacute;union</a></li>\n".
				"	<li class=\"confirmation\">Nous vous remercions de votre confirmation.<br />Un accusé réception vous sera transmis par courriel.</li>\n";

		} else if ($_GET["s"] == 2) {

			echo
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=1&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM et PM</a></li>\n".
				"	<li class=\"present\"><strong>Pr&eacute;sent(e) &agrave; la r&eacute;union AM</strong></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=3&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union PM</a></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=4&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) via Go to Meeting</a></li>\n".
				"	<li class=\"absent\"><a href=\"/evenements/directions_de_travail.php?e=" .  $_GET["e"] . "&s=0&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Absent(e) &agrave; la r&eacute;union</a></li>\n".
				"	<li class=\"confirmation\">Nous vous remercions de votre confirmation.<br />Un accusé réception vous sera transmis par courriel.</li>\n";

		} else if ($_GET["s"] == 3) {

			echo
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=1&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM et PM</a></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=2&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM</a></li>\n".
				"	<li class=\"present\"><strong>Pr&eacute;sent(e) &agrave; la r&eacute;union PM</strong></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=4&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) via Go to Meeting</a></li>\n".
				"	<li class=\"absent\"><a href=\"/evenements/directions_de_travail.php?e=" .  $_GET["e"] . "&s=0&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Absent(e) &agrave; la r&eacute;union</a></li>\n".
				"	<li class=\"confirmation\">Nous vous remercions de votre confirmation.<br />Un accusé réception vous sera transmis par courriel.</li>\n";

		} else if ($_GET["s"] == 4) {

			echo
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=1&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM et PM</a></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=2&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM</a></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=3&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union PM</a></li>\n".
				"	<li class=\"present\"><strong>Pr&eacute;sent(e) via Go to Meeting</strong></li>\n".
				"	<li class=\"absent\"><a href=\"/evenements/directions_de_travail.php?e=" .  $_GET["e"] . "&s=0&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Absent(e) &agrave; la r&eacute;union</a></li>\n".
				"	<li class=\"confirmation\">Nous vous remercions de votre confirmation.<br />Un accusé réception vous sera transmis par courriel.</li>\n";

		} else {

			echo
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=1&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM et PM</a></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=2&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM</a></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=3&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union PM</a></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $_GET["e"] . "&s=4&ek=" . $_GET["ek"] . (isset($_GET["i"]) ? "&i=" . $_GET["i"] : "") . "\">Pr&eacute;sent(e) via Go to Meeting</a></li>\n".
				"	<li class=\"absent\"><strong>Absent(e) &agrave; la r&eacute;union</strong></li>\n".
				"	<li class=\"confirmation\">Nous vous remercions de votre confirmation.<br />Un accusé réception vous sera transmis par courriel.</li>\n";

		}


		if ($json_email != "") {

			$statut = "absence";
			if ($_GET["s"] == 1) { $statut = "présence"; }

			$message = "";

			$DB->query(
				"SELECT * ".
				"FROM directions_de_travail_event ".
				"WHERE id = '" . $_GET["e"] . "' "
			);

			while ($DB->next_record()) {
				$message = $DB->getField("email_confirmation");
				$message = preg_replace("/\[STATUT\]/is", $statut, $message);
				$message = preg_replace("/\[LIEU\]/is", $DB->getField("lieu"), $message);
				$message = preg_replace("/\[NOTE\]/is", $DB->getField("note"), $message);
				$message = preg_replace("/\[DATE\]/is", $DB->getField("date"), $message);

				$message = preg_replace("/\[DATE_INSCRIPTION\]/is", $DB->getField("date_inscription"), $message);
				$message = preg_replace("/\[HEURE_DEBUT\]/is", $DB->getField("heure_debut"), $message);
				$message = preg_replace("/\[HEURE_FIN\]/is", $DB->getField("heure_fin"), $message);
				$message = preg_replace("/\[CONFERENCIER\]/is", $DB->getField("conferencier"), $message);
				$message = preg_replace("/\[SUJET\]/is", $DB->getField("sujet"), $message);

				$directions_de_travail_key = $DB->getField("directions_de_travail_key");

				$DB->query(
					"SELECT * FROM `directions_de_travail` WHERE `key` = '" . $directions_de_travail_key . "'"
				);

				$comite = "";
				while ($DB->next_record()) {
					$comite = $DB->getField("nom");
				}

				$message = preg_replace("/\[COMITE\]/is", $comite, $message);

			}

			if (trim($message) != "") {
				$email = new Email(
					"info@pmeinter.com",
					$json_email,
					"PME INTER : CONFIRMATION : " . ucfirst(utf8_decode($comite)),
					utf8_decode($message));
				$email->build();
				$email->send();
			}

		}



		$DB->close();

		exit;
	}



	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;
	}



	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;
	}

	import("com.quiboweb.form.Form");
	import("com.pmeinter.Direction_de_travail_event");






	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$formString = "";
	$successString = "";
	$errorString = "";
	$titreString = "";
	$action = getorpost('action');
	$id = getorpost('id');
	$begin = getorpost("begin");

	if (count($_POST) == 0) {
		switch ($action) {
			case "add":
				$formString = "<h2>Ajouter un &eacute;v&eacute;nement</h2>\n";
				importForm("directions_de_travail");
				$FormObject = new Form("", "", getForm($action));
				$formString .= $FormObject->returnForm();
				break;

			case "modify":
				$formString = modifyEvenementsString();
				break;
			case "delete":
				$formString = deleteEvenementsString();
				break;
			case "read":
				$formString = readEvenementsString();
				break;
		}
	} else {
		switch ($action) {
			case "add" :
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "evenements/directions_de_travail.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("directions_de_travail");
					$FormObject = new Form("", $_POST, getForm($action));
					$formString = $FormObject->returnFilledForm();
				}
				break;

			case "modify" :
				$titreString = "<h2>Modifier un &eacute;v&eacute;nement</h2>\n";

				if ($begin) {
					importForm("directions_de_travail");
					$FormObject = new Form("", "", getForm($action));
					$FormObject->setValue("id", $id);
					$FormObject->setValue("action", $action);
					$FormObject->setParameters(getAllFieldsEvenement($id));
					$formString = $FormObject->returnFilledForm();

				} else {
					$errorString = validateFormValues($action);
					if ($errorString == "") {
						$errorString = updateDbValues($action);
						if ($errorString == "") {
							$successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
							$formString = "<a href=\"" . $BASEURL . "evenements/directions_de_travail.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
						}
					} else {
						importForm("directions_de_travail");
						$FormObject = new Form("", $_POST, getForm($action));
						$FormObject->setValue("id", $id);
						$FormObject->setValue("action", $action);
						$FormObject->setParameters($_POST);
						$formString = $FormObject->returnFilledForm();
					}
				}
				break;

			case "read" :
				$titreString = "<h2>Consulter un &eacute;v&eacute;nement</h2>\n";
				importForm("directions_de_travail");
				$FormObject = new Form("", $_POST, getForm($action));
				$parameters = getAllFieldsEvenement($id, "read");
				$FormObject->setParameters($parameters);
				$FormObject->setAllHidden();
				$formString = $FormObject->returnFilledForm();
				break;
		}
	}



	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");
	$Skin->assign("page", "directions_de_travail");
	$Skin->assign("title", pageTitleString(getorpost('action')));
	$Skin->assign("menu", menuEvenementsString());
	$Skin->assign("menu_title", menuTitleString(getorpost('action')));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("evenements.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------



	function getAllFieldsEvenement($id, $read=""){
		global $DB, $BASEPATH;

		$Evenement = new Direction_de_travail_event($DB, "", 0);
		$Evenement->scanfields();
		$Evenement->setQKey("id", $id);
		$Evenement->loadAll();

		if ($read=="read"){
			$description = $Evenement->get("description");

			$DB->query(
				"SELECT * FROM `directions_de_travail` WHERE `key` = '" . $Evenement->get("directions_de_travail_key") . "'"
			);

			while ($DB->next_record()) {
				$comite = $DB->getField("nom");
			}

			$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
			$description = preg_replace("/\[COMITE\]/", $comite, $description);
			$description = preg_replace("/\[NOTE\]/", $Evenement->get("note"), $description);
			$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);
			$description = preg_replace("/\[INSCRIPTIONS\]/", "", $description);
			$description = preg_replace("/\[DATE_INSCRIPTION\]/is", translateDate($Evenement->get("date_inscription")), $description);
			$description = preg_replace("/\[HEURE_DEBUT\]/is", $Evenement->get("heure_debut"), $description);
			$description = preg_replace("/\[HEURE_FIN\]/is", $Evenement->get("heure_fin"), $description);
			$description = preg_replace("/\[CONFERENCIER\]/is", $Evenement->get("conferencier"), $description);
			$description = preg_replace("/\[SUJET\]/is", $Evenement->get("sujet"), $description);

			$fichiers = "";
			for ($i = 1; $i < 6; $i++) {
				if (trim($Evenement->get("fichier" . $i)) != "" && file_exists($BASEPATH . "docs/evenements/" . $Evenement->get("fichier" . $i))) {
					$fichiers .= "<li><a href=\"http://bd.pmeinter.ca/docs/evenements/" . $Evenement->get("fichier" . $i) . "\" target=\"_blank\">Pi&egrave;ce jointe #" . $i . "</a></li>\n";
				}
			}

			if (trim($fichiers) != "") {
				$fichiers = "<ul>" . $fichiers . "</ul>";
			}

			$description = preg_replace("/\[FICHIERS\]/", $fichiers, $description);

			$description = "<h2 style=\"font-size: 1.4em; color: #253767; margin-top: 2px; margin-bottom: 22px; text-transform: uppercase; text-decoration: none; text-align: center;padding: 20px;\">" . $Evenement->get("note") . "</h2>\n" . $description;

			$Evenement->set("description", $description);
		}

		return $Evenement->getAll();

	}



	function validateFormValues($action) {
		global $DB, $EMAIL_FORMAT_PATTERN;
		$errorString = "";
		switch ($action) {
			case "add":
				if ($_SESSION['user_type'] != 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('type');\">Vous ne poss&eacute;ss&eacute;dez pas les droits requis pour cr&eacute;er cet &eacute;v&eacute;nement.</a>");
				}

				if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $_POST["date"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date');\">Veuillez indiquez une date valide.</a>");
				}

				if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $_POST["date_inscription"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date_inscription');\">Veuillez indiquez une date valide.</a>");
				}

				if (trim($_POST["lieu"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('lieu');\">Veuillez indiquez un lieu valide.</a>");
				}

				if (trim($_POST["note"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('note');\">Veuillez indiquez une note valide.</a>");
				}

				if (trim($_POST["directions_de_travail_key"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('directions_de_travail_key');\">Veuillez choisir un comit&eacute;.</a>");
				}

				if (trim($_POST["actif"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('actif');\">Veuillez indiquez si l'&eacute;v&eacute;nement est actif ou non.</a>");
				}

				for ($i = 1; $i < 6; $i++) {
					if ($_FILES['fichier' . $i . '_up']['name'] != "") {
						$file_error = $_FILES['fichier' . $i . '_up']['error'];
						if ($file_error) {
							$errorString .= _error("<a href=\"javascript: showErrorField('fichier" . $i . "_up');\">Une erreur s'est produite avec le fichier.</a>");
						}
					}
				}

				break;

			case "modify":
				if ($_SESSION['user_type'] != 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('type');\">Vous ne poss&eacute;ss&eacute;dez pas les droits requis pour modifier cet &eacute;v&eacute;nement.</a>");
				}

				if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $_POST["date"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date');\">Veuillez indiquez une date valide.</a>");
				}

				if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $_POST["date_inscription"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date_inscription');\">Veuillez indiquez une date valide.</a>");
				}

				if (trim($_POST["lieu"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('lieu');\">Veuillez indiquez un lieu valide.</a>");
				}

				if (trim($_POST["note"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('note');\">Veuillez indiquez une note valide.</a>");
				}

				if (trim($_POST["directions_de_travail_key"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('directions_de_travail_key');\">Veuillez choisir un comit&eacute;.</a>");
				}

				if (trim($_POST["actif"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('actif');\">Veuillez indiquez si l'&eacute;v&eacute;nement est actif ou non.</a>");
				}

				for ($i = 1; $i < 6; $i++) {
					if ($_FILES['fichier' . $i . '_up']['name'] != "") {
						$file_error = $_FILES['fichier' . $i . '_up']['error'];
						if ($file_error) {
							$errorString .= _error("<a href=\"javascript: showErrorField('fichier" . $i . "_up');\">Une erreur s'est produite avec le fichier.</a>");
						}
					}
				}

				break;
		}
		return $errorString;
	}

	function updateDBValues($action) {
		global $DB, $BASEPATH;
		$errorString = "";

		$parameters = addslashesToValues($_POST);

		switch ($action) {
			case "add":

				$Evenement = new Direction_de_travail_event($DB, "", "");

				$Evenement->scanfields();
				$Evenement->insertmode();

				$Evenement->set("id", trim($_POST['id']));
				$Evenement->set("date", trim($_POST['date']));

				$Evenement->set("date_inscription", trim($_POST['date_inscription']));
				$Evenement->set("heure_debut", trim($_POST['heure_debut']));
				$Evenement->set("heure_fin", trim($_POST['heure_fin']));
				$Evenement->set("conferencier", trim($_POST['conferencier']));
				$Evenement->set("sujet", trim($_POST['sujet']));

				$Evenement->set("lieu", trim($_POST['lieu']));
				$Evenement->set("note", trim($_POST['note']));
				$Evenement->set("directions_de_travail_key", trim($_POST['directions_de_travail_key']));
				$Evenement->set("description", trim($_POST['description']));
				$Evenement->set("email_confirmation", trim($_POST['email_confirmation']));
				$Evenement->set("actif", trim($_POST['actif']));


				for ($i = 1; $i < 6; $i++) {
					if ($_FILES["fichier" . $i . "_up"]["name"] != "") {
						$tmp_name = $_FILES["fichier" . $i . "_up"]["tmp_name"];
						$name = $_FILES["fichier" . $i . "_up"]["name"];
						$extension = substr($name, strrpos($name, "."));
						$newname = "direction" . $i . "_" . $_POST["id"] . $extension;
						$move = move_uploaded_file($tmp_name, $BASEPATH . "docs/evenements/" . $newname);
						$Evenement->set("fichier" . $i, $newname);
						if (!$Evenement->save("fichier" . $i)) {
							$errorString .= _error("Une erreur s'est produite lors de l'insertion du fichier sur le serveur.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
							break;
						}
					}
				}

				if (!$Evenement->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion.");
				}

				break;

			case "modify":

				$Evenement = new Direction_de_travail_event($DB, "", "");

				$Evenement->scanfields();
				$Evenement->setQKey("id", $_POST['id']);

				$Evenement->set("id", trim($_POST['id']));
				$Evenement->set("date", trim($_POST['date']));

				$Evenement->set("date_inscription", trim($_POST['date_inscription']));
				$Evenement->set("heure_debut", trim($_POST['heure_debut']));
				$Evenement->set("heure_fin", trim($_POST['heure_fin']));
				$Evenement->set("conferencier", trim($_POST['conferencier']));
				$Evenement->set("sujet", trim($_POST['sujet']));

				$Evenement->set("lieu", trim($_POST['lieu']));
				$Evenement->set("note", trim($_POST['note']));
				$Evenement->set("directions_de_travail_key", trim($_POST['directions_de_travail_key']));
				$Evenement->set("description", trim($_POST['description']));
				$Evenement->set("email_confirmation", trim($_POST['email_confirmation']));
				$Evenement->set("actif", trim($_POST['actif']));

				for ($i = 1; $i < 6; $i++) {
					if ($_FILES["fichier" . $i . "_up"]["name"] != "") {
						$tmp_name = $_FILES["fichier" . $i . "_up"]["tmp_name"];
						$name = $_FILES["fichier" . $i . "_up"]["name"];
						$extension = substr($name, strrpos($name, "."));
						$newname = "direction" . $i . "_" . $_POST['id'] . $extension;
						$move = move_uploaded_file($tmp_name, $BASEPATH . "docs/evenements/" . $newname);
						$Evenement->set("fichier" . $i, $newname);
						if (!$Evenement->save("fichier" . $i)) {
							$errorString .= _error("Une erreur s'est produite lors de l'insertion du fichier sur le serveur.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
							break;
						}

					} else {
						if ($parameters['fichier' . $i . '_del'] == "1") {
							$fichier = $parameters['fichier' . $i];
							$fichier_path = $BASEPATH . "docs/evenements/" . $fichier;
							if (file_exists($fichier_path)) {
								unlink($fichier_path);
							}
							$Evenement->set("fichier" . $i, "NULL", 1);
						} else {
							$Evenement->excludefield("fichier" . $i);
						}
					}
				}


				if (!$Evenement->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de la modification.");
				}

				break;
		}
		return $errorString;
	}

	function addslashesToValues($parameters = "") {
		if (is_array($parameters) && count($parameters) > 0) {
			foreach ($parameters as $field => $value) {
				if (is_array($value) && count($value) > 0) {
					foreach ($value as $field2 => $value2) {
						if (is_array($value2) && count($value2) > 0) {
							foreach ($value2 as $field3 => $value3) {
								$parameters[$field][$field2][$field3] = addslashes($value3);
							}
						} else {
							$parameters[$field][$field2] = addslashes($value2);
						}
					}
				} else {
					$parameters[$field] = addslashes($value);
				}
			}
		} else {
			$parameters = addslashes($parameters);
		}
		return $parameters;
	}

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) {
		switch ($action) {
			case "add":
				$title = "Ajout d'un &eacute;v&eacute;nement";
				break;
			case "modify":
				$title = "Modification d'un &eacute;v&eacute;nement";
				break;
			case "delete":
				$title = "Suppression d'un &eacute;v&eacute;nement";
				break;
			case "read":
				$title = "Consultation d'un &eacute;v&eacute;nement";
				break;
		}
		return $title;
	}

	// Fonction qui retourne le html pour le menu d'actions des evenements
	function menuEvenementsString() {
		$html =
			"<h1>&Eacute;v&eacute;nements - Directions de travail</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"directions_de_travail.php?action=add\">Ajouter un &eacute;v&eacute;nement</a></li>\n".
			"		<li><a href=\"directions_de_travail.php?action=modify\">Modifier un &eacute;v&eacute;nement</a></li>\n".
			//"		<li><a href=\"directions_de_travail.php?action=delete\">Supprimer un &eacute;v&eacute;nement</a></li>\n".
			"		<li><a href=\"directions_de_travail.php?action=read\">Consulter un &eacute;v&eacute;nement</a></li>\n".
			"	</ul>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le titre du sous-menu
	function menuTitleString($action) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .= "<h2>Modifier un &eacute;v&eacute;nement</h2>\n";
				break;
			case "delete":
				$html .= "<h2>Supprimer un &eacute;v&eacute;nement</h2>\n";
				break;
			case "read":
				$html .= "<h2>Consulter un &eacute;v&eacute;nement</h2>\n";
				break;
		}
		return $html;
	}

	function getEvenementsOptionsString($id=""){
		global $DB;
		$html = "";

		$DB->query(
			"SELECT de.id, de.date, d.nom ".
			"FROM `directions_de_travail_event` de INNER JOIN `directions_de_travail` d ON d.key = de.directions_de_travail_key ".
			//"WHERE de.actif = '1' ".
			"ORDER BY `date` DESC"
		);
		while($DB->next_record()) {
			$selected = "";
			if ($id != "" && $id == $DB->getField("id")){
				$selected = "selected=\"selected\" ";
			}
			$html .= "<option ".$selected."value=\"" . $DB->getField("id") . "\">" . $DB->getField("nom") . " (" . $DB->getField("date") . ")</option>\n";
		}

		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à modifier
	function modifyEvenementsString() {
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"directions_de_travail.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"		<input type=\"hidden\" name=\"begin\" value=\"1\" />\n".
			"		<select name=\"id\">\n".
			"			<option value=\"0\">- Choisir un &eacute;v&eacute;nement -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à consulter
	function readEvenementsString() {
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"directions_de_travail.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"		<input type=\"hidden\" name=\"begin\" value=\"1\" />\n".
			"		<select name=\"id\">\n".
			"			<option value=\"0\">- Choisir un &eacute;v&eacute;nement -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à supprimer
	function deleteEvenementsString() {
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"directions_de_travail.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"		<input type=\"hidden\" name=\"begin\" value=\"1\" />\n".
			"		<select name=\"id\">\n".
			"			<option value=\"0\">- Choisir un &eacute;v&eacute;nement -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Supprimer\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui supprime une formation de la DB
	function deleteEvenementsFromDB($id) {
		global $DB;

		//$Evenement->set('actif', '0');

	}

