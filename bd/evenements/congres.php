<?php

	require "../conf/conf.inc.php";

	session_start();

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);

		

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Congres_event");
	





	// ------------------
	// LE TRAITEMENT HTML
	// ------------------
	
	$formString = "";
	$successString = "";
	$errorString = "";
	$titreString = "";
	$action = getorpost('action');
	$id = getorpost('id');
	$begin = getorpost("begin");

	if (count($_POST) == 0) {
		switch ($action) {
			case "add":
				$formString = "<h2>Ajouter un congr&egrave;s</h2>\n";
				importForm("congres");
				$FormObject = new Form("", "", getForm($action));
				$formString .= $FormObject->returnForm();
				break;

			case "modify":
				$formString = modifyEvenementsString();
				break;
			case "delete":
				$formString = deleteEvenementsString();
				break;
			case "read":
				$formString = readEvenementsString();
				break;
		}
	} else {
		switch ($action) {
			case "add" :
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "evenements/congres.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("congres");
					$FormObject = new Form("", $_POST, getForm($action));
					$formString = $FormObject->returnFilledForm();
				}
				break;

			case "modify" :
				$titreString = "<h2>Modifier un congr&egrave;s</h2>\n";

				if ($begin) {
					importForm("congres");
					$FormObject = new Form("", "", getForm($action));
					$FormObject->setValue("id", $id);			
					$FormObject->setValue("action", $action);
					$FormObject->setParameters(getAllFieldsEvenement($id));
					$formString = $FormObject->returnFilledForm();

				} else {
					$errorString = validateFormValues($action);
					if ($errorString == "") {
						$errorString = updateDbValues($action);
						if ($errorString == "") {
							$successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
							$formString = "<a href=\"" . $BASEURL . "evenements/congres.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
						}
					} else {
						importForm("congres");
						$FormObject = new Form("", $_POST, getForm($action));
						$FormObject->setValue("id", $id);
						$FormObject->setValue("action", $action);
						$FormObject->setParameters($_POST);
						$formString = $FormObject->returnFilledForm();
					}
				}
				break;

			case "read" :
				$titreString = "<h2>Consulter un congr&egrave;s</h2>\n";
				importForm("congres");
				$FormObject = new Form("", $_POST, getForm($action));
				$parameters = getAllFieldsEvenement($id, "read");
				$FormObject->setParameters($parameters);
				$FormObject->setAllHidden();
				$formString = $FormObject->returnFilledForm();			
				break;
		}
	}



	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");	
	$Skin->assign("page", "congres");
	$Skin->assign("title", pageTitleString(getorpost('action')));
	$Skin->assign("menu", menuEvenementsString());
	$Skin->assign("menu_title", menuTitleString(getorpost('action')));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("evenements.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------



	function getAllFieldsEvenement($id, $read=""){
		global $DB;

		$Evenement = new Congres_event($DB, "", 0);
		$Evenement->scanfields();
		$Evenement->setQKey("id", $id);
		$Evenement->loadAll();
		
		if ($read=="read"){
			$description = $Evenement->get("description");
			
			$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
			$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);
			$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($Evenement->get("date_limite")), $description);
			$description = preg_replace("/\[SUJET1\]/", translateDate($Evenement->get("sujet1")), $description);
			$description = preg_replace("/\[SUJET2\]/", translateDate($Evenement->get("sujet2")), $description);
			$description = preg_replace("/\[SUJET3\]/", translateDate($Evenement->get("sujet3")), $description);
			$description = preg_replace("/\[SUJET4\]/", translateDate($Evenement->get("sujet4")), $description);

			$description = preg_replace("/\[PRIX_NOTAIRE\]/", translateDate($Evenement->get("prix_notaire")), $description);
			$description = preg_replace("/\[PRIX_COLLABORATRICE\]/", translateDate($Evenement->get("prix_collaboratrice")), $description);

			$description = preg_replace("/\[NOTE\]/", translateDate($Evenement->get("note")), $description);

			$description = preg_replace("/\[HEBERGEMENT_LIMITE\]/", translateDate($Evenement->get("hebergement_limite")), $description);
			$description = preg_replace("/\[HEBERGEMENT_NOTE\]/", translateDate($Evenement->get("hebergement_note")), $description);
			$description = preg_replace("/\[HEBERGEMENT_COORDONNEES\]/", translateDate($Evenement->get("hebergement_coordonnees")), $description);
			$description = preg_replace("/\[HEBERGEMENT_DETAILS\]/", translateDate($Evenement->get("hebergement_details")), $description);
			$description = preg_replace("/\[HEBERGEMENT_OCCUPATION_SIMPLE\]/", translateDate($Evenement->get("hebergement_occupation_simple")), $description);
			$description = preg_replace("/\[HEBERGEMENT_OCCUPATION_DOUBLE\]/", translateDate($Evenement->get("hebergement_occupation_double")), $description);


// [INSCRIPTIONS_NOTAIRES]

// [INSCRIPTIONS_COLLABORATRICES]

// [LISTE_ACTIVITES]

// [INSCRIPTIONS_ACTIVITES]






			$Evenement->set("description", $description);	
		}

		return $Evenement->getAll();

	}



	function validateFormValues($action) {
		global $DB, $EMAIL_FORMAT_PATTERN;
		$errorString = "";
		switch ($action) {
			case "add":
				if ($_SESSION['user_type'] != 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('type');\">Vous ne poss&eacute;ss&eacute;dez pas les droits requis pour cr&eacute;er cet &eacute;v&eacute;nement.</a>");
				}

				if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $_POST["date"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date');\">Veuillez indiquez une date valide.</a>");
				}

				if (!preg_match("/^(\d{4}-\d{2}-\d{2})?$/", $_POST["date_limite"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date_limite');\">Veuillez indiquez une date valide.</a>");
				}

				if (trim($_POST["actif"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('actif');\">Veuillez indiquez si l'&eacute;v&eacute;nement est actif ou non.</a>");
				}
				
				if (!preg_match("/^([\d\.\,]+)?$/", $_POST["prix_notaire"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('prix_notaire');\">Veuillez indiquez un prix valide.</a>");
				}

				if (!preg_match("/^([\d\.\,]+)?$/", $_POST["prix_collaboratrice"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('prix_collaboratrice');\">Veuillez indiquez un prix valide.</a>");
				}

				if (!preg_match("/^([\d\.\,]+)?$/", $_POST["hebergement_occupation_simple"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('hebergement_occupation_simple');\">Veuillez indiquez un prix valide.</a>");
				}

				if (!preg_match("/^([\d\.\,]+)?$/", $_POST["hebergement_occupation_double"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('hebergement_occupation_double');\">Veuillez indiquez un prix valide.</a>");
				}

				for ($i = 0; $i < 21; $i++) {
					if (!preg_match("/^([\d\.\,]+)?$/", $_POST["activite" . $i . "_prix"])) {
						$errorString .= _error("<a href=\"javascript: showErrorField('activite" . $i . "_prix');\">Veuillez indiquez un prix valide.</a>");
					}
				}

				break;

			case "modify":
				if ($_SESSION['user_type'] != 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('type');\">Vous ne poss&eacute;ss&eacute;dez pas les droits requis pour modifier cet &eacute;v&eacute;nement.</a>");
				}
				
				if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $_POST["date"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date');\">Veuillez indiquez une date valide.</a>");
				}

				if (!preg_match("/^(\d{4}-\d{2}-\d{2})?$/", $_POST["date_limite"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date_limite');\">Veuillez indiquez une date valide.</a>");
				}

				if (trim($_POST["actif"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('actif');\">Veuillez indiquez si l'&eacute;v&eacute;nement est actif ou non.</a>");
				}

				if (!preg_match("/^([\d\.\,]+)?$/", $_POST["prix_notaire"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('prix_notaire');\">Veuillez indiquez un prix valide.</a>");
				}

				if (!preg_match("/^([\d\.\,]+)?$/", $_POST["prix_collaboratrice"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('prix_collaboratrice');\">Veuillez indiquez un prix valide.</a>");
				}

				if (!preg_match("/^([\d\.\,]+)?$/", $_POST["hebergement_occupation_simple"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('hebergement_occupation_simple');\">Veuillez indiquez un prix valide.</a>");
				}

				if (!preg_match("/^([\d\.\,]+)?$/", $_POST["hebergement_occupation_double"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('hebergement_occupation_double');\">Veuillez indiquez un prix valide.</a>");
				}

				for ($i = 0; $i < 21; $i++) {
					if (!preg_match("/^([\d\.\,]+)?$/", $_POST["activite" . $i . "_prix"])) {
						$errorString .= _error("<a href=\"javascript: showErrorField('activite" . $i . "_prix');\">Veuillez indiquez un prix valide.</a>");
					}
				}

				break;
		}
		return $errorString;
	}

	function updateDBValues($action) {
		global $DB;
		$errorString = "";

		$parameters = addslashesToValues($_POST);

		switch ($action) {
			case "add":
				
				$Evenement = new Congres_event($DB, "", "");
				
				$Evenement->scanfields();
				$Evenement->insertmode();
				
				$Evenement->set("id", trim($_POST['id']));
				$Evenement->set("date", trim($_POST['date']));
				$Evenement->set("date_limite", trim($_POST['date_limite']));
				$Evenement->set("lieu", trim($_POST['lieu']));
				$Evenement->set("description", trim($_POST['description']));
				$Evenement->set("actif", trim($_POST['actif']));
				$Evenement->set("sujet1", trim($_POST['sujet1']));
				$Evenement->set("sujet2", trim($_POST['sujet2']));
				$Evenement->set("sujet3", trim($_POST['sujet3']));
				$Evenement->set("sujet4", trim($_POST['sujet4']));
				$Evenement->set("prix_notaire", trim($_POST['prix_notaire']));
				$Evenement->set("prix_collaboratrice", trim($_POST['prix_collaboratrice']));
				$Evenement->set("note", trim($_POST['note']));
				for ($i = 1; $i < 21; $i++) {
					$Evenement->set("activite" . $i, trim($_POST['activite' . $i]));
					$Evenement->set("activite" . $i . "_prix", trim($_POST['activite' . $i . '_prix']));
					$Evenement->set("activite" . $i . "_jour", trim($_POST['activite' . $i . '_jour']));
				}

				$Evenement->set("hebergement_limite", trim($_POST['hebergement_limite']));
				$Evenement->set("hebergement_note", trim($_POST['hebergement_note']));
				$Evenement->set("hebergement_coordonnees", trim($_POST['hebergement_coordonnees']));
				$Evenement->set("hebergement_details", trim($_POST['hebergement_details']));
				$Evenement->set("hebergement_occupation_simple", trim($_POST['hebergement_occupation_simple']));
				$Evenement->set("hebergement_occupation_double", trim($_POST['hebergement_occupation_double']));
				
				if (!$Evenement->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion.");
				}

				break;
			
			case "modify":
				
				$Evenement = new Congres_event($DB, "", "");
				
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_POST['id']);			
	
				$Evenement->set("id", trim($_POST['id']));
				$Evenement->set("date", trim($_POST['date']));
				$Evenement->set("date_limite", trim($_POST['date_limite']));
				$Evenement->set("lieu", trim($_POST['lieu']));		
				$Evenement->set("description", trim($_POST['description']));
				$Evenement->set("actif", trim($_POST['actif']));

				$Evenement->set("sujet1", trim($_POST['sujet1']));
				$Evenement->set("sujet2", trim($_POST['sujet2']));
				$Evenement->set("sujet3", trim($_POST['sujet3']));
				$Evenement->set("sujet4", trim($_POST['sujet4']));
				$Evenement->set("prix_notaire", trim($_POST['prix_notaire']));
				$Evenement->set("prix_collaboratrice", trim($_POST['prix_collaboratrice']));
				$Evenement->set("note", trim($_POST['note']));
				for ($i = 1; $i < 21; $i++) {
					$Evenement->set("activite" . $i, trim($_POST['activite' . $i]));
					$Evenement->set("activite" . $i . "_prix", trim($_POST['activite' . $i . '_prix']));
					$Evenement->set("activite" . $i . "_jour", trim($_POST['activite' . $i . '_jour']));
				}

				$Evenement->set("hebergement_limite", trim($_POST['hebergement_limite']));
				$Evenement->set("hebergement_note", trim($_POST['hebergement_note']));
				$Evenement->set("hebergement_coordonnees", trim($_POST['hebergement_coordonnees']));
				$Evenement->set("hebergement_details", trim($_POST['hebergement_details']));
				$Evenement->set("hebergement_occupation_simple", trim($_POST['hebergement_occupation_simple']));
				$Evenement->set("hebergement_occupation_double", trim($_POST['hebergement_occupation_double']));

				if (!$Evenement->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de la modification.");
				}

				break;				
		}
		return $errorString;
	}

	function addslashesToValues($parameters = "") {
		if (is_array($parameters) && count($parameters) > 0) {
			foreach ($parameters as $field => $value) {
				if (is_array($value) && count($value) > 0) {
					foreach ($value as $field2 => $value2) {
						if (is_array($value2) && count($value2) > 0) {
							foreach ($value2 as $field3 => $value3) {
								$parameters[$field][$field2][$field3] = addslashes($value3);
							}
						} else {
							$parameters[$field][$field2] = addslashes($value2);
						}
					}
				} else {
					$parameters[$field] = addslashes($value);
				}
			}
		} else {
			$parameters = addslashes($parameters);
		}
		return $parameters;
	}

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "Ajout d'un congr&egrave;s";
				break;	
			case "modify":
				$title = "Modification d'un congr&egrave;s";
				break;
			case "delete":
				$title = "Suppression d'un congr&egrave;s";
				break;
			case "read":
				$title = "Consultation d'un congr&egrave;s";
				break;
		}
		return $title;
	}

	// Fonction qui retourne le html pour le menu d'actions des evenements
	function menuEvenementsString() { 
		$html =
			"<h1>&Eacute;v&eacute;nements - Congr&egrave;s</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"congres.php?action=add\">Ajouter un congr&egrave;s</a></li>\n".
			"		<li><a href=\"congres.php?action=modify\">Modifier congr&egrave;s</a></li>\n".
			//"		<li><a href=\"congres.php?action=delete\">Supprimer un congr&egrave;s</a></li>\n".
			"		<li><a href=\"congres.php?action=read\">Consulter un congr&egrave;s</a></li>\n".
			"	</ul>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le titre du sous-menu
	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "modify":
				$html .= "<h2>Modifier un congr&egrave;s</h2>\n";
				break;
			case "delete":
				$html .= "<h2>Supprimer un congr&egrave;s</h2>\n";
				break;
			case "read":
				$html .= "<h2>Consulter un congr&egrave;s</h2>\n";
				break;
		}
		return $html;
	}

	function getEvenementsOptionsString($id=""){
		global $DB;
		$html = "";

		$DB->query(
			"SELECT ca.id, ca.date, CONCAT_WS(' ', 'Congres du', ca.date) as nom ".
			"FROM `congres_event` ca ".
			//"WHERE ca.actif = '1' ".
			"ORDER BY ca.date DESC"
		);

		while($DB->next_record()) {
			$selected = "";		
			if ($id != "" && $id == $DB->getField("id")){
				$selected = "selected=\"selected\" ";		
			}
			$html .= "<option ".$selected."value=\"" . $DB->getField("id") . "\">" . $DB->getField("nom") . " (" . $DB->getField("date") . ")</option>\n";
		}

		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à modifier
	function modifyEvenementsString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"congres.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"		<input type=\"hidden\" name=\"begin\" value=\"1\" />\n".
			"		<select name=\"id\">\n".
			"			<option value=\"0\">- Choisir un congres -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à consulter
	function readEvenementsString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"congres.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"		<input type=\"hidden\" name=\"begin\" value=\"1\" />\n".
			"		<select name=\"id\">\n".
			"			<option value=\"0\">- Choisir un congres -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à supprimer
	function deleteEvenementsString() {
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"congres.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"		<input type=\"hidden\" name=\"begin\" value=\"1\" />\n".
			"		<select name=\"id\">\n".
			"			<option value=\"0\">- Choisir un congres -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Supprimer\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui supprime une formation de la DB
	function deleteEvenementsFromDB($id) {
		global $DB;
		
		//$Evenement->set('actif', '0');		

	}

?>