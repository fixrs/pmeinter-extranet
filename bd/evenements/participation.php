<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Evenement");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	
	
	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$successString = "";
	$errorString = "";
	$formString = "";

	// ÉTAPE #1
	// Traitement des GET provenant de la sélection d'une action dans le PREMIER menu $menuString
	switch ($_GET['action']) {
		case "list":
			$formString .= javascriptString().listInscriptionsString(getorpost('action'));
			break;

		case "present"://setPresence($key, $action, $value)
			if (isset($_GET['mod_key']) && $_GET['mod_key']!="" && isset($_GET['change_to']) && $_GET['change_to']!=""){
				setPresence($_GET['mod_key'], $_GET['action'], $_GET['change_to']);
				$successString = _success("<br/>Modification compl&eacute;t&eacute;e avec succ&egrave;s");
			}
			$formString .= javascriptString().listInscriptionsString(getorpost('action'));
			break;

		case "non_present":
			if (isset($_GET['mod_key']) && $_GET['mod_key']!="" && isset($_GET['change_to']) && $_GET['change_to']!=""){
				setPresence($_GET['mod_key'], $_GET['action'], $_GET['change_to']);
				$successString = _success("<br/>Modification compl&eacute;t&eacute;e avec succ&egrave;s");
			}
			$formString .= javascriptString().listInscriptionsString(getorpost('action'));
			break;
		case "not":
			if (isset($_GET['mod_key']) && $_GET['mod_key']!="" && isset($_GET['change_to']) && $_GET['change_to']!=""){
				setPresence($_GET['mod_key'], $_GET['action'], $_GET['change_to']);
				$successString = _success("<br/>Modification compl&eacute;t&eacute;e avec succ&egrave;s");
			}
			$formString .= javascriptString().listInscriptionsString(getorpost('action'));
			break;
		case "del_ins":
			if (isset($_GET['ins_key']) && $_GET['ins_key']!="" && $_GET['ins_key']!="0" && is_numeric($_GET['ins_key'])){
				deleteInscriptionsFromDB($_GET['ins_key']);
				$evenements_key = "";
				if (isset($_GET['evenements_key'])){
					$evenements_key =$_GET['evenements_key'];
				}
				header('Location: participation.php?action=del_ins&ins_key_yes=&evenements_key='.$evenements_key);
				exit;
			}
			else if(isset($_GET['ins_key_yes']) && $_GET['ins_key_yes']==""){
				$successString = _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s");
				$formString .= javascriptString().getResultInscriptionsString('del_ins');
			}
			else {
				$formString .= javascriptString().getResultInscriptionsString('del_ins');
				
			}
			break;
		case "add_ins":
			$formString .= javascriptString().addInscriptionsString();
			break;
		case "mod_ins":
			$formString .= javascriptString().getResultInscriptionsString('mod_ins');
			break;
		case "read_ins":
			$formString .= javascriptString().getResultInscriptionsString('read_ins');
			break;

	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");	
	$Skin->assign("page", "evenements");
	$Skin->assign("title", pageTitleString(getorpost('action')));
	$Skin->assign("menu_title", menuInscriptionsString(getorpost('action')));
	$Skin->assign("menu", menuTitleString(getorpost('action')));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("evenements.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "list":
				$title = "Liste d'inscriptions &agrave; un &eacute;v&eacute;nement";
				break;
			case "present":
				$title = "Liste des participants pr&eacute;sents aux &eacute;v&eacute;nements";
				break;
			case "non_present":
				$title = "Liste des participants absents aux &eacute;v&eacute;nements";
				break;
			case "not":
				$title = "Liste des participants dont la pr&eacute;sence n'est pas connue";
				break;
			case "del_ins":
				$title = "Suppression d'inscriptions &agrave; un &eacute;v&eacute;nement";
				break;
			case "mod_ins":
				$title = "Modification d'inscriptions &agrave; un &eacute;v&eacute;nement";
				break;
			case "read_ins":
				$title = "Consultation d'inscriptions &agrave; un &eacute;v&eacute;nement";
				break;
			default:
				$title = "Liste d'inscriptions &agrave; un &eacute;v&eacute;nement";
		}
		return $title;
	}

	// Fonction qui retourne le html pour le menu d'actions des formations
	function menuInscriptionsString($action) { 
		$key = "";
		if (isset($_GET['evenements_key'])){
			$key = $_GET['evenements_key'];	
		}
		switch ($action) {
			case "list":
				$html =
					"<p><span class=\"answer\">Listes de : </span><a href=\"participation.php?action=list&evenements_key=".$key."\">Inscriptions</a> | <a href=\"participation.php?action=present&evenements_key=".$key."\">Pr&eacute;sences</a> | <a href=\"participation.php?action=non_present&evenements_key=".$key."\">Absences</a> | <a href=\"participation.php?action=not&evenements_key=".$key."\">Pr&eacute;sence inconnue</a></p>\n";
				break;
			case "present":
				$html =
					"<p><span class=\"answer\">Listes de : </span><a href=\"participation.php?action=list&evenements_key=".$key."\">Inscriptions</a> | <a href=\"participation.php?action=present&evenements_key=".$key."\">Pr&eacute;sences</a> | <a href=\"participation.php?action=non_present&evenements_key=".$key."\">Absences</a> | <a href=\"participation.php?action=not&evenements_key=".$key."\">Pr&eacute;sence inconnue</a></p>\n";
				break;
			case "non_present":
				$html =
					"<p><span class=\"answer\">Listes de : </span><a href=\"participation.php?action=list&evenements_key=".$key."\">Inscriptions</a> | <a href=\"participation.php?action=present&evenements_key=".$key."\">Pr&eacute;sences</a> | <a href=\"participation.php?action=non_present&evenements_key=".$key."\">Absences</a> | <a href=\"participation.php?action=not&evenements_key=".$key."\">Pr&eacute;sence inconnue</a></p>\n";
				break;
			case "not":
				$html =
					"<p><span class=\"answer\">Listes de : </span><a href=\"participation.php?action=list&evenements_key=".$key."\">Inscriptions</a> | <a href=\"participation.php?action=present&evenements_key=".$key."\">Pr&eacute;sences</a> | <a href=\"participation.php?action=non_present&evenements_key=".$key."\">Absences</a> | <a href=\"participation.php?action=not&evenements_key=".$key."\">Pr&eacute;sence inconnue</a></p>\n";
				break;
			case "del_ins":
				$html =
					"<p><span class=\"answer\">Inscriptions : </span><a href=\"index.php?action=add_ins#part\">Ajouter</a> | <a href=\"index.php?action=mod_ins#part\">Modifier</a> | <a href=\"index.php?action=del_ins#part\">Supprimer</a> | <a href=\"index.php?action=read_ins#part\">Consulter</a></p>\n";
				break;
			case "mod_ins":
				$html =
					"<p><span class=\"answer\">Inscriptions : </span><a href=\"index.php?action=add_ins#part\">Ajouter</a> | <a href=\"index.php?action=mod_ins#part\">Modifier</a> | <a href=\"index.php?action=del_ins#part\">Supprimer</a> | <a href=\"index.php?action=read_ins#part\">Consulter</a></p>\n";
				break;
			case "read_ins":
				$html =
					"<p><span class=\"answer\">Inscriptions : </span><a href=\"index.php?action=add_ins#part\">Ajouter</a> | <a href=\"index.php?action=mod_ins#part\">Modifier</a> | <a href=\"index.php?action=del_ins#part\">Supprimer</a> | <a href=\"index.php?action=read_ins#part\">Consulter</a></p>\n";
				break;
			default:
				$html = "Liste d'inscriptions &agrave; une formation";
		}
		return $html;
	}

	// Fonction qui retourne le html pour le menu d'actions des formations
	function menuListesString($action) { 
		global $DB;
		$key = "";
		$blnIsSet = false;
		if (isset($_GET['evenements_key']) && $_GET['evenements_key']!=""){
			$key = $_GET['evenements_key'];
			$blnIsSet = true;
		}		
		$html =
			"<div class=\"form\">\n".
			"	<form id=\"participations\" name=\"participations\" action=\"participation.php?action=".$action."\" method=\"get\">\n" .
			"		<p>Pour avoir la liste pour un &eacute;v&eacute;nement seulement, veuillez le choisir ci-dessous.</p>".
			"		<input type=\"hidden\" name=\"action\" value=\"".$action."\" />\n".
			"		<select name=\"evenements_key\" onchange=\"javascript:document.participations.submit();\">\n";

		if ($blnIsSet){
			$html .=
				"			<option value=\"0\" class=\"default\">- Tous les &eacute;v&eacute;nements -</option>\n";
		}
		else {
			$html .=
				"			<option value=\"-1\" class=\"default\">- Veuillez choisir un &eacute;v&eacute;nement ou tous les &eacute;v&eacute;nements -</option>\n".
				"			<option value=\"0\">Tous les &eacute;v&eacute;nements</option>\n";
		}
		$html .= getEvenementsOptionsString($key).
			"		</select>\n".
			"	</form>\n".
			"</div><br/>\n";
		return $html;
	}



	// Fonction qui retourne le html pour le titre du sous-menu
	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "list":
				$html = "<h2>Consulter la liste d'inscriptions aux &eacute;v&eacute;nements</h2>";
				break;
			case "present":
				$html = "<h2>Consulter la liste des pr&eacute;sences aux &eacute;v&eacute;nements</h2>";
				break;
			case "non_present":
				$html = "<h2>Consulter la liste des absences aux &eacute;v&eacute;nements</h2>";
				break;
			case "not":
				$html = "<h2>Consulter la liste des participants dont la pr&eacute;sence n'est pas connue</h2>";
				break;
			case "del_ins":
				$html = "<h2>Supprimer une inscription &agrave; un &eacute;v&eacute;nement</h2>\n";
				break;
			case "mod_ins":
				$html = "<h2>Modifier une inscription &agrave; un &eacute;v&eacute;nement</h2>\n";
				break;
			case "read_ins":
				$html = "<h2>Consulter une inscription &agrave; un &eacute;v&eacute;nement</h2>\n";
				break;
		}
		return $html;
	}

	function getEvenementsOptionsString($key=""){
		global $DB;
		$html = "";
		$DB->query(
			"SELECT `key`, `nom`, `date_debut` ".
			"FROM `evenements` ".
			"WHERE `actif`='1' ".
			"ORDER BY `date_debut` DESC"
		);
		while($DB->next_record()) {
			$selected = "";		
			if ($key!="" && $key!="0" && $key==$DB->getField("key")){
				$selected = "selected=\"selected\" ";		
			}
				$html .= "<option ".$selected."value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . " (" . $DB->getField("date_debut") . ")</option>\n";
		}

		return $html;
	}

	function getEvenementsKeyMenuString($action){
		$key = "";
		$blnIsSet = false;
		if (isset($_GET['evenements_key']) && $_GET['evenements_key']!=""){
			$key = $_GET['evenements_key'];
			$blnIsSet = true;
		}		
		$html =
			"<div class=\"form\">\n".
			"	<form id=\"participations\" name=\"participations\" action=\"participation.php?action=".$action."\" method=\"get\">\n" .
//			"		<p>Pour avoir la liste pour un &eacute;v&eacute;nement seulement, veuillez le choisir ci-dessous.</p>".
			"		<input type=\"hidden\" name=\"action\" value=\"".$action."\" />\n".
			"		<select name=\"evenements_key\" onchange=\"javascript:document.participations.submit();\">\n";
		if ($blnIsSet){
			$html .=
				"			<option value=\"0\" class=\"default\">- Tous les &eacute;v&eacute;nements  -</option>\n";
		}
		else {
			$html .=
				"			<option value=\"-1\" class=\"default\">- Veuillez choisir un &eacute;v&eacute;nement ou tous les &eacute;v&eacute;nements -</option>\n".
				"			<option value=\"0\">Tous les &eacute;v&eacute;nements</option>\n";
		}
		$html .= getEvenementsOptionsString($key).
			"		</select>\n".
			"		<br/><br/>\n".
			"	</form>\n".
			"</div>\n";

		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à supprimer
	function getResultInscriptionsString($action) {
		global $DB;
		$orderBy = "`evenements_participation`.`nom` ";
		$way = "ASC";
		$evenements_key = "";
		$sql_event_key = "";

		$html = getEvenementsKeyMenuString($action);
		
		if (isset($_GET['evenements_key'])){
			$sql_event_key = "";
			if ($_GET['evenements_key']!="" && $_GET['evenements_key']!="0"){
				$sql_event_key = "AND `evenements`.`key`='".$_GET['evenements_key']. "' ";
			}
		
			if (isset($_GET['order_by']) && $_GET['order_by']!=""){
				switch($_GET['order_by']){
					case "nom":
						$orderBy = "`evenements_participation`.`nom` ";
						$way = "ASC";
					break;
					case "etude":
						$orderBy = "`etudes`.`nom` ";
						$way = "ASC";
					break;
					case "evenement":
						$orderBy = "`evenements`.`nom` ";
						$way = "ASC";
					break;
					case "date":
						$orderBy = "`evenements`.`date_debut` ";
						$way = "DESC";
					break;
					default:
						$orderBy = "`evenements_participation`.`nom` ";
						$way = "ASC";
					break;
				}
			}
			
			$DB->query(
				"SELECT `evenements`.`nom` AS `evenements_nom`, `evenements`.`key` AS `evenements_key`, `evenements`.`date_debut`, `evenements_participation`.`nom`, " .
				"`evenements_participation`.`prenom`, `evenements_participation`.`key`, " .
				"`etudes`.`nom` AS `etudes_nom` ".
				"FROM `evenements`, `evenements_participation`, `etudes` ".
				"WHERE `evenements`.`key`=`evenements_participation`.`evenements_key` " .
				"AND `evenements_participation`.`etudes_key`=`etudes`.`key` " .
				$sql_event_key.
				"GROUP BY `evenements_participation`.`key` ".
				"ORDER BY ". $orderBy. $way
			);
			$nb = $DB->getNumRows();
	
			if ($nb>0){
				$html .=
					"<table class=\"resultsTable\">\n" .
					"	<tr>\n".
					"		<th><a href=\"participation.php?action=".$action."&order_by=nom&evenements_key=".$_GET['evenements_key']."#part\">Nom, pr&eacute;nom du participant</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=etude&evenements_key=".$_GET['evenements_key']."#part\">Nom de l'&eacute;tude</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=evenement&evenements_key=".$_GET['evenements_key']."#part\">Nom de l'&eacute;v&eacute;nement</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=date&evenements_key=".$_GET['evenements_key']."#part\">Date de l'&eacute;v&eacute;nement</a></th>\n" .
					"		<th>&nbsp;</th>\n".
					"	</tr>\n";
			}
			$compteur = 0;
			
			while($DB->next_record()) {
				$key = $DB->getField("key");
				$color = "";
				if ($compteur%2==0) {
					$color = " class=\"altTr\"";			
				}
				$html .=
				"    <tr". $color .">\n" .
				"		<td>".$DB->getField("nom"). ", " .$DB->getField("prenom") ."</td>\n " .
				"		<td>". $DB->getField("etudes_nom")."</td>\n" .
				"		<td>". $DB->getField("evenements_nom")."</td>\n" .
				"		<td>". $DB->getField("date_debut")."</td>\n" ;
	
				if ($action=="del_ins"){
					$html .=
						"		<td><a href=\"participation.php?action=".$action."&ins_key=".$key."&evenements_key=".$_GET['evenements_key']."#part\" title=\"Pour supprimer\" onclick=\"return areUSureUWantToDelete('".$DB->getField("nom"). ", " .$DB->getField("prenom") ."', ".$DB->getField("key") .");\">Supprimer</a></td>\n";
				}
				else if ($action=='read_ins'){
					$html .=
						"		<td><a href=\"inscriptions.php?action=read&ins_key=".$key."\" title=\"Pour visualiser\">Visualiser</a></td>\n";
				}
				else {
					$html .=
						"		<td><a href=\"inscriptions.php?action=modify&begin=1&ins_key=".$key."\" title=\"Pour modifier\">Modifier</a></td>\n";
				}
				$html .=
				"	</tr>\n";
				$compteur++;
			}
	
			if ($nb==0) {
				$html .= "<p>Aucune inscription.</p>";
			}
			else if ($nb>0) {
				$html .=
					"</table>";			
			}
		}
		
		return $html;

	}


	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à supprimer
	function listInscriptionsString($action) {
		global $DB;
		$orderBy = "`evenements_participation`.`nom` ";
		$way = "ASC";
		$event_key = "";
		$sqlevent_key = "";
		$sqlaction = "";
		$html = menuListesString($action); 
			
		if (isset($_GET['evenements_key']) && $_GET['evenements_key']!=""){

			if ($_GET['evenements_key']!="0"){
				$event_key = $_GET['evenements_key'];
				$sqlevent_key = "AND `evenements`.`key`= '" .$event_key. "' ";
			}
		
			if (isset($_GET['order_by']) && $_GET['order_by']!=""){
				switch($_GET['order_by']){
					case "nom":
						$orderBy = "`evenements_participation`.`nom` ";
						$way = "ASC";
					break;
					case "etude":
						$orderBy = "`etudes`.`nom` ";
						$way = "ASC";
					break;
					case "evenement":
						$orderBy = "`evenements`.`nom` ";
						$way = "ASC";
					break;
					case "date":
						$orderBy = "`evenements`.`date_debut` ";
						$way = "DESC";
					break;
					case "date_ins":
						$orderBy = "`evenements_participation`.`date_inscription` ";
						$way = "DESC";
					break;
					case "present":
						$orderBy = "`evenements_participation`.`present` ";
						$way = "DESC";
					break;
					default:
						$orderBy = "`evenements_participation`.`nom` ";
						$way = "ASC";
					break;
				}
			}
			if ($action=="non_present"){
				$sqlaction = "AND `evenements_participation`.`present`='0' ";// .
							//"AND `evenements`.`date_debut` <= CURRENT_DATE()";
				
			}
			else if ($action=="present"){
				$sqlaction = "AND `evenements_participation`.`present`='1' ";// .
							//"AND `evenements`.`date_debut` <= CURRENT_DATE()";
			}
			else if ($action=="not"){
				$sqlaction = "AND `evenements_participation`.`present` IS NULL ";
				
			}
			
			$query = 
				"SELECT `evenements`.`nom` AS `evenements_nom`, `evenements`.`key` AS `evenements_key`, `evenements`.`date_debut`, `evenements_participation`.`nom`, " .
				"`evenements_participation`.`prenom`, `evenements_participation`.`key`, " .
				"`evenements_participation`.`telephone`, `evenements_participation`.`present`, `evenements_participation`.`date_inscription`, " .
				"`etudes`.`nom` AS `etudes_nom` ".
				"FROM `evenements`, `evenements_participation`, `etudes` ".
				"WHERE `evenements`.`key`=`evenements_participation`.`evenements_key` " .
				"AND `evenements_participation`.`etudes_key`=`etudes`.`key` ".
				$sqlevent_key.
				$sqlaction.
				"GROUP BY `evenements_participation`.`key` ".
				"ORDER BY ". $orderBy. $way;
			
			$DB->query($query);
	
			$nb = $DB->getNumRows();
	
			if ($nb>0){
				$html .=
					"<table class=\"resultsTable\">\n" .
					"	<tr>\n".
					"		<th><a href=\"participation.php?action=".$action."&order_by=nom&evenements_key=".$_GET['evenements_key']."\">Nom, pr&eacute;nom du participant</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=etude&evenements_key=".$_GET['evenements_key']."\">&Eacute;tude</a></th>\n" .
					"		<th><a href=\"javascript:;\">T&eacute;l&eacute;phone</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=date_ins&evenements_key=".$_GET['evenements_key']."\">Date d'inscription</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=evenement&evenements_key=".$_GET['evenements_key']."\">&Eacute;v&eacute;nement</a> <a href=\"participation.php?action=".$action."&order_by=date&evenements_key=".$_GET['evenements_key']."\">(date)</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=present&evenements_key=".$_GET['evenements_key']."\">Pr&eacute;sence</a></th>\n" .
					"		<th>&nbsp;</th>\n".
					"	</tr>\n";
			}
			$compteur = 0;
			
			while($DB->next_record()) {
				$key = $DB->getField("key");
				$color = "";
				$date_debut = $DB->getField("date_debut");
				if ($compteur%2==0) {
					$color = " class=\"altTr\"";			
				}
				$html .=
				"    <tr". $color .">\n" .
				"		<td><a href=\"inscriptions.php?action=read&ins_key=".$key."\" title=\"Pour visualiser\">".$DB->getField("nom"). ", " .$DB->getField("prenom") ."</a></td>\n " .
				"		<td>". $DB->getField("etudes_nom")."</td>\n" .
				"		<td>". $DB->getField("telephone")."</td>\n" .
				"		<td>". $DB->getField("date_inscription")."</td>\n" .
				"		<td>". $DB->getField("evenements_nom")." (".$date_debut.")</td>\n";
				if ($action=='present' || $action=='non_present' || $action=='not' ){
					
					$presence = $DB->getField("present");
					$reponse = "";
					if (verifyDateEqualsOrOlderThanToday($date_debut)){
						// l'événement est déjà commencé
						if ($presence=="1"){
							$reponse =	"<b>Oui</b> | <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=0&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme absent\">Non</a>";
						}
						else if ($presence=="0"){
							$reponse =	"<a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=1&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme pr&eacute;sent\">Oui</a> |" .
										" <b>Non</b>";
						}
						else {
							$reponse =	"<a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=1&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme pr&eacute;sent\">Oui</a> |" .
										" <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=0&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme absent\">Non</a>\n";
						}
						
					}
					else {
						// l'événement n'est pas encore commencé
						if ($presence=="1"){
							$reponse =	"<b>Oui</b> | <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=0&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme absent\" " .
										"onclick=\"return window.confirm('Cet &eacute;v&eacute;nement ne s\'est pas encore d&eacute;roul&eacute;. &Ecirc;tes-vous s&ucirc;r de vouloir marquer cette personne comme absente?');\">Non</a> |" .
										" <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=NULL&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme inconnu\">?</a>";
						}
						else if ($presence=="0"){
							$reponse =	"<a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=1&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme pr&eacute;sent\" " .
										"onclick=\"return window.confirm('Cet &eacute;v&eacute;nement ne s\'est pas encore d&eacute;roul&eacute;. &Ecirc;tes-vous s&ucirc;r de vouloir marquer cette personne comme pr&eacute;sente?');\">Oui</a> |" .
										" <b>Non</b> |" .
										" <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=NULL&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme inconnu\">?</a>";
						}
						else {
							$reponse =	"<a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=1&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme pr&eacute;sent\" " .
										"onclick=\"return window.confirm('Cet &eacute;v&eacute;nement ne s\'est pas encore d&eacute;roul&eacute;. &Ecirc;tes-vous s&ucirc;r de vouloir marquer cette personne comme pr&eacute;sente?');\">Oui</a> |" .
										" <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=0&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme absent\" " .
										"onclick=\"return window.confirm('Cet &eacute;v&eacute;nement ne s\'est pas encore d&eacute;roul&eacute;. &Ecirc;tes-vous s&ucirc;r de vouloir marquer cette personne comme absente?');\">Non</a>\n";
						}
					}
					$html .=
					"		<td>". $reponse ."</td>\n" ;
				}
				else {
					
					$presence = $DB->getField("present");
					$reponse = "";
					if ($presence=="1"){
						$reponse = "Oui";
					}
					else if ($presence=="0"){
						$reponse = "Non";
					}
					else {
						$reponse = "Inconnue";
					}
					$html .=
					"		<td>". $reponse ."</td>\n" ;
				}
				$html .=
				"	</tr>\n";
				$compteur++;
			}
	
			if ($nb==0) {
				$html .= "<p>Aucune inscription.</p>";
			}
			else if ($nb>0) {
				$html .=
					"</table>";			
			}
		}
		$html .= "	\n";
		
		return $html;

	}

	function verifyDateEqualsOrOlderThanToday($date){
		$blnOk = false;
		$arrDate = explode( "-", $date);
		if (date("Y-m-d", mktime(0, 0, 0, $arrDate[1], $arrDate[2], $arrDate[0]))<=date("Y-m-d")){
			
			$blnOk = true;
		}
		return $blnOk;
	}
	
	function setPresence($key, $action, $value){
		global $DB;
		$Participation = new Evenement($DB, "", 1);
		$Participation->setTable("evenements_participation");				
		$Participation->scanfields();
		$Participation->setQKey("key", $key);
		if( $value == "NULL" ) {
			$Participation->set("present", $value, 1 );
		}
		else {
			$Participation->set("present", $value);
		}
		
	}
	
		// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à supprimer
	function deleteInscriptionsFromDB($participation_key) {
		global $DB;
		$html = "";
		
		$DB->query(
			"DELETE FROM `evenements_participation` " .
			"WHERE `evenements_participation`.`key`= '".$participation_key."'"
		);

		return $html;

	}

	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function javascriptString() { 
		$html =
			"<script type=\"text/javascript\" language=\"javascript\">\n" .
			"	String.prototype.trim = function(){ \n" .
			"		return this.replace(/^\s+|\s+$/g,'');\n" .
			"		}\n".
			
			"	function areUSureUWantToDelete(strName, key) {\n".
			"		var answer = confirm('Vous êtes sure de vouloir supprimer \\n'+ strName);\n".
			"		if (answer) {\n".
			"			return true;\n".
			"		}\n".
			"		else {\n".
			"			return false;\n".
			"		} \n" .
			"	}\n".
			"</script>\n" .
			"\n";
		return $html;
	}
	
	
?>
