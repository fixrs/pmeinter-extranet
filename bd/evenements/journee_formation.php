<?php

	require "../conf/conf.inc.php";

	session_start();

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	/*if ($_GET["e"] != "" && $_GET["s"] != "" && $_SESSION["user_key"] != "") {

		$DB->query(
			"SELECT * ".
			"FROM directions_de_travail_event_assists ".
			"WHERE directions_de_travail_event_id = '" . $_GET["e"] . "' ".
			"AND utilisateurs_key = '" . $_SESSION["user_key"] . "'"
		);

		$assist_key = "";

		if ($DB->getNumRows() > 0) {
			$DB->query(
				"UPDATE `directions_de_travail_event_assists` ".
				"SET `status` = '" . $_GET["s"] . "' ".
				"WHERE `directions_de_travail_event_id` = '" . $_GET["e"] . "' ".
				"AND `utilisateurs_key` = '" . $_SESSION["user_key"] . "'"
			);
		} else {
			$DB->query(
				"INSERT INTO `directions_de_travail_event_assists` ".
				"(`status`, `directions_de_travail_event_id`, `utilisateurs_key`) VALUES (".
				"'" . $_GET["s"] . "', ".
				"'" . $_GET["e"] . "', ".
				"'" . $_SESSION["user_key"] . "')"
			);
		}

		if ($_GET["s"] == 1) {
			echo 
				//"	<li><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=1\">Je serai finalement pr&eacute;sent(e)</a></li>\n".
				"	<li class=\"present\"><a href=\"/evenements/journee_formation.php?e=" . $DB->getField("id") . "&s=0\">Je ne pourrai finalement pas y assister</a></li>\n";
		} else {
			echo 
				"	<li class=\"absent\"><a href=\"/evenements/journee_formation.php?e=" . $DB->getField("id") . "&s=1\">Je serai finalement pr&eacute;sent(e)</a></li>\n";
				//"	<li><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=0\">Je ne pourrai finalement pas y assister</a></li>\n";
		}

		$DB->close();

		exit;
	}*/
		

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Journee_Formation_event");
	





	// ------------------
	// LE TRAITEMENT HTML
	// ------------------
	
	$formString = "";
	$successString = "";
	$errorString = "";
	$titreString = "";
	$action = getorpost('action');
	$id = getorpost('id');
	$begin = getorpost("begin");

	if (count($_POST) == 0) {
		switch ($action) {
			case "add":
				$formString = "<h2>Ajouter une formation</h2>\n";
				importForm("journee_formation");
				$FormObject = new Form("", "", getForm($action));
				$formString .= $FormObject->returnForm();
				break;

			case "modify":
				$formString = modifyEvenementsString();
				break;
			case "delete":
				$formString = deleteEvenementsString();
				break;
			case "read":
				$formString = readEvenementsString();
				break;
		}
	} else {
		switch ($action) {
			case "add" :
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "evenements/journee_formation.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("journee_formation");
					$FormObject = new Form("", $_POST, getForm($action));
					$formString = $FormObject->returnFilledForm();
				}
				break;

			case "modify" :
				$titreString = "<h2>Modifier une formation</h2>\n";

				if ($begin) {
					importForm("journee_formation");
					$FormObject = new Form("", "", getForm($action));
					$FormObject->setValue("id", $id);			
					$FormObject->setValue("action", $action);
					$FormObject->setParameters(getAllFieldsEvenement($id));
					$formString = $FormObject->returnFilledForm();

				} else {
					$errorString = validateFormValues($action);
					if ($errorString == "") {
						$errorString = updateDbValues($action);
						if ($errorString == "") {
							$successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
							$formString = "<a href=\"" . $BASEURL . "evenements/journee_formation.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
						}
					} else {
						importForm("journee_formation");
						$FormObject = new Form("", $_POST, getForm($action));
						$FormObject->setValue("id", $id);
						$FormObject->setValue("action", $action);
						$FormObject->setParameters($_POST);
						$formString = $FormObject->returnFilledForm();
					}
				}
				break;

			case "read" :
				$titreString = "<h2>Consulter une formation</h2>\n";
				importForm("journee_formation");
				$FormObject = new Form("", $_POST, getForm($action));
				$parameters = getAllFieldsEvenement($id, "read");
				$FormObject->setParameters($parameters);
				$FormObject->setAllHidden();
				$formString = $FormObject->returnFilledForm();			
				break;
		}
	}



	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");	
	$Skin->assign("page", "journee_formation");
	$Skin->assign("title", pageTitleString(getorpost('action')));
	$Skin->assign("menu", menuEvenementsString());
	$Skin->assign("menu_title", menuTitleString(getorpost('action')));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("evenements.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------



	function getAllFieldsEvenement($id, $read=""){
		global $DB;

		$Evenement = new Journee_Formation_event($DB, "", 0);
		$Evenement->scanfields();
		$Evenement->setQKey("id", $id);
		$Evenement->loadAll();
		
		if ($read=="read"){
			$description = $Evenement->get("description");
			
			$description = preg_replace("/\[LIEU\]/", $Evenement->get("lieu"), $description);
			$description = preg_replace("/\[DATE\]/", translateDate($Evenement->get("date")), $description);
			$description = preg_replace("/\[COUT\]/", $Evenement->get("cout"), $description);
			$description = preg_replace("/\[SUJET\]/", $Evenement->get("sujet"), $description);
			$description = preg_replace("/\[DEPARTEMENT\]/", $Evenement->get("departement"), $description);
			$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($Evenement->get("date_limite")), $description);
			$description = preg_replace("/\[NOTE\]/", $Evenement->get("note"), $description);

			$Evenement->set("description", $description);	
		}

		return $Evenement->getAll();

	}




	function validateFormValues($action) {
		global $DB, $EMAIL_FORMAT_PATTERN;
		$errorString = "";
		switch ($action) {
			case "add":
				if ($_SESSION['user_type'] != 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('type');\">Vous ne poss&eacute;ss&eacute;dez pas les droits requis pour cr&eacute;er cet &eacute;v&eacute;nement.</a>");
				}

				if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $_POST["date"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date');\">Veuillez indiquez une date valide.</a>");
				}

				if (!preg_match("/^(\d{4}-\d{2}-\d{2})?$/", $_POST["date_limite"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date_limite');\">Veuillez indiquez une date valide.</a>");
				}

				if (trim($_POST["actif"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('actif');\">Veuillez indiquez si l'&eacute;v&eacute;nement est actif ou non.</a>");
				}
				
				break;

			case "modify":
				if ($_SESSION['user_type'] != 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('type');\">Vous ne poss&eacute;ss&eacute;dez pas les droits requis pour modifier cet &eacute;v&eacute;nement.</a>");
				}
				
				if (!preg_match("/^\d{4}-\d{2}-\d{2}$/", $_POST["date"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date');\">Veuillez indiquez une date valide.</a>");
				}

				if (!preg_match("/^(\d{4}-\d{2}-\d{2})?$/", $_POST["date_limite"])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('date_limite');\">Veuillez indiquez une date valide.</a>");
				}

				if (trim($_POST["actif"]) == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('actif');\">Veuillez indiquez si l'&eacute;v&eacute;nement est actif ou non.</a>");
				}

				break;
		}
		return $errorString;
	}

	function updateDBValues($action) {
		global $DB;
		$errorString = "";

		$parameters = addslashesToValues($_POST);

		switch ($action) {
			case "add":
				
				$Evenement = new Journee_Formation_event($DB, "", "");
				
				$Evenement->scanfields();
				$Evenement->insertmode();
				
				$Evenement->set("id", trim($_POST['id']));
				$Evenement->set("date", trim($_POST['date']));
				$Evenement->set("date_limite", trim($_POST['date_limite']));
				$Evenement->set("lieu", trim($_POST['lieu']));		
				$Evenement->set("departement", trim($_POST['departement']));
				$Evenement->set("sujet", trim($_POST['sujet']));
				$Evenement->set("note", trim($_POST['note']));
				$Evenement->set("cout", trim($_POST['cout']));
				$Evenement->set("description", trim($_POST['description']));
				$Evenement->set("actif", trim($_POST['actif']));
				
				if (!$Evenement->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion.");
				}

				break;
			
			case "modify":
				
				$Evenement = new Journee_Formation_event($DB, "", "");
				
				$Evenement->scanfields();
				$Evenement->setQKey("id", $_POST['id']);			
	
				$Evenement->set("id", trim($_POST['id']));
				$Evenement->set("date", trim($_POST['date']));
				$Evenement->set("date_limite", trim($_POST['date_limite']));
				$Evenement->set("lieu", trim($_POST['lieu']));		
				$Evenement->set("departement", trim($_POST['departement']));
				$Evenement->set("sujet", trim($_POST['sujet']));
				$Evenement->set("note", trim($_POST['note']));
				$Evenement->set("cout", trim($_POST['cout']));
				$Evenement->set("description", trim($_POST['description']));
				$Evenement->set("actif", trim($_POST['actif']));

				if (!$Evenement->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de la modification.");
				}

				break;				
		}
		return $errorString;
	}

	function addslashesToValues($parameters = "") {
		if (is_array($parameters) && count($parameters) > 0) {
			foreach ($parameters as $field => $value) {
				if (is_array($value) && count($value) > 0) {
					foreach ($value as $field2 => $value2) {
						if (is_array($value2) && count($value2) > 0) {
							foreach ($value2 as $field3 => $value3) {
								$parameters[$field][$field2][$field3] = addslashes($value3);
							}
						} else {
							$parameters[$field][$field2] = addslashes($value2);
						}
					}
				} else {
					$parameters[$field] = addslashes($value);
				}
			}
		} else {
			$parameters = addslashes($parameters);
		}
		return $parameters;
	}

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "Ajout d'une formation";
				break;	
			case "modify":
				$title = "Modification d'une formation";
				break;
			case "delete":
				$title = "Suppression d'une formation";
				break;
			case "read":
				$title = "Consultation d'une formation";
				break;
		}
		return $title;
	}

	// Fonction qui retourne le html pour le menu d'actions des evenements
	function menuEvenementsString() { 
		$html =
			"<h1>&Eacute;v&eacute;nements - Journ&eacute;e de formation</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"journee_formation.php?action=add\">Ajouter une formation</a></li>\n".
			"		<li><a href=\"journee_formation.php?action=modify\">Modifier une formation</a></li>\n".
			//"		<li><a href=\"journee_formation.php?action=delete\">Supprimer une formation</a></li>\n".
			"		<li><a href=\"journee_formation.php?action=read\">Consulter une formation</a></li>\n".
			"	</ul>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le titre du sous-menu
	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "modify":
				$html .= "<h2>Modifier une formation</h2>\n";
				break;
			case "delete":
				$html .= "<h2>Supprimer une formation</h2>\n";
				break;
			case "read":
				$html .= "<h2>Consulter une formation</h2>\n";
				break;
		}
		return $html;
	}

	function getEvenementsOptionsString($id=""){
		global $DB;
		$html = "";

		$DB->query(
			"SELECT jf.id, jf.date, CONCAT_WS(' ', 'Journ&eacute;e de formation du', jf.date) as nom ".
			"FROM `journee_formation_event` jf ".
			//"WHERE jf.actif = '1' ".
			"ORDER BY jf.date DESC"
		);

		while($DB->next_record()) {
			$selected = "";		
			if ($id != "" && $id == $DB->getField("id")){
				$selected = "selected=\"selected\" ";		
			}
			$html .= "<option ".$selected."value=\"" . $DB->getField("id") . "\">" . $DB->getField("nom") . " (" . $DB->getField("date") . ")</option>\n";
		}

		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à modifier
	function modifyEvenementsString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"journee_formation.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"		<input type=\"hidden\" name=\"begin\" value=\"1\" />\n".
			"		<select name=\"id\">\n".
			"			<option value=\"0\">- Choisir une formation -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à consulter
	function readEvenementsString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"journee_formation.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"		<input type=\"hidden\" name=\"begin\" value=\"1\" />\n".
			"		<select name=\"id\">\n".
			"			<option value=\"0\">- Choisir une formation -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à supprimer
	function deleteEvenementsString() {
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"journee_formation.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"		<input type=\"hidden\" name=\"begin\" value=\"1\" />\n".
			"		<select name=\"id\">\n".
			"			<option value=\"0\">- Choisir une formation -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Supprimer\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui supprime une formation de la DB
	function deleteEvenementsFromDB($id) {
		global $DB;
		
		//$Evenement->set('actif', '0');		

	}

?>