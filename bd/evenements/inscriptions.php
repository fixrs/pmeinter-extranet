<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Evenement");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	

	// ------------------
	// LE TRAITEMENT HTML
	// ------------------
	
	$formString = "";
	$successString = "";
	$errorString = "";
	$titreString = "";
	$begin = getorpost('begin');
	$action = getorpost('action');
	$key = getorpost('ins_key');


	switch ($action) {
		case "add" :
			$titreString = "<h1>Ajouter une inscription &agrave; un &eacute;v&eacute;vement</h1>\n";
			if ($begin) {
				importForm("evenements_inscription");
				$FormObject = new Form("", "", getForm($action));
				$FormObject->setValue("evenements_key", getorpost('evenements_key'));			
				$formString = $FormObject->returnForm();

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "evenements/index.php?action=add_ins#part\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("evenements_inscription");
					$FormObject = new Form("", getParametersFromPOST("add"), getForm($action));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "modify" :
			$titreString = "<h1>Modifier  une inscription &agrave; un &eacute;v&eacute;vement</h1>\n";
			if ($begin) {
				importForm("evenements_inscription");
				$FormObject = new Form("", "", getForm($action));
				$FormObject->setValue("ins_key", $key);			
				$FormObject->setValue("action", $action);
				$FormObject->setParameters(getAllFieldsEvenementParticipation($key));
				$formString = $FormObject->returnFilledForm();

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Modifications compl&eacute;t&eacute;es avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "evenements/index.php?action=mod_ins#part\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("evenements_inscription");
					$parameters = getParametersFromPOST("modify");
					$FormObject = new Form("", $parameters, getForm($action));
					$FormObject->setValue("ins_key", $key);
					$FormObject->setValue("action", $action);
					$FormObject->setParameters($parameters);
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "read" :
			$titreString = "<h1>Consulter une inscription</h1>\n";
			importForm("evenements_inscription");
			$FormObject = new Form("", "", getForm($action));
			$parameters = getAllFieldsEvenementParticipation($key, "read");
			$FormObject->setParameters($parameters);
			$FormObject->setAllHidden();
			$formString = $FormObject->returnFilledForm();			
			break;

	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");
	$Skin->assign("page", "evenements");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("titre", $titreString);
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);

	$Skin->display("evenements.tpl");

	$DB->close();
	

	// -------------
	// LES FONCTIONS
	// -------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "PME Inter : Administration : Inscriptions aux &eacute;v&eacute;nements : Ajout";
				break;
			case "modify":
				$title = "PME Inter : Administration : Inscriptions aux &eacute;v&eacute;nements : Modification";
				break;
			default:
				$title = "PME Inter : Administration : Inscriptions aux &eacute;v&eacute;nements";
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "modify":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "read":
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "modify":
				break;

			case "modify":
				break;

			case "read":
				break;
		}
		return $html;
	}

	// Fonction qui filtre les POST provenant d'un formulaire d'évaluation
	// Retourne une string html d'erreurs OU retourne "" si aucune n'est détectée
	function validateFormValues($action) {
		$errorString = "";
		global $DB, $EMAIL_FORMAT_PATTERN;
		switch($action){
			case "add":
				// le nom de l'événement doit être entré
				if ($_POST['etudes_key']=="0"){
					$errorString .= _error("<a href=\"javascript: showErrorField('etudes_key');\">Veuillez choisir une &eacute;tude</a><br />");
				}
				if (trim($_POST['prenom'])==""){
					$errorString .= _error("<a href=\"javascript: showErrorField('prenom');\">SVP entrez le pr&eacute;nom du participant</a><br />");
				}
				if (trim($_POST['nom'])==""){
					$errorString .= _error("<a href=\"#nom_v\" onclick=\"javascript: showErrorField('nom');\">SVP entrez le nom du participant</a><br />");
				}
				if (trim($_POST['courriel'])!=""){
					if (eregi('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$', trim($_POST['courriel']))){
					}
					else {
						$errorString .= _error("<a href=\"#courriel_v\" onclick=\"javascript: showErrorField('courriel');\">SVP entrez une adresse courriel valide</a><br />");
					}
				}
			break;
			
			case "modify":
				if ($_POST['etudes_key']=="0"){
					$errorString .= _error("<a href=\"javascript: showErrorField('etudes_key');\">Veuillez choisir une &eacute;tude</a><br />");
				}
				if (trim($_POST['prenom'])==""){
					$errorString .= _error("<a href=\"javascript: showErrorField('prenom');\">Veuillez entrer le pr&eacute;nom du participant</a><br />");
				}
				if (trim($_POST['nom'])==""){
					$errorString .= _error("<a href=\"#nom_v\" onclick=\"javascript: showErrorField('nom');\">Veuillez entrer le nom du participant</a><br />");
				}
				if (trim($_POST['courriel'])!=""){
					if (eregi('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$', trim($_POST['courriel']))){
					}
					else {
						$errorString .= _error("<a href=\"#courriel_v\" onclick=\"javascript: showErrorField('courriel');\">Veuillez entrer une adresse courriel valide</a><br />");
					}
				}
			break;
		}
		return $errorString;
	}
	
	// Fonction qui met à jour les valeurs dela DB à partir des valeurs de POST provenant du formulaire
	// Retourne une string html d'erreurs OU retourne "" si aucune n'est détectée
	function updateDbValues($action) {
		global $DB;
		$errorString = "";
		$valuesArray = $_POST;

		switch ($action) {
			case "add":
				$Participation  = new Evenement($DB, "", "");
				$Participation->setTable("evenements_participation");				
				$Participation->scanfields();
				$Participation->insertmode();

				$Participation->set("nom", trim($valuesArray['nom']));
				$Participation->set("prenom", trim($valuesArray['prenom']));
				$Participation->set("etudes_key", trim($valuesArray['etudes_key']));
				$Participation->set("evenements_key", trim($valuesArray['evenements_key']));
				$Participation->set("telephone", trim($valuesArray['telephone']));
				$Participation->set("courriel", trim($valuesArray['courriel']));
				$Participation->set("date_inscription", date('Y-m-d'));

				if (!$Participation->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion.");
				}
			break;

			case "modify":
				$Participation  = new Evenement($DB, "", "");
				$Participation->setTable("evenements_participation");				
				$Participation->scanfields();
				$Participation->setQKey("key", $_POST['ins_key']);			

				$Participation->set("nom", trim($valuesArray['nom']));
				$Participation->set("prenom", trim($valuesArray['prenom']));
				$Participation->set("etudes_key", trim($valuesArray['etudes_key']));
				$Participation->set("telephone", trim($valuesArray['telephone']));
				$Participation->set("courriel", trim($valuesArray['courriel']));

				if (!$Participation->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de la modification.");
				}
			break;
		}
				
		return $errorString;		
	}

	
	function getAllFieldsEvenementParticipation($key, $read=""){
		global $DB;

		$Participation = new Evenement($DB, "", 0);
		$Participation->setTable("evenements_participation");				
		$Participation->scanfields();
		$Participation->setQKey("key", $key);
		$Participation->loadAll();
		if ($read=="read"){
			$event_key = $Participation->get('evenements_key');
			$Participation->set('evenements_key', getEvenementsName($read, $event_key));
			$etudes_key = $Participation->get('etudes_key');
			$Participation->set('etudes_key', getEtudesName($etudes_key));
		}

		return $Participation->getAll();
	}
	
	function getParametersFromPOST($action) {
		$parameters = array();
		switch ($action) {
			case "add":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;			
			case "modify":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;	
		}
		return $parameters;
	}

	function getEvenementsName($action, $event_key){
		global $DB;
		if ($event_key!= "" && $event_key!= "0"){
			$Evenement = new Evenement($DB, "", 0);
			$Evenement->scanfields();
			$Evenement->setQKey("key", $event_key);
			$Evenement->loadAll();
			$actif = "";
			if ($Evenement->get("actif")==0){
				$actif = " - Supprim&eacute;e";
			}
			$nomDate = $Evenement->get("nom") . " (". $Evenement->get("date_debut").")".$actif;

			return $nomDate;

		}
		else {
			return "";
		}
	}
	
	function getEvenementsKey($ins_key){
		global $DB;
		$html = "";

		$DB->query(
			"SELECT `evenements_key` ".
			"FROM `evenements_participation` ".
			"WHERE `key`='".$ins_key."' "
		);
		while($DB->next_record()) {
			$html = $DB->getField("evenements_key");
		}
		return $html;
	}

	function getEtudesName($etudes_key){
		global $DB;
		if ($etudes_key!= "" && $etudes_key!= "0"){
			$Etude = new Evenement($DB, "", 0);
			$Etude->setTable("etudes");				
			$Etude->scanfields();
			$Etude->setQKey("key", $etudes_key);
			$Etude->load('nom');
			return $Etude->get('nom');
		}
		else {
			return "";
		}
	}
	
?>
