<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");
		$Skin->assign("errors", $html);
		$Skin->display("evenements.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Evenement");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$successString = "";
	$errorString = "";
	$formString = "";

	// ÉTAPE #1
	// Traitement des GET provenant de la sélection d'une action dans le PREMIER menu $menuString
	switch ($_GET['action']) {
		case "add":
			// Création d'une évalutation dans la DB
			// Redirection vers la page du formulaire pour ajouter un événement
			header("Location: evenements.php?action=add&begin=1&event_key=" . $event_key);
			exit;

		case "modify":
			$formString .= modifyEvenementsString();
			break;

		case "delete":
			$formString .= deleteEvenementsString();
			break;

		case "read":
			$formString .= readEvenementsString();
			break;

		case "del_ins":
			$formString .= javascriptString().getEvenementsKeyMenuString($_GET['action']);
			break;
			
		case "add_ins":
			$formString .= javascriptString().addInscriptionsString();
			break;
			
		case "mod_ins":
			$formString .= javascriptString().getEvenementsKeyMenuString($_GET['action']);
			break;
			
		case "read_ins":
			$formString .= javascriptString().getEvenementsKeyMenuString($_GET['action']);
			break;
	}

	// ÉTAPE #2
	// Traitement des POST provenant de la sélection d'un item sélectionné dans un dropdown APRES le menu $menuString
	switch ($_POST['action']) {
		case "modify":
			// Si aucun événement sélectionné, Alors on réaffiche
			if ($_POST['event_key'] == "0") {
				$formString .= modifyEvenementsString();
				$errorString .= _error("Veuillez choisir un &eacute;v&eacute;nement dans la liste");
			// Autrement on redirige vers evenements.php
			} else {
				header("Location: evenements.php?action=modify&begin=1&event_key=" . $_POST['event_key'] . "");
				exit;
			}
			break;

		case "delete":
			// Si aucun événement sélectionné, Alors on réaffiche
			if ($_POST['event_key'] == "0") {
				$formString .= deleteEvenementsString();
				$errorString .= _error("Veuillez choisir un &eacute;v&eacute;nement dans la liste");
			// Autrement on procède à la suppression
			} else {
				if (isset($_POST['confirmation'])){
					deleteEvenementsFromDB($_POST['event_key']);
					$formString .= deleteEvenementsString();
					$successString = _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s");
				}
				else {
					$formString .= deleteEvenementsString();
					$errorString .= _error("Pour supprimer, veuillez cocher la case de confirmation");
				}
			}
			break;

		case "read":
			// Si aucun événement sélectionné, Alors on réaffiche
			if ($_POST['event_key'] == "0") {
				$formString .= modifyEvenementsString();
				$errorString .= _error("Veuillez choisir un &eacute;v&eacute;nement dans la liste");
			// Autrement on redirige vers evenements.php
			} else {
				header("Location: evenements.php?action=read&begin=1&event_key=" . $_POST['event_key'] . "");
				exit;
			}
			break;

		case "add_ins":
			// Si aucune formation sélectionnée, Alors on réaffiche
			if ($_POST['event_key'] == "0") {
				$formString .= javascriptString().addInscriptionsString();
				$errorString .= _error("Veuillez choisir un &eacute;v&eacute;nement dans la liste");
			// Autrement on redirige vers formations.php
			} else {
				header("Location: inscriptions.php?action=add&begin=1&evenements_key=" . $_POST['event_key'] . "");
				exit;
			}
			break;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "evenements");	
	$Skin->assign("page", "evenements");
	$Skin->assign("title", pageTitleString(getorpost('action')));
	$Skin->assign("menu", menuEvenementsString());
	$Skin->assign("menu_title", menuTitleString(getorpost('action')));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("evenements.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "Ajout d'&eacute;v&eacute;nements";
				break;
			case "modify":
				$title = "Modificationt d'&eacute;v&eacute;nements";
				break;
			case "delete":
				$title = "Suppression d'&eacute;v&eacute;nements";
				break;
			case "read":
				$title = "Consultation d'&eacute;v&eacute;nements";
				break;
			case "search":
				$title = "Recherche d'&eacute;v&eacute;nements";
				break;
			case "del_ins":
				$title = "Suppression d'inscriptions";
				break;
			case "add_ins":
				$title = "Ajout d'inscriptions &agrave; un &eacute;v&eacute;nement";
				break;
			case "mod_ins":
				$title = "Modification d'inscriptions &agrave; un &eacute;v&eacute;nement";
				break;
			case "read_ins":
				$title = "Consultation d'inscriptions &agrave; un &eacute;v&eacute;nement";
				break;
			default:
				$title = "Gestion des &eacute;v&eacute;nements";
		}
		return $title;
	}

	// Fonction qui retourne le html pour le menu d'actions des evenements
	function menuEvenementsString() { 
		$html =
			"<h1>&Eacute;v&eacute;nements sp&eacute;cifiques</h1>\n".
			"<h2>Directions de travail</h2>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"directions_de_travail.php?action=add\">Ajouter une rencontre</a></li>\n".
			"		<li><a href=\"directions_de_travail.php?action=modify\">Modifier une rencontre</a></li>\n".
			"		<li><a href=\"directions_de_travail.php?action=read\">Consulter une rencontre</a></li>\n".
			"	</ul>\n".
			"</div>\n".

			"<h2>Journ&eacute;e de formation</h2>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"journee_formation.php?action=add\">Ajouter une journ&eacute;e de formation</a></li>\n".
			"		<li><a href=\"journee_formation.php?action=modify\">Modifier une journ&eacute;e de formation</a></li>\n".
			"		<li><a href=\"journee_formation.php?action=read\">Consulter une journ&eacute;e de formation</a></li>\n".
			"	</ul>\n".
			"</div>\n".

			"<h2>Avis de convocation Assembl&eacute;e g&eacute;n&eacute;rale annuelle</h2>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"convocation_aga.php?action=add\">Ajouter un avis de convocation</a></li>\n".
			"		<li><a href=\"convocation_aga.php?action=modify\">Modifier un avis de convocation</a></li>\n".
			"		<li><a href=\"convocation_aga.php?action=read\">Consulter un avis de convocation</a></li>\n".
			"	</ul>\n".
			"</div>\n".

			"<h2>Avis de convocation Assembl&eacute;e g&eacute;n&eacute;rale</h2>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"convocation_ag.php?action=add\">Ajouter un avis de convocation</a></li>\n".
			"		<li><a href=\"convocation_ag.php?action=modify\">Modifier un avis de convocation</a></li>\n".
			"		<li><a href=\"convocation_ag.php?action=read\">Consulter un avis de convocation</a></li>\n".
			"	</ul>\n".
			"</div>\n".

			"<h2>Congr&egrave;s</h2>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"congres.php?action=add\">Ajouter un congr&egrave;s</a></li>\n".
			"		<li><a href=\"congres.php?action=modify\">Modifier un congr&egrave;s</a></li>\n".
			"		<li><a href=\"congres.php?action=read\">Consulter un congr&egrave;s</a></li>\n".
			"	</ul>\n".
			"</div>\n".
			

			"<br /><hr /><br />\n".

			"<h1>&Eacute;v&eacute;nements G&eacute;n&eacute;riques</h1><h2>Gestions des &eacute;v&eacute;nements</h2>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"index.php?action=add\">Ajouter un &eacute;v&eacute;nement</a></li>\n".
			"		<li><a href=\"index.php?action=modify\">Modifier un &eacute;v&eacute;nement</a></li>\n".
			"		<li><a href=\"index.php?action=delete\">Supprimer un &eacute;v&eacute;nement</a></li>\n".
			"		<li><a href=\"index.php?action=read\">Consulter un &eacute;v&eacute;nement</a></li>\n".
			"		<li><a href=\"search.php?action=search\">Rechercher des &eacute;v&eacute;nements</a></li>\n".
			"	</ul>\n".
			"</div>\n".
			"<a name=\"part\"></a><h2>Participation aux &eacute;v&eacute;nements</h2>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"index.php?action=add_ins#part\">Ajouter une inscription &agrave; un &eacute;v&eacute;nement</a></li>\n".
			"		<li><a href=\"index.php?action=mod_ins#part\">Modifier une inscription &agrave; un &eacute;v&eacute;nement</a></li>\n".
			"		<li><a href=\"index.php?action=del_ins#part\">Supprimer une inscription &agrave; un &eacute;v&eacute;nement</a></li>\n".
			"		<li><a href=\"index.php?action=read_ins#part\">Consulter une inscription &agrave; un &eacute;v&eacute;nement</a></li>\n".
			"		<li><a href=\"participation.php?action=list\">Liste des inscriptions, absences, pr&eacute;sences</a></li>\n".
			"	</ul>\n".
			"</div>\n".

			"<h2>Formations</h2>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"../formations/index.php?action=add\">Ajouter une formation</a></li>\n".
			"		<li><a href=\"../formations/index.php?action=modify\">Modifier une formation</a></li>\n".
			"		<li><a href=\"../formations/index.php?action=delete\">Supprimer une formation</a></li>\n".
			"		<li><a href=\"../formations/index.php?action=read\">Consulter une formation</a></li>\n".
			"		<li><a href=\"../formations/search.php?action=search\">Rechercher des formations</a></li>\n".
			"	</ul>\n".
			"</div>\n".
			"<a name=\"part\"></a><h2>Participation aux formations</h2>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"../formations/index.php?action=add_ins#part\">Ajouter une inscription &agrave; une formation</a></li>\n".
			"		<li><a href=\"../formations/index.php?action=mod_ins#part\">Modifier une inscription &agrave; une formation</a></li>\n".
			"		<li><a href=\"../formations/index.php?action=del_ins#part\">Supprimer une inscription &agrave; une formation</a></li>\n".
			"		<li><a href=\"../formations/index.php?action=read_ins#part\">Consulter une inscription &agrave; une formation</a></li>\n".
			"		<li><a href=\"../formations/participation.php?action=list\">Liste des inscriptions, absences, pr&eacute;sences</a></li>\n".
			"	</ul>\n".
			"</div>\n";

		return $html;
	}

	// Fonction qui retourne le html pour le titre du sous-menu
	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "modify":
				$html .= "<h2>Modifier un &eacute;v&eacute;nement</h2>\n";
				break;
			case "delete":
				$html .= "<h2>Supprimer un &eacute;v&eacute;nement</h2>\n";
				break;
			case "read":
				$html .= "<h2>Consulter un &eacute;v&eacute;nement</h2>\n";
				break;
			case "search":
				$html .= "<h2>Rechercher des &eacute;v&eacute;nements</h2>\n";
				break;
			case "del_ins":
				$title = "<h2>Supprimer une inscription</h2>\n";
				break;
			case "add_ins":
				$title = "<h2>Ajouter une inscription &agrave; un &eacute;v&eacute;nement</h2>\n";
				break;
			case "mod_ins":
				$title = "<h2>Modifier une inscription &agrave; un &eacute;v&eacute;nement</h2>\n";
				break;
			case "read_ins":
				$title = "<h2>Consulter une inscription &agrave; un &eacute;v&eacute;nement</h2>\n";
				break;
		}
		return $html;
	}

	function getEvenementsOptionsString($key=""){
		global $DB;
		$html = "";

		$DB->query(
			"SELECT `key`, `nom`, `date_debut` ".
			"FROM `evenements` ".
			"WHERE `actif`='1' ".
			"ORDER BY `date_debut` DESC"
		);
		while($DB->next_record()) {
			$selected = "";		
			if ($key!="" && $key==$DB->getField("key")){
				$selected = "selected=\"selected\" ";		
			}
			$html .= "<option ".$selected."value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . " (" . $DB->getField("date_debut") . ")</option>\n";
		}

		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à modifier
	function modifyEvenementsString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"		<select name=\"event_key\">\n".
			"			<option value=\"0\">- Choisir un &eacute;v&eacute;nement -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à consulter
	function readEvenementsString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"		<select name=\"event_key\">\n".
			"			<option value=\"0\">- Choisir un &eacute;v&eacute;nement -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à supprimer
	function deleteEvenementsString() {
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"		<select name=\"event_key\">\n".
			"			<option value=\"0\">- Choisir un &eacute;v&eacute;nement -</option>\n";
		$html .= getEvenementsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Supprimer\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui supprime une formation de la DB
	function deleteEvenementsFromDB($key) {
		global $DB;
		
		$Evenement = new Evenement($DB, "", 1);
		$Evenement->setQKey("key", $key);
		$Evenement->set('actif', '0');		

	}

	function getEvenementsKeyMenuString($action){
		$html = "";
		$formations_key = "";
		switch ($action){
			case "del_ins":
				$html .= "<h2>Supprimer une inscription</h2>";
			break;
	
			case "read_ins":
				$html .= "<h2>Consulter une inscription</h2>";
			break;
	
			default:
				$html .= "<h2>Modifier une inscription</h2>";
			break;
			
		}
		$html .= 
			"<div class=\"form\">\n".
			"	<form id=\"delete_inscription\" name=\"delete_inscription\" action=\"participation.php\" method=\"get\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"".$action."\" />\n".
			"		<select name=\"evenements_key\" onchange=\"javascript:document.delete_inscription.submit();\">\n";

		if (!isset($_GET['evenements_key'])){
			$html .=
				"			<option value=\"-1\" class=\"default\">- Veuillez choisir un &eacute;v&eacute;nement ou tous les &eacute;v&eacute;nements -</option>\n";
			$html .=
				"			<option value=\"0\">Tous les &eacute;v&eacute;nements</option>\n";
		}
		else {
			$evenements_key = $_GET['evenements_key'];
			$html .=
				"			<option value=\"0\" class=\"default\">- Tous les &eacute;v&eacute;nements -</option>\n";
		}
		$html .= getEvenementsOptionsString($evenements_key).
			"		</select>\n".
			"		<br/><br/>\n";

		$html .= "	</form>\n".
			"</div>\n";
		
		return $html;
	}
	
	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à consulter
	function addInscriptionsString() { 
		global $DB;
		$DB->query(
			"SELECT `key`, `nom`, `date_debut`, `actif` ".
			"FROM `evenements` ".
			"WHERE `actif`='1' ".
#			"AND `date_fin_inscription`>=CURRENT_DATE() ".
			"ORDER BY `date_debut` DESC"
		);
		$nb = $DB->getNumRows();
		$html =
			"<h2>Ajout d'une inscription &agrave; un &eacute;v&eacute;nement</h2>\n";
		if ($nb>0){
			$html .=
				"<div class=\"form\">\n".
				"	<form action=\"index.php\" method=\"post\">\n".
				"		<input type=\"hidden\" name=\"action\" value=\"add_ins\" />\n".
				"		<select name=\"event_key\">\n".
				"			<option value=\"0\" class=\"default\">- Choisir l'&eacute;v&eacute;nement pour lequel vous voulez faire une inscription -</option>\n";
			while($DB->next_record()) {
				$actif = "";
/*				if ($DB->getField("actif")==0){
					$actif = " - Supprim&eacute;e";
				}*/
				$html .= "<option value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . " (" . $DB->getField("date_debut") . ")".$actif."</option>\n";
			}
	
			$html .= 
				"		</select>\n".
				"		<br /><br />\n".
				"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
				"	</form>\n".
				"</div>\n";
		}
		else {
			$html .= "Aucun &eacute;v&eacute;nement n'accepte de nouvelles inscriptions";
		}
		return $html;
	}

	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function javascriptString() { 
		$html =
			"<script type=\"text/javascript\" language=\"javascript\">\n" .
			"	String.prototype.trim = function(){ \n" .
			"		return this.replace(/^\s+|\s+$/g,'');\n" .
			"		}\n".
			
			"	function areUSureUWantToDelete(strName, key) {\n".
			"		var answer = confirm('Vous êtes sure de vouloir supprimer \\n'+ strName);\n".
			"		if (answer) {\n".
			"			return true;\n".
			"		}\n".
			"		else {\n".
			"			return false;\n".
			"		} \n" .
			"	}\n".
			"</script>\n" .
			"\n";
		return $html;
	}

?>
