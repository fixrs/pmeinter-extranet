<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL;
		
		$_FORM =

			form_header($action).
			
			"	<div class=\"entry\">\n".
			"		" . userkey($action) . "\n".
			"	</div>\n".
			
			"	<div class=\"entry\">\n".
			"		" . type($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . etudes($action) . "\n".
			"	</div>\n".
			
			"	<div class=\"entry\">\n".
			"		" . nom($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . prenom($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . password($action) . "\n".
			"	</div>\n".

			password_confirmation($action) .

			"	<div class=\"entry\">\n".
			"		" . courriel($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . employe($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . actif($action) . "\n".
			"	</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html .=
					"<form name=\"utilisateur\" action=\"utilisateurs.php\" method=\"post\">\n".
					"	<input type=\"hidden\" id=\"action\" name=\"action\" value=\"add\" />\n";
				break;
			case "modify":
				$html .=	
					"<form name=\"utilisateur\" action=\"utilisateurs.php\" method=\"post\">\n".
					"	<input type=\"hidden\" id=\"action\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" id=\"key\" name=\"key\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<div class=\"readform\">\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html .=
					"	<input type=\"submit\" class=\"submit\" value=\"Soumettre\" />\n".
					"</form>\n";
				break;
			case "modify":
				$html .=
					"	<input type=\"submit\" class=\"submit\" value=\"Soumettre\" />\n".
					"</form>\n";
				break;
			case "read":
				$html .=
					"</div>\n";
				break;
		}
		return $html;
	}

	function userkey($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html .=
					"<label for=\"key\">Nom d'usager&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"key\" name=\"key\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"newkey\">Nom d'usager&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"newkey\" name=\"newkey\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Nom d'usager&nbsp;: </label>\n".					
					"<span class=\"answer\"><!---HIDDEN_key /HIDDEN_key---></span>";
				break;
		}
		return $html;
	}
	
	function type($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"	<label for=\"type\">Type de compte&nbsp;:</label>\n".
					"	<input type=\"radio\" class=\"radio\" id=\"type_super_admin\" name=\"type\" value=\"1\" onChange=\"javascript: showEtudesField()\" /><span class=\"radio_label\">Super administrateur (Contr&ocirc;le total)</span><br />".
					"	<input type=\"radio\" class=\"radio\" id=\"type_etude_total\" name=\"type\" value=\"2\" onChange=\"javascript: showEtudesField()\" /><span class=\"radio_label\">Repr&eacute;sentant d'&eacute;tude (&Eacute;criture + Lecture associ&eacute;e &agrave; une &eacute;tude)</span><br />".
					"	<input type=\"radio\" class=\"radio\" id=\"type_etude_limite\" name=\"type\" value=\"3\" onChange=\"javascript: showEtudesField()\" /><span class=\"radio_label\">Repr&eacute;sentant d'&eacute;tude (Lecture associ&eacute;e &agrave; une &eacute;tude)</span><br />".
					"	<input type=\"radio\" class=\"radio\" id=\"type_ratios\" name=\"type\" value=\"4\" onChange=\"javascript: showEtudesField()\" /><span class=\"radio_label\">Repr&eacute;sentant d'&eacute;tude (Responsable des ratios de gestion)</span>";
				break;
			case "modify":
				$html .=
					"	<label for=\"type\">Type de compte&nbsp;:</label>\n".
					"	<input type=\"radio\" class=\"radio\" id=\"type_super_admin\" name=\"type\" value=\"1\" onChange=\"javascript: showEtudesField()\" /><span class=\"radio_label\">Super administrateur (Contr&ocirc;le total)</span><br />".
					"	<input type=\"radio\" class=\"radio\" id=\"type_etude_total\" name=\"type\" value=\"2\" onChange=\"javascript: showEtudesField()\" /><span class=\"radio_label\">Repr&eacute;sentant d'&eacute;tude (&Eacute;criture + Lecture associ&eacute;e &agrave; une &eacute;tude)</span><br />".
					"	<input type=\"radio\" class=\"radio\" id=\"type_etude_limite\" name=\"type\" value=\"3\" onChange=\"javascript: showEtudesField()\" /><span class=\"radio_label\">Repr&eacute;sentant d'&eacute;tude (Lecture associ&eacute;e &agrave; une &eacute;tude)</span><br />".
					"	<input type=\"radio\" class=\"radio\" id=\"type_ratios\" name=\"type\" value=\"4\" onChange=\"javascript: showEtudesField()\" /><span class=\"radio_label\">Repr&eacute;sentant d'&eacute;tude (Responsable des ratios de gestion)</span>";
				break;
			case "read":
				$html .=
					"	<label for=\"type\">Type de compte&nbsp;:</label>\n".
					"	<span class=\"answer\"><!---HIDDEN_type /HIDDEN_type---></span>\n";
				break;
		}
		return $html;
	}
	
	function etudes($action) {
		global $DB;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"	<div class=\"entry\">\n".
					"		<label for=\"etudes_key\">Étude du représentant&nbsp;:</label>\n".
#					"		<select id=\"etudes_key\" name=\"etudes_key[]\" multiple=\"multiple\" size=\"8\" disabled=\"disabled\">\n".
					"		<select id=\"etudes_key\" name=\"etudes_key\" disabled=\"disabled\">\n";
		
				if ($_SESSION['user_type'] == 1) {
					$html .= "<option class=\"default\" value=\"-1\"> - Choisir une &eacute;tude - </option>\n";
					$DB->query(
						"SELECT `key`, `nom` ".
						"FROM `etudes` ".
						"WHERE `actif` = '1' ".
						"ORDER BY `key` ASC;"
					);
				} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
					$DB->query(
						"SELECT `key`, `nom` ".
						"FROM `etudes` ".
						"WHERE `key` = '" . $_SESSION['user_etudes_key'] . "' AND `actif` = '1' ".
						"ORDER BY `key` ASC;"
					);				
				}
		
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"etudes_key\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . "</option>\n";
				}

				$html .=
					"		</select>\n".
					"	</div>\n";
				break;

			case "modify":
				$html .=
					"	<div class=\"entry\">\n".
					"		<label for=\"etudes_key\">Étude du représentant&nbsp;:</label>\n".
#					"		<select id=\"etudes_key\" name=\"etudes_key[]\" multiple=\"multiple\" size=\"8\" disabled=\"disabled\">\n".
					"		<select id=\"etudes_key\" name=\"etudes_key\" disabled=\"disabled\">\n";
		
				if ($_SESSION['user_type'] == 1) {
					$html .= "<option class=\"default\" value=\"-1\"> - Choisir une &eacute;tude - </option>\n";
					$DB->query(
						"SELECT `key`, `nom` ".
						"FROM `etudes` ".
						"WHERE `actif` = '1' ".
						"ORDER BY `key` ASC;"
					);
				} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
					$DB->query(
						"SELECT `key`, `nom` ".
						"FROM `etudes` ".
						"WHERE `key` = '" . $_SESSION['user_etudes_key'] . "' AND `actif` = '1' ".
						"ORDER BY `key` ASC;"
					);				
				}
		
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"etudes_key\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . "</option>\n";
				}

				$html .=
					"		</select>\n".
					"	</div>\n";
				break;

			case "read":
				$html .=
					"	<label for=\"etudes_key\">Étude du représentant&nbsp;:</label>\n".
					"	<span class=\"answer\"><!---HIDDEN_etudes_nom /HIDDEN_etudes_nom---></span>\n";
				break;
		}
		return $html;
	}	

	function employe($action) {
		global $DB;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"	<div class=\"entry\">\n".
					"		<label for=\"employes_key\">Profil de l'employ&eacute;&nbsp;:</label>\n".
#					"		<select id=\"etudes_key\" name=\"etudes_key[]\" multiple=\"multiple\" size=\"8\" disabled=\"disabled\">\n".
					"		<select id=\"employes_key\" name=\"employes_key\">\n";
		
				if ($_SESSION['user_type'] == 1) {
					$html .= "<option class=\"default\" value=\"-1\"> - Choisir un profil - </option>\n";
					$DB->query(
						"SELECT `key`, `nom`, `prenom`, `courriel` ".
						"FROM `employes` ".
						"WHERE `actif` = '1' ".
						"ORDER BY `nom` ASC, `prenom` ASC;"
					);
				} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
					$DB->query(
						"SELECT e.key, e.nom, e.prenom, e.courriel ".
						"FROM `employes` e INNER JOIN `employes_etudes_succursales` ees ON e.key = ees.employes_key INNER JOIN `etudes_succursales` es ON ees.etudes_succursales_key = es.key ".
						"WHERE es.etudes_key = '" . $_SESSION['user_etudes_key'] . "' ".
						"ORDER BY e.nom, e.prenom ASC;"
					);				
				}
		
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"employes_key\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . " " . $DB->getField("prenom") . " " . $DB->getField("courriel") . "</option>\n";
				}

				$html .=
					"		</select>\n".
					"	</div>\n";
				break;

			case "modify":
				$html .=
					"	<div class=\"entry\">\n".
					"		<label for=\"employes_key\">Profil de l'employ&eacute;&nbsp;:</label>\n".
#					"		<select id=\"etudes_key\" name=\"etudes_key[]\" multiple=\"multiple\" size=\"8\" disabled=\"disabled\">\n".
					"		<select id=\"employes_key\" name=\"employes_key\">\n";
		
				if ($_SESSION['user_type'] == 1) {
					$html .= "<option class=\"default\" value=\"-1\"> - Choisir un profil - </option>\n";
					$DB->query(
						"SELECT `key`, `nom`, `prenom`, `courriel` ".
						"FROM `employes` ".
						"WHERE `actif` = '1' ".
						"ORDER BY `nom` ASC, `prenom` ASC;"
					);
				} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
					$DB->query(
						"SELECT e.key, e.nom, e.prenom, e.courriel ".
						"FROM `employes` e INNER JOIN `employes_etudes_succursales` ees ON e.key = ees.employes_key INNER JOIN `etudes_succursales` es ON ees.etudes_succursales_key = es.key ".
						"WHERE es.etudes_key = '" . $_SESSION['user_etudes_key'] . "' ".
						"ORDER BY e.nom, e.prenom ASC;"
					);				
				}
		
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"employes_key\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . " " . $DB->getField("prenom") . " " . $DB->getField("courriel") . "</option>\n";
				}

				$html .=
					"		</select>\n".
					"	</div>\n";
				break;

			case "read":
				$html .=
					"	<label for=\"employes_key\">Profil de l'employ&eacute;&nbsp;:</label>\n".
					"	<span class=\"answer\"><!---HIDDEN_employe_nom /HIDDEN_employe_nom---></span>\n";
				break;
		}
		return $html;
	}	

	function nom($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html .=
					"<label for=\"nom\">Nom de l'utilisateur&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"nom\" name=\"nom\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"nom\">Nom de l'utilisateur&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"nom\" name=\"nom\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Nom de l'utilisateur&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_nom /HIDDEN_nom---></span>";
				break;
		}
		return $html;
	}

	function prenom($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html .=
					"<label for=\"prenom\">Pr&eacute;nom de l'utilisateur&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"prenom\" name=\"prenom\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"prenom\">Pr&eacute;nom de l'utilisateur&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"prenom\" name=\"prenom\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Pr&eacute;nom de l'utilisateur&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_prenom /HIDDEN_prenom---></span>";
				break;
		}
		return $html;
	}

	function password($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html .=
					"<label for=\"password\">Mot de passe&nbsp;: </label>\n".
					"<input type=\"password\" id=\"password\" name=\"password\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"password\">Mot de passe&nbsp;: </label>\n".
					"<input type=\"password\" id=\"password\" name=\"password\" value=\"\" /> <span class=\"note\">Laisser vide si aucun changement</span>\n";
				break;
			case "read":
				$html .=
					"<label>Mot de passe&nbsp;: </label>\n".
					"<span class=\"answer\">*******</span>";
				break;
		}
		return $html;
	}

	function password_confirmation($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html .=
					"<div class=\"entry\">\n".
					"	<label for=\"password_confirmation\">Confirmation du mot de passe&nbsp;: </label>\n".
					"	<input type=\"password\" id=\"password_confirmation\" name=\"password_confirmation\" value=\"\" />\n".
					"</div>\n";
				break;
			case "modify":
				$html .=
					"<div class=\"entry\">\n".
					"	<label for=\"password_confirmation\">Confirmation du mot de passe&nbsp;: </label>\n".
					"	<input type=\"password\" id=\"password_confirmation\" name=\"password_confirmation\" value=\"\" /> <span class=\"note\">Laisser vide si aucun changement</span>\n".
					"</div>\n";
				break;
			case "read":
				$html .=
					"";				
				break;
		}
		return $html;
	}

	function courriel($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html .=
					"<label for=\"courriel\">Adresse courriel&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"courriel\" name=\"courriel\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"courriel\">Adresse courriel&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"courriel\" name=\"courriel\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Adresse courriel&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_courriel /HIDDEN_courriel---></span>";				
				break;
		}
		return $html;
	}

	function actif($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html .=
					"<label for=\"siege_social\">Compte actif&nbsp;?</label>\n".
					"<input type=\"radio\" class=\"radio\" id=\"actif_oui\" name=\"actif\" value=\"1\" checked=\"checked\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" id=\"actif_non\" name=\"actif\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;
			case "modify":
				$html .=
					"<label for=\"siege_social\">Compte actif&nbsp;?</label>\n".
					"<input type=\"radio\" class=\"radio\" id=\"actif_oui\" name=\"actif\" value=\"1\" checked=\"checked\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" id=\"actif_non\" name=\"actif\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;
			case "read":
				$html .=
					"<label>Compte actif&nbsp;? </label>\n".
					"<span class=\"answer\"><!---HIDDEN_actif /HIDDEN_actif---></span>";				
				break;
		}
		return $html;
	}

?>