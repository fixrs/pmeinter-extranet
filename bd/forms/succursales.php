<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $DB, $BASEURL;

		$_FORM =

			form_header($action).

			"	<div class=\"entry\">\n".
			"		" . etudes_key($action, $DB) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . siege_social($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . courriel_etudes_succursales($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . adresse($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . ville($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . province($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . code_postal($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . telephone1($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . telephone2($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . telecopieur($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . latlong($action) . "\n".
			"	</div>\n".

			employes_nom($action).

			form_footer($action);

		return $_FORM;
	}


	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<form name=\"succursales\" action=\"succursales.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n";
				break;
			case "modify":
				$html .=
					"<form name=\"succursales\" action=\"succursales.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"key\" value=\"\" />\n".
					"	<input type=\"hidden\" name=\"etudes_key\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<div class=\"readform\">\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html =
			"<script src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyCU1f6duHlMUlUxvZMhUgQRpWn-eA27xu8\"></script>".
			"<script src=\"/skin/js/jquery.js\"></script>".
			"<script>\n".
			"	jQuery(document).ready(function($){\n".
			"		function setLatLong() {\n".
			"			if ($(\"#lat\").val() == \"\" || $(\"#long\").val() == \"\") {\n".
			"				var geocoder = new google.maps.Geocoder();\n".
			"				var address = $(\"#adresse\").val() + ' ' + $(\"#code_postal\").val();\n".
    		"				geocoder.geocode( { 'address': address}, function(results, status) {\n".
      		"					if (status == google.maps.GeocoderStatus.OK) {\n".
        	//"						console.log(results[0].geometry.location);//results[0].geometry.location\n".
        	"						$(\"#lat\").val(results[0].geometry.location.lat());\n".
        	"						$(\"#long\").val(results[0].geometry.location.lng());\n".
        	"						return true;\n".
        	"					} else {\n".
        	"						//alert(\"Geocode was not successful for the following reason: \" + status);\n".
        	"						return false;\n".
      		"					}\n".
    		"				});\n".
    		"			}\n".
    		"			return true;\n".
    		"		}\n".
			"		$(\"input.submit\").click(function(e) {\n".
			"			if (!setLatLong()) {\n".
    		"				e.preventDefault();\n".
    		"			}\n".
    		"		});\n".

    		"		if ($(\"#adresse\").val() != \"\") {\n".
    		"			setLatLong();\n".
    		"		}\n".

			"	});\n".
			"</script>\n";


		switch ($action) {
			case "add":
				$html .=
					"	<br />\n".
					"	<input type=\"submit\" class=\"submit\" value=\"Soumettre\" />\n".
					"</form>\n";
				break;
			case "modify":
				$html .=
					"	<br />\n".
					"	<input type=\"submit\" class=\"submit\" value=\"Soumettre\" />\n".
					"</form>\n";
				break;
			case "read":
				$html .=
					"</div>\n";
				break;
		}
		return $html;
	}

	function etudes_key($action, $DB) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"etudes_key\">Nom de l'&eacute;tude&nbsp;: </label>\n".
					"<select id=\"etudes_key\" name=\"etudes_key\">\n";

				if ($_SESSION['user_type'] == 1) {
					$html .= "<option class=\"default\" value=\"-1\"> - Choisir l'&eacute;tude - </option>\n";

					$DB->query(
						"SELECT `key`, `nom` ".
						"FROM `etudes` ".
						"WHERE `actif` = '1' ".
						"ORDER BY `key` ASC"
					);
					while ($DB->next_record()) {
						$key = $DB->getField("key");
						$nom = $DB->getField("nom");
						$html .=
							"<option selectname=\"etudes_key\" value=\"" . $key . "\">" . $nom . "</option>\n";
					}

				} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
					$DB->query(
						"SELECT `key`, `nom` ".
						"FROM `etudes` ".
						"WHERE `key` = '" . $_SESSION['user_etudes_key'] . "' AND `actif` = '1' "
					);
					while ($DB->next_record()) {
						$key = $DB->getField("key");
						$nom = $DB->getField("nom");
						$html .=
							"<option selectname=\"etudes_key\" value=\"" . $key . "\">" . $nom . "</option>\n";
					}
				}

				$html .=
					"</select>\n";
				break;

			case "modify":
				$html .=
					"<label>Nom de l'&eacute;tude&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_etude_nom /HIDDEN_etude_nom---></span>";
				break;

			case "read":
				$html .=
					"<label>Nom de l'&eacute;tude&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_etude_nom /HIDDEN_etude_nom---></span>";
				break;
		}
		return $html;
	}

	function siege_social($action){
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"siege_social\">Est-ce le si&egrave;ge social de l'&eacute;tude&nbsp;?</label>\n".
					"<input type=\"radio\" class=\"radio\" id=\"siege_social_oui\" name=\"siege_social\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" id=\"siege_social_non\" name=\"siege_social\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;
			case "modify":
				$html .=
					"<label for=\"siege_social\">Est-ce le si&egrave;ge social de l'&eacute;tude&nbsp;?</label>\n".
					"<input type=\"radio\" class=\"radio\" id=\"siege_social_oui\" name=\"siege_social\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" id=\"siege_social_non\" name=\"siege_social\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;
			case "read":
				$html .=
					"<label>Si&egrave;ge social de l'&eacute;tude&nbsp;?</label>\n".
					"<span class=\"answer\"><!---HIDDEN_siege_social /HIDDEN_siege_social---></span>";
				break;
		}
		return $html;
	}

	function courriel_etudes_succursales($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"courriel\">Courriel de la succursale&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"courriel_etudes_succursales\" name=\"courriel_etudes_succursales\" value=\"\" /><span style=\"note\" style=\"font-size: 0.8em;\">Laisser vide si c'est la m&ecirc;me que l'&eacute;tude</span>\n";
				break;
			case "modify":
				$html .=
					"<label for=\"courriel\">Courriel de la succursale&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"courriel_etudes_succursales\" name=\"courriel_etudes_succursales\" value=\"\" /><span class=\"note\" style=\"font-size: 0.8em;\">Laisser vide si c'est la m&ecirc;me que l'&eacute;tude</span>\n";
				break;
			case "read":
				$html .=
					"<label>Courriel de la succursale&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_courriel_etudes_succursales /HIDDEN_courriel_etudes_succursales---></span>";
				break;
		}
		return $html;
	}

	function adresse($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"adresse\">Adresse civique&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"adresse\" name=\"adresse\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"adresse\">Adresse civique&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"adresse\" name=\"adresse\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Adresse civique&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_adresse /HIDDEN_adresse---></span>";
				break;
		}
		return $html;
	}

	function ville($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"ville\">Ville&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"ville\" name=\"ville\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"ville\">Ville&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"ville\" name=\"ville\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Ville&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_ville /HIDDEN_ville---></span>";
				break;
		}
		return $html;
	}

	function province($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"province\">Province&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"province\" name=\"province\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"province\">Province&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"province\" name=\"province\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Province&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_province /HIDDEN_province---></span>";
				break;
		}
		return $html;
	}

	function code_postal($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"code_postal\">Code postal&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"code_postal\" name=\"code_postal\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"code_postal\">Code postal&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"code_postal\" name=\"code_postal\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Code postal&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_code_postal /HIDDEN_code_postal---></span>";
				break;
		}
		return $html;
	}

	function telephone1($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"telephone1\">Num&eacute;ro de t&eacute;l&eacute;phone #1&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"telephone1\" name=\"telephone1\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"telephone1\">Num&eacute;ro de t&eacute;l&eacute;phone #1&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"telephone1\" name=\"telephone1\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Num&eacute;ro de t&eacute;l&eacute;phone #1&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_telephone1 /HIDDEN_telephone1---></span>";
				break;
		}
		return $html;
	}

	function telephone2($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"telephone2\">Num&eacute;ro de t&eacute;l&eacute;phone #2&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"telephone2\" name=\"telephone2\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"telephone2\">Num&eacute;ro de t&eacute;l&eacute;phone #2&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"telephone2\" name=\"telephone2\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Num&eacute;ro de t&eacute;l&eacute;phone #2&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_telephone2 /HIDDEN_telephone2---></span>";
				break;
		}
		return $html;
	}

	function telecopieur($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"telecopieur\">Num&eacute;ro de t&eacute;l&eacute;copieur&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"telecopieur\" name=\"telecopieur\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"telecopieur\">Num&eacute;ro de t&eacute;l&eacute;copieur&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"telecopieur\" name=\"telecopieur\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Num&eacute;ro de t&eacute;l&eacute;copieur&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_telecopieur /HIDDEN_telecopieur---></span>";
				break;
		}
		return $html;
	}

	function latlong($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"telecopieur\">Latitude &amp; Longitude&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"lat\" readonly=\"readonly\" name=\"lat\" value=\"\" /><br />\n".
					"<input type=\"text\" class=\"text\" id=\"long\" readonly=\"readonly\" name=\"long\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"telecopieur\">Latitude &amp; Longitude&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"lat\" readonly=\"readonly\" name=\"lat\" value=\"\" /><br />\n".
					"<input type=\"text\" class=\"text\" id=\"long\" readonly=\"readonly\" name=\"long\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label for=\"telecopieur\">Latitude &amp; Longitude&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_lat /HIDDEN_lat---> / <!---HIDDEN_long /HIDDEN_long---> </span>\n";
				break;
		}
		return $html;
	}

	function employes_nom($action) {
		global $DB, $BASEURL;
		$key = getorpost('key');
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<div class=\"entry\">\n".
					"	<label>Employ&eacute;s affect&eacute;s &agrave; cette succursale&nbsp;:</label>\n";

				$DB->query(
					"SELECT DISTINCT em.key AS employes_key, em.prenom AS prenom, em.nom AS nom, em.associe AS associe, f.nom AS fonctions_nom ".
					"FROM `employes` AS em, `etudes` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees, `employes_fonctions` AS ef, `fonctions` AS f ".
					"WHERE es.key = '" . $key . "' AND em.key = ees.employes_key AND es.etudes_key = et.key AND ees.etudes_succursales_key = es.key AND em.key = ef.employes_key AND ef.fonctions_key = f.key AND em.actif = '1';"
				);
				$html .= "<ul class=\"answer\">\n";
				while ($DB->next_record()) {
					$notaires_nom = $DB->getField("prenom") . " " . $DB->getField("nom") . ", " . $DB->getField("fonctions_nom");
					if ($DB->getField("associe") == "1") {
						$notaires_nom .= " [<strong>Associ&eacute;</strong>]";
					}
					$html .= "<li><a href=\"" . $BASEURL . "employes/employes.php?action=modify&begin=1&key=" . $DB->getField("employes_key") . "\" title=\"Modifier\">" . $notaires_nom . "</a></li>\n";
				}
				$html .= "</ul>\n";
				if ($notaires_nom == "") {
					$html .= "<span class=\"answer\">(aucun)</span>";
				}
				$html .=
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"entry\">\n".
					"	<label>Employ&eacute;(s) affect&eacute;s &agrave; cette succursale&nbsp;:</label>\n";

				$DB->query(
					"SELECT DISTINCT em.key AS employes_key, em.prenom AS prenom, em.nom AS nom, em.associe AS associe, f.nom AS fonctions_nom ".
					"FROM `employes` AS em, `etudes` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees, `employes_fonctions` AS ef, `fonctions` AS f ".
					"WHERE es.key = '" . $key . "' AND em.key = ees.employes_key AND es.etudes_key = et.key AND ees.etudes_succursales_key = es.key AND em.key = ef.employes_key AND ef.fonctions_key = f.key AND em.actif = '1';"
				);
				$html .= "<ul class=\"answer\">\n";
				while ($DB->next_record()) {
					$notaires_nom = $DB->getField("prenom") . " " . $DB->getField("nom") . ", " . $DB->getField("fonctions_nom");
					if ($DB->getField("associe") == "1") {
						$notaires_nom .= " [<strong>Associ&eacute;</strong>]";
					}
					$html .= "<li><a href=\"" . $BASEURL . "employes/employes.php?action=read&begin=1&key=" . $DB->getField("employes_key") . "\" title=\"Consulter\">" . $notaires_nom . "</a></li>\n";
				}
				$html .= "</ul>\n";
				if ($notaires_nom == "") {
					$html .= "<span class=\"answer\">(aucun)</span>";
				}
				$html .=
					"</div>\n";
				break;
		}
		return $html;
	}

?>
