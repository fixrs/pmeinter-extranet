<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "7").

			"	<div class=\"entry_long\">\n".
			"		<label for=\"notaires_directions_de_travail\">Liste des notaires impliqu&eacute;s dans les trois directions de travail actives de PME INTER Notaires&nbsp;:</label>\n".
			"		" . notaires_directions_de_travail($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s7_direction_aucun_notaire_raisons\">Si l'&eacute;tude n'a pas de notaires impliqu&eacute;s activement dans les trois directions de travail, veuillez en sp&eacute;cifier le raisons&nbsp;:</label>\n".
			"		" . s7_direction_aucun_notaire_raisons($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s7_suggestions_notaires_directions\">Veuillez sp&eacute;cifier les raisons qui permettraient une implication plus soutenue des notaires au sein des directions de travail (co-voiturage, participation &agrave; distance, etc.)&nbsp;:</label>\n".
			"		" . s7_suggestions_notaires_directions($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s7_facon_notaire_diffusent_info\">Veuillez indiquer de quelle fa&ccedil;on les notaires impliqu&eacute;s dans une direction de travail rediffusent l'information aupr&egrave;s de l'&eacute;tude (rapport, r&eacute;union d'associ&eacute;s, autres)&nbsp;:</label>\n".
			"		" . s7_facon_notaire_diffusent_info($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}

	function notaires_directions_de_travail($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$notaires_direction_de_travail = get_notaires_direction_de_travail();
				$html .=
					"<table>\n".
					"	<thead>\n".
					"		<tr>\n".
					"			<th width=\"34%\">&nbsp;</th>\n".
					"			<th width=\"33%\">Notaire(s) impliqu&eacute;(s)</th>\n".
					"			<th width=\"33%\">Niveau de participation en " . annee(-1) . "</th>\n".
					"		</tr>\n".
					"	</thead>\n".
					"	<tbody>\n".
					"		<tr>\n".
					"			<th>Direction du droit des affaires</th>\n".
					"			<td>\n".
					"				<select name=\"notaires_directions_de_travail_droit_des_affaires_employes_key[]\" id=\"notaires_directions_de_travail_droit_des_affaires_employes_key\" multiple=\"multiple\" size=\"12\" style=\"width: 100%;\">\n".
					"					" . notaires_direction_de_travail_selectOptions($notaires_direction_de_travail, "notaires_directions_de_travail_droit_des_affaires_employes_key") . "\n".
					"				</select>\n".
					"			</td>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s7_notaires_droit_des_affaires_participation\" id=\"s7_notaires_droit_des_affaires_participation\" value=\"\" />&nbsp;%</td>\n".
					"		</tr>\n".
					"		<tr>\n".
					"			<th>Direction du droit des personnes</th>\n".
					"			<td>\n".
					"				<select name=\"notaires_directions_de_travail_droit_des_personnes_employes_key[]\" id=\"notaires_directions_de_travail_droit_des_personnes_employes_key\" multiple=\"multiple\" size=\"12\" style=\"width: 100%;\">\n".
					"					" . notaires_direction_de_travail_selectOptions($notaires_direction_de_travail, "notaires_directions_de_travail_droit_des_personnes_employes_key") . "\n".
					"				</select>\n".
					"			</td>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s7_notairesize=\"12\"s_personnes_participation\" id=\"s7_notaires_droit_des_personnes_participation\" value=\"\" />&nbsp;%</td>\n".
					"		</tr>\n".
					"		<tr>\n".
					"			<th>Comit&eacute; de gestion de bureaux</th>\n".
					"			<td>\n".
					"				<select name=\"notaires_directions_de_travail_gestion_des_bureaux_employes_key[]\" id=\"notaires_directions_de_travail_gestion_des_bureaux_employes_key\" multiple=\"multiple\" size=\"12\" style=\"width: 100%;\">\n".
					"					" . notaires_direction_de_travail_selectOptions($notaires_direction_de_travail, "notaires_directions_de_travail_gestion_des_bureaux_employes_key") . "\n".
					"				</select>\n".
					"			</td>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s7_notaires_gestion_des_bureaux_participation\" id=\"s7_notaires_gestion_des_bureaux_participation\" value=\"\" />&nbsp;%</td>\n".
					"		</tr>\n".
					"	</tbody>\n".
					"</table>\n";
				break;

			case "read":
				$notaires_direction_de_travail = get_notaires_direction_de_travail();
				$html .=
					"<table>\n".
					"	<thead>\n".
					"		<tr>\n".
					"			<th width=\"34%\">&nbsp;</th>\n".
					"			<th width=\"33%\">Notaire(s) impliqu&eacute;(s)</th>\n".
					"			<th width=\"33%\">Niveau de participation en " . annee(-1) . "</th>\n".
					"		</tr>\n".
					"	</thead>\n".
					"	<tbody>\n".
					"		<tr>\n".
					"			<th>Direction du droit des affaires</th>\n".
					"			<td>\n".
					"				<select name=\"notaires_directions_de_travail_droit_des_affaires_employes_key[]\" id=\"notaires_directions_de_travail_droit_des_affaires_employes_key\" multiple=\"multiple\" size=\"12\" style=\"width: 100%;\">\n".
					"					" . notaires_direction_de_travail_selectOptions($notaires_direction_de_travail, "notaires_directions_de_travail_droit_des_affaires_employes_key") . "\n".
					"				</select>\n".
					"			</td>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s7_notaires_droit_des_affaires_participation\" id=\"s7_notaires_droit_des_affaires_participation\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
					"		</tr>\n".
					"		<tr>\n".
					"			<th>Direction du droit des personnes</th>\n".
					"			<td>\n".
					"				<select name=\"notaires_directions_de_travail_droit_des_personnes_employes_key[]\" id=\"notaires_directions_de_travail_droit_des_personnes_employes_key\" multiple=\"multiple\" size=\"12\" style=\"width: 100%;\">\n".
					"					" . notaires_direction_de_travail_selectOptions($notaires_direction_de_travail, "notaires_directions_de_travail_droit_des_personnes_employes_key") . "\n".
					"				</select>\n".
					"			</td>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s7_notaires_droit_des_personnes_participation\" id=\"s7_notaires_droit_des_personnes_participation\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
					"		</tr>\n".
					"		<tr>\n".
					"			<th>Comit&eacute; de gestion de bureaux</th>\n".
					"			<td>\n".
					"				<select name=\"notaires_directions_de_travail_gestion_des_bureaux_employes_key[]\" id=\"notaires_directions_de_travail_gestion_des_bureaux_employes_key\" multiple=\"multiple\" size=\"12\" style=\"width: 100%;\">\n".
					"					" . notaires_direction_de_travail_selectOptions($notaires_direction_de_travail, "notaires_directions_de_travail_gestion_des_bureaux_employes_key") . "\n".
					"				</select>\n".
					"			</td>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s7_notaires_gestion_des_bureaux_participation\" id=\"s7_notaires_gestion_des_bureaux_participation\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
					"		</tr>\n".
					"	</tbody>\n".
					"</table>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$notaires_direction_de_travail = get_notaires_direction_de_travail();
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<table>\n".
							"	<thead>\n".
							"		<tr>\n".
							"			<th width=\"34%\">&nbsp;</th>\n".
							"			<th width=\"33%\">Notaire(s) impliqu&eacute;(s)</th>\n".
							"			<th width=\"33%\">Niveau de participation en " . annee(-1, "compare", $compare_sid) . "</th>\n".
							"		</tr>\n".
							"	</thead>\n".
							"	<tbody>\n".
							"		<tr>\n".
							"			<th>Direction du droit des affaires</th>\n".
							"			<td>\n".
							"				<select name=\"n" . $compare_sid . "_otaires_directions_de_travail_droit_des_affaires_employes_key[]\" multiple=\"multiple\" size=\"12\" style=\"width: 100%;\">\n".
							"					" . notaires_direction_de_travail_selectOptions($notaires_direction_de_travail, "" . $compare_sid . "_notaires_directions_de_travail_droit_des_affaires_employes_key") . "\n".
							"				</select>\n".
							"			</td>\n".
							"			<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s7_notaires_droit_des_affaires_participation\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
							"		</tr>\n".
							"		<tr>\n".
							"			<th>Direction du droit des personnes</th>\n".
							"			<td>\n".
							"				<select name=\"" . $compare_sid . "_notaires_directions_de_travail_droit_des_personnes_employes_key[]\" multiple=\"multiple\" size=\"12\" style=\"width: 100%;\">\n".
							"					" . notaires_direction_de_travail_selectOptions($notaires_direction_de_travail, "" . $compare_sid . "_notaires_directions_de_travail_droit_des_personnes_employes_key") . "\n".
							"				</select>\n".
							"			</td>\n".
							"			<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s7_notaires_droit_des_personnes_participation\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
							"		</tr>\n".
							"		<tr>\n".
							"			<th>Comit&eacute; de gestion de bureaux</th>\n".
							"			<td>\n".
							"				<select name=\"" . $compare_sid . "_notaires_directions_de_travail_gestion_des_bureaux_employes_key[]\" multiple=\"multiple\" size=\"12\" style=\"width: 100%;\">\n".
							"					" . notaires_direction_de_travail_selectOptions($notaires_direction_de_travail, "" . $compare_sid . "_notaires_directions_de_travail_gestion_des_bureaux_employes_key") . "\n".
							"				</select>\n".
							"			</td>\n".
							"			<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s7_notaires_gestion_des_bureaux_participation\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
							"		</tr>\n".
							"	</tbody>\n".
							"</table>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function get_notaires_direction_de_travail() {
		global $DB;
		$notaires = array();
		$DB->query( 	
			"SELECT e.key AS employes_key, e.prenom AS prenom, e.nom AS nom, e.courriel AS courriel, e.annee_debut_pratique AS pratique, e.associe AS associe ".
			"FROM `employes` e, `employes_titres` et ".
			"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.actif = '1' ".
			"ORDER BY e.nom ASC;"
		);
		while ($DB->next_record()) {
			$notaires[$DB->getField("employes_key")] = $DB->getField("prenom") . " " . $DB->getField("nom");
		}
		return $notaires;
	}

	function notaires_direction_de_travail_selectOptions($employes_autres_titres, $selectname) {
		$html = "";
		foreach ($employes_autres_titres as $key => $nom) {
			$html .= "<option selectname=\"" . $selectname . "\" value=\"" . $key . "\">" . $nom . "</option>\n";
		}
		return $html;
	}

	function s7_direction_aucun_notaire_raisons($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s7_direction_aucun_notaire_raisons\" id=\"s7_direction_aucun_notaire_raisons\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s7_direction_aucun_notaire_raisons\" id=\"s7_direction_aucun_notaire_raisons\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s7_direction_aucun_notaire_raisons\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}	
		return $html;
	}
	
	function s7_suggestions_notaires_directions($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s7_suggestions_notaires_directions\" id=\"s7_suggestions_notaires_directions\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s7_suggestions_notaires_directions\" id=\"s7_suggestions_notaires_directions\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s7_suggestions_notaires_directions\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}		
		return $html;
	}

	function s7_facon_notaire_diffusent_info($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s7_facon_notaire_diffusent_info\" id=\"s7_facon_notaire_diffusent_info\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s7_facon_notaire_diffusent_info\" id=\"s7_facon_notaire_diffusent_info\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s7_facon_notaire_diffusent_info\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

?>