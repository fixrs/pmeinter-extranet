<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $DB;

		$_FORM =

			form_header($action).

			lastmod($action).

			"	<div class=\"entry\">\n".
			"		" . prenom($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . nom($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . courriel($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . ddn($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . etudes_succursales($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . succursale($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . fonctions($action) . "\n".
			"	</div>\n".

//			"	<div class=\"entry\">\n".
//			"		" . titres($action) . "\n".
//			"	</div>\n".

			"	<div id=\"notaire_only\" style=\"display: none;\">\n".

			"		<div class=\"entry\">\n".
			"			" . associe($action) . "\n".
			"		</div>\n".



			"		<div class=\"entry\">\n".
			"			" . annee_debut_pratique($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry\">\n".
			"			" . annee_debut_pratique_avocat($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry_long\">\n".
			"			" . description($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry_long\">\n".
			"			" . image($action) . "\n".
			"		</div>\n".

			expertises($action).



			/*"		<div class=\"entry\">\n".
			"			" . expertises_droit_des_affaires($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry\">\n".
			"			" . expertises_droit_des_personnes($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry\">\n".
			"			" . expertises_droit_immobilier($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry\">\n".
			"			" . expertises_autre($action) . "\n".
			"		</div>\n".
			*/

			"	</div>\n".
			"		<div class=\"entry_long\">\n".
			"			" . secteur($action) . "\n".
			"		</div>\n".
			"		<div class=\"entry_long\">\n".
			"			" . comite_direction_travail($action) . "\n".
			"		</div>\n".

			"	<div class=\"entry\">\n".
			"		" . linkedin($action) . "\n".
			"	</div>\n".
			"	<div class=\"entry\">\n".
			"		" . twitter($action) . "\n".
			"	</div>\n".
			"	<div class=\"entry\">\n".
			"		" . facebook($action) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		global $EMPLOYES_IMG_MAX_SIZE;
		$html = "";
		switch ($action) {
			case "add":
				$html =
					"<form name=\"employe\" action=\"employes.php\" method=\"post\" enctype=\"multipart/form-data\">\n".
#					"	<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"" . $EMPLOYES_IMG_MAX_SIZE . "\" />\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n";
				break;
			case "modify":
				$html =
					"<form name=\"employe\" action=\"employes.php\" method=\"post\" enctype=\"multipart/form-data\">\n".
#					"	<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"" . $EMPLOYES_IMG_MAX_SIZE . "\" />\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"key\" value=\"\" />\n";
				break;
			case "read":
				$html =
					"<div class=\"readform\">\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					//"	<input type=\"submit\" style=\"display:none;\"/>\n".onclick=\"javascript: hta_description.update(); document.employe.submit();\"
					"	<input type=\"submit\" class=\"submit\" value=\"Soumettre\"  />\n".
					"</form>\n";
				break;
			case "modify":
				$html .=
					//"	<input type=\"submit\" style=\"display:none;\"/>\n". onclick=\"javascript: hta_description.update(); document.employe.submit();\"
					"	<input type=\"submit\" class=\"submit\" value=\"Soumettre\" />\n".
					"</form>\n";
				break;
			case "read":
				$html .=
					"</div>\n";
				break;
		}
		return $html;
	}

	function lastmod($action) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<div class=\"entry\">\n".
					"	<label for=\"lastmod\">Date de la derni&egrave;re modification&nbsp;:</label>\n".
					"	<input type=\"text\" class=\"text\" id=\"lastmod\" name=\"lastmod\" value=\"\" readonly=\"readonly\"/>\n".
					"</div>\n";
				break;
			case "read":
				$html .=
					"<div class=\"entry\">\n".
					"	<label>Date de la derni&egrave;re modification&nbsp;:</label>\n".
					"	<span class=\"answer\"><!---HIDDEN_lastmod /HIDDEN_lastmod---></span>\n".
					"</div>\n";
				break;
		}
		return $html;
	}

	function prenom($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"prenom\">Pr&eacute;nom de l'employ&eacute;&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"prenom\" name=\"prenom\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"prenom\">Pr&eacute;nom de l'employ&eacute;&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"prenom\" name=\"prenom\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Pr&eacute;nom de l'employ&eacute;&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_prenom /HIDDEN_prenom---></span>\n";
				break;
		}
		return $html;
	}

	function nom($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"nom\">Nom de l'employ&eacute;&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"nom\" name=\"nom\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"nom\">Nom de l'employ&eacute;&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"nom\" name=\"nom\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Nom de l'employ&eacute;&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_nom /HIDDEN_nom---></span>\n";
				break;
		}
		return $html;
	}

	function courriel($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"courriel\">Courriel de l'employ&eacute;&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"courriel\" name=\"courriel\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"courriel\">Courriel de l'employ&eacute;&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"courriel\" name=\"courriel\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Courriel de l'employ&eacute;&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_courriel /HIDDEN_courriel---></span>\n";
				break;
		}
		return $html;
	}

	function linkedin($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"linkedin\">LinkedIn (url complet)&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"linkedin\" name=\"linkedin\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"linkedin\">LinkedIn (url complet)&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"linkedin\" name=\"linkedin\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>LinkedIn (url complet)</label>\n".
					"<span class=\"answer\"><!---HIDDEN_linkedin /HIDDEN_linkedin---></span>\n";
				break;
		}
		return $html;
	}

	function twitter($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"twitter\">Twitter (url complet)&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"twitter\" name=\"twitter\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"twitter\">Twitter (url complet)&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"twitter\" name=\"twitter\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Twitter (url complet)</label>\n".
					"<span class=\"answer\"><!---HIDDEN_twitter /HIDDEN_twitter---></span>\n";
				break;
		}
		return $html;
	}

	function facebook($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"facebook\">Facebook (url complet)&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"facebook\" name=\"facebook\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"facebook\">Facebook (url complet)&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"facebook\" name=\"facebook\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Facebook (url complet)</label>\n".
					"<span class=\"answer\"><!---HIDDEN_facebook /HIDDEN_facebook---></span>\n";
				break;
		}
		return $html;
	}

	function etudes_succursales($action) {
		global $DB;
		$html = "";
		switch ($action) {

			case "add":
				$html .=
					"	<label for=\"etudes_succursales_key\">Succursale(s) pour laquelle ou lesquelles l'employ&eacute; travaille&nbsp;: </label>\n".
					"	<select id=\"etudes_succursales_key\" name=\"etudes_succursales_key[]\" multiple=\"multiple\" size=\"8\">\n";

				if ($_SESSION['user_type'] == 1) {
					$DB->query(
						"SELECT e.nom AS etude_nom, es.key AS succursale_key, es.etudes_key AS etude_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville ".
						"FROM `etudes` AS e, `etudes_succursales` AS es ".
						"WHERE es.etudes_key = e.key AND es.actif = '1' ".
						"ORDER BY e.nom;"
					);

					$previous = "";
					$first = 1;
					while ($DB->next_record()) {
						$optgroup = $DB->getField("etude_nom");
						$value = $DB->getField("succursale_key");
						$option = $DB->getField("adresse") . ", " . $DB->getField("ville");
						if ($DB->getField("siege_social") == 1) {
							$option .= " [SS]";
						}
						if ($optgroup != $previous) {
							if (!$first) {
								$html .= "</optgroup>";
							}
							$html .= "<optgroup label=\"" . $optgroup . "\">";
						}
						$html .= "<option selectname=\"etudes_succursales_key\" value=\"" . $value . "\">" . $option . "</option>\n";
						$previous = $optgroup;
						$first = 0;
					}

				} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
					$DB->query(
						"SELECT e.nom AS etude_nom, es.key AS succursale_key, es.etudes_key AS etude_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville ".
						"FROM `etudes` AS e, `etudes_succursales` AS es ".
						"WHERE es.etudes_key = e.key AND es.actif = '1' AND e.key = '" . $_SESSION['user_etudes_key'] . "' ".
						"ORDER BY e.nom;"
					);

					$previous = "";
					$first = 1;
					while ($DB->next_record()) {
						$optgroup = $DB->getField("etude_nom");
						$value = $DB->getField("succursale_key");
						$option = $DB->getField("adresse") . ", " . $DB->getField("ville");
						if ($DB->getField("siege_social") == 1) {
							$option .= " [SS]";
						}
						if ($optgroup != $previous) {
							if (!$first) {
								$html .= "</optgroup>";
							}
							$html .= "<optgroup label=\"" . $optgroup . "\">";
						}
						$html .= "<option selectname=\"etudes_succursales_key\" value=\"" . $value . "\">" . $option . "</option>\n";
						$previous = $optgroup;
						$first = 0;
					}
				}

				$html .=
					"	</select>\n";
				break;

			case "modify":
				$html .=
					"	<label for=\"etudes_succursales_key\">Succursale(s) pour laquelle ou lesquelles l'employ&eacute; travaille&nbsp;: </label>\n".
					"	<select id=\"etudes_succursales_key\" name=\"etudes_succursales_key[]\" multiple=\"multiple\" size=\"8\">\n";

				if ($_SESSION['user_type'] == 1) {
					$DB->query(
						"SELECT e.nom AS etude_nom, es.key AS succursale_key, es.etudes_key AS etude_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville ".
						"FROM `etudes` AS e, `etudes_succursales` AS es ".
						"WHERE es.etudes_key = e.key AND es.actif = '1' ".
						"ORDER BY e.nom;"
					);

					$previous = "";
					$first = 1;
					while ($DB->next_record()) {
						$optgroup = $DB->getField("etude_nom");
						$value = $DB->getField("succursale_key");
						$option = $DB->getField("adresse") . ", " . $DB->getField("ville");
						if ($DB->getField("siege_social") == 1) {
							$option .= " [SS]";
						}
						if ($optgroup != $previous) {
							if (!$first) {
								$html .= "</optgroup>";
							}
							$html .= "<optgroup label=\"" . $optgroup . "\">";
						}
						$html .= "<option selectname=\"etudes_succursales_key\" value=\"" . $value . "\">" . $option . "</option>\n";
						$previous = $optgroup;
						$first = 0;
					}

				} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
					$DB->query(
						"SELECT e.nom AS etude_nom, es.key AS succursale_key, es.etudes_key AS etude_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville ".
						"FROM `etudes` AS e, `etudes_succursales` AS es ".
						"WHERE es.etudes_key = e.key AND es.actif = '1' AND e.key = '" . $_SESSION['user_etudes_key'] . "' ".
						"ORDER BY e.nom;"
					);

					$previous = "";
					$first = 1;
					while ($DB->next_record()) {
						$optgroup = $DB->getField("etude_nom");
						$value = $DB->getField("succursale_key");
						$option = $DB->getField("adresse") . ", " . $DB->getField("ville");
						if ($DB->getField("siege_social") == 1) {
							$option .= " [SS]";
						}
						if ($optgroup != $previous) {
							if (!$first) {
								$html .= "</optgroup>";
							}
							$html .= "<optgroup label=\"" . $optgroup . "\">";
						}
						$html .= "<option selectname=\"etudes_succursales_key\" value=\"" . $value . "\">" . $option . "</option>\n";
						$previous = $optgroup;
						$first = 0;
					}
				}

				$html .=
					"	</select>\n";
				break;

			case "read":
				$html .=
					"	<label>Succursale(s) pour laquelle ou lesquelles l'employ&eacute; travaille&nbsp;:</label>\n".
					"	<!---HIDDEN_etudes_succursales_nom /HIDDEN_etudes_succursales_nom--->\n";
				break;
		}
		return $html;
	}

	function succursale($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"succursale\">Succursale(s) attitr&eacute;e(s) &agrave; l'employ&eacute;&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"succursale\" name=\"succursale\" value=\"\" /><br />\n".
					"<span class=\"hint\">* Facultatif&nbsp;: Remplissez ce champs uniquement si vous souhaitez attitrer &agrave; l'employ&eacute; une ou plusieurs succursales en particulier. Cette information appara&icirc;tra sur le site publique.</span>\n";
				break;
			case "modify":
				$html .=
					"<label for=\"succursale\">Succursale(s) attitr&eacute;e(s) &agrave; l'employ&eacute;&nbsp;: </label>\n".
					"<input type=\"text\" class=\"text\" id=\"succursale\" name=\"succursale\" value=\"\" /><br />\n".
					"<span class=\"hint\">* Facultatif&nbsp;: Remplissez ce champs uniquement si vous souhaitez attitrer &agrave; l'employ&eacute; une ou plusieurs succursales en particulier. Cette information appara&icirc;tra sur le site publique.</span>\n";
				break;
			case "read":
				$html .=
					"<label>Succursale(s) attitr&eacute;e(s) &agrave; l'employ&eacute;&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_succursale /HIDDEN_succursale---></span>\n";
				break;
		}
		return $html;
	}

	function fonctions($action) {
		global $DB;
		$html = "";

		switch ($action) {

			case "add":
				$html .=
					"	<label for=\"fonctions_key\">Fonction(s) de l'employ&eacute;&nbsp;: </label><br /><br />\n";
					//"	<select id=\"fonctions_key\" name=\"fonctions_key[]\" multiple=\"multiple\" size=\"8\" onchange=\"javascript: showNotaireFields('modify');\">\n";
				$DB->query(
					"SELECT `nom`, `key` ".
					"FROM `fonctions` ".
					"ORDER BY `nom` ASC"
				);
				while ($DB->next_record()) {
					$value = $DB->getField("key");
					$option = $DB->getField("nom");
					$html .= //"<option selectname=\"fonctions_key\" value=\"" . $value . "\">" . $option . "</option>\n";
						"<input type=\"checkbox\" name=\"fonctions_key_" . $value . "\" id=\"fonctions_key_" . $value . "\" value=\"" . $value . "\"> <label for=\"fonctions_key_" . $value . "\" style=\"margin-top: 0px;\">" . $option . "</label><br />\n";
				}
				// $html .=
				// 	"	</select>\n";
				break;

			case "modify":
				$html .=
					"	<label for=\"fonctions_key\">Fonction(s) de l'employ&eacute;&nbsp;: </label><br /><br />\n";//.
					//"	<select id=\"fonctions_key\" name=\"fonctions_key[]\" multiple=\"multiple\" size=\"8\" onchange=\"javascript: showNotaireFields('modify');\">\n";
				$DB->query(
					"SELECT `nom`, `key` ".
					"FROM `fonctions` ".
					"ORDER BY `nom` ASC"
				);
				while ($DB->next_record()) {
					$value = $DB->getField("key");
					$option = $DB->getField("nom");
					$html .= //"<option selectname=\"fonctions_key\" value=\"" . $value . "\">" . $option . "</option>\n";
						"<input type=\"checkbox\" name=\"fonctions_key_" . $value . "\" id=\"fonctions_key_" . $value . "\" value=\"" . $value . "\"> <label for=\"fonctions_key_" . $value . "\" style=\"margin-top: 0px;\" onclick=\"javascript: showNotaireFields('modify');\">" . $option . "</label><br />\n";
				}

				// while ($DB->next_record()) {
				// 	$value = $DB->getField("key");
				// 	$option = $DB->getField("nom");
				// 	$html .= "<option selectname=\"fonctions_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				// }
				// $html .=
				// 	"	</select>\n";
				break;

			case "read":
				$html .=
					"	<label>Fonction(s) de l'employ&eacute;&nbsp;:</label>\n".
					"	<!---HIDDEN_fonctions_nom /HIDDEN_fonctions_nom--->\n";
				break;
		}
		return $html;
	}

/*	function titres($action) {
		global $DB;
		$html = "";
		switch ($action) {

			case "add":
				$html .=
					"	<label for=\"titres_key\">Titre(s) de l'employ&eacute;&nbsp;: </label>\n".
					"	<select id=\"titres_key\" name=\"titres_key[]\" multiple=\"multiple\" size=\"6\" onclick=\"javascript: showNotaireFields('add')\">\n";
				$DB->query(
					"SELECT `nom`, `key` ".
					"FROM `titres` ".
					"ORDER BY `nom` ASC"
				);
				while ($DB->next_record()) {
					$value = $DB->getField("key");
					$option = $DB->getField("nom");
					$html .= "<option selectname=\"titres_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				}
				$html .=
					"	</select>\n";
				break;

			case "modify":
				$html .=
					"	<label for=\"titres_key\">Titre(s) de l'employ&eacute;&nbsp;: </label>\n".
					"	<select id=\"titres_key\" name=\"titres_key[]\" multiple=\"multiple\" size=\"6\" onclick=\"javascript: showNotaireFields('modify')\">\n";
				$DB->query(
					"SELECT `nom`, `key` ".
					"FROM `titres` ".
					"ORDER BY `nom` ASC"
				);
				while ($DB->next_record()) {
					$value = $DB->getField("key");
					$option = $DB->getField("nom");
					$html .= "<option selectname=\"titres_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				}
				$html .=
					"	</select>\n";
				break;

			case "read":
				$html .=
					"	<label>Titre(s) de l'employ&eacute;&nbsp;:</label>\n".
					"	<!---HIDDEN_titres_nom /HIDDEN_titres_nom--->\n";
				break;
		}
		return $html;
	}*/

	function secteur($action) {
		global $DB;
		$html = "";
		switch ($action) {

			case "add":
				$html .=
					"	<label for=\"secteur\"><strong>Secteur de droit&nbsp;:</strong></label>\n".
					"	<input type=\"checkbox\" id=\"secteur1\" name=\"secteur1\" value=\"Droit des affaires\"> <label class=\"inline\" for=\"secteur1\">Droit des affaires</label><br />\n".
					"	<input type=\"checkbox\" id=\"secteur2\" name=\"secteur2\" value=\"Droit de la personne\"> <label class=\"inline\" for=\"secteur2\">Droit de la personne</label><br />\n".
					"	<input type=\"checkbox\" id=\"secteur3\" name=\"secteur3\" value=\"Droit immobilier\"> <label class=\"inline\" for=\"secteur3\">Droit immobilier</label><br />\n".
					"	<input type=\"checkbox\" id=\"secteur4\" name=\"secteur4\" value=\"Droit agricole\"> <label class=\"inline\" for=\"secteur4\">Droit agricole</label><br />\n".
					"	<input type=\"checkbox\" id=\"secteur5\" name=\"secteur5\" value=\"Expertise sectorielle\"> <label class=\"inline\" for=\"secteur5\">Expertise sectorielle</label><br />\n".
					"	<label for=\"secteuro\">Autre : précisez</label> <input type=\"text\" id=\"secteuro\" name=\"secteuro\" value=\"\">\n";
				break;

			case "modify":
				$html .=
					"	<label for=\"secteur\"><strong>Secteur de droit&nbsp;:</strong></label>\n".
					"	<input type=\"checkbox\" id=\"secteur1\" name=\"secteur1\" value=\"Droit des affaires\"> <label class=\"inline\" for=\"secteur1\">Droit des affaires</label><br />\n".
					"	<input type=\"checkbox\" id=\"secteur2\" name=\"secteur2\" value=\"Droit de la personne\"> <label class=\"inline\" for=\"secteur2\">Droit de la personne</label><br />\n".
					"	<input type=\"checkbox\" id=\"secteur3\" name=\"secteur3\" value=\"Droit immobilier\"> <label class=\"inline\" for=\"secteur3\">Droit immobilier</label><br />\n".
					"	<input type=\"checkbox\" id=\"secteur4\" name=\"secteur4\" value=\"Droit agricole\"> <label class=\"inline\" for=\"secteur4\">Droit agricole</label><br />\n".
					"	<input type=\"checkbox\" id=\"secteur5\" name=\"secteur5\" value=\"Expertise sectorielle\"> <label class=\"inline\" for=\"secteur5\">Expertise sectorielle</label><br />\n".
					"	<label for=\"secteuro\">Autre : précisez</label> <input type=\"text\" id=\"secteuro\" name=\"secteuro\" value=\"\">\n";
				break;

			case "read":
				$html .=
					"	<label>Secteur de droit&nbsp;:</label>\n".
					"	<!---HIDDEN_secteur1 /HIDDEN_secteur1---><br />\n".
					"	<!---HIDDEN_secteur2 /HIDDEN_secteur2---><br />\n".
					"	<!---HIDDEN_secteur3 /HIDDEN_secteur3---><br />\n".
					"	<!---HIDDEN_secteur4 /HIDDEN_secteur4---><br />\n".
					"	<!---HIDDEN_secteur5 /HIDDEN_secteur5---><br />\n".
					"	<!---HIDDEN_secteuro /HIDDEN_secteuro--->\n";

				break;
		}
		return $html;
	}

	function comite_direction_travail($action) {
		global $DB;
		$html = "";
		switch ($action) {

			case "add":
				$html .= "";
				break;

			case "modify":
				$html .=
					"	<label for=\"directions_de_travail_key\">Comit&eacute;s des directions de travail&nbsp;: </label>\n".
					"	<select id=\"directions_de_travail_key\" name=\"directions_de_travail_key[]\" multiple=\"multiple\" size=\"6\" onclick=\"javascript: showNotaireFields('modify')\">\n";
				$DB->query(
					"SELECT `nom`, `key` ".
					"FROM `directions_de_travail` ".
					"ORDER BY `nom` ASC"
				);
				while ($DB->next_record()) {
					$value = $DB->getField("key");
					$option = $DB->getField("nom");
					$html .= "<option selectname=\"directions_de_travail_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				}
				$html .=
					"	</select>\n";
				break;

			case "read":
				$html .=
					"	<label>Comit&eacute;s des directions de travail&nbsp;:</label>\n".
					"	<!---HIDDEN_direction_nom /HIDDEN_direction_nom--->\n";
				break;
		}
		return $html;
	}

	function associe($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"	<label for=\"associe_1\">Est-ce que le notaire/avocat est associ&eacute;&nbsp;?</label>\n".
					"	<div id=\"associe\">\n".
					"		<input type=\"radio\" class=\"radio\" id=\"associe_oui\" name=\"associe\" value=\"1\" /><span class=\"radio_label\">Oui</span>\n".
					"		<input type=\"radio\" class=\"radio\" id=\"associe_non\" name=\"associe\" value=\"0\" /><span class=\"radio_label\">Non</span>\n".
					"	</div>\n";
				break;
			case "modify":
				$html .=
					"	<label for=\"associe_1\">Est-ce que le notaire/avocat est associ&eacute;&nbsp;?</label>\n".
					"	<div id=\"associe\">\n".
					"		<input type=\"radio\" class=\"radio\" id=\"associe_oui\" name=\"associe\" value=\"1\" /><span class=\"radio_label\">Oui</span>\n".
					"		<input type=\"radio\" class=\"radio\" id=\"associe_non\" name=\"associe\" value=\"0\" /><span class=\"radio_label\">Non</span>\n".
					"	</div>\n";
				break;
			case "read":
				$html .=
					"	<label>Est-ce que le notaire/avocat est associ&eacute;&nbsp;?</label>\n".
					"	<span class=\"answer\"><!---HIDDEN_associe /HIDDEN_associe---></span>\n";
				break;
		}
		return $html;
	}

	function ddn($action) {
		global $DB;
		$html = "";
		switch ($action) {

			case "add":
				$html .=
					"	<small><em>Afin de bonifier nos données statistiques concernant le profil de l’ensemble des membres selon leur groupe d’âge, il est dorénavant obligatoire d’entrer l’année de naissance de chaque employé sur la base de données. Ces informations demeureront strictement confidentielles  et ne seront utilisées qu’à des fins statistiques.</em></small><br />\n".
					"	<label for=\"ddn\">Date de naissance&nbsp;: </label>\n".
					"	<select id=\"ddn_a\" name=\"ddn_a\">" . getOptionsAnnee($action, $DB, "ddn_a") . "</select>&nbsp;<select id=\"ddn_m\" name=\"ddn_m\">" . getOptionsMois($action, $DB, "ddn_m") . "</select>&nbsp;<select id=\"ddn_j\" name=\"ddn_j\">" . getOptionsJour($action, $DB, "ddn_j") . "</select><br />\n".
					"	<!--span class=\"hint\">* L'ann&eacute;e est facultative.</span-->\n";
				break;

			case "modify":
				$html .=
					"	<small><em>Afin de bonifier nos données statistiques concernant le profil de l’ensemble des membres selon leur groupe d’âge, il est dorénavant obligatoire d’entrer l’année de naissance de chaque employé sur la base de données. Ces informations demeureront strictement confidentielles  et ne seront utilisées qu’à des fins statistiques.</em></small><br />\n".
					"	<label for=\"ddn\">Date de naissance&nbsp;: </label>\n".
					"	<select id=\"ddn_a\" name=\"ddn_a\">" . getOptionsAnnee($action, $DB, "ddn_a") . "</select>&nbsp;<select id=\"ddn_m\" name=\"ddn_m\">" . getOptionsMois($action, $DB, "ddn_m") . "</select>&nbsp;<select id=\"ddn_j\" name=\"ddn_j\">" . getOptionsJour($action, $DB, "ddn_j") . "</select><br />\n".
					"	<!--span class=\"hint\">* L'ann&eacute;e est facultative.</span-->\n";
				break;

			case "read":
				$html .=
					"	<label>Date de naissance (AAAA-MM-JJ)&nbsp;: </label>\n".
					"	<span class=\"answer\"><!---HIDDEN_ddn_a /HIDDEN_ddn_a--->-<!---HIDDEN_ddn_m /HIDDEN_ddn_m--->-<!---HIDDEN_ddn_j /HIDDEN_ddn_j---></span>\n";
				break;
		}
		return $html;
	}

	function getOptionsAnnee($action, $DB = "", $field = "") {
		$key = trim(getorpost('key'));
		$html = "<option class=\"default\" value=\"\"> - Ann&eacute;e - </option>";
		switch ($action) {
			case "add":
				$value = getorpost($field);
				for ($n = 1910; $n <= date("Y"); $n++) {
					if ($n == $value) {
						$html .= "<option value=\"" . $n . "\" selected=\"selected\">" . $n . "</option>\n";
					} else {
						$html .= "<option value=\"" . $n . "\">" . $n . "</option>\n";
					}
				}
				break;

			case "modify":
				if ($field != "" && getorpost($field) == "" && $key != "") {
					$DB->query(
						"SELECT `" . $field . "` ".
						"FROM `employes` ".
						"WHERE `key` = '" . addslashes($key) . "';"
					);
					while ($DB->next_record()) {
						$value = $DB->getField($field);
					}
				} else {
					$value = getorpost($field);
				}
				for ($n = 1910; $n <= date("Y"); $n++) {
					if ($n == $value) {
						$html .= "<option value=\"" . $n . "\" selected=\"selected\">" . $n . "</option>\n";
					} else {
						$html .= "<option value=\"" . $n . "\">" . $n . "</option>\n";
					}
				}
				break;
		}
		return $html;
	}

	function getOptionsMois($action, $DB, $field) {
		$key = trim(getorpost('key'));
		$html = "<option class=\"default\" value=\"\"> - Mois - </option>";
		switch ($action) {
			case "add":
				$value = getorpost($field);
				for ($n = 1; $n <= 12; $n++) {
					if ($n == $value) {
						$html .= "<option value=\"" . $n . "\" selected=\"selected\">" . $n . "</option>\n";
					} else {
						$html .= "<option value=\"" . $n . "\">" . $n . "</option>\n";
					}
				}
				break;

			case "modify":
				if ($field != "" && getorpost($field) == "" && $key != "") {
					$DB->query(
						"SELECT `" . $field . "` ".
						"FROM `employes` ".
						"WHERE `key` = '" . addslashes($key) . "';"
					);
					while ($DB->next_record()) {
						$value = $DB->getField($field);
					}
				} else {
					$value = getorpost($field);
				}
				for ($n = 1; $n <= 12; $n++) {
					if ($n == $value) {
						$html .= "<option value=\"" . $n . "\" selected=\"selected\">" . $n . "</option>\n";
					} else {
						$html .= "<option value=\"" . $n . "\">" . $n . "</option>\n";
					}
				}
				break;
		}
		return $html;
	}

	function getOptionsJour($action, $DB, $field) {
		$key = trim(getorpost('key'));
		$html = "<option class=\"default\" value=\"\"> - Jour - </option>";
		switch ($action) {
			case "add":
				$value = getorpost($field);
				for ($n = 1; $n <= 31; $n++) {
					if ($n == $value) {
						$html .= "<option value=\"" . $n . "\" selected=\"selected\">" . $n . "</option>\n";
					} else {
						$html .= "<option value=\"" . $n . "\">" . $n . "</option>\n";
					}
				}
				break;

			case "modify":
				if ($field != "" && getorpost($field) == "" && $key != "") {
					$DB->query(
						"SELECT `" . $field . "` ".
						"FROM `employes` ".
						"WHERE `key` = '" . addslashes($key) . "';"
					);
					while ($DB->next_record()) {
						$value = $DB->getField($field);
					}
				} else {
					$value = getorpost($field);
				}
				for ($n = 1; $n <= 31; $n++) {
					if ($n == $value) {
						$html .= "<option value=\"" . $n . "\" selected=\"selected\">" . $n . "</option>\n";
					} else {
						$html .= "<option value=\"" . $n . "\">" . $n . "</option>\n";
					}
				}
				break;
		}
		return $html;
	}

	function annee_debut_pratique($action) {
		global $DB;

		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"	<label for=\"annee_debut_pratique\"><strong>Notaire depuis</strong>&nbsp;: </label>\n".
					"	<select id=\"annee_debut_pratique\" name=\"annee_debut_pratique\" >" . getOptionsAnnee($action, $DB, "annee_debut_pratique") . "</select>\n";
				break;

			case "modify":
				$html .=
					"	<label for=\"annee_debut_pratique\"><strong>Notaire depuis</strong>&nbsp;: </label>\n".
					"	<select id=\"annee_debut_pratique\" name=\"annee_debut_pratique\" >" . getOptionsAnnee($action, $DB, "annee_debut_pratique") . "</select>\n";
				break;

			case "read":
				$html .=
					"	<label>Notaire depuis&nbsp;:</label>\n".
					"	<span class=\"answer\"><!---HIDDEN_annee_debut_pratique /HIDDEN_annee_debut_pratique---></span>\n";
				break;
		}
		return $html;
	}

	function annee_debut_pratique_avocat($action) {
		global $DB;

		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"	<label for=\"annee_debut_pratique_avocat\"><strong>Avocat depuis</strong>&nbsp;: </label>\n".
					"	<select id=\"annee_debut_pratique_avocat\" name=\"annee_debut_pratique_avocat\" >" . getOptionsAnnee($action, $DB, "annee_debut_pratique_avocat") . "</select>\n";
				break;

			case "modify":
				$html .=
					"	<label for=\"annee_debut_pratique_avocat\"><strong>Avocat depuis</strong>&nbsp;: </label>\n".
					"	<select id=\"annee_debut_pratique_avocat\" name=\"annee_debut_pratique_avocat\" >" . getOptionsAnnee($action, $DB, "annee_debut_pratique_avocat") . "</select>\n";
				break;

			case "read":
				$html .=
					"	<label>Avocat depuis&nbsp;:</label>\n".
					"	<span class=\"answer\"><!---HIDDEN_annee_debut_pratique_avocat /HIDDEN_annee_debut_pratique_avocat---></span>\n";
				break;
		}
		return $html;
	}

	function description($action) {
		global $DB;
		$key = getorpost('key');
		$html = "";

		switch ($action) {
			case "add":
				$description = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('description')))));
				$description = getorpost('description');

				$html .=
					"<label for=\"description\">Notes biographiques&nbsp;:</label>\n".
					// "<div id=\"hta_description_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_description = new HyperTextArea('employes', 'description', '" . $description . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					"<textarea name=\"description\" id=\"description\" rows=\"15\">" . $description . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { \n".
					"		$('#description').htmlarea(); \n".
					"		$('.jHtmlArea, .jHtmlArea .ToolBar').css('width', '600px'); \n".
					"		$('.jHtmlArea').find('iframe').css('width', '600px'); \n".
					"		$('.jHtmlArea').find('iframe').css('height', '200px'); \n".
					"	}); \n".
					"</script>\n".
					"<br clear=\"all\" />\n".
					//"<span class=\"hta_chrcount\"><span id=\"description_chrcount\" class=\"description_chrcount\">0</span>&nbsp;/&nbsp;1750 caract&egrave;res maximum.</span>\n".
					//"<span style=\"float:right\";><input type=\"button\" class=\"hta_button\" value=\"Actualiser le nb de caract&egrave;res\" onclick=\"javascript: hta_description.updateChrCount();\" />&nbsp;&nbsp;<input type=\"button\" class=\"hta_button\" value=\"Tout effacer\" onclick=\"javascript: hta_description.clearText(); hta_description.updateChrCount();\" /></span>\n".
					"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					"<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";
				break;

			case "modify":
				$description = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('description')))));
				$description = getorpost("description");

				if ($description == "") {
					$DB->query(
						"SELECT `description` ".
						"FROM `employes` ".
						"WHERE `key` = " . addslashes($key) . ";"
					);
					while ($DB->next_record()) {
						$description = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes($DB->getField('description')))));
						$description = $DB->getField("description");
					}
				}

				$html .=
					"<label for=\"description\">Notes biographiques&nbsp;:</label>\n".
					// "<div id=\"hta_description_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_description = new HyperTextArea('etudes', 'description', '" . $description . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					"<textarea name=\"description\" id=\"description\" rows=\"15\">" . $description . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { \n".
					"		$('#description').htmlarea(); \n".
					"		$('.jHtmlArea, .jHtmlArea .ToolBar').css('width', '600px'); \n".
					"		$('.jHtmlArea').find('iframe').css('width', '600px'); \n".
					"		$('.jHtmlArea').find('iframe').css('height', '200px'); \n".
					"	}); \n".
					"</script>\n".
					"<br clear=\"all\" />\n".
					//"<span class=\"hta_chrcount\"><span id=\"description_chrcount\" class=\"description_chrcount\">0</span>&nbsp;/&nbsp;1750 caract&egrave;res maximum.</span>\n".
					//"<span style=\"float:right\";><input type=\"button\" class=\"hta_button\" value=\"Actualiser le nb de caract&egrave;res\" onclick=\"javascript: hta_description.updateChrCount();\" />&nbsp;&nbsp;<input type=\"button\" class=\"hta_button\" value=\"Tout effacer\" onclick=\"javascript: hta_description.clearText(); hta_description.updateChrCount();\" /></span>\n".
					"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					"<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";
				break;

			case "read":
				$html .=
					"<label>Notes biographiques&nbsp;:</label>\n".
					"<div class=\"hta_ro\"><!---HIDDEN_description /HIDDEN_description---></div>\n";
				break;
		}
		return $html;
	}

	function image($action) {
		global $DB, $BASEURL;
		$key = getorpost('key');
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"image\">La photo du notaire/avocat &agrave; activer sur le site&nbsp;:</label>\n".
					"<input type=\"file\" class=\"file\" name=\"image_up\" id=\"image_up\" /><br />\n".
					"<div class=\"notice\">Le format doit &ecirc;tre <strong>JPG</strong>, <strong>JPEG</strong>, <strong>GIF</strong> ou <strong>PNG</strong>.<br />Les dimensions doivent &ecirc;tre <strong>126</strong> par <strong>180</strong> pixels.</div>\n";
				break;

			case "modify":
				$image = "";
				$DB->query(
					"SELECT `image` ".
					"FROM `employes` ".
					"WHERE `key` = '" . addslashes($key) . "';"
				);
				while ($DB->next_record()) {
					$image = $DB->getField("image");
				}

				$html .=
					"<label for=\"image\">La photo du notaire/avocat &agrave; activer sur le site&nbsp;:</label>\n".
					"<input type=\"hidden\" name=\"image\" id=\"image\" value=\"\" />\n".
					"<input type=\"file\" class=\"file\" name=\"image_up\" id=\"image_up\" /><br />\n".
					"<div class=\"notice\">Le format doit &ecirc;tre <strong>JPG</strong>, <strong>JPEG</strong>, <strong>GIF</strong> ou <strong>PNG</strong>.<br />Les dimensions doivent &ecirc;tre <strong>126</strong> par <strong>180</strong> pixels.</div>\n";

				if ($image != "") {
					$html .=
						"<br />\n".
						"Image pr&eacute;sentement dans les registres&nbsp;:<br />\n".
						"<br />\n".
						"<img class=\"answer\" src=\"" . $BASEURL . "docs/employes_img/" . $image . "\" alt=\"photo du notaire\" /><br />\n".
						"<br />\n".
						"<input type=\"checkbox\" class=\"checkbox\" name=\"image_del\" id=\"image_del\" value=\"1\" /><span class=\"checkbox_label\">Supprimer l'image&nbsp;?</span>\n";
				} else {
					$html .=
						"<br />\n".
						"Aucune image n'est d&eacute;j&agrave; incluse.\n";
				}
				break;

			case "read":
				$html .=
					"<label>Photo du notaire/avocat&nbsp;:</label>\n";
				$DB->query(
					"SELECT `image` ".
					"FROM `employes` ".
					"WHERE `key` = '" . addslashes($key) . "';"
				);
				while ($DB->next_record()) {
					$image = $DB->getField("image");
				}
				if ($image != "") {
					$html .=
						"<img class=\"answer\" src=\"" . $BASEURL . "docs/employes_img/<!---HIDDEN_image /HIDDEN_image--->\" alt=\"photo du notaire\" />\n";
				} else {
					$html .=
						"<span class=\"answer\">(aucune)</span>\n";
				}
				break;
		}
		return $html;
	}

	function expertises($action) {
		global $DB, $BASEURL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<div class=\"entry\">\n".
					"	Expertises par secteur de droit&nbsp;:<br />\n".
					"	<br />\n".

					"	<label for=\"expertises_droit_affaires\">-&nbsp;<strong>Droit des affaires</strong>&nbsp;: </label>\n".
					"	<textarea rows=\"3\" cols=\"40\" id=\"expertises_droit_affaires\" name=\"expertises_droit_affaires\" onkeyup=\"javascript: showChrCount('expertises_droit_affaires', 'expertises_droit_affaires_chrcount');\"></textarea><br />\n".
					"	<span id=\"expertises_droit_affaires_chrcount\"></span> / 420 caract&egrave;res maximum.<br />\n".
					"	<br />\n".

					"	<label for=\"expertises_droit_personne\">-&nbsp;<strong>Droit de la personne</strong>&nbsp;: </label>\n".
					"	<textarea rows=\"3\" cols=\"40\" id=\"expertises_droit_personne\" name=\"expertises_droit_personne\" onkeyup=\"javascript: showChrCount('expertises_droit_personne', 'expertises_droit_personne_chrcount');\"></textarea><br />\n".
					"	<span id=\"expertises_droit_personne_chrcount\"></span> / 420 caract&egrave;res maximum.<br />\n".
					"	<br />\n".

					"	<label for=\"expertises_droit_immobilier\">-&nbsp;<strong>Droit immobilier</strong>&nbsp;: </label>\n".
					"	<textarea rows=\"3\" cols=\"40\" id=\"expertises_droit_immobilier\" name=\"expertises_droit_immobilier\" onkeyup=\"javascript: showChrCount('expertises_droit_immobilier', 'expertises_droit_immobilier_chrcount');\"></textarea><br />\n".
					"	<span id=\"expertises_droit_immobilier_chrcount\"></span> / 420 caract&egrave;res maximum.<br />\n".
					"	<br />\n".

					"	<label for=\"expertises_expertises_sectorielles\">-&nbsp;<strong>Expertises sectorielles</strong>&nbsp;: </label>\n".
					"	<textarea rows=\"3\" cols=\"40\" id=\"expertises_sectorielles\" name=\"expertises_sectorielles\" onkeyup=\"javascript: showChrCount('expertises_sectorielles', 'expertises_sectorielles_chrcount');\"></textarea><br />\n".
					"	<span id=\"expertises_sectorielles_chrcount\"></span> / 420 caract&egrave;res maximum.<br />\n".
					"</div>\n";
				break;

			case "modify":
				$html .=
					"<div class=\"entry\">\n".
					"	Expertises par secteur de droit&nbsp;:<br />\n".
					"	<br />\n".

					"	<label for=\"expertises_droit_affaires\">-&nbsp;<strong>Droit des affaires</strong>&nbsp;: </label>\n".
					"	<textarea rows=\"3\" cols=\"40\" id=\"expertises_droit_affaires\" name=\"expertises_droit_affaires\" onkeyup=\"javascript: showChrCount('expertises_droit_affaires', 'expertises_droit_affaires_chrcount');\"></textarea><br />\n".
					"	<span id=\"expertises_droit_affaires_chrcount\"></span> / 420 caract&egrave;res maximum.<br />\n".
					"	<br />\n".

					"	<label for=\"expertises_droit_personne\">-&nbsp;<strong>Droit de la personne</strong>&nbsp;: </label>\n".
					"	<textarea rows=\"3\" cols=\"40\" id=\"expertises_droit_personne\" name=\"expertises_droit_personne\" onkeyup=\"javascript: showChrCount('expertises_droit_personne', 'expertises_droit_personne_chrcount');\"></textarea><br />\n".
					"	<span id=\"expertises_droit_personne_chrcount\"></span> / 420 caract&egrave;res maximum.<br />\n".
					"	<br />\n".

					"	<label for=\"expertises_droit_immobilier\">-&nbsp;<strong>Droit immobilier</strong>&nbsp;: </label>\n".
					"	<textarea rows=\"3\" cols=\"40\" id=\"expertises_droit_immobilier\" name=\"expertises_droit_immobilier\" onkeyup=\"javascript: showChrCount('expertises_droit_immobilier', 'expertises_droit_immobilier_chrcount');\"></textarea><br />\n".
					"	<span id=\"expertises_droit_immobilier_chrcount\"></span> / 420 caract&egrave;res maximum.<br />\n".
					"	<br />\n".

					"	<label for=\"expertises_expertises_sectorielles\">-&nbsp;<strong>Expertises sectorielles</strong>&nbsp;: </label>\n".
					"	<textarea rows=\"3\" cols=\"40\" id=\"expertises_sectorielles\" name=\"expertises_sectorielles\" onkeyup=\"javascript: showChrCount('expertises_sectorielles', 'expertises_sectorielles_chrcount');\"></textarea><br />\n".
					"	<span id=\"expertises_sectorielles_chrcount\"></span> / 420 caract&egrave;res maximum.<br />\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"entry\">\n".
					"	Expertises par secteur de droit&nbsp;:<br />\n".
					"	<br />\n".

					"	<label for=\"expertises_droit_affaires\">-&nbsp;Droit des affaires&nbsp;: </label>\n".
					"	<span class=\"answer\"><!---HIDDEN_expertises_droit_affaires /HIDDEN_expertises_droit_affaires---></span><br />\n".
					"	<br />\n".

					"	<label for=\"expertises_droit_personne\">-&nbsp;Droit de la personne&nbsp;: </label>\n".
					"	<span class=\"answer\"><!---HIDDEN_expertises_droit_personne /HIDDEN_expertises_droit_personne---></span><br />\n".
					"	<br />\n".

					"	<label for=\"expertises_droit_immobilier\">-&nbsp;Droit immobilier&nbsp;: </label>\n".
					"	<span class=\"answer\"><!---HIDDEN_expertises_droit_immobilier /HIDDEN_expertises_droit_immobilier---></span><br />\n".
					"	<br />\n".

					"	<label for=\"expertises_expertises_sectorielles\">-&nbsp;Expertises sectorielles&nbsp;: </label>\n".
					"	<span class=\"answer\"><!---HIDDEN_expertises_sectorielles /HIDDEN_expertises_sectorielles---></span><br />\n".
					"</div>\n";
				break;
		}
		return $html;
	}

	/*
	function expertises_droit_des_affaires($action) {
		global $DB;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"expertises_droit_des_affaires\">Les sp&eacute;cialisations du notaire en droit des affaires&nbsp;: </label>\n".
					"<select id=\"expertises_droit_des_affaires\" name=\"expertises_droit_des_affaires[]\" multiple=\"multiple\" size=\"12\" >";

				$DB->query(
					"SELECT `key`, `expertise` ".
					"FROM `expertises` ".
					"WHERE `domaines_daffaires_key` = '2' ".
					"ORDER BY `expertise` ASC"
				);
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"expertises_droit_des_affaires\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("expertise") . "</option>\n";
				}

				$html .=
					"</select>";
				break;

			case "modify":
				$html =
					"<label for=\"expertises_droit_des_affaires\">Les sp&eacute;cialisations du notaire en droit des affaires&nbsp;: </label>\n".
					"<select id=\"expertises_droit_des_affaires\" name=\"expertises_droit_des_affaires[]\" multiple=\"multiple\" size=\"12\" disabled=\"disabled\">";

				$DB->query(
					"SELECT `key`, `expertise` ".
					"FROM `expertises` ".
					"WHERE `domaines_daffaires_key` = '1' ".
					"ORDER BY `expertise` ASC"
				);
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"expertises_droit_des_affaires\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("expertise") . "</option>\n";
				}

				$html .=
					"</select>";
				break;

			case "read":
				$html .=
					"<label>Les sp&eacute;cialisations du notaire en droit des affaires&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_expertises_droit_des_affaires /HIDDEN_expertises_droit_des_affaires---></span>\n";
				break;
		}
		return $html;
	}

	function expertises_droit_des_personnes($action) {
		global $DB;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"expertises_droit_des_personnes\">Les sp&eacute;cialisations du notaire en droit des personnes&nbsp;: </label>\n".
					"<select id=\"expertises_droit_des_personnes\" name=\"expertises_droit_des_personnes[]\" multiple=\"multiple\" size=\"12\" disabled=\"disabled\">";

				$DB->query(
					"SELECT `key`, `expertise` ".
					"FROM `expertises` ".
					"WHERE `domaines_daffaires_key` = '2' ".
					"ORDER BY `expertise` ASC"
				);
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"expertises_droit_des_personnes\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("expertise") . "</option>\n";
				}

				$html .=
					"</select>";
				break;

			case "modify":
				$html =
					"<label for=\"expertises_droit_des_personnes\">Les sp&eacute;cialisations du notaire en droit des personnes&nbsp;: </label>\n".
					"<select id=\"expertises_droit_des_personnes\" name=\"expertises_droit_des_personnes[]\" multiple=\"multiple\" size=\"12\" disabled=\"disabled\">";

				$DB->query(
					"SELECT `key`, `expertise` ".
					"FROM `expertises` ".
					"WHERE `domaines_daffaires_key` = '2' ".
					"ORDER BY `expertise` ASC"
				);
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"expertises_droit_des_personnes\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("expertise") . "</option>\n";
				}

				$html .=
					"</select>";
				break;

			case "read":
				$html .=
					"<label>Les sp&eacute;cialisations du notaire en droit des personnes&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_expertises_droit_des_personnes /HIDDEN_expertises_droit_des_personnes---></span>\n";
				break;
		}
		return $html;
	}

	function expertises_droit_immobilier($action) {
		global $DB;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"expertises_droit_immobilier\">Les sp&eacute;cialisations du notaire en droit immobilier&nbsp;: </label>\n".
					"<select id=\"expertises_droit_immobilier\" name=\"expertises_droit_immobilier[]\" multiple=\"multiple\" size=\"12\" disabled=\"disabled\">";

				$DB->query(
					"SELECT `key`, `expertise` ".
					"FROM `expertises` ".
					"WHERE `domaines_daffaires_key` = '3' ".
					"ORDER BY `expertise` ASC"
				);
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"expertises_droit_immobilier\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("expertise") . "</option>\n";
				}

				$html .=
					"</select>";
				break;

			case "modify":
				$html =
					"<label for=\"expertises_droit_immobilier\">Les sp&eacute;cialisations du notaire en droit immobilier&nbsp;: </label>\n".
					"<select id=\"expertises_droit_immobilier\" name=\"expertises_droit_immobilier[]\" multiple=\"multiple\" size=\"12\" disabled=\"disabled\">";

				$DB->query(
					"SELECT `key`, `expertise` ".
					"FROM `expertises` ".
					"WHERE `domaines_daffaires_key` = '3' ".
					"ORDER BY `expertise` ASC"
				);
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"expertises_droit_immobilier\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("expertise") . "</option>\n";
				}

				$html .=
					"</select>";
				break;

			case "read":
				$html .=
					"<label>Les sp&eacute;cialisations du notaire en droit immobilier&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_expertises_droit_immobilier /HIDDEN_expertises_droit_immobilier---></span>\n";
				break;
		}
		return $html;
	}

	function expertises_autre($action) {
		global $DB;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"expertises_autre\">Autres sp&eacute;cialisations du notaire&nbsp;: </label>\n".
					"<select id=\"expertises_autre\" name=\"expertises_autre[]\" multiple=\"multiple\" size=\"12\" disabled=\"disabled\">";

				$DB->query(
					"SELECT `key`, `expertise` ".
					"FROM `expertises` ".
					"WHERE `domaines_daffaires_key` = '3' ".
					"ORDER BY `expertise` ASC"
				);
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"expertises_autre\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("expertise") . "</option>\n";
				}

				$html .=
					"</select>";
				break;

			case "modify":
				$html =
					"<label for=\"expertises_autre\">Autres sp&eacute;cialisations du notaire&nbsp;: </label>\n".
					"<select id=\"expertises_autre\" name=\"expertises_autre[]\" multiple=\"multiple\" size=\"12\" disabled=\"disabled\">";

				$DB->query(
					"SELECT `key`, `expertise` ".
					"FROM `expertises` ".
					"WHERE `domaines_daffaires_key` = '3' ".
					"ORDER BY `expertise` ASC"
				);
				while ($DB->next_record()) {
					$html .=
						"<option selectname=\"expertises_autre\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("expertise") . "</option>\n";
				}

				$html .=
					"</select>";
				break;

			case "read":
				$html .=
					"<label>Autres sp&eacute;cialisations du notaire&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_expertises_autre /HIDDEN_expertises_autre---></span>\n";
				break;
		}
		return $html;
	}

	function expertises_sectorielles_desc($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"expertises_sectorielles_desc\">Les expertises sectorielles&nbsp;: </label>\n".
					"<textarea rows=\"3\" cols=\"40\" id=\"expertises_sectorielles_desc\" name=\"expertises_sectorielles_desc\" disabled=\"disabled\"></textarea>\n";
				break;
			case "modify":
				$html .=
					"<label for=\"expertises_sectorielles_desc\">Les expertises sectorielles&nbsp;: </label>\n".
					"<textarea rows=\"3\" cols=\"40\" id=\"expertises_sectorielles_desc\" name=\"expertises_sectorielles_desc\" disabled=\"disabled\"></textarea>\n";
				break;
			case "read":
				$html .=
					"<label>Les expertises sectorielles&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_expertises_sectorielles_desc /HIDDEN_expertises_sectorielles_desc---></span>\n";
				break;
		}
		return $html;
	}*/

?>
