<?PHP
	global $DB;

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL;
		
		$_FORM =
			form_header($action).
			"<div id=\"congres\">\n".

			"		<a name=\"date\"></a><label for=\"date\">Date de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_debut($action) . "\n".
			"		</div>\n".

			"		<a name=\"date_limite\"></a><label for=\"date_limite\">Date limite&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_limite($action) . "\n".
			"		</div>\n".

			"		<a name=\"lieu\"></a><label for=\"lieu\">Lieu&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . lieu($action) . "\n".
			"		</div>\n".

			"		<ul class=\"sujets\">\n".
			"		<li><a name=\"sujet1\"></a><label for=\"sujet1\">Sujet 1&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . sujet1($action) . "\n".
			"		</div></li>\n".

			"		<li><a name=\"sujet2\"></a><label for=\"sujet2\">Sujet 2&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . sujet2($action) . "\n".
			"		</div></li>\n".

			"		<li><a name=\"sujet3\"></a><label for=\"sujet3\">Sujet 3&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . sujet3($action) . "\n".
			"		</div></li>\n".

			"		<li><a name=\"sujet4\"></a><label for=\"sujet4\">Sujet 4&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . sujet4($action) . "\n".
			"		</div></li>\n".
			"		</ul>\n".
			"		<br clear=\"all\" />\n".

			"		<a name=\"prix_notaire\"></a><label for=\"prix_notaire\">Prix notaire&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . prix_notaire($action) . "\n".
			"		</div>\n".

			"		<a name=\"prix_collaboratrice\"></a><label for=\"prix_collaboratrice\">Prix collaboratrice&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . prix_collaboratrice($action) . "\n".
			"		</div>\n".

			"		<a name=\"note\"></a><label for=\"note\">Note importante&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . note($action) . "\n".
			"		</div>\n".

			"		<ul class=\"activites\">\n";

		for ($i = 1; $i < 21; $i++) {
			$_FORM .=
				"		<li><a name=\"activite" . $i . "\"></a><label for=\"activite" . $i . "\">Activit&eacute; #" . $i . "&nbsp;: </label>\n".
				"		<div class=\"field\">\n".
				"			" . activite($i, $action) . "\n".
				"		</div></li>\n".
				"		<li><a name=\"activite" . $i . "_prix\"></a><label for=\"activite" . $i . "_prix\">Prix de l'activit&eacute; #" . $i . "&nbsp;: </label>\n".
				"		<div class=\"field\">\n".
				"			" . activite_prix($i, $action) . "\n".
				"		</div></li>\n".
				"		<li><a name=\"activite" . $i . "_jour\"></a><label for=\"activite" . $i . "_jour\">Libellé du jour de l'activit&eacute; #" . $i . "&nbsp;: </label>\n".
				"		<div class=\"field\">\n".
				"			" . activite_jour($i, $action) . "\n".
				"		</div></li>\n";
		}

		$_FORM .=
			"		</ul>\n".
			"		<br clear=\"all\" />\n".

			"		<a name=\"hebergement_limite\"></a><label for=\"hebergement_limite\">Date limite pour l'h&eacute;bergement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . hebergement_limite($action) . "\n".
			"		</div>\n".

			"		<a name=\"hebergement_note\"></a><label for=\"hebergement_note\">Note pour l'h&eacute;bergement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . hebergement_note($action) . "\n".
			"		</div>\n".

			"		<a name=\"hebergement_coordonnees\"></a><label for=\"hebergement_coordonnees\">Coordonn&eacute;es de l'h&eacute;bergement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . hebergement_coordonnees($action) . "\n".
			"		</div>\n".

			"		<a name=\"hebergement_details\"></a><label for=\"hebergement_details\">D&eacute;tails sur la r&eacute;servation de l'h&eacute;bergement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . hebergement_details($action) . "\n".
			"		</div>\n".

			"		<a name=\"hebergement_occupation_simple_price\"></a><label for=\"hebergement_occupation_simple_price\">Prix occupation simple de l'h&eacute;bergement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . hebergement_occupation_simple_price($action) . "\n".
			"		</div>\n".

			"		<a name=\"hebergement_occupation_double_price\"></a><label for=\"hebergement_occupation_double_price\">Prix occupation double de l'h&eacute;bergement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . hebergement_occupation_double_price($action) . "\n".
			"		</div>\n".

			"		<label for=\"description\">Description de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . description($action) . "\n".
			"		</div>\n".

			"		<label for=\"actif\">Actif</label>\n".
			"		<div class=\"field\">\n".
			"			" . actif($action) . "\n".
			"		</div>\n".

			form_footer($action);
			
		return $_FORM;
	}








	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html = 
			"<script type=\"text/javascript\" src=\"/skin/js/tinymce/tinymce.min.js\"></script>\n".
			"<script type=\"text/javascript\">\n".
			"	tinymce.init({\n".
    		"		selector: \"textarea\",\n".
    		"		height: 400\n".
 			"	});\n".
			"</script>\n";

		switch ($action) {
			case "add":					
				$html .=
					"<form name=\"evenements\" action=\"congres.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n".
					"	<input type=\"hidden\" name=\"id\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "modify":
				$html .=	
					"<form name=\"evenements\" action=\"congres.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"id\" value=\"\" />\n".
					"	<input type=\"hidden\" name=\"key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "read":
				$html .=
					"	<div class=\"readform\">\n".
					"		<a href=\"/evenements/alerte.php?table=congres&id=" . $_POST["id"] . "\" target=\"_blank\"><strong>Alerte courriel</strong></a><br />\n".
					"		<a href=\"/evenements/inscription.php?table=congres&id=" . $_POST["id"] . "\" target=\"_blank\"><strong>Inscriptions</strong></a><br />\n".
					"		<a href=\"/evenements/inscription.php?table=congres&id=" . $_POST["id"] . "&excel=1\" target=\"_blank\"><strong>Inscriptions Excel</strong></a><br /><br />\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "modify":
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "read":
				$html =
					"	</div>\n";
				break;
		}
		return $html;
	}


	function date_limite($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"date_limite\" id=\"date_limite\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date_limite\" id=\"date_limite\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date_limite /HIDDEN_date_limite---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function actif($action){
		$html="";
		switch ($action) {	
			case "add":
				$html = 
					"<select name=\"actif\">\n".
					"	<option id=\"actif\" value=\"\"></option>\n".
					"	<option id=\"actif\" value=\"1\">Oui</option>\n".
					"	<option id=\"actif\" value=\"0\">Non</option>\n".
					"</select>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html =
					"<select name=\"actif\">\n".
					"	<option id=\"actif\" value=\"\"></option>\n".
					"	<option id=\"actif\" value=\"1\">Oui</option>\n".
					"	<option id=\"actif\" value=\"0\">Non</option>\n".
					"</select>\n".
					"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_actif /HIDDEN_actif---></span><br/><br/>\n";
				break;
		}
		return $html;
		
	}

	function date_debut($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"date\" id=\"date\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date\" id=\"date\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date /HIDDEN_date---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function lieu($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"lieu\" id=\"lieu\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"lieu\" id=\"lieu\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_lieu /HIDDEN_lieu---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function sujet1($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"sujet1\" id=\"sujet1\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"sujet1\" id=\"sujet1\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_sujet1 /HIDDEN_sujet1---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function sujet2($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"sujet2\" id=\"sujet2\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"sujet2\" id=\"sujet2\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_sujet2 /HIDDEN_sujet2---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function sujet3($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"sujet3\" id=\"sujet3\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"sujet3\" id=\"sujet3\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_sujet3 /HIDDEN_sujet3---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function sujet4($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"sujet4\" id=\"sujet4\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"sujet4\" id=\"sujet4\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_sujet4 /HIDDEN_sujet4---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function prix_notaire($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"prix_notaire\" id=\"prix_notaire\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"prix_notaire\" id=\"prix_notaire\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_prix_notaire /HIDDEN_prix_notaire---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function prix_collaboratrice($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"prix_collaboratrice\" id=\"prix_collaboratrice\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"prix_collaboratrice\" id=\"prix_collaboratrice\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_prix_collaboratrice /HIDDEN_prix_collaboratrice---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function note($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = 
					"<textarea rows=\"3\" cols=\"40\" name=\"note\"></textarea>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"note\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_note /HIDDEN_note---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function activite($index, $action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"activite" . $index . "\" id=\"activite" . $index . "\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"activite" . $index . "\" id=\"activite" . $index . "\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_activite" . $index . " /HIDDEN_activite" . $index . "---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function activite_prix($index, $action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"activite" . $index . "_prix\" id=\"activite" . $index . "_prix\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"activite" . $index . "_prix\" id=\"activite" . $index . "_prix\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_activite" . $index . "_prix /HIDDEN_activite" . $index . "_prix---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function activite_jour($index, $action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"activite" . $index . "_jour\" id=\"activite" . $index . "_jour\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"activite" . $index . "_jour\" id=\"activite" . $index . "_jour\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_activite" . $index . "_jour /HIDDEN_activite" . $index . "_jour---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function hebergement_limite($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"hebergement_limite\" id=\"hebergement_limite\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"hebergement_limite\" id=\"hebergement_limite\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_hebergement_limite /HIDDEN_hebergement_limite---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

  	function hebergement_note($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = 
					"<textarea rows=\"3\" cols=\"40\" name=\"hebergement_note\"></textarea>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"hebergement_note\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_hebergement_note /HIDDEN_hebergement_note---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function hebergement_coordonnees($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = 
					"<textarea rows=\"3\" cols=\"40\" name=\"hebergement_coordonnees\"></textarea>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"hebergement_coordonnees\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_hebergement_coordonnees /HIDDEN_hebergement_coordonnees---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

  	function hebergement_details($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"hebergement_details\" id=\"hebergement_details\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"hebergement_details\" id=\"hebergement_details\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_hebergement_details /HIDDEN_hebergement_details---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function hebergement_occupation_simple_price($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"hebergement_occupation_simple_price\" id=\"hebergement_occupation_simple_price\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"hebergement_occupation_simple_price\" id=\"hebergement_occupation_simple_price\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_hebergement_occupation_simple_price /HIDDEN_hebergement_occupation_simple_price---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function hebergement_occupation_double_price($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"hebergement_occupation_double_price\" id=\"hebergement_occupation_double_price\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"hebergement_occupation_double_price\" id=\"hebergement_occupation_double_price\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_hebergement_occupation_double_price /HIDDEN_hebergement_occupation_double_price---></span><br/><br/>\n";
				break;
		}
		return $html;
	}








	function description($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = 
					"<textarea rows=\"8\" cols=\"40\" name=\"description\">\n".
					"	<div style=\"width: 550px;\">\n".
					"		<img src=\"/skin/img/logo.png\" alt=\"PME Inter\" style=\"width: 80px; height: auto; float: left; padding-right: 10px; padding-bottom: 20px;\" />\n".
					"		<div style=\"padding: 10px 50px; text-align: center; width: 80%; height: 125px; color: #CC6633; margin-left: 85px; font-size: 22px; line-height: 35px; font-weight: normal;\">CONGR&Egrave;S " . date("Y") . " PME INTER NOTAIRES<br /><small style=\"color: #253767;\">Formulaire d'inscription notaires, collaboratrices et accompagnateurs<br />DATES DE L'EVENEMENT &agrave; [LIEU]</small></div>\n".
					"		<br />\n".
					"		<div>\n".
					"			<table class=\"inscriptions_notaires\" style=\"width: 100%;\">\n".
					"				<tr>\n".
					"					<td>\n".
					"						<h1 style=\"color: #253767; font-size: 15px;\">INSCRIPTION NOTAIRES</h1>\n".
					"					</td>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: center;\">&nbsp;</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF;\">Nom des notaires</p>\n".
					"					</td>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF;\">Allergie alimentaire (sp&eacute;cifier)</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr><td colspan=\"2\">[INSCRIPTIONS_NOTAIRES]</td></tr>\n".
					"			</table>\n".
					"		</div>\n".
					"		<br /><br />\n".
					"		<div>\n".
					"			<table class=\"inscriptions_collaboratrices\" style=\"width: 100%;\">\n".
					"				<tr>\n".
					"					<td colspan=\"2\">\n".
					"						<h1 style=\"color: #253767; font-size: 15px;\">INSCRIPTION COLLABORATRICES</h1>\n".
					"					</td>\n".
					"					<td colspan=\"4\" style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: center;\">Veuillez cocher votre choix</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td colspan=\"2\">\n".
					"						<p>(Cocher le choix pour chaque participante)</p>\n".
					"					</td>\n".
					"					<td colspan=\"2\" style=\"background-color: #DD8855;\">\n".
					"						<p style=\"color: #253767; text-align: center;\">(8 h 30 &agrave; 10 h)</p>\n".
					"					</td>\n".
					"					<td colspan=\"2\" style=\"background-color: #DD8855;\">\n".
					"						<p style=\"color: #253767; text-align: center;\">(10 h 15 &agrave; 11 h 15)</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF;\">Nom des collaboratrices</p>\n".
					"					</td>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF;\">Allergie alimentaire (sp&eacute;cifier)</p>\n".
					"					</td>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: center;\">[SUJET1]</p>\n".
					"					</td>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: center;\">[SUJET2]</p>\n".
					"					</td>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: center;\">[SUJET3]</p>\n".
					"					</td>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: center;\">[SUJET4]</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr><td colspan=\"6\">[INSCRIPTIONS_COLLABORATRICES]</td></tr>\n".
					"			</table>\n".
					"		</div>\n".

					"		<br /><br />\n".
					"		<div>\n".
					"			<h1 style=\"color: #253767; font-size: 15px;\">CO&ucirc;T D'INSCRIPTION NOTAIRES - COLLABORATRICES</h1>\n".
					"			<table style=\"width: 100%;\">\n".
					"				<tr>\n".
					"					<td style=\"background-color: #CC6633;\" colspan=\"3\">\n".
					"						<p style=\"color: #FFF;\">LES CO&ucirc;T D'INSCRIPTION ET DE TRANSPORT POUR LES NOTAIRES ET LES COLLABORATRICES</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td>\n".
					"						<p>Nombre d'inscriptions de notaires :</p>\n".
					"					</td>\n".
					"					<td>\n".
					"						<p style=\"text-align: center;\">@ [PRIX_NOTAIRE] $</p>\n".
					"					</td>\n".
					"					<td>\n".
					"						<p style=\"text-align: right;\"><span></span>$</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr style=\"background-color: #DD8855;\">\n".
					"					<td>\n".
					"						<p>Nombre d'inscriptions de collaboratrices :</p>\n".
					"					</td>\n".
					"					<td>\n".
					"						<p style=\"text-align: center;\">@ [PRIX_COLLABORATRICE] $</p>\n".
					"					</td>\n".
					"					<td>\n".
					"						<p style=\"text-align: right;\"><span></span>$</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td colspan=\"2\" style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: right;\">Sous-total :</p>\n".
					"					</td>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: right;\"><span></span>$</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td colspan=\"2\">\n".
					"						<p style=\"padding-left: 50px;\">+ T.P.S. 5% (No: 891149015)</p>\n".
					"					</td>\n".
					"					<td>\n".
					"						<p style=\"text-align: right;\"><span></span>$</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr style=\"background-color: #DD8855;\">\n".
					"					<td colspan=\"2\">\n".
					"						<p style=\"padding-left: 50px;\">+ T.V.Q. 9,975% (No: 1086723880)</p>\n".
					"					</td>\n".
					"					<td>\n".
					"						<p style=\"text-align: right;\"><span></span>$</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td colspan=\"2\">\n".
					"						<p style=\"text-align: right;\">TOTAL :</p>\n".
					"					</td>\n".
					"					<td>\n".
					"						<p style=\"text-align: right;\"><span></span>$</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td colspan=\"3\" style=\"background-color: #DD8855;\">\n".
					"						<p style=\"text-align: center;\">[NOTE]</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td colspan=\"3\" style=\"background-color: #CC6633;\">\n".
					"						<p style=\"text-align: center; color: #FFF;\">Les frais d'inscription incluent : Les formations, les d&icirc;ners, les soupers et les activit&eacute;s.</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"			</table>\n".
					"		</div>\n".





					"		<br /><br />\n".
					"		<div>\n".
					"			<h1 style=\"color: #253767; font-size: 15px;\">CO&ucirc;T D'INSCRIPTION ACCOMPAGNATEURS</h1>\n".
					"			<table style=\"width: 100%;\">\n".
					"				<tr>\n".
					"					<td style=\"background-color: #CC6633;\" colspan=\"3\">\n".
					"						<p style=\"color: #FFF;\">LES DES ACTIVIT&Eacute;S (Veuillez identifier le num&eacute;ro de la ou des activit&eacute;s s&eacute;lectionn&eacute;es pour chaque accompagneteur)</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td colspan=\"3\">\n".
					"						[LISTE_ACTIVITES]\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td colspan=\"2\" style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: right;\">Sous-total :</p>\n".
					"					</td>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: right;\"><span></span>$</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td colspan=\"3\">\n".
					"						<br />\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td style=\"background-color: #CC6633;\" colspan=\"3\">\n".
					"						<p style=\"color: #FFF;\">LES CO&Ucirc;TS D'INSCRIPTION ET DE TRANSPORT POUR LES ACCOMPAGNATEURS</h1>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF;\">Nom des accompagneteurs</p>\n".
					"					</td>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: center;\">Num&eacute;ro des activit&eacute;s s&eacute;lectionn&eacute;es</p>\n".
					"					</td>\n".
					"					<td style=\"background-color: #CC6633;\">\n".
					"						<p style=\"color: #FFF; text-align: center;\">Co&ucirc;t des activit&eacute;s s&eacute;lectionn&eacute;es</p>\n".
					"					</td>\n".
					"				</tr>\n".

					"				<tr>\n".
					"					<td colspan=\"3\">\n".
					"						[INSCRIPTIONS_ACTIVITES]\n".
					"					</td>\n".
					"				</tr>\n".

					"				<tr>\n".
					"					<td colspan=\"2\">\n".
					"						<p style=\"padding-left: 50px;\">+ T.P.S. 5% (No: 891149015)</p>\n".
					"					</td>\n".
					"					<td>\n".
					"						<p style=\"text-align: right;\"><span></span>$</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr style=\"background-color: #DD8855;\">\n".
					"					<td colspan=\"2\">\n".
					"						<p style=\"padding-left: 50px;\">+ T.V.Q. 9,975% (No: 1086723880)</p>\n".
					"					</td>\n".
					"					<td>\n".
					"						<p style=\"text-align: right;\"><span></span>$</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td colspan=\"2\">\n".
					"						<p style=\"text-align: right;\">TOTAL :</p>\n".
					"					</td>\n".
					"					<td>\n".
					"						<p style=\"text-align: right;\"><span></span>$</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td colspan=\"3\" style=\"background-color: #DD8855;\">\n".
					"						<p style=\"text-align: center;\">[NOTE]</p>\n".
					"					</td>\n".
					"				</tr>\n".
					
					"			</table>\n".
					"		</div>\n".



					"		<br /><br />\n".
					"		<div>\n".
					"			<h1 style=\"color: #253767; font-size: 15px;\">CO&ucirc;T H&Eacute;BERGEMENT [HEBERGEMENT_LIMITE]</h1>\n".
					"			<table style=\"width: 100%;\">\n".
					"				<tr>\n".
					"					<td style=\"background-color: #CC6633;\" colspan=\"3\">\n".
					"						<p style=\"color: #FFF;\">[HEBERGEMENT_NOTE]</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td rowspan=\"3\">\n".
					"						<p>[HEBERGEMENT_COORDONNEES]</p>\n".
					"					</td>\n".
					"					<td colspan=\"2\">\n".
					"						<p style=\"text-align: center;\">[HEBERGEMENT_DETAILS]</p>\n".
					"					</td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td><p style=\"text-align: center;\">Occupation simple</p></td>\n".
					"					<td><p style=\"text-align: center;\">Occupation double</p></td>\n".
					"				</tr>\n".
					"				<tr>\n".
					"					<td><p style=\"text-align: center;\">[HEBERGEMENT_OCCUPATION_SIMPLE]</p></td>\n".
					"					<td><p style=\"text-align: center;\">[HEBERGEMENT_OCCUPATION_DOUBLE]</p></td>\n".
					"				</tr>\n".
					
					"			</table>\n".
					"		</div>\n".


					"		<div>\n".
					"			<p style=\"text-align: center;\">Veuillez nous retourner votre formulaire avant le [DATE_LIMITE] par t&eacute;l&eacute;copieur au num&eacute;ro 514.874.9618 ou par courriel &agrave; <a href=\"mailto:info@pmeinter.com\">info@pmeinter.com</a> et votre paiement &agrave; l'ordre de : PME INTER Notaires par la poste au 100, boulevard Alexis-Nihon, bureau 985, Montr&eacute;al (Qu&eacute;bec) H4M 2P5</p>\n".

					"		</div>\n".


					
					"	</div>".
					"</textarea>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"description\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_description /HIDDEN_description---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

?>