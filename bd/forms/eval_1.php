<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "1").

			"	<div class=\"entry\">\n".
			"		<label for=\"etude_nom\">Nom de l'&eacute;tude&nbsp;:</label>\n".
			"		" . etudes_nom($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s0_date_visite\">Date de la visite&nbsp;:</label>\n".
			"		" . s0_date_visite($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".	
			"		<label for=\"s0_nom_agent_accompagnateur\">Nom de l'agent accompagnateur&nbsp;:</label>\n".
			"		" . s0_nom_agent_accompagnateur($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".	
			"		<label for=\"s1_nom_agent_liaison\">Nom de l'agent de liaison&nbsp;:</label>\n".
			"		" . s1_nom_agent_liaison($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"etudes_succursales_coordonnees\">Indiquer, s'il y a lieu, les changements dans les coordonn&eacute;es des succursales de l'&eacute;tude&nbsp;:</label>\n".
			"		" . etudes_succursales($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	function etudes_nom($action, $parameters){
		global $DB;
		$html = "";
		switch ($action) {
			case "modify":
				$DB->query(
					"SELECT `nom` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $parameters['etudes_key'] . "'"
				);
				while ($DB->next_record()) {
					$etudes_nom = $DB->getField("nom");
				}
				$html .=
					"<input type=\"text\" class=\"text readonly\" id=\"etudes_nom\" name=\"etudes_nom\" value=\"" . $etudes_nom . "\" readonly=\"readonly\" />\n";
				break;

			case "read":
				$DB->query(
					"SELECT `nom` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $parameters['etudes_key'] . "'"
				);
				while ($DB->next_record()) {
					$etudes_nom = $DB->getField("nom");
				}
				$html .=
					"<input type=\"text\" class=\"text\" id=\"etudes_nom\" name=\"etudes_nom\" value=\"" . $etudes_nom . "\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$DB->query(
							"SELECT `nom` ".
							"FROM `etudes` ".
							"WHERE `key` = '" . $compare_parameters['etudes_key'] . "'"
						);
						while ($DB->next_record()) {
							$etudes_nom = $DB->getField("nom");
						}
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"text\" class=\"text\" name=\"" . $compare_sid . "_etudes_nom\" value=\"" . $etudes_nom . "\" readonly=\"readonly\" />\n".
							"</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s0_date_visite($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text_short readonly\" id=\"s0_date_visite\" name=\"s0_date_visite\" value=\"" . $parameters['s0_date_visite'] . "\" readonly=\"readonly\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text_short\" id=\"s0_date_visite\" name=\"s0_date_visite\" value=\"" . $parameters['s0_date_visite'] . "\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"text\" class=\"text_short\" name=\"" . $compare_sid . "_s0_date_visite\" value=\"" . $compare_parameters['s0_date_visite'] . "\" readonly=\"readonly\" />\n".
							"</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s0_nom_agent_accompagnateur($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text readonly\" id=\"s0_nom_agent_accompagnateur\" name=\"s0_nom_agent_accompagnateur\" value=\"" . $parameters['s0_nom_agent_accompagnateur'] . "\" readonly=\"readonly\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text\" id=\"s0_nom_agent_accompagnateur\" name=\"s0_nom_agent_accompagnateur\" value=\"" . $parameters['s0_nom_agent_accompagnateur'] . "\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">".
							"<input type=\"text\" class=\"text\" name=\"" . $compare_sid . "_s0_nom_agent_accompagnateur\" value=\"" . $compare_parameters['s0_nom_agent_accompagnateur'] . "\" readonly=\"readonly\" />\n".
							"</div>";
					}
				}
			break;
		}
		return $html;
	}

	function s1_nom_agent_liaison($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text\" id=\"s1_nom_agent_liaison\" name=\"s1_nom_agent_liaison\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text\" id=\"s1_nom_agent_liaison\" name=\"s1_nom_agent_liaison\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">".
							"<input type=\"text\" class=\"text\" name=\"" . $compare_sid . "_s1_nom_agent_liaison\" value=\"\" readonly=\"readonly\" />\n".
							"</div>";
					}
				}
				break;
		}
		return $html;
	}


	function etudes_succursales($action, $parameters) {
		global $DB, $sid;
		$html = "";
		switch ($action) {
			case "modify":
				$html .= etudes_succursales_jscript();

				$coordonnees = array();
				$coordonnees_originales = array();

				// en 1er, on importe les valeurs contenues dans la table `etudes_succursales`
				$DB->query(
					"SELECT `key`, `adresse`, `ville`, `province`, `code_postal`, `telephone1`, `telephone2`, `telecopieur` ".
					"FROM `etudes_succursales` ".
					"WHERE `etudes_key` = '" . $parameters['etudes_key'] . "' AND `actif` = '1';"
				);
				while ($DB->next_record()) {
					$coordString = "Adresse : " . $DB->getField("adresse") . "\nVille : " . $DB->getField("ville") . "\nProvince : " . $DB->getField("province") . "\nCode Postal : " . $DB->getField("code_postal") . "\nTéléphone #1 : " . $DB->getField("telephone1") . "\nTéléphone #2 : " . $DB->getField("telephone2") . "\nTélécopieur : " . $DB->getField("telecopieur"); 
					$coordonnees_originales[$DB->getField("key")] = $coordString;
					$coordonnees[$DB->getField("key")] = $coordString;
					$source[$DB->getField("key")] = 1;
					$enable[$DB->getField("key")] = 0;
				}

				// en 2e, on importe les valeurs modifiées de la table `evaluations_etudes_succursales` en écrasant celles précédement importées
				$DB->query(
					"SELECT `etudes_succursales_key`, `coordonnees` ".
					"FROM `evaluations_etudes_succursales` ".
					"WHERE `evaluations_key` = '" . $parameters['key'] . "';"
				);
				while ($DB->next_record()) {
					$coordonnees[$DB->getField("etudes_succursales_key")] = $DB->getField("coordonnees");
					$source[$DB->getField("etudes_succursales_key")] = 2;
					$enable[$DB->getField("etudes_succursales_key")] = 0;
				}

				// en 3e, on importe les valeurs modifiées courantes de la variable de session en écrasant celles précédement importées si elles diffèrent
				if (is_array($parameters['etudes_succursales_key']) && count($parameters['etudes_succursales_key']) > 0) {
					foreach ($parameters['etudes_succursales_key'] as $etudes_succursales_key) {
						if (preg_replace("/(\n|\r|\t)/", "", $coordonnees[$etudes_succursales_key]) != preg_replace("/(\n|\r|\t)/", "", $parameters['etudes_succursales_coordonnees'][$etudes_succursales_key])) {
							$coordonnees[$etudes_succursales_key] = $parameters['etudes_succursales_coordonnees'][$etudes_succursales_key];
							$source[$etudes_succursales_key] = 3;
							$enable[$etudes_succursales_key] = 1;
						}
					}
				}

				if (is_array($coordonnees) && count($coordonnees) > 0) {
					foreach ($coordonnees as $etudes_succursales_key => $value) {
						$readonly = "readonly=\"readonly\"";
						$class = "class=\"readonly\"";
						if ($enable[$etudes_succursales_key]) {
							$readonly = "";
							$class = "";
						}
						
						switch ($source[$etudes_succursales_key]) {
							case 1:
								$info = "Aucune modification.";
								break;
							case 2:
								$info = "Modifi&eacute; pr&eacute;c&eacute;dement par l'&eacute;valuateur&nbsp:";
								break;
							case 3:
								$info = "En cours de modification...";
								break;
						}
	
						$html .=
							"<div class=\"etudes_succursales_coordonnees_entry\">\n".
							"	<input type=\"hidden\" name=\"etudes_succursales_key[" . $etudes_succursales_key . "]\" id=\"etudes_succursales_key[" . $etudes_succursales_key . "]\" value=\"" . $etudes_succursales_key . "\" " . $readonly . "/>\n".
							"	<table>\n".
							"		<tr>\n".
							"			<td width=\"20%\" rowspan=\"2\" style=\"text-align: center; border-top: 0px;\"><a href=\"javascript: etudes_succursales_modifier('" . $etudes_succursales_key . "')\">Modifier</a><br /><br /><a href=\"javascript: etudes_succursales_annuler('" . $etudes_succursales_key . "')\">Annuler</a></td>\n".
							"			<th width=\"40%\" style=\"text-align: left; border-bottom: 0px\">Coordonn&eacute;ees des registres&nbsp;:</td>\n".
							"			<th width=\"40%\" style=\"text-align: left; border-bottom: 0px\"><span id=\"etudes_succursales_info[" . $etudes_succursales_key . "]\">" . $info . "</span></td>\n".
							"		</tr>\n".
							"		<tr>\n".
							"			<td width=\"40%\" style=\"text-align: center; border-top: 0px;\"><textarea class=\"readonly\" id=\"etudes_succursales_coordonnees_originales[" . $etudes_succursales_key . "]\" name=\"etudes_succursales_coordonnees_originales[" . $etudes_succursales_key . "]\" rows=\"8\" readonly=\"readonly\">" . $coordonnees_originales[$etudes_succursales_key] . "</textarea></td>\n".
							"			<td width=\"40%\" style=\"text-align: center; border-top: 0px;\"><textarea " . $class . " id=\"etudes_succursales_coordonnees[" . $etudes_succursales_key . "]\" name=\"etudes_succursales_coordonnees[" . $etudes_succursales_key . "]\" rows=\"8\" " . $readonly . " onkeyup=\"javascript: etudes_succursales_onTextBox('" . $etudes_succursales_key . "');\">" . $coordonnees[$etudes_succursales_key] . "</textarea></td>\n".
							"		</tr>\n". 
							"	</table>\n".
							"</div>\n".
							"<br />\n";
					}
					
					$html = substr($html, 0, strlen($html) - 8);
	
					$html .=
						"<span class=\"hint\">* Les changements apport&eacute;es ne modifieront pas les valeurs originales des registres du r&eacute;seau.</span>\n";

				} else {
					$html .=
						"<br /><span class=\"warning\">L'&eacute;tude ne contient aucune succursale.</span><br />\n";
				}
				break;

			case "read":
				// en 1er, on importe les valeurs contenues dans la table `etudes_succursales`
				$DB->query(
					"SELECT `key`, `adresse`, `ville`, `province`, `code_postal`, `telephone1`, `telephone2`, `telecopieur` ".
					"FROM `etudes_succursales` ".
					"WHERE `etudes_key` = '" . $parameters['etudes_key'] . "' AND `actif` = '1';"
				);
				while ($DB->next_record()) {
					$coordString = "Adresse : " . $DB->getField("adresse") . "\nVille : " . $DB->getField("ville") . "\nProvince : " . $DB->getField("province") . "\nCode Postal : " . $DB->getField("code_postal") . "\nTéléphone #1 : " . $DB->getField("telephone1") . "\nTéléphone #2 : " . $DB->getField("telephone2") . "\nTélécopieur : " . $DB->getField("telecopieur"); 
					$coordonnees_originales[$DB->getField("key")] = $coordString;
					$coordonnees[$DB->getField("key")] = $coordString;
				}

				// en 2e, on importe les valeurs modifiées de la table `evaluations_etudes_succursales` en écrasant celles précédement importées
				$DB->query(
					"SELECT `etudes_succursales_key`, `coordonnees` ".
					"FROM `evaluations_etudes_succursales` ".
					"WHERE `evaluations_key` = '" . $parameters['key'] . "';"
				);
				while ($DB->next_record()) {
					$coordonnees[$DB->getField("etudes_succursales_key")] = $DB->getField("coordonnees");
				}

				if (is_array($coordonnees) && count($coordonnees) > 0) {
					foreach ($coordonnees as $etudes_succursales_key => $value) {
						$html .=
							"<div class=\"etudes_succursales_coordonnees_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th width=\"50%\" style=\"text-align: left; border-bottom: 0px\">Coordonn&eacute;ees des registres&nbsp;:</td>\n".
							"				<th width=\"50%\" style=\"text-align: left; border-bottom: 0px\">Coordonn&eacute;ees modifi&eacute;es&nbsp;:</td>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n".
							"			<tr>\n".
							"				<td width=\"50%\" style=\"text-align: center; border-top: 0px;\"><textarea id=\"etudes_succursales_coordonnees_originales[" . $etudes_succursales_key . "]\" name=\"etudes_succursales_coordonnees_originales[" . $etudes_succursales_key . "]\" rows=\"8\" readonly=\"readonly\">" . $coordonnees_originales[$etudes_succursales_key] . "</textarea></td>\n".
							"				<td width=\"50%\" style=\"text-align: center; border-top: 0px;\"><textarea id=\"etudes_succursales_coordonnees[" . $etudes_succursales_key . "]\" name=\"etudes_succursales_coordonnees[" . $etudes_succursales_key . "]\" rows=\"8\" readonly=\"readonly\">" . $coordonnees[$etudes_succursales_key] . "</textarea></td>\n".
							"			</tr>\n". 
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n";
					}
				} else {
					$html .=
						"<br /><span class=\"warning readonly\">L'&eacute;tude ne contient aucune succursale.</span><br />\n";
				}

				break;

			case "compare":
				if (is_array($parameters) &&  count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$coordonnees = array();
						$coordonnees_originales = array();
						// en 1er, on importe les valeurs contenues dans la table `etudes_succursales`
						$DB->query(
							"SELECT `key`, `adresse`, `ville`, `province`, `code_postal`, `telephone1`, `telephone2`, `telecopieur` ".
							"FROM `etudes_succursales` ".
							"WHERE `etudes_key` = '" . $compare_parameters['etudes_key'] . "' AND `actif` = '1';"
						);
						while ($DB->next_record()) {
							$coordString = "Adresse : " . $DB->getField("adresse") . "\nVille : " . $DB->getField("ville") . "\nProvince : " . $DB->getField("province") . "\nCode Postal : " . $DB->getField("code_postal") . "\nTéléphone #1 : " . $DB->getField("telephone1") . "\nTéléphone #2 : " . $DB->getField("telephone2") . "\nTélécopieur : " . $DB->getField("telecopieur"); 
							$coordonnees_originales[$DB->getField("key")] = $coordString;
							$coordonnees[$DB->getField("key")] = $coordString;
						}
		
						// en 2e, on importe les valeurs modifiées de la table `evaluations_etudes_succursales` en écrasant celles précédement importées
						$DB->query(
							"SELECT `etudes_succursales_key`, `coordonnees` ".
							"FROM `evaluations_etudes_succursales` ".
							"WHERE `evaluations_key` = '" . $compare_parameters['key'] . "';"
						);
						while ($DB->next_record()) {
							$coordonnees[$DB->getField("etudes_succursales_key")] = $DB->getField("coordonnees");
						}

						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n";

						if (is_array($coordonnees) && count($coordonnees) > 0) {
							foreach ($coordonnees as $etudes_succursales_key => $value) {
								$html .=
									"<div class=\"etudes_succursales_coordonnees_entry\">\n".
									"	<table>\n".
									"		<thead>\n".
									"			<tr>\n".
									"				<th width=\"50%\" style=\"text-align: left; border-bottom: 0px\">Coordonn&eacute;ees des registres&nbsp;:</td>\n".
									"				<th width=\"50%\" style=\"text-align: left; border-bottom: 0px\">Coordonn&eacute;ees modifi&eacute;es&nbsp;:</td>\n".
									"			</tr>\n".
									"		</thead>\n".
									"		<tbody>\n".
									"			<tr>\n".
									"				<td width=\"50%\" style=\"text-align: center; border-top: 0px;\"><textarea name=\"" . $compare_sid . "_etudes_succursales_coordonnees_originales[" . $etudes_succursales_key . "]\" rows=\"8\" readonly=\"readonly\">" . $coordonnees_originales[$etudes_succursales_key] . "</textarea></td>\n".
									"				<td width=\"50%\" style=\"text-align: center; border-top: 0px;\"><textarea name=\"" . $compare_sid . "_etudes_succursales_coordonnees[" . $etudes_succursales_key . "]\" rows=\"8\" readonly=\"readonly\">" . $coordonnees[$etudes_succursales_key] . "</textarea></td>\n".
									"			</tr>\n". 
									"		</tbody>\n".
									"	</table>\n".
									"</div>\n";
							}
						} else {
							$html .=
								"<br /><span class=\"warning\">L'&eacute;tude ne contient aucune succursale.</span><br />\n";
						}
						$html .=
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function etudes_succursales_jscript() {
		$html = 
			"<script type=\"text/javascript\" language=\"JavaScript\">\n".
			"<!--\n".
			"	function etudes_succursales_modifier(nb) { \n".
			"		var key = document.getElementById('etudes_succursales_key[' + nb + ']')\n".
			"		var coordonnees = document.getElementById('etudes_succursales_coordonnees[' + nb + ']')\n".
			"		key.readOnly = false; \n".
			"		coordonnees.readOnly = false; \n".
			"		coordonnees.className = 'void'; \n".
			"	} \n".

			"	function etudes_succursales_annuler(nb) { \n".
			"		var key = document.getElementById('etudes_succursales_key[' + nb + ']')\n".
			"		var coordonnees = document.getElementById('etudes_succursales_coordonnees[' + nb + ']')\n".
			"		var coordonnees_originales = document.getElementById('etudes_succursales_coordonnees_originales[' + nb + ']')\n".
			"		key.readOnly = true; \n".
			"		coordonnees.value = coordonnees_originales.value; \n".
			"		coordonnees.className = 'readonly'; \n".
			"		coordonnees.readOnly = true; \n".
			"		etudes_succursales_changeinfo(nb, 'Aucune modification.'); \n".
			"	} \n".

			"	function etudes_succursales_onTextBox(nb) { \n".
			"		var textBox = document.getElementById('etudes_succursales_coordonnees[' + nb + ']')\n".
			"		var info = document.getElementById('etudes_succursales_info[' + nb + ']')\n".
			"		if (!textBox.readOnly) { \n".
			"			var key = document.getElementById('etudes_succursales_key[' + nb + ']')\n".
			"			var info = document.getElementById('etudes_succursales_info[' + nb + ']')\n".
			"			etudes_succursales_changeinfo(nb, 'En cours de modification...');\n".
			"		} \n".
			"	} \n".

			"	function etudes_succursales_changeinfo(nb, info_string) { \n".
			"		var key = document.getElementById('etudes_succursales_key[' + nb + ']')\n".
			"		var info = document.getElementById('etudes_succursales_info[' + nb + ']')\n".
			"		info.innerHTML = info_string; \n".
			"	} \n".
			"//-->\n".
			"</script>\n";

		return $html;
	}

?>