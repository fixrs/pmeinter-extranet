<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "20").

			"	<div class=\"entry\">\n".
			"		<label for=\"s20_publie_doc_en_ligne_bool\">Est-ce que vous publiez TOUS vos documents immobiliers en ligne&nbsp;?</label>\n".
			"		" . s20_publie_doc_en_ligne_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s20_publie_doc_en_ligne_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s20_publie_doc_en_ligne_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s20_assistant_immo_bool\">Utilisez-vous l'Assistant Immobilier&nbsp;?</label>\n".
			"		" . s20_assistant_immo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s20_assistant_immo_sioui_satisfait\">Si <b>OUI</b>, est-ce que vous &ecirc;tes satisfaites&nbsp;?</label>\n".
			"		" . s20_assistant_immo_sioui_satisfait($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s20_assistant_immo_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s20_assistant_immo_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s20_assistant_immo_non_utilise\">Si vous n'utilisez pas l'Assistant Immobilier, veuillez &eacute;num&eacute;rer les raisons&nbsp;:</label>\n".
			"		" . s20_assistant_immo_non_utilise($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s20_utilise_enfaces_reseau_bool\">Est-ce que l'on vous demande de toujours utiliser les en-faces (endos) du r&eacute;seau&nbsp;?</label>\n".
			"		" . s20_utilise_enfaces_reseau_bool($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}

	function s20_publie_doc_en_ligne_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s20_publie_doc_en_ligne_bool\" id=\"s20_publie_doc_en_ligne_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s20_publie_doc_en_ligne_bool\" id=\"s20_publie_doc_en_ligne_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s20_publie_doc_en_ligne_bool\" id=\"s20_publie_doc_en_ligne_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s20_publie_doc_en_ligne_bool\" id=\"s20_publie_doc_en_ligne_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s20_publie_doc_en_ligne_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s20_publie_doc_en_ligne_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s20_publie_doc_en_ligne_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s20_publie_doc_en_ligne_sinon\" id=\"s20_publie_doc_en_ligne_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s20_publie_doc_en_ligne_sinon\" id=\"s20_publie_doc_en_ligne_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s20_publie_doc_en_ligne_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s20_assistant_immo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s20_assistant_immo_bool\" id=\"s20_assistant_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s20_assistant_immo_bool\" id=\"s20_assistant_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s20_assistant_immo_bool\" id=\"s20_assistant_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s20_assistant_immo_bool\" id=\"s20_assistant_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s20_assistant_immo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s20_assistant_immo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s20_assistant_immo_sioui_satisfait($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s20_assistant_immo_sioui_satisfait\" id=\"s20_assistant_immo_sioui_satisfait_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s20_assistant_immo_sioui_satisfait\" id=\"s20_assistant_immo_sioui_satisfait_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s20_assistant_immo_sioui_satisfait\" id=\"s20_assistant_immo_sioui_satisfait_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s20_assistant_immo_sioui_satisfait\" id=\"s20_assistant_immo_sioui_satisfait_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s20_assistant_immo_sioui_satisfait\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s20_assistant_immo_sioui_satisfait\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s20_assistant_immo_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s20_assistant_immo_sinon\" id=\"s20_assistant_immo_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s20_assistant_immo_sinon\" id=\"s20_assistant_immo_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s20_assistant_immo_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s20_assistant_immo_non_utilise($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s20_assistant_immo_non_utilise\" id=\"s20_assistant_immo_non_utilise\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s20_assistant_immo_non_utilise\" id=\"s20_assistant_immo_non_utilise\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s20_assistant_immo_non_utilise\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s20_utilise_enfaces_reseau_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s20_utilise_enfaces_reseau_bool\" id=\"s20_utilise_enfaces_reseau_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s20_utilise_enfaces_reseau_bool\" id=\"s20_utilise_enfaces_reseau_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s20_utilise_enfaces_reseau_bool\" id=\"s20_utilise_enfaces_reseau_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s20_utilise_enfaces_reseau_bool\" id=\"s20_utilise_enfaces_reseau_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s20_utilise_enfaces_reseau_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s20_utilise_enfaces_reseau_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

?>