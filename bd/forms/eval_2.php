<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "2").

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s2_notaires_tous_presents_bool\">Est-ce que tous les notaires de l'&eacute;tude &eacute;taient pr&eacute;sents &agrave; la rencontre&nbsp;?</label>\n".
			"		" . s2_notaires_tous_presents_bool($action, getParametersFromSession($sid)) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"notaires\">V&eacute;rifier avec l'&eacute;tude la liste des notaires inscrits dans les registres du r&eacute;seau et ajouter les notaires manquants&nbsp;:</label>\n".
			"		" . notaires($action, getParametersFromSession($sid)) . "\n".
			"	</div>\n".
/*
			"	<div class=\"entry_long\">\n".
			"		<label for=\"notaires_titres\">Est-ce que l'un des notaires d&eacute;tient un ou plusieurs autres titres&nbsp;?</label>\n".
			"		" . notaires_titres($action, getParametersFromSession($sid)) . "\n".
			"	</div>\n".
*/
			"	<div class=\"entry\">\n".
			"		<label for=\"s2_notes_notaires\">Notes suppl&eacute;mentaires&nbsp;:</label>\n".
			"		" . s2_notes_notaires($action, getParametersFromSession($sid)) . "\n".
			"	</div>\n".

			form_footer($action, "2");

		return $_FORM;
	}

	function s2_notaires_tous_presents_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" id=\"s2_notaires_tous_presents_bool\" name=\"s2_notaires_tous_presents_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" id=\"s2_notaires_tous_presents_bool\" name=\"s2_notaires_tous_presents_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" id=\"s2_notaires_tous_presents_bool\" name=\"s2_notaires_tous_presents_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" id=\"s2_notaires_tous_presents_bool\" name=\"s2_notaires_tous_presents_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s2_notaires_tous_presents_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s2_notaires_tous_presents_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}
		
	function notaires($action, $parameters) {
		global $DB, $sid;
		$html = "";
		switch ($action) {	
			case "modify":

				// Suppression des notaires enlevés
				if (is_array($_POST['notaires_enlever']) && count($_POST['notaires_enlever']) > 0) {
					foreach ($_POST['notaires_enlever'] as $employes_key => $value) {
						if ($value == "1") {
							unset($_SESSION['evaluations'][$sid]['notaires_employes_key'][$employes_key]);
							unset($parameters['notaires_employes_key'][$employes_key]);
						}
					}
				}

				$DB->query(
					"SELECT e.key AS employes_key, e.prenom AS prenom, e.nom AS nom, e.courriel AS courriel, e.annee_debut_pratique AS pratique, e.associe AS associe ".
					"FROM `employes` e, `employes_titres` et ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1'".
					"ORDER BY e.nom ASC;"
				);

				// Ajout du JavaScript
				$html .=
					"<script type=\"text/javascript\" language=\"javascript\">\n".
					"	var notaires = new Array();\n";

				// on remplit un tableau jscript + php avec les valeurs de la table `employés`
				$notaires = array();
				while ($DB->next_record()) {
					$notaires[$DB->getField("employes_key")]['employes_key'] = $DB->getField("employes_key");
					$notaires[$DB->getField("employes_key")]['fullname'] = $DB->getField("prenom") . ", " . $DB->getField("nom");

					

					$html .=
						"notaires[\"" . $DB->getField("employes_key") . "\"] = { \n".
						"	'employes_key': \"" . $DB->getField("employes_key") . "\", \n".
						"	'nom': \"" . $DB->getField("nom") . "\", \n".
						"	'prenom': \"" . $DB->getField("prenom") . "\", \n".
						"	'courriel': \"" . $DB->getField("courriel") . "\", \n".
						"	'annee_debut_pratique': \"" . $DB->getField("pratique") . "\", \n".
						"	'associe': \"" . $DB->getField("associe") . "\" \n".
						"	'titres': \"" . $titres_JSArray . "\" \n".
						"};\n";
				}

				$html .=
					"	function notaires_onSelect(self) { \n".
					"		var employes_key_fromSelect = self.options[self.selectedIndex].value;\n".
					"		var already_added = '-1';\n".
					"		var already_added_warning = document.getElementById('already_added_warning');\n".
					"		already_added_warning.style.display = 'none';\n".
					"		if (document.getElementById('notaires_employes_key[' + employes_key_fromSelect + ']')) { \n".
					"			already_added = document.getElementById('notaires_employes_key[' + employes_key_fromSelect + ']').value; \n".
					"		}\n".
					"		if (employes_key_fromSelect == '-2') { \n".
					"			notaires_remplir_new();\n".
					"		} else { \n".
					"			if (employes_key_fromSelect != '' && employes_key_fromSelect != '-1' && employes_key_fromSelect != '-2') { \n".
					"				if (document.getElementById('notaires_employes_key[' + employes_key_fromSelect + ']') && already_added != '') { \n".
					"					notaires_showWarning(employes_key_fromSelect);\n".
					"				} else { \n".
					"					notaires_remplir(self);\n".
					"				}\n".
					"			}\n".
					"		}\n".
					"	}\n".

					"	function notaires_remplir(item) { \n".
					"		var employes_key_fromSelect = item.options[item.selectedIndex].value;\n".
					"		var employes_key_add = document.getElementById('notaires_employes_key_add');\n".
					"		var annee_debut_pratique_add = document.getElementById('notaires_annee_debut_pratique_add');\n".
					"		var nom_add = document.getElementById('notaires_nom_add');\n".
					"		var prenom_add = document.getElementById('notaires_prenom_add');\n".
					"		var courriel_add = document.getElementById('notaires_courriel_add');\n".
					"		var associe_oui_add = document.getElementById('notaires_associe_oui_add');\n".
					"		var associe_non_add = document.getElementById('notaires_associe_non_add');\n".
					"		employes_key_add.value = notaires[employes_key_fromSelect]['employes_key'];\n".
					"		nom_add.value = notaires[employes_key_fromSelect]['nom'];\n".
					"		prenom_add.value = notaires[employes_key_fromSelect]['prenom'];\n".
					"		courriel_add.value = notaires[employes_key_fromSelect]['courriel'];\n".
					"		annee_debut_pratique_add.value = notaires[employes_key_fromSelect]['annee_debut_pratique'];\n".
					"		if (notaires[employes_key_fromSelect]['associe'] == '1') { \n".
					"			associe_oui_add.checked = true;\n".
					"			associe_non_add.checked = false;\n".
					"		}\n".
					"		if (notaires[employes_key_fromSelect]['associe'] == '0') { \n".
					"			associe_oui_add.checked = false;\n".
					"			associe_non_add.checked = true;\n".
					"		}\n".
					"		notaires_enableForm()\n".
					"	}\n".

					"	function notaires_remplir_new() { \n".
					"		var employes_key_add = document.getElementById('notaires_employes_key_add');\n".
					"		var nom_add = document.getElementById('notaires_nom_add');\n".
					"		var prenom_add = document.getElementById('notaires_prenom_add');\n".
					"		var courriel_add = document.getElementById('notaires_courriel_add');\n".
					"		var annee_debut_pratique_add = document.getElementById('notaires_annee_debut_pratique_add');\n".
					"		var associe_oui_add = document.getElementById('notaires_associe_oui_add');\n".
					"		var associe_non_add = document.getElementById('notaires_associe_non_add');\n".
					"		var present_evaluation_oui_add = document.getElementById('notaires_present_evaluation_oui_add');\n".
					"		var present_evaluation_non_add = document.getElementById('notaires_present_evaluation_non_add');\n".
					"		var i = 0;\n".
					"		while (document.getElementById('notaires_employes_key[new_' + i + ']')) { \n".
					"			i++;\n".
					"		}\n".
					"		employes_key = 'new_' + i;\n".
					"		employes_key_add.value = employes_key;\n".
					"		nom_add.value = '';\n".
					"		nom_add.readOnly = false;\n".
					"		nom_add.className = 'void';\n".					
					"		prenom_add.value = '';\n".
					"		prenom_add.readOnly = false;\n".
					"		prenom_add.className = 'void';\n".					
					"		courriel_add.value = '';\n".
					"		annee_debut_pratique_add.value = '';\n".
					"		associe_oui_add.checked = false;\n".
					"		associe_non_add.checked = false;\n".
					"		present_evaluation_oui_add.checked = false;\n".
					"		present_evaluation_non_add.checked = false;\n".
					"		notaires_enableForm()\n".
					"	}\n".

					"	function notaires_ajouter() { \n".
					"		var employes_key = document.getElementById('notaires_employes_key_add');\n".
					"		var already_added_warning = document.getElementById('already_added_warning');\n".
					"		already_added_warning.style.display = 'none';\n".
					"		if (employes_key.value != '') { \n".
					"			if (document.getElementById('notaires_employes_key[' + employes_key + ']') && document.getElementById('notaires_employes_key[' + employes_key + ']')) { \n".
					"				notaires_showWarning(employes_key);\n".
					"			} else { \n".
					"				document.getElementById('notaires_ajouter').value = '1';\n".
					"				submitEvalForm('', '', '2');\n".
					"			}\n".
					"		}\n".
					"	}\n".

					"	function notaires_enlever(employes_key) { \n".
					"		document.getElementById('notaires_enlever[' + employes_key + ']').value = '1';\n".
					"		submitEvalForm('', '', '2');\n".
					"	}\n".

					"	function notaires_enableForm() { \n".
					"		var employes_key = document.getElementById('notaires_employes_key_add');\n".
					"		var courriel = document.getElementById('notaires_courriel_add');\n".
					"		var pratique = document.getElementById('notaires_annee_debut_pratique_add');\n".
					"		var nom = document.getElementById('notaires_nom_add');\n".
					"		var prenom = document.getElementById('notaires_prenom_add');\n".
					"		var associe_oui = document.getElementById('notaires_associe_oui_add');\n".
					"		var associe_non = document.getElementById('notaires_associe_non_add');\n".
					"		var raison_absence = document.getElementById('notaires_raison_absence_add');\n".
					"		var notaires_present_evaluation_oui = document.getElementById('notaires_present_evaluation_oui_add');\n".
					"		var notaires_present_evaluation_non = document.getElementById('notaires_present_evaluation_non_add');\n".
					"		employes_key.disabled = false;\n".
					"		courriel.disabled = false;\n".
					"		pratique.disabled = false;\n".
					"		nom.disabled = false;\n".
					"		prenom.disabled = false;\n".
					"		associe_oui.disabled = false;\n".
					"		associe_non.disabled = false;\n".
					"		raison_absence.disabled = false;\n".
					"		notaires_present_evaluation_oui.disabled = false;\n".
					"		notaires_present_evaluation_non.disabled = false;\n".
					"	}\n".

					"	function notaires_showWarning(employes_key) { \n".
					"		var table = document.getElementById('notaires_table[' + employes_key + ']');\n".
					"		var already_added_warning = document.getElementById('already_added_warning');\n".
					"		table.style.backgroundColor = '#EEEEEE';\n".
					"		var string = \"notaires_changeColor('notaires_table[\" + employes_key + \"]', '#FFFFFF');\"\n".
					"		setTimeout(string, 1200);\n".
					"		already_added_warning.style.display = 'block';\n".
					"	}\n".

					"	function notaires_changeColor(elementId, color) { \n".
					"		var element = document.getElementById(elementId);".
					"		element.style.backgroundColor = color;\n".
					"	}\n".
					"</script>\n";

				// Affiche et remplit le formulaire pour chaque notaire précédement ajouté
				if (is_array($parameters['notaires_employes_key']) && count($parameters['notaires_employes_key']) > 0) {
					ksort($parameters['notaires_employes_key']);
					foreach ($parameters['notaires_employes_key'] as $employes_key) {
						if ($employes_key != "" && $employes_key != "-1" && $employes_key != "0") {
	
							$notaires_employes_key = $employes_key;
							$notaires_nom = $parameters['notaires_nom'][$employes_key];
							$notaires_prenom = $parameters['notaires_prenom'][$employes_key];
							$notaires_courriel = $parameters['notaires_courriel'][$employes_key];
							$notaires_annee_debut_pratique = $parameters['notaires_annee_debut_pratique'][$employes_key];
							$notaires_raison_absence = $parameters['notaires_raison_absence'][$employes_key];
		
							if ($parameters['notaires_associe'][$employes_key] == "1") {
								$notaires_associe_oui_checked = "checked=\"checked\"";
								$notaires_associe_non_checked = "";
							} else {
								$notaires_associe_non_checked = "checked=\"checked\"";
								$notaires_associe_oui_checked = "";
							} 
		
							if ($parameters['notaires_present_evaluation'][$employes_key] == "1") {
								$notaires_present_evaluation_oui_checked = "checked=\"checked\"";
								$notaires_present_evaluation_non_checked = "";
							} else {
								$notaires_present_evaluation_non_checked = "checked=\"checked\"";
								$notaires_present_evaluation_oui_checked = "";
							} 			
		
							$html .=				
								"<div id=\"notaires_div[" . $notaires_employes_key . "]\" class=\"notaires_entry\">\n".
								"	<table id=\"notaires_table[" . $notaires_employes_key . "]\" style=\"margin-top: 20px;\">\n".
								"		<tbody>\n".
								"			<tr>\n".
								"				<td width=\"200px;\" rowspan=\"8\" style=\"text-align: center;\">\n" .
								"					<input type=\"hidden\" name=\"notaires_employes_key[" . $notaires_employes_key . "]\" id=\"notaires_employes_key[" . $notaires_employes_key . "]\" value=\"" . $notaires_employes_key . "\" />\n".
								"					<input type=\"hidden\" name=\"notaires_enlever[" . $notaires_employes_key . "]\" id=\"notaires_enlever[" . $notaires_employes_key . "]\" value=\"\" />\n".
								"					<a href=\"javascript: notaires_enlever('" . $notaires_employes_key . "');\">Enlever</a><br />\n".
								"				</td>\n".
								"				<th>\n".
								"					Statut&nbsp;:\n".
								"				</th>\n".
								"				<td width=\"175px\">\n".
								"					<input type=\"radio\" class=\"radio\" name=\"notaires_associe[" . $notaires_employes_key . "]\" id=\"notaires_associe_oui[" . $notaires_employes_key . "]\" value=\"1\" " .  $notaires_associe_oui_checked . " /><span class=\"radio_label\">Associ&eacute;</span>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"notaires_associe[" . $notaires_employes_key . "]\" id=\"notaires_associe_non[" . $notaires_employes_key . "]\" value=\"0\" " .  $notaires_associe_non_checked . " /><span class=\"radio_label\">Salari&eacute;</span>\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Nom du notaire&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_nom[" . $notaires_employes_key . "]\" id=\"notaires_nom[" . $notaires_employes_key . "]\" value=\"" . $notaires_nom . "\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Pr&eacute;nom du notaire&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_prenom[" . $notaires_employes_key . "]\" id=\"notaires_prenom[" . $notaires_employes_key . "]\" value=\"" . $notaires_prenom . "\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Adresse courriel&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_courriel[" . $notaires_employes_key  . "]\" id=\"notaires_courriel[" . $notaires_employes_key . "]\" value=\"" . $notaires_courriel . "\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Ann&eacute;e du d&eacute;but de la pratique&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_annee_debut_pratique[" . $notaires_employes_key . "]\" id=\"notaires_annee_debut_pratique[" . $notaires_employes_key . "]\" value=\"" . $notaires_annee_debut_pratique . "\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Pr&eacute;sent lors de l'&eacute;valuation&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"notaires_present_evaluation[" . $notaires_employes_key . "]\" id=\"notaires_present_evaluation_oui[" . $notaires_employes_key . "]\" value=\"1\" " . $notaires_present_evaluation_oui_checked . " /><span class=\"radio_label\">Oui</span>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"notaires_present_evaluation[" . $notaires_employes_key . "]\" id=\"notaires_present_evaluation_non[" . $notaires_employes_key . "]\" value=\"0\" " . $notaires_present_evaluation_non_checked . " /><span class=\"radio_label\">Non</span>\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Raison de l'absence, s'il y a lieu&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_raison_absence[" . $notaires_employes_key . "]\" id=\"notaires_raison_absence[" . $notaires_employes_key . "]\" value=\"" . $notaires_raison_absence . "\" />\n".
								"				</td>\n".
								"			</tr>\n".

								"			<tr>\n".
								"				<th>\n".
								"					Titres du notaire&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<select id=\"notaires_titres_titres_key[" . $notaires_employes_key . "]\" name=\"notaires_titres_titres_key[" . $notaires_employes_key . "][]\" multiple=\"multiple\" size=\"6\">\n".
								"						" . notaires_titres_selectOptions(get_employes_autres_titres(), "notaires_titres_titres_key[" . $notaires_employes_key . "]") . "\n".
								"					</select>\n".
								"				</td>\n".
								"			</tr>\n".

								"		</tbody>\n".
								"	</table>\n".
								"</div>\n";
						}
					}
				}

				// Affiche et remplit le formulaire pour le notaire qui vient d'être ajouté
				if ($parameters['notaires_ajouter'] == "1" && $parameters['notaires_employes_key_add'] != "" && $parameters['notaires_employes_key_add'] != "-1" && $parameters['notaires_nom_add'] != "" && $parameters['notaires_prenom_add'] != "") {

					$notaires_employes_key = $parameters['notaires_employes_key_add'];
					$notaires_nom = $parameters['notaires_nom_add'];
					$notaires_prenom = $parameters['notaires_prenom_add'];
					$notaires_courriel = $parameters['notaires_courriel_add'];
					$notaires_annee_debut_pratique = $parameters['notaires_annee_debut_pratique_add'];
					$notaires_raison_absence = $parameters['notaires_raison_absence_add'];

					if ($parameters['notaires_associe_add'] == "1") {
						$notaires_associe_oui_checked = "checked=\"checked\"";
						$notaires_associe_non_checked = "";
					} else {
						$notaires_associe_non_checked = "checked=\"checked\"";
						$notaires_associe_oui_checked = "";
					}

					if ($parameters['notaires_present_evaluation_add'] == "1") {
						$notaires_present_evaluation_oui_checked = "checked=\"checked\"";
						$notaires_present_evaluation_non_checked = "";
					} else {
						$notaires_present_evaluation_non_checked = "checked=\"checked\"";
						$notaires_present_evaluation_oui_checked = "";
					}

					$html .=				
						"<div id=\"notaires_div[" . $notaires_employes_key . "]\" class=\"notaires_entry\">\n".
						"	<table id=\"notaires_table[" . $notaires_employes_key . "]\" style=\"margin-top: 20px;\">\n".
						"		<tbody>\n".
						"			<tr>\n".
						"				<td width=\"200px\" rowspan=\"8\" style=\"text-align: center;\">\n" .
						"					<input type=\"hidden\" name=\"notaires_employes_key[" . $notaires_employes_key . "]\" id=\"notaires_employes_key[" . $notaires_employes_key . "]\" value=\"" . $notaires_employes_key . "\" />\n".
						"					<input type=\"hidden\" name=\"notaires_enlever[" . $notaires_employes_key . "]\" id=\"notaires_enlever[" . $notaires_employes_key . "]\" value=\"\" />\n".
						"					<a href=\"javascript: notaires_enlever('" . $notaires_employes_key . "');\">Enlever</a><br />\n".
						"				</td>\n".
						"				<th>\n".
						"					Statut&nbsp;:\n".
						"				</th>\n".
						"				<td width=\"175px\">\n".
						"					<input type=\"radio\" class=\"radio\" name=\"notaires_associe[" . $notaires_employes_key . "]\" id=\"notaires_associe_oui[" . $notaires_employes_key . "]\" value=\"1\" " .  $notaires_associe_oui_checked . " /><span class=\"radio_label\">Associ&eacute;</span>\n".
						"					<input type=\"radio\" class=\"radio\" name=\"notaires_associe[" . $notaires_employes_key . "]\" id=\"notaires_associe_non[" . $notaires_employes_key . "]\" value=\"0\" " .  $notaires_associe_non_checked . " /><span class=\"radio_label\">Salari&eacute;</span>\n".
						"				</td>\n".
						"			</tr>\n".
						"			<tr>\n".
						"				<th>\n".
						"					Nom du notaire&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_nom[" . $notaires_employes_key . "]\" id=\"notaires_nom[" . $notaires_employes_key . "]\" value=\"" . $notaires_nom . "\" readonly=\"readonly\" />\n".
						"				</td>\n".
						"			</tr>\n".
						"			<tr>\n".
						"				<th>\n".
						"					Pr&eacute;nom du notaire&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_prenom[" . $notaires_employes_key . "]\" id=\"notaires_prenom[" . $notaires_employes_key . "]\" value=\"" . $notaires_prenom . "\" readonly=\"readonly\" />\n".
						"				</td>\n".
						"			</tr>\n".
						"			<tr>\n".
						"				<th>\n".
						"					Adresse courriel&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_courriel[" . $notaires_employes_key  . "]\" id=\"notaires_courriel[" . $notaires_employes_key . "]\" value=\"" . $notaires_courriel . "\" />\n".
						"				</td>\n".
						"			</tr>\n".
						"			<tr>\n".
						"				<th>\n".
						"					Ann&eacute;e du d&eacute;but de la pratique&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_annee_debut_pratique[" . $notaires_employes_key . "]\" id=\"notaires_annee_debut_pratique[" . $notaires_employes_key . "]\" value=\"" . $notaires_annee_debut_pratique . "\" />\n".
						"				</td>\n".
						"			</tr>\n".
						"			<tr>\n".
						"				<th>\n".
						"					Pr&eacute;sent lors de l'&eacute;valuation&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<input type=\"radio\" class=\"radio\" name=\"notaires_present_evaluation[" . $notaires_employes_key . "]\" id=\"notaires_present_evaluation_oui[" . $notaires_employes_key . "]\" value=\"1\" " . $notaires_present_evaluation_oui_checked . " /><span class=\"radio_label\">Oui</span>\n".
						"					<input type=\"radio\" class=\"radio\" name=\"notaires_present_evaluation[" . $notaires_employes_key . "]\" id=\"notaires_present_evaluation_non[" . $notaires_employes_key . "]\" value=\"0\" " . $notaires_present_evaluation_non_checked . " /><span class=\"radio_label\">Non</span>\n".
						"				</td>\n".
						"			</tr>\n".
						"			<tr>\n".
						"				<th>\n".
						"					Raison de l'absence, s'il y a lieu&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_raison_absence[" . $notaires_employes_key . "]\" id=\"notaires_raison_absence[" . $notaires_employes_key . "]\" value=\"" . $notaires_raison_absence . "\" />\n".
						"				</td>\n".
						"			</tr>\n".

						"			<tr>\n".
						"				<th>\n".
						"					Titres du notaire&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<select id=\"notaires_titres_titres_key[" . $notaires_employes_key . "]\" name=\"notaires_titres_titres_key[" . $notaires_employes_key . "][]\" multiple=\"multiple\" size=\"6\">\n".
						"						" . notaires_titres_selectOptions(get_employes_autres_titres(), "notaires_titres_titres_key[" . $notaires_employes_key . "]") . "\n".
						"					</select>\n".
						"				</td>\n".
						"			</tr>\n".

						"		</tbody>\n".
						"	</table>\n".
						"</div>\n";
				}

				// Affiche le formulaire d'ajout
				$html .=				
					"<div id=\"notaires_div_add\" class=\"notaires_entry\">\n".
					"	<table id=\"notaires_table_add\" style=\"margin-top: 20px;\">\n".
					"		<tbody>\n".
					"			<tr>\n".
					"				<td width=\"200px\" rowspan=\"8\" style=\"text-align: center;\">\n".
					"					<input type=\"hidden\" name=\"notaires_ajouter\" id=\"notaires_ajouter\" value=\"\" />\n".
					"					<input type=\"hidden\" name=\"notaires_employes_key_add\" id=\"notaires_employes_key_add\" value=\"\" />\n".
					"					<span id=\"already_added_warning\" style=\"display: none;\">" . _error("Ce notaire est d&eacute;j&agrave; ajout&eacute;") . "</span><br />\n".
					"					<select name=\"notaires_employes_key_fromSelect\" id=\"notaires_employes_key_fromSelect\" style=\"width: 175px;\" onchange=\"notaires_onSelect(this)\" />\n".
					"						<option class=\"default\" value=\"-1\">- Choisir un notaire -</option>\n".
					"						<option value=\"-2\">(Un nouveau notaire)</option>\n";					

				if (is_array($notaires) && count($notaires) > 0) {
					foreach ($notaires as $notaire) {
						$html .= "<option value=\"" . $notaire['employes_key'] . "\">" . $notaire['fullname'] . "</option>\n";
					}
				}

				$html.= 
					"					</select><br />\n".
					"					<br />\n".
					"					<a href=\"javascript: notaires_ajouter();\">ajouter</a>\n".
					"				</td>\n".
					"				<th>\n".
					"					Statut&nbsp;:\n".
					"				</th>\n".
					"				<td width=\"175px\">\n".
					"					<input type=\"radio\" class=\"radio\" name=\"notaires_associe_add\" id=\"notaires_associe_oui_add\" value=\"1\" disabled=\"disabled\" /><span class=\"radio_label\">Associ&eacute;</span>\n".
					"					<input type=\"radio\" class=\"radio\" name=\"notaires_associe_add\" id=\"notaires_associe_non_add\" value=\"0\" disabled=\"disabled\" /><span class=\"radio_label\">Salari&eacute;</span>\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>\n".
					"					Nom du notaire&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_nom_add\" id=\"notaires_nom_add\" value=\"\" disabled=\"disabled\" readonly=\"readonly\" />\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>\n".
					"					Pr&eacute;nom du notaire&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_prenom_add\" id=\"notaires_prenom_add\" value=\"\" disabled=\"disabled\" readonly=\"readonly\" />\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>\n".
					"					Adresse courriel&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_courriel_add\" id=\"notaires_courriel_add\" value=\"\" disabled=\"disabled\" />\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>\n".
					"					Ann&eacute;e du d&eacute;but de la pratique&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_annee_debut_pratique_add\" id=\"notaires_annee_debut_pratique_add\" value=\"\" disabled=\"disabled\" />\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>\n".
					"					Pr&eacute;sent lors de l'&eacute;valuation&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<input type=\"radio\" class=\"radio\" name=\"notaires_present_evaluation_add\" id=\"notaires_present_evaluation_oui_add\" value=\"1\" disabled=\"disabled\" /><span class=\"radio_label\">Oui</span>\n".
					"					<input type=\"radio\" class=\"radio\" name=\"notaires_present_evaluation_add\" id=\"notaires_present_evaluation_non_add\" value=\"0\" disabled=\"disabled\" /><span class=\"radio_label\">Non</span>\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>\n".
					"					Raison de l'absence, s'il y a lieu&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_raison_absence_add\" id=\"notaires_raison_absence_add\" value=\"\" disabled=\"disabled\" />\n".
					"				</td>\n".
					"			</tr>\n".

					"			<tr>\n".
					"				<th>\n".
					"					Titres du notaire&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<select id=\"notaires_titres_titres_key[" . $employes_key . "]\" name=\"notaires_titres_titres_key_add[]\" multiple=\"multiple\" size=\"6\">\n".
					"						" . notaires_titres_selectOptions(get_employes_autres_titres(), "notaires_titres_titres_key_add") . "\n".
					"					</select>\n".
					"				</td>\n".
					"			</tr>\n".

					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";

				$html .=
					"<span class=\"hint\">* Les changements apport&eacute;es ne modifieront pas les valeurs originales des registres du r&eacute;seau.</span>\n";
				break;

			case "read":
				if (is_array($parameters['notaires_employes_key']) && count($parameters['notaires_employes_key']) > 0) {
					ksort($parameters['notaires_employes_key']);
					foreach ($parameters['notaires_employes_key'] as $employes_key) {
						if ($employes_key != "" && $employes_key != "-1" && $employes_key != "0") {
	
							$notaires_employes_key = $employes_key;
							$notaires_nom = $parameters['notaires_nom'][$employes_key];
							$notaires_prenom = $parameters['notaires_prenom'][$employes_key];
							$notaires_courriel = $parameters['notaires_courriel'][$employes_key];
							$notaires_annee_debut_pratique = $parameters['notaires_annee_debut_pratique'][$employes_key];
							$notaires_raison_absence = $parameters['notaires_raison_absence'][$employes_key];
		
							if ($parameters['notaires_associe'][$employes_key] == "1") {
								$notaires_associe_oui_checked = "checked=\"checked\"";
								$notaires_associe_non_checked = "";
							} else {
								$notaires_associe_non_checked = "checked=\"checked\"";
								$notaires_associe_oui_checked = "";
							} 
		
							if ($parameters['notaires_present_evaluation'][$employes_key] == "1") {
								$notaires_present_evaluation_oui_checked = "checked=\"checked\"";
								$notaires_present_evaluation_non_checked = "";
							} else {
								$notaires_present_evaluation_non_checked = "checked=\"checked\"";
								$notaires_present_evaluation_oui_checked = "";
							} 			
		
							$html .=				
								"<div id=\"notaires_div[" . $notaires_employes_key . "]\" class=\"notaires_entry\">\n".
								"	<table id=\"notaires_table[" . $notaires_employes_key . "]\" style=\"margin-top: 20px;\">\n".
								"		<tbody>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Statut&nbsp;:\n".
								"				</th>\n".
								"				<td width=\"175px\">\n".
								"					<input type=\"radio\" class=\"radio\" name=\"notaires_associe[" . $notaires_employes_key . "]\" id=\"notaires_associe_oui[" . $notaires_employes_key . "]\" value=\"1\" " .  $notaires_associe_oui_checked . " /><span class=\"radio_label\">Associ&eacute;</span>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"notaires_associe[" . $notaires_employes_key . "]\" id=\"notaires_associe_non[" . $notaires_employes_key . "]\" value=\"0\" " .  $notaires_associe_non_checked . " /><span class=\"radio_label\">Salari&eacute;</span>\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Nom du notaire&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_nom[" . $notaires_employes_key . "]\" id=\"notaires_nom[" . $notaires_employes_key . "]\" value=\"" . $notaires_nom . "\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Pr&eacute;nom du notaire&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_prenom[" . $notaires_employes_key . "]\" id=\"notaires_prenom[" . $notaires_employes_key . "]\" value=\"" . $notaires_prenom . "\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Adresse courriel&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_courriel[" . $notaires_employes_key  . "]\" id=\"notaires_courriel[" . $notaires_employes_key . "]\" value=\"" . $notaires_courriel . "\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Ann&eacute;e du d&eacute;but de la pratique&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_annee_debut_pratique[" . $notaires_employes_key . "]\" id=\"notaires_annee_debut_pratique[" . $notaires_employes_key . "]\" value=\"" . $notaires_annee_debut_pratique . "\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Pr&eacute;sent lors de l'&eacute;valuation&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"notaires_present_evaluation[" . $notaires_employes_key . "]\" id=\"notaires_present_evaluation_oui[" . $notaires_employes_key . "]\" value=\"1\" " . $notaires_present_evaluation_oui_checked . " readonly=\"readonly\" /><span class=\"radio_label\">Oui</span>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"notaires_present_evaluation[" . $notaires_employes_key . "]\" id=\"notaires_present_evaluation_non[" . $notaires_employes_key . "]\" value=\"0\" " . $notaires_present_evaluation_non_checked . " readonly=\"readonly\" /><span class=\"radio_label\">Non</span>\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Raison de l'absence, s'il y a lieu&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"notaires_raison_absence[" . $notaires_employes_key . "]\" id=\"notaires_raison_absence[" . $notaires_employes_key . "]\" value=\"" . $notaires_raison_absence . "\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"		</tbody>\n".
								"	</table>\n".
								"</div>\n";
						}
					}
				}
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						if (is_array($compare_parameters['notaires_employes_key']) && count($compare_parameters['notaires_employes_key']) > 0) {
							ksort($compare_parameters['notaires_employes_key']);
							foreach ($compare_parameters['notaires_employes_key'] as $employes_key) {
								if ($employes_key != "" && $employes_key != "-1" && $employes_key != "0") {
			
									$notaires_employes_key = $employes_key;
									$notaires_nom = $compare_parameters['notaires_nom'][$employes_key];
									$notaires_prenom = $compare_parameters['notaires_prenom'][$employes_key];
									$notaires_courriel = $compare_parameters['notaires_courriel'][$employes_key];
									$notaires_annee_debut_pratique = $compare_parameters['notaires_annee_debut_pratique'][$employes_key];
									$notaires_raison_absence = $compare_parameters['notaires_raison_absence'][$employes_key];
				
									if ($compare_parameters['notaires_associe'][$employes_key] == "1") {
										$notaires_associe_oui_checked = "checked=\"checked\"";
										$notaires_associe_non_checked = "";
									} else {
										$notaires_associe_non_checked = "checked=\"checked\"";
										$notaires_associe_oui_checked = "";
									} 
				
									if ($compare_parameters['notaires_present_evaluation'][$employes_key] == "1") {
										$notaires_present_evaluation_oui_checked = "checked=\"checked\"";
										$notaires_present_evaluation_non_checked = "";
									} else {
										$notaires_present_evaluation_non_checked = "checked=\"checked\"";
										$notaires_present_evaluation_oui_checked = "";
									} 			
				
									$html .=				
										"<div class=\"answer_compare_" . $compare_color . "\">\n".
										"<div class=\"notaires_entry\">\n".
										"	<table style=\"margin-top: 20px;\">\n".
										"		<tbody>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Statut&nbsp;:\n".
										"				</th>\n".
										"				<td width=\"175px\">\n".
										"					<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_notaires_associe[" . $notaires_employes_key . "]\" value=\"1\" " .  $notaires_associe_oui_checked . " /><span class=\"radio_label\">Associ&eacute;</span>\n".
										"					<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_notaires_associe[" . $notaires_employes_key . "]\" value=\"0\" " .  $notaires_associe_non_checked . " /><span class=\"radio_label\">Salari&eacute;</span>\n".
										"				</td>\n".
										"			</tr>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Nom du notaire&nbsp;:\n".
										"				</th>\n".
										"				<td>\n".
										"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"" . $compare_sid . "_notaires_nom[" . $notaires_employes_key . "]\" value=\"" . $notaires_nom . "\" readonly=\"readonly\" />\n".
										"				</td>\n".
										"			</tr>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Pr&eacute;nom du notaire&nbsp;:\n".
										"				</th>\n".
										"				<td>\n".
										"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"" . $compare_sid . "_notaires_prenom[" . $notaires_employes_key . "]\" value=\"" . $notaires_prenom . "\" readonly=\"readonly\" />\n".
										"				</td>\n".
										"			</tr>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Adresse courriel&nbsp;:\n".
										"				</th>\n".
										"				<td>\n".
										"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"" . $compare_sid . "_notaires_courriel[" . $notaires_employes_key  . "]\" value=\"" . $notaires_courriel . "\" readonly=\"readonly\" />\n".
										"				</td>\n".
										"			</tr>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Ann&eacute;e du d&eacute;but de la pratique&nbsp;:\n".
										"				</th>\n".
										"				<td>\n".
										"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"" . $compare_sid . "_notaires_annee_debut_pratique[" . $notaires_employes_key . "]\" value=\"" . $notaires_annee_debut_pratique . "\" readonly=\"readonly\" />\n".
										"				</td>\n".
										"			</tr>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Pr&eacute;sent lors de l'&eacute;valuation&nbsp;:\n".
										"				</th>\n".
										"				<td>\n".
										"					<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_notaires_present_evaluation[" . $notaires_employes_key . "]\" value=\"1\" " . $notaires_present_evaluation_oui_checked . " readonly=\"readonly\" /><span class=\"radio_label\">Oui</span>\n".
										"					<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_notaires_present_evaluation[" . $notaires_employes_key . "]\" value=\"0\" " . $notaires_present_evaluation_non_checked . " readonly=\"readonly\" /><span class=\"radio_label\">Non</span>\n".
										"				</td>\n".
										"			</tr>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Raison de l'absence, s'il y a lieu&nbsp;:\n".
										"				</th>\n".
										"				<td>\n".
										"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"" . $compare_sid . "_notaires_raison_absence[" . $notaires_employes_key . "]\" value=\"" . $notaires_raison_absence . "\" readonly=\"readonly\" />\n".
										"				</td>\n".
										"			</tr>\n".
										"		</tbody>\n".
										"	</table>\n".
										"</div>\n".
										"</div>\n";
								}
							}
						}
					}
				}
				break;
		}
	return $html;
	}

	function notaires_titres($action, $parameters) {
		global $DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS;
		$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
		$DB2 = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
		$html = "";
		$notaires_titres = array();
		switch ($action) {	
			case "modify":
				// on charge les valeurs originales depuis les registres
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS employes_nom, e.prenom AS employes_prenom ".
					"FROM `employes` AS e, `employes_etudes_succursales` AS ees, `etudes_succursales` AS es, `employes_titres` AS et ".
					"WHERE e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $parameters['etudes_key'] . "' AND et.employes_key = e.key AND et.titres_key = '1' AND e.actif = '1' ".
					"ORDER BY e.nom;"
				);
				while ($DB->next_record()) {
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("employes_prenom") . " " . $DB->getField("employes_nom");
					$notaires_titres['employes_key'][$employes_key] = $employes_key;
					$notaires_titres['employes_fullname'][$employes_key] = $fullname;
					$DB2->reset();
					$DB2->query(
						"SELECT t.key AS titres_key, t.nom AS titres_nom ".
						"FROM `employes` AS e, `titres` AS t, `employes_titres` AS et ".
						"WHERE e.key = '" . $employes_key . "' AND e.key = et.employes_key AND et.titres_key = t.key;"
					);
					while ($DB2->next_record()) {
						$titres_key = $DB2->getField("titres_key");
						$titres_nom = $DB2->getField("titres_nom");						
						if ($titres_key != "1") {
							$notaires_titres['titres_originaux_nom'][$employes_key][$titres_key] = $titres_nom;
						}
					}
				}

				$html .=
					"<div class=\"notaires_titres_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th>Notaire</th>\n".
					"				<th>Valeurs des registres</th>\n".
					"				<th>Valeurs modifi&eacute;es</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";

				if (is_array($notaires_titres['employes_key']) && count($notaires_titres['employes_key']) > 0) {
					foreach ($notaires_titres['employes_key'] as $employes_key) {
						$html .=
							"<tr>\n".
							"	<td>" . $notaires_titres['employes_fullname'][$employes_key] . "</td>\n".
							"	<td>\n";
						if (is_array($notaires_titres['titres_originaux_nom']) && count($notaires_titres['titres_originaux_nom']) > 0) {
							foreach ($notaires_titres['titres_originaux_nom'][$employes_key] as $titres_key => $titres_nom) {
								$html .=
									"<span class=\"readonly\">" . $titres_nom . "</span><br />\n";
							}
						}
	
						$html .=
							"	</td>\n".
							"	<td>\n".
							"		<input id=\"notaires_titres_employes_key[" . $employes_key . "]\" name=\"notaires_titres_employes_key[" . $employes_key . "]\" type=\"hidden\" value=\"" . $employes_key . "\" />\n".
							"		<select id=\"notaires_titres_titres_key[" . $employes_key . "]\" name=\"notaires_titres_titres_key[" . $employes_key . "][]\" multiple=\"multiple\" size=\"6\">\n".
							"			" . notaires_titres_selectOptions(get_employes_autres_titres(), "notaires_titres_titres_key[" . $employes_key . "]") . "\n".
							"		</select>\n".
							"	</td>\n".
							"</tr>\n";
					}
				} else {
					$html .=
						"<tr>\n".
						"	<td colspan=\"3\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire</span></td>\n".
						"</tr>\n";
				}

				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";

				$html .=
					"<span class=\"hint\">* Les changements apport&eacute;es ne modifieront pas les valeurs originales des registres du r&eacute;seau.</span>\n";
				break;

			case "read":
				// on charge les valeurs originales depuis les registres
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS employes_nom, e.prenom AS employes_prenom ".
					"FROM `employes` AS e, `employes_etudes_succursales` AS ees, `etudes_succursales` AS es, `employes_titres` AS et ".
					"WHERE e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $parameters['etudes_key'] . "' AND et.employes_key = e.key AND et.titres_key = '1' AND e.actif = '1' ".
					"ORDER BY e.nom;"
				);
				while ($DB->next_record()) {
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("employes_prenom") . " " . $DB->getField("employes_nom");
					$notaires_titres["employes_key"][$employes_key] = $employes_key;
					$notaires_titres["employes_fullname"][$employes_key] = $fullname;
					$DB2->reset();
					$DB2->query(
						"SELECT t.key AS titres_key, t.nom AS titres_nom ".
						"FROM `employes` AS e, `titres` AS t, `employes_titres` AS et ".
						"WHERE e.key = '" . $employes_key . "' AND e.key = et.employes_key AND et.titres_key = t.key;"
					);
					while ($DB2->next_record()) {
						$titres_key = $DB2->getField("titres_key");
						$titres_nom = $DB2->getField("titres_nom");						
						if ($titres_key != "1") {
							$notaires_titres["titres_originaux_nom"][$employes_key][$titres_key] = $titres_nom;
						}
					}
				}

				$html .=
					"<div class=\"notaires_titres_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th>Notaire</th>\n".
					"				<th>Valeurs des registres</th>\n".
					"				<th>Valeurs modifi&eacute;es</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";

				if (is_array($notaires_titres['employes_key']) && count($notaires_titres['employes_key']) > 0) {
					foreach ($notaires_titres['employes_key'] as $employes_key) {
						$html .=
							"<tr>\n".
							"	<td>" . $notaires_titres['employes_fullname'][$employes_key] . "</td>\n".
							"	<td>\n";
						if (is_array($notaires_titres['titres_originaux_nom']) && count($notaires_titres['titres_originaux_nom']) > 0) {
							foreach ($notaires_titres['titres_originaux_nom'][$employes_key] as $titres_key => $titres_nom) {
								$html .=
									"<span>" . $titres_nom . "</span><br />\n";
							}
						}
	
						$html .=
							"	</td>\n".
							"	<td>\n".
							"		<select id=\"notaires_titres_titres_key[" . $employes_key . "]\" name=\"notaires_titres_titres_key[" . $employes_key . "][]\" multiple=\"multiple\" size=\"6\">\n".
							"			" . notaires_titres_selectOptions(get_employes_autres_titres(), "notaires_titres_titres_key[" . $employes_key . "]") . "\n".
							"		</select>\n".
							"	</td>\n".
							"</tr>\n";
					}
				} else {
					$html .=
						"<tr>\n".
						"	<td colspan=\"3\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire</span></td>\n".
						"</tr>\n";
				}

				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						// on charge les valeurs originales depuis les registres
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS employes_nom, e.prenom AS employes_prenom ".
							"FROM `employes` AS e, `employes_etudes_succursales` AS ees, `etudes_succursales` AS es, `employes_titres` AS et ".
							"WHERE e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $compare_parameters['etudes_key'] . "' AND et.employes_key = e.key AND et.titres_key = '1' AND e.actif = '1' ".
							"ORDER BY e.nom;"
						);
						while ($DB->next_record()) {
							$employes_key = $DB->getField("employes_key");
							$fullname = $DB->getField("employes_prenom") . " " . $DB->getField("employes_nom");
							$notaires_titres['employes_key'][$employes_key] = $employes_key;
							$notaires_titres['employes_fullname'][$employes_key] = $fullname;
							$DB2->reset();
							$DB2->query(
								"SELECT t.key AS titres_key, t.nom AS titres_nom ".
								"FROM `employes` AS e, `titres` AS t, `employes_titres` AS et ".
								"WHERE e.key = '" . $employes_key . "' AND e.key = et.employes_key AND et.titres_key = t.key;"
							);
							while ($DB2->next_record()) {
								$titres_key = $DB2->getField("titres_key");
								$titres_nom = $DB2->getField("titres_nom");						
								if ($titres_key != "1") {
									$notaires_titres['titres_originaux_nom'][$employes_key][$titres_key] = $titres_nom;
								}
							}
						}
		
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"notaires_titres_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th>Notaire</th>\n".
							"				<th>Valeurs des registres</th>\n".
							"				<th>Valeurs modifi&eacute;es</th>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n";
		
						if (is_array($notaires_titres['employes_key']) && count($notaires_titres['employes_key']) > 0) {
							foreach ($notaires_titres['employes_key'] as $employes_key) {
								$html .=
									"<tr>\n".
									"	<td>" . $notaires_titres['employes_fullname'][$employes_key] . "</td>\n".
									"	<td>\n";
								if (is_array($notaires_titres['titres_originaux_nom'][$employes_key]) && count($notaires_titres['titres_originaux_nom'][$employes_key]) > 0) {
									foreach ($notaires_titres['titres_originaux_nom'][$employes_key] as $titres_key => $titres_nom) {
										$html .=
											"<span>" . $titres_nom . "</span><br />\n";
									}
								}
			
								$html .=
									"	</td>\n".
									"	<td>\n".
									"		<select name=\"" . $compare_sid . "_notaires_titres_titres_key[" . $employes_key . "][]\" multiple=\"multiple\" size=\"6\" readonly=\"readonly\">\n".
									"			" . notaires_titres_selectOptions(get_employes_autres_titres(), "" . $compare_sid . "_notaires_titres_titres_key[" . $employes_key . "]") . "\n".
									"		</select>\n".
									"	</td>\n".
									"</tr>\n";
							}
						} else {
							$html .=
								"<tr>\n".
								"	<td colspan=\"3\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire</span></td>\n".
								"</tr>\n";
						}
		
						$html .=
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n".
							"</div>\n";
					}
				}			
				break;
		}
		return $html;
	}

	function get_employes_autres_titres() {
		global $DB;
		$employes_autres_titres = array();
		$DB->query(
			"SELECT `nom`, `key` ".
			"FROM `titres` ".
			"ORDER BY `nom` ASC;"
		);
		while ($DB->next_record()) {
			$key = $DB->getField("key");
			$nom = $DB->getField("nom");
			if ($key != "1") {
				$employes_autres_titres[$key] = $nom;
			}
		}
		return $employes_autres_titres;
	}

	function notaires_titres_selectOptions($employes_autres_titres, $selectname) {
		$html = "";
		if (is_array($employes_autres_titres) && count($employes_autres_titres) > 0) {
			foreach ($employes_autres_titres as $key => $nom) {
				$html .= "<option selectname=\"" . $selectname . "\" value=\"" . $key . "\">" . $nom . "</option>\n";
			}
		}
		return $html;
	}

	function s2_notes_notaires($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"10\" cols=\"40\" id=\"s2_notes_notaires\" name=\"s2_notes_notaires\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"10\" cols=\"40\" id=\"s2_notes_notaires\" name=\"s2_notes_notaires\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"10\" cols=\"40\" name=\"" . $compare_sid . "_s2_notes_notaires\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;

		}
		return $html;
	}

?>