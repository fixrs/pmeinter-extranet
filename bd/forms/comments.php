<?php


    // LE FORMULAIRE //
    function getCommentForm($action, $dossier_key) {
        global $DB, $BASEURL;

        if ($action == "add") {
            $class = ' add-comment-form ';
            if (isset($_POST) && count($_POST) > 0) {
                $class .= ' error ';
            }

            $_FORM =
                '<div id="add-comment-form-' . $dossier_key . '" class="' . $class . '"></a>' .
                    comment_form_header($action, $dossier_key).
                '   <div class="row">
                        <div class="col-xs-12 col-sm-12">
                            <div class="description">' . comment_description($action) . '</div>
                        </div>
                    </div>
                    ' . comment_form_footer($action, $dossier_key) . '
                </div>';

        } else {

            $class = "";
            if (count($_POST) > 0) {
                $class = ' class="error" ';
            }

            $_FORM =
                '<div class="edit-comment-form" ' . $class . '>' .
                    comment_form_header($action, $dossier_key) .
                '   <div class="row">
                        <div class="description">' . comment_description($action) . '</div>
                    </div>
                    ' . comment_form_footer($action, $dossier_key) . '
                </div>';

        }

        return $_FORM;
    }


    // LES SOUS-ROUTINES POUR LES INPUTS //

    function comment_form_header($action, $dossier_key) {
        global $DB;

        $html = "";
        $form_class = "";
        if ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") { $form_class = ' class="limited" '; }

        $employe_key = $_SESSION["user_key"];
        if (!is_numeric($_SESSION['user_key'])) {
            $DB->query(
                "SELECT u.employes_key ".
                "FROM `utilisateurs` u ".
                "WHERE u.key = '" . $_SESSION['user_key'] . "' "
            );

            while ($DB->next_record()) {
                $employe_key = $DB->getField("employes_key");
            }
        }


        switch ($action) {
            case "add":
                $html .=
                    "<form name=\"comment\" action=\"#\" method=\"post\">\n".
                    "   <input type=\"hidden\" name=\"action\" value=\"add-comment\" />\n".
                    "   <input type=\"hidden\" name=\"dossiers_key\" value=\"" . $dossier_key . "\" />\n".
                    "   <input type=\"hidden\" name=\"comments_key\" value=\"\" />\n".
                    "   <input type=\"hidden\" name=\"employes_key\" value=\"" . $employe_key . "\" />\n";
                break;
            case "modify":
                $html .=
                    "<form name=\"comment\" action=\"#\" method=\"post\">\n".
                    "   <input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
                    "   <input type=\"hidden\" name=\"key\" value=\"\" />\n".
                    "   <input type=\"hidden\" name=\"comments_key\" value=\"\" />\n".
                    "   <input type=\"hidden\" name=\"dossiers_key\" value=\"" . $dossier_key . "\" />\n".
                    "   <input type=\"hidden\" name=\"employes_key\" value=\"" . $employe_key . "\" />\n";
                break;
            case "read":
                $html .=
                    "<div class=\"readform\">\n";
                break;
        }
        return $html;
    }

    function comment_form_footer($action, $dossier_key) {
        $html = ""; //"<script src=\"/extranet/bd/skin/js/jquery.js\"></script>";

        switch ($action) {
            case "add":
                $html .=
                    "   <input type=\"submit\" class=\"submit\" value=\"Envoyer\" />\n".
                    "</form>\n";
                break;
            case "modify":
                $html .=
                    "   <input type=\"submit\" class=\"submit\" value=\"Envoyer\" /> <a class=\"cancel-link\">Annuler</a>\n".
                    "</form>\n";
                break;
            case "read":
                $html .=
                    "</div>\n";
                break;
        }
        return $html;
    }


    function comment_description($action) {
        $html = "";
        switch ($action) {
            case "add":
                $html =
                    //"<label for=\"description\">Description&nbsp;: </label>\n".
                    "<textarea rows=\"4\" cols=\"40\" name=\"description\"></textarea>\n";
                    //"<br/><br/>";
                break;
            case "modify":
                $html =
                        //"<label for=\"description\">Description&nbsp;: </label>\n".
                        "<textarea rows=\"4\" cols=\"40\" name=\"description\"></textarea>\n";
                        //"<br/><br/>";
                break;
            case "read":
                $html =
                    "<label for=\"description\">Description&nbsp;: </label>\n".
                    "<span class=\"answer\"><!---HIDDEN_description /HIDDEN_description---></span><br/><br/>\n";
                break;
        }
        return $html;
    }
