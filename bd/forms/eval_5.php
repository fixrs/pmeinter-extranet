<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "5").

			"	<h2>Affichage et identification au r&eacute;seau</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_logo_visible_ext_bool\">Le logo de PME INTER Notaires est visible &agrave; l'ext&eacute;rieur des locaux&nbsp;?</label>\n".
			"		" . s5_logo_visible_ext_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_logo_visible_int_bool\">Le logo de PME INTER Notaires est visible &agrave; l'int&eacute;rieur des locaux&nbsp;?</label>\n".
			"		" . s5_logo_visible_int_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_produits_visibles_bool\">Les produits de PME INTER Notaires sont visibles dans la salle d'attente&nbsp;?</label>\n".
			"		" . s5_produits_visibles_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_produits_visibles_sioui\">Si <b>OUI</b>, indiquer lesquels&nbsp;:</label>\n".
			"		" . s5_produits_visibles_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_produits_visibles_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s5_produits_visibles_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_utilise_brochure_bool\">Est-ce que l'&eacute;tude utilise la brochure corporative de PME INTER Notaires (<a href=\"" . $BASEURL . "docs/PMEInter/annexe_05.pdf\" target=\"_annexe\">annexe 5</a>) aupr&egrave;s de ses donneurs d'ordres ou de la pr&eacute;sentation Power Point disponible (fin du cartable bleu)&nbsp;?</label>\n".
			"		" . s5_utilise_brochure_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_message_attente_bool\">Est-ce que l'&eacute;tude a des messages de mise en attente&nbsp;?</label>\n".
			"		" . s5_message_attente_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_message_courtoisie_bool\">Est-ce que l'&eacute;tude utilise les messages de courtoisies d&eacute;velopp&eacute;s par le r&eacute;seau&nbsp;?</label>\n".
			"		" . s5_message_courtoisie_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_message_courtoisie_sinon_interet\">Si <b>NON</b>, est-ce que l'&eacute;tude serait int&eacute;ress&eacute;e &agrave; les utiliser&nbsp;?</label>\n".
			"		" . s5_message_courtoisie_sinon_interet($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Visibilit&eacute;</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_pagesblanches_bool\">Est-ce que l'&eacute;tude affiche le nom de PME INTER Notaires dans les pages blanches de l'annuaire t&eacute;l&eacute;phonique&nbsp;?</label>\n".
			"		" . s5_affiche_pagesblanches_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_pagesblanches_doc\">Si <b>OUI</b>, joindre une photocopie en annexe&nbsp;:</label>\n".
			"		" . s5_affiche_pagesblanches_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_pagesblanches_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s5_affiche_pagesblanches_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_pagesjaunes_bool\">Est-ce que l'&eacute;tude affiche le nom de PME INTER Notaires dans les pages jaunes de l'annuaire t&eacute;l&eacute;phonique&nbsp;?</label>\n".
			"		" . s5_affiche_pagesjaunes_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_pagesjaunes_sioui\">Si <b>OUI</b>, joindre une photocopie en annexe&nbsp;:</label>\n".
			"		" . s5_affiche_pagesjaunes_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_pagesjaunes_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s5_affiche_pagesjaunes_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_judiciaire_bool\">Est-ce que l'&eacute;tude affiche le nom de PME INTER Notaires dans l'annuaire t&eacute;l&eacute;phonique judiciaire du Qu&eacute;bec&nbsp;?</label>\n".
			"		" . s5_affiche_judiciaire_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_judiciaire_sioui\">Si <b>OUI</b>, joindre une photocopie en annexe&nbsp;:</label>\n".
			"		" . s5_affiche_judiciaire_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_judiciaire_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s5_affiche_judiciaire_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_emplois_bool\">Est-ce que l'&eacute;tude affiche le nom de PME INTER Notaires lors de la publication d'offres d'emplois (Entracte, journaux locaux ou autres) ou autres publicit&eacute;s&nbsp;?</label>\n".
			"		" . s5_affiche_emplois_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_emplois_sioui\">Si <b>OUI</b>, joindre si possible une photocopie en annexe&nbsp;:</label>\n".
			"		" . s5_affiche_emplois_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_emplois_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s5_affiche_emplois_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_publication_bool\">Est-ce que l'&eacute;tude affiche le nom de PME INTER Notaires lors d'avis de publication (cl&ocirc;ture d'inventaire successorale, dissolution de compagnie ou autres)&nbsp;?</label>\n".
			"		" . s5_affiche_publication_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_publication_sioui\">Si <b>OUI</b>, joindre si possible une photocopie en annexe&nbsp;:</label>\n".
			"		" . s5_affiche_publication_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_affiche_publication_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s5_affiche_publication_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Papeterie</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"produits_papeterie_utilisee\">Entre 2003 et 2004, l'&eacute;tude a command&eacute; des articles de papeterie aupr&egrave;s de notre fournisseur Impression Rive-Sud. Indiquer la fr&eacute;quence d'utilisation de ces articles par l'&eacute;tude et sp&eacute;cifier les raisons s'il y a lieu.</label>\n".
			"		" . produits_papeterie_utilisee($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_satisfait_impression_bool\">Est-ce que l'&eacute;tude est satisfaite des produits et services rendus par Impression Rive-Sud&nbsp;?</label>\n".
			"		" . s5_satisfait_impression_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_satisfait_impression_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s5_satisfait_impression_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_utilise_fournisseur_papier_bool\">Est-ce que l'&eacute;tude utilise un autre fournisseur pour l'achat de sa papeterie&nbsp;?</label>\n".
			"		" . s5_utilise_fournisseur_papier_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_utilise_fournisseur_papier_sioui\">Si <b>OUI</b>, quels sont les articles et les quantit&eacute;s approximatives&nbsp;:</label>\n".
			"		" . s5_utilise_fournisseur_papier_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_boudine_dossiers_bool\">Est-ce que l'&eacute;tude boudine les dossiers importants &agrave; exp&eacute;dier &agrave; ses clients&nbsp;?</label>\n".
			"		" . s5_boudine_dossiers_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_boudine_dossiers_sioui\">Si <b>OUI</b>, veuillez les &eacute;num&eacute;rer&nbsp;:</label>\n".
			"		" . s5_boudine_dossiers_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s5_boudine_dossiers_exemple_bool\">Est-ce que vous avez au moins un exemplaire&nbsp;?</label>\n".
			"		" . s5_boudine_dossiers_exemple_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s5_notes_reseau\">Notes suppl&eacute;mentaires&nbsp;?</label>\n".
			"		" . s5_notes_reseau($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	function s5_logo_visible_ext_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_logo_visible_ext_bool\" id=\"s5_logo_visible_ext_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_logo_visible_ext_bool\" id=\"s5_logo_visible_ext_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_logo_visible_ext_bool\" id=\"s5_logo_visible_ext_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_logo_visible_ext_bool\" id=\"s5_logo_visible_ext_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_logo_visible_ext_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_logo_visible_ext_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_logo_visible_int_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_logo_visible_int_bool\" id=\"s5_logo_visible_int_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_logo_visible_int_bool\" id=\"s5_logo_visible_int_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_logo_visible_int_bool\" id=\"s5_logo_visible_int_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_logo_visible_int_bool\" id=\"s5_logo_visible_int_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_logo_visible_int_bool\" id=\"s5_logo_visible_int_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_logo_visible_int_bool\" id=\"s5_logo_visible_int_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_produits_visibles_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_produits_visibles_bool\" id=\"s5_produits_visibles_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_produits_visibles_bool\" id=\"s5_produits_visibles_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_produits_visibles_bool\" id=\"s5_produits_visibles_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_produits_visibles_bool\" id=\"s5_produits_visibles_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_produits_visibles_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_produits_visibles_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_produits_visibles_sioui($action, $parameters){
		global $DB;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<div class=\"produits_visibles_entry\">\n".
					"	<table>\n";

				$DB->query(
					"SELECT `key`, `nom` ".
					"FROM `produits` " .
					"WHERE `identification_reseau` = '1' ".
					"ORDER BY `nom`;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$produits_key = $DB->getField("key");
					if ($nb == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"hidden\" name=\"produits_visibles_produits_key[" . $produits_key . "]\" id=\"produits_visibles_produits_key[" . $produits_key . "]\" value=\"" . $produits_key . "\" />\n".
						"	<input type=\"hidden\" name=\"produits_visibles_nom[" . $produits_key . "]\" id=\"produits_visibles_nom[" . $produits_key . "]\" value=\"" . $DB->getField("nom") . "\" />\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"produits_visibles_visible[" . $produits_key . "]\" id=\"produits_visibles_visible[" . $produits_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $DB->getField("nom") . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"produits_visibles_entry\">\n".
					"	<table>\n";

				$DB->query(
					"SELECT `key`, `nom` ".
					"FROM `produits` " .
					"WHERE `identification_reseau` = '1' ".
					"ORDER BY `nom`;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$produits_key = $DB->getField("key");
					if ($nb == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"produits_visibles_visible[" . $produits_key . "]\" id=\"produits_visibles_visible[" . $produits_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $DB->getField("nom") . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"produits_visibles_entry\">\n".
							"	<table>\n";
		
						$DB->query(
							"SELECT `key`, `nom` ".
							"FROM `produits` " .
							"WHERE `identification_reseau` = '1' ".
							"ORDER BY `nom`;"
						);
						$nb = 0;
						$nb_cols = 0;
						while ($DB->next_record()){
							$nb++;
							$nb_cols++;
							$produits_key = $DB->getField("key");
							if ($nb == 1) {
								$nb_lastcol = 0;
								$html .=
									"<tr>\n";
							}
							$nb_lastcol++;
							$html .=
								"<td>\n".
								"	<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_produits_visibles_visible[" . $produits_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $DB->getField("nom") . "</span>\n".
								"</td>\n";
							if ($nb_cols == 4) {
								$nb_cols = 0;
								$html .=
									"</tr>\n";
							}
						}
						if ($nb_lastcol < 4) {
							$html .=
								str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
								"</tr>\n";
						}
						$html .=
							"	</table>\n".
							"</div>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}
	
	function s5_produits_visibles_sinon($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_produits_visibles_sinon\" id=\"s5_produits_visibles_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_produits_visibles_sinon\" id=\"s5_produits_visibles_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s5_produits_visibles_sinon\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;

		}
		return $html;
	}

	function s5_utilise_brochure_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_utilise_brochure_bool\" id=\"s5_utilise_brochure_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_utilise_brochure_bool\" id=\"s5_utilise_brochure_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_utilise_brochure_bool\" id=\"s5_utilise_brochure_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_utilise_brochure_bool\" id=\"s5_utilise_brochure_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_utilise_brochure_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_utilise_brochure_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_message_attente_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_attente_bool\" id=\"s5_message_attente_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_attente_bool\" id=\"s5_message_attente_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_attente_bool\" id=\"s5_message_attente_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_attente_bool\" id=\"s5_message_attente_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_message_attente_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_message_attente_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_message_courtoisie_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_courtoisie_bool\" id=\"s5_message_courtoisie_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_courtoisie_bool\" id=\"s5_message_courtoisie_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_courtoisie_bool\" id=\"s5_message_courtoisie_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_courtoisie_bool\" id=\"s5_message_courtoisie_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_message_courtoisie_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_message_courtoisie_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_message_courtoisie_sinon_interet($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_courtoisie_sinon_interet\" id=\"s5_message_courtoisie_sinon_interet_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_courtoisie_sinon_interet\" id=\"s5_message_courtoisie_sinon_interet_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_courtoisie_sinon_interet\" id=\"s5_message_courtoisie_sinon_interet_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_message_courtoisie_sinon_interet\" id=\"s5_message_courtoisie_sinon_interet_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_message_courtoisie_sinon_interet\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_message_courtoisie_sinon_interet\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_pagesblanches_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_pagesblanches_bool\" id=\"s5_affiche_pagesblanches_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_pagesblanches_bool\" id=\"s5_affiche_pagesblanches_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_pagesblanches_bool\" id=\"s5_affiche_pagesblanches_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_pagesblanches_bool\" id=\"s5_affiche_pagesblanches_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_affiche_pagesblanches_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_affiche_pagesblanches_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_pagesblanches_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s5_affiche_pagesblanches_doc_num'] == "" && $parameters['s5_affiche_pagesblanches_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_affiche_pagesblanches_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_affiche_pagesblanches_doc_num\" id=\"s5_affiche_pagesblanches_doc_num\" value=\"" . $parameters['s5_affiche_pagesblanches_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_pagesblanches_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_affiche_pagesblanches_doc_num_del\" id=\"s5_affiche_pagesblanches_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s5_affiche_pagesblanches_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_affiche_pagesblanches_doc_num_tmp\" id=\"s5_affiche_pagesblanches_doc_num_tmp\" value=\"" . $parameters['s5_affiche_pagesblanches_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_pagesblanches_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_affiche_pagesblanches_doc_num_tmp_del\" id=\"s5_affiche_pagesblanches_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s5_affiche_pagesblanches_doc_num_up\" id=\"s5_affiche_pagesblanches_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_affiche_pagesblanches_doc_phy\" id=\"s5_affiche_pagesblanches_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s5_affiche_pagesblanches_doc_num'] == "" && $parameters['s5_affiche_pagesblanches_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_affiche_pagesblanches_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_pagesblanches_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_affiche_pagesblanches_doc_phy\" id=\"s5_affiche_pagesblanches_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<br />\n";
		
						if ($compare_parameters['s5_affiche_pagesblanches_doc_num'] == "" && $compare_parameters['s5_affiche_pagesblanches_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s5_affiche_pagesblanches_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s5_affiche_pagesblanches_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s5_affiche_pagesblanches_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_pagesblanches_sinon($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_affiche_pagesblanches_sinon\" id=\"s5_affiche_pagesblanches_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_affiche_pagesblanches_sinon\" id=\"s5_affiche_pagesblanches_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s5_affiche_pagesblanches_sinon\" id=\"s5_affiche_pagesblanches_sinon\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_pagesjaunes_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_pagesjaunes_bool\" id=\"s5_affiche_pagesjaunes_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_pagesjaunes_bool\" id=\"s5_affiche_pagesjaunes_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_pagesjaunes_bool\" id=\"s5_affiche_pagesjaunes_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_pagesjaunes_bool\" id=\"s5_affiche_pagesjaunes_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_affiche_pagesjaunes_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_affiche_pagesjaunes_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_pagesjaunes_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s5_affiche_pagesjaunes_doc_num'] == "" && $parameters['s5_affiche_pagesjaunes_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_affiche_pagesjaunes_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_affiche_pagesjaunes_doc_num\" id=\"s5_affiche_pagesjaunes_doc_num\" value=\"" . $parameters['s5_affiche_pagesjaunes_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_pagesjaunes_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_affiche_pagesjaunes_doc_num_del\" id=\"s5_affiche_pagesjaunes_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s5_affiche_pagesjaunes_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_affiche_pagesjaunes_doc_num_tmp\" id=\"s5_affiche_pagesjaunes_doc_num_tmp\" value=\"" . $parameters['s5_affiche_pagesjaunes_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_pagesjaunes_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_affiche_pagesjaunes_doc_num_tmp_del\" id=\"s5_affiche_pagesjaunes_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s5_affiche_pagesjaunes_doc_num_up\" id=\"s5_affiche_pagesjaunes_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_affiche_pagesjaunes_doc_phy\" id=\"s5_affiche_pagesjaunes_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s5_affiche_pagesjaunes_doc_num'] == "" && $parameters['s5_affiche_pagesjaunes_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_affiche_pagesjaunes_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_pagesjaunes_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_affiche_pagesjaunes_doc_phy\" id=\"s5_affiche_pagesjaunes_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<br />\n";
		
						if ($compare_parameters['s5_affiche_pagesjaunes_doc_num'] == "" && $compare_parameters['s5_affiche_pagesjaunes_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s5_affiche_pagesjaunes_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s5_affiche_pagesjaunes_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s5_affiche_pagesjaunes_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_pagesjaunes_sinon($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_affiche_pagesjaunes_sinon\" id=\"s5_affiche_pagesjaunes_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_affiche_pagesjaunes_sinon\" id=\"s5_affiche_pagesjaunes_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s5_affiche_pagesjaunes_sinon\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;

		}
		return $html;
	}

	function s5_affiche_judiciaire_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_judiciaire_bool\" id=\"s5_affiche_judiciaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_judiciaire_bool\" id=\"s5_affiche_judiciaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_judiciaire_bool\" id=\"s5_affiche_judiciaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_judiciaire_bool\" id=\"s5_affiche_judiciaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_affiche_judiciaire_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_affiche_judiciaire_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_judiciaire_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s5_affiche_judiciaire_doc_num'] == "" && $parameters['s5_affiche_judiciaire_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_affiche_judiciaire_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_affiche_judiciaire_doc_num\" id=\"s5_affiche_judiciaire_doc_num\" value=\"" . $parameters['s5_affiche_judiciaire_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_judiciaire_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_affiche_judiciaire_doc_num_del\" id=\"s5_affiche_judiciaire_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s5_affiche_judiciaire_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_affiche_judiciaire_doc_num_tmp\" id=\"s5_affiche_judiciaire_doc_num_tmp\" value=\"" . $parameters['s5_affiche_judiciaire_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_judiciaire_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_affiche_judiciaire_doc_num_tmp_del\" id=\"s5_affiche_judiciaire_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s5_affiche_judiciaire_doc_num_up\" id=\"s5_affiche_judiciaire_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_affiche_judiciaire_doc_phy\" id=\"s5_affiche_judiciaire_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s5_affiche_judiciaire_doc_num'] == "" && $parameters['s5_affiche_judiciaire_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_affiche_judiciaire_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_judiciaire_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_affiche_judiciaire_doc_phy\" id=\"s5_affiche_judiciaire_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<br />\n";
		
						if ($compare_parameters['s5_affiche_judiciaire_doc_num'] == "" && $compare_parameters['s5_affiche_judiciaire_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s5_affiche_judiciaire_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s5_affiche_judiciaire_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s5_affiche_judiciaire_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_judiciaire_sinon($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_affiche_judiciaire_sinon\" id=\"s5_affiche_judiciaire_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_affiche_judiciaire_sinon\" id=\"s5_affiche_judiciaire_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s5_affiche_judiciaire_sinon\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;

		}
		return $html;
	}

	function s5_affiche_emplois_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_emplois_bool\" id=\"s5_affiche_emplois_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_emplois_bool\" id=\"s5_affiche_emplois_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_emplois_bool\" id=\"s5_affiche_emplois_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_emplois_bool\" id=\"s5_affiche_emplois_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_affiche_emplois_bool\" id=\"s5_affiche_emplois_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_affiche_emplois_bool\" id=\"s5_affiche_emplois_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_emplois_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s5_affiche_emplois_doc_num'] == "" && $parameters['s5_affiche_emplois_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_affiche_emplois_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_affiche_emplois_doc_num\" id=\"s5_affiche_emplois_doc_num\" value=\"" . $parameters['s5_affiche_emplois_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_emplois_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_affiche_emplois_doc_num_del\" id=\"s5_affiche_emplois_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s5_affiche_emplois_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_affiche_emplois_doc_num_tmp\" id=\"s5_affiche_emplois_doc_num_tmp\" value=\"" . $parameters['s5_affiche_emplois_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_emplois_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_affiche_emplois_doc_num_tmp_del\" id=\"s5_affiche_emplois_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s5_affiche_emplois_doc_num_up\" id=\"s5_affiche_emplois_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_affiche_emplois_doc_phy\" id=\"s5_affiche_emplois_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s5_affiche_emplois_doc_num'] == "" && $parameters['s5_affiche_emplois_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_affiche_emplois_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_emplois_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_affiche_emplois_doc_phy\" id=\"s5_affiche_emplois_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<br />\n";
		
						if ($compare_parameters['s5_affiche_emplois_doc_num'] == "" && $compare_parameters['s5_affiche_emplois_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s5_affiche_emplois_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s5_affiche_emplois_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s5_affiche_emplois_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_emplois_sinon($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_affiche_emplois_sinon\" id=\"s5_affiche_emplois_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_affiche_emplois_sinon\" id=\"s5_affiche_emplois_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s5_affiche_emplois_sinon\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_publication_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_publication_bool\" id=\"s5_affiche_publication_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_publication_bool\" id=\"s5_affiche_publication_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_publication_bool\" id=\"s5_affiche_publication_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_affiche_publication_bool\" id=\"s5_affiche_publication_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_affiche_publication_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_affiche_publication_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_affiche_publication_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s5_affiche_publication_doc_num'] == "" && $parameters['s5_affiche_publication_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_affiche_publication_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_affiche_publication_doc_num\" id=\"s5_affiche_publication_doc_num\" value=\"" . $parameters['s5_affiche_publication_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_publication_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_affiche_publication_doc_num_del\" id=\"s5_affiche_publication_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s5_affiche_publication_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_affiche_publication_doc_num_tmp\" id=\"s5_affiche_publication_doc_num_tmp\" value=\"" . $parameters['s5_affiche_publication_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_publication_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_affiche_publication_doc_num_tmp_del\" id=\"s5_affiche_publication_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s5_affiche_publication_doc_num_up\" id=\"s5_affiche_publication_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_affiche_publication_doc_phy\" id=\"s5_affiche_publication_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s5_affiche_publication_doc_num'] == "" && $parameters['s5_affiche_publication_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_affiche_publication_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_affiche_publication_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_affiche_publication_doc_phy\" id=\"s5_affiche_publication_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<br />\n";
		
						if ($compare_parameters['s5_affiche_publication_doc_num'] == "" && $compare_parameters['s5_affiche_publication_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s5_affiche_publication_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s5_affiche_publication_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s5_affiche_publication_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n".
							"</div>\n";
					}
				}
				break;
		}

		return $html;
	}

	function s5_affiche_publication_sinon($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_affiche_publication_sinon\" id=\"s5_affiche_publication_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_affiche_publication_sinon\" id=\"s5_affiche_publication_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s5_affiche_publication_sinon\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function produits_papeterie_utilisee($action, $parameters) {
		global $DB;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<div class=\"papeterie_utilisee_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th rowspan=\"2\">Article</th><th colspan=\"2\">Quantit&eacute;s</th><th colspan=\"3\">L'utilisation de la papeterie</th><th rowspan=\"2\">Les raisons</th>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>2003</th><th>2004</th><th>Jamais</th><th>Parfois</th><th>Toujours</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";

				$DB->query(
					"SELECT `key`, `nom` ".
					"FROM `produits` ".
					"WHERE `produits_categories_key` = '1';"
				);
				while ($DB->next_record()) {
					$produits_key = $DB->getField("key");
					$nom = $DB->getField("nom");

					$html .=
						"			<tr>\n".
						"				<td width=\"100px\">\n".
						"					<input type=\"hidden\" name=\"produits_papeterie_utilisee_produits_key[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_produits_key[" . $produits_key . "]\" value=\"" . $produits_key . "\" />\n".
						"					<input type=\"hidden\" name=\"produits_papeterie_utilisee_nom[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_nom[" . $produits_key . "]\" value=\"" . $nom . "\" />\n".
						"					" . $nom . "\n".
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"text\" class=\"text\" style=\"width: 40px\" style=\"text-align: right;\" name=\"produits_papeterie_utilisee_qte_2003[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_qte_2003[" . $produits_key . "]\" value=\"\" />\n".						
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"text\" class=\"text\" style=\"width: 40px\" style=\"text-align: right;\" name=\"produits_papeterie_utilisee_qte_2004[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_qte_2004[" . $produits_key . "]\" value=\"\" />\n".						
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"radio\" class=\"radio\" name=\"produits_papeterie_utilisee_utilisation[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_utilisation_jamais[" . $produits_key . "]\" value=\"Jamais\" />\n".						
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"radio\" class=\"radio\" name=\"produits_papeterie_utilisee_utilisation[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_utilisation_parfois[" . $produits_key . "]\" value=\"Parfois\" />\n".						
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"radio\" class=\"radio\" name=\"produits_papeterie_utilisee_utilisation[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_utilisation_toujours[" . $produits_key . "]\" value=\"Toujours\" />\n".						
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"text\" class=\"text\" style=\"width: 92%;\" name=\"produits_papeterie_utilisee_raisons[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_raisons[" . $produits_key . "]\" value=\"\" />\n".						
						"				</td>\n".
						"			</tr>\n";
				}
				
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"papeterie_utilisee_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th rowspan=\"2\">Article</th><th colspan=\"2\">Quantit&eacute;s</th><th colspan=\"3\">L'utilisation de la papeterie</th><th rowspan=\"2\">Les raisons</th>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>2003</th><th>2004</th><th>Jamais</th><th>Parfois</th><th>Toujours</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";

				$DB->query(
					"SELECT `key`, `nom` ".
					"FROM `produits` ".
					"WHERE `produits_categories_key` = '1';"
				);
				while ($DB->next_record()) {
					$produits_key = $DB->getField("key");
					$nom = $DB->getField("nom");

					$html .=
						"			<tr>\n".
						"				<td width=\"100px\">\n".
						"					<input type=\"hidden\" name=\"produits_papeterie_utilisee_produits_key[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_produits_key[" . $produits_key . "]\" value=\"" . $produits_key . "\" />\n".
						"					<input type=\"hidden\" name=\"produits_papeterie_utilisee_nom[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_nom[" . $produits_key . "]\" value=\"" . $nom . "\" />\n".
						"					" . $nom . "\n".
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"text\" class=\"text\" style=\"width: 40px\" style=\"text-align: right;\" name=\"produits_papeterie_utilisee_qte_2003[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_qte_2003[" . $produits_key . "]\" value=\"\" readonly=\"readonly\" />\n".						
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"text\" class=\"text\" style=\"width: 40px\" style=\"text-align: right;\" name=\"produits_papeterie_utilisee_qte_2004[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_qte_2004[" . $produits_key . "]\" value=\"\" readonly=\"readonly\" />\n".						
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"radio\" class=\"radio\" name=\"produits_papeterie_utilisee_utilisation[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_utilisation_jamais[" . $produits_key . "]\" value=\"Jamais\" />\n".						
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"radio\" class=\"radio\" name=\"produits_papeterie_utilisee_utilisation[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_utilisation_parfois[" . $produits_key . "]\" value=\"Parfois\" />\n".						
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"radio\" class=\"radio\" name=\"produits_papeterie_utilisee_utilisation[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_utilisation_toujours[" . $produits_key . "]\" value=\"Toujours\" />\n".						
						"				</td>\n".
						"				<td>\n".
						"					<input type=\"text\" class=\"text\" style=\"width: 92%;\" name=\"produits_papeterie_utilisee_raisons[" . $produits_key . "]\" id=\"produits_papeterie_utilisee_raisons[" . $produits_key . "]\" value=\"\" readonly=\"readonly\" />\n".						
						"				</td>\n".
						"			</tr>\n";
				}
				
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"papeterie_utilisee_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th rowspan=\"2\">Article</th><th colspan=\"2\">Quantit&eacute;s</th><th colspan=\"3\">L'utilisation de la papeterie</th><th rowspan=\"2\">Les raisons</th>\n".
							"			</tr>\n".
							"			<tr>\n".
							"				<th>2003</th><th>2004</th><th>Jamais</th><th>Parfois</th><th>Toujours</th>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n";
		
						$DB->query(
							"SELECT `key`, `nom` ".
							"FROM `produits` ".
							"WHERE `produits_categories_key` = '1';"
						);
						while ($DB->next_record()) {
							$produits_key = $DB->getField("key");
							$nom = $DB->getField("nom");
		
							$html .=
								"			<tr>\n".
								"				<td width=\"100px\">\n".
								"					" . $nom . "\n".
								"				</td>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 40px\" style=\"text-align: right;\" name=\"" . $compare_sid . "_produits_papeterie_utilisee_qte_2003[" . $produits_key . "]\" value=\"\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 40px\" style=\"text-align: right;\" name=\"" . $compare_sid . "_produits_papeterie_utilisee_qte_2004[" . $produits_key . "]\" value=\"\" readonly=\"readonly\" />\n".						
								"				</td>\n".
								"				<td>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_produits_papeterie_utilisee_utilisation[" . $produits_key . "]\" value=\"Jamais\" />\n".						
								"				</td>\n".
								"				<td>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_produits_papeterie_utilisee_utilisation[" . $produits_key . "]\" value=\"Parfois\" />\n".						
								"				</td>\n".
								"				<td>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_produits_papeterie_utilisee_utilisation[" . $produits_key . "]\" value=\"Toujours\" />\n".						
								"				</td>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 92%;\" name=\"" . $compare_sid . "_produits_papeterie_utilisee_raisons[" . $produits_key . "]\" value=\"\" readonly=\"readonly\" />\n".						
								"				</td>\n".
								"			</tr>\n";
						}
						
						$html .=
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_satisfait_impression_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_satisfait_impression_bool\" id=\"s5_satisfait_impression_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_satisfait_impression_bool\" id=\"s5_satisfait_impression_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_satisfait_impression_bool\" id=\"s5_satisfait_impression_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_satisfait_impression_bool\" id=\"s5_satisfait_impression_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_satisfait_impression_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_satisfait_impression_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_satisfait_impression_sinon($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_satisfait_impression_sinon\" id=\"s5_satisfait_impression_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_satisfait_impression_sinon\" id=\"s5_satisfait_impression_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s5_satisfait_impression_sinon\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;

		}
		return $html;
	}

	function s5_utilise_fournisseur_papier_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_utilise_fournisseur_papier_bool\" id=\"s5_utilise_fournisseur_papier_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_utilise_fournisseur_papier_bool\" id=\"s5_utilise_fournisseur_papier_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_utilise_fournisseur_papier_bool\" id=\"s5_utilise_fournisseur_papier_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_utilise_fournisseur_papier_bool\" id=\"s5_utilise_fournisseur_papier_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_utilise_fournisseur_papier_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_utilise_fournisseur_papier_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_utilise_fournisseur_papier_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s5_utilise_fournisseur_papier_doc_num'] == "" && $parameters['s5_utilise_fournisseur_papier_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_utilise_fournisseur_papier_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_utilise_fournisseur_papier_doc_num\" id=\"s5_utilise_fournisseur_papier_doc_num\" value=\"" . $parameters['s5_utilise_fournisseur_papier_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_utilise_fournisseur_papier_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_utilise_fournisseur_papier_doc_num_del\" id=\"s5_utilise_fournisseur_papier_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s5_utilise_fournisseur_papier_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s5_utilise_fournisseur_papier_doc_num_tmp\" id=\"s5_utilise_fournisseur_papier_doc_num_tmp\" value=\"" . $parameters['s5_utilise_fournisseur_papier_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_utilise_fournisseur_papier_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s5_utilise_fournisseur_papier_doc_num_tmp_del\" id=\"s5_utilise_fournisseur_papier_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s5_utilise_fournisseur_papier_doc_num_up\" id=\"s5_utilise_fournisseur_papier_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_utilise_fournisseur_papier_doc_phy\" id=\"s5_utilise_fournisseur_papier_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s5_utilise_fournisseur_papier_doc_num'] == "" && $parameters['s5_utilise_fournisseur_papier_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s5_utilise_fournisseur_papier_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s5_utilise_fournisseur_papier_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s5_utilise_fournisseur_papier_doc_phy\" id=\"s5_utilise_fournisseur_papier_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<br />\n";
		
						if ($compare_parameters['s5_utilise_fournisseur_papier_doc_num'] == "" && $compare_parameters['s5_utilise_fournisseur_papier_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s5_utilise_fournisseur_papier_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s5_utilise_fournisseur_papier_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}

						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s5_utilise_fournisseur_papier_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_boudine_dossiers_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_boudine_dossiers_bool\" id=\"s5_boudine_dossiers_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_boudine_dossiers_bool\" id=\"s5_boudine_dossiers_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_boudine_dossiers_bool\" id=\"s5_boudine_dossiers_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_boudine_dossiers_bool\" id=\"s5_boudine_dossiers_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_boudine_dossiers_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_boudine_dossiers_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_boudine_dossiers_sioui($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .= 
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_boudine_dossiers_sioui\" id=\"s5_boudine_dossiers_sioui\"></textarea>\n";
				break;

			case "read":
				$html .= 
					"<textarea rows=\"4\" cols=\"40\" name=\"s5_boudine_dossiers_sioui\" id=\"s5_boudine_dossiers_sioui\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s5_boudine_dossiers_sioui\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_boudine_dossiers_exemple_bool($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_boudine_dossiers_exemple_bool\" id=\"s5_boudine_dossiers_exemple_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_boudine_dossiers_exemple_bool\" id=\"s5_boudine_dossiers_exemple_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s5_boudine_dossiers_exemple_bool\" id=\"s5_boudine_dossiers_exemple_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s5_boudine_dossiers_exemple_bool\" id=\"s5_boudine_dossiers_exemple_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"s5_boudine_dossiers_exemple_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s5_boudine_dossiers_exemple_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s5_notes_reseau($action, $parameters) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<textarea rows=\"10\" cols=\"40\" name=\"s5_notes_reseau\" id=\"s5_notes_reseau\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"10\" cols=\"40\" name=\"s5_notes_reseau\" id=\"s5_notes_reseau\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"10\" cols=\"40\" name=\"" . $compare_sid . "_s5_notes_reseau\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

?>