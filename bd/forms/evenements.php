<?PHP
	global $DB;

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL;
		
		$_FORM =
			
			form_header($action).
			
			"			<label for=\"nom\">Nom de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . nom($action) . "\n".
			"		</div>\n".

			"			<label for=\"date_fin_inscription\">Date limite d'inscription &agrave; cet &eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_fin_inscription($action) . "\n".
			"		</div>\n".

			"			<label for=\"nom_responsable\">Nom du responsable de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . nom_responsable($action) . "\n".
			"		</div>\n".

			"			<label for=\"telephone_responsable\">Num&eacute;ro de t&eacute;l&eacute;phone du responsable&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . telephone_responsable($action) . "\n".
			"		</div>\n".

			"			<a name=\"date_debut_v\"></a><label for=\"date_debut\">Date de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_debut($action) . "\n".
			"		</div>\n".

			"			<a name=\"date_fin_v\"></a><label for=\"date_fin\">Date de fin, s'il y a lieu&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_fin($action) . "\n".
			"		</div>\n".

			"			<a name=\"nombre_jours_v\"></a><label for=\"nombre_jours\">Nombre jours : </label>\n".
			"		<div class=\"field\">\n".
			"			" . nombre_jours($action) . "\n".
			"		</div>\n".

			"			<label for=\"date_precision\">Pr&eacute;cisions sur la(les) date(s) : (exemple&nbsp;: 2 mercredis en ligne...)</label>\n".
			"		<div class=\"field\">\n".
			"			" . date_precision($action) . "\n".
			"		</div>\n".

			"			<label for=\"heure\">Heure de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . heure($action) . "\n".
			"		</div>\n".

			"			<label for=\"coordonnees\">Coordonn&eacute;es où se tient l'&eacute;v&eacute;nement, si applicable : </label>\n".
			"		<div class=\"field\">\n".
			"			" . coordonnees($action) . "\n".
			"		</div>\n".

			"			<label for=\"salle\">Nom de la salle&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . salle($action) . "\n".
			"		</div>\n".

			"			<label for=\"description\">Description de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . description($action) . "\n".
			"		</div>\n".

			"			<label for=\"conferencier\">Nom du conf&eacute;rencier, s'il y a lieu&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . conferencier($action) . "\n".
			"		</div>\n".

			"			<a name=\"prix_v\"></a><label for=\"prix\">Prix par personne pour assister &agrave; l'&eacute;v&eacute;nement, si applicable : </label>\n".
			"		<div class=\"field\">\n".
			"			" . prix($action) . "\n".
			"		</div>\n".

			"			<label for=\"notes\">Notes suppl&eacute;mentaires&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . notes($action) . "\n".
			"		</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html =
					"<form name=\"evenements\" action=\"evenements.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n".
					"	<input type=\"hidden\" name=\"event_key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "modify":
				$html =	
					"<form name=\"evenements\" action=\"evenements.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"event_key\" value=\"\" />\n".
					"	<input type=\"hidden\" name=\"key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "read":
				$html =
					"	<div class=\"readform\">\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "modify":
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "read":
				$html =
					"	</div>\n";
				break;
		}
		return $html;
	}


	function nom($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"nom\" id=\"nom\" value=\"\" tabindex=\"20\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"nom\" id=\"nom\" value=\"\" tabindex=\"20\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_nom /HIDDEN_nom---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function date_fin_inscription($action){
		$html="";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"date_fin_inscription\" id=\"date_fin_inscription\" value=\"\" tabindex=\"20\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date_fin_inscription\" id=\"date_fin_inscription\" value=\"\" tabindex=\"20\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date_fin_inscription /HIDDEN_date_fin_inscription---></span><br/><br/>\n";
				break;
		}
		return $html;
		
	}

	function nom_responsable($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"nom_responsable\" value=\"\" tabindex=\"21\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"nom_responsable\" value=\"\" tabindex=\"21\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_nom_responsable /HIDDEN_nom_responsable---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function telephone_responsable($action){

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"telephone_responsable\" value=\"\" tabindex=\"22\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"telephone_responsable\" value=\"\" tabindex=\"22\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_telephone_responsable /HIDDEN_telephone_responsable---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function date_debut($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"date_debut\" id=\"date_debut\" value=\"\" tabindex=\"23\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date_debut\" id=\"date_debut\" value=\"\" tabindex=\"23\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date_debut /HIDDEN_date_debut---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function date_fin($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"date_fin\" id=\"date_fin\" value=\"\" tabindex=\"24\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date_fin\" id=\"date_fin\" value=\"\" tabindex=\"24\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date_fin /HIDDEN_date_fin---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function nombre_jours($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"nombre_jours\" id=\"nombre_jours\" value=\"\" tabindex=\"25\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"nombre_jours\" id=\"nombre_jours\" value=\"\" tabindex=\"25\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_nombre_jours /HIDDEN_nombre_jours---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function date_precision($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"date_precision\" tabindex=\"26\"></textarea>\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"date_precision\" tabindex=\"26\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date_precision /HIDDEN_date_precision---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function heure($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"heure\" value=\"\" tabindex=\"27\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"heure\" value=\"\" tabindex=\"27\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_heure /HIDDEN_heure---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function coordonnees($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"coordonnees\" tabindex=\"28\"></textarea>\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"coordonnees\" tabindex=\"28\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_coordonnees /HIDDEN_coordonnees---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function salle($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"salle\" value=\"\" tabindex=\"29\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"salle\" value=\"\" tabindex=\"29\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_salle /HIDDEN_salle---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function description($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"description\" tabindex=\"30\"></textarea>\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"description\" tabindex=\"30\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_description /HIDDEN_description---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function conferencier($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"conferencier\" value=\"\" tabindex=\"31\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"conferencier\" value=\"\" tabindex=\"31\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_conferencier /HIDDEN_conferencier---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function prix($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"prix\" id=\"prix\" value=\"\" tabindex=\"32\" /> (format 1000.00)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"prix\" id=\"prix\" value=\"\" tabindex=\"32\" /> (format 1000.00)\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_prix /HIDDEN_prix---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function notes($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"notes\" tabindex=\"33\"></textarea>\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"notes\" tabindex=\"33\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_notes /HIDDEN_notes---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

?>