<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "4").

			"	<h2>Constitution de l'&eacute;tude</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s4_constitution_min_bool\">Est-ce que l'&eacute;tude est constitu&eacute;e d'au moins trois notaires dont deux associ&eacute;s&nbsp;?</label>\n".
			"		" . s4_constitution_min_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s4_constitution_min_sinon_mesures\">Si <b>NON</b>, indiquer les mesures qui seront mises en place pour r&eacute;pondre &agrave; cette norme et dans quel d&eacute;lai&nbsp;:</label>\n".
			"		" . s4_constitution_min_sinon_mesures($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>V&eacute;rification du contrat d'adh&eacute;sion</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"signataires_adhesion_employes_key\">Le contrat d'adh&eacute;sion a &eacute;t&eacute; sign&eacute; par l'actionnaire (compagnie ou soci&eacute;t&eacute;) suivant ou par les associ&eacute;s de l'&eacute;tude suivants&nbsp;:</label>\n".
			"		" . signataires_adhesion($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s4_contrat_signe_bool\">Est-ce que le contrat d'adh&eacute;sion a &eacute;t&eacute; sign&eacute; par l'actionnaire ou ratifi&eacute; par tous les associ&eacute;s&nbsp;?</label>\n".
			"		" . s4_contrat_signe_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s4_entente_signee_bool\">Est-ce que l'&eacute;tude a fait signer par ses salari&eacute;s (notaires, collaboratrices, techniciennes, autres) une entente de confidentialit&eacute; quant aux produits et processus d&eacute;velopp&eacute;s par le r&eacute;seau et utilis&eacute; par l'&eacute;tudes&nbsp;?</label>\n".
			"		" . s4_entente_signee_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s4_entente_signe_doc_phy\">Si <b>OUI</b>, annexer un document&nbsp;:</label>\n".
			"		" . s4_entente_signee_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s4_entente_signee_sinon\">Si <b>NON</b>, expliquer les raisons&nbsp;:</label>\n".
			"		" . s4_entente_signee_sinon($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}
		
	function s4_constitution_min_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s4_constitution_min_bool\" id=\"s4_constitution_min_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>\n".
					"<input type=\"radio\" class=\"radio\" name=\"s4_constitution_min_bool\" id=\"s4_constitution_min_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>\n";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s4_constitution_min_bool\" id=\"s4_constitution_min_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>\n".
					"<input type=\"radio\" class=\"radio\" name=\"s4_constitution_min_bool\" id=\"s4_constitution_min_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s4_constitution_min_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s4_constitution_min_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s4_constitution_min_sinon_mesures($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s4_constitution_min_sinon_mesures\" id=\"s4_constitution_min_sinon_mesures\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s4_constitution_min_sinon_mesures\" id=\"s4_constitution_min_sinon_mesures\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s4_constitution_min_sinon_mesures\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}	
		return $html;
	}

	function signataires_adhesion($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$employes_associes = get_employes_associes($parameters);
				$employes_associes_selectOptions = employes_associes_selectOptions($employes_associes, "signataires_adhesion_employes_key");
				$html .=
					"<select name=\"signataires_adhesion_employes_key[]\" id=\"signataires_adhesion_employes_key\" style=\"width: 222px;\" multiple=\"multiple\" size=\"6\" />\n".
					"	" . $employes_associes_selectOptions . "\n".
					"</select>&nbsp; <span style=\"vertical-align: bottom;\">S&eacute;lectionner les associ&eacute;s, si applicable.</span><br />\n".
					"<br />\n".					
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s4_signataires_adhesion_actionnaire\" id=\"s4_signataires_adhesion_actionnaire\" value=\"\" />&nbsp; <span style=\"vertical-align: bottom;\">Donner le nom de l'actionnaire, si applicable.</span>\n";
				break;

			case "read":
				$employes_associes = get_employes_associes($parameters);
				$employes_associes_selectOptions = employes_associes_selectOptions($employes_associes, "signataires_adhesion_employes_key");
				$html .=
					"<select name=\"signataires_adhesion_employes_key[]\" id=\"signataires_adhesion_employes_key\" style=\"width: 222px;\" multiple=\"multiple\" size=\"6\" />\n".
					"	" . $employes_associes_selectOptions . "\n".
					"</select>&nbsp; <span style=\"vertical-align: bottom;\">S&eacute;lectionner les associ&eacute;s, si applicable.</span><br />\n".
					"<br />\n".					
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s4_signataires_adhesion_actionnaire\" id=\"s4_signataires_adhesion_actionnaire\" value=\"\" readonly=\"readonly\" />&nbsp; <span style=\"vertical-align: bottom;\">Donner le nom de l'actionnaire, si applicable.</span>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						if ($compare_color == 0) {
/* BUG */					$employes_associes = get_employes_associes($compare_parameters);
						}
						$employes_associes_selectOptions = employes_associes_selectOptions($employes_associes, $compare_sid . "_signataires_adhesion_employes_key");
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<select name=\"" . $compare_sid . "_signataires_adhesion_employes_key[]\" style=\"width: 222px;\" multiple=\"multiple\" size=\"6\" />\n".
							"	" . $employes_associes_selectOptions . "\n".
							"</select>&nbsp; <span style=\"vertical-align: bottom;\">S&eacute;lectionner les associ&eacute;s, si applicable.</span><br />\n".
							"<br />\n".					
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s4_signataires_adhesion_actionnaire\" value=\"\" readonly=\"readonly\" />&nbsp; <span style=\"vertical-align: bottom;\">Donner le nom de l'actionnaire, si applicable.</span>\n".
							"</div>\n";
					}
				}					
				break;
		}	
		return $html;
	}

	function get_employes_associes($parameters) {
		global $DB;
		$html = "";
		$DB->query(
			"SELECT DISTINCT e.key, e.nom, e.prenom ".
			"FROM `employes` AS e, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
			"WHERE ees.employes_key = e.key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $parameters['etudes_key'] . "' AND e.associe = '1' AND e.actif = '1' ".
			"ORDER BY `nom` ASC;"
		);
		while ($DB->next_record()) {
			$employes_associes[$DB->getField("key")]['key'] = $DB->getField("key");
			$employes_associes[$DB->getField("key")]['nom'] = $DB->getField("nom");
			$employes_associes[$DB->getField("key")]['prenom'] = $DB->getField("prenom");
		}
		return $employes_associes;		
	}

	function employes_associes_selectOptions($employes_associes, $selectname) {
		if (is_array($employes_associes) && count($employes_associes) > 0) {
			foreach ($employes_associes as $employes_key => $useless) {
				$value = $employes_associes[$employes_key]['key'];
				$option = $employes_associes[$employes_key]['nom'] . ", " . $employes_associes[$employes_key]['prenom'];
				$html .= "<option selectname=\"" . $selectname . "\" value=\"" . $value . "\">" . $option . "</option>\n";
			}
		}
		return $html;
	}

	function s4_contrat_signe_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s4_contrat_signe_bool\" id=\"s4_contrat_signe_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>\n".
					"<input type=\"radio\" class=\"radio\" name=\"s4_contrat_signe_bool\" id=\"s4_contrat_signe_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>\n";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s4_contrat_signe_bool\" id=\"s4_contrat_signe_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>\n".
					"<input type=\"radio\" class=\"radio\" name=\"s4_contrat_signe_bool\" id=\"s4_contrat_signe_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s4_contrat_signe_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s4_contrat_signe_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s4_entente_signee_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s4_entente_signee_bool\" id=\"s4_entente_signee_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>\n".
					"<input type=\"radio\" class=\"radio\" name=\"s4_entente_signee_bool\" id=\"s4_entente_signee_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>\n";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s4_entente_signee_bool\" id=\"s4_entente_signee_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>\n".
					"<input type=\"radio\" class=\"radio\" name=\"s4_entente_signee_bool\" id=\"s4_entente_signee_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s4_entente_signee_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s4_entente_signee_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>\n".
							"</div>\n";
					}
				}
				break;
		}		
		return $html;
	}

	function s4_entente_signee_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s4_entente_signee_doc_num'] == "" && $parameters['s4_entente_signee_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s4_entente_signee_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s4_entente_signee_doc_num\" id=\"s4_entente_signee_doc_num\" value=\"" . $parameters['s4_entente_signee_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s4_entente_signee_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s4_entente_signee_doc_num_del\" id=\"s4_entente_signee_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s4_entente_signee_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s4_entente_signee_doc_num_tmp\" id=\"s4_entente_signee_doc_num_tmp\" value=\"" . $parameters['s4_entente_signee_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s4_entente_signee_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s4_entente_signee_doc_num_tmp_del\" id=\"s4_entente_signee_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s4_entente_signee_doc_num_up\" id=\"s4_entente_signee_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s4_entente_signee_doc_phy\" id=\"s4_entente_signee_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s4_entente_signee_doc_num'] == "" && $parameters['s4_entente_signee_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s4_entente_signee_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s4_entente_signee_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s4_entente_signee_doc_phy\" id=\"s4_entente_signee_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<br />\n";

						if ($compare_parameters['s4_entente_signee_doc_num'] == "" && $compare_parameters['s4_entente_signee_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s4_entente_signee_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s4_entente_signee_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 222px;\" name=\"" . $compare_sid . "_s4_entente_signee_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s4_entente_signee_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s4_entente_signee_sinon\" id=\"s4_entente_signee_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s4_entente_signee_sinon\" id=\"s4_entente_signee_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s4_entente_signee_sinon\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}		
		return $html;
	}
?>