<?PHP
	global $DB;

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL;
		
		$_FORM =
			form_header($action).
			"<div id=\"direction_de_travail_convocation\">\n".

			"		<a name=\"date\"></a><label for=\"date\">Date de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_debut($action) . "\n".
			"		</div>\n".

			"		<a name=\"lieu\"></a><label for=\"lieu\">Lieu&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . lieu($action) . "\n".
			"		</div>\n".

			"		<a name=\"departement\"></a><label for=\"departement\">D&eacute;partement/Groupe&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . departement($action) . "\n".
			"		</div>\n".

			"		<a name=\"sujet\"></a><label for=\"sujet\">Sujet&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . sujet($action) . "\n".
			"		</div>\n".

			"		<a name=\"cout\"></a><label for=\"cout\">Co&ucirc;t&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . cout($action) . "\n".
			"		</div>\n".	

			"		<a name=\"date_limite\"></a><label for=\"date_limite\">Date limite&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_limite($action) . "\n".
			"		</div>\n".	

			"		<a name=\"note\"></a><label for=\"note\">Note&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . note($action) . "\n".
			"		</div>\n".			

			"			<label for=\"description\">Description de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . description($action) . "\n".
			"		</div>\n".

			"		<label for=\"actif\">Actif</label>\n".
			"		<div class=\"field\">\n".
			"			" . actif($action) . "\n".
			"		</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html = 
			"<script type=\"text/javascript\" src=\"/skin/js/tinymce/tinymce.min.js\"></script>\n".
			"<script type=\"text/javascript\">\n".
			"	tinymce.init({\n".
    		"		selector: \"textarea\",\n".
    		"		height: 600\n".
 			"	});\n".
			"</script>\n";

		switch ($action) {
			case "add":					
				$html .=
					"<form name=\"evenements\" action=\"journee_formation.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n".
					"	<input type=\"hidden\" name=\"id\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "modify":
				$html .=	
					"<form name=\"evenements\" action=\"journee_formation.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"id\" value=\"\" />\n".
					"	<input type=\"hidden\" name=\"key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "read":
				$html .=
					"	<div class=\"readform\">\n".
					"		<a href=\"/evenements/alerte.php?table=journee_formation&id=" . $_POST["id"] . "\" target=\"_blank\"><strong>Alerte courriel</strong></a><br />\n".
					"		<a href=\"/evenements/inscription.php?table=journee_formation&id=" . $_POST["id"] . "\" target=\"_blank\"><strong>Inscriptions</strong></a><br />\n".
					"		<a href=\"/evenements/inscription.php?table=journee_formation&id=" . $_POST["id"] . "&excel=1\" target=\"_blank\"><strong>Inscriptions Excel</strong></a><br /><br />\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "modify":
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "read":
				$html =
					"	</div>\n";
				break;
		}
		return $html;
	}

	function actif($action){
		$html="";
		switch ($action) {	
			case "add":
				$html = 
					"<select name=\"actif\">\n".
					"	<option id=\"actif\" value=\"\"></option>\n".
					"	<option id=\"actif\" value=\"1\">Oui</option>\n".
					"	<option id=\"actif\" value=\"0\">Non</option>\n".
					"</select>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html =
					"<select name=\"actif\">\n".
					"	<option id=\"actif\" value=\"\"></option>\n".
					"	<option id=\"actif\" value=\"1\">Oui</option>\n".
					"	<option id=\"actif\" value=\"0\">Non</option>\n".
					"</select>\n".
					"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_actif /HIDDEN_actif---></span><br/><br/>\n";
				break;
		}
		return $html;
		
	}

	function date_debut($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"date\" id=\"date\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date\" id=\"date\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date /HIDDEN_date---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function lieu($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"lieu\" id=\"lieu\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"lieu\" id=\"lieu\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_lieu /HIDDEN_lieu---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function cout($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"cout\" id=\"cout\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"cout\" id=\"cout\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_cout /HIDDEN_cout---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function date_limite($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"date_limite\" id=\"date_limite\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date_limite\" id=\"date_limite\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date_limite /HIDDEN_date_limite---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function note($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"note\" id=\"note\" value=\"Veuillez prendre note qu'il n'y aura aucun remboursement sur les d&eacute;sistements dans les 5 jours ouvrables pr&eacute;c&eacute;dant la formation.\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"note\" id=\"note\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_note /HIDDEN_note---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function departement($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"departement\" id=\"departement\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"departement\" id=\"departement\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_departement /HIDDEN_departement---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function sujet($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"sujet\" id=\"sujet\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"sujet\" id=\"sujet\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_sujet /HIDDEN_sujet---></span><br/><br/>\n";
				break;
		}
		return $html;
	}


	function description($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = 
					"<textarea rows=\"8\" cols=\"40\" name=\"description\">\n".
					"	<div style=\"width: 600px;\">\n".
					"		<img src=\"/skin/img/logo.png\" alt=\"PME Inter\" style=\"width: 80px; height: auto; float: left; padding-right: 30px; padding-bottom: 20px;\" />\n".
					"		<div style=\"border: dotted 2px #CC6633; padding: 10px 20px; text-align: center; width: 300px; margin-left: auto; margin-right: auto; margin-bottom: 20px;\">&Agrave; l'attention de tous les notaires et les collaboratrices du r&eacute;seau</div>\n".
					"		<h1 style=\"text-align: center;\">Journ&eacute;e de formation</h1>\n".
					"		<div style=\"text-align: center; width: 90%; margin-left: auto; margin-right: auto;\">\n".
					"			<p><strong>[DEPARTEMENT]</strong> vous propose une journ&eacute;e de formation sur les sujets suivants : <em>&laquo; [SUJET] &raquo;</em> <strong>qui d&eacute;butera &agrave; 9 h le vendredi [DATE]</strong> <strong>[LIEU]</strong></p>\n".
					"			<p style=\"text-align: center;\">Collaboratrices et notaires sont les bienvenus !</p>\n".
					"		</div>\n".
					"		<div>\n".
					"			<p style=\"color: #CC6633; font-weight: bold;\">Voici tous les d&eacute;tails de cette journ&eacute;e de formation du [DATE]</p>\n".
					"			<hr style=\"border: none; border-bottom: solid 2px #CC6633; width: 60%; margin-left: auto; margin-right: auto;\" />\n".
					"			<p>Co&ucirc;t d'inscription : [COUT]</p>\n".
					"			<p style=\"text-align: center; color: #CC6633;\">Pour la planification du repas veuillez nous informer de toutes allergies alimentaires</p>\n".
					"		</div>\n".
					"		<table border=\"0\">\n".
					"			<tr>\n".
					"				<td><strong>9 h &agrave; 11 h 45</strong></td>\n".
					"				<td></td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<td><strong>12 h &agrave; 13 h</strong></td>\n".
					"				<td></td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<td><strong>13 h &agrave; 17 h</strong></td>\n".
					"				<td></td>\n".
					"			</tr>\n".
					"		</table>\n".
					"		<div>\n".
					"			<p style=\"color: #CC6633;\">Formulaire d'inscription</p>\n".
					"			<hr style=\"border: none; border-bottom: solid 2px #CC6633; width: 60%; margin-left: auto; margin-right: auto;\" />\n".
					"			<p><strong>Pour vous inscrire</strong>, veuillez compl&eacute;ter et retourner le formulaire d'inscription ici-bas avant le [DATE_LIMITE] par t&eacute;l&eacute;copieur au (514) 874-9618 ou par courriel &agrave; <a href=\"mailto:info@pmeinter.com\">info@pmeinter.com</a>. [NOTE]</p>\n".
					"		</div>\n".
					"		[INSCRIPTIONS]\n".
					"		<hr style=\"border: none; border-bottom: solid 2px #000;\" />\n".
					"		<p style=\"font-size: 10px; text-align: center; width: 300px; margin-left: auto; margin-right: auto;\">Veuillez faire parvenir votre paiement effectu&eacute; &agrave; l'ordre de 9096-7621 Qu&eacute;bec Inc. au 100, boul. Alexis Nihon, bureau 985, ville Saint-Laurent (Qu&eacute;bec) H4M 2P5</p>\n".
					"	</div>\n".
					"</textarea>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"description\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_description /HIDDEN_description---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

?>