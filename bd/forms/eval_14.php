<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "14") .

			"	<div class=\"entry\">\n".
			"		<label for=\"s14_courtier_affilie_bool\">Est-ce que les notaires de l'&eacute;tude ont un courtier affili&eacute; au Cabinet financier&nbsp;?</label>\n".
			"		" . s14_courtier_affilie_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s14_courtier_affilie_sioui\">Si <b>OUI</b>, inscrire ses coordonn&eacute;es&nbsp;:</label>\n".
			"		" . s14_courtier_affilie_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s14_refere_clients_cabinet_bool\">Au cours de la derni&egrave;re ann&eacute;e, les notaires de l’&eacute;tude ont-ils r&eacute;f&eacute;r&eacute; des clients aupr&egrave;s du Cabinet financier&nbsp;?</label>\n".
			"		" . s14_refere_clients_cabinet_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s14_refere_clients_cabinet_sioui_nb\">Si <b>OUI</b>, combien de clients ont-ils r&eacute;f&eacute;r&eacute;&nbsp;?</label>\n".
			"		" . s14_refere_clients_cabinet_sioui_nb($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s14_refere_clients_cabinet_sioui_liste\">Si <b>OUI</b>, lister pour quels produits&nbsp;:</label>\n".
			"		" . s14_refere_clients_cabinet_sioui_liste($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s14_refere_clients_cabinet_sioui_commissions\">Si <b>OUI</b>, indiquer le montant des commissions qui leurs ont &eacute;t&eacute; vers&eacute;&nbsp;:</label>\n".
			"		" . s14_refere_clients_cabinet_sioui_commissions($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s14_affiche_cabinet_bool\">Est-ce que l'affiche lamin&eacute;e du Cabinet financier est visible dans les bureaux de l'&eacute;tude&nbsp;?</label>\n".
			"		" . s14_affiche_cabinet_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s14_affiche_cabinet_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s14_affiche_cabinet_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s14_utilise_depliant_cabinet_bool\">Est-ce que les notaires de l'&eacute;tude utilisent le d&eacute;pliant promotionnel du Cabinet financier&nbsp;?</label>\n".
			"		" . s14_utilise_depliant_cabinet_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s14_utilise_depliant_cabinet_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s14_utilise_depliant_cabinet_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s14_notaires_satisfaits_cabinet_bool\">Est-ce que les notaires de l'&eacute;tude sont satisfaits des services offerts par le Cabinet financier&nbsp;?</label>\n".
			"		" . s14_notaires_satisfaits_cabinet_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s14_notaires_satisfaits_cabinet_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s14_notaires_satisfaits_cabinet_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s14_suggestions_cabinet_bool\">Est-ce que les notaires de l'&eacute;tude auraient des am&eacute;liorations &agrave; proposer pour le Cabinet financier&nbsp;?</label>\n".
			"		" . s14_suggestions_cabinet_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s14_suggestions_cabinet_sioui\">Si <b>OUI</b>, lesquels&nbsp;:</label>\n".
			"		" . s14_suggestions_cabinet_sioui($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	function s14_courtier_affilie_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_courtier_affilie_bool\" id=\"s14_courtier_affilie_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_courtier_affilie_bool\" id=\"s14_courtier_affilie_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_courtier_affilie_bool\" id=\"s14_courtier_affilie_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_courtier_affilie_bool\" id=\"s14_courtier_affilie_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_courtier_affilie_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_courtier_affilie_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s14_courtier_affilie_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_courtier_affilie_sioui\" id=\"s14_courtier_affilie_sioui\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_courtier_affilie_sioui\" id=\"s14_courtier_affilie_sioui\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s14_courtier_affilie_sioui\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s14_refere_clients_cabinet_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_refere_clients_cabinet_bool\" id=\"s14_refere_clients_cabinet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_refere_clients_cabinet_bool\" id=\"s14_refere_clients_cabinet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_refere_clients_cabinet_bool\" id=\"s14_refere_clients_cabinet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_refere_clients_cabinet_bool\" id=\"s14_refere_clients_cabinet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_refere_clients_cabinet_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_refere_clients_cabinet_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s14_refere_clients_cabinet_sioui_nb($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s14_refere_clients_cabinet_sioui_nb\" id=\"s14_refere_clients_cabinet_sioui_nb\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s14_refere_clients_cabinet_sioui_nb\" id=\"s14_refere_clients_cabinet_sioui_nb\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"text\" class=\"text_short\" name=\"" . $compare_sid . "_s14_refere_clients_cabinet_sioui_nb\" value=\"\" readonly=\"readonly\" />\n";
					}
				}
				break;
		}
		return $html;
	}

	function s14_refere_clients_cabinet_sioui_liste($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_refere_clients_cabinet_sioui_liste\" id=\"s14_refere_clients_cabinet_sioui_liste\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_refere_clients_cabinet_sioui_liste\" id=\"s14_refere_clients_cabinet_sioui_liste\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s14_refere_clients_cabinet_sioui_liste\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s14_refere_clients_cabinet_sioui_commissions($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text_short\" style=\"text-align: right;\" name=\"s14_refere_clients_cabinet_sioui_commissions\" id=\"s14_refere_clients_cabinet_sioui_commissions\" value=\"\" />&nbsp;$\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text_short\" style=\"text-align: right;\" name=\"s14_refere_clients_cabinet_sioui_commissions\" id=\"s14_refere_clients_cabinet_sioui_commissions\" value=\"\" readonly=\"readonly\" />&nbsp;$\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"text\" class=\"text_short\" style=\"text-align: right;\" name=\"" . $compare_sid . "_s14_refere_clients_cabinet_sioui_commissions\" value=\"\" readonly=\"readonly\" />&nbsp;$\n";
					}
				}
				break;
		}
		return $html;
	}

	function s14_affiche_cabinet_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_affiche_cabinet_bool\" id=\"s14_affiche_cabinet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_affiche_cabinet_bool\" id=\"s14_affiche_cabinet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_affiche_cabinet_bool\" id=\"s14_affiche_cabinet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_affiche_cabinet_bool\" id=\"s14_affiche_cabinet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_affiche_cabinet_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_affiche_cabinet_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s14_affiche_cabinet_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_affiche_cabinet_sinon\" id=\"s14_affiche_cabinet_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_affiche_cabinet_sinon\" id=\"s14_affiche_cabinet_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s14_affiche_cabinet_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s14_utilise_depliant_cabinet_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_utilise_depliant_cabinet_bool\" id=\"s14_utilise_depliant_cabinet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_utilise_depliant_cabinet_bool\" id=\"s14_utilise_depliant_cabinet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_utilise_depliant_cabinet_bool\" id=\"s14_utilise_depliant_cabinet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_utilise_depliant_cabinet_bool\" id=\"s14_utilise_depliant_cabinet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_utilise_depliant_cabinet_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_utilise_depliant_cabinet_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s14_utilise_depliant_cabinet_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_utilise_depliant_cabinet_sinon\" id=\"s14_utilise_depliant_cabinet_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_utilise_depliant_cabinet_sinon\" id=\"s14_utilise_depliant_cabinet_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s14_utilise_depliant_cabinet_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s14_notaires_satisfaits_cabinet_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_notaires_satisfaits_cabinet_bool\" id=\"s14_notaires_satisfaits_cabinet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_notaires_satisfaits_cabinet_bool\" id=\"s14_notaires_satisfaits_cabinet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_notaires_satisfaits_cabinet_bool\" id=\"s14_notaires_satisfaits_cabinet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_notaires_satisfaits_cabinet_bool\" id=\"s14_notaires_satisfaits_cabinet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_notaires_satisfaits_cabinet_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_notaires_satisfaits_cabinet_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s14_notaires_satisfaits_cabinet_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_notaires_satisfaits_cabinet_sinon\" id=\"s14_notaires_satisfaits_cabinet_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_notaires_satisfaits_cabinet_sinon\" id=\"s14_notaires_satisfaits_cabinet_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s14_notaires_satisfaits_cabinet_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s14_suggestions_cabinet_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_suggestions_cabinet_bool\" id=\"s14_suggestions_cabinet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_suggestions_cabinet_bool\" id=\"s14_suggestions_cabinet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s14_suggestions_cabinet_bool\" id=\"s14_suggestions_cabinet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s14_suggestions_cabinet_bool\" id=\"s14_suggestions_cabinet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_suggestions_cabinet_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s14_suggestions_cabinet_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s14_suggestions_cabinet_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_suggestions_cabinet_sioui\" id=\"s14_suggestions_cabinet_sioui\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s14_suggestions_cabinet_sioui\" id=\"s14_suggestions_cabinet_sioui\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s14_suggestions_cabinet_sioui\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

?>