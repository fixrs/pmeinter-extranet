<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "15").

			"	<div class=\"entry\">\n".
			"		<label for=\"s15_migration_paramaitre_bool\">Est-ce que vous pensez que l'&eacute;tude migrera vers Para-Ma&icirc;tre Web&nbsp;?</label>\n".
			"		" . s15_migration_paramaitre_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s15_migration_paramaitre_sioui\">Si <b>OUI</b>, quand&nbsp;:</label>\n".
			"		" . s15_migration_paramaitre_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s15_migration_paramaitre_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s15_migration_paramaitre_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s15_suggestions_generales_bool\">Est-ce que les notaires de l'&eacute;tude auraient des am&eacute;liorations &agrave; sugg&eacute;rer au r&eacute;seau concernant la formation, les rencontres, les directions de travail, la permanence, le CA ou autre&nbsp;?</label>\n".
			"		" . s15_suggestions_generales_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s15_suggestions_generales\">Si <b>OUI</b>, veuillez les &eacute;num&eacute;rer&nbsp;:</label>\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s15_suggestions_generales_formation\">Au niveau de la formation</b>&nbsp;:</label>\n".
			"		" . s15_suggestions_generales_formation($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s15_suggestions_generales_rencontres\">Au niveau des rencontres&nbsp;:</label>\n".
			"		" . s15_suggestions_generales_rencontres($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s15_suggestions_generales_permanence\">Au niveau de la permanence&nbsp;:</label>\n".
			"		" . s15_suggestions_generales_permanence($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s15_suggestions_generales_conseil\">Au niveau du conseil d'administration&nbsp;:</label>\n".
			"		" . s15_suggestions_generales_conseil($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s15_suggestions_generales_directions\">Au niveau des directions de travail&nbsp;:</label>\n".
			"		" . s15_suggestions_generales_directions($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s15_suggestions_generales_autres\">Autres&nbsp;:</label>\n".
			"		" . s15_suggestions_generales_autres($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s15_prets_impliquer_reseau_bool\">Est-ce que les notaires de l'&eacute;tude seraient pr&ecirc;ts &agrave; s'impliquer aupr&egrave;s du r&eacute;seau&nbsp;?</label>\n".
			"		" . s15_prets_impliquer_reseau_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s15_prets_impliquer_reseau_sioui\">Si <b>OUI</b>, &agrave; quel niveau (CA., directions de travail, autres)&nbsp;:</label>\n".
			"		" . s15_prets_impliquer_reseau_sioui($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}

	function s15_migration_paramaitre_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s15_migration_paramaitre_bool\" id=\"s15_migration_paramaitre_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s15_migration_paramaitre_bool\" id=\"s15_migration_paramaitre_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s15_migration_paramaitre_bool\" id=\"s15_migration_paramaitre_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s15_migration_paramaitre_bool\" id=\"s15_migration_paramaitre_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s15_migration_paramaitre_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s15_migration_paramaitre_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s15_migration_paramaitre_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s15_migration_paramaitre_sioui\" id=\"s14_refere_clients_cabinet_sioui_nb\" value=\"\" /><span class=\"note\">Format&nbsp;: AAAA-MM-JJ</span>\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s15_migration_paramaitre_sioui\" id=\"s14_refere_clients_cabinet_sioui_nb\" value=\"\" readonly=\"readonly\" /><span class=\"note\">format&nbsp;: AAAA-MM-JJ</span>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"text\" class=\"text_short\" name=\"" . $compare_sid . "_s15_migration_paramaitre_sioui\" value=\"\" readonly=\"readonly\" /><span class=\"note\">format&nbsp;: AAAA-MM-JJ</span>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s15_migration_paramaitre_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_migration_paramaitre_sinon\" id=\"s15_migration_paramaitre_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_migration_paramaitre_sinon\" id=\"s15_migration_paramaitre_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s15_migration_paramaitre_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s15_suggestions_generales_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s15_suggestions_generales_bool\" id=\"s15_suggestions_generales_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s15_suggestions_generales_bool\" id=\"s15_suggestions_generales_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s15_suggestions_generales_bool\" id=\"s15_suggestions_generales_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s15_suggestions_generales_bool\" id=\"s15_suggestions_generales_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s15_suggestions_generales_bool\" id=\"s15_suggestions_generales_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s15_suggestions_generales_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s15_suggestions_generales_formation($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_formation\" id=\"s15_suggestions_generales_formation\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_formation\" id=\"s15_suggestions_generales_formation\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s15_suggestions_generales_formation\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s15_suggestions_generales_rencontres($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_rencontres\" id=\"s15_suggestions_generales_rencontres\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_rencontres\" id=\"s15_suggestions_generales_rencontres\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s15_suggestions_generales_rencontres\" id=\"s15_suggestions_generales_rencontres\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s15_suggestions_generales_permanence($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_permanence\" id=\"s15_suggestions_generales_permanence\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_permanence\" id=\"s15_suggestions_generales_permanence\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s15_suggestions_generales_permanence\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s15_suggestions_generales_conseil($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_conseil\" id=\"s15_suggestions_generales_conseil\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_conseil\" id=\"s15_suggestions_generales_conseil\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s15_suggestions_generales_conseil\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s15_suggestions_generales_directions($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_directions\" id=\"s15_suggestions_generales_directions\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_directions\" id=\"s15_suggestions_generales_directions\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s15_suggestions_generales_directions\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s15_suggestions_generales_autres($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_autres\" id=\"s15_suggestions_generales_autres\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s15_suggestions_generales_autres\" id=\"s15_suggestions_generales_autres\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s15_suggestions_generales_autres\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s15_prets_impliquer_reseau_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s15_prets_impliquer_reseau_bool\" id=\"s15_prets_impliquer_reseau_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s15_prets_impliquer_reseau_bool\" id=\"s15_prets_impliquer_reseau_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s15_prets_impliquer_reseau_bool\" id=\"s15_prets_impliquer_reseau_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s15_prets_impliquer_reseau_bool\" id=\"s15_prets_impliquer_reseau_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s15_prets_impliquer_reseau_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s15_prets_impliquer_reseau_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s15_prets_impliquer_reseau_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text\" name=\"s15_prets_impliquer_reseau_sioui\" id=\"s15_prets_impliquer_reseau_sioui\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text\" name=\"s15_prets_impliquer_reseau_sioui\" id=\"s15_prets_impliquer_reseau_sioui\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"text\" class=\"text\" name=\"" . $compare_sid . "_s15_prets_impliquer_reseau_sioui\" value=\"\" readonly=\"readonly\" />\n";
					}
				}
				break;
		}
		return $html;
	}

?>