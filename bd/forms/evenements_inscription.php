<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL;
		
		$_FORM =
			
			form_header($action).
			
			"		<div class=\"entry\">\n".
			"			" . evenements_key($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry\">\n".
			"			" . etudes_key($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry\">\n".
			"			" . prenom($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry\">\n".
			"			" . nom($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry\">\n".
			"			" . telephone($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry\">\n".
			"			" . courriel($action) . "\n".
			"		</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html = "";
		if ($action=="read"){
				$html =
					"	<div class=\"readform\">\n";
		}
		else {
				$html =	
					"<form name=\"inscriptions\" action=\"inscriptions.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"".$action."\" />\n".
					"	<input type=\"hidden\" name=\"ins_key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
			
		}	
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "modify":
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "read":
				$html =
					"	</div>\n";
				break;
		}
		return $html;
	}


	function evenements_key($action){
		$html = "";
		switch ($action) {	
			case "add":
				$event_key = getorpost('evenements_key');
				$html =
					"<label for=\"evenements_key\">&Eacute;v&eacute;nement&nbsp;: </label>\n" .
					"<span class=\"answer\">".getEvenementsName($action, $event_key)."</span>\n".
					"<input type=\"hidden\" name=\"evenements_key\" value=\"".$event_key."\" />\n";
				break;
			case "modify":
				$evenements_key = getEvenementsKey(getorpost('ins_key'));
				$html =
					"<label for=\"evenements_key\">&Eacute;v&eacute;nement&nbsp;: </label>\n" .
					"<span class=\"answer\">".getEvenementsName($action, $evenements_key)."</span>\n".
					"<input type=\"hidden\" name=\"evenements_key\" value=\"".$evenements_key."\" />\n";
				break;
			case "read":
				$html = 
					"<label for=\"formations_key\">&Eacute;v&eacute;nement&nbsp;: </label>\n" .
					"<span class=\"answer\"><!---HIDDEN_evenements_key /HIDDEN_evenements_key---></span><br/><br/>\n";
				break;
		}
		return $html;
	}


	function etudes_key($action){
		$html = "";
		switch ($action) {	
			case "add":
				$html =
					"<a name=\"etudes_key_v\"></a><label for=\"etudes_key\">&Eacute;tude&nbsp;: </label>\n" .
					getEtudesSelectString().
					"		\n";
				break;
			case "modify":
				$html =
					"<a name=\"etudes_key_v\"></a><label for=\"etudes_key\">&Eacute;tude&nbsp;: </label>\n" .
					getEtudesSelectString().
					"		\n";
				break;
			case "read":
				$html = 
					"<label for=\"etudes_key\">&Eacute;tude&nbsp;: </label>\n" .
					"<span class=\"answer\"><!---HIDDEN_etudes_key /HIDDEN_etudes_key---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function prenom($action) {
		$html="";
		switch ($action) {	
			case "add":
				$html = "<a name=\"prenom_v\"></a><label for=\"prenom\">Pr&eacute;nom du participant&nbsp;: </label>\n" .
						"<input type=\"text\" name=\"prenom\" id=\"prenom\" value=\"\" tabindex=\"19\" />\n".
						"";
				break;
			case "modify":
				$html = "<a name=\"prenom_v\"></a><label for=\"prenom\">Pr&eacute;nom du participant&nbsp;: </label>\n" .
						"<input type=\"text\" name=\"prenom\" id=\"prenom\" value=\"\" tabindex=\"19\" />\n".
						"";
				break;
			case "read":
				$html = 
					"<label for=\"prenom\">Pr&eacute;nom du participant&nbsp;: </label>\n" .
					"<span class=\"answer\"><!---HIDDEN_prenom /HIDDEN_prenom---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function nom($action) {
		$html="";
		switch ($action) {	
			case "add":
				$html = "<a name=\"nom_v\"></a><label for=\"nom\">Nom du participant&nbsp;: </label>\n" .
						"<input type=\"text\" name=\"nom\" id=\"nom\" value=\"\" tabindex=\"19\" />\n".
						"";
				break;
			case "modify":
				$html = "<a name=\"nom_v\"></a><label for=\"nom\">Nom du participant&nbsp;: </label>\n" .
						"<input type=\"text\" name=\"nom\" id=\"nom\" value=\"\" tabindex=\"19\" />\n".
						"";
				break;
			case "read":
				$html = 
					"<label for=\"nom\">Nom du participant&nbsp;: </label>\n" .
					"<span class=\"answer\"><!---HIDDEN_nom /HIDDEN_nom---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function telephone($action){
		$html="";
		switch ($action) {	
			case "add":
				$html = "<label for=\"telephone\">Num&eacute;ro de t&eacute;l&eacute;phone du participant&nbsp;: </label>\n" .
						"<input type=\"text\" name=\"telephone\" value=\"\" tabindex=\"22\" />\n".
						"";
				break;
			case "modify":
				$html = "<label for=\"telephone\">Num&eacute;ro de t&eacute;l&eacute;phone du participant&nbsp;: </label>\n" .
						"<input type=\"text\" name=\"telephone\" value=\"\" tabindex=\"22\" />\n".
						"";
				break;
			case "read":
				$html = 
					"<label for=\"telephone\">Num&eacute;ro de t&eacute;l&eacute;phone du participant&nbsp;: </label>\n" .
					"<span class=\"answer\"><!---HIDDEN_telephone /HIDDEN_telephone---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function courriel($action) {
		$html="";
		switch ($action) {	
			case "add":
				$html = "<a name=\"courriel_v\"></a><label for=\"courriel\">Courriel du participant&nbsp;: </label>\n" .
						"<input type=\"text\" name=\"courriel\" id=\"courriel\" value=\"\" tabindex=\"23\" /> \n".
						"";
				break;
			case "modify":
				$html = "<a name=\"courriel_v\"></a><label for=\"courriel\">Courriel du participant&nbsp;: </label>\n" .
						"<input type=\"text\" name=\"courriel\" id=\"courriel\" value=\"\" tabindex=\"23\" />\n".
						"";
				break;
			case "read":
				$html = 
					"<label for=\"courriel\">Courriel du participant&nbsp;: </label>\n" .
					"<span class=\"answer\"><!---HIDDEN_courriel /HIDDEN_courriel---></span>\n";
				break;
		}
		return $html;
	}

	function getEtudesSelectString($key=""){
		global $DB;
		$html = 
			"		<select name=\"etudes_key\" id=\"etudes_key\">\n".
			"			<option value=\"0\">- Choisir une &eacute;tude -</option>\n";

		$DB->query(
			"SELECT `key`, `nom` ".
			"FROM `etudes` ".
			"WHERE `actif`='1' ".
			"ORDER BY `nom` ASC"
		);
		while($DB->next_record()) {
			$html .= "			<option id=\"etudes_key\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . "</option>\n";
		}

		$html .= 
			"		</select>\n";

		return $html;
	}
?>