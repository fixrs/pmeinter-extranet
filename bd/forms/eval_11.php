<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "11").

			"	<h2>Campagne de publicit&eacute;</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s11_debourser_additionnels_pub_bool\">Est-ce que l'&eacute;tude serait pr&ecirc;te &agrave; d&eacute;bourser des co&ucirc;ts additionnels pour supporter une campagne de publicit&eacute; nationale r&eacute;alis&eacute;e par PME INTER Notaires&nbsp;?</label>\n".
			"		" . s11_debourser_additionnels_pub_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s11_debourser_additionnels_pub_detail\">Expliquer la r&eacute;ponse&nbsp;:</label>\n".
			"		" . s11_debourser_additionnels_pub_detail($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s11_campagne_pub_locale_bool\">Est-ce que l'&eacute;tude a effectu&eacute; une campagne de publicit&eacute; locale au cours de la derni&egrave;re ann&eacute;e&nbsp;?</label>\n".
			"		" . s11_campagne_pub_locale_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s11_campagne_pub_locale_sioui\">Si <b>OUI</b>, sp&eacute;cifier&nbsp;:</label>\n".
			"		" . s11_campagne_pub_locale_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s11_affiche_pmeinter_pub_locale_bool\">Est-ce que l'&eacute;tude affiche le nom de PME INTER Notaires lors de sa campagne de publicit&eacute; locale&nbsp;?</label>\n".
			"		" . s11_affiche_pmeinter_pub_locale_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s11_affiche_pmeinter_pub_locale_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s11_affiche_pmeinter_pub_locale_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Articles promotionnels</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s11_sait_acceder_promo_extranet_bool\">Est-ce que l'&eacute;tude sait comment acc&eacute;der &agrave; la section des articles promotionnels sur l'extranet du r&eacute;seau&nbsp;?</label>\n".
			"		" . s11_sait_acceder_promo_extranet_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s11_validation_extranet\">Valider les mots de passe utilis&eacute;s avec l'&eacute;tude&nbsp;:</label>\n".
			"		" . s11_validation_extranet($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"produits_articles_promotionnels\">&Eacute;num&eacute;rer les articles promotionnels que l'&eacute;tude a command&eacute;s au cours de la derni&egrave;re ann&eacute;e&nbsp;:</label>\n".
			"		" . produits_articles_promotionnels($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s11_satisfait_produits_promo_bool\">Est-ce que l'&eacute;tude est satisfaite du rapport qualit&eacute;/prix et de la diversit&eacute; des produits promotionnels offerts par le r&eacute;seau&nbsp;?</label>\n".
			"		" . s11_satisfait_produits_promo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s11_satisfait_produits_promo_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s11_satisfait_produits_promo_sinon($action, $parameters) . "\n".
			"	</div>\n".
/*
			"	<div class=\"suggestion\">\n".
			"		Suggestions &agrave; faire &agrave; l'&eacute;tude&nbsp;:<br />\n".
			"		<br />\n".
			"		Avoir toujours en inventaire certains des produits promotionnels du r&eacute;seau en r&eacute;serve afin d'&ecirc;tre en mesure de r&eacute;pondre &agrave; tous les besoins de commandites &agrave; venir.\n".
			"	</div>\n".
*/
			"	<div class=\"entry\">\n".
			"		<label for=\"s11_utilise_fournisseur_promo_bool\">Est-ce que l'&eacute;tude utilise un autre fournisseur pour l'achat d'articles promotionnels&nbsp;?</label>\n".
			"		" . s11_utilise_fournisseur_promo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s11_utilise_fournisseur_promo_sioui\">Si <b>OUI</b>, pourquoi&nbsp;:</label>\n".
			"		" . s11_utilise_fournisseur_promo_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s11_suggestions_ajout_promo_bool\">Est-ce que l'&eacute;tude a des suggestions pour ajouter de nouveaux articles promotionnels&nbsp;?</label>\n".
			"		" . s11_suggestions_ajout_promo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s11_suggestions_ajout_promo_sioui\">Si <b>OUI</b>, &eacute;num&eacute;rer ses suggestions&nbsp;:</label>\n".
			"		" . s11_suggestions_ajout_promo_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s11_notes_marketing\">Notes suppl&eacute;mentaires&nbsp;:</label>\n".
			"		" . s11_notes_marketing($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}	

	function s11_debourser_additionnels_pub_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_debourser_additionnels_pub_bool\" id=\"s11_debourser_additionnels_pub_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_debourser_additionnels_pub_bool\" id=\"s11_debourser_additionnels_pub_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_debourser_additionnels_pub_bool\" id=\"s11_debourser_additionnels_pub_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_debourser_additionnels_pub_bool\" id=\"s11_debourser_additionnels_pub_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_debourser_additionnels_pub_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_debourser_additionnels_pub_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s11_debourser_additionnels_pub_detail($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_debourser_additionnels_pub_detail\" id=\"s11_debourser_additionnels_pub_detail\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_debourser_additionnels_pub_detail\" id=\"s11_debourser_additionnels_pub_detail\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s11_debourser_additionnels_pub_detail\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s11_campagne_pub_locale_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_campagne_pub_locale_bool\" id=\"s11_campagne_pub_locale_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_campagne_pub_locale_bool\" id=\"s11_campagne_pub_locale_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .= 
					"<input type=\"radio\" class=\"radio\" name=\"s11_campagne_pub_locale_bool\" id=\"s11_campagne_pub_locale_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_campagne_pub_locale_bool\" id=\"s11_campagne_pub_locale_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_campagne_pub_locale_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_campagne_pub_locale_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s11_campagne_pub_locale_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_campagne_pub_locale_sioui\" id=\"s11_campagne_pub_locale_sioui\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_campagne_pub_locale_sioui\" id=\"s11_campagne_pub_locale_sioui\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s11_campagne_pub_locale_sioui\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s11_affiche_pmeinter_pub_locale_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_affiche_pmeinter_pub_locale_bool\" id=\"s11_affiche_pmeinter_pub_locale_bool_1\" value=\"1\"/><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_affiche_pmeinter_pub_locale_bool\" id=\"s11_affiche_pmeinter_pub_locale_bool_0\" value=\"0\"/><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_affiche_pmeinter_pub_locale_bool\" id=\"s11_affiche_pmeinter_pub_locale_bool_1\" value=\"1\"/><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_affiche_pmeinter_pub_locale_bool\" id=\"s11_affiche_pmeinter_pub_locale_bool_0\" value=\"0\"/><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_affiche_pmeinter_pub_locale_bool\" value=\"1\"/><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_affiche_pmeinter_pub_locale_bool\" value=\"0\"/><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s11_affiche_pmeinter_pub_locale_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_affiche_pmeinter_pub_locale_sinon\" id=\"s11_affiche_pmeinter_pub_locale_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_affiche_pmeinter_pub_locale_sinon\" id=\"s11_affiche_pmeinter_pub_locale_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s11_affiche_pmeinter_pub_locale_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s11_sait_acceder_promo_extranet_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_sait_acceder_promo_extranet_bool\" id=\"s11_sait_acceder_promo_extranet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_sait_acceder_promo_extranet_bool\" id=\"s11_sait_acceder_promo_extranet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_sait_acceder_promo_extranet_bool\" id=\"s11_sait_acceder_promo_extranet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_sait_acceder_promo_extranet_bool\" id=\"s11_sait_acceder_promo_extranet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_sait_acceder_promo_extranet_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_sait_acceder_promo_extranet_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s11_validation_extranet($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s11_validation_extranet_username\" id=\"s11_validation_extranet_username\" value=\"\" />&nbsp; Nom d'usager<br />\n".
					"<br />\n".
					"<input type=\"text\" class=\"text_short\" name=\"s11_validation_extranet_password\" id=\"s11_validation_extranet_password\" value=\"\" />&nbsp; Mot de passe\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s11_validation_extranet_username\" id=\"s11_validation_extranet_username\" value=\"\" readonly=\"readonly\" />&nbsp;&nbsp; <i>Username</i><br />\n".
					"<br />\n".
					"<input type=\"text\" class=\"text_short\" name=\"s11_validation_extranet_password\" id=\"s11_validation_extranet_password\" value=\"\" readonly=\"readonly\" />&nbsp;&nbsp; <i>Password</i>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"text\" class=\"text_short\" name=\"" . $compare_sid . "_s11_validation_extranet_username\" value=\"\" readonly=\"readonly\" />&nbsp;&nbsp; <i>Username</i><br />\n".
							"<br />\n".
							"<input type=\"text\" class=\"text_short\" name=\"" . $compare_sid . "_s11_validation_extranet_password\" value=\"\" readonly=\"readonly\" />&nbsp;&nbsp; <i>Password</i>\n";
					}
				}
				break;
		}
		return $html;
	}

	function produits_articles_promotionnels($action, $parameters) {
		global $DB;
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"produits_articles_promotionnels_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Produits</th>\n".
					"				<th width=\"60%\">Quantit&eacute;</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";

				$DB->query(
					"SELECT `key`, `nom` ".
					"FROM `produits` ".
					"WHERE produits_categories_key = '4';"
				);
				while ($DB->next_record()) {
					$produits_key = $DB->getField("key");
					$produits_nom = $DB->getField("nom");
					$html .=
						"<tr>\n".
						"	<td>" . $produits_nom . "</td>\n".
						"	<td>\n".
						"		<input type=\"hidden\" name=\"produits_articles_promotionnels_produits_key[" . $produits_key . "]\" id=\"produits_articles_promotionnels_produits_key[" . $produits_key . "]\" value=\"" . $produits_key . "\" />\n".
						"		<input type=\"hidden\" name=\"produits_articles_promotionnels_nom[" . $produits_key . "]\" id=\"produits_articles_promotionnels_nom[" . $produits_key . "]\" value=\"" . $produits_nom . "\" />\n".
						"		<input type=\"text\" class=\"text\" name=\"produits_articles_promotionnels_qte[" . $produits_key . "]\" id=\"produits_articles_promotionnels_qte[" . $produits_key . "]\" style=\"width: 40px; text-align: right;\" value=\"\" />\n".
						"	</td>\n".
						"</tr>\n";
				}

				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"produits_articles_promotionnels_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Produits</th>\n".
					"				<th width=\"60%\">Quantit&eacute;</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";

				$DB->query(
					"SELECT `key`, `nom` ".
					"FROM `produits` ".
					"WHERE produits_categories_key = '4';"
				);
				while ($DB->next_record()) {
					$produits_key = $DB->getField("key");
					$produits_nom = $DB->getField("nom");
					$html .=
						"<tr>\n".
						"	<td>" . $produits_nom . "</td>\n".
						"	<td>\n".
						"		<input type=\"text\" class=\"text\" name=\"produits_articles_promotionnels_qte[" . $produits_key . "]\" id=\"produits_articles_promotionnels_qte[" . $produits_key . "]\" style=\"width: 40px; text-align: right;\" value=\"\" readonly=\"readonly\" />\n".
						"	</td>\n".
						"</tr>\n";
				}

				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<div class=\"produits_articles_promotionnels_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th width=\"40%\">Produits</th>\n".
							"				<th width=\"60%\">Quantit&eacute;</th>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n";
		
						$DB->query(
							"SELECT `key`, `nom` ".
							"FROM `produits` ".
							"WHERE produits_categories_key = '4';"
						);
						while ($DB->next_record()) {
							$produits_key = $DB->getField("key");
							$produits_nom = $DB->getField("nom");
							$html .=
								"<tr>\n".
								"	<td>" . $produits_nom . "</td>\n".
								"	<td>\n".
								"		<input type=\"text\" class=\"text\" name=\"" . $compare_sid . "_produits_articles_promotionnels_qte[" . $produits_key . "]\" style=\"width: 40px; text-align: right;\" value=\"\" readonly=\"readonly\" />\n".
								"	</td>\n".
								"</tr>\n";
						}
		
						$html .=
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s11_satisfait_produits_promo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_satisfait_produits_promo_bool\" id=\"s11_satisfait_produits_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_satisfait_produits_promo_bool\" id=\"s11_satisfait_produits_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_satisfait_produits_promo_bool\" id=\"s11_satisfait_produits_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_satisfait_produits_promo_bool\" id=\"s11_satisfait_produits_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_satisfait_produits_promo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_satisfait_produits_promo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s11_satisfait_produits_promo_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_satisfait_produits_promo_sinon\" id=\"s11_satisfait_produits_promo_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_satisfait_produits_promo_sinon\" id=\"s11_satisfait_produits_promo_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s11_satisfait_produits_promo_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s11_utilise_fournisseur_promo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_utilise_fournisseur_promo_bool\" id=\"s11_utilise_fournisseur_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_utilise_fournisseur_promo_bool\" id=\"s11_utilise_fournisseur_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_utilise_fournisseur_promo_bool\" id=\"s11_utilise_fournisseur_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_utilise_fournisseur_promo_bool\" id=\"s11_utilise_fournisseur_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_utilise_fournisseur_promo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_utilise_fournisseur_promo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s11_utilise_fournisseur_promo_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_utilise_fournisseur_promo_sioui\" id=\"s11_utilise_fournisseur_promo_sioui\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_utilise_fournisseur_promo_sioui\" id=\"s11_utilise_fournisseur_promo_sioui\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s11_utilise_fournisseur_promo_sioui\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s11_suggestions_ajout_promo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_suggestions_ajout_promo_bool\" id=\"s11_suggestions_ajout_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_suggestions_ajout_promo_bool\" id=\"s11_suggestions_ajout_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s11_suggestions_ajout_promo_bool\" id=\"s11_suggestions_ajout_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s11_suggestions_ajout_promo_bool\" id=\"s11_suggestions_ajout_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_suggestions_ajout_promo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s11_suggestions_ajout_promo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s11_suggestions_ajout_promo_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_suggestions_ajout_promo_sioui\" id=\"s11_suggestions_ajout_promo_sioui\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_suggestions_ajout_promo_sioui\" id=\"s11_suggestions_ajout_promo_sioui\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s11_suggestions_ajout_promo_sioui\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s11_notes_marketing($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_notes_marketing\" id=\"s11_notes_marketing\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s11_notes_marketing\" id=\"s11_notes_marketing\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s11_notes_marketing\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

?>