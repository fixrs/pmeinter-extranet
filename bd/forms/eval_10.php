<?PHP

	// LE FORMULAIRE //
	function getForm($action, $parameters) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "10").

			"	<h2>Responsables</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"domaines_daffaires_notaires_droit_immo\">S&eacute;lectionner les notaires qui se sp&eacute;cialisent en droit immobilier et d&eacute;finir le temps consacr&eacute; &agrave; cette activit&eacute; en %&nbsp;:</label>\n".
			"		" . domaines_daffaires_notaires_droit_immo($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"domaines_daffaires_collaboratrices_droit_immo\">S&eacute;lectionner les collaboratrices ou les techniciennes qui oeuvrent au niveau du droit immobilier et le temps consacr&eacute; &agrave; cette activit&eacute; en %&nbsp;:</label>\n".
			"		" . domaines_daffaires_collaboratrices_droit_immo($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_publie_enligne_bool\">Est-ce que l'&eacute;tude publie ses documents immobiliers en lignes&nbsp;?</label>\n".
			"		" . s10_publie_enligne_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Formation</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s10_notaires_formation_assistant_immo_bool\">Est-ce que des notaires ont suivi la formation sur l'<b>Assistant Immobilier</b>&nbsp;?</label>\n".
			"		" . s10_notaires_formation_assistant_immo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"formations_notaires_domaines_daffaires_formation_assistant_immo\">Si <b>OUI</b>, s&eacute;lectionner le nom des notaires&nbsp;:</label>\n".
			"		" . formations_notaires_domaines_daffaires_formation_assistant_immo($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s10_collaboratrices_formation_assistant_immo_bool\">Est-ce que les collaboratrices et les techniciennes de l'&eacute;tude ont suivi la formation sur l'<b>Assistant Immobilier</b>&nbsp;?</label>\n".
			"		" . s10_collaboratrices_formation_assistant_immo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"formations_collaboratrices_domaines_daffaires_formation_assistant_immo\">Si <b>OUI</b>, s&eacute;lectionner le nom des participants&nbsp;:</label>\n".
			"		" . formations_collaboratrices_domaines_daffaires_formation_assistant_immo($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_formations_assistant_immo_encore_bool\">Est-ce que l'&eacute;tude aurait besoin que cette formation soit donn&eacute;e &agrave; nouveau&nbsp;?</label>\n".
			"		" . s10_formation_assistant_immo_encore_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_notes_formation_droit_affaires\">Notes suppl&eacute;mentaires&nbsp;:</label>\n".
			"		" . s10_notes_formation_droit_affaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Produit informatis&eacute; du r&eacute;seau</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s10_utilise_assistant_immo_bool\">Est-ce que l'&eacute;tude utilise l'Assistant Immobilier&nbsp;?</label>\n".
			"		" . s10_utilise_assistant_immo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_utilise_assistant_immo_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s10_utilise_assistant_immo_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Produit du r&eacute;seau</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s10_utilise_enface_bool\">Est-ce que l'&eacute;tude utilise syst&eacute;matiquement les en-faces (endos) du r&eacute;seau&nbsp;?</label>\n".
			"		" . s10_utilise_enface_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_utilise_enface_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s10_utilise_enface_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s10_utilise_fournisseur_enface_bool\">Est-ce que l'&eacute;tude utilise un autre fournisseur pour l'achat de ses en-faces (endos) personnalis&eacute;s&nbsp;?</label>\n".
			"		" . s10_utilise_fournisseur_enface_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_utilise_fournisseur_enface_sioui\">Si <b>OUI</b>, pourquoi&nbsp;:</label>\n".
			"		" . s10_utilise_fournisseur_enface_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Publicit&eacute;</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s10_utilise_docs_promo_immo_bool\">Est-ce que le d&eacute;partement du droit immobilier utilise des documents promotionnels pour promouvoir ses services&nbsp;?</label>\n".
			"		" . s10_utilise_docs_promo_immo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_utilise_docs_promo_immo_lesquels\">Si <b>OUI</b>, lesquels&nbsp;:</label>\n".
			"		" . s10_utilise_docs_promo_immo_lesquels($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_utilise_docs_promo_immo_doc\">Annexer une copie de cette publicit&eacute;&nbsp;:</label>\n".
			"		" . s10_utilise_docs_promo_immo_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s10_affiche_docs_promo_immo_bool\">Est-ce que l'&eacute;tude affiche le nom de PME INTER Notaires dans ses documents promotionnels&nbsp;?</label>\n".
			"		" . s10_affiche_docs_promo_immo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s10_affiche_docs_promo_immo_doc\">Si <b>OUI</b>, joindre un exemplaire en annexe&nbsp;:</label>\n".
			"		" . s10_affiche_docs_promo_immo_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_affiche_docs_promo_immo_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s10_affiche_docs_promo_immo_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_logo_docs_immobiliers_bool\">Est-ce que l'&eacute;tude affiche le logo de PME INTER Notaires (en bas &agrave; droite) sur chaque page de ses documents immobiliers&nbsp;?</label>\n".
			"		" . s10_logo_docs_immobiliers_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_logo_docs_immobiliers_doc\">Si <b>OUI</b>, joindre un exemplaire en annexe&nbsp;:</label>\n".
			"		" . s10_logo_docs_immobiliers_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s10_logo_docs_immobiliers_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s10_logo_docs_immobiliers_sinon($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action, $parameters);

		return $_FORM;
	}

	function domaines_daffaires_notaires_droit_immo($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Notaire</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_notaires_droit_immo_employes_key[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_immo_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_notaires_droit_immo_nom[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_immo_nom[" . $employes_key . "]\" value=\"" . $DB->getField("prenom") . " " . $DB->getField("nom") . "\" />\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_notaires_droit_immo_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_immo_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_notaires_droit_immo_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_immo_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" />&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Notaire</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_notaires_droit_immo_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_immo_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_notaires_droit_immo_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_immo_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" readonly=\"readonly\" />&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<div class=\"domaines_daffaires_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th width=\"40%\">Notaire</th>\n".
							"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						while ($DB->next_record()) {
							$nb++;
							$employes_key = $DB->getField("employes_key");
							$html .=
								"	<tr>\n".
								"		<td>\n".
								"			<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_domaines_daffaires_notaires_droit_immo_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
								"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
								"		</td>\n".
								"		<td>\n".
								"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_domaines_daffaires_notaires_droit_immo_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" readonly=\"readonly\" />&nbsp;%\n".
								"		</td>\n".
								"	</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function domaines_daffaires_collaboratrices_droit_immo($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Employ&eacute;</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_collaboratrices_droit_immo_employes_key[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_immo_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_collaboratrices_droit_immo_nom[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_immo_nom[" . $employes_key . "]\" value=\"" . $DB->getField("prenom") . " " . $DB->getField("nom") . "\" />\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_collaboratrices_droit_immo_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_immo_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_collaboratrices_droit_immo_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_immo_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" />&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Employ&eacute;</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_collaboratrices_droit_immo_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_immo_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_collaboratrices_droit_immo_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_immo_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" readonly=\"readonly\" />&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<div class=\"domaines_daffaires_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th width=\"40%\">Employ&eacute;</th>\n".
							"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						while ($DB->next_record()) {
							$nb++;
							$employes_key = $DB->getField("employes_key");
							$html .=
								"	<tr>\n".
								"		<td>\n".
								"			<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_domaines_daffaires_collaboratrices_droit_immo_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
								"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
								"		</td>\n".
								"		<td>\n".
								"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_domaines_daffaires_collaboratrices_droit_immo_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" readonly=\"readonly\" />&nbsp;%\n".
								"		</td>\n".
								"	</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_publie_enligne_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_publie_enligne_bool\" id=\"s10_publie_enligne_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_publie_enligne_bool\" id=\"s10_publie_enligne_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_publie_enligne_bool\" id=\"s10_publie_enligne_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_publie_enligne_bool\" id=\"s10_publie_enligne_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_publie_enligne_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_publie_enligne_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s10_notaires_formation_assistant_immo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_notaires_formation_assistant_immo_bool\" id=\"s10_notaires_formation_assistant_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_notaires_formation_assistant_immo_bool\" id=\"s10_notaires_formation_assistant_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_notaires_formation_assistant_immo_bool\" id=\"s10_notaires_formation_assistant_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_notaires_formation_assistant_immo_bool\" id=\"s10_notaires_formation_assistant_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_notaires_formation_assistant_immo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_notaires_formation_assistant_immo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function formations_notaires_domaines_daffaires_formation_assistant_immo($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"hidden\" name=\"formations_notaires_domaines_daffaires_formation_assistant_immo_employes_key[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_assistant_immo_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"	<input type=\"hidden\" name=\"formations_notaires_domaines_daffaires_formation_assistant_immo_nom[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_assistant_immo_nom[" . $employes_key . "]\" value=\"" . $fullname . "\" />\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_notaires_domaines_daffaires_formation_assistant_immo_selectionne[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_assistant_immo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_notaires_domaines_daffaires_formation_assistant_immo_selectionne[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_assistant_immo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<div class=\"formations_domaines_daffaires_entry\">\n".
							"	<table>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` AS e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						$nb_cols = 0;
						while ($DB->next_record()){
							$nb++;
							$nb_cols++;
							$employes_key = $DB->getField("employes_key");
							$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
							if ($nb_cols == 1) {
								$nb_lastcol = 0;
								$html .=
									"<tr>\n";
							}
							$nb_lastcol++;
							$html .=
								"<td>\n".
								"	<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_formations_notaires_domaines_daffaires_formation_assistant_immo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
								"</td>\n";
							if ($nb_cols == 4) {
								$nb_cols = 0;
								$html .=
									"</tr>\n";
							}
						}
						if ($nb_lastcol < 4 && $nb > 0) {
							$html .=
								str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
								"</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"	</table>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_collaboratrices_formation_assistant_immo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_collaboratrices_formation_assistant_immo_bool\" id=\"s10_collaboratrices_formation_assistant_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_collaboratrices_formation_assistant_immo_bool\" id=\"s10_collaboratrices_formation_assistant_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_collaboratrices_formation_assistant_immo_bool\" id=\"s10_collaboratrices_formation_assistant_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_collaboratrices_formation_assistant_immo_bool\" id=\"s10_collaboratrices_formation_assistant_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_collaboratrices_formation_assistant_immo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_collaboratrices_formation_assistant_immo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function formations_collaboratrices_domaines_daffaires_formation_assistant_immo($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"hidden\" name=\"formations_collaboratrices_domaines_daffaires_formation_assistant_immo_employes_key[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_assistant_immo_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"	<input type=\"hidden\" name=\"formations_collaboratrices_domaines_daffaires_formation_assistant_immo_nom[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_assistant_immo_nom[" . $employes_key . "]\" value=\"" . $fullname . "\" />\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_collaboratrices_domaines_daffaires_formation_assistant_immo_selectionne[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_assistant_immo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_collaboratrices_domaines_daffaires_formation_assistant_immo_selectionne[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_assistant_immo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<div class=\"formations_domaines_daffaires_entry\">\n".
							"	<table>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` AS e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						$nb_cols = 0;
						while ($DB->next_record()){
							$nb++;
							$nb_cols++;
							$employes_key = $DB->getField("employes_key");
							$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
							if ($nb_cols == 1) {
								$nb_lastcol = 0;
								$html .=
									"<tr>\n";
							}
							$nb_lastcol++;
							$html .=
								"<td>\n".
								"	<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_formations_collaboratrices_domaines_daffaires_formation_assistant_immo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
								"</td>\n";
							if ($nb_cols == 4) {
								$nb_cols = 0;
								$html .=
									"</tr>\n";
							}
						}
						if ($nb_lastcol < 4 && $nb > 0) {
							$html .=
								str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
								"</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"	</table>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_formation_assistant_immo_encore_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_formation_assistant_immo_encore_bool\" id=\"s10_formation_assistant_immo_encore_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_formation_assistant_immo_encore_bool\" id=\"s10_formation_assistant_immo_encore_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_formation_assistant_immo_encore_bool\" id=\"s10_formation_assistant_immo_encore_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_formation_assistant_immo_encore_bool\" id=\"s10_formation_assistant_immo_encore_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_formation_assistant_immo_encore_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_formation_assistant_immo_encore_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s10_notes_formation_droit_affaires($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"8\" name=\"s10_notes_formation_droit_affaires\" id=\"s10_notes_formation_droit_affaires\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"8\" name=\"s10_notes_formation_droit_affaires\" id=\"s10_notes_formation_droit_affaires\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"8\" name=\"" . $compare_sid . "_s10_notes_formation_droit_affaires\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_utilise_assistant_immo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_assistant_immo_bool\" id=\"s10_utilise_assistant_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_assistant_immo_bool\" id=\"s10_utilise_assistant_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_assistant_immo_bool\" id=\"s10_utilise_assistant_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_assistant_immo_bool\" id=\"s10_utilise_assistant_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_utilise_assistant_immo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_utilise_assistant_immo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s10_utilise_assistant_immo_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_utilise_assistant_immo_sinon\" id=\"s10_utilise_assistant_immo_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_utilise_assistant_immo_sinon\" id=\"s10_utilise_assistant_immo_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s10_utilise_assistant_immo_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_utilise_enface_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_enface_bool\" id=\"s10_utilise_enface_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_enface_bool\" id=\"s10_utilise_enface_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_enface_bool\" id=\"s10_utilise_enface_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_enface_bool\" id=\"s10_utilise_enface_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_utilise_enface_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_utilise_enface_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s10_utilise_enface_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_utilise_enface_sinon\" id=\"s10_utilise_enface_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_utilise_enface_sinon\" id=\"s10_utilise_enface_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s10_utilise_enface_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_utilise_fournisseur_enface_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_fournisseur_enface_bool\" id=\"s10_utilise_fournisseur_enface_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_fournisseur_enface_bool\" id=\"s10_utilise_fournisseur_enface_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_fournisseur_enface_bool\" id=\"s10_utilise_fournisseur_enface_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_fournisseur_enface_bool\" id=\"s10_utilise_fournisseur_enface_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_utilise_fournisseur_enface_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_utilise_fournisseur_enface_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s10_utilise_fournisseur_enface_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_utilise_fournisseur_enface_sioui\" id=\"s10_utilise_fournisseur_enface_sioui\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_utilise_fournisseur_enface_sioui\" id=\"s10_utilise_fournisseur_enface_sioui\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s10_utilise_fournisseur_enface_sioui\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_utilise_docs_promo_immo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_docs_promo_immo_bool\" id=\"s10_utilise_docs_promo_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_docs_promo_immo_bool\" id=\"s10_utilise_docs_promo_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_docs_promo_immo_bool\" id=\"s10_utilise_docs_promo_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_utilise_docs_promo_immo_bool\" id=\"s10_utilise_docs_promo_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_utilise_docs_promo_immo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_utilise_docs_promo_immo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s10_utilise_docs_promo_immo_lesquels($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_utilise_docs_promo_immo_lesquels\" id=\"s10_utilise_docs_promo_immo_lesquels\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_utilise_docs_promo_immo_lesquels\" id=\"s10_utilise_docs_promo_immo_lesquels\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s10_utilise_docs_promo_immo_lesquels\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_utilise_docs_promo_immo_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s10_utilise_docs_promo_immo_doc_num'] == "" && $parameters['s10_utilise_docs_promo_immo_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s10_utilise_docs_promo_immo_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s10_utilise_docs_promo_immo_doc_num\" id=\"s10_utilise_docs_promo_immo_doc_num\" value=\"" . $parameters['s10_utilise_docs_promo_immo_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s10_utilise_docs_promo_immo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s10_utilise_docs_promo_immo_doc_num_del\" id=\"s10_utilise_docs_promo_immo_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s10_utilise_docs_promo_immo_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s10_utilise_docs_promo_immo_doc_num_tmp\" id=\"s10_utilise_docs_promo_immo_doc_num_tmp\" value=\"" . $parameters['s10_utilise_docs_promo_immo_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s10_utilise_docs_promo_immo_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s10_utilise_docs_promo_immo_doc_num_tmp_del\" id=\"s10_utilise_docs_promo_immo_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s10_utilise_docs_promo_immo_doc_num_up\" id=\"s10_utilise_docs_promo_immo_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s10_utilise_docs_promo_immo_doc_phy\" id=\"s10_utilise_docs_promo_immo_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s10_utilise_docs_promo_immo_doc_num'] == "" && $parameters['s10_utilise_docs_promo_immo_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s10_utilise_docs_promo_immo_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s10_utilise_docs_promo_immo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s10_utilise_docs_promo_immo_doc_phy\" id=\"s10_utilise_docs_promo_immo_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<br />\n";
		
						if ($compare_parameters['s10_utilise_docs_promo_immo_doc_num'] == "" && $compare_parameters['s10_utilise_docs_promo_immo_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s10_utilise_docs_promo_immo_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s10_utilise_docs_promo_immo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s10_utilise_docs_promo_immo_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_affiche_docs_promo_immo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_affiche_docs_promo_immo_bool\" id=\"s10_affiche_docs_promo_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_affiche_docs_promo_immo_bool\" id=\"s10_affiche_docs_promo_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_affiche_docs_promo_immo_bool\" id=\"s10_affiche_docs_promo_immo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_affiche_docs_promo_immo_bool\" id=\"s10_affiche_docs_promo_immo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_affiche_docs_promo_immo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_affiche_docs_promo_immo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s10_affiche_docs_promo_immo_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s10_affiche_docs_promo_immo_doc_num'] == "" && $parameters['s10_affiche_docs_promo_immo_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s10_affiche_docs_promo_immo_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s10_affiche_docs_promo_immo_doc_num\" id=\"s10_affiche_docs_promo_immo_doc_num\" value=\"" . $parameters['s10_affiche_docs_promo_immo_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s10_affiche_docs_promo_immo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s10_affiche_docs_promo_immo_doc_num_del\" id=\"s10_affiche_docs_promo_immo_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s10_affiche_docs_promo_immo_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s10_affiche_docs_promo_immo_doc_num_tmp\" id=\"s10_affiche_docs_promo_immo_doc_num_tmp\" value=\"" . $parameters['s10_affiche_docs_promo_immo_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s10_affiche_docs_promo_immo_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s10_affiche_docs_promo_immo_doc_num_tmp_del\" id=\"s10_affiche_docs_promo_immo_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s10_affiche_docs_promo_immo_doc_num_up\" id=\"s10_affiche_docs_promo_immo_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s10_affiche_docs_promo_immo_doc_phy\" id=\"s10_affiche_docs_promo_immo_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s10_affiche_docs_promo_immo_doc_num'] == "" && $parameters['s10_affiche_docs_promo_immo_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s10_affiche_docs_promo_immo_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s10_affiche_docs_promo_immo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s10_affiche_docs_promo_immo_doc_phy\" id=\"s10_affiche_docs_promo_immo_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<br />\n";
		
						if ($compare_parameters['s10_affiche_docs_promo_immo_doc_num'] == "" && $compare_parameters['s10_affiche_docs_promo_immo_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s10_affiche_docs_promo_immo_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s10_affiche_docs_promo_immo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s10_affiche_docs_promo_immo_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_affiche_docs_promo_immo_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_affiche_docs_promo_immo_sinon\" id=\"s10_affiche_docs_promo_immo_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_affiche_docs_promo_immo_sinon\" id=\"s10_affiche_docs_promo_immo_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s10_affiche_docs_promo_immo_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_logo_docs_immobiliers_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_logo_docs_immobiliers_bool\" id=\"s10_logo_docs_immobiliers_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_logo_docs_immobiliers_bool\" id=\"s10_logo_docs_immobiliers_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s10_logo_docs_immobiliers_bool\" id=\"s10_logo_docs_immobiliers_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s10_logo_docs_immobiliers_bool\" id=\"s10_logo_docs_immobiliers_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_logo_docs_immobiliers_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s10_logo_docs_immobiliers_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s10_logo_docs_immobiliers_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s10_logo_docs_immobiliers_doc_num'] == "" && $parameters['s10_logo_docs_immobiliers_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s10_logo_docs_immobiliers_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s10_logo_docs_immobiliers_doc_num\" id=\"s10_logo_docs_immobiliers_doc_num\" value=\"" . $parameters['s10_logo_docs_immobiliers_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s10_logo_docs_immobiliers_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s10_logo_docs_immobiliers_doc_num_del\" id=\"s10_logo_docs_immobiliers_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s10_logo_docs_immobiliers_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s10_logo_docs_immobiliers_doc_num_tmp\" id=\"s10_logo_docs_immobiliers_doc_num_tmp\" value=\"" . $parameters['s10_logo_docs_immobiliers_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s10_logo_docs_immobiliers_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s10_logo_docs_immobiliers_doc_num_tmp_del\" id=\"s10_logo_docs_immobiliers_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s10_logo_docs_immobiliers_doc_num_up\" id=\"s10_logo_docs_immobiliers_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s10_logo_docs_immobiliers_doc_phy\" id=\"s10_logo_docs_immobiliers_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s10_logo_docs_immobiliers_doc_num'] == "" && $parameters['s10_logo_docs_immobiliers_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s10_logo_docs_immobiliers_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s10_logo_docs_immobiliers_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s10_logo_docs_immobiliers_doc_phy\" id=\"s10_logo_docs_immobiliers_doc_phy\" value=\"\" readonly=\"readonly\"/>&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<br />\n";
		
						if ($compare_parameters['s10_logo_docs_immobiliers_doc_num'] == "" && $compare_parameters['s10_logo_docs_immobiliers_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s10_logo_docs_immobiliers_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s10_logo_docs_immobiliers_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s10_logo_docs_immobiliers_doc_phy\" value=\"\" readonly=\"readonly\"/>&nbsp; Copie physique (donner le nom du document)\n";
					}
				}
				break;
		}
		return $html;
	}

	function s10_logo_docs_immobiliers_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_logo_docs_immobiliers_sinon\" id=\"s10_logo_docs_immobiliers_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s10_logo_docs_immobiliers_sinon\" id=\"s10_logo_docs_immobiliers_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s10_logo_docs_immobiliers_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

?>