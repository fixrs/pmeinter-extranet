<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $DB;

		$_FORM =

			form_header($action).

			"	<div class=\"entry\">\n".
			"		" . etudes_key($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . date_affichage($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . poste($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . exigences($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . remuneration($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . date_entree($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . contact($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . afficher($action) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$key = getorpost('key');
		$html = "";
		switch ($action) {
			case "add":
				$html =
					"<form name=\"emploi\" action=\"emplois.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n";
				break;
			case "modify":
				$html =
					"<form name=\"emploi\" action=\"emplois.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"key\" value=\"" . $key . "\" />\n";
				break;
			case "read":
				$html =
					"<div class=\"readform\">\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					//"	<input type=\"submit\" style=\"display:none;\"/>\n".
					"	<input type=\"submit\" class=\"submit\" value=\"Soumettre\"  />\n".//hta_poste.update(); onclick=\"javascript: hta_exigences.update(); hta_remuneration.update(); hta_date_entree.update(); document.emploi.submit();\"
					"</form>\n";
				break;
			case "modify":
				$html .=
					//"	<input type=\"submit\" style=\"display:none;\"/>\n".
					"	<input type=\"submit\" class=\"submit\" value=\"Soumettre\"  />\n".//hta_poste.update(); onclick=\"javascript: hta_exigences.update(); hta_remuneration.update(); hta_date_entree.update(); document.emploi.submit();\"
					"</form>\n";
				break;
			case "read":
				$html .=
					"</div>\n";
				break;
		}
		return $html;
	}

	function etudes_key($action) {
		global $DB;
		$key = getorpost('key');
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"etudes_key\">Nom de l'&eacute;tude&nbsp;: </label>\n".
					"<select id=\"etudes_key\" name=\"etudes_key\">\n";

				if ($_SESSION['user_type'] == 1) {
					$html .= "<option class=\"default\" value=\"-1\"> - Choisir l'&eacute;tude - </option>\n";

					$DB->query(
						"SELECT `key`, `nom` ".
						"FROM `etudes` ".
						"WHERE `actif` = '1' ".
						"ORDER BY `key` ASC"
					);
					while ($DB->next_record()) {
						$key = $DB->getField("key");
						$nom = $DB->getField("nom");
						$html .=
							"<option selectname=\"etudes_key\" value=\"" . $key . "\">" . $nom . "</option>\n";
					}

				} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
					$DB->query(
						"SELECT `key`, `nom` ".
						"FROM `etudes` ".
						"WHERE `key` = '" . $_SESSION['user_etudes_key'] . "' AND `actif` = '1' "
					);
					while ($DB->next_record()) {
						$key = $DB->getField("key");
						$nom = $DB->getField("nom");
						$html .=
							"<option selectname=\"etudes_key\" value=\"" . $key . "\">" . $nom . "</option>\n";
					}
				}

				$html .=
					"</select>\n";
				break;

			case "modify":
				$DB->query(
					"SELECT ee.etudes_key AS etude_key, e.nom AS etude_nom ".
					"FROM `etudes` AS e, `etudes_emplois` AS ee ".
					"WHERE ee.key = '" . addslashes($key) . "' AND ee.etudes_key = e.key "
				);
				while ($DB->next_record()) {
					$etude_key = $DB->getField("etude_key");
					$etude_nom = $DB->getField("etude_nom");
				}
				$html .=
					"<input type=\"hidden\" name=\"etudes_key\" value=\"" . $etude_key . "\" />\n".
					"<label>Nom de l'&eacute;tude&nbsp;: </label>\n".
					"<span class=\"answer\">" . $etude_nom . "</span>";
				break;

			case "read":
				$html .=
					"<label>Nom de l'&eacute;tude&nbsp;: </label>\n".
					"<span class=\"answer\"><!---HIDDEN_etude_nom /HIDDEN_etude_nom---></span>";
				break;
		}
		return $html;
	}

	function poste($action){
		global $DB;
		$key = getorpost('key');
		$html = "";
		switch ($action) {

			case "add":
				$poste = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('poste')))));

				$html .=
					"<label for=\"poste\">Titre du poste&nbsp;: </label>\n".
					"<input type=\"text\" name=\"poste\" id=\"poste\" value=\"" . strip_tags($poste) . "\" style=\"width: 450px;\" />\n";
					// "<div id=\"hta_poste_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_poste = new HyperTextArea('emploi', 'poste', '" . $poste . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					// "<br clear=\"all\" />\n".
					// "<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					// "<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";
				break;

			case "modify":

				$poste = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('poste')))));

				if ($poste == "") {
					$DB->query(
						"SELECT `poste` ".
						"FROM `etudes_emplois` ".
						"WHERE `key` = " . addslashes($key) . ";"
					);
					while ($DB->next_record()) {
						$poste = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes($DB->getField('poste')))));
					}
				}

				$html .=
					"<label for=\"poste\">Titre du poste&nbsp;: </label>\n".
					"<input type=\"text\" name=\"poste\" id=\"poste\" value=\"" . strip_tags($poste) . "\" style=\"width: 450px;\" />\n";
					// "<div id=\"hta_poste_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_poste = new HyperTextArea('emploi', 'poste', '" . $poste . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					// "<br clear=\"all\" />\n".
					// "<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					// "<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";
					//"	<input type=\"text\" name=\"date_entree\" id=\"date_entree\" onchange=\"javascript: adjustDateField('date_entree');\" value=\"\" /><span class=\"note\" style=\"font-size:0.8em;\">Format&nbsp;: AAAA-MM-JJ</span>\n";
				break;

			case "read":
				$html .=
					"	<label>Titre du poste&nbsp;: </label>\n".
					"	<span class=\"answer\"><!---HIDDEN_poste /HIDDEN_poste---></span>\n";
				break;
		}


		return $html;


		/*$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"poste\">Titre du poste&nbsp;:</label>\n".
					"<input type=\"radio\" class=\"radio\" id=\"poste_notaire\" name=\"poste\" value=\"1\" /><span class=\"radio_label\">Notaire</span>".
					"<input type=\"radio\" class=\"radio\" id=\"poste_technicien\" name=\"poste\" value=\"2\" /><span class=\"radio_label\">Technicien(ne) juridique</span>";
				break;
			case "modify":
				$html .=
					"<label for=\"poste\">Titre du poste&nbsp;:</label>\n".
					"<input type=\"radio\" class=\"radio\" id=\"poste_notaire\" name=\"poste\" value=\"1\" /><span class=\"radio_label\">Notaire</span>".
					"<input type=\"radio\" class=\"radio\" id=\"poste_technicien\" name=\"poste\" value=\"2\" /><span class=\"radio_label\">Technicien(ne) juridique</span>";
				break;
			case "read":
				$html .=
					"<label>Titre du poste&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_poste /HIDDEN_poste---></span>";
				break;
		}
		return $html;*/
	}

	function date_affichage($action) {
		global $DB;
		$html = "";
		switch ($action) {

			case "add":
				$html .=
					"	<label for=\"date_affichage\">Date d'affichage&nbsp;: </label>\n".
					"	<input type=\"text\" name=\"date_affichage\" id=\"date_affichage\" onchange=\"javascript: adjustDateField('date_affichage');\" value=\"\" /><span class=\"note\" style=\"font-size:0.8em;\">Format&nbsp;: AAAA-MM-JJ</span>\n";
				break;

			case "modify":
				$html .=
					"	<label for=\"date_affichage\">Date d'affichage&nbsp;: </label>\n".
					"	<input type=\"text\" name=\"date_affichage\" id=\"date_affichage\" onchange=\"javascript: adjustDateField('date_affichage');\" value=\"\" /><span class=\"note\" style=\"font-size:0.8em;\">Format&nbsp;: AAAA-MM-JJ</span>\n";
				break;

			case "read":
				$html .=
					"	<label>Date d'affichage (AAAA-MM-JJ)&nbsp;: </label>\n".
					"	<span class=\"answer\"><!---HIDDEN_date_affichage /HIDDEN_date_affichage---></span>\n";
				break;
		}
		return $html;
	}

	function date_entree($action) {
		global $DB;
		$key = getorpost('key');
		$html = "";
		switch ($action) {

			case "add":
				$date_entree = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('date_entree')))));
				$date_entree = getorpost('date_entree');

				$html .=
					"	<label for=\"date_entree\">Date d'entr&eacute;e en fonction&nbsp;: </label>\n".
					//"<div id=\"hta_date_entree_div\" class=\"hta_rw\">\n".
					//"	<script language=\"javaScript\" type=\"text/javascript\">\n".
					//"		hta_date_entree = new HyperTextArea('emploi', 'date_entree', '" . $date_entree . "', 582, 300, '');\n".
					//"		document.getElementById('hta_date_entree_div').htmlarea();\n".
					//"		chrcount(undefined);\n".
					//"	</script>\n".
					//"</div>\n".

					"<textarea name=\"date_entree\" id=\"date_entree\" rows=\"15\">" . $date_entree . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { $('#date_entree').htmlarea(); }); \n".
					"</script>\n".

					"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					"<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";
					//"	<input type=\"text\" name=\"date_entree\" id=\"date_entree\" onchange=\"javascript: adjustDateField('date_entree');\" value=\"\" /><span class=\"note\" style=\"font-size:0.8em;\">Format&nbsp;: AAAA-MM-JJ</span>\n";
				break;

			case "modify":

				$date_entree = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('date_entree')))));
				$date_entree = getorpost('date_entree');

				if ($date_entree == "") {
					$DB->query(
						"SELECT `date_entree` ".
						"FROM `etudes_emplois` ".
						"WHERE `key` = " . addslashes($key) . ";"
					);
					while ($DB->next_record()) {
						$date_entree = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes($DB->getField('date_entree')))));
						$date_entree = $DB->getField('date_entree');
					}
				}

				$html .=
					"	<label for=\"date_entree\">Date d'entr&eacute;e en fonction&nbsp;: </label>\n".
					// "<div id=\"hta_date_entree_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_date_entree = new HyperTextArea('emploi', 'date_entree', '" . $date_entree . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".

					"<textarea name=\"date_entree\" id=\"date_entree\" rows=\"15\">" . $date_entree . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { $('#date_entree').htmlarea(); }); \n".
					"</script>\n".

					"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					"<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";
					//"	<input type=\"text\" name=\"date_entree\" id=\"date_entree\" onchange=\"javascript: adjustDateField('date_entree');\" value=\"\" /><span class=\"note\" style=\"font-size:0.8em;\">Format&nbsp;: AAAA-MM-JJ</span>\n";
				break;

			case "read":
				$html .=
					"	<label>Date d'entr&eacute;e en fonction (AAAA-MM-JJ)&nbsp;: </label>\n".
					"	<span class=\"answer\"><!---HIDDEN_date_entree /HIDDEN_date_entree---></span>\n";
				break;
		}


		return $html;
	}

	function exigences($action) {
		global $DB;
		$key = getorpost('key');
		$html = "";

		switch ($action) {
			case "add":
				$exigences = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('exigences')))));
				$exigences = getorpost('exigences');

				$html .=
					"<label for=\"exigences\">Exigences requises&nbsp;:</label>\n".
					// "<div id=\"hta_exigences_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_exigences = new HyperTextArea('emploi', 'exigences', '" . $exigences . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					"<textarea name=\"exigences\" id=\"exigences\" rows=\"15\">" . $exigences . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { $('#exigences').htmlarea(); }); \n".
					"</script>\n".

//					"<br clear=\"all\" />\n".
//					"<span class=\"hta_chrcount\"><span id=\"exigences_chrcount\" class=\"exigences_chrcount\">0</span>&nbsp;/&nbsp;1750 caract&egrave;res maximum.</span>\n".
//					"<span style=\"float:right\";><input type=\"button\" class=\"hta_button\" value=\"Actualiser le nb de caract&egrave;res\" onclick=\"javascript: hta_exigences.updateChrCount();\" />&nbsp;&nbsp;<input type=\"button\" class=\"hta_button\" value=\"Tout effacer\" onclick=\"javascript: hta_exigences.clearText(); hta_exigences.updateChrCount();\" /></span>\n".
					"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					"<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";
				break;

			case "modify":
				$exigences = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('exigences')))));
				$exigences = getorpost('exigences');

				if ($exigences == "") {
					$DB->query(
						"SELECT `exigences` ".
						"FROM `etudes_emplois` ".
						"WHERE `key` = " . addslashes($key) . ";"
					);
					while ($DB->next_record()) {
						$exigences = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes($DB->getField('exigences')))));
						$exigences = $DB->getField('exigences');
					}
				}

				$html .=
					"<label for=\"exigences\">Exigences requises&nbsp;:</label>\n".
					// "<div id=\"hta_exigences_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_exigences = new HyperTextArea('emploi', 'exigences', '" . $exigences . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					"<textarea name=\"exigences\" id=\"exigences\" rows=\"15\">" . $exigences . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { $('#exigences').htmlarea(); }); \n".
					"</script>\n".
//					"<br clear=\"all\" />\n".
//					"<span class=\"hta_chrcount\"><span id=\"exigences_chrcount\" class=\"exigences_chrcount\">0</span>&nbsp;/&nbsp;1750 caract&egrave;res maximum.</span>\n".
//					"<span style=\"float:right\";><input type=\"button\" class=\"hta_button\" value=\"Actualiser le nb de caract&egrave;res\" onclick=\"javascript: hta_exigences.updateChrCount();\" />&nbsp;&nbsp;<input type=\"button\" class=\"hta_button\" value=\"Tout effacer\" onclick=\"javascript: hta_exigences.clearText(); hta_exigences.updateChrCount();\" /></span>\n".
					"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					"<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";
				break;

			case "read":
				$html .=
					"<label>Exigences requises&nbsp;:</label>\n".
					"<div class=\"hta_ro\"><!---HIDDEN_exigences /HIDDEN_exigences---></div>\n";
				break;
		}
		return $html;
	}

	function remuneration($action) {
		global $DB;
		$key = getorpost('key');
		$html = "";

		switch ($action) {
			case "add":
				$remuneration = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('remuneration')))));
				$remuneration = getorpost('remuneration');

				$html .=
					"<label for=\"remuneration\">R&eacute;mun&eacute;ration et avantages&nbsp;:</label>\n".
					// "<div id=\"hta_remuneration_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_remuneration = new HyperTextArea('emploi', 'remuneration', '" . $remuneration . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					"<textarea name=\"remuneration\" id=\"remuneration\" rows=\"15\">" . $remuneration . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { $('#remuneration').htmlarea(); }); \n".
					"</script>\n".
//					"<br clear=\"all\" />\n".
//					"<span class=\"hta_chrcount\"><span id=\"remuneration_chrcount\" class=\"remuneration_chrcount\">0</span>&nbsp;/&nbsp;1750 caract&egrave;res maximum.</span>\n".
//					"<span style=\"float:right\";><input type=\"button\" class=\"hta_button\" value=\"Actualiser le nb de caract&egrave;res\" onclick=\"javascript: hta_remuneration.updateChrCount();\" />&nbsp;&nbsp;<input type=\"button\" class=\"hta_button\" value=\"Tout effacer\" onclick=\"javascript: hta_remuneration.clearText(); hta_remuneration.updateChrCount();\" /></span>\n".
					"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					"<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";
				break;

			case "modify":
				$remuneration = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('remuneration')))));
				$remuneration = getorpost('remuneration');

				if ($remuneration == "") {
					$DB->query(
						"SELECT `remuneration` ".
						"FROM `etudes_emplois` ".
						"WHERE `key` = " . addslashes($key) . ";"
					);
					while ($DB->next_record()) {
						$remuneration = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes($DB->getField('remuneration')))));
						$remuneration = $DB->getField('remuneration');
					}
				}

				$html .=
					"<label for=\"remuneration\">R&eacute;mun&eacute;ration et avantages&nbsp;:</label>\n".
					// "<div id=\"hta_remuneration_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_remuneration = new HyperTextArea('emploi', 'remuneration', '" . $remuneration . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					"<textarea name=\"remuneration\" id=\"remuneration\" rows=\"15\">" . $remuneration . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { $('#remuneration').htmlarea(); }); \n".
					"</script>\n".
//					"<br clear=\"all\" />\n".
//					"<span class=\"hta_chrcount\"><span id=\"remuneration_chrcount\" class=\"exigences_chrcount\">0</span>&nbsp;/&nbsp;1750 caract&egrave;res maximum.</span>\n".
//					"<span style=\"float:right\";><input type=\"button\" class=\"hta_button\" value=\"Actualiser le nb de caract&egrave;res\" onclick=\"javascript: hta_remuneration.updateChrCount();\" />&nbsp;&nbsp;<input type=\"button\" class=\"hta_button\" value=\"Tout effacer\" onclick=\"javascript: hta_remuneration.clearText(); hta_remuneration.updateChrCount();\" /></span>\n".
					"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					"<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";
				break;

			case "read":
				$html .=
					"<label>R&eacute;mun&eacute;ration et avantages&nbsp;:</label>\n".
					"<div class=\"hta_ro\"><!---HIDDEN_remuneration /HIDDEN_remuneration---></div>\n";
				break;
		}
		return $html;
	}

	function contact($action) {
		global $DB;
		$key = getorpost('key');

		$html = "";
		switch ($action) {
			case "add":
				$contact = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('contact')))));
				$contact = getorpost('contact');

				$html .=
					"<label for=\"contact\">Adresse du contact&nbsp;:</label>\n".
					//"<textarea rows=\"3\" cols=\"40\" id=\"contact\" name=\"contact\"></textarea><br />\n".

					// "<div id=\"hta_contact_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_contact = new HyperTextArea('emploi', 'contact', '" . $contact . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					"<textarea name=\"contact\" id=\"contact\" rows=\"15\">" . $contact . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { $('#contact').htmlarea(); }); \n".
					"</script>\n".

					"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					"<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";
				break;

			case "modify":

				$contact = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('contact')))));
				$contact = getorpost('contact');

				if ($contact == "") {
					$DB->query(
						"SELECT `contact` ".
						"FROM `etudes_emplois` ".
						"WHERE `key` = " . addslashes($key) . ";"
					);
					while ($DB->next_record()) {
						$contact = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes($DB->getField('contact')))));
						$contact = $DB->getField('contact');
					}
				}


				$html .=
					"<label for=\"contact\">Adresse du contact&nbsp;:</label>\n".
					// "<div id=\"hta_contact_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_contact = new HyperTextArea('emploi', 'contact', '" . $contact . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					"<textarea name=\"contact\" id=\"contact\" rows=\"15\">" . $contact . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { $('#contact').htmlarea(); }); \n".
					"</script>\n".

					"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n".
					"<div class=\"notice ie7fix\"><strong>Ne jamais copier/coller directement depuis Words. Utilisez plut&ocirc;t NotePad ou BlocNote</strong>.</div>\n";

					//"<textarea rows=\"3\" cols=\"40\" id=\"contact\" name=\"contact\"></textarea><br />\n".
					//"<br />\n";
				break;

			case "read":
				$html .=
					"<label>Adresse de contact&nbsp;:</label>\n".
					"<div class=\"hta_ro\"><!---HIDDEN_contact /HIDDEN_contact---></div>\n";
				break;
		}
		return $html;
	}

	function afficher($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"afficher\">Afficher l'offre sur le site publique&nbsp;?</label>\n".
					"<input type=\"radio\" class=\"radio\" id=\"afficher_oui\" name=\"afficher\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" id=\"afficher_non\" name=\"afficher\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;
			case "modify":
				$html .=
					"<label for=\"afficher\">Afficher l'offre sur le site publique&nbsp;?</label>\n".
					"<input type=\"radio\" class=\"radio\" id=\"afficher_oui\" name=\"afficher\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" id=\"afficher_non\" name=\"afficher\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;
			case "read":
				$html .=
					"<label>Afficher l'offre sur le site publique&nbsp;?</label>\n".
					"<span class=\"answer\"><!---HIDDEN_afficher /HIDDEN_afficher---></span>";
				break;
		}
		return $html;
	}

?>
