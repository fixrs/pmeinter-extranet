<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =	

			form_header($action, "21").

			"	<div class=\"entry\">\n".
			"		<label for=\"s21_rdprm_publie_en_ligne_bool\">Au RDPRM, est-ce que vous publiez en ligne&nbsp;?</label>\n".
			"		" . s21_rdprm_publie_en_ligne_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s21_rdprm_publie_en_ligne_sioui_qui\">Si <b>OUI</b>, qui est autoris&eacute;&nbsp;?</label>\n".
			"		" . s21_rdprm_publie_en_ligne_sioui_qui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s21_rdprm_publie_en_ligne_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s21_rdprm_publie_en_ligne_sinon($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}

	function s21_rdprm_publie_en_ligne_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s21_rdprm_publie_en_ligne_bool\" id=\"s21_rdprm_publie_en_ligne_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s21_rdprm_publie_en_ligne_bool\" id=\"s21_rdprm_publie_en_ligne_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s21_rdprm_publie_en_ligne_bool\" id=\"s21_rdprm_publie_en_ligne_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s21_rdprm_publie_en_ligne_bool\" id=\"s21_rdprm_publie_en_ligne_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s21_rdprm_publie_en_ligne_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s21_rdprm_publie_en_ligne_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s21_rdprm_publie_en_ligne_sioui_qui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s21_rdprm_publie_en_ligne_sioui_qui\" id=\"s21_rdprm_publie_en_ligne_sioui_qui_1\" value=\"1\" /><span class=\"radio_label\">Collaboratrice</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s21_rdprm_publie_en_ligne_sioui_qui\" id=\"s21_rdprm_publie_en_ligne_sioui_qui_0\" value=\"0\" /><span class=\"radio_label\">Notaire</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s21_rdprm_publie_en_ligne_sioui_qui\" id=\"s21_rdprm_publie_en_ligne_sioui_qui_1\" value=\"1\" /><span class=\"radio_label\">Collaboratrice</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s21_rdprm_publie_en_ligne_sioui_qui\" id=\"s21_rdprm_publie_en_ligne_sioui_qui_0\" value=\"0\" /><span class=\"radio_label\">Notaire</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s21_rdprm_publie_en_ligne_sioui_qui\" value=\"1\" /><span class=\"radio_label\">Collaboratrice</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s21_rdprm_publie_en_ligne_sioui_qui\" value=\"0\" /><span class=\"radio_label\">Notaire</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s21_rdprm_publie_en_ligne_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s21_rdprm_publie_en_ligne_sinon\" id=\"s21_rdprm_publie_en_ligne_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s21_rdprm_publie_en_ligne_sinon\" id=\"s21_rdprm_publie_en_ligne_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s21_rdprm_publie_en_ligne_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

?>