<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =
		
			form_header($action, "12").

			"	<div class=\"entry\">\n".
			"		<label for=\"s12_recoivent_lintermediaire_bool\">Est-ce que tous les notaires re&ccedil;oivent l'InterM&eacute;diaire de PME INTER Notaires&nbsp;?</label>\n".
			"		" . s12_recoivent_lintermediaire_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s12_recoivent_lintermediaire_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s12_recoivent_lintermediaire_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s12_lisent_lintermediaire_bool\">Est-ce que les notaires lisent syst&eacute;matiquement l'InterM&eacute;diaire de PME INTER Notaires&nbsp;?</label>\n".
			"		" . s12_lisent_lintermediaire_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s12_lisent_lintermediaire_sinon\">Si <b>NON</b>, pourquoi:&nbsp;</label>\n".
			"		" . s12_lisent_lintermediaire_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s12_lisent_courriels_bool\">Est-ce que les notaires lisent syst&eacute;matiquement les courriels de PME INTER Notaires&nbsp;?</label>\n".
			"		" . s12_lisent_courriels_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s12_lisent_courriels_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s12_lisent_courriels_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s12_actionnaire_satisfait_communi_bool\">Est-ce que l'actionnaire est satisfait des moyens et de la fr&eacute;quence des communications&nbsp;?</label>\n".
			"		" . s12_actionnaire_satisfait_communi_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s12_actionnaire_satisfait_communi_sinon\">Si <b>NON</b>, pourquoi et &eacute;num&eacute;rer ses suggestions&nbsp;:</label>\n".
			"		" . s12_actionnaire_satisfait_communi_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s12_actionnaire_informe_bool\">Est-ce que l'actionnaire trouve qu'il est suffisamment inform&eacute; des activit&eacute;s en cours et &agrave; venir de PME INTER Notaires&nbsp;?</label>\n".
			"		" . s12_actionnaire_informe_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s12_actionnaire_informe_sinon\">Si <b>NON</b>, pourquoi et &eacute;num&eacute;rer ses suggestions&nbsp;:</label>\n".
			"		" . s12_actionnaire_informe_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s12_notaires_lisent_appels_bool\">Est-ce que les notaires de l'&eacute;tude lisent les \"appels &agrave; tous\" de leurs coll&egrave;gues transmis par PME INTER Notaires&nbsp;?</label>\n".
			"		" . s12_notaires_lisent_appels_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s12_notaires_lisent_appels_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s12_notaires_lisent_appels_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s12_notaires_repondent_appels_bool\">Est-ce que les notaires de l'&eacute;tude r&eacute;pondent aux \"appels &agrave; tous\"&nbsp;?</label>\n".
			"		" . s12_notaires_repondent_appels_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s12_notaires_repondent_appels_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s12_notaires_repondent_appels_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s12_etude_nb_reponses_appels\">Combien de fois l'&eacute;tude a-t-elle r&eacute;pondue aux \"appels &agrave; tous\" au cours de la derni&egrave;re ann&eacute;e&nbsp;?</label>\n".
			"		" . s12_etude_nb_reponses_appels($action, $parameters) . "\n".
			"	</div>\n".
/*
			"	<div class=\"suggestion\">\n".
			"		Suggestions &agrave; faire &agrave; l'&eacute;tude&nbsp;:<br />\n".
			"		<br />\n".
			"		Rappeler &agrave; l'&eacute;tude que les r&eacute;ponses aux \"appels &agrave; tous\" sont disponibles sur l'extranet du r&eacute;seau.\n".
			"	</div>\n".
*/
			form_footer($action);

		return $_FORM;
	}

	function s12_recoivent_lintermediaire_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_recoivent_lintermediaire_bool\" id=\"s12_recoivent_lintermediaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_recoivent_lintermediaire_bool\" id=\"s12_recoivent_lintermediaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_recoivent_lintermediaire_bool\" id=\"s12_recoivent_lintermediaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_recoivent_lintermediaire_bool\" id=\"s12_recoivent_lintermediaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_recoivent_lintermediaire_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_recoivent_lintermediaire_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s12_recoivent_lintermediaire_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_recoivent_lintermediaire_sinon\" id=\"s12_recoivent_lintermediaire_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_recoivent_lintermediaire_sinon\" id=\"s12_recoivent_lintermediaire_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s12_recoivent_lintermediaire_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s12_lisent_lintermediaire_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_lisent_lintermediaire_bool\" id=\"s12_lisent_lintermediaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_lisent_lintermediaire_bool\" id=\"s12_lisent_lintermediaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_lisent_lintermediaire_bool\" id=\"s12_lisent_lintermediaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_lisent_lintermediaire_bool\" id=\"s12_lisent_lintermediaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_lisent_lintermediaire_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_lisent_lintermediaire_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s12_lisent_lintermediaire_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_lisent_lintermediaire_sinon\" id=\"s12_lisent_lintermediaire_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_lisent_lintermediaire_sinon\" id=\"s12_lisent_lintermediaire_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s12_lisent_lintermediaire_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s12_lisent_courriels_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_lisent_courriels_bool\" id=\"s12_lisent_courriels_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_lisent_courriels_bool\" id=\"s12_lisent_courriels_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_lisent_courriels_bool\" id=\"s12_lisent_courriels_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_lisent_courriels_bool\" id=\"s12_lisent_courriels_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_lisent_courriels_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_lisent_courriels_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s12_lisent_courriels_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_lisent_courriels_sinon\" id=\"s12_lisent_courriels_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_lisent_courriels_sinon\" id=\"s12_lisent_courriels_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s12_lisent_courriels_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s12_actionnaire_satisfait_communi_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_actionnaire_satisfait_communi_bool\" id=\"s12_actionnaire_satisfait_communi_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_actionnaire_satisfait_communi_bool\" id=\"s12_actionnaire_satisfait_communi_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_actionnaire_satisfait_communi_bool\" id=\"s12_actionnaire_satisfait_communi_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_actionnaire_satisfait_communi_bool\" id=\"s12_actionnaire_satisfait_communi_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_actionnaire_satisfait_communi_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_actionnaire_satisfait_communi_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s12_actionnaire_satisfait_communi_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_actionnaire_satisfait_communi_sinon\" id=\"s12_actionnaire_satisfait_communi_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_actionnaire_satisfait_communi_sinon\" id=\"s12_actionnaire_satisfait_communi_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s12_actionnaire_satisfait_communi_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s12_actionnaire_informe_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_actionnaire_informe_bool\" id=\"s12_actionnaire_informe_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_actionnaire_informe_bool\" id=\"s12_actionnaire_informe_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_actionnaire_informe_bool\" id=\"s12_actionnaire_informe_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_actionnaire_informe_bool\" id=\"s12_actionnaire_informe_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_actionnaire_informe_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_actionnaire_informe_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s12_actionnaire_informe_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_actionnaire_informe_sinon\" id=\"s12_actionnaire_informe_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_actionnaire_informe_sinon\" id=\"s12_actionnaire_informe_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s12_actionnaire_informe_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s12_notaires_lisent_appels_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_notaires_lisent_appels_bool\" id=\"s12_notaires_lisent_appels_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_notaires_lisent_appels_bool\" id=\"s12_notaires_lisent_appels_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_notaires_lisent_appels_bool\" id=\"s12_notaires_lisent_appels_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_notaires_lisent_appels_bool\" id=\"s12_notaires_lisent_appels_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_notaires_lisent_appels_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_notaires_lisent_appels_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s12_notaires_lisent_appels_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_notaires_lisent_appels_sinon\" id=\"s12_notaires_lisent_appels_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_notaires_lisent_appels_sinon\" id=\"s12_notaires_lisent_appels_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s12_notaires_lisent_appels_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s12_notaires_repondent_appels_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_notaires_repondent_appels_bool\" id=\"s12_notaires_repondent_appels_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_notaires_repondent_appels_bool\" id=\"s12_notaires_repondent_appels_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s12_notaires_repondent_appels_bool\" id=\"s12_notaires_repondent_appels_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s12_notaires_repondent_appels_bool\" id=\"s12_notaires_repondent_appels_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_notaires_repondent_appels_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s12_notaires_repondent_appels_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s12_notaires_repondent_appels_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_notaires_repondent_appels_sinon\" id=\"s12_notaires_repondent_appels_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s12_notaires_repondent_appels_sinon\" id=\"s12_notaires_repondent_appels_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s12_notaires_repondent_appels_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s12_etude_nb_reponses_appels($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s12_etude_nb_reponses_appels\" id=\"s12_etude_nb_reponses_appels\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s12_etude_nb_reponses_appels\" id=\"s12_etude_nb_reponses_appels\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"text\" class=\"text_short\" name=\"" . $compare_sid . "_s12_etude_nb_reponses_appels\" value=\"\" readonly=\"readonly\" />\n";
					}
				}
				break;
		}
		return $html;
	}

?>