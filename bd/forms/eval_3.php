<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "3").
		
			"	<div class=\"entry_long\">\n".
			"		<label for=\"employes\">V&eacute;rifier avec l'&eacute;tude la liste des employ&eacute;s inscrits dans les registres du r&eacute;seau et ajouter les employ&eacute;s manquants&nbsp;:</label>\n".
			"		" . employes($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s3_notes_personnel_etude\">Notes suppl&eacute;mentaires&nbsp;:</label>\n".
			"		" . s3_notes_personnel_etude($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}
	
	function employes($action, $parameters) {
		global $DB, $sid;
		$html = "";

		switch ($action) {	
			case "modify":

				// Suppression des employés enlevés
				if (is_array($_POST['employes_enlever']) && count($_POST['employes_enlever']) > 0) {
					foreach ($_POST['employes_enlever'] as $employes_key => $value) {
						if ($value == "1") {
							unset($_SESSION['evaluations'][$sid]['employes_employes_key'][$employes_key]);
							unset($parameters['employes_employes_key'][$employes_key]);
						}
					}
				}

				$fonctionsArray = get_fonctions();
				
				// Ajout du JavaScript
				$html .=
					"<script type=\"text/javascript\" language=\"javascript\">\n".
					"	var employes = new Array();\n";

				$query = 	
					"SELECT e.key AS employes_key, ef.fonctions_key AS fonctions_key, e.prenom AS prenom, e.nom AS nom, e.courriel AS courriel ".
					"FROM `employes` e, `employes_fonctions` ef ".
					"WHERE e.key = ef.employes_key ".
					"ORDER BY e.key ASC;";

				$DB->query($query);

				// on remplie un tableau jscript + php avec les valeurs de la table `employés`
				$employes = array();
				$employe_courant = "";
				$i = 0;
				while ($DB->next_record()) {
					if ($employe_courant != $DB->getField("employes_key")) {
						$i = 0;
						$employe_courant = $DB->getField("employes_key");
						$employes[$DB->getField("employes_key")]['employes_key'] = $DB->getField("employes_key");
						$employes[$DB->getField("employes_key")]['fullname'] = $DB->getField("prenom") . ", " . $DB->getField("nom");
						$html .=
							"employes['" . $DB->getField("employes_key") . "'] = new Array();\n".
							"employes['" . $DB->getField("employes_key") . "']['employes_key'] = '" . $DB->getField("employes_key") . "';\n".
							"employes['" . $DB->getField("employes_key") . "']['nom'] = '" . $DB->getField("nom") . "';\n".
							"employes['" . $DB->getField("employes_key") . "']['prenom'] = '" . $DB->getField("prenom") . "';\n".
							"employes['" . $DB->getField("employes_key") . "']['courriel'] = '" . $DB->getField("courriel") . "';\n".
							"employes['" . $DB->getField("employes_key") . "']['fonctions_key'] = new Array();\n".
							"employes['" . $DB->getField("employes_key") . "']['fonctions_key'][" . $i . "] = '" . $DB->getField("fonctions_key") . "';\n";
					} else {
						$html .=
							"employes['" . $DB->getField("employes_key") . "']['fonctions_key'][" . $i . "] = '" . $DB->getField("fonctions_key") . "';\n";
					}					
					$i++;
				}

				$html .=
					"	function employes_onSelect(self) { \n".
					"		var employes_key_fromSelect = self.options[self.selectedIndex].value;\n".
					"		var already_added = '-1';\n".
					"		var already_added_warning = document.getElementById('already_added_warning');\n".
					"		already_added_warning.style.display = 'none';\n".
					"		if (document.getElementById('employes_employes_key[' + employes_key_fromSelect + ']')) { \n".
					"			already_added = document.getElementById('employes_employes_key[' + employes_key_fromSelect + ']').value; \n".
					"		}\n".
					"		if (employes_key_fromSelect == '-2') { \n".
					"			employes_remplir_new();\n".
					"		} else { \n".
					"			if (employes_key_fromSelect != '' && employes_key_fromSelect != '-1' && employes_key_fromSelect != '-2') { \n".
					"				if (document.getElementById('employes_employes_key[' + employes_key_fromSelect + ']') && already_added != '') { \n".
					"					employes_showWarning(employes_key_fromSelect);\n".
					"				} else { \n".
					"					employes_remplir(self);\n".
					"				}\n".
					"			}\n".
					"		}\n".
					"	}\n".

					"	function employes_remplir(item) { \n".
					"		var employes_key_fromSelect = item.options[item.selectedIndex].value;\n".
					"		var employes_key_add = document.getElementById('employes_employes_key_add');\n".
					"		var courriel_add = document.getElementById('employes_courriel_add');\n".
					"		var nom_add = document.getElementById('employes_nom_add');\n".
					"		var prenom_add = document.getElementById('employes_prenom_add');\n".
					"		var fonctions_key_add = document.getElementById('employes_fonctions_key_add');\n".
					"		employes_key_add.value = employes[employes_key_fromSelect]['employes_key'];\n".
					"		courriel_add.value = employes[employes_key_fromSelect]['courriel'];\n".
					"		nom_add.value = employes[employes_key_fromSelect]['nom'];\n".
					"		prenom_add.value = employes[employes_key_fromSelect]['prenom'];\n".
					"		fonctions_key_array = employes[employes_key_fromSelect]['fonctions_key'];\n".
					"		for (k = 0; k < fonctions_key_add.options.length; k++) {\n".
					"			fonctions_key_add.options[k].selected = false;\n".
					"		}\n".
					"		for (i = 0; i < fonctions_key_array.length; i++) {\n".
					"			var j = fonctions_key_array[i];\n".
					"			for (k = 0; k < fonctions_key_add.options.length; k++) {\n".
					"				if (fonctions_key_add.options[k].value == j) {\n".
					"					fonctions_key_add.options[k].selected = true;\n".
					"				}\n".
					"			}\n".
					"		}\n".
					"		employes_enableForm()\n".
					"	}\n".

					"	function employes_remplir_new() { \n".
					"		var employes_key_add = document.getElementById('employes_employes_key_add');\n".
					"		var courriel_add = document.getElementById('employes_courriel_add');\n".
					"		var nom_add = document.getElementById('employes_nom_add');\n".
					"		var prenom_add = document.getElementById('employes_prenom_add');\n".
					"		var present_evaluation_oui_add = document.getElementById('employes_present_evaluation_oui_add');\n".
					"		var present_evaluation_non_add = document.getElementById('employes_present_evaluation_non_add');\n".
					"		var fonctions_key_add = document.getElementById('employes_fonctions_key_add');\n".
					"		var i = 0;\n".
					"		while (document.getElementById('employes_employes_key[new_' + i + ']')) { \n".
					"			i++;\n".
					"		}\n".
					"		employes_key = 'new_' + i;\n".
					"		for (k = 0; k < fonctions_key_add.options.length; k++) {\n".
					"			fonctions_key_add.options[k].selected = false;\n".
					"		}\n".
					"		employes_key_add.value = employes_key;\n".
					"		courriel_add.value = '';\n".
					"		nom_add.value = '';\n".
					"		nom_add.readOnly = false;\n".
					"		nom_add.className = 'void';\n".					
					"		prenom_add.value = '';\n".
					"		prenom_add.readOnly = false;\n".
					"		prenom_add.className = 'void';\n".					
					"		present_evaluation_oui_add.checked = false;\n".
					"		present_evaluation_non_add.checked = false;\n".
					"		employes_enableForm()\n".
					"	}\n".

					"	function employes_ajouter() { \n".
					"		var employes_key = document.getElementById('employes_employes_key_add');\n".
					"		var already_added_warning = document.getElementById('already_added_warning');\n".
					"		already_added_warning.style.display = 'none';\n".
					"		if (employes_key.value != '') { \n".
					"			if (document.getElementById('employes_employes_key[' + employes_key + ']')) { \n".
					"				employes_showWarning(employes_key);\n".
					"			} else { \n".
					"				document.getElementById('employes_ajouter').value = '1';\n".
					"				submitEvalForm('', '', '3');\n".
					"			}\n".
					"		}\n".
					"	}\n".

					"	function employes_enlever(employes_key) { \n".
					"		document.getElementById('employes_enlever[' + employes_key + ']').value = '1';\n".
					"		submitEvalForm('', '', '3');\n".
					"	}\n".
					
					"	function employes_enableForm() { \n".
					"		var employes_key = document.getElementById('employes_employes_key_add');\n".
					"		var nom = document.getElementById('employes_nom_add');\n".
					"		var prenom = document.getElementById('employes_prenom_add');\n".
					"		var fonctions = document.getElementById('employes_fonctions_key_add');\n".
					"		var courriel = document.getElementById('employes_courriel_add');\n".
					"		var raison_absence = document.getElementById('employes_raison_absence_add');\n".
					"		var employes_present_evaluation_oui = document.getElementById('employes_present_evaluation_oui_add');\n".
					"		var employes_present_evaluation_non = document.getElementById('employes_present_evaluation_non_add');\n".
					"		employes_key.disabled = false;\n".
					"		nom.disabled = false;\n".
					"		prenom.disabled = false;\n".
					"		fonctions.disabled = false;\n".
					"		courriel.disabled = false;\n".
					"		raison_absence.disabled = false;\n".
					"		employes_present_evaluation_oui.disabled = false;\n".
					"		employes_present_evaluation_non.disabled = false;\n".
					"	}\n".

					"	function employes_showWarning(employes_key) { \n".
					"		var table = document.getElementById('employes_table[' + employes_key + ']');\n".
					"		var already_added_warning = document.getElementById('already_added_warning');\n".
					"		table.style.backgroundColor = '#EEEEEE';\n".
					"		var string = \"employes_changeColor('employes_table[\" + employes_key + \"]', '#FFFFFF');\"\n".
					"		setTimeout(string, 1200);\n".
					"		already_added_warning.style.display = 'block';\n".
					"	}\n".

					"	function employes_changeColor(elementId, color) { \n".
					"		var element = document.getElementById(elementId);".
					"		element.style.backgroundColor = color;\n".
					"	}\n".
					"</script>\n";

				// Affiche et remplit le formulaire pour chaque employé précédement ajouté
				if (is_array($parameters['employes_employes_key']) && count($parameters['employes_employes_key']) > 0) {
					ksort($parameters['employes_employes_key']);
					foreach ($parameters['employes_employes_key'] as $employes_key) {
						if ($employes_key != "" && $employes_key != "-1") {

							$employes_employes_key = $employes_key;
							$employes_nom = $parameters['employes_nom'][$employes_key];
							$employes_prenom = $parameters['employes_prenom'][$employes_key];
							$employes_courriel = $parameters['employes_courriel'][$employes_key];
							$employes_raison_absence = $parameters['employes_raison_absence'][$employes_key];

							$employes_fonctions_key = array();
							if (is_array($parameters['employes_fonctions_key'][$employes_key]) && count($parameters['employes_fonctions_key'][$employes_key]) > 0) {
								foreach ($parameters['employes_fonctions_key'][$employes_key] as $nb => $fonctions_key) {
									$employes_fonctions_key[$fonctions_key] = 1;
								}
							}

							if ($parameters['employes_present_evaluation'][$employes_key] == "1") {
								$employes_present_evaluation_oui_checked = "checked=\"checked\"";
								$employes_present_evaluation_non_checked = "";
							} else {
								$employes_present_evaluation_non_checked = "checked=\"checked\"";
								$employes_present_evaluation_oui_checked = "";
							} 			

							$html .=				
								"<div id=\"notaires_div[" . $employes_employes_key . "]\" class=\"employes_entry\">\n".
								"	<table id=\"employes_table[" . $employes_employes_key . "]\" style=\"margin-top: 20px;\">\n".
								"		<tbody>\n".
								"			<tr>\n".
								"				<td width=\"200px\" rowspan=\"6\" style=\"text-align: center;\">" .
								"					<input type=\"hidden\" name=\"employes_employes_key[" . $employes_employes_key . "]\" id=\"employes_employes_key[" . $employes_employes_key . "]\" value=\"" . $employes_employes_key . "\" />\n".
								"					<input type=\"hidden\" name=\"employes_enlever[" . $employes_employes_key . "]\" id=\"employes_enlever[" . $employes_employes_key . "]\" value=\"\" />\n".
								"					<a href=\"javascript: employes_enlever('" . $employes_employes_key . "');\">Enlever</a><br />\n".
								"				</td>\n".
								"				<th>\n".
								"					Fonction(s) de l'employ&eacute;&nbsp;:\n".
								"				</th>\n".
								"				<td width=\"175px\">\n".
								"					<select id=\"employes_fonctions_key[" . $employes_key . "]\" name=\"employes_fonctions_key[" . $employes_employes_key . "][]\" multiple=\"multiple\" size=\"5\" style=\"width: 100%;\">\n";

							$fonctions = get_fonctions();

							if (is_array($fonctions) && count($fonctions) > 0) {
								foreach ($fonctions as $fonctions_key => $fonctions_nom) {
									if ($employes_fonctions_key[$fonctions_key] == 1) {
										$html .=
											"<option selectname=\"employes_fonctions_key[" . $employes_employes_key . "][]\" value=\"" . $fonctions_key . "\" selected=\"selected\">" . $fonctions_nom . "</option>\n";
									} else {
										$html .=
											"<option selectname=\"employes_fonctions_key[" . $employes_employes_key . "][]\" value=\"" . $fonctions_key . "\">" . $fonctions_nom . "</option>\n";
									}
								}
							}

							$html .=
								"					</select>\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Nom de l'employ&eacute;&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_nom[" . $employes_employes_key . "]\" id=\"employes_nom[" . $employes_employes_key . "]\" value=\"" . $employes_nom . "\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Pr&eacute;nom de l'employ&eacute;&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_prenom[" . $employes_employes_key . "]\" id=\"employes_prenom[" . $employes_employes_key . "]\" value=\"" . $employes_prenom . "\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Adresse courriel&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_courriel[" . $employes_employes_key  . "]\" id=\"employes_courriel[" . $employes_employes_key . "]\" value=\"" . $employes_courriel . "\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Pr&eacute;sent lors de l'&eacute;valuation&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"employes_present_evaluation[" . $employes_employes_key . "]\" id=\"employes_present_evaluation_oui[" . $employes_employes_key . "]\" value=\"1\" " . $employes_present_evaluation_oui_checked . " /><span class=\"radio_label\">Oui</span>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"employes_present_evaluation[" . $employes_employes_key . "]\" id=\"employes_present_evaluation_non[" . $employes_employes_key . "]\" value=\"0\" " . $employes_present_evaluation_non_checked . " /><span class=\"radio_label\">Non</span>\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Raison de l'absence, s'il y a lieu&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_raison_absence[" . $employes_employes_key . "]\" id=\"employes_raison_absence[" . $employes_employes_key . "]\" value=\"" . $employes_raison_absence . "\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"		</tbody>\n".
								"	</table>\n".
								"</div>\n";
						}
					}
				}

				// Affiche et remplit le formulaire pour l'employé qui vient d'être ajouté
				if ($parameters['employes_ajouter'] == "1" && $parameters['employes_employes_key_add'] != "" && $parameters['employes_employes_key_add'] != "-1" && $parameters['employes_nom_add'] != "" && $parameters['employes_prenom_add'] != "") {

					$employes_employes_key = $parameters['employes_employes_key_add'];
					$employes_nom = $parameters['employes_nom_add'];
					$employes_prenom = $parameters['employes_prenom_add'];
					$employes_courriel = $parameters['employes_courriel_add'];
					$employes_raison_absence = $parameters['employes_raison_absence_add'];

					$employes_fonctions_key = array();
					if (is_array($parameters['employes_fonctions_key_add']) && count($parameters['employes_fonctions_key_add']) > 0) {
						foreach ($parameters['employes_fonctions_key_add'] as $nb => $fonctions_key) {
							$employes_fonctions_key[$fonctions_key] = 1;
						}
					}

					if ($parameters['employes_present_evaluation_add'] == "1") {
						$employes_present_evaluation_oui_checked = "checked=\"checked\"";
						$employes_present_evaluation_non_checked = "";
					} else {
						$employes_present_evaluation_non_checked = "checked=\"checked\"";
						$employes_present_evaluation_oui_checked = "";
					}

					$html .=				
						"<div id=\"employes_div[" . $employes_employes_key . "]\" class=\"employes_entry\">\n".
						"	<table id=\"employes_table[" . $employes_employes_key . "]\" style=\"margin-top: 20px;\">\n".
						"		<tbody>\n".
						"			<tr>\n".
						"				<td width=\"200px\" rowspan=\"6\" style=\"text-align: center;\">" .
						"					<input type=\"hidden\" name=\"employes_employes_key[" . $employes_employes_key . "]\" id=\"employes_employes_key[" . $employes_employes_key . "]\" value=\"" . $employes_employes_key . "\" />\n".
						"					<input type=\"hidden\" name=\"employes_enlever[" . $employes_employes_key . "]\" id=\"employes_enlever[" . $employes_employes_key . "]\" value=\"\" />\n".						
						"					<a href=\"javascript: employes_enlever('" . $employes_employes_key . "');\">Enlever</a><br />\n".
						"				</td>\n".
						"				<th>\n".
						"					Fonction(s) de l'employ&eacute;&nbsp;:\n".
						"				</th>\n".
						"				<td width=\"175px\">\n".
						"					<select id=\"employes_fonctions_key[" . $employes_employes_key . "]\" name=\"employes_fonctions_key[" . $employes_employes_key . "][]\" multiple=\"multiple\" size=\"5\" style=\"width: 100%;\">\n";

					$fonctions = get_fonctions();

					if (is_array($fonctions) && count($fonctions) > 0) {
						foreach ($fonctions as $fonctions_key => $fonctions_nom) {
							if ($employes_fonctions_key[$fonctions_key] == 1) {
								$html .=
									"<option selectname=\"employes_fonctions_key[" . $employes_employes_key . "][]\" value=\"" . $fonctions_key . "\" selected=\"selected\">" . $fonctions_nom . "</option>\n";
							} else {
								$html .=
									"<option selectname=\"employes_fonctions_key[" . $employes_employes_key . "][]\" value=\"" . $fonctions_key . "\">" . $fonctions_nom . "</option>\n";
							}
						}
					}

					$html .=
						"					</select>\n".
						"				</td>\n".
						"			</tr>\n".
						"			<tr>\n".
						"				<th>\n".
						"					Nom de l'employ&eacute;&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_nom[" . $employes_employes_key . "]\" id=\"employes_nom[" . $employes_employes_key . "]\" value=\"" . $employes_nom . "\" readonly=\"readonly\" />\n".
						"				</td>\n".
						"			</tr>\n".
						"			<tr>\n".
						"				<th>\n".
						"					Pr&eacute;nom de l'employ&eacute;&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_prenom[" . $employes_employes_key . "]\" id=\"employes_prenom[" . $employes_employes_key . "]\" value=\"" . $employes_prenom . "\" readonly=\"readonly\" />\n".
						"				</td>\n".
						"			</tr>\n".
						"			<tr>\n".
						"				<th>\n".
						"					Adresse courriel&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_courriel[" . $employes_employes_key  . "]\" id=\"employes_courriel[" . $employes_employes_key . "]\" value=\"" . $employes_courriel . "\" />\n".
						"				</td>\n".
						"			</tr>\n".
						"			<tr>\n".
						"				<th>\n".
						"					Pr&eacute;sent lors de l'&eacute;valuation&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<input type=\"radio\" class=\"radio\" name=\"employes_present_evaluation[" . $employes_employes_key . "]\" id=\"employes_present_evaluation_oui[" . $employes_employes_key . "]\" value=\"1\" " . $employes_present_evaluation_oui_checked . " /><span class=\"radio_label\">Oui</span>\n".
						"					<input type=\"radio\" class=\"radio\" name=\"employes_present_evaluation[" . $employes_employes_key . "]\" id=\"employes_present_evaluation_non[" . $employes_employes_key . "]\" value=\"0\" " . $employes_present_evaluation_non_checked . " /><span class=\"radio_label\">Non</span>\n".
						"				</td>\n".
						"			</tr>\n".
						"			<tr>\n".
						"				<th>\n".
						"					Raison de l'absence, s'il y a lieu&nbsp;:\n".
						"				</th>\n".
						"				<td>\n".
						"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_raison_absence[" . $employes_employes_key . "]\" id=\"employes_raison_absence[" . $employes_employes_key . "]\" value=\"" . $employes_raison_absence . "\" />\n".
						"				</td>\n".
						"			</tr>\n".
						"		</tbody>\n".
						"	</table>\n".
						"</div>\n";
				}

				// Affiche le formulaire d'ajout
				$html .=				
					"<div id=\"employes_div_add\" class=\"employes_entry\">\n".
					"	<table id=\"employes_table_add\" style=\"margin-top: 20px;\">\n".
					"		<tbody>\n".
					"			<tr>\n".
					"				<td width=\"200px\" rowspan=\"6\" style=\"text-align: center;\">\n".
					"					<input type=\"hidden\" name=\"employes_ajouter\" id=\"employes_ajouter\" value=\"\" />\n".
					"					<input type=\"hidden\" name=\"employes_employes_key_add\" id=\"employes_employes_key_add\" value=\"\" />\n".
					"					<span id=\"already_added_warning\" style=\"display: none;\">" . _error("Cet employ&eacute; est d&eacute;j&agrave; ajout&eacute;") . "</span><br />\n".
					"					<select name=\"employes_employes_key_fromSelect\" id=\"employes_employes_key_fromSelect\" style=\"width: 175px;\" onchange=\"employes_onSelect(this)\" />\n".
					"						<option class=\"default\" value=\"-1\">- Choisir un employ&eacute; -</option>\n".
					"						<option value=\"-2\">(Un nouvel employ&eacute;)</option>\n";					

				if (is_array($employes) && count($employes) > 0) {
					foreach ($employes as $employe) {
						$html .= "<option value=\"" . $employe['employes_key'] . "\">" . $employe['fullname'] . "</option>\n";
					}
				}

				$html .= 
					"					</select><br />\n".
					"					<br />\n".
					"					<a href=\"javascript: employes_ajouter();\">ajouter</a>\n".
					"				</td>\n".
					"				<th>\n".
					"					Fonction(s) de l'employ&eacute;&nbsp;:\n".
					"				</th>\n".
					"				<td width=\"175px\">\n".
					"					<select id=\"employes_fonctions_key_add\" name=\"employes_fonctions_key_add[]\" multiple=\"multiple\" size=\"5\" style=\"width: 100%;\" disabled=\"disabled\">\n".
					"						" . employes_fonctions_selectOptions(get_fonctions(), "employes_fonctions_key_add[]") . "\n".
					"					</select>\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>\n".
					"					Nom de l'employ&eacute;&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_nom_add\" id=\"employes_nom_add\" value=\"\" disabled=\"disabled\" readonly=\"readonly\" />\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>\n".
					"					Pr&eacute;nom de l'employ&eacute;&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<input class=\"readonly\" type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_prenom_add\" id=\"employes_prenom_add\" value=\"\" disabled=\"disabled\" readonly=\"readonly\" />\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>\n".
					"					Adresse courriel&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_courriel_add\" id=\"employes_courriel_add\" value=\"\" disabled=\"disabled\" />\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>\n".
					"					Pr&eacute;sent lors de l'&eacute;valuation&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<input type=\"radio\" class=\"radio\" name=\"employes_present_evaluation_add\" id=\"employes_present_evaluation_oui_add\" value=\"1\" disabled=\"disabled\" /><span class=\"radio_label\">Oui</span>\n".
					"					<input type=\"radio\" class=\"radio\" name=\"employes_present_evaluation_add\" id=\"employes_present_evaluation_non_add\" value=\"0\" disabled=\"disabled\" /><span class=\"radio_label\">Non</span>\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>\n".
					"					Raison de l'absence, s'il y a lieu&nbsp;:\n".
					"				</th>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_raison_absence_add\" id=\"employes_raison_absence_add\" value=\"\" disabled=\"disabled\" />\n".
					"				</td>\n".
					"			</tr>\n".
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";

				$html .=
					"<span class=\"hint\">* Les changements apport&eacute;es ne modifieront pas les valeurs originales des registres du r&eacute;seau.</span>\n";
				break;

			case "read":
				// Affiche et remplit le formulaire pour chaque employé précédement ajouté
				if (is_array($parameters['employes_employes_key']) && count($parameters['employes_employes_key']) > 0) {
					ksort($parameters['employes_employes_key']);
					foreach ($parameters['employes_employes_key'] as $employes_key) {
						if ($employes_key != "" && $employes_key != "-1") {

							$employes_employes_key = $employes_key;
							$employes_nom = $parameters['employes_nom'][$employes_key];
							$employes_prenom = $parameters['employes_prenom'][$employes_key];
							$employes_courriel = $parameters['employes_courriel'][$employes_key];
							$employes_raison_absence = $parameters['employes_raison_absence'][$employes_key];

							$employes_fonctions_key = array();
							if (is_array($parameters['employes_fonctions_key'][$employes_key]) && count($parameters['employes_fonctions_key'][$employes_key]) > 0) {
								foreach ($parameters['employes_fonctions_key'][$employes_key] as $nb => $fonctions_key) {
									$employes_fonctions_key[$fonctions_key] = 1;
								}
							}

							if ($parameters['employes_present_evaluation'][$employes_key] == "1") {
								$employes_present_evaluation_oui_checked = "checked=\"checked\"";
								$employes_present_evaluation_non_checked = "";
							} else {
								$employes_present_evaluation_non_checked = "checked=\"checked\"";
								$employes_present_evaluation_oui_checked = "";
							} 			

							$html .=				
								"<div id=\"notaires_div[" . $employes_employes_key . "]\" class=\"employes_entry\">\n".
								"	<table id=\"employes_table[" . $employes_employes_key . "]\" style=\"margin-top: 20px;\">\n".
								"		<tbody>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Fonction(s) de l'employ&eacute;&nbsp;:\n".
								"				</th>\n".
								"				<td width=\"175px\">\n".
								"					<select id=\"employes_fonctions_key[" . $employes_key . "]\" name=\"employes_fonctions_key[" . $employes_employes_key . "][]\" multiple=\"multiple\" size=\"5\" style=\"width: 100%;\">\n";

							$fonctions = get_fonctions();

							if (is_array($fonctions) && count($fonctions) > 0) {
								foreach ($fonctions as $fonctions_key => $fonctions_nom) {
									if ($employes_fonctions_key[$fonctions_key] == 1) {
										$html .=
											"<option selectname=\"employes_fonctions_key[" . $employes_employes_key . "][]\" value=\"" . $fonctions_key . "\" selected=\"selected\">" . $fonctions_nom . "</option>\n";
									} else {
										$html .=
											"<option selectname=\"employes_fonctions_key[" . $employes_employes_key . "][]\" value=\"" . $fonctions_key . "\">" . $fonctions_nom . "</option>\n";
									}
								}
							}

							$html .=
								"					</select>\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Nom de l'employ&eacute;&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_nom[" . $employes_employes_key . "]\" id=\"employes_nom[" . $employes_employes_key . "]\" value=\"" . $employes_nom . "\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Pr&eacute;nom de l'employ&eacute;&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_prenom[" . $employes_employes_key . "]\" id=\"employes_prenom[" . $employes_employes_key . "]\" value=\"" . $employes_prenom . "\" readonly=\"readonly\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Adresse courriel&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_courriel[" . $employes_employes_key  . "]\" id=\"employes_courriel[" . $employes_employes_key . "]\" value=\"" . $employes_courriel . "\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Pr&eacute;sent lors de l'&eacute;valuation&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"employes_present_evaluation[" . $employes_employes_key . "]\" id=\"employes_present_evaluation_oui[" . $employes_employes_key . "]\" value=\"1\" " . $employes_present_evaluation_oui_checked . " /><span class=\"radio_label\">Oui</span>\n".
								"					<input type=\"radio\" class=\"radio\" name=\"employes_present_evaluation[" . $employes_employes_key . "]\" id=\"employes_present_evaluation_non[" . $employes_employes_key . "]\" value=\"0\" " . $employes_present_evaluation_non_checked . " /><span class=\"radio_label\">Non</span>\n".
								"				</td>\n".
								"			</tr>\n".
								"			<tr>\n".
								"				<th>\n".
								"					Raison de l'absence, s'il y a lieu&nbsp;:\n".
								"				</th>\n".
								"				<td>\n".
								"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"employes_raison_absence[" . $employes_employes_key . "]\" id=\"employes_raison_absence[" . $employes_employes_key . "]\" value=\"" . $employes_raison_absence . "\" />\n".
								"				</td>\n".
								"			</tr>\n".
								"		</tbody>\n".
								"	</table>\n".
								"</div>\n";
						}
					}
				}
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						// Affiche et remplit le formulaire pour chaque employé précédement ajouté
						if (is_array($compare_parameters['employes_employes_key']) && count($compare_parameters['employes_employes_key']) > 0) {
							ksort($compare_parameters['employes_employes_key']);
							foreach ($compare_parameters['employes_employes_key'] as $employes_key) {
								if ($employes_key != "" && $employes_key != "-1") {
		
									$employes_employes_key = $employes_key;
									$employes_nom = $compare_parameters['employes_nom'][$employes_key];
									$employes_prenom = $compare_parameters['employes_prenom'][$employes_key];
									$employes_courriel = $compare_parameters['employes_courriel'][$employes_key];
									$employes_raison_absence = $compare_parameters['employes_raison_absence'][$employes_key];
		
									$employes_fonctions_key = array();
									if (is_array($compare_parameters['employes_fonctions_key'][$employes_key]) && count($compare_parameters['employes_fonctions_key'][$employes_key]) > 0) {
										foreach ($compare_parameters['employes_fonctions_key'][$employes_key] as $i => $fonctions_key) {
											$employes_fonctions_key[$fonctions_key] = 1;
										}
									}
		
									if ($compare_parameters['employes_present_evaluation'][$employes_key] == "1") {
										$employes_present_evaluation_oui_checked = "checked=\"checked\"";
										$employes_present_evaluation_non_checked = "";
									} else {
										$employes_present_evaluation_non_checked = "checked=\"checked\"";
										$employes_present_evaluation_oui_checked = "";
									} 			
		
									$html .=				
										"<div class=\"answer_compare_" . $compare_color . "\">\n".
										"<div class=\"employes_entry\">\n".
										"	<table style=\"margin-top: 20px;\">\n".
										"		<tbody>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Fonction(s) de l'employ&eacute;&nbsp;:\n".
										"				</th>\n".
										"				<td width=\"175px\">\n".
										"					<select name=\"" . $compare_sid . "_employes_fonctions_key[" . $employes_employes_key . "][]\" multiple=\"multiple\" size=\"5\" style=\"width: 100%;\">\n";
		
									$fonctions = get_fonctions();
		
									if (is_array($fonctions) && count($fonctions) > 0) {
										foreach ($fonctions as $fonctions_key => $fonctions_nom) {
											if ($employes_fonctions_key[$fonctions_key] == 1) {
												$html .=
													"<option selectname=\"" . $compare_sid . "_employes_fonctions_key[" . $employes_employes_key . "][]\" value=\"" . $fonctions_key . "\" selected=\"selected\">" . $fonctions_nom . "</option>\n";
											} else {
												$html .=
													"<option selectname=\"" . $compare_sid . "_employes_fonctions_key[" . $employes_employes_key . "][]\" value=\"" . $fonctions_key . "\">" . $fonctions_nom . "</option>\n";
											}
										}
									}
		
									$html .=
										"					</select>\n".
										"				</td>\n".
										"			</tr>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Nom de l'employ&eacute;&nbsp;:\n".
										"				</th>\n".
										"				<td>\n".
										"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"" . $compare_sid . "_employes_nom[" . $employes_employes_key . "]\" value=\"" . $employes_nom . "\" readonly=\"readonly\" />\n".
										"				</td>\n".
										"			</tr>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Pr&eacute;nom de l'employ&eacute;&nbsp;:\n".
										"				</th>\n".
										"				<td>\n".
										"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"" . $compare_sid . "_employes_prenom[" . $employes_employes_key . "]\" value=\"" . $employes_prenom . "\" readonly=\"readonly\" />\n".
										"				</td>\n".
										"			</tr>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Adresse courriel&nbsp;:\n".
										"				</th>\n".
										"				<td>\n".
										"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"" . $compare_sid . "_employes_courriel[" . $employes_employes_key  . "]\" value=\"" . $employes_courriel . "\" />\n".
										"				</td>\n".
										"			</tr>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Pr&eacute;sent lors de l'&eacute;valuation&nbsp;:\n".
										"				</th>\n".
										"				<td>\n".
										"					<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_employes_present_evaluation[" . $employes_employes_key . "]\" value=\"1\" " . $employes_present_evaluation_oui_checked . " /><span class=\"radio_label\">Oui</span>\n".
										"					<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_employes_present_evaluation[" . $employes_employes_key . "]\" value=\"0\" " . $employes_present_evaluation_non_checked . " /><span class=\"radio_label\">Non</span>\n".
										"				</td>\n".
										"			</tr>\n".
										"			<tr>\n".
										"				<th>\n".
										"					Raison de l'absence, s'il y a lieu&nbsp;:\n".
										"				</th>\n".
										"				<td>\n".
										"					<input type=\"text\" class=\"text\" style=\"width: 165px;\" name=\"" . $compare_sid . "_employes_raison_absence[" . $employes_employes_key . "]\" value=\"" . $employes_raison_absence . "\" />\n".
										"				</td>\n".
										"			</tr>\n".
										"		</tbody>\n".
										"	</table>\n".
										"</div>\n".
										"</div>\n";
								}
							}
						}
					}
				}
				break;
		}
	return $html;
	}

	function get_fonctions() {
		global $DB;
		$html = "";
		$DB->query(
			"SELECT `nom`, `key` ".
			"FROM `fonctions` ".
			"ORDER BY `nom` ASC;"
		);
		$fonctions = array();
		while ($DB->next_record()) {
			$fonctions[$DB->getField("key")] = $DB->getField("nom");
		}
		return $fonctions;
	}

	function employes_fonctions_selectOptions($fonctions, $selectname) {
		$html = "";
		if (is_array($fonctions) && count($fonctions) > 0) {
			foreach ($fonctions as $key => $nom) {
				$html .= "<option selectname=\"" . $selectname . "\" value=\"" . $key . "\">" . $nom . "</option>\n";
			}
		}
		return $html;
	}

	function s3_notes_personnel_etude($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"10\" cols=\"40\" id=\"s3_notes_personnel_etude\" name=\"s3_notes_personnel_etude\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"10\" cols=\"40\" id=\"s3_notes_personnel_etude\" name=\"s3_notes_personnel_etude\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"10\" cols=\"40\" name=\"" . $compare_sid . "_s3_notes_personnel_etude\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}
				
		return $html;
	}


?>