<?PHP
	global $DB;

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL;

		$_FORM =
			form_header($action).
			"<div id=\"direction_de_travail_convocation\">\n".

			"		<a name=\"date\"></a><label for=\"date\">Date de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_debut($action) . "\n".
			"		</div>\n".

			"		<a name=\"heure_debut\"></a><label for=\"heure_debut\">Heure d&eacute;but&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . heure_debut($action) . "\n".
			"		</div>\n".

			"		<a name=\"heure_fin\"></a><label for=\"heure_fin\">Heure fin&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . heure_fin($action) . "\n".
			"		</div>\n".

			"		<a name=\"date_inscription\"></a><label for=\"date_inscription\">Date limite d'inscription&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_inscription($action) . "\n".
			"		</div>\n".

			"		<a name=\"lieu\"></a><label for=\"lieu\">Lieu&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . lieu($action) . "\n".
			"		</div>\n".

			"		<a name=\"directions_de_travail_key\"></a><label for=\"directions_de_travail_key\">Direction de travail : </label>\n".
			"		<div class=\"field\">\n".
			"			" . direction_travail($action) . "\n".
			"		</div>\n".

			"		<a name=\"note\"></a><label for=\"note\">Note&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . note($action) . "\n".
			"		</div>\n".

			"		<label for=\"description\">Description de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . description($action) . "\n".
			"		</div>\n".

			"		<label for=\"conferencier\">Conf&eacute;rencier&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . conferencier($action) . "\n".
			"		</div>\n".

			"		<label for=\"sujet\">Sujet&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . sujet($action) . "\n".
			"		</div>\n".

			"		<div class=\"entry_long\">\n".
			"			" . fichier($action, 1) . "\n".
			"		</div>\n".

			"		<div class=\"entry_long\">\n".
			"			" . fichier($action, 2) . "\n".
			"		</div>\n".

			"		<div class=\"entry_long\">\n".
			"			" . fichier($action, 3) . "\n".
			"		</div>\n".

			"		<div class=\"entry_long\">\n".
			"			" . fichier($action, 4) . "\n".
			"		</div>\n".

			"		<div class=\"entry_long\">\n".
			"			" . fichier($action, 5) . "\n".
			"		</div>\n".

			"		<label for=\"email_confirmation\">Courriel de confirmation&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . email_confirmation($action) . "\n".
			"		</div>\n".

			"		<label for=\"actif\">Actif</label>\n".
			"		<div class=\"field\">\n".
			"			" . actif($action) . "\n".
			"		</div>\n".

			form_footer($action);

		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html =
			"<script type=\"text/javascript\" src=\"/skin/js/tinymce/tinymce.min.js\"></script>\n".
			"<script type=\"text/javascript\">\n".
			"	tinymce.init({\n".
    		"		selector: \"textarea\",\n".
    		"		height: 600\n".
 			"	});\n".
			"</script>\n";

		switch ($action) {
			case "add":
				$html .=
					"<form name=\"evenements\" action=\"directions_de_travail.php\" method=\"post\" enctype=\"multipart/form-data\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n".
					"	<input type=\"hidden\" name=\"id\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "modify":
				$html .=
					"<form name=\"evenements\" action=\"directions_de_travail.php\" method=\"post\" enctype=\"multipart/form-data\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"id\" value=\"\" />\n".
					"	<input type=\"hidden\" name=\"key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "read":
				$html .=
					"	<div class=\"readform\">\n".
					"		<a href=\"/evenements/alerte.php?table=direction_de_travail&id=" . $_POST["id"] . "\" target=\"_blank\"><strong>Alerte courriel</strong></a><br />\n".
					"		<a href=\"/evenements/inscription.php?table=direction_de_travail&id=" . $_POST["id"] . "\" target=\"_blank\"><strong>Inscriptions</strong></a><br />\n".
					"		<a href=\"/evenements/inscription.php?table=direction_de_travail&id=" . $_POST["id"] . "&excel=1\" target=\"_blank\"><strong>Inscriptions Excel</strong></a><br /><br />\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html =
					"	<br /><br />\n".
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "modify":
				$html =
					"	<br /><br />\n".
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "read":
				$html =
					"	</div>\n";
				break;
		}
		return $html;
	}


	function direction_travail($action) {
		global $DB;
		$html = "";

		$DB->query("SELECT * FROM directions_de_travail");

		$select_dd =
			"<select name=\"directions_de_travail_key\">\n".
			"	<option id=\"directions_de_travail_key\" value=\"\"></option>\n";

		while($DB->next_record()) {
			$select_dd .=
				"	<option id=\"directions_de_travail_key\" value=\"" . $DB->getField("key") ."\">" . $DB->getField("nom") . "</option>\n";
		}

		$select_dd .=
			"</select>\n";

		switch ($action) {
			case "add":
				$html = $select_dd .
						"<br/><br/>";
				break;
			case "modify":
				$html = $select_dd .
						"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_directions_de_travail_key /HIDDEN_directions_de_travail_key---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function actif($action){
		$html="";
		switch ($action) {
			case "add":
				$html =
					"<select name=\"actif\">\n".
					"	<option id=\"actif\" value=\"\"></option>\n".
					"	<option id=\"actif\" value=\"1\">Oui</option>\n".
					"	<option id=\"actif\" value=\"0\">Non</option>\n".
					"</select>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html =
					"<select name=\"actif\">\n".
					"	<option id=\"actif\" value=\"\"></option>\n".
					"	<option id=\"actif\" value=\"1\">Oui</option>\n".
					"	<option id=\"actif\" value=\"0\">Non</option>\n".
					"</select>\n".
					"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_actif /HIDDEN_actif---></span><br/><br/>\n";
				break;
		}
		return $html;

	}

	function date_debut($action) {

		$html = "";
		switch ($action) {
			case "add":
				$html = "<input type=\"text\" name=\"date\" id=\"date\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date\" id=\"date\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_date /HIDDEN_date---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function heure_debut($action) {

		$html = "";
		switch ($action) {
			case "add":
				$html = "<input type=\"text\" name=\"heure_debut\" id=\"heure_debut\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"heure_debut\" id=\"heure_debut\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_heure_debut /HIDDEN_heure_debut---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function heure_fin($action) {

		$html = "";
		switch ($action) {
			case "add":
				$html = "<input type=\"text\" name=\"heure_fin\" id=\"heure_fin\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"heure_fin\" id=\"heure_fin\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_heure_fin /HIDDEN_heure_fin---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function date_inscription($action) {

		$html = "";
		switch ($action) {
			case "add":
				$html = "<input type=\"text\" name=\"date_inscription\" id=\"date_inscription\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date_inscription\" id=\"date_inscription\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_date_inscription /HIDDEN_date_inscription---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function conferencier($action) {

		$html = "";
		switch ($action) {
			case "add":
				$html = "<input type=\"text\" name=\"conferencier\" id=\"conferencier\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"conferencier\" id=\"conferencier\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_conferencier /HIDDEN_conferencier---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function sujet($action) {

		$html = "";
		switch ($action) {
			case "add":
				$html = "<input type=\"text\" name=\"sujet\" id=\"sujet\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"sujet\" id=\"sujet\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_sujet /HIDDEN_sujet---></span><br/><br/>\n";
				break;
		}
		return $html;
	}



	function lieu($action) {

		$html = "";
		switch ($action) {
			case "add":
				$html = "<input type=\"text\" name=\"lieu\" id=\"lieu\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"lieu\" id=\"lieu\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_lieu /HIDDEN_lieu---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function note($action) {

		$html = "";
		switch ($action) {
			case "add":
				$html = "<input type=\"text\" name=\"note\" id=\"note\" value=\"Nous vous prions de bien vouloir confirmer votre présence ou absence aux fins d’organisation.\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"note\" id=\"note\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_note /HIDDEN_note---></span><br/><br/>\n";
				break;
		}
		return $html;
	}


	function description($action) {

		$html = "";
		switch ($action) {
			case "add":
				$html =
					"<textarea rows=\"8\" cols=\"40\" name=\"description\">\n".
					"	<h1 style=\"text-align: center; color: red;\">AVIS DE CONVOCATION</h1>\n".
					"	<h2 style=\"text-align: center;\">À l’attention de tous les membres du comité \"[COMITE]\"</h2>\n".
					"	<p style=\"text-align: center;\"><strong>Réunion, le [DATE]</strong></p>\n".
					"	<p class=\"note\" style=\"text-align: center; color: red;\">[NOTE]</p>\n".
					"	<div style=\"text-align: center;\">[INSCRIPTIONS]</div><br />\n".
					"	<p>Vous êtes priés d’assister à une réunion du comité \"[COMITE]\" qui aura lieu le [DATE] de [HEURE_DEBUT]h à [HEURE_FIN]h.</p>\n".
					"	<p>La réunion se tiendra à [LIEU].</p>\n".
					"	<p>(À SUPPRIMER SI NON NÉCESSAIRE) <em>Nous vous confirmerons l’emplacement final dans les meilleurs délais.</em></p>\n".
					"	<p>Suite à la réunion de l'avant-midi, suivra une formation de [CONFERENCIER] sur [SUJET].</p>\n".
					"	<p>Merci de prendre bonne note de cette information et, pour des raisons de logistique, de nous confirmer le moment de votre présence (am, pm ou les deux), et si vous serez présent physiquement ou via «Go To Meeting», avant le [DATE_INSCRIPTION] à 15h00. Si vous comptez être absent, merci de nous en aviser également.</p>\n".
					"	<p>Si vous souhaitez vous joindre à nous à distance via le logiciel «Go To Meeting», nous vous ferons parvenir un courriel d’informations complémentaires afférentes à votre connexion quelques jours avant la rencontre.</p>\n".
					"	<p>Veuillez trouver ci-bas les liens pour ouvrir le procès-verbal de la dernière rencontre de cette direction ainsi que la dernière mise à jour de la liste des participants.</p>\n".
					"	<p>OU</p>\n".
					"	<p>L'ordre du jour et le procès verbal suivront sous peu.</p>\n".
					"	<p>Merci de votre précieuse collaboration et de votre participation.</p>".

					"	[FICHIERS]\n".
					"</textarea>\n".
					"<br/><br/>";

				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"description\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_description /HIDDEN_description---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function email_confirmation($action) {

		$html = "";
		switch ($action) {
			case "add":
				$html =
					"<textarea rows=\"8\" cols=\"40\" name=\"email_confirmation\">\n".
					"	<p>Bonjour,</p>\n".
					"	<p>La présente accuse réception de votre [STATUT] à la réunion du comité \"[COMITE]\" et/ou à la formation qui aura lieu le [DATE] à [LIEU] de [HEURE_DEBUT] à [HEURE_FIN].</p>\n".
					"	<p>Si jamais vous souhaitiez changer votre statut, merci de nous en avisezr le plus tôt possible ou de le modifier directement en cliquant sur le lien dans l'avis de convocation.</p>\n".
					"	<p>En toute collaboration,</p>\n".
					"</textarea>\n".
					"<br/><br/>";

				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"email_confirmation\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html =
					"<span class=\"answer\"><!---HIDDEN_email_confirmation /HIDDEN_email_confirmation---></span><br/><br/>\n";
				break;
		}
		return $html;
	}


	function fichier($action, $index) {
		global $DB, $BASEURL;
		$id = getorpost('id');
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"fichier" . $index . "_up\">Fichier #" . $index . "&nbsp;:</label>\n".
					"<input type=\"file\" class=\"file\" name=\"fichier" . $index . "_up\" id=\"fichier" . $index . "_up\" /><br />\n";
				break;

			case "modify":
				$fichier = array();
				$fichier[$index] = "";
				$DB->query(
					"SELECT `fichier" . $index . "` ".
					"FROM `directions_de_travail_event` ".
					"WHERE `id` = '" . addslashes($id) . "';"
				);
				while ($DB->next_record()) {
					$fichier[$index] = $DB->getField("fichier" . $index);
				}

				$html .=
					"<label for=\"fichier" . $index . "_up\">Fichier #" . $index . "&nbsp;:</label>\n".
					"<input type=\"hidden\" name=\"fichier" . $index . "\" id=\"fichier" . $index . "\" value=\"\" />\n".
					"<input type=\"file\" class=\"file\" name=\"fichier" . $index . "_up\" id=\"fichier" . $index . "_up\" /><br />\n";

				if ($fichier[$index] != "") {
					$html .=
						"<br />\n".
						"<a href=\"" . $BASEURL . "docs/evenements/" . $fichier[$index] . "\" target=\"_blank\">Fichier #" . $index . "</a><br />\n".
						"<input type=\"checkbox\" class=\"checkbox\" name=\"fichier" . $index . "_del\" id=\"fichier" . $index . "_del\" value=\"1\" /><span class=\"checkbox_label\">Supprimer le fichier&nbsp;?</span>\n";
				} else {
					$html .=
						"<br />\n".
						"Aucun fichier pr&eacute;sent.\n";
				}
				break;

			case "read":
				$html .=
					"<label>Fichier #" . $index . "&nbsp;:</label>\n";
				$DB->query(
					"SELECT `fichier" . $index . "` ".
					"FROM `directions_de_travail_event` ".
					"WHERE `id` = '" . addslashes($id) . "';"
				);
				while ($DB->next_record()) {
					$fichier[$index] = $DB->getField("fichier" . $index);
				}
				if ($fichier[$index] != "") {
					$html .=
						"<a href=\"" . $BASEURL . "docs/evenements/<!---HIDDEN_fichier" . $index . " /HIDDEN_fichier" . $index . "--->\" target=\"_blank\">Fichier #" . $index . "</a>\n";
				} else {
					$html .=
						"<span class=\"answer\">(aucun)</span>\n";
				}
				break;
		}
		return $html;
	}
