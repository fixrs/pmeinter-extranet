<?PHP


    // LE FORMULAIRE //
    function getForm($action) {
        global $DB, $BASEURL;

        if ($action == "add") {
            $class = "";
            if (isset($_POST) && count($_POST) > 0) {
                $class = ' class="error" ';
            }

            $_FORM =
                '<div id="add-form" ' . $class . '><a class="close"><span class="fa fa-close"></span></a>' .
                    form_header($action).
                '   <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="etude">' . etudes_key($action, $DB) . '</div>
                            <br /><div class="employes">' . employes_key($action, $DB) . '</div>
                            <div class="titre">' . titre($action) . '</div>
                            <div class="description">' . description($action) . '</div>
                            <div class="fichiers">
                                <script type="text/javascript" src="/extranet/bd/skin/js/dropzone.js"></script>
                                <input type="hidden" id="fichiers" name="fichiers" class="file_uploader fr" data-id="add-form" data-editable="1" data-qty="5" value="" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="secteurs">' . secteurs($action) . '</div>
                            <div class="deadline">' . deadline($action) . '</div>
                            <div class="budget">' . budget($action) . '</div>
                            <div class="suivi">' . suivi($action) . '</div>
                            <div class="partager_avec">' . partager_avec($action, $DB) . '</div>
                        </div>
                    </div>
                    ' . form_footer($action) . '
                </div>';

        } else {

            $class = "";
            if (count($_POST) > 0) {
                $class = ' class="error" ';
            }

            $_FORM =
                '<div id="edit-form" ' . $class . '>' .
                    form_header($action).
                '   <div class="row">
                        <div class="employes col-xs-6 col-sm-6">' . employes_key($action, $DB) . '</div>
                        <div class="col-xs-6 col-sm-6">' . etudes_key($action, $DB) . '</div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="titre">' . titre($action) . '</div>
                            <div class="description">' . description($action) . '</div>
                            <div class="fichiers">
                                <script type="text/javascript" src="/extranet/bd/skin/js/dropzone.js"></script>
                                <input type="hidden" id="fichiers" name="fichiers" class="file_uploader fr" data-id="edit-form" data-editable="1" data-qty="5" value="" />
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="secteurs">' . secteurs($action) . '</div>
                            <div class="deadline">' . deadline($action) . '</div>
                            <div class="budget">' . budget($action) . '</div>
                            <div class="suivi">' . suivi($action) . '</div>
                            <div class="partager_avec">' . partager_avec($action, $DB) . '</div>
                        </div>
                    </div>
                    ' . form_footer($action) . '
                </div>';

        }




        return $_FORM;
    }


    // LES SOUS-ROUTINES POUR LES INPUTS //

    function form_header($action) {
        $html = "";
        $form_class = "";
        if ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") { $form_class = ' class="limited" '; }

        switch ($action) {
            case "add":
                $html .=
                    "<form name=\"dossiers\" action=\"/extranet/mandats/\" method=\"post\" " . $form_class . ">\n".
                    "   <input type=\"hidden\" name=\"action\" value=\"add\" />\n";
                break;
            case "modify":
                $html .=
                    "<form name=\"dossiers\" action=\"/extranet/mandats/\" method=\"post\" " . $form_class . ">\n".
                    "   <input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
                    "   <input type=\"hidden\" name=\"key\" value=\"\" />\n".
                    "   <input type=\"hidden\" name=\"etudes_key\" value=\"\" />\n".
                    "   <input type=\"hidden\" name=\"employes_key\" value=\"\" />\n";
                break;
            case "read":
                $html .=
                    "<div class=\"readform\">\n";
                break;
        }
        return $html;
    }

    function form_footer($action) {
        $html = ""; //"<script src=\"/extranet/bd/skin/js/jquery.js\"></script>";

        switch ($action) {
            case "add":
                $html .=
                    "   <br />\n".
                    "   <input type=\"submit\" class=\"submit\" value=\"Publier\" />\n".
                    "</form>\n";
                break;
            case "modify":
                $html .=
                    "   <br />\n".
                    "   <input type=\"submit\" class=\"submit\" value=\"Publier\" /> <a class=\"cancel-link\">Annuler</a>\n".
                    "</form>\n";
                break;
            case "read":
                $html .=
                    "</div>\n";
                break;
        }
        return $html;
    }

    function etudes_key($action, $DB) {
        $html = "";
        switch ($action) {
            case "add":
                $html .=
                    "<label for=\"etudes_key\">Nom de l'&eacute;tude&nbsp;: </label>\n".
                    "<select id=\"etudes_key\" name=\"etudes_key\">\n";

                if ($_SESSION['user_type'] == 1) {
                    $html .= "<option class=\"default\" value=\"-1\"> - Choisir l'&eacute;tude - </option>\n";

                    $DB->query(
                        "SELECT `key`, `nom` ".
                        "FROM `etudes` ".
                        "WHERE `actif` = '1' ".
                        "ORDER BY `key` ASC"
                    );
                    while ($DB->next_record()) {
                        $key = $DB->getField("key");
                        $nom = $DB->getField("nom");
                        $html .=
                            "<option selectname=\"etudes_key\" value=\"" . $key . "\">" . stripslashes($nom) . "</option>\n";
                    }

                } elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
                    $DB->query(
                        "SELECT `key`, `nom` ".
                        "FROM `etudes` ".
                        "WHERE `key` = '" . $_SESSION['user_etudes_key'] . "' AND `actif` = '1' "
                    );
                    while ($DB->next_record()) {
                        $key = $DB->getField("key");
                        $nom = $DB->getField("nom");
                        $html .=
                            "<option selectname=\"etudes_key\" value=\"" . $key . "\">" . stripslashes($nom) . "</option>\n";
                    }
                }

                $html .=
                    "</select>\n";
                break;

            case "modify":
                $html .=
                    "<label for=\"etudes_key\">Nom de l'&eacute;tude mandataire&nbsp;: </label>\n".
                    "<select id=\"etudes_key\" name=\"etudes_key\">\n";

                if ($_SESSION['user_type'] == 1) {
                    $html .= "<option class=\"default\" value=\"-1\"> - Choisir l'&eacute;tude - </option>\n";

                    $DB->query(
                        "SELECT `key`, `nom` ".
                        "FROM `etudes` ".
                        "WHERE `actif` = '1' ".
                        "ORDER BY `key` ASC"
                    );
                    while ($DB->next_record()) {
                        $key = $DB->getField("key");
                        $nom = $DB->getField("nom");
                        $html .=
                            "<option selectname=\"etudes_key\" value=\"" . $key . "\">" . stripslashes($nom) . "</option>\n";
                    }

                } elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
                    $DB->query(
                        "SELECT `key`, `nom` ".
                        "FROM `etudes` ".
                        "WHERE `key` = '" . $_SESSION['user_etudes_key'] . "' AND `actif` = '1' "
                    );
                    while ($DB->next_record()) {
                        $key = $DB->getField("key");
                        $nom = $DB->getField("nom");
                        $html .=
                            "<option selectname=\"etudes_key\" value=\"" . $key . "\">" . stripslashes($nom) . "</option>\n";
                    }
                }

                $html .=
                    "</select>\n";

                break;

            case "read":
                $html .=
                    "<label>Nom de l'&eacute;tude&nbsp;: </label>\n".
                    "<span class=\"answer\"><!---HIDDEN_etude_nom /HIDDEN_etude_nom---></span>";
                break;
        }
        return $html;
    }

    function partager_avec($action, $DB) {
        $html = "";
        switch ($action) {
            case "add":
                $html .=
                    "<br clear=\"all\" /><br /><label for=\"partager_avec\">Publication du mandat auprès de&nbsp;:<br /><small>Ne sélectionnez aucune étude pour les alerter toutes.</small></label>\n".
                    "<input type=\"hidden\" name=\"partager_avec\" id=\"partager_avec\" value=\"\" /><div class=\"p_a\">\n";


                if ($_SESSION['user_type'] == 1) {

                    $DB->query(
                        "SELECT DISTINCT es.ville, em.key AS employes_key, em.prenom AS employes_prenom, em.nom AS employes_nom, em.courriel, em.associe AS associe, et.nom, et.key AS etude_key
                        FROM `employes` em INNER JOIN `employes_etudes_succursales` ees ON ees.employes_key = em.key
                        INNER JOIN `etudes_succursales` es ON es.key = ees.etudes_succursales_key
                        INNER JOIN `etudes` et ON et.key = es.etudes_key
                        LEFT JOIN employes_fonctions ef ON em.key = ef.employes_key AND ef.fonctions_key IN (15,8,12)
                        WHERE et.actif = '1'
                        AND es.actif = '1'
                        AND em.actif = '1'
                        ORDER BY et.nom ASC, em.nom ASC"
                    );

                    $last_etude_key = '';
                    $notaires = array();
                    while ($row = $DB->next_record()) {
                        $notaires[$row["etude_key"]]["nom"] = $row["nom"];
                        $notaires[$row["etude_key"]]["villes"][$row["ville"]] = 1;
                        $notaires[$row["etude_key"]]["notaires"]["u" . $row["employes_key"]] = $row;
                    }

                    foreach ($notaires as $ek => $et) {

                        $html .=
                            '<div><label for="e' . $ek . '"><input type="checkbox" id="e' . $ek . '" name="choix_etude" value="e' . $ek . '"> <span>' . stripslashes($et["nom"] . "<br />" . implode(", ", array_keys($et["villes"]))) . "</span><br clear=\"all\"></label>\n" .
                            '<div class="e' . $ek . '">';

                        foreach ($et["notaires"] as $i => $notaire) {
                            $html .= '<label for="u' . $notaire["employes_key"] . '"><input type="checkbox" id="u' . $notaire["employes_key"] . '" name="choix_etude" value="u' . $notaire["employes_key"] . '"> <span>' . stripslashes($notaire["employes_nom"] . ", " . $notaire["employes_prenom"]) . ($notaire["associe"] ? " - <em>Associé</em>" : "") . "</span></label>\n";
                        }

                        $html .= '</div></div>';

                    }

                } elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {

                    $DB->query(
                        "SELECT DISTINCT es.ville, em.key AS employes_key, em.prenom AS employes_prenom, em.nom AS employes_nom, em.courriel, em.associe AS associe, et.nom, et.key AS etude_key
                        FROM `employes` em INNER JOIN `employes_etudes_succursales` ees ON ees.employes_key = em.key
                        INNER JOIN `etudes_succursales` es ON es.key = ees.etudes_succursales_key
                        INNER JOIN `etudes` et ON et.key = es.etudes_key AND et.key != '" . $_SESSION['user_etudes_key'] . "'
                        LEFT JOIN employes_fonctions ef ON em.key = ef.employes_key AND ef.fonctions_key IN (15,8,12)
                        WHERE et.actif = '1'
                        AND es.actif = '1'
                        AND em.actif = '1'
                        ORDER BY et.nom ASC, em.nom ASC"
                    );

                    $last_etude_key = '';
                    $notaires = array();
                    while ($row = $DB->next_record()) {
                        $notaires[$row["etude_key"]]["nom"] = $row["nom"];
                        $notaires[$row["etude_key"]]["villes"][$row["ville"]] = 1;
                        $notaires[$row["etude_key"]]["notaires"]["u" . $row["employes_key"]] = $row;
                    }

                    foreach ($notaires as $ek => $et) {

                        $html .=
                            '<div><label for="e' . $ek . '"><input type="checkbox" id="e' . $ek . '" name="choix_etude" value="e' . $ek . '"> <span>' . stripslashes($et["nom"] . "<br />" . implode(", ", array_keys($et["villes"]))) . "</span><br clear=\"all\"></label>\n" .
                            '<div class="e' . $ek . '">';

                        foreach ($et["notaires"] as $i => $notaire) {
                            $html .= '<label for="u' . $notaire["employes_key"] . '"><input type="checkbox" id="u' . $notaire["employes_key"] . '" name="choix_etude" value="u' . $notaire["employes_key"] . '"> <span>' . stripslashes($notaire["employes_nom"] . ", " . $notaire["employes_prenom"]) . ($notaire["associe"] ? " - <em>Associé</em>" : "") . "</span></label>\n";
                        }

                        $html .= '</div></div>';

                    }


                }

                $html .= "</div>";

                break;

            case "modify":
                $html .=
                    "<label for=\"partager_avec\">Publication du mandat auprès de&nbsp;: </label>\n".
                    "<input type=\"hidden\" name=\"partager_avec\" id=\"partager_avec\" value=\"\" /><div class=\"p_a\">";
                    //"<select id=\"choix_etude\" name=\"choix_etude[]\" multiple=\"multiple\" style=\"height: 300px;\">\n";

                if ($_SESSION['user_type'] == 1) {
                    //$html .= "<option class=\"default\" value=\"-1\"> - Choisir l'&eacute;tude - </option>\n";

                    $DB->query(
                        "SELECT DISTINCT es.ville, em.key AS employes_key, em.prenom AS employes_prenom, em.nom AS employes_nom, em.courriel, em.associe AS associe, et.nom, et.key AS etude_key
                        FROM `employes` em INNER JOIN `employes_etudes_succursales` ees ON ees.employes_key = em.key
                        INNER JOIN `etudes_succursales` es ON es.key = ees.etudes_succursales_key
                        INNER JOIN `etudes` et ON et.key = es.etudes_key
                        LEFT JOIN employes_fonctions ef ON em.key = ef.employes_key AND ef.fonctions_key IN (15,8,12)
                        WHERE et.actif = '1'
                        AND es.actif = '1'
                        AND em.actif = '1'
                        ORDER BY et.nom ASC, em.nom ASC"
                    );

                    $last_etude_key = '';
                    $notaires = array();
                    while ($row = $DB->next_record()) {
                        $notaires[$row["etude_key"]]["nom"] = $row["nom"];
                        $notaires[$row["etude_key"]]["villes"][$row["ville"]] = 1;
                        $notaires[$row["etude_key"]]["notaires"]["u" . $row["employes_key"]] = $row;
                    }

                    foreach ($notaires as $ek => $et) {

                        $html .=
                            '<div><label for="e' . $ek . '"><input type="checkbox" id="e' . $ek . '" name="choix_etude" value="e' . $ek . '"> <span>' . stripslashes($et["nom"] . "<br />" . implode(", ", array_keys($et["villes"]))) . "</span><br clear=\"all\"></label>\n" .
                            '<div class="e' . $ek . '">';

                        foreach ($et["notaires"] as $i => $notaire) {
                            $html .= '<label for="u' . $notaire["employes_key"] . '"><input type="checkbox" id="u' . $notaire["employes_key"] . '" name="choix_etude" value="u' . $notaire["employes_key"] . '"> <span>' . stripslashes($notaire["employes_nom"] . ", " . $notaire["employes_prenom"]) . ($notaire["associe"] ? " - <em>Associé</em>" : "") . "</span></label>\n";
                        }

                        $html .= '</div></div>';

                    }

                } elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {

                    $DB->query(
                        "SELECT DISTINCT es.ville, em.key AS employes_key, em.prenom AS employes_prenom, em.nom AS employes_nom, em.courriel, em.associe AS associe, et.nom, et.key AS etude_key
                        FROM `employes` em INNER JOIN `employes_etudes_succursales` ees ON ees.employes_key = em.key
                        INNER JOIN `etudes_succursales` es ON es.key = ees.etudes_succursales_key
                        INNER JOIN `etudes` et ON et.key = es.etudes_key AND et.key != '" . $_SESSION['user_etudes_key'] . "'
                        LEFT JOIN employes_fonctions ef ON em.key = ef.employes_key AND ef.fonctions_key IN (15,8,12)
                        WHERE et.actif = '1'
                        AND es.actif = '1'
                        AND em.actif = '1'
                        ORDER BY et.nom ASC, em.nom ASC"
                    );

                    $last_etude_key = '';
                    $notaires = array();
                    while ($row = $DB->next_record()) {
                        $notaires[$row["etude_key"]]["nom"] = $row["nom"];
                        $notaires[$row["etude_key"]]["villes"][$row["ville"]] = 1;
                        $notaires[$row["etude_key"]]["notaires"]["u" . $row["employes_key"]] = $row;
                    }

                    foreach ($notaires as $ek => $et) {

                        $html .=
                            '<div><label for="e' . $ek . '"><input type="checkbox" id="e' . $ek . '" name="choix_etude" value="e' . $ek . '"> <span>' . stripslashes($et["nom"] . "<br />" . implode(", ", array_keys($et["villes"]))) . "</span><br clear=\"all\"></label>\n" .
                            '<div class="e' . $ek . '">';

                        foreach ($et["notaires"] as $i => $notaire) {
                            $html .= '<label for="u' . $notaire["employes_key"] . '"><input type="checkbox" id="u' . $notaire["employes_key"] . '" name="choix_etude" value="u' . $notaire["employes_key"] . '"> <span>' . stripslashes($notaire["employes_nom"] . ", " . $notaire["employes_prenom"]) . ($notaire["associe"] ? " - <em>Associé</em>" : "") . "</span></label>\n";
                        }

                        $html .= '</div></div>';

                    }


                }

                $html .= "</div>";

                break;

        }
        return $html;
    }

    function employes_key($action, $DB) {
        $html = "";
        switch ($action) {
            case "add":
                $html .=
                    "<label for=\"employes_key\">Nom du responsable&nbsp;: </label>\n".
                    "<select id=\"employes_key\" name=\"employes_key\">\n";

                if ($_SESSION['user_type'] == 1) {
                    $html .= "<option class=\"default\" value=\"-1\"> - Choisir le responsable - </option>\n";

                    $DB->query(
                        "SELECT DISTINCT em.key AS employes_key, em.prenom AS prenom, em.nom AS nom, em.associe AS associe, et.nom AS etude ".
                        "FROM `employes` AS em, `etudes` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
                        "WHERE em.key = ees.employes_key AND es.etudes_key = et.key AND ees.etudes_succursales_key = es.key AND et.actif = '1' AND es.actif = '1' AND em.actif = '1' ORDER BY et.nom ASC;"
                    );

                    $last_etude = "";
                    while ($DB->next_record()) {

                        if ($last_etude != $DB->getField("etude")) {

                            if ($last_etude == "") {
                                $html .= "<optgroup label=\"" . $DB->getField("etude") . "\">";
                            } else {
                                $html .= "</optgroup><optgroup label=\"" . $DB->getField("etude") . "\">";
                            }

                            $last_etude = stripslashes($DB->getField("etude"));

                        }


                        $key = $DB->getField("employes_key");
                        $nom = $DB->getField("nom") . ", " . $DB->getField("prenom");
                        $html .=
                            "<option selectname=\"employes_key\" value=\"" . $key . "\">" . stripslashes($nom) . "</option>\n";
                    }

                    $html .= "</optgroup>";

                } elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {

                    $DB->query(
                        "SELECT DISTINCT em.key AS employes_key, em.prenom AS prenom, em.nom AS nom, em.associe AS associe ".
                        "FROM `employes` AS em, `etudes` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
                        "WHERE et.key = '" . $_SESSION['user_etudes_key'] . "' AND em.key = ees.employes_key AND es.etudes_key = et.key AND ees.etudes_succursales_key = es.key AND em.actif = '1' ORDER BY et.nom ASC;"
                    );

                    while ($DB->next_record()) {
                        $key = $DB->getField("employes_key");
                        $nom = $DB->getField("nom") . ", " . $DB->getField("prenom");

                        if ($_SESSION['employes_key'] == $key) {
                            $html .=
                                "<option selectname=\"employes_key\" value=\"" . $key . "\" selected=\"selected\">" . stripslashes($nom) . "</option>\n";
                        } else {
                            $html .=
                                "<option selectname=\"employes_key\" value=\"" . $key . "\">" . stripslashes($nom) . "</option>\n";
                        }
                    }
                }

                $html .=
                    "</select>\n";
                break;

            case "modify":
                $html .=
                    "<label>Nom du responsable&nbsp;: </label>\n".
                    "<select id=\"employes_key\" name=\"employes_key\">\n";
                    //"<span class=\"answer\"><!---HIDDEN_employe_nom /HIDDEN_employe_nom---></span>";


                if ($_SESSION['user_type'] == 1) {
                    $html .= "<option class=\"default\" value=\"-1\"> - Choisir le responsable - </option>\n";

                    $DB->query(
                        "SELECT DISTINCT em.key AS employes_key, em.prenom AS prenom, em.nom AS nom, em.associe AS associe, et.nom AS etude ".
                        "FROM `employes` AS em, `etudes` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
                        "WHERE em.key = ees.employes_key AND es.etudes_key = et.key AND ees.etudes_succursales_key = es.key AND et.actif = '1' AND es.actif = '1' AND em.actif = '1' ORDER BY et.nom ASC;"
                    );

                    $last_etude = "";
                    while ($DB->next_record()) {

                        if ($last_etude != $DB->getField("etude")) {

                            if ($last_etude == "") {
                                $html .= "<optgroup label=\"" . $DB->getField("etude") . "\">";
                            } else {
                                $html .= "</optgroup><optgroup label=\"" . $DB->getField("etude") . "\">";
                            }

                            $last_etude = stripslashes($DB->getField("etude"));

                        }

                        $key = $DB->getField("employes_key");
                        $nom = $DB->getField("nom") . ", " . $DB->getField("prenom");
                        $html .=
                            "<option selectname=\"employes_key\" value=\"" . $key . "\">" . stripslashes($nom) . "</option>\n";
                    }

                    $html .= "</optgroup>";

                } elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {

                    $DB->query(
                        "SELECT DISTINCT em.key AS employes_key, em.prenom AS prenom, em.nom AS nom, em.associe AS associe ".
                        "FROM `employes` AS em, `etudes` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
                        "WHERE et.key = '" . $_SESSION['user_etudes_key'] . "' AND em.key = ees.employes_key AND es.etudes_key = et.key AND ees.etudes_succursales_key = es.key AND em.actif = '1' ORDER BY et.nom ASC;"
                    );

                    while ($DB->next_record()) {
                        $key = $DB->getField("employes_key");
                        $nom = $DB->getField("nom") . ", " . $DB->getField("prenom");

                        $html .=
                            "<option selectname=\"employes_key\" value=\"" . $key . "\">" . stripslashes($nom) . "</option>\n";
                    }
                }

                $html .=
                    "</select>\n";
                break;

            case "read":
                $html .=
                    "<label>Nom du responsable&nbsp;: </label>\n".
                    "<span class=\"answer\"><!---HIDDEN_employe_nom /HIDDEN_employe_nom---></span>";
                break;
        }
        return $html;
    }

    function secteurs($action){
        $html = "";
        switch ($action) {
            case "add":
                $html .=
                    "<br /><label for=\"secteurs\">Cochez le ou les secteurs concernés</label>\n".
                    "<label for=\"secteur_agricole\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_agricole\" name=\"secteur_agricole\" value=\"1\" /> <span class=\"radio_label\">Droit agricole</span></label>".

                    "<label for=\"secteur_affaire\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_affaire\" name=\"secteur_affaire\" value=\"1\" /> <span class=\"radio_label\">Droit des affaires</span></label>".

                    "<label for=\"secteur_personne\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_personne\" name=\"secteur_personne\" value=\"1\" /> <span class=\"radio_label\">Droit de la personne</span></label>".

                    "<label for=\"secteur_immobilier\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_immobilier\" name=\"secteur_immobilier\" value=\"1\" /> <span class=\"radio_label\">Droit immobilier</span></label>".

                    "<label for=\"secteur_fiducie\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_fiducie\" name=\"secteur_fiducie\" value=\"1\" /> <span class=\"radio_label\">Fiducie</span></label>".

                    "<label for=\"secteur_prd\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_prd\" name=\"secteur_prd\" value=\"1\" /> <span class=\"radio_label\">PRD</span></label>".

                    "<label for=\"secteur_autre\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_autre\" name=\"secteur_autre\" value=\"1\" /> <span class=\"radio_label\">Autres</span></label>".

                    "<div class=\"secteur_autre_text\" style=\"display: none;\"><label for=\"secteur_autre_text\">Spécifiez :</label><input type=\"text\" class=\"text\" id=\"secteur_autre_text\" name=\"secteur_autre_text\" value=\"\" /><br /><br /></div>";
                break;
            case "modify":
                $html .=
                    "<br /><label for=\"secteurs\">Cochez les secteurs pertinents</label>\n".
                    "<label for=\"secteur_agricole\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_agricole\" name=\"secteur_agricole\" value=\"1\" /> <span class=\"radio_label\">Droit agricole</span></label>".

                    "<label for=\"secteur_affaire\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_affaire\" name=\"secteur_affaire\" value=\"1\" /> <span class=\"radio_label\">Droit des affaires</span></label>".

                    "<label for=\"secteur_personne\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_personne\" name=\"secteur_personne\" value=\"1\" /> <span class=\"radio_label\">Droit de la personne</span></label>".

                    "<label for=\"secteur_immobilier\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_immobilier\" name=\"secteur_immobilier\" value=\"1\" /> <span class=\"radio_label\">Droit immobilier</span></label>".

                    "<label for=\"secteur_fiducie\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_fiducie\" name=\"secteur_fiducie\" value=\"1\" /> <span class=\"radio_label\">Fiducie</span></label>".

                    "<label for=\"secteur_prd\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_prd\" name=\"secteur_prd\" value=\"1\" /> <span class=\"radio_label\">PRD</span></label>".

                    "<label for=\"secteur_autre\"><input type=\"checkbox\" class=\"checkbox\" id=\"secteur_autre\" name=\"secteur_autre\" value=\"1\" /> <span class=\"radio_label\">Autres</span></label>".

                    "<div class=\"secteur_autre_text\" style=\"display: none;\"><label for=\"secteur_autre_text\" >Spécifiez :</label><input type=\"text\" class=\"text\" id=\"secteur_autre_text\" name=\"secteur_autre_text\" value=\"\" /><br /><br /></div>";
                break;
            case "read":
                $html .=
                    "<!--label for=\"secteurs\">Cochez les secteurs pertinents</label><br /-->\n".
                    "<span class=\"answer\">Secteur(s) : <!---HIDDEN_secteurs /HIDDEN_secteurs---></span>";
                break;
        }
        return $html;
    }


    function deadline($action) {
        $html = "";
        switch ($action) {
            case "add":
                $html =
                        "<label for=\"deadline\">Date d'échéance&nbsp;: </label>\n".
                        "<input type=\"text\" class=\"text\" name=\"deadline\" id=\"deadline\" value=\"\" /> (format AAAA-MM-JJ)\n".
                        "<br/><br/>";
                break;
            case "modify":
                $html =
                        "<label for=\"deadline\">Date d'échéance&nbsp;: </label>\n".
                        "<input type=\"text\" class=\"text\" name=\"deadline\" id=\"deadline\" value=\"\" /> (format AAAA-MM-JJ)\n".
                        "<br/><br/>";
                break;
            case "read":
                $html =
                    "<span class=\"answer\">Date d'échéance : <!---HIDDEN_deadline /HIDDEN_deadline---></span><br/><br/>\n";
                break;
        }
        return $html;
    }

    function budget($action) {
        $html = "";
        switch ($action) {
            case "add":
                $html .=
                    "<div class=\"col-sm-12\" style=\"padding-left: 0px;\"><label for=\"budget\">Montant alloué pour l’exécution du présent mandat&nbsp;: </label>\n".
                    "<input type=\"text\" id=\"budget\" name=\"budget\" value=\"\" /></div>\n".
                    "<!--div class=\"col-sm-6\" style=\"padding-left: 0px;\"><label for=\"budget_low\">Budget min&nbsp;: </label>\n".
                    "<input type=\"text\" id=\"budget_min\" name=\"budget_min\" value=\"\" /></div>\n".
                    "<div class=\"col-sm-6\"><label for=\"adresse\">Budget max&nbsp;: </label>\n".
                    "<input type=\"text\" id=\"budget_max\" name=\"budget_max\" value=\"\" /></div-->\n";
                break;
            case "modify":
                $html .=
                    "<div class=\"col-sm-12\" style=\"padding-left: 0px;\"><label for=\"budget\">Montant alloué pour l’exécution du présent mandat&nbsp;: </label>\n".
                    "<input type=\"text\" id=\"budget\" name=\"budget\" value=\"\" /></div>\n".
                    "<!--div class=\"col-sm-6\" style=\"padding-left: 0px;\"><label for=\"budget_low\">Budget min&nbsp;: </label>\n".
                    "<input type=\"text\" id=\"budget_min\" name=\"budget_min\" value=\"\" /></div>\n".
                    "<div class=\"col-sm-6\"><label for=\"adresse\">Budget max&nbsp;: </label>\n".
                    "<input type=\"text\" id=\"budget_max\" name=\"budget_max\" value=\"\" /></div-->\n";
                break;
            case "read":
                $html .=
                    "<label>Budget&nbsp;: </label>\n".
                    "<span class=\"answer\"><!---HIDDEN_budget /HIDDEN_budget---></span>".
                    "<!--span class=\"answer\">Entre <!---HIDDEN_budget_min /HIDDEN_budget_min---> et <!---HIDDEN_budget_max /HIDDEN_budget_max---></span-->";
                break;
        }
        return $html;
    }

    function titre($action) {
        $html = "";
        switch ($action) {
            case "add":
                $html .=
                    "<label for=\"titre\">Titre du mandat&nbsp;: </label>\n".
                    "<input type=\"text\" class=\"text\" id=\"titre\" name=\"titre\" value=\"\" />\n";
                break;
            case "modify":
                $html .=
                    "<label for=\"titre\">Titre&nbsp;: </label>\n".
                    "<input type=\"text\" class=\"text\" id=\"titre\" name=\"titre\" value=\"\" />\n";
                break;
            case "read":
                $html .=
                    "<label>Titre&nbsp;: </label>\n".
                    "<span class=\"answer\"><!---HIDDEN_titre /HIDDEN_titre---></span>";
                break;
        }
        return $html;
    }

    function description($action) {
        $html = "";
        switch ($action) {
            case "add":
                $html =
                    "<label for=\"description\">Description du mandat&nbsp;: </label>\n".
                    "<textarea rows=\"8\" cols=\"40\" name=\"description\"></textarea>\n".
                    "<br/><br/>";
                break;
            case "modify":
                $html =
                        "<label for=\"description\">Description&nbsp;: </label>\n".
                        "<textarea rows=\"3\" cols=\"40\" name=\"description\"></textarea>\n".
                        "<br/><br/>";
                break;
            case "read":
                $html =
                    "<label for=\"description\">Description&nbsp;: </label>\n".
                    "<span class=\"answer\"><!---HIDDEN_description /HIDDEN_description---></span><br/><br/>\n";
                break;
        }
        return $html;
    }

    function suivi($action) {
        $html = "";
        switch ($action) {
            case "add":
                $html =
                    "<label for=\"suivi\">Statut&nbsp;: </label>\n".
                    "<select name=\"suivi\">\n".
                    "   <option id=\"suivi\" value=\"1\"></option>\n".
                    "   <option id=\"suivi\" value=\"1\">En cours</option>\n".
                    "   <option id=\"suivi\" value=\"0\">Annul&eacute;</option>\n".
                    "   <option id=\"suivi\" value=\"2\">Complété</option>\n".
                    "</select>\n".
                    "<br/><br/>";
                break;
            case "modify":
                $html =
                    "<label for=\"suivi\">Statut&nbsp;: </label>\n".
                    "<select name=\"suivi\">\n".
                    "   <option id=\"suivi\" value=\"1\"></option>\n".
                    "   <option id=\"suivi\" value=\"1\">En cours</option>\n".
                    "   <option id=\"suivi\" value=\"3\">Annul&eacute;</option>\n".
                    "   <option id=\"suivi\" value=\"2\">Complété</option>\n".
                    "</select>\n".
                    "<br/><br/>";
                break;
            case "read":
                $html =
                    "<label for=\"suivi\">Statut&nbsp;: </label>\n".
                    "<span class=\"answer\"><!---HIDDEN_suivi /HIDDEN_suivi---></span><br/><br/>\n";
                break;
        }
        return $html;

    }
