<?PHP

	// LE FORMULAIRE //
	function getForm($action, $parameters) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "9").

			"	<h2>Responsables</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"domaines_daffaires_notaires_droit_des_personnes\">S&eacute;lectionner les notaires qui se sp&eacute;cialisent en droit des personnes et d&eacute;finir le temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage&nbsp;:</label>\n".
			"		" . domaines_daffaires_notaires_droit_des_personnes($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"employes_domaines_daffaires_droit_des_personnes\">S&eacute;lectionner les collaboratrices ou les techniciennes qui oeuvrent au niveau du droit des personnes et le temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage&nbsp;:</label>\n".
			"		" . domaines_daffaires_collaboratrices_droit_des_personnes($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Formation</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s9_notaires_formation_testament_bool\">Est-ce que des notaires ont suivi la formation sur le <b>testament fiduciaire</b>&nbsp;?</label>\n".
			"		" . s9_notaires_formation_testament_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"formations_notaires_domaines_daffaires_formation_testament\">Si <b>OUI</b>, s&eacute;lectionner le nom des notaires&nbsp;:</label>\n".
			"		" . formations_notaires_domaines_daffaires_formation_testament($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s9_collaboratrices_formation_testament_bool\">Est-ce que des collaboratrices et techniciennes de l'&eacute;tude ont suivi la formation sur le <b>testament fiduciaire</b>&nbsp;?</label>\n".
			"		" . s9_collaboratrices_formation_testament_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"formations_collaboratrices_domaines_daffaires_formation_testament\">Si <b>OUI</b>, s&eacute;lectionner le nom des participants&nbsp;:</label>\n".
			"		" . formations_collaboratrices_domaines_daffaires_formation_testament($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_formations_testament_encore_bool\">Est-ce que l'&eacute;tude aurait besoin que cette formation soit donn&eacute;e &agrave; nouveau&nbsp;?</label>\n".
			"		" . s9_formation_testament_encore_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_notes_formation_droit_affaires\">Notes suppl&eacute;mentaires&nbsp;:</label>\n".
			"		" . s9_notes_formation_droit_affaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Produits informatis&eacute;s de PME INTER Notaires</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s9_utilise_testament_pmeinter_bool\">Est-ce que l'&eacute;tude utilise le testament fiduciaire de PME INTER Notaires&nbsp;?</label>\n".
			"		" . s9_utilise_testament_pmeinter_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_utilise_testament_pmeinter_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s9_utilise_testament_pmeinter_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_nb_testaments\">Combien de testaments fiduciaires l'&eacute;tude a effectu&eacute; l'ann&eacute;e derni&egrave;re et quelles sont les pr&eacute;visions pour l'ann&eacute;e en cours&nbsp;?</label>\n".
			"		" . s9_nb_testaments($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Produits du r&eacute;seau</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_nb_carnets_commandes_2_dernieres_annees\">Combien de carnets l'&eacute;tude a-t-elle command&eacute;s au cours des deux derni&egrave;res ann&eacute;es&nbsp;?</label>\n".
			"			" . s9_nb_carnets_commandes_2_dernieres_annees($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_carnet_moyen_distribution\">Sp&eacute;cifier le moyen de distribution utilis&eacute; pour le carnet personnel ou si l'&eacute;tude n'a command&eacute; aucun carnet personnel et en sp&eacute;cifier les raisons&nbsp;:</label>\n".
			"		" . s9_carnet_moyen_distribution($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s9_utilise_livre_fiducie_bool\">Est-ce que l'&eacute;tude utilise le livre de fiducie de PME INTER Notaires disponible chez R&eacute;so Gestion&nbsp;?</label>\n".
			"		" . s9_utilise_livre_fiducie_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_utilise_livre_fiducie_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s9_utilise_livre_fiducie_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s9_utilise_livre_familial_bool\">Est-ce que l'&eacute;tude utilise le livre familial de PME INTER Notaires disponible chez R&eacute;so Gestion&nbsp;?</label>\n".
			"		" . s9_utilise_livre_familial_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_utilise_livre_familial_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s9_utilise_livre_familial_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s9_utilise_consultations_bool\">Est-ce que l'&eacute;tude utilise les consultations sans frais offerts par PME INTER Notaires <i>(donnant droit &agrave une consultation gratuite au liquidateur lors du d&eacute;c&egrave;s du testateur)</i>&nbsp;?</label>\n".
			"		" . s9_utilise_consultations_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_utilise_consultations_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s9_utilise_consultations_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s9_utilise_autre_promo_bool\">Est-ce que l'&eacute;tude utilise d'autres formes de promotion de services ou de rabais aupr&egrave;s de ses clients&nbsp;?</label>\n".
			"		" . s9_utilise_autre_promo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_utilise_autre_promo_lesquels\">Si <b>OUI</b>, lesquels&nbsp;:</label>\n".
			"		" . s9_utilise_autre_promo_lesquels($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"	<label for=\"s9_utilise_autre_promo_doc\">Annexer un exemplaire&nbsp;:</label>\n".
			"		" . s9_utilise_autre_promo_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Publicit&eacute;</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s9_utilise_docs_promo_bool\">Est-ce que le d&eacute;partement du droit des personnes utilise des documents promotionnels pour promouvoir ses services&nbsp;?</label>\n".
			"		" . s9_utilise_docs_promo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_utilise_docs_promo_sioui_lesquels\">Si <b>OUI</b>, lesquels&nbsp;:</label>\n".
			"		" . s9_utilise_docs_promo_sioui_lesquels($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_utilise_docs_promo_doc\">Annexer un exemplaire&nbsp;:</label>\n".
			"		" . s9_utilise_docs_promo_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"	<label for=\"s9_affiche_docs_promo_bool\">Est-ce que l'&eacute;tude affiche le nom de PME INTER Notaires dans ses documents promotionnels&nbsp;?</label>\n".
			"		" . s9_affiche_docs_promo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"	<label for=\"s9_affiche_docs_promo_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s9_affiche_docs_promo_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"	<label for=\"s9_affiche_conventions_bool\">Est-ce que l'&eacute;tude affiche le logo de PME INTER Notaires (en bas &agrave; droite) sur chaque page des conventions de proc&eacute;dure non contientieuse&nbsp;?</label>\n".
			"		" . s9_affiche_conventions_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s9_affiche_conventions_doc\">Si <b>OUI</b>, annexer un exemplaire&nbsp;:</label>\n".
			"		" . s9_affiche_conventions_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"	<label for=\"s9_affiche_conventions_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s9_affiche_conventions_sinon($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action, $parameters);

		return $_FORM;
	}

	function domaines_daffaires_notaires_droit_des_personnes($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Notaire</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_notaires_droit_des_personnes_employes_key[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_personnes_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_notaires_droit_des_personnes_nom[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_personnes_nom[" . $employes_key . "]\" value=\"" . $DB->getField("prenom") . " " . $DB->getField("nom") . "\" />\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_notaires_droit_des_personnes_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_personnes_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_notaires_droit_des_personnes_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_personnes_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" />&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Notaire</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_notaires_droit_des_personnes_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_personnes_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_notaires_droit_des_personnes_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_personnes_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" />&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"domaines_daffaires_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th width=\"40%\">Notaire</th>\n".
							"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						while ($DB->next_record()) {
							$nb++;
							$employes_key = $DB->getField("employes_key");
							$html .=
								"	<tr>\n".
								"		<td>\n".
								"			<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_domaines_daffaires_notaires_droit_des_personnes_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
								"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
								"		</td>\n".
								"		<td>\n".
								"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_domaines_daffaires_notaires_droit_des_personnes_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" />&nbsp;%\n".
								"		</td>\n".
								"	</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function domaines_daffaires_collaboratrices_droit_des_personnes($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Employ&eacute;</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_fonctions` AS ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_collaboratrices_droit_des_personnes_employes_key[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_personnes_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_collaboratrices_droit_des_personnes_nom[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_personnes_nom[" . $employes_key . "]\" value=\"" . $DB->getField("prenom") . " " . $DB->getField("nom") . "\" />\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_collaboratrices_droit_des_personnes_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_personnes_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_collaboratrices_droit_des_personnes_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_personnes_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" />&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Employ&eacute;</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_fonctions` AS ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_collaboratrices_droit_des_personnes_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_personnes_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_collaboratrices_droit_des_personnes_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_personnes_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" />&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"domaines_daffaires_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th width=\"40%\">Employ&eacute;</th>\n".
							"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` e, `employes_fonctions` AS ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						while ($DB->next_record()) {
							$nb++;
							$employes_key = $DB->getField("employes_key");
							$html .=
								"	<tr>\n".
								"		<td>\n".
								"			<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_domaines_daffaires_collaboratrices_droit_des_personnes_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
								"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
								"		</td>\n".
								"		<td>\n".
								"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_domaines_daffaires_collaboratrices_droit_des_personnes_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" />&nbsp;%\n".
								"		</td>\n".
								"	</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n</div>";
					}
				}
				break;
		}
		return $html;
	}
		
	function s9_notaires_formation_testament_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_notaires_formation_testament_bool\" id=\"s9_notaires_formation_testament_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_notaires_formation_testament_bool\" id=\"s9_notaires_formation_testament_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_notaires_formation_testament_bool\" id=\"s9_notaires_formation_testament_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_notaires_formation_testament_bool\" id=\"s9_notaires_formation_testament_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_notaires_formation_testament_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_notaires_formation_testament_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function formations_notaires_domaines_daffaires_formation_testament($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"hidden\" name=\"formations_notaires_domaines_daffaires_formation_testament_fiduciaire_employes_key[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_testament_fiduciaire_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"	<input type=\"hidden\" name=\"formations_notaires_domaines_daffaires_formation_testament_fiduciaire_nom[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_testament_fiduciaire_nom[" . $employes_key . "]\" value=\"" . $fullname . "\" />\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_notaires_domaines_daffaires_formation_testament_fiduciaire_selectionne[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_testament_fiduciaire_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_notaires_domaines_daffaires_formation_testament_fiduciaire_selectionne[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_testament_fiduciaire_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"formations_domaines_daffaires_entry\">\n".
							"	<table>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` AS e, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						$nb_cols = 0;
						while ($DB->next_record()){
							$nb++;
							$nb_cols++;
							$employes_key = $DB->getField("employes_key");
							$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
							if ($nb_cols == 1) {
								$nb_lastcol = 0;
								$html .=
									"<tr>\n";
							}
							$nb_lastcol++;
							$html .=
								"<td>\n".
								"	<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_formations_notaires_domaines_daffaires_formation_testament_fiduciaire_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
								"</td>\n";
							if ($nb_cols == 4) {
								$nb_cols = 0;
								$html .=
									"</tr>\n";
							}
						}
						if ($nb_lastcol < 4 && $nb > 0) {
							$html .=
								str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
								"</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"	</table>\n".
							"</div>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_collaboratrices_formation_testament_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_collaboratrices_formation_testament_bool\" id=\"s9_collaboratrices_formation_testament_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_collaboratrices_formation_testament_bool\" id=\"s9_collaboratrices_formation_testament_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_collaboratrices_formation_testament_bool\" id=\"s9_collaboratrices_formation_testament_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_collaboratrices_formation_testament_bool\" id=\"s9_collaboratrices_formation_testament_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_collaboratrices_formation_testament_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_collaboratrices_formation_testament_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function formations_collaboratrices_domaines_daffaires_formation_testament($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_fonctions` AS ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"hidden\" name=\"formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_employes_key[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"	<input type=\"hidden\" name=\"formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_nom[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_nom[" . $employes_key . "]\" value=\"" . $fullname . "\" />\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_selectionne[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_fonctions` AS ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_selectionne[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"formations_domaines_daffaires_entry\">\n".
							"	<table>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` AS e, `employes_fonctions` AS ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						$nb_cols = 0;
						while ($DB->next_record()){
							$nb++;
							$nb_cols++;
							$employes_key = $DB->getField("employes_key");
							$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
							if ($nb_cols == 1) {
								$nb_lastcol = 0;
								$html .=
									"<tr>\n";
							}
							$nb_lastcol++;
							$html .=
								"<td>\n".
								"	<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_formations_collaboratrices_domaines_daffaires_formation_testament_fiduciaire_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
								"</td>\n";
							if ($nb_cols == 4) {
								$nb_cols = 0;
								$html .=
									"</tr>\n";
							}
						}
						if ($nb_lastcol < 4 && $nb > 0) {
							$html .=
								str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
								"</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"	</table>\n".
							"</div>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_formation_testament_encore_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_formation_testament_encore_bool\" id=\"s9_formation_testament_encore_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_formation_testament_encore_bool\" id=\"s9_formation_testament_encore_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_formation_testament_encore_bool\" id=\"s9_formation_testament_encore_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_formation_testament_encore_bool\" id=\"s9_formation_testament_encore_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_formation_testament_encore_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_formation_testament_encore_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_notes_formation_droit_affaires($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"8\" name=\"s9_notes_formation_droit_affaires\" id=\"s9_notes_formation_droit_affaires\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"8\" name=\"s9_notes_formation_droit_affaires\" id=\"s9_notes_formation_droit_affaires\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"8\" name=\"" . $compare_sid . "_s9_notes_formation_droit_affaires\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_testament_pmeinter_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_testament_pmeinter_bool\" id=\"s9_utilise_testament_pmeinter_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_testament_pmeinter_bool\" id=\"s9_utilise_testament_pmeinter_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_testament_pmeinter_bool\" id=\"s9_utilise_testament_pmeinter_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_testament_pmeinter_bool\" id=\"s9_utilise_testament_pmeinter_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_testament_pmeinter_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_testament_pmeinter_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_testament_pmeinter_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_testament_pmeinter_sinon\" id=\"s9_utilise_testament_pmeinter_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_testament_pmeinter_sinon\" id=\"s9_utilise_testament_pmeinter_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s9_utilise_testament_pmeinter_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_nb_testaments($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"s9_nb_testaments_entry\">\n".
					"	<table>\n".
					"		<tr>\n".
					"			<th class=\"titre\">Nombre de testaments en " . annee(-1) . "</th><th class=\"titre\">Pr&eacute;visions pour " . annee(0) . "</th>\n".
					"		</tr>\n".
					"		<tr>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"s9_nb_testaments_an_precedent\" id=\"s9_nb_testaments_an_precedent\" value=\"\" /></td>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"s9_nb_testaments_prev_an_actuel\" id=\"s9_nb_testaments_prev_an_actuel\" value=\"\" /></td>\n".
					"		</tr>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"s9_nb_testaments_entry\">\n".
					"	<table>\n".
					"		<tr>\n".
					"			<th class=\"titre\">Nombre de testaments en " . annee(-1) . "</th><th class=\"titre\">Pr&eacute;visions pour " . annee(0) . "</th>\n".
					"		</tr>\n".
					"		<tr>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"s9_nb_testaments_an_precedent\" id=\"s9_nb_testaments_an_precedent\" value=\"\" readonly=\"readonly\" /></td>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"s9_nb_testaments_prev_an_actuel\" id=\"s9_nb_testaments_prev_an_actuel\" value=\"\" readonly=\"readonly\" /></td>\n".
					"		</tr>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"s9_nb_testaments_entry\">\n".
							"	<table>\n".
							"		<tr>\n".
							"			<th class=\"titre\">Nombre de testaments en " . annee(-1) . "</th><th class=\"titre\">Pr&eacute;visions pour " . annee(0) . "</th>\n".
							"		</tr>\n".
							"		<tr>\n".
							"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"" . $compare_sid . "_s9_nb_testaments_an_precedent\" value=\"\" readonly=\"readonly\" /></td>\n".
							"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"" . $compare_sid . "_s9_nb_testaments_prev_an_actuel\" value=\"\" readonly=\"readonly\" /></td>\n".
							"		</tr>\n".
							"	</table>\n".
							"</div>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_nb_carnets_commandes_2_dernieres_annees($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"s9_nb_carnets_commandes_2_dernieres_annees\" id=\"s9_nb_carnets_commandes_2_dernieres_annees\" value=\"1\" />\n";
				break;

			case "read":
				$html = 
					"<input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"s9_nb_carnets_commandes_2_dernieres_annees\" id=\"s9_nb_carnets_commandes_2_dernieres_annees\" value=\"1\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .= "<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"" . $compare_sid . "_s9_nb_carnets_commandes_2_dernieres_annees\" value=\"1\" readonly=\"readonly\" />\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_carnet_moyen_distribution($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_carnet_moyen_distribution\" id=\"s9_carnet_moyen_distribution\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_carnet_moyen_distribution\" id=\"s9_carnet_moyen_distribution\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s9_carnet_moyen_distribution\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_livre_fiducie_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_livre_fiducie_bool\" id=\"s9_utilise_livre_fiducie_bool_1\" value=\"1\" tabindex=\"110\"/><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_livre_fiducie_bool\" id=\"s9_utilise_livre_fiducie_bool_0\" value=\"0\" tabindex=\"110\"/><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_livre_fiducie_bool\" id=\"s9_utilise_livre_fiducie_bool_1\" value=\"1\" tabindex=\"110\"/><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_livre_fiducie_bool\" id=\"s9_utilise_livre_fiducie_bool_0\" value=\"0\" tabindex=\"110\"/><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_livre_fiducie_bool\" value=\"1\" tabindex=\"110\"/><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_livre_fiducie_bool\" value=\"0\" tabindex=\"110\"/><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_livre_fiducie_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_livre_fiducie_sinon\" id=\"s9_utilise_livre_fiducie_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_livre_fiducie_sinon\" id=\"s9_utilise_livre_fiducie_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s9_utilise_livre_fiducie_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_livre_familial_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_livre_familial_bool\" id=\"s9_utilise_livre_familial_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_livre_familial_bool\" id=\"s9_utilise_livre_familial_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_livre_familial_bool\" id=\"s9_utilise_livre_familial_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_livre_familial_bool\" id=\"s9_utilise_livre_familial_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_livre_familial_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_livre_familial_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_livre_familial_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_livre_familial_sinon\" id=\"s9_utilise_livre_familial_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_livre_familial_sinon\" id=\"s9_utilise_livre_familial_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s9_utilise_livre_familial_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_consultations_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_consultations_bool\" id=\"s9_utilise_consultations_bool_1\" value=\"1\" tabindex=\"150\"/><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_consultations_bool\" id=\"s9_utilise_consultations_bool_0\" value=\"0\" tabindex=\"150\"/><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_consultations_bool\" id=\"s9_utilise_consultations_bool_1\" value=\"1\" tabindex=\"150\"/><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_consultations_bool\" id=\"s9_utilise_consultations_bool_0\" value=\"0\" tabindex=\"150\"/><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_consultations_bool\" value=\"1\" tabindex=\"150\"/><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_consultations_bool\" value=\"0\" tabindex=\"150\"/><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_consultations_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_consultations_sinon\" id=\"s9_utilise_consultations_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_consultations_sinon\" id=\"s9_utilise_consultations_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s9_utilise_consultations_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}
	
	function s9_utilise_autre_promo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_autre_promo_bool\" id=\"s9_utilise_autre_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_autre_promo_bool\" id=\"s9_utilise_autre_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_autre_promo_bool\" id=\"s9_utilise_autre_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_autre_promo_bool\" id=\"s9_utilise_autre_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_autre_promo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_autre_promo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_autre_promo_lesquels($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_autre_promo_lesquels\" id=\"s9_utilise_autre_promo_lesquels\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_autre_promo_lesquels\" id=\"s9_utilise_autre_promo_lesquels\" readonly=\"readopnly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s9_utilise_autre_promo_lesquels\" readonly=\"readopnly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_autre_promo_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s9_utilise_autre_promo_doc_num'] == "" && $parameters['s9_utilise_autre_promo_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s9_utilise_autre_promo_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s9_utilise_autre_promo_doc_num\" id=\"s9_utilise_autre_promo_doc_num\" value=\"" . $parameters['s9_utilise_autre_promo_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s9_utilise_autre_promo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s9_utilise_autre_promo_doc_num_del\" id=\"s9_utilise_autre_promo_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s9_utilise_autre_promo_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s9_utilise_autre_promo_doc_num_tmp\" id=\"s9_utilise_autre_promo_doc_num_tmp\" value=\"" . $parameters['s9_utilise_autre_promo_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s9_utilise_autre_promo_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s9_utilise_autre_promo_doc_num_tmp_del\" id=\"s9_utilise_autre_promo_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s9_utilise_autre_promo_doc_num_up\" id=\"s9_utilise_autre_promo_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s9_utilise_autre_promo_doc_phy\" id=\"s9_utilise_autre_promo_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s9_utilise_autre_promo_doc_num'] == "" && $parameters['s9_utilise_autre_promo_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s9_utilise_autre_promo_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s9_utilise_autre_promo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s9_utilise_autre_promo_doc_phy\" id=\"s9_utilise_autre_promo_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<br />\n";
		
						if ($compare_parameters['s9_utilise_autre_promo_doc_num'] == "" && $compare_parameters['s9_utilise_autre_promo_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s9_utilise_autre_promo_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s9_utilise_autre_promo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s9_utilise_autre_promo_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n
							</div>";
					}
				}
				break;
		}

		return $html;
	}

	function s9_utilise_docs_promo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_docs_promo_bool\" id=\"s9_utilise_docs_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_docs_promo_bool\" id=\"s9_utilise_docs_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_docs_promo_bool\" id=\"s9_utilise_docs_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_utilise_docs_promo_bool\" id=\"s9_utilise_docs_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_docs_promo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_utilise_docs_promo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_docs_promo_sioui_lesquels($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_docs_promo_sioui_lesquels\" id=\"s9_utilise_docs_promo_sioui_lesquels\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_utilise_docs_promo_sioui_lesquels\" id=\"s9_utilise_docs_promo_sioui_lesquels\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s9_utilise_docs_promo_sioui_lesquels\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_utilise_docs_promo_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s9_utilise_docs_promo_doc_num'] == "" && $parameters['s9_utilise_docs_promo_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s9_utilise_docs_promo_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s9_utilise_docs_promo_doc_num\" id=\"s9_utilise_docs_promo_doc_num\" value=\"" . $parameters['s9_utilise_docs_promo_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s9_utilise_docs_promo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s9_utilise_docs_promo_doc_num_del\" id=\"s9_utilise_docs_promo_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s9_utilise_docs_promo_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s9_utilise_docs_promo_doc_num_tmp\" id=\"s9_utilise_docs_promo_doc_num_tmp\" value=\"" . $parameters['s9_utilise_docs_promo_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s9_utilise_docs_promo_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s9_utilise_docs_promo_doc_num_tmp_del\" id=\"s9_utilise_docs_promo_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s9_utilise_docs_promo_doc_num_up\" id=\"s9_utilise_docs_promo_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s9_utilise_docs_promo_doc_phy\" id=\"s9_utilise_docs_promo_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s9_utilise_docs_promo_doc_num'] == "" && $parameters['s9_utilise_docs_promo_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s9_utilise_docs_promo_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s9_utilise_docs_promo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s9_utilise_docs_promo_doc_phy\" id=\"s9_utilise_docs_promo_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<br />\n";
		
						if ($compare_parameters['s9_utilise_docs_promo_doc_num'] == "" && $compare_parameters['s9_utilise_docs_promo_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s9_utilise_docs_promo_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s9_utilise_docs_promo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s9_utilise_docs_promo_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_affiche_docs_promo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_affiche_docs_promo_bool\" id=\"s9_affiche_docs_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_affiche_docs_promo_bool\" id=\"s9_affiche_docs_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_affiche_docs_promo_bool\" id=\"s9_affiche_docs_promo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_affiche_docs_promo_bool\" id=\"s9_affiche_docs_promo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_affiche_docs_promo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_affiche_docs_promo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}
	
	function s9_affiche_docs_promo_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_affiche_docs_promo_sinon\" id=\"s9_affiche_docs_promo_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_affiche_docs_promo_sinon\" id=\"s9_affiche_docs_promo_sinon\" readonly\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {			
					$compare_color++;	
						$html .="<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s9_affiche_docs_promo_sinon\" readonly\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}
	
	function s9_affiche_conventions_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_affiche_conventions_bool\" id=\"s9_affiche_conventions_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_affiche_conventions_bool\" id=\"s9_affiche_conventions_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s9_affiche_conventions_bool\" id=\"s9_affiche_conventions_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s9_affiche_conventions_bool\" id=\"s9_affiche_conventions_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_affiche_conventions_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s9_affiche_conventions_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s9_affiche_conventions_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s9_affiche_conventions_doc_num'] == "" && $parameters['s9_affiche_conventions_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s9_affiche_conventions_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s9_affiche_conventions_doc_num\" id=\"s9_affiche_conventions_doc_num\" value=\"" . $parameters['s9_affiche_conventions_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s9_affiche_conventions_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s9_affiche_conventions_doc_num_del\" id=\"s9_affiche_conventions_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s9_affiche_conventions_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s9_affiche_conventions_doc_num_tmp\" id=\"s9_affiche_conventions_doc_num_tmp\" value=\"" . $parameters['s9_affiche_conventions_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s9_affiche_conventions_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s9_affiche_conventions_doc_num_tmp_del\" id=\"s9_affiche_conventions_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s9_affiche_conventions_doc_num_up\" id=\"s9_affiche_conventions_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s9_affiche_conventions_doc_phy\" id=\"s9_affiche_conventions_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s9_affiche_conventions_doc_num'] == "" && $parameters['s9_affiche_conventions_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s9_affiche_conventions_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s9_affiche_conventions_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s9_affiche_conventions_doc_phy\" id=\"s9_affiche_conventions_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
					$compare_color++;
					$html .=
					"<div class=\"answer_compare_" . $compare_color . "\">\n".
						"<br />\n";
	
					if ($compare_parameters['s9_affiche_conventions_doc_num'] == "" && $compare_parameters['s9_affiche_conventions_doc_num_tmp'] == "") {
						$html .=
							"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
	
					} else {
						if ($compare_parameters['s9_affiche_conventions_doc_num'] != "") {
							$html .=
								"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s9_affiche_conventions_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
						}
					}
	
					$html .=
						"<br />\n".
						"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s9_affiche_conventions_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n
						</div>";
					}
				}
				break;
		}
		return $html;
	}
	
	function s9_affiche_conventions_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_affiche_conventions_sinon\" id=\"s9_affiche_conventions_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s9_affiche_conventions_sinon\" id=\"s9_affiche_conventions_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s9_affiche_conventions_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}
?>