<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL;
		
		$_FORM =
			
			form_header($action).
			
			"			<label for=\"nom\">Nom du produit&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . nom($action) . "\n".
			"		</div>\n".

			"			<label for=\"produits_categories_key\">Le groupe dont ce produit fait partie&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . produits_categories_key($action) . "\n".
			"		</div>\n".

			"			<label for=\"description\">Description du produit&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . description($action) . "\n".
			"		</div>\n".

			"			<label for=\"url_image\">Pour ajouter une photo du produit, la s&eacute;lectionner avec le menu ci-dessous <br/>(pour changer la photo, si elle a d&eacute;j&agrave; &eacute;t&eacute; transmise, vous n'avez qu'&agrave; s&eacute;lectionner la nouvelle)&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . url_image($action) . "\n".
			"		</div>\n".

			"			" . delete_image($action) . "\n".

			"			<a name=\"prix_v\"></a><label for=\"prix\">Le prix de ce produit (facultatif)&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . prix($action) . "\n".
			"		</div>\n".
			
			"			<label for=\"numero_produit\">Le num&eacute;ro du produit (facultatif)&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . numero_produit($action) . "\n".
			"		</div>\n".

			"			<label for=\"identification_reseau\">Est-ce que le produit est un produit d'identification au r&eacute;seau&nbsp;? </label>\n".
			"		<div class=\"radio\">\n".
			"			" . identification_reseau($action) . "\n".
			"		</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html =
					"<form name=\"produits\" action=\"produits.php\" method=\"post\" enctype=\"multipart/form-data\">\n".
					"	<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"100000\" />\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n".
					"	<input type=\"hidden\" name=\"produit_key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "modify":
				$html =	
					"<form name=\"produits\" action=\"produits.php\" method=\"post\" enctype=\"multipart/form-data\">\n".
					"	<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"100000\" />\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"produit_key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "read":
				$html =
					"	<div class=\"readform\">\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "modify":
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "read":
				$html =
					"	</div>\n";
				break;
		}
		return $html;
	}


	function nom($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"nom\" id=\"nom\" value=\"\" tabindex=\"20\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"nom\" id=\"nom\" value=\"\" tabindex=\"20\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_nom /HIDDEN_nom---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

 	function produits_categories_key($action){
		$html = "";
		switch ($action) {	
			case "add":
				$html = getCategoriesProduitsSelect("21")."\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = getCategoriesProduitsSelect("21")."\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_produits_categories_key /HIDDEN_produits_categories_key---></span><br/><br/>\n";
				break;
		}
		return $html;
 	}
	function description($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"description\" tabindex=\"22\"></textarea>\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"description\" tabindex=\"22\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_description /HIDDEN_description---></span><br/><br/>\n";
				break;
		}
		return $html;
	}


	function url_image($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"file\" name=\"url_image\" tabindex=\"23\" />".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"file\" name=\"url_image\" tabindex=\"23\" /><span><!---HIDDEN_url_image /HIDDEN_url_image---></span>".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_url_image /HIDDEN_url_image---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function delete_image($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html = " ";
				break;
			case "modify":
				$html = "<span class=\"answer\"><!---HIDDEN_delete_image /HIDDEN_delete_image---></span>";
				break;
			case "read":
				$html = 
					" ";
				break;
		}
		return $html;
	}
	function conferencier($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"conferencier\" value=\"\" tabindex=\"24\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"conferencier\" value=\"\" tabindex=\"24\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_conferencier /HIDDEN_conferencier---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function prix($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"prix\" id=\"prix\" value=\"\" tabindex=\"25\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"prix\" id=\"prix\" value=\"\" tabindex=\"25\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_prix /HIDDEN_prix---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function numero_produit($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"numero_produit\" value=\"\" tabindex=\"26\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"numero_produit\" value=\"\" tabindex=\"26\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_numero_produit /HIDDEN_numero_produit---></span><br/><br/>\n";
				break;
		}
		return $html;
	}
	
	function identification_reseau($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html .=
					"<select name=\"identification_reseau\" tabindex=\"28\"><option id=\"identification_reseau\" value=\"0\">Non</option><option id=\"identification_reseau\" value=\"1\">Oui</option></select>\n";
				break;
			case "modify":
				$html .=
					"<select name=\"identification_reseau\" tabindex=\"28\"><option id=\"identification_reseau\" value=\"0\">Non</option><option id=\"identification_reseau\" value=\"1\">Oui</option></select>\n";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_identification_reseau /HIDDEN_identification_reseau---></span><br/><br/>\n";
				break;
		}
		return $html;
	}
	
?>