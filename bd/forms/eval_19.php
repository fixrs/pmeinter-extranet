<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =	

			form_header($action, "19").

			"	<div class=\"entry\">\n".
			"		<label for=\"s19_testament_fiduciaire_bool\">Est-ce que l'on vous a offert de suivre la formation sur le testament fiduciaire&nbsp;?</label>\n".
			"		" . s19_testament_fiduciaire_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s19_testament_fiduciaire_sinon_offerte\">Si <b>NON</b>, est-ce que vous aimeriez qu'elle vous soit offerte&nbsp;?</label>\n".
			"		" . s19_testament_fiduciaire_sinon_offerte($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s19_utilise_macro_pmeinter_bool\">Utilisez-vous toujours la macro de PME INTER Notaires pour faire vos testaments fiduciaires&nbsp;?</label>\n".
			"		" . s19_utilise_macro_pmeinter_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s19_utilise_macro_pmeinter_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s19_utilise_macro_pmeinter_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s19_nb_testaments_fiduciaires\">Combien de testament fiduciaire avez-vous fait la derni&egrave;re ann&eacute;e&nbsp;?</label>\n".
			"		" . s19_nb_testaments_fiduciaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s19_connait_carnet_pmeinter_bool\">Est-ce que vous connaissez le carnet personnel offert par PME INTER Notaires&nbsp;?</label>\n".
			"		" . s19_connait_carnet_pmeinter_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s19_connait_carnet_pmeinter_sioui\">Si <b>OUI</b>, quelle est l'utilisation que l'on vous demande d'en faire&nbsp;?</label>\n".
			"		" . s19_connait_carnet_pmeinter_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s19_notes_droit_personnes\">Notes suppl&eacute;mentaires</label>\n".
			"		" . s19_notes_droit_personnes($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}

	function s19_testament_fiduciaire_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s19_testament_fiduciaire_bool\" id=\"s19_testament_fiduciaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s19_testament_fiduciaire_bool\" id=\"s19_testament_fiduciaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s19_testament_fiduciaire_bool\" id=\"s19_testament_fiduciaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s19_testament_fiduciaire_bool\" id=\"s19_testament_fiduciaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s19_testament_fiduciaire_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s19_testament_fiduciaire_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s19_testament_fiduciaire_sinon_offerte($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s19_testament_fiduciaire_sinon_offerte\" id=\"s19_testament_fiduciaire_sinon_offerte_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s19_testament_fiduciaire_sinon_offerte\" id=\"s19_testament_fiduciaire_sinon_offerte_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s19_testament_fiduciaire_sinon_offerte\" id=\"s19_testament_fiduciaire_sinon_offerte_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s19_testament_fiduciaire_sinon_offerte\" id=\"s19_testament_fiduciaire_sinon_offerte_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s19_testament_fiduciaire_sinon_offerte\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s19_testament_fiduciaire_sinon_offerte\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s19_utilise_macro_pmeinter_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s19_utilise_macro_pmeinter_bool\" id=\"s19_utilise_macro_pmeinter_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s19_utilise_macro_pmeinter_bool\" id=\"s19_utilise_macro_pmeinter_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s19_utilise_macro_pmeinter_bool\" id=\"s19_utilise_macro_pmeinter_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s19_utilise_macro_pmeinter_bool\" id=\"s19_utilise_macro_pmeinter_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s19_utilise_macro_pmeinter_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s19_utilise_macro_pmeinter_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s19_utilise_macro_pmeinter_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s19_utilise_macro_pmeinter_sinon\" id=\"s19_utilise_macro_pmeinter_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s19_utilise_macro_pmeinter_sinon\" id=\"s19_utilise_macro_pmeinter_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s19_utilise_macro_pmeinter_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s19_nb_testaments_fiduciaires($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s19_nb_testaments_fiduciaires\" id=\"s19_nb_testaments_fiduciaires\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s19_nb_testaments_fiduciaires\" id=\"s19_nb_testaments_fiduciaires\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"text\" class=\"text_short\" name=\"" . $compare_sid . "_s19_nb_testaments_fiduciaires\" value=\"\" readonly=\"readonly\" />\n";
					}
				}
				break;
		}
		return $html;
	}

	function s19_connait_carnet_pmeinter_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s19_connait_carnet_pmeinter_bool\" id=\"s19_connait_carnet_pmeinter_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s19_connait_carnet_pmeinter_bool\" id=\"s19_connait_carnet_pmeinter_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s19_connait_carnet_pmeinter_bool\" id=\"s19_connait_carnet_pmeinter_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s19_connait_carnet_pmeinter_bool\" id=\"s19_connait_carnet_pmeinter_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s19_connait_carnet_pmeinter_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s19_connait_carnet_pmeinter_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s19_connait_carnet_pmeinter_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s19_connait_carnet_pmeinter_sioui\" id=\"s19_connait_carnet_pmeinter_sioui\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s19_connait_carnet_pmeinter_sioui\" id=\"s19_connait_carnet_pmeinter_sioui\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s19_connait_carnet_pmeinter_sioui\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s19_notes_droit_personnes($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s19_notes_droit_personnes\" id=\"s19_notes_droit_personnes\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s19_notes_droit_personnes\" id=\"s19_notes_droit_personnes\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s19_notes_droit_personnes\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

?>