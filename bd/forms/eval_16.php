<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "16").

			"	<h2>Notes suppl&eacute;mentaires&nbsp;</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		" . s16_notes_rencontre_notaires($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}
		
	function s16_notes_rencontre_notaires($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"30\" cols=\"40\" name=\"s16_notes_rencontre_notaires\" id=\"s16_notes_rencontre_notaires\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"30\" cols=\"40\" name=\"s16_notes_rencontre_notaires\" id=\"s16_notes_rencontre_notaires\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"30\" cols=\"40\" name=\"" . $compare_sid . "_s16_notes_rencontre_notaires\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

?>