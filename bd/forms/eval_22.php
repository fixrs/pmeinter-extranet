<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =	

			form_header($action, "22").

			"	<div class=\"entry\">\n".
			"		<label for=\"s22_reception_intermediaire_bool\">Est-ce que vous recevez l'InterM&eacute;diaire de PME INTER Notaires&nbsp;?</label>\n".
			"		" . s22_reception_intermediaire_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s22_reception_intermediaire_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s22_reception_intermediaire_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s22_lecture_intermediaire_bool\">Est-ce que vous lisez syst&eacute;matiquement l'InterM&eacute;diaire&nbsp;?</label>\n".
			"		" . s22_lecture_intermediaire_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s22_lecture_intermediaire_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s22_lecture_intermediaire_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s22_lecture_courriels_pmeinter_bool\">Est-ce que vous lisez syst&eacute;matiquement les courriels de PME INTER Notaires qui sont adress&eacute;s aux techniciennes et aux collaboratrices&nbsp;?</label>\n".
			"		" . s22_lecture_courriels_pmeinter_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s22_lecture_courriels_pmeinter_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s22_lecture_courriels_pmeinter_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s22_partage_trucs_intermediaire_bool\">Est-ce que vous avez d&eacute;j&agrave; partag&eacute; des trucs et astuces pour alimenter la section qui vous est d&eacute;di&eacute;e dans l'InterM&eacute;diaire&nbsp;?</label>\n".
			"		" . s22_partage_trucs_intermediaire_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s22_partage_trucs_intermediaire_sioui_nombre\">Si <b>OUI</b>, combien de fois&nbsp;?</label>\n".
			"		" . s22_partage_trucs_intermediaire_sioui_nombre($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s22_partage_trucs_intermediaire_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s22_partage_trucs_intermediaire_sinon($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}

	function s22_reception_intermediaire_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s22_reception_intermediaire_bool\" id=\"s22_reception_intermediaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s22_reception_intermediaire_bool\" id=\"s22_reception_intermediaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s22_reception_intermediaire_bool\" id=\"s22_reception_intermediaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s22_reception_intermediaire_bool\" id=\"s22_reception_intermediaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s22_reception_intermediaire_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s22_reception_intermediaire_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s22_reception_intermediaire_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s22_reception_intermediaire_sinon\" id=\"s22_reception_intermediaire_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s22_reception_intermediaire_sinon\" id=\"s22_reception_intermediaire_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s22_reception_intermediaire_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s22_lecture_intermediaire_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s22_lecture_intermediaire_bool\" id=\"s22_lecture_intermediaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s22_lecture_intermediaire_bool\" id=\"s22_lecture_intermediaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s22_lecture_intermediaire_bool\" id=\"s22_lecture_intermediaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s22_lecture_intermediaire_bool\" id=\"s22_lecture_intermediaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s22_lecture_intermediaire_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s22_lecture_intermediaire_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s22_lecture_intermediaire_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s22_lecture_intermediaire_sinon\" id=\"s22_lecture_intermediaire_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s22_lecture_intermediaire_sinon\" id=\"s22_lecture_intermediaire_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s22_lecture_intermediaire_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s22_lecture_courriels_pmeinter_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s22_lecture_courriels_pmeinter_bool\" id=\"s22_lecture_courriels_pmeinter_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s22_lecture_courriels_pmeinter_bool\" id=\"s22_lecture_courriels_pmeinter_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s22_lecture_courriels_pmeinter_bool\" id=\"s22_lecture_courriels_pmeinter_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s22_lecture_courriels_pmeinter_bool\" id=\"s22_lecture_courriels_pmeinter_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s22_lecture_courriels_pmeinter_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s22_lecture_courriels_pmeinter_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s22_lecture_courriels_pmeinter_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s22_lecture_courriels_pmeinter_sinon\" id=\"s22_lecture_courriels_pmeinter_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s22_lecture_courriels_pmeinter_sinon\" id=\"s22_lecture_courriels_pmeinter_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s22_lecture_courriels_pmeinter_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s22_partage_trucs_intermediaire_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s22_partage_trucs_intermediaire_bool\" id=\"s22_partage_trucs_intermediaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s22_partage_trucs_intermediaire_bool\" id=\"s22_partage_trucs_intermediaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s22_partage_trucs_intermediaire_bool\" id=\"s22_partage_trucs_intermediaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s22_partage_trucs_intermediaire_bool\" id=\"s22_partage_trucs_intermediaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s22_partage_trucs_intermediaire_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s22_partage_trucs_intermediaire_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s22_partage_trucs_intermediaire_sioui_nombre($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s22_partage_trucs_intermediaire_sioui_nombre\" id=\"s22_partage_trucs_intermediaire_sioui_nombre\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s22_partage_trucs_intermediaire_sioui_nombre\" id=\"s22_partage_trucs_intermediaire_sioui_nombre\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<input type=\"text\" class=\"text_short\" name=\"" . $compare_sid . "_s22_partage_trucs_intermediaire_sioui_nombre\" value=\"\" readonly=\"readonly\" />\n";
					}
				}
				break;
		}
		return $html;
	}

	function s22_partage_trucs_intermediaire_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s22_partage_trucs_intermediaire_sinon\" id=\"s22_partage_trucs_intermediaire_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s22_partage_trucs_intermediaire_sinon\" id=\"s22_partage_trucs_intermediaire_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "s22_partage_trucs_intermediaire_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

?>