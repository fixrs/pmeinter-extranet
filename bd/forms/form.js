function addslashes(str) {
	str = str.replace(/\'/g, '\\\'');
	str = str.replace(/\"/g, '\\"');
	str = str.replace(/\\/g, '\\\\');
	str = str.replace(/\0/g, '\\0');
	return str;
}

function stripslashes(str) {
	str = str.replace(/\\'/g, '\'');
	str = str.replace(/\\"/g, '"');
	str = str.replace(/\\\\/g, '\\');
	str = str.replace(/\\0/g, '\0');
	return str;
}

function trim(str) {
	str = str.replace(/^\s+/, '');
	for (var i = str.length - 1; i > 0; i--) {
		if (/\S/.test(str.charAt(i))) {
			str = str.substring(0, i + 1);
			break;
		}
	}
	return str;
}

function ltrim(str) {
	str = str.replace(/^\s+/, '');
	return str;
}

function rtrim(str) {
	str = str.replace(/\s+$/, '');
	return str;
}

function getRadioValue(radioObj) {
	if (radioObj != null && radioObj != 'undefined') {
		for (var i = radioObj.length-1; i > -1; i--) {
			if (radioObj[i].checked) {
				return radioObj[i].value;
			}
		}
	}
	return null;
}

function setSelectValue(selectObj, selectValue) {
	if (selectObj != null && selectObj != 'undefined') {
		for (var i = 0; i < selectObj.length; i++) {
			if (selectObj[i].value == selectValue) {
				selectObj.selectedIndex = i;
			}
		}
	}
}

function setSelectValues(selectObj, selectValuesArr) {
	if (selectObj != null && selectObj != 'undefined') {
		if (selectValuesArr.lenght > 0) {
			for (var i = 0; i < selectObj.options.length; i++) {
				for (var j = 0; j < selectValuesArr.lenght; j++) {
					if (selectObj.options[i].value == selectValues[j]) {
						selectObj.options[i].selected = true;
					}
				}
			}
		}
	}
}

function getSelectValue(selectObj) {
	if (selectObj != null && selectObj != 'undefined') {
		for (var i = 0; i < selectObj.options.length; i++) {
			if (selectObj.options[i].selected) {
				return selectObj.options[i].value;
			}
		}
	}
	return null;
}

function getSelectValues(selectObj) {
	if (selectObj != null && selectObj != 'undefined') {
		var selected = new Array();
		for (var i = 0; i < selectObj.options.length; i++) {
			if (selectObj.options[i].selected) {
				selected.push(selectObj.options[i].value);
			}
		}
		return selected;
	}
	return null;
}

function validateEmail(value) {
	atpos = value.indexOf('@');
	dotpos = value.lastIndexOf('.');
	if (atpos < 1 || dotpos - atpos < 2) {
		return false;
	}
	return true;
}

jQuery(document).ready(function($) {

	$("#etudes_key").on("change", function() {

		var etude = $( "#etudes_key option:selected" ).text();

		$("#employes_key optgroup").show();
		$("#employes_key optgroup").each(function() {

			if ($(this).attr("label") != etude) {
				$(this).hide();
			}

		});

	});


	if ($("#secteur_autre").length) {
		if ($("#secteur_autre").prop("checked")) {
			$("div.secteur_autre_text").show();
		} else {
			$("div.secteur_autre_text").hide();
		}

		$("#secteur_autre").on("change", function() {
			if ($("#secteur_autre").prop("checked")) {
				$("div.secteur_autre_text").show();
			} else {
				$("div.secteur_autre_text").hide();
			}
		});
	}

	$("#dossiers a.close").on("click", function() {
		$(this).parent().hide();
	});

	$("#dossiers .dossiers-list .mandat .apply-bar a.apply").on("click", function() {

		var key = $(this).parents(".mandat").attr("data-key");

		if ($('#dossiers .dossiers-list .mandat[data-key="' + key + '"] .apply-form').css("display") == "block") {
			$('#dossiers .dossiers-list .mandat[data-key="' + key + '"] .apply-bar').animate({height: '40px'}, 750, function() {
				$('#dossiers .dossiers-list .mandat[data-key="' + key + '"] .apply-form').hide();
			});

		} else {
			$('#dossiers .dossiers-list .mandat[data-key="' + key + '"] .apply-form').show();
			$('#dossiers .dossiers-list .mandat[data-key="' + key + '"] .apply-bar').animate({height: '200px'}, 750);
		}

	});

	function dossierDropzoneEvents() {
		$("input.file_uploader").each(function() {

			var specific_id = $(this).attr("data-id");
			if (!$("#dropzone-box-" + specific_id).length) {
	            $(this).parent().append(getDropzone($(this)));
	            setDropzone($(this));
	        } else {
				$("#dropzone-box-" + specific_id).remove();
	            $("#my-dropzone-files-" + specific_id).remove();
	            $(this).parent().append(getDropzone($(this)));
				setDropzone($(this));
	        }

	    });
    }

    if ($("#dossiers").length) {
    	dossierDropzoneEvents();
    }

	$("#dossiers .dossiers-list .mandat .apply-form form").on("submit", function(e) {
		e.preventDefault();

		if ($(this).find("textarea").val().trim() != '') {
			dossiers_key = $(this).find('input[name="dossiers_key"]').val();
			action = 'apply';
			employes_key = $(this).find('input[name="employes_key"]').val();
			description = $(this).find('textarea').val();
			fichiers = $(this).find('input.file_uploader').val();

			$.ajax({
		        url: '/extranet/mandats/',
		        type: 'post',
		        async: true,
		        cache: false,
		        data: {
		        	dossiers_key: dossiers_key,
		        	action: action,
		        	employes_key: employes_key,
		        	description: description,
		        	fichiers: fichiers,
		        	ajax: 1
		        },
		        success: function(response) {
					$('#dossiers .dossiers-list .mandat[data-key="' + dossiers_key + '"] .apply-form textarea').val('');
		            $('#dossiers .dossiers-list .mandat[data-key="' + dossiers_key + '"] .apply-bar').hide();
		        },
		        error: function(e) {

		        }
		    });

		}

	});





	$("#dossiers .dossiers-list .mandat a.comment").on("click", function() {
		var key = $(this).parents(".mandat").attr("data-key");

		if ($('#dossiers .dossiers-list .mandat[data-key="' + key + '"] .add-comment-form').css("display") == "block") {
			$('#dossiers .dossiers-list .mandat[data-key="' + key + '"] .comments-list .comments').hide();
			$('#dossiers .dossiers-list .mandat[data-key="' + key + '"] .add-comment-form').hide();
		} else {

			$.ajax({
		        url: '/extranet/mandats/',
		        type: 'post',
		        async: true,
		        cache: false,
		        data: {
		        	key: key,
		        	action: 'getComment',
		        	ajax: 1
		        },
		        success: function(response) {
		            $('#dossiers .dossiers-list .mandat[data-key="' + key + '"] .comments-list .comments').html(response);
		            $('#dossiers .dossiers-list .mandat[data-key="' + key + '"] .comments-list .comments').show();
		            $('#dossiers .dossiers-list .mandat[data-key="' + key + '"] .add-comment-form').show();

		            addEventOnComments()
		        },
		        error: function(e) {

		        }
		    });
		}
	});

	$("#dossiers .dossiers-list .mandat .add-comment-form form").on("submit", function(e) {
		e.preventDefault();

		if ($(this).find("textarea").val().trim() != '') {
			dossiers_key = $(this).find('input[name="dossiers_key"]').val();
			action = 'add-comment';
			comments_key = $(this).find('input[name="comments_key"]').val();
			employes_key = $(this).find('input[name="employes_key"]').val();
			description = $(this).find('textarea').val();
			fichiers = $(this).find("input.file_uploader").val();

			$.ajax({
		        url: '/extranet/mandats/',
		        type: 'post',
		        async: true,
		        cache: false,
		        data: {
		        	dossiers_key: dossiers_key,
		        	action: action,
		        	comments_key: comments_key,
		        	employes_key: employes_key,
		        	description: description,
		        	fichiers: fichiers,
		        	ajax: 1
		        },
		        success: function(response) {
					$('#dossiers .dossiers-list .mandat[data-key="' + dossiers_key + '"] .comments-list .comments').html(response);

					$('#dossiers .dossiers-list .mandat[data-key="' + dossiers_key + '"] .add-comment-form input[name="comments_key"]').val('');
					$('#dossiers .dossiers-list .mandat[data-key="' + dossiers_key + '"] .add-comment-form textarea').val('');

		            $('#dossiers .dossiers-list .mandat[data-key="' + dossiers_key + '"] .add-comment-form').show();

		            addEventOnComments();
		        },
		        error: function(e) {

		        }
		    });

		}

	});



	$("#dossiers #search").on("change", function() {

		if ($("#dossiers #search").val() == '') {
			$(".dossiers-list .mandat").show();
		} else {
			$(".dossiers-list .mandat").each(function() {
				var search = $("#dossiers #search").val();
				var hide = 1;
				var reg = new RegExp(search,"gi");

				var budget = parseInt(search);
				var budget_min = $(this).find(".budget").html().replace(/^[^0-9]+([0-9]+).*?$/, '$1');
				var budget_max = $(this).find(".budget").html().replace(/^[^0-9]+([0-9]+).*?[^0-9]+([0-9]+).*?$/, '$2');

				if (budget_min.match(/strong/)) { budget_min = 0; }
				if (budget_max.match(/strong/)) { budget_max = 1000000000; }

				if ($(this).find(".suivi span").html().match(reg)) { hide = 0; }
				if ($(this).find(".responsable").html().match(reg)) { hide = 0; }
				if ($(this).find(".date").html().match(reg)) { hide = 0; }
				if ($(this).find(".etude").html().match(reg)) { hide = 0; }
				if ($(this).find("h2").html().match(reg)) { hide = 0; }
				if ($(this).find(".description").html().match(reg)) { hide = 0; }
				if ($(this).find(".ville").html().match(reg)) { hide = 0; }
				if ($(this).find(".deadline").html().match(reg)) { hide = 0; }
				if ($(this).find(".secteurs").html().match(reg)) { hide = 0; }

				if (budget >= budget_min && budget <= budget_max) { hide = 0; }

				if (hide == 1) { $(this).hide(); } else { $(this).show(); }

			});
		}

	});

	$("#dossiers .tools a.files").on("click", function() {
		var key = $(this).parents(".mandat").attr("data-key");

		if ($("#application" + key).length) {

			$("#application" + key + " a.close").on("click", function() {
				$(this).parent().hide();
			});

			$("#application" + key).show();

		}
	});

	$("#dossiers .tools a.delete").on("click", function() {
		if (confirm("Voulez-vous vraiment supprimer ce mandat?")) {
			var key = $(this).parents(".mandat").attr("data-key");
			var etude = $(this).parents(".mandat").attr("data-etudes_key");

		    $.ajax({
		        url: '/extranet/mandats/',
		        type: 'post',
		        async: true,
		        cache: false,
		        data: {
		        	key: key,
		        	etude: etude,
		        	action: 'delete',
		        	ajax: 1
		        },
		        success: function(response) {
		            $('#dossiers .mandat[data-key="' + key + '"]').remove();
		        },
		        error: function(e) {

		        }
		    });
		}
	});

	$("#dossiers .tools a.edit").on("click", function() {

		var key = $(this).parents(".mandat").attr("data-key");
		var etude = $(this).parents(".mandat").attr("data-etudes_key");

	    $.ajax({
	        url: '/extranet/mandats/',
	        type: 'post',
	        async: true,
	        cache: false,
	        data: {
	        	key: key,
	        	etude: etude,
	        	action: 'edit',
	        	ajax: 1
	        },
	        success: function(response) {
	            $('#dossiers .mandat[data-key="' + key + '"] .edit-form').html(response);
	            $('#dossiers .mandat[data-key="' + key + '"] .edit-form').show();

	            $("#dossiers form .cancel-link").on("click", function() {
					$("#edit-form").remove();
				});

				if ($("#dossiers #partager_avec").val() != '') {
					var partager_avec = $("#dossiers #partager_avec").val().split(",");

					for (var i = 0; i < partager_avec.length; i++) {
						$("#dossiers #choix_etude option[value='" + partager_avec[i] + "']").attr("checked", "checked");
					}

				}

				dossierDropzoneEvents();
	        },
	        error: function(e) {

	        }
	    });

	});

	$("#dossiers ul.filter-suivi li input").on("change", function() {

		var url = window.location.href.replace(/(\#.*)?\?.*$/, '');
		console.log(url);
		$("ul.filter-suivi li input").each(function() {
			if ($(this).prop("checked")) {
				if (url.indexOf('?') > -1){
				   url += '&suivi[]=' + $(this).val();
				}else{
				   url += '?suivi[]=' + $(this).val();
				}
			}
		});

		window.location.href = url;

	});

	$("#dossiers #add-button").on("click", function() {
		$("#dossiers #add-form").show();
		$("#dossiers #add-form-long").show();
	});

	$("table.sortable tr.short").on("click", function() {
		var id = $(this).attr("id").replace(/short/, 'long');
		$("table.sortable tr.long").hide();
		$("#" + id).show();
	});



	function addEventOnComments() {

		$("#dossiers .dossiers-list .mandat .comment.deletable a.delete-comment").on("click", function() {

			key = $(this).parents('.comment').attr("data-key");
			dossiers_key = $(this).parents('.mandat').attr("data-key");

			$.ajax({
		        url: '/extranet/mandats/',
		        type: 'post',
		        async: true,
		        cache: false,
		        data: {
		        	key: key,
		        	dossiers_key: dossiers_key,
		        	action: 'delete-comment',
		        	ajax: 1
		        },
		        success: function(response) {
					$('#dossiers .dossiers-list .mandat[data-key="' + dossiers_key + '"] .comments-list .comments').html(response);
					$('#dossiers .dossiers-list .mandat[data-key="' + dossiers_key + '"] .add-comment-form input[name="comments_key"]').val('');
					$('#dossiers .dossiers-list .mandat[data-key="' + dossiers_key + '"] .add-comment-form textarea').val('');
		            $('#dossiers .dossiers-list .mandat[data-key="' + dossiers_key + '"] .add-comment-form').show();
		        },
		        error: function(e) {

		        }
	    	});
		});

		$("#dossiers .dossiers-list .mandat .comment.deletable a.reply").on("click", function() {

			key = $(this).parents('.comment').attr("data-key");
			$(this).parents(".comments-list").find('.add-comment-form input[name="comments_key"]').val(key);

		});

	}

});


