<?PHP
	global $DB;

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL;
		
		$_FORM =
			form_header($action).
			"<div id=\"convocation_ag\">\n".

			"		<a name=\"date\"></a><label for=\"date\">Date de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_debut($action) . "\n".
			"		</div>\n".

			"		<a name=\"date_limite\"></a><label for=\"date_limite\">Date limite&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_limite($action) . "\n".
			"		</div>\n".

			"		<a name=\"lieu\"></a><label for=\"lieu\">Lieu&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . lieu($action) . "\n".
			"		</div>\n".

			"			<label for=\"description\">Description de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . description($action) . "\n".
			"		</div>\n".

			"		<label for=\"actif\">Actif</label>\n".
			"		<div class=\"field\">\n".
			"			" . actif($action) . "\n".
			"		</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html = 
			"<script type=\"text/javascript\" src=\"/skin/js/tinymce/tinymce.min.js\"></script>\n".
			"<script type=\"text/javascript\">\n".
			"	tinymce.init({\n".
    		"		selector: \"textarea\",\n".
    		"		height: 600\n".
 			"	});\n".
			"</script>\n";

		switch ($action) {
			case "add":					
				$html .=
					"<form name=\"evenements\" action=\"convocation_ag.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n".
					"	<input type=\"hidden\" name=\"id\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "modify":
				$html .=	
					"<form name=\"evenements\" action=\"convocation_ag.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"id\" value=\"\" />\n".
					"	<input type=\"hidden\" name=\"key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "read":
				$html .=
					"	<div class=\"readform\">\n".
					"		<a href=\"/evenements/alerte.php?table=convocation_ag&id=" . $_POST["id"] . "\" target=\"_blank\"><strong>Alerte courriel</strong></a><br />\n".
					"		<a href=\"/evenements/inscription.php?table=convocation_ag&id=" . $_POST["id"] . "\" target=\"_blank\"><strong>Inscriptions</strong></a><br />\n".
					"		<a href=\"/evenements/inscription.php?table=convocation_ag&id=" . $_POST["id"] . "&excel=1\" target=\"_blank\"><strong>Inscriptions Excel</strong></a><br /><br />\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "modify":
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "read":
				$html =
					"	</div>\n";
				break;
		}
		return $html;
	}


	

	function actif($action){
		$html="";
		switch ($action) {	
			case "add":
				$html = 
					"<select name=\"actif\">\n".
					"	<option id=\"actif\" value=\"\"></option>\n".
					"	<option id=\"actif\" value=\"1\">Oui</option>\n".
					"	<option id=\"actif\" value=\"0\">Non</option>\n".
					"</select>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html =
					"<select name=\"actif\">\n".
					"	<option id=\"actif\" value=\"\"></option>\n".
					"	<option id=\"actif\" value=\"1\">Oui</option>\n".
					"	<option id=\"actif\" value=\"0\">Non</option>\n".
					"</select>\n".
					"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_actif /HIDDEN_actif---></span><br/><br/>\n";
				break;
		}
		return $html;
		
	}

	function date_debut($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"date\" id=\"date\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date\" id=\"date\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date /HIDDEN_date---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function date_limite($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"date_limite\" id=\"date_limite\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date_limite\" id=\"date_limite\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date_limite /HIDDEN_date_limite---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function lieu($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"lieu\" id=\"lieu\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"lieu\" id=\"lieu\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_lieu /HIDDEN_lieu---></span><br/><br/>\n";
				break;
		}
		return $html;
	}


	function description($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = 
					"<textarea rows=\"8\" cols=\"40\" name=\"description\">\n".
					"	<div style=\"width: 550px;\">\n".
					"		<img src=\"/skin/img/logo.png\" alt=\"PME Inter\" style=\"width: 80px; height: auto; float: left; padding-right: 10px; padding-bottom: 20px;\" />\n".
					"		<div style=\"background-color: #CC6633; padding: 10px 50px; text-align: center; width: 80%; height: 85px; color: #FFF; margin-left: 85px; font-size: 25px; line-height: 40px; font-weight: normal;\">AVIS DE CONVOCATION<br />&agrave; tous les notaires du r&eacute;seau</div>\n".
					"		<br />\n".
					"		<div>\n".
					"			<p>Vous &ecirc;tes, par la pr&eacute;sente, convoqu&eacute;s &agrave; participer &agrave; l'<em>Assembl&eacute;e annuelle</em> de PME INTER NOTAIRES qui aura lieu <strong>le samedi [DATE], &agrave; 9 h</strong>, &agrave; [LIEU].</p>\n".
					"		</div>\n".
					"		<div>\n".
					"			<p>L'ordre du jour pour cette assembl&eacute;e sera le suivant :</p>\n".
					"			<ul>\n".
					"				<li>1.0 Ouverture de l'assembl&eacute;e</li>\n".
					"				<li>2.0 Rapport du pr&eacute;sident</li>\n".
					"				<li>3.0 Rapport des pr&eacute;sidents des directions de travail\n".
					"					<ul>\n".
					"						<li>3.1 Direction du droit des affaires</li>\n".
					"						<li>3.2 Direction du droit de la personne</li>\n".
					"						<li>3.3 Direction du droit immobilier</li>\n".
					"						<li>3.4 Comit&eacute; de gestion de bureaux</li>\n".
					"					</ul>\n".
					"				</li>\n".
					"				<li>&Eacute;lections</li>\n".
					"				<li>Varia</li>\n".
					"			</ul>\n".
					"			<p>Nous vous remercions de nous confirmer votre pr&eacute;sence <strong><u>avant le [DATE_LIMITE]</u></strong> par t&eacute;l&eacute;copieur au (514) 874-9618 ou par courriel &agrave; <a href=\"mailto:info@pmeinter.com\">info@pmeinter.com</a>.</p>\n".
					"			<p>Au plaisir de vous accueillir,</p>\n".
					"			<hr style=\"border: none; border-top: solid 2px #CC6633; border-bottom: solid 1px #CC6633;\" />\n".
					"		</div>\n".
					"		<div>\n".
					"			<p style=\"color: #CC6633;\">Confirmation de pr&eacute;sence</p>\n".
					"			[INSCRIPTIONS]\n".
					"			<p><em>Pri&egrave;re de nous retourner votre confirmation de pr&eacute;sence <u>avant le [DATE_LIMITE]</u> par t&eacute;l&eacute;copieur au (514) 874-9618 ou par courriel &agrave; <a href=\"mailto:info@pmeinter.com\">info@pmeinter.com</a>.</em></p>\n".
					"		</div>\n".
					"		<hr style=\"border: none; border-top: solid 2px #CC6633; border-bottom: solid 1px #CC6633;\" />\n".
					"		<p>100, boul. Alexis-Nihon bureau 985, Montr&eacute;al, Qu&eacute;bec H4M 2P5 T&eacute;l. : (514) 874-0455 Fax. : (514) 874-9618</p>\n".
					"	</div>".
					"</textarea>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"description\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_description /HIDDEN_description---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

?>