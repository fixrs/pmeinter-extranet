<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =	

			form_header($action, "17").

			"	<h2>Rencontre</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s17_collabo_presentes_rencontre_bool\">Est-ce que toutes les collaboratrices et les techniciennes de l'&eacute;tude sont pr&eacute;sentes &agrave; la rencontre&nbsp;?</label>\n".
			"		" . s17_collabo_presentes_rencontre_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s17_collabo_presentes_rencontre_sinon\">Si <b>NON</b>, indiquer le nom des participantes absentes et sp&eacute;cifiez les raisons&nbsp;:</label>\n".
			"		" . s17_collabo_presentes_rencontre_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Identification au r&eacute;seau</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s17_telephone_identification_reseau\">L'&eacute;tude se pr&eacute;sente et s'identifie au r&eacute;seau de quelle fa&ccedil;on lors de l'accueil t&eacute;l&eacute;phonique (inscrire les mots utilis&eacute;s)&nbsp;:</label>\n".
			"		" . s17_telephone_identification_reseau($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s17_collabo_papeterie_reseau_bool\">Est-ce que les collaboratrices et les techniciennes utilisent syst&eacute;matiquement la papeterie du r&eacute;seau&nbsp;?</label>\n".
			"		" . s17_collabo_papeterie_reseau_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s17_collabo_papeterie_reseau_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s17_collabo_papeterie_reseau_sinon($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}

	function s17_collabo_presentes_rencontre_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s17_collabo_presentes_rencontre_bool\" id=\"s17_collabo_presentes_rencontre_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s17_collabo_presentes_rencontre_bool\" id=\"s17_collabo_presentes_rencontre_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s17_collabo_presentes_rencontre_bool\" id=\"s17_collabo_presentes_rencontre_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s17_collabo_presentes_rencontre_bool\" id=\"s17_collabo_presentes_rencontre_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s17_collabo_presentes_rencontre_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s17_collabo_presentes_rencontre_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s17_collabo_presentes_rencontre_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s17_collabo_presentes_rencontre_sinon\" id=\"s17_collabo_presentes_rencontre_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s17_collabo_presentes_rencontre_sinon\" id=\"s17_collabo_presentes_rencontre_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s17_collabo_presentes_rencontre_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}
	
	function s17_telephone_identification_reseau($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s17_telephone_identification_reseau\" id=\"s17_telephone_identification_reseau\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s17_telephone_identification_reseau\" id=\"s17_telephone_identification_reseau\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s17_telephone_identification_reseau\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}
	
	function s17_collabo_papeterie_reseau_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s17_collabo_papeterie_reseau_bool\" id=\"s17_collabo_papeterie_reseau_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s17_collabo_papeterie_reseau_bool\" id=\"s17_collabo_papeterie_reseau_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s17_collabo_papeterie_reseau_bool\" id=\"s17_collabo_papeterie_reseau_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s17_collabo_papeterie_reseau_bool\" id=\"s17_collabo_papeterie_reseau_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s17_collabo_papeterie_reseau_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s17_collabo_papeterie_reseau_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s17_collabo_papeterie_reseau_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s17_collabo_papeterie_reseau_sinon\" id=\"s17_collabo_papeterie_reseau_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s17_collabo_papeterie_reseau_sinon\" id=\"s17_collabo_papeterie_reseau_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s17_collabo_papeterie_reseau_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

?>