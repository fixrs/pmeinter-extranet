<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "8").

			"	<h2>Responsables</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"domaines_daffaires_notaires_droit_des_affaires\">S&eacute;lectionner les notaires qui se sp&eacute;cialisent en droit des affaires et d&eacute;finir le temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage&nbsp;:</label>\n".
			"		" . domaines_daffaires_notaires_droit_des_affaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"employes_domaines_daffaires_droit_des_affaires\">S&eacute;lectionner les collaboratrices ou les techniciennes qui oeuvrent au niveau du droit des affaires et le temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage&nbsp;:</label>\n".
			"		" . domaines_daffaires_collaboratrices_droit_des_affaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Formation</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_notaires_formation_greffe_corpo_bool\">Est-ce que des notaires ont suivi la formation de base de PME INTER Notaires sur la tenue du <b>greffe corporatif</b>&nbsp;?</label>\n".
			"		" . s8_notaires_formation_greffe_corpo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"formations_notaires_domaines_daffaires_formation_greffe_corpo\">Si <b>OUI</b>, s&eacute;lectionner le nom des notaires&nbsp;:</label>\n".
			"		" . formations_notaires_domaines_daffaires_formation_greffe_corpo($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_collaboratrices_formation_greffe_corpo_bool\">Est-ce que des collaboratrices et des techniciennes de l'&eacute;tude ont suivi la formation de base de PME INTER Notaires sur la tenue du <b>greffe corporatif</b>&nbsp;?</label>\n".
			"		" . s8_collaboratrices_formation_greffe_corpo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"formations_collaboratrices_domaines_daffaires_formation_greffe_corpo\">Si <b>OUI</b>, s&eacute;lectionner le nom des participants&nbsp;:</label>\n".
			"		" . formations_collaboratrices_domaines_daffaires_formation_greffe_corpo($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_formations_greffe_corpo_encore_bool\">Est-ce que l'&eacute;tude aurait besoin que cette formation soit donn&eacute;e &agrave; nouveau&nbsp;?</label>\n".
			"		" . s8_formation_greffe_corpo_encore_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_notes_formation_droit_affaires\">Notes suppl&eacute;mentaires&nbsp;:</label>\n".
			"		" . s8_notes_formation_droit_affaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"formations_notaires_domaines_daffaires_formation_assistant_corpo\">S&eacute;lectionner les notaires de l'&eacute;tude qui ont suivi la formation sur l'<b>Assistant Corporatif</b>&nbsp;:</label>\n".
			"		" . formations_notaires_domaines_daffaires_formation_assistant_corpo($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"formations_collaboratrices_domaines_daffaires_formation_assistant_corpo\">S&eacute;lectionner les collaboratrices et les techniciennes de l'&eacute;tude qui ont suivi la formation sur l'<b>Assistant Corporatif</b>&nbsp;:</label>\n".
			"		" . formations_collaboratrices_domaines_daffaires_formation_assistant_corpo($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_formation_assistant_corpo_aucun_raisons\">Si aucun notaires, collaboratrices ou techniciennes ont suivi la formation sur l'<b>Assistant Corporatif</b>, sp&eacute;cifier les raisons&nbsp;:</label>\n".
			"		" . s8_formation_assistant_corpo_aucun_raisons($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_formation_assistant_corpo_encore_bool\">Est-ce que l'&eacute;tude aurait besoin que cette formation soit donn&eacute;e &agrave; nouveau&nbsp;?</label>\n".
			"		" . s8_formation_assistant_corpo_encore_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Produits informatis&eacute;s du r&eacute;seau</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_logiciel_greffe_bool\">Est-ce que l'&eacute;tude utilise un logiciel pour la tenue du greffe corporatif&nbsp;?</label>\n".
			"		" . s8_logiciel_greffe_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_logiciel_greffe_sioui_lequel\">Si <b>OUI</b>, lequel (Para-Ma&icirc;tre, greffe corporatif PME)&nbsp;:</label>\n".
			"		" . s8_logiciel_greffe_sioui_lequel($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_logiciel_greffe_sioui_raison\"> et pourquoi&nbsp;?</label>\n".
			"		" . s8_logiciel_greffe_sioui_raison($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_logiciel_greffe_sioui\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s8_logiciel_greffe_sinon_raison($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_assistant_corpo_dossiers_bool\">Est-ce que l'&eacute;tude utilise l'Assistant Corporatif pour la gestion de ses dossiers corporatifs&nbsp;?</label>\n".
			"		" . s8_assistant_corpo_dossiers_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_assistant_corpo_dossiers_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s8_assistant_corpo_dossiers_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Exploitation de la formule de PME INTER Notaires</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_greffe_actif_bool\">Est-ce que l'&eacute;tude utilise un logiciel pour la tenue du greffe corporatif&nbsp;?</label>\n".
			"		" . s8_greffe_actif_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_greffe_actif_sinon\">Si <b>NON</b>, indiquer les raisons pour lesquelles l'&eacute;tude n'a pas un greffe corporatif et &agrave; quel moment elle pr&eacute;voit l'activer (ne pas compl&eacute;ter les questions concernant le greffe corporatif)&nbsp;:</label>\n".
			"		" . s8_greffe_actif_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"greffe_actif_sioui\">Si <b>OUI</b>, &eacute;num&eacute;rer les personnes qui le font fonctionner&nbsp;:</label>\n".
			"		" . s8_greffe_actif_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_dediees_greffe_bool\">Est-ce que ces personnes sont d&eacute;di&eacute;es uniquement &agrave; cette fonction&nbsp;?</label>\n".
			"		" . s8_dediees_greffe_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_dediees_greffe_sinon\">Si <b>NON</b>, d&eacute;criver les autres fonctions&nbsp;:</label>\n".
			"		" . s8_dediees_greffe_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_temps_hebdo_greffe\">Identifier le temps hebdomadaire consacr&eacute; &agrave; cette activit&eacute;&nbsp;:</label>\n".
			"		" . s8_temps_hebdo_greffe($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_greffe_agenda\">Est-ce que le temps consacr&eacute; &agrave; la tenue du greffe corporatif est syst&eacute;matiquement inscrit dans l'agenda, ou devient-il important seulement quand il n'y a pas plus d'argent&nbsp;?</label>\n".
			"		" . s8_greffe_agenda($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_rencontre_client_frequence\">Combien de fois par ann&eacute;e le notaire ou sa collaboratrice rencontre le client&nbsp;?</label>\n".
			"		" . s8_rencontre_client_frequence($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_rencontre_client_detail\">D&eacute;finir ce qui se passe lors de ces rencontres&nbsp;:</label>\n".
			"		" . s8_rencontre_client_detail($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_rencontre_offre_produits_bool\">Est-ce que l'&eacute;tude offre d'autres produits lors de ces rencontres (Feuille de suivi corporatif, testament, mandat d'inaptitude, convention d'actionnaires etc.)&nbsp;?</label>\n".
			"		" . s8_rencontre_offre_produits_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_rencontre_offre_produits_sioui\">Si <b>OUI</b>, veuillez les &eacute;num&eacute;rer&nbsp;:</label>\n".
			"		" . s8_rencontre_offre_produits_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_rencontre_offre_produits_sinon\">Si <b>NON</b>, veuillez sp&eacute;cifier les raisons&nbsp;:</label>\n".
			"		" . s8_rencontre_offre_produits_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_nb_clients_greffe\">Veuillez indiquer le nombre de clients actifs du greffe corporatif pour l'ann&eacute;e derni&egrave;re et les pr&eacute;visions pour l'ann&eacute;e en cours&nbsp;:</label>\n".
			"		" . s8_nb_clients_greffe($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_forfait_cout\">Indiquer le co&ucirc;t du forfait pour le service de base de l'&eacute;tude&nbsp;:</label>\n".
			"		" . s8_forfait_cout($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_offre_non_membre_bool\">Est-ce que l'&eacute;tude offre des services corporatifs &agrave; des clients qui ne sont pas membre PME INTER Notaires&nbsp;?</label>\n".
			"		" . s8_offre_non_membre_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_offre_non_membre_sioui\">Si <b>OUI</b>, pourquoi ne sont-ils pas membres&nbsp;?</label>\n".
			"		" . s8_offre_non_membre_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_participe_tous_vente_bool\">Les notaires de l'&eacute;tude participent-ils <b>tous</b> activement &agrave; la vente du service de base de PME INTER Notaires&nbsp;?</label>\n".
			"		" . s8_participe_tous_vente_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_participe_tous_vente_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s8_participe_tous_vente_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_registre_voute_bool\">Est-ce que tous les registres corporatifs (livres de minutes) sont conserv&eacute;s dans une vo&ucirc;te&nbsp;?</label>\n".
			"		" . s8_registre_voute_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_annexes_identifiees_bool\">Les annexes aux statuts d'incorporation ou autres sont-elles identifi&eacute;es au nom de PME INTER Notaires (logo en bas de page &agrave; droite)&nbsp;?</label>\n".
			"		" . s8_annexes_identifiees_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_annexes_identifiees_doc\">Si <b>OUI</b>, joindre un exemple en annexe&nbsp;:</label>\n".
			"		" . s8_annexes_identifiees_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_annexes_identifiees_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s8_annexes_identifiees_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_notes_exploitation_droit_affaires\">Notes suppl&eacute;mentaires&nbsp;:</label>\n".
			"		" . s8_notes_exploitation_droit_affaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_document_logo_bool\">Les documents corporatifs (conventions, ventes ou autres) sont-ils imprim&eacute;s sur du papier portant le logo \"PME INTER Notaires\" (logo en bas de page &agrave; droite)&nbsp;?</label>\n".
			"		" . s8_document_logo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_document_logo_doc\">Si <b>OUI</b>, joindre un exemple en annexe&nbsp;:</label>\n".
			"		" . s8_document_logo_doc($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_document_logo_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s8_document_logo_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Publicit&eacute;</h2>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_promotion_forfait_bool\">Est-ce que le d&eacute;partement du droit corporatif utilise le d&eacute;pliant corporatif pour promouvoir son forfait annuel&nbsp;?</label>\n".
			"		" . s8_promotion_forfait_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_promotion_forfait_moyen\">Si <b>OUI</b>, quel est le moyen de distribution utilis&eacute;&nbsp;:</label>\n".
			"		" . s8_promotion_forfait_moyen($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_promotion_forfait_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s8_promotion_forfait_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_promotion_services_bool\">Est-ce que l'&eacute;tude utilise d'autres documents promotionnels &agrave; l'interne pour promouvoir ses services corporatifs&nbsp;?</label>\n".
			"		" . s8_promotion_services_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_promotion_services_lesquels\">Si <b>OUI</b>, lesquels&nbsp;:</label>\n".
			"		" . s8_promotion_services_lesquels($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_promotion_services_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s8_promotion_services_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_promotion_secteur_bool\">Est-ce que l'&eacute;tude affiche le nom de PME INTER Notaires dans les documents promotionnels d&eacute;velopp&eacute;s par ce secteur d'activit&eacute;&nbsp;?</label>\n".
			"		" . s8_promotion_secteur_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_promotion_secteur_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s8_promotion_secteur_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_notes_doc_promo_droit_affaires\">Notes suppl&eacute;mentaires&nbsp;:</label>\n".
			"		" . s8_notes_doc_promo_droit_affaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_nb_entreprises_bulletin\">Pour l'envoi du bulletin d'affaires \"La R&eacute;f&eacute;rence\" l'&eacute;tude a transmis une liste de combien de noms d'entreprises&nbsp;?</label>\n".
			"		" . s8_nb_entreprises_bulletin($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_satisfait_bulletin_bool\">Est-ce que l'&eacute;tude est satisfaite de cet outil de promotion&nbsp;?</label>\n".
			"		" . s8_satisfait_bulletin_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_satisfait_bulletin_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s8_satisfait_bulletin_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_bulletin_retours_bool\">Est-ce que l'&eacute;tude a eu des retours de ses clients sur cet outil de promotion&nbsp;?</label>\n".
			"		" . s8_bulletin_retours_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_bulletin_retours_commentaires\">Si <b>OUI</b>, indiquer le nombre et les commentaires re&ccedil;us&nbsp;:</label>\n".
			"		" . s8_bulletin_retours_commentaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_notes_bulletin_droit_affaires\">Notes suppl&eacute;mentaires&nbsp;:</label>\n".
			"		" . s8_notes_bulletin_droit_affaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<h2>Produits et services du r&eacute;seau</h2>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_produits_services_reso_gestion\">Montant (en dollars) des produits et des services command&eacute;s par l'&eacute;tude aupr&egrave;s de R&eacute;so Gestion pour les deux derniers trimestres de l'ann&eacute;e " . annee(-1, $sid, "", 1) . "&nbsp;:</label>\n".
			"		" . s8_produits_services_reso_gestion($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_satisfait_services_reso_gestion_bool\">Est-ce que l'&eacute;tude est satisfaite des produits et des services rendus par R&eacute;so Gestion&nbsp;?</label>\n".
			"		" . s8_satisfait_services_reso_gestion_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s8_satisfait_services_reso_gestion_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s8_satisfait_services_reso_gestion_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_utilise_services_reso_gestion_bool\">Est-ce que l'&eacute;tude utilise uniquement les produits et services de R&eacute;so Gestion&nbsp;?</label>\n".
			"		" . s8_utilise_services_reso_gestion_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s8_utilise_services_reso_gestion_sinon\">Si <b>NON</b>, pourquoi&nbsp;:</label>\n".
			"		" . s8_utilise_services_reso_gestion_sinon($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	function domaines_daffaires_notaires_droit_des_affaires($action, $parameters) {
		global $DB;
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Notaire</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_notaires_droit_des_affaires_employes_key[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_affaires_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_notaires_droit_des_affaires_nom[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_affaires_nom[" . $employes_key . "]\" value=\"" . $DB->getField("prenom") . " " . $DB->getField("nom") . "\" />\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_notaires_droit_des_affaires_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_affaires_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_notaires_droit_des_affaires_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_affaires_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" />&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Notaire</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_notaires_droit_des_affaires_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_affaires_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_notaires_droit_des_affaires_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_notaires_droit_des_affaires_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" readonly=\"readonly\"/>&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
					$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"domaines_daffaires_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th width=\"40%\">Notaire</th>\n".
							"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						while ($DB->next_record()) {
							$nb++;
							$employes_key = $DB->getField("employes_key");
							$html .=
								"	<tr>\n".
								"		<td>\n".
								"			<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_domaines_daffaires_notaires_droit_des_affaires_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
								"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
								"		</td>\n".
								"		<td>\n".
								"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_domaines_daffaires_notaires_droit_des_affaires_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" readonly=\"readonly\"/>&nbsp;%\n".
								"		</td>\n".
								"	</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function domaines_daffaires_collaboratrices_droit_des_affaires($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Employ&eacute;</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_collaboratrices_droit_des_affaires_employes_key[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_affaires_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"			<input type=\"hidden\" name=\"domaines_daffaires_collaboratrices_droit_des_affaires_nom[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_affaires_nom[" . $employes_key . "]\" value=\"" . $DB->getField("prenom") . " " . $DB->getField("nom") . "\" />\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_collaboratrices_droit_des_affaires_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_affaires_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_collaboratrices_droit_des_affaires_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_affaires_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" />&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"domaines_daffaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Employ&eacute;</th>\n".
					"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				while ($DB->next_record()) {
					$nb++;
					$employes_key = $DB->getField("employes_key");
					$html .=
						"	<tr>\n".
						"		<td>\n".
						"			<input type=\"checkbox\" class=\"checkbox\" name=\"domaines_daffaires_collaboratrices_droit_des_affaires_selectionne[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_affaires_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
						"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
						"		</td>\n".
						"		<td>\n".
						"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_collaboratrices_droit_des_affaires_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_affaires_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" readonly=\"readonly\" />&nbsp;%\n".
						"		</td>\n".
						"	</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
					$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"domaines_daffaires_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th width=\"40%\">Employ&eacute;</th>\n".
							"				<th width=\"60%\">Temps consacr&eacute; &agrave; cette activit&eacute; en pourcentage</th>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						while ($DB->next_record()) {
							$nb++;
							$employes_key = $DB->getField("employes_key");
							$html .=
								"	<tr>\n".
								"		<td>\n".
								"			<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_domaines_daffaires_collaboratrices_droit_des_affaires_selectionne[" . $employes_key . "]\" value=\"1\" />\n".
								"			<span class=\"checkbox_label\">" . $DB->getField("prenom") . " " . $DB->getField("nom") . "</span>\n".
								"		</td>\n".
								"		<td>\n".
								"			<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"domaines_daffaires_collaboratrices_droit_des_affaires_temps_consacre_pourcent[" . $employes_key . "]\" id=\"domaines_daffaires_collaboratrices_droit_des_affaires_temps_consacre_pourcent[" . $employes_key . "]\" value=\"\" readonly=\"readonly\" />&nbsp;%\n".
								"		</td>\n".
								"	</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_notaires_formation_greffe_corpo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_notaires_formation_greffe_corpo_bool\" id=\"s8_notaires_formation_greffe_corpo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_notaires_formation_greffe_corpo_bool\" id=\"s8_notaires_formation_greffe_corpo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_notaires_formation_greffe_corpo_bool\" id=\"s8_notaires_formation_greffe_corpo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_notaires_formation_greffe_corpo_bool\" id=\"s8_notaires_formation_greffe_corpo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_notaires_formation_greffe_corpo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_notaires_formation_greffe_corpo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function formations_notaires_domaines_daffaires_formation_greffe_corpo($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"hidden\" name=\"formations_notaires_domaines_daffaires_formation_greffe_corpo_employes_key[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_greffe_corpo_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"	<input type=\"hidden\" name=\"formations_notaires_domaines_daffaires_formation_greffe_corpo_nom[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_greffe_corpo_nom[" . $employes_key . "]\" value=\"" . $fullname . "\" />\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_notaires_domaines_daffaires_formation_greffe_corpo_selectionne[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_greffe_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_notaires_domaines_daffaires_formation_greffe_corpo_selectionne[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_greffe_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;	
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"formations_domaines_daffaires_entry\">\n".
							"	<table>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` AS e, `employes_titres` et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						$nb_cols = 0;
						while ($DB->next_record()){
							$nb++;
							$nb_cols++;
							$employes_key = $DB->getField("employes_key");
							$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
							if ($nb_cols == 1) {
								$nb_lastcol = 0;
								$html .=
									"<tr>\n";
							}
							$nb_lastcol++;
							$html .=
								"<td>\n".
								"	<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_formations_notaires_domaines_daffaires_formation_greffe_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
								"</td>\n";
							if ($nb_cols == 4) {
								$nb_cols = 0;
								$html .=
									"</tr>\n";
							}
						}
						if ($nb_lastcol < 4 && $nb > 0) {
							$html .=
								str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
								"</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"	</table>\n".
							"</div>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_collaboratrices_formation_greffe_corpo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_collaboratrices_formation_greffe_corpo_bool\" id=\"s8_collaboratrices_formation_greffe_corpo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_collaboratrices_formation_greffe_corpo_bool\" id=\"s8_collaboratrices_formation_greffe_corpo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_collaboratrices_formation_greffe_corpo_bool\" id=\"s8_collaboratrices_formation_greffe_corpo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_collaboratrices_formation_greffe_corpo_bool\" id=\"s8_collaboratrices_formation_greffe_corpo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_collaboratrices_formation_greffe_corpo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_collaboratrices_formation_greffe_corpo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function formations_collaboratrices_domaines_daffaires_formation_greffe_corpo($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"hidden\" name=\"formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_employes_key[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"	<input type=\"hidden\" name=\"formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_nom[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_nom[" . $employes_key . "]\" value=\"" . $fullname . "\" />\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_selectionne[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_selectionne[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"formations_domaines_daffaires_entry\">\n".
							"	<table>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` AS e, `employes_fonctions` ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						$nb_cols = 0;
						while ($DB->next_record()){
							$nb++;
							$nb_cols++;
							$employes_key = $DB->getField("employes_key");
							$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
							if ($nb_cols == 1) {
								$nb_lastcol = 0;
								$html .=
									"<tr>\n";
							}
							$nb_lastcol++;
							$html .=
								"<td>\n".
								"	<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_formations_collaboratrices_domaines_daffaires_formation_greffe_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
								"</td>\n";
							if ($nb_cols == 4) {
								$nb_cols = 0;
								$html .=
									"</tr>\n";
							}
						}
						if ($nb_lastcol < 4 && $nb > 0) {
							$html .=
								str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
								"</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"	</table>\n".
							"</div>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_formation_greffe_corpo_encore_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_formation_greffe_corpo_encore_bool\" id=\"s8_formation_greffe_corpo_encore_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_formation_greffe_corpo_encore_bool\" id=\"s8_formation_greffe_corpo_encore_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_formation_greffe_corpo_encore_bool\" id=\"s8_formation_greffe_corpo_encore_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_formation_greffe_corpo_encore_bool\" id=\"s8_formation_greffe_corpo_encore_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_formation_greffe_corpo_encore_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_formation_greffe_corpo_encore_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_notes_formation_droit_affaires($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"8\" name=\"s8_notes_formation_droit_affaires\" id=\"s8_notes_formation_droit_affaires\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"8\" name=\"s8_notes_formation_droit_affaires\" id=\"s8_notes_formation_droit_affaires\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"8\" name=\"" . $compare_sid . "_s8_notes_formation_droit_affaires\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function formations_notaires_domaines_daffaires_formation_assistant_corpo($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"hidden\" name=\"formations_notaires_domaines_daffaires_formation_assistant_corpo_employes_key[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_assistant_corpo_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"	<input type=\"hidden\" name=\"formations_notaires_domaines_daffaires_formation_assistant_corpo_nom[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_assistant_corpo_nom[" . $employes_key . "]\" value=\"" . $fullname . "\" />\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_notaires_domaines_daffaires_formation_assistant_corpo_selectionne[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_assistant_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_notaires_domaines_daffaires_formation_assistant_corpo_selectionne[" . $employes_key . "]\" id=\"formations_notaires_domaines_daffaires_formation_assistant_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"formations_domaines_daffaires_entry\">\n".
							"	<table>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` AS e, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = et.employes_key AND et.titres_key = '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						$nb_cols = 0;
						while ($DB->next_record()){
							$nb++;
							$nb_cols++;
							$employes_key = $DB->getField("employes_key");
							$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
							if ($nb_cols == 1) {
								$nb_lastcol = 0;
								$html .=
									"<tr>\n";
							}
							$nb_lastcol++;
							$html .=
								"<td>\n".
								"	<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_formations_notaires_domaines_daffaires_formation_assistant_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
								"</td>\n";
							if ($nb_cols == 4) {
								$nb_cols = 0;
								$html .=
									"</tr>\n";
							}
						}
						if ($nb_lastcol < 4 && $nb > 0) {
							$html .=
								str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
								"</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucun notaire.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"	</table>\n".
							"</div>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function formations_collaboratrices_domaines_daffaires_formation_assistant_corpo($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_fonctions` AS ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"hidden\" name=\"formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_employes_key[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_employes_key[" . $employes_key . "]\" value=\"" . $employes_key . "\" />\n".
						"	<input type=\"hidden\" name=\"formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_nom[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_nom[" . $employes_key . "]\" value=\"" . $fullname . "\" />\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_selectionne[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"formations_domaines_daffaires_entry\">\n".
					"	<table>\n";
				$etudes_key = $parameters['etudes_key'];
				$DB->query(
					"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
					"FROM `employes` AS e, `employes_fonctions` AS ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
					"ORDER BY e.nom ASC;"
				);
				$nb = 0;
				$nb_cols = 0;
				while ($DB->next_record()){
					$nb++;
					$nb_cols++;
					$employes_key = $DB->getField("employes_key");
					$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
					if ($nb_cols == 1) {
						$nb_lastcol = 0;
						$html .=
							"<tr>\n";
					}
					$nb_lastcol++;
					$html .=
						"<td>\n".
						"	<input type=\"checkbox\" class=\"checkbox\" name=\"formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_selectionne[" . $employes_key . "]\" id=\"formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
						"</td>\n";
					if ($nb_cols == 4) {
						$nb_cols = 0;
						$html .=
							"</tr>\n";
					}
				}
				if ($nb_lastcol < 4 && $nb > 0) {
					$html .=
						str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
						"</tr>\n";
				}
				if ($nb == 0) {
					$html .=
						"<tr>\n".
						"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
						"</tr>\n";
				}
				$html .=
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"formations_domaines_daffaires_entry\">\n".
							"	<table>\n";
						$etudes_key = $compare_parameters['etudes_key'];
						$DB->query(
							"SELECT DISTINCT e.key AS employes_key, e.nom AS nom, e.prenom AS prenom ".
							"FROM `employes` AS e, `employes_fonctions` AS ef, `employes_titres` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
							"WHERE e.key = ef.employes_key AND (ef.fonctions_key = '1' OR ef.fonctions_key = '2') AND e.key = et.employes_key AND et.titres_key != '1' AND e.key = ees.employes_key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $etudes_key . "' ".
							"ORDER BY e.nom ASC;"
						);
						$nb = 0;
						$nb_cols = 0;
						while ($DB->next_record()){
							$nb++;
							$nb_cols++;
							$employes_key = $DB->getField("employes_key");
							$fullname = $DB->getField("prenom") . " " . $DB->getField("nom"); 
							if ($nb_cols == 1) {
								$nb_lastcol = 0;
								$html .=
									"<tr>\n";
							}
							$nb_lastcol++;
							$html .=
								"<td>\n".
								"	<input type=\"checkbox\" class=\"checkbox\" name=\"" . $compare_sid . "_formations_collaboratrices_domaines_daffaires_formation_assistant_corpo_selectionne[" . $employes_key . "]\" value=\"1\" /><span class=\"checkbox_label\">" . $fullname . "</span>\n".
								"</td>\n";
							if ($nb_cols == 4) {
								$nb_cols = 0;
								$html .=
									"</tr>\n";
							}
						}
						if ($nb_lastcol < 4 && $nb > 0) {
							$html .=
								str_repeat("<td>&nbsp;</td>\n", 4 - $nb_lastcol).
								"</tr>\n";
						}
						if ($nb == 0) {
							$html .=
								"<tr>\n".
								"	<td colspan=\"2\"><span class=\"warning\">L'&eacute;tude ne contient aucune collaboratrice ou technicienne.</span></td>\n".
								"</tr>\n";
						}
						$html .=
							"	</table>\n".
							"</div>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_formation_assistant_corpo_aucun_raisons($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" name=\"s8_formation_assistant_corpo_aucun_raisons\" id=\"s8_formation_assistant_corpo_aucun_raisons\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" name=\"s8_formation_assistant_corpo_aucun_raisons\" id=\"s8_formation_assistant_corpo_aucun_raisons\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" name=\"" . $compare_sid . "_s8_formation_assistant_corpo_aucun_raisons\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_formation_assistant_corpo_encore_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_formation_assistant_corpo_encore_bool\" id=\"s8_formation_assistant_corpo_encore_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_formation_assistant_corpo_encore_bool\" id=\"s8_formation_assistant_corpo_encore_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_formation_assistant_corpo_encore_bool\" id=\"s8_formation_assistant_corpo_encore_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_formation_assistant_corpo_encore_bool\" id=\"s8_formation_assistant_corpo_encore_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_formation_assistant_corpo_encore_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_formation_assistant_corpo_encore_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_logiciel_greffe_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_logiciel_greffe_bool\" id=\"s8_logiciel_greffe_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_logiciel_greffe_bool\" id=\"s8_logiciel_greffe_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_logiciel_greffe_bool\" id=\"s8_logiciel_greffe_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_logiciel_greffe_bool\" id=\"s8_logiciel_greffe_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_logiciel_greffe_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_logiciel_greffe_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_logiciel_greffe_sioui_lequel($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text\" name=\"s8_logiciel_greffe_sioui_lequel\" id=\"s8_logiciel_greffe_sioui_lequel\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text\" name=\"s8_logiciel_greffe_sioui_lequel\" id=\"s8_logiciel_greffe_sioui_lequel\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;	
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"text\" class=\"text\" name=\"" . $compare_sid . "_s8_logiciel_greffe_sioui_lequel\" value=\"\" readonly=\"readonly\" />\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_logiciel_greffe_sioui_raison($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" name=\"s8_logiciel_greffe_sioui_raison\" id=\"s8_logiciel_greffe_sioui_raison\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" name=\"s8_logiciel_greffe_sioui_raison\" id=\"s8_logiciel_greffe_sioui_raison\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;	
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" name=\"" . $compare_sid . "_s8_logiciel_greffe_sioui_raison\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_logiciel_greffe_sinon_raison($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" name=\"s8_logiciel_greffe_sinon_raison\" id=\"s8_logiciel_greffe_sinon_raison\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" name=\"s8_logiciel_greffe_sinon_raison\" id=\"s8_logiciel_greffe_sinon_raison\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;	
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" name=\"" . $compare_sid . "_s8_logiciel_greffe_sinon_raison\" readonly=\"readonly\"></textarea>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_assistant_corpo_dossiers_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_assistant_corpo_dossiers_bool\" id=\"s8_assistant_corpo_dossiers_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_assistant_corpo_dossiers_bool\" id=\"s8_assistant_corpo_dossiers_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_assistant_corpo_dossiers_bool\" id=\"s8_assistant_corpo_dossiers_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_assistant_corpo_dossiers_bool\" id=\"s8_assistant_corpo_dossiers_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
						
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_assistant_corpo_dossiers_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_assistant_corpo_dossiers_bool\" value=\"0\" /><span class=\"radio_label\">Non</span></div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_assistant_corpo_dossiers_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" name=\"s8_assistant_corpo_dossiers_sinon\" id=\"s8_assistant_corpo_dossiers_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" name=\"s8_assistant_corpo_dossiers_sinon\" id=\"s8_assistant_corpo_dossiers_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" name=\"" . $compare_sid . "_s8_assistant_corpo_dossiers_sinon\" readonly=\"readonly\"></textarea>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_greffe_actif_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_greffe_actif_bool\" id=\"s8_greffe_actif_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_greffe_actif_bool\" id=\"s8_greffe_actif_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_greffe_actif_bool\" id=\"s8_greffe_actif_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_greffe_actif_bool\" id=\"s8_greffe_actif_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_greffe_actif_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_greffe_actif_bool\" value=\"0\" /><span class=\"radio_label\">Non</span></div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_greffe_actif_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" name=\"s8_greffe_actif_sinon\" id=\"s8_greffe_actif_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" name=\"s8_greffe_actif_sinon\" id=\"s8_greffe_actif_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" name=\"" . $compare_sid . "_s8_greffe_actif_sinon\" readonly=\"readonly\"></textarea>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_greffe_actif_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" name=\"s8_greffe_actif_sioui\" id=\"s8_greffe_actif_sioui\"></textarea>\n";
				break;

			case "read":
				$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
					"<textarea rows=\"4\" name=\"s8_greffe_actif_sioui\" id=\"s8_greffe_actif_sioui\" readonly=\"readonly\"></textarea>\n</div>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" name=\"" . $compare_sid . "_s8_greffe_actif_sioui\" readonly=\"readonly\"></textarea>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_dediees_greffe_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_dediees_greffe_bool\" id=\"s8_dediees_greffe_bool_1\" value=\"1\"/><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_dediees_greffe_bool\" id=\"s8_dediees_greffe_bool_0\" value=\"0\"/><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_dediees_greffe_bool\" id=\"s8_dediees_greffe_bool_1\" value=\"1\"/><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_dediees_greffe_bool\" id=\"s8_dediees_greffe_bool_0\" value=\"0\"/><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_dediees_greffe_bool\" value=\"1\"/><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_dediees_greffe_bool\" value=\"0\"/><span class=\"radio_label\">Non</span></div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_dediees_greffe_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_dediees_greffe_sinon\" id=\"s8_dediees_greffe_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_dediees_fonction_sinon\" id=\"s8_dediees_fonction_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_dediees_fonction_sinon\" readonly=\"readonly\"></textarea>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_temps_hebdo_greffe($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"short_text\" name=\"s8_temps_hebdo_greffe\" id=\"s8_temps_hebdo_greffe\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"short_text\" name=\"s8_temps_hebdo_greffe\" id=\"s8_temps_hebdo_greffe\" value=\"\" readonly=\"readonly\"/>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"text\" class=\"short_text\" name=\"" . $compare_sid . "_s8_temps_hebdo_greffe\" value=\"\" readonly=\"readonly\"/>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_greffe_agenda($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"short_text\" name=\"s8_greffe_agenda\" id=\"s8_greffe_agenda\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"short_text\" name=\"s8_greffe_agenda\" id=\"s8_greffe_agenda\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"text\" class=\"short_text\" name=\"" . $compare_sid . "_s8_greffe_agenda\" value=\"\" readonly=\"readonly\" />\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_rencontre_client_frequence($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"short_text\" name=\"s8_rencontre_client_frequence\" id=\"s8_rencontre_client_frequence\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"short_text\" name=\"s8_rencontre_client_frequence\" id=\"s8_rencontre_client_frequence\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"text\" class=\"short_text\" name=\"" . $compare_sid . "_s8_rencontre_client_frequence\" value=\"\" readonly=\"readonly\" />\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_rencontre_client_detail($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_rencontre_client_detail\" id=\"s8_rencontre_client_detail\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_rencontre_client_detail\" id=\"s8_rencontre_client_detail\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_rencontre_client_detail\" readonly=\"readonly\"></textarea>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_rencontre_offre_produits_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_rencontre_offre_produits_bool\" id=\"s8_rencontre_offre_produits_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_rencontre_offre_produits_bool\" id=\"s8_rencontre_offre_produits_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_rencontre_offre_produits_bool\" id=\"s8_rencontre_offre_produits_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_rencontre_offre_produits_bool\" id=\"s8_rencontre_offre_produits_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_rencontre_offre_produits_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_rencontre_offre_produits_bool\" value=\"0\" /><span class=\"radio_label\">Non</span></div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_rencontre_offre_produits_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_rencontre_offre_produits_sioui\" id=\"s8_rencontre_offre_produits_sioui\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_rencontre_offre_produits_sioui\" id=\"s8_rencontre_offre_produits_sioui\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_rencontre_offre_produits_sioui\" readonly=\"readonly\"></textarea>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_rencontre_offre_produits_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_rencontre_offre_produits_sinon\" id=\"s8_rencontre_offre_produits_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_rencontre_offre_produits_sinon\" id=\"s8_rencontre_offre_produits_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_rencontre_offre_produits_sinon\" readonly=\"readonly\"></textarea>\n</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_nb_clients_greffe($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"s8_nb_clients_greffe_entry\">\n".
					"	<table>\n".
					"		<tr>\n".
					"			<th class=\"titre\">Nombre de clients en " . annee(-1) . "</th><th class=\"titre\">Nombre de clients en " . annee(0) . "</th>\n".
					"		</tr>\n".
					"		<tr>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"s8_nb_clients_greffe_an_precedent\" id=\"s8_nb_clients_greffe_an_precedent\" value=\"\" /></td>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"s8_nb_clients_greffe_an_actuel\" id=\"s8_nb_clients_greffe_an_actuel\" value=\"\" /></td>\n".
					"		</tr>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"s8_nb_clients_greffe_entry\">\n".
					"	<table>\n".
					"		<tr>\n".
					"			<th class=\"titre\">Nombre de clients en " . annee(-1) . "</th><th class=\"titre\">Nombre de clients en " . annee(0) . "</th>\n".
					"		</tr>\n".
					"		<tr>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"s8_nb_clients_greffe_an_precedent\" id=\"s8_nb_clients_greffe_an_precedent\" value=\"\" readonly=\"readonly\" /></td>\n".
					"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"s8_nb_clients_greffe_an_actuel\" id=\"s8_nb_clients_greffe_an_actuel\" value=\"\" readonly=\"readonly\" /></td>\n".
					"		</tr>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"s8_nb_clients_greffe_entry\">\n".
							"	<table>\n".
							"		<tr>\n".
							"			<th class=\"titre\">Nombre de clients en " . annee(-1) . "</th><th class=\"titre\">Nombre de clients en " . annee(0) . "</th>\n".
							"		</tr>\n".
							"		<tr>\n".
							"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"" . $compare_sid . "_s8_nb_clients_greffe_an_precedent\" value=\"\" readonly=\"readonly\" /></td>\n".
							"			<td><input type=\"text\" class=\"text\" style=\"width: 40px;\" name=\"" . $compare_sid . "_s8_nb_clients_greffe_an_actuel\" value=\"\" readonly=\"readonly\" /></td>\n".
							"		</tr>\n".
							"	</table>\n".
							"</div>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_forfait_cout($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s8_forfait_cout\" id=\"s8_forfait_cout\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s8_forfait_cout\" id=\"s8_forfait_cout\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
							$compare_color++;
							$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"text\" class=\"text_short\" name=\"" . $compare_sid . "_s8_forfait_cout\" value=\"\" readonly=\"readonly\" />\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_offre_non_membre_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_offre_non_membre_bool\" id=\"s8_offre_non_membre_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_offre_non_membre_bool\" id=\"s8_offre_non_membre_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_offre_non_membre_bool\" id=\"s8_offre_non_membre_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_offre_non_membre_bool\" id=\"s8_offre_non_membre_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_offre_non_membre_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_offre_non_membre_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_offre_non_membre_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_offre_non_membre_sioui\" id=\"s8_offre_non_membre_sioui\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_offre_non_membre_sioui\" id=\"s8_offre_non_membre_sioui\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_offre_non_membre_sioui\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_participe_tous_vente_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_participe_tous_vente_bool\" id=\"s8_participe_tous_vente_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_participe_tous_vente_bool\" id=\"s8_participe_tous_vente_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_participe_tous_vente_bool\" id=\"s8_participe_tous_vente_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_participe_tous_vente_bool\" id=\"s8_participe_tous_vente_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_participe_tous_vente_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_participe_tous_vente_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_participe_tous_vente_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_participe_tous_vente_sinon\" id=\"s8_participe_tous_vente_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_participe_tous_vente_sinon\" id=\"s8_participe_tous_vente_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_participe_tous_vente_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_registre_voute_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_registre_voute_bool\" id=\"s8_registre_voute_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_registre_voute_bool\" id=\"s8_registre_voute_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_registre_voute_bool\" id=\"s8_registre_voute_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_registre_voute_bool\" id=\"s8_registre_voute_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_registre_voute_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_registre_voute_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_annexes_identifiees_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_annexes_identifiees_bool\" id=\"s8_annexes_identifiees_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_annexes_identifiees_bool\" id=\"s8_annexes_identifiees_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_annexes_identifiees_bool\" id=\"s8_annexes_identifiees_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_annexes_identifiees_bool\" id=\"s8_annexes_identifiees_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_annexes_identifiees_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_annexes_identifiees_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_annexes_identifiees_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s8_annexes_identifiees_doc_num'] == "" && $parameters['s8_annexes_identifiees_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s8_annexes_identifiees_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s8_annexes_identifiees_doc_num\" id=\"s8_annexes_identifiees_doc_num\" value=\"" . $parameters['s8_annexes_identifiees_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s8_annexes_identifiees_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s8_annexes_identifiees_doc_num_del\" id=\"s8_annexes_identifiees_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s8_annexes_identifiees_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s8_annexes_identifiees_doc_num_tmp\" id=\"s8_annexes_identifiees_doc_num_tmp\" value=\"" . $parameters['s8_annexes_identifiees_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s8_annexes_identifiees_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s8_annexes_identifiees_doc_num_tmp_del\" id=\"s8_annexes_identifiees_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s8_annexes_identifiees_doc_num_up\" id=\"s8_annexes_identifiees_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s8_annexes_identifiees_doc_phy\" id=\"s8_annexes_identifiees_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s8_annexes_identifiees_doc_num'] == "" && $parameters['s8_annexes_identifiees_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s8_annexes_identifiees_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s8_annexes_identifiees_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s8_annexes_identifiees_doc_phy\" id=\"s8_annexes_identifiees_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<br />\n";
		
						if ($compare_parameters['s8_annexes_identifiees_doc_num'] == "" && $compare_parameters['s8_annexes_identifiees_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s8_annexes_identifiees_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s8_annexes_identifiees_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s8_annexes_identifiees_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_annexes_identifiees_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_annexes_identifiees_sinon\" id=\"s8_annexes_identifiees_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_annexes_identifiees_sinon\" id=\"s8_annexes_identifiees_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_annexes_identifiees_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_notes_exploitation_droit_affaires($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_notes_exploitation_droit_affaires\" id=\"s8_notes_exploitation_droit_affaires\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_notes_exploitation_droit_affaires\" id=\"s8_notes_exploitation_droit_affaires\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_notes_exploitation_droit_affaires\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_document_logo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_document_logo_bool\" id=\"s8_document_logo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_document_logo_bool\" id=\"s8_document_logo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_document_logo_bool\" id=\"s8_document_logo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_document_logo_bool\" id=\"s8_document_logo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_document_logo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_document_logo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_document_logo_doc($action, $parameters) {
		global $BASEURL;
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<br />\n";

				if ($parameters['s8_document_logo_doc_num'] == "" && $parameters['s8_document_logo_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s8_document_logo_doc_num'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s8_document_logo_doc_num\" id=\"s8_document_logo_doc_num\" value=\"" . $parameters['s8_document_logo_doc_num'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s8_document_logo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s8_document_logo_doc_num_del\" id=\"s8_document_logo_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n".
							"<br />\n";
					}
					if ($parameters['s8_document_logo_doc_num_tmp'] != "") {
						$html .=
							"<input type=\"hidden\" name=\"s8_document_logo_doc_num_tmp\" id=\"s8_document_logo_doc_num_tmp\" value=\"" . $parameters['s8_document_logo_doc_num_tmp'] . "\" />\n".
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s8_document_logo_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s8_document_logo_doc_num_tmp_del\" id=\"s8_document_logo_doc_num_tmp_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document actuel.</span><br />\n".
							"<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"file\" class=\"file\" name=\"s8_document_logo_doc_num_up\" id=\"s8_document_logo_doc_num_up\" />&nbsp; Copie num&eacute;rique (fichier DOC, XLS ou PDF)\n".
					"<div style=\"padding: 4px 0px 4px 0px;\">-&nbsp;ou&nbsp;-</div>\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s8_document_logo_doc_phy\" id=\"s8_document_logo_doc_phy\" value=\"\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "read":
				$html .=
					"<br />\n";

				if ($parameters['s8_document_logo_doc_num'] == "" && $parameters['s8_document_logo_doc_num_tmp'] == "") {
					$html .=
						"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";

				} else {
					if ($parameters['s8_document_logo_doc_num'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s8_document_logo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
							"<br />\n".
							"<input type=\"checkbox\" class=\"checkbox\" name=\"s8_document_logo_doc_num_del\" id=\"s8_document_logo_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n";
					}
					if ($parameters['s8_document_logo_doc_num_tmp'] != "") {
						$html .=
							"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $parameters['s8_document_logo_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n";
					}
				}

				$html .=
					"<br />\n".
					"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"s8_document_logo_doc_phy\" id=\"s8_document_logo_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<br />\n";
		
						if ($parameters['s8_document_logo_doc_num'] == "" && $parameters['s8_document_logo_doc_num_tmp'] == "") {
							$html .=
								"Aucune copie num&eacute;rique n'est d&eacute;j&agrave; incluse.<br />\n";
		
						} else {
							if ($compare_parameters['s8_document_logo_doc_num'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s8_document_logo_doc_num'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique d&eacute;j&agrave; incluse (<b>sauvegard&eacute;</b>).<br />\n".
									"<br />\n".
									"<input type=\"checkbox\" class=\"checkbox\" name=\"s8_document_logo_doc_num_del\" id=\"s8_document_logo_doc_num_del\" value=\"1\" /><span class=\"checkbox_label\">Cocher pour supprimer le document d&eacute;j&agrave; inclus.</span><br />\n";
							}
							if ($compare_parameters['s8_document_logo_doc_num_tmp'] != "") {
								$html .=
									"<b><a href=\"" . $BASEURL . "docs/evaluations/" . $compare_parameters['s8_document_logo_doc_num_tmp'] . "\" target=\"blank_\">Cliquer ici</a></b> pour un apper&ccedil;u de la copie num&eacute;rique actuelle (<b>non sauvegard&eacute;</b>).<br />\n";
							}
						}
		
						$html .=
							"<br />\n".
							"<input type=\"text\" class=\"text\" style=\"width: 212px;\" name=\"" . $compare_sid . "_s8_document_logo_doc_phy\" value=\"\" readonly=\"readonly\" />&nbsp; Copie physique (donner le nom du document)\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_document_logo_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_document_logo_sinon\" id=\"s8_document_logo_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_document_logo_sinon\" id=\"s8_document_logo_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_document_logo_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_promotion_forfait_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_forfait_bool\" id=\"s8_promotion_forfait_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_forfait_bool\" id=\"s8_promotion_forfait_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_forfait_bool\" id=\"s8_promotion_forfait_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_forfait_bool\" id=\"s8_promotion_forfait_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_promotion_forfait_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_promotion_forfait_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_promotion_forfait_moyen($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_promotion_forfait_moyen\" id=\"s8_promotion_forfait_moyen\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_promotion_forfait_moyen\" id=\"s8_promotion_forfait_moyen\" readonly\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_promotion_forfait_moyen\" readonly\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_promotion_forfait_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_promotion_forfait_sinon\" id=\"s8_promotion_forfait_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_promotion_forfait_sinon\" id=\"s8_promotion_forfait_sinon\" readonly\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_promotion_forfait_sinon\" readonly\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_promotion_services_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_services_bool\" id=\"s8_promotion_services_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_services_bool\" id=\"s8_promotion_services_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_services_bool\" id=\"s8_promotion_services_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_services_bool\" id=\"s8_promotion_services_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_promotion_services_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_promotion_services_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_promotion_services_lesquels($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_promotion_services_lesquels\" id=\"s8_promotion_services_lesquels\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_promotion_services_lesquels\" id=\"s8_promotion_services_lesquels\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_promotion_services_lesquels\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_promotion_services_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_promotion_services_sinon\" id=\"s8_promotion_services_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_promotion_services_sinon\" id=\"s8_promotion_services_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_promotion_services_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_promotion_secteur_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_secteur_bool\" id=\"s8_promotion_secteur_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_secteur_bool\" id=\"s8_promotion_secteur_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_secteur_bool\" id=\"s8_promotion_secteur_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_promotion_secteur_bool\" id=\"s8_promotion_secteur_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_promotion_secteur_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_promotion_secteur_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_promotion_secteur_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_promotion_secteur_sinon\" id=\"s8_promotion_secteur_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_promotion_secteur_sinon\" id=\"s8_promotion_secteur_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_promotion_secteur_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_notes_doc_promo_droit_affaires($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_notes_doc_promo_droit_affaires\" id=\"s8_notes_doc_promo_droit_affaires\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_notes_doc_promo_droit_affaires\" id=\"s8_notes_doc_promo_droit_affaires\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_notes_doc_promo_droit_affaires\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_nb_entreprises_bulletin($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"short_text\" name=\"s8_nb_entreprises_bulletin\" id=\"s8_nb_entreprises_bulletin\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"short_text\" name=\"s8_nb_entreprises_bulletin\" id=\"s8_nb_entreprises_bulletin\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"text\" class=\"short_text\" name=\"" . $compare_sid . "_s8_nb_entreprises_bulletin\" value=\"\" readonly=\"readonly\" />\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_satisfait_bulletin_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_satisfait_bulletin_bool\" id=\"s8_satisfait_bulletin_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_satisfait_bulletin_bool\" id=\"s8_satisfait_bulletin_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_satisfait_bulletin_bool\" id=\"s8_satisfait_bulletin_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_satisfait_bulletin_bool\" id=\"s8_satisfait_bulletin_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_satisfait_bulletin_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_satisfait_bulletin_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_satisfait_bulletin_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_satisfait_bulletin_sinon\" id=\"s8_satisfait_bulletin_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_satisfait_bulletin_sinon\" id=\"s8_satisfait_bulletin_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
					$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_satisfait_bulletin_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_bulletin_retours_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_bulletin_retours_bool\" id=\"s8_bulletin_retours_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_bulletin_retours_bool\" id=\"s8_bulletin_retours_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_bulletin_retours_bool\" id=\"s8_bulletin_retours_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_bulletin_retours_bool\" id=\"s8_bulletin_retours_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_bulletin_retours_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_bulletin_retours_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}
	
	
	function s8_bulletin_retours_commentaires($action, $parameters){
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_bulletin_retours_commentaires\" id=\"s8_bulletin_retours_commentaires\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_bulletin_retours_commentaires\" id=\"s8_bulletin_retours_commentaires\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;	
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_bulletin_retours_commentaires\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_notes_bulletin_droit_affaires($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_notes_bulletin_droit_affaires\" id=\"s8_notes_bulletin_droit_affaires\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_notes_bulletin_droit_affaires\" id=\"s8_notes_bulletin_droit_affaires\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_notes_bulletin_droit_affaires\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_produits_services_reso_gestion($action, $parameters) {
		global $DB;
		$html = ""; 
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"s8_produits_services_reso_gestion_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Produits</th>\n".
					"				<th width=\"60%\">Montant</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n".
					"			<tr>\n".
					"				<td>\n".
					"					Livre de minutes\n".
					"				</td>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s8_produits_services_reso_gestion_livre_minutes_montant\" id=\"s8_produits_services_reso_gestion_livre_minutes_montant\" value=\"\" />&nbsp;$\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<td>\n".
					"					Livre de fiducie\n".
					"				</td>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s8_produits_services_reso_gestion_livre_fiducie_montant\" id=\"s8_produits_services_reso_gestion_livre_fiducie_montant\" value=\"\" />&nbsp;$\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<td>\n".
					"					Services corporatifs<br />(feuilles de registres, certificats d’actions, etc)\n".
					"				</td>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s8_produits_services_reso_gestion_services_corpo_montant\" id=\"s8_produits_services_reso_gestion_service_corpo_montant\" value=\"\" />&nbsp;$\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<td>\n".
					"					Recherche de nom\n".
					"				</td>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 40px;text-align: right;\" name=\"s8_produits_services_reso_gestion_recherche_nom_montant\" id=\"s8_produits_services_reso_gestion_recherche_nom_montant\" value=\"\" />&nbsp;$\n".
					"				</td>\n".
					"			</tr>\n".
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"s8_produits_services_reso_gestion_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"40%\">Produits</th>\n".
					"				<th width=\"60%\">Montant</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n".
					"			<tr>\n".
					"				<td>\n".
					"					Livre de minutes\n".
					"				</td>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s8_produits_services_reso_gestion_livre_minutes_montant\" id=\"s8_produits_services_reso_gestion_livre_minutes_montant\" value=\"\" readonly=\"readonly\" />&nbsp;$\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<td>\n".
					"					Livre de fiducie\n".
					"				</td>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s8_produits_services_reso_gestion_livre_fiducie_montant\" id=\"s8_produits_services_reso_gestion_livre_fiducie_montant\" value=\"\" readonly=\"readonly\" />&nbsp;$\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<td>\n".
					"					Services corporatifs<br />(feuilles de registres, certificats d’actions, etc)\n".
					"				</td>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s8_produits_services_reso_gestion_services_corpo_montant\" id=\"s8_produits_services_reso_gestion_service_corpo_montant\" value=\"\" readonly=\"readonly\" />&nbsp;$\n".
					"				</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<td>\n".
					"					Recherche de nom\n".
					"				</td>\n".
					"				<td>\n".
					"					<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s8_produits_services_reso_gestion_recherche_nom_montant\" id=\"s8_produits_services_reso_gestion_recherche_nom_montant\" value=\"\" readonly=\"readonly\" />&nbsp;$\n".
					"				</td>\n".
					"			</tr>\n".
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<div class=\"s8_produits_services_reso_gestion_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th width=\"40%\">Produits</th>\n".
							"				<th width=\"60%\">Montant</th>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n".
							"			<tr>\n".
							"				<td>\n".
							"					Livre de minutes\n".
							"				</td>\n".
							"				<td>\n".
							"					<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s8_produits_services_reso_gestion_livre_minutes_montant\" value=\"\" readonly=\"readonly\" />&nbsp;$\n".
							"				</td>\n".
							"			</tr>\n".
							"			<tr>\n".
							"				<td>\n".
							"					Livre de fiducie\n".
							"				</td>\n".
							"				<td>\n".
							"					<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s8_produits_services_reso_gestion_livre_fiducie_montant\" value=\"\" readonly=\"readonly\" />&nbsp;$\n".
							"				</td>\n".
							"			</tr>\n".
							"			<tr>\n".
							"				<td>\n".
							"					Services corporatifs<br />(feuilles de registres, certificats d’actions, etc)\n".
							"				</td>\n".
							"				<td>\n".
							"					<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s8_produits_services_reso_gestion_services_corpo_montant\" value=\"\" readonly=\"readonly\" />&nbsp;$\n".
							"				</td>\n".
							"			</tr>\n".
							"			<tr>\n".
							"				<td>\n".
							"					Recherche de nom\n".
							"				</td>\n".
							"				<td>\n".
							"					<input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s8_produits_services_reso_gestion_recherche_nom_montant\" value=\"\" readonly=\"readonly\" />&nbsp;$\n".
							"				</td>\n".
							"			</tr>\n".
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_satisfait_services_reso_gestion_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_satisfait_services_reso_gestion_bool\" id=\"s8_satisfait_services_reso_gestion_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_satisfait_services_reso_gestion_bool\" id=\"s8_satisfait_services_reso_gestion_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_satisfait_services_reso_gestion_bool\" id=\"s8_satisfait_services_reso_gestion_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_satisfait_services_reso_gestion_bool\" id=\"s8_satisfait_services_reso_gestion_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_satisfait_services_reso_gestion_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_satisfait_services_reso_gestion_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_satisfait_services_reso_gestion_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_satisfait_services_reso_gestion_sinon\" id=\"s8_satisfait_services_reso_gestion_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_satisfait_services_reso_gestion_sinon\" id=\"s8_satisfait_services_reso_gestion_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;	
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_satisfait_services_reso_gestion_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

	function s8_utilise_services_reso_gestion_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_utilise_services_reso_gestion_bool\" id=\"s8_utilise_services_reso_gestion_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_utilise_services_reso_gestion_bool\" id=\"s8_utilise_services_reso_gestion_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s8_utilise_services_reso_gestion_bool\" id=\"s8_utilise_services_reso_gestion_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s8_utilise_services_reso_gestion_bool\" id=\"s8_utilise_services_reso_gestion_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_utilise_services_reso_gestion_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s8_utilise_services_reso_gestion_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>
							</div>";
					}
				}
				break;
		}
		return $html;
	}
	
	function s8_utilise_services_reso_gestion_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_utilise_services_reso_gestion_sinon\" id=\"s8_utilise_services_reso_gestion_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s8_utilise_services_reso_gestion_sinon\" id=\"s8_utilise_services_reso_gestion_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s8_utilise_services_reso_gestion_sinon\" readonly=\"readonly\"></textarea>\n
							</div>";
					}
				}
				break;
		}
		return $html;
	}

?>