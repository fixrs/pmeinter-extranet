<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL;
		
		$_FORM =
			
			form_header($action).
			
			"			<label for=\"nom\">Nom de la cat&eacute;gorie de produit&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . nom($action) . "\n".
			"		</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html =
					"<form name=\"categories\" action=\"categories.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n".
					"	<input type=\"hidden\" name=\"categorie_key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "modify":
				$html =	
					"<form name=\"categories\" action=\"categories.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"categorie_key\" value=\"\" />\n".
					"	<input type=\"hidden\" name=\"key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "read":
				$html =
					"	<div class=\"readform\">\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "modify":
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "read":
				$html =
					"	</div>\n";
				break;
		}
		return $html;
	}

	function nom($action) {
		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"nom\" id=\"nom\" value=\"\" tabindex=\"20\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"nom\" id=\"nom\" value=\"\" tabindex=\"20\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_nom /HIDDEN_nom---></span><br/><br/>\n";
				break;
		}
		return $html;
	}
	
?>