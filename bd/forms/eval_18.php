<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "18").

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s18_greffe_corporatif\">Comment va votre greffe corporatif&nbsp;?</label>\n".
			"		" . s18_greffe_corporatif($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s18_greffe_corpo_temps_jour\">Combien de temps y consacrez-vous par semaine&nbsp;?</label>\n".
			"		" . s18_greffe_corpo_temps_jour($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s18_greffe_corpo_horaire_bool\">Est-ce syst&eacute;matique dans votre horaire&nbsp;?</label>\n".
			"		" . s18_greffe_corpo_horaire_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s18_greffe_corpo_horaire_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s18_greffe_corpo_horaire_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s18_contacts_clients_corpo_bool\">Avez-vous des contacts directement avec les clients corporatifs&nbsp;:</label>\n".
			"		" . s18_contacts_clients_corpo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s18_contacts_clients_corpo_sioui\">Si <b>OUI</b>, expliquez le type de contact&nbsp;:</label>\n".
			"		" . s18_contacts_clients_corpo_sioui($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s18_nb_clients_corpo\">Combien de clients corporatifs actifs avez-vous&nbsp;?</label>\n".
			"		" . s18_nb_clients_corpo($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s18_assistant_corpo_bool\">Utilisez-vous l'Assistant Corporatif&nbsp;?</label>\n".
			"		" . s18_assistant_corpo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s18_assistant_corpo_sioui_satisfait_bool\">Si <b>OUI</b>, est-ce que vous &ecirc;tes satisfaites&nbsp;?</label>\n".
			"		" . s18_assistant_corpo_sioui_satisfait_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s18_assistant_corpo_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label>\n".
			"		" . s18_assistant_corpo_sinon($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s18_assistant_corpo_non_utilise\">Si vous n'utilisez pas l'Assistant Corporatif, veuillez &eacute;num&eacute;rer les raisons&nbsp;:</label>\n".
			"		" . s18_assistant_corpo_non_utilise($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s18_formation_assistant_corpo_bool\">Est-ce qu'on vous a offert de suivre la formation sur l'Assistant Corporatif&nbsp;?</label>\n".
			"		" . s18_formation_assistant_corpo_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s18_formation_assistant_corpo_sinon_offerte\">Si <b>NON</b>, est-ce que vous aimeriez quelle vous soit offerte&nbsp;?</label>\n".
			"		" . s18_formation_assistant_corpo_sinon_offerte($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s18_connait_reso_gestion_bool\">Est-ce que vous connaissez les produits et services offerts par R&eacute;so Gestion&nbsp;?</label>\n".
			"		" . s18_connait_reso_gestion_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s18_utilise_reso_gestion_bool\">Est-ce que l'on vous encourage &agrave; utiliser les produits et services de R&eacute;so Gestion&nbsp;?</label>\n".
			"		" . s18_utilise_reso_gestion_bool($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		<label for=\"s18_utilise_reso_gestion_sinon\">Si <b>NON</b>, pourquoi&nbsp;?</label><br/>\n".
			"		" . s18_utilise_reso_gestion_sinon($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}

	function s18_greffe_corporatif($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_greffe_corporatif\" id=\"s18_greffe_corporatif\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_greffe_corporatif\" id=\"s18_greffe_corporatif\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s18_greffe_corporatif\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}
	
	function s18_greffe_corpo_temps_jour($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s18_greffe_corpo_temps_jour\" id=\"s18_greffe_corpo_temps_jour\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s18_greffe_corpo_temps_jour\" id=\"s18_greffe_corpo_temps_jour\" value=\"\" readonly=\"readonly\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"text\" class=\"text_short\" name=\"" . $compare_sid . "_s18_greffe_corpo_temps_jour\" value=\"\" readonly=\"readonly\" />\n";
					}
				}
				break;
		}
		return $html;
	}

	function s18_greffe_corpo_horaire_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_greffe_corpo_horaire_bool\" id=\"s18_greffe_corpo_horaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_greffe_corpo_horaire_bool\" id=\"s18_greffe_corpo_horaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_greffe_corpo_horaire_bool\" id=\"s18_greffe_corpo_horaire_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_greffe_corpo_horaire_bool\" id=\"s18_greffe_corpo_horaire_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_greffe_corpo_horaire_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_greffe_corpo_horaire_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s18_greffe_corpo_horaire_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_greffe_corpo_horaire_sinon\" id=\"s18_greffe_corpo_horaire_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_greffe_corpo_horaire_sinon\" id=\"s18_greffe_corpo_horaire_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s18_greffe_corpo_horaire_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s18_contacts_clients_corpo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_contacts_clients_corpo_bool\" id=\"s18_contacts_clients_corpo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_contacts_clients_corpo_bool\" id=\"s18_contacts_clients_corpo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_contacts_clients_corpo_bool\" id=\"s18_contacts_clients_corpo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_contacts_clients_corpo_bool\" id=\"s18_contacts_clients_corpo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_contacts_clients_corpo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_contacts_clients_corpo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s18_contacts_clients_corpo_sioui($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_contacts_clients_corpo_sioui\" id=\"s18_contacts_clients_corpo_sioui\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_contacts_clients_corpo_sioui\" id=\"s18_contacts_clients_corpo_sioui\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s18_contacts_clients_corpo_sioui\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s18_nb_clients_corpo($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s18_nb_clients_corpo\" id=\"s18_nb_clients_corpo\" value=\"\" />\n";
				break;

			case "read":
				$html .=
					"<input type=\"text\" class=\"text_short\" name=\"s18_nb_clients_corpo\" id=\"s18_nb_clients_corpo\" value=\"\" />\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s18_nb_clients_corpo\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s18_assistant_corpo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_assistant_corpo_bool\" id=\"s18_assistant_corpo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_assistant_corpo_bool\" id=\"s18_assistant_corpo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_assistant_corpo_bool\" id=\"s18_assistant_corpo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_assistant_corpo_bool\" id=\"s18_assistant_corpo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_assistant_corpo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_assistant_corpo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s18_assistant_corpo_sioui_satisfait_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_assistant_corpo_sioui_satisfait_bool\" id=\"s18_assistant_corpo_sioui_satisfait_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_assistant_corpo_sioui_satisfait_bool\" id=\"s18_assistant_corpo_sioui_satisfait_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_assistant_corpo_sioui_satisfait_bool\" id=\"s18_assistant_corpo_sioui_satisfait_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_assistant_corpo_sioui_satisfait_bool\" id=\"s18_assistant_corpo_sioui_satisfait_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_assistant_corpo_sioui_satisfait_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_assistant_corpo_sioui_satisfait_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s18_assistant_corpo_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_assistant_corpo_sinon\" id=\"s18_assistant_corpo_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_assistant_corpo_sinon\" id=\"s18_assistant_corpo_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s18_assistant_corpo_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s18_assistant_corpo_non_utilise($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_assistant_corpo_non_utilise\" id=\"s18_assistant_corpo_non_utilise\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_assistant_corpo_non_utilise\" id=\"s18_assistant_corpo_non_utilise\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s18_assistant_corpo_non_utilise\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s18_formation_assistant_corpo_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_formation_assistant_corpo_bool\" id=\"s18_formation_assistant_corpo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_formation_assistant_corpo_bool\" id=\"s18_formation_assistant_corpo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_formation_assistant_corpo_bool\" id=\"s18_formation_assistant_corpo_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_formation_assistant_corpo_bool\" id=\"s18_formation_assistant_corpo_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_formation_assistant_corpo_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_formation_assistant_corpo_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s18_formation_assistant_corpo_sinon_offerte($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_formation_assistant_corpo_sinon_offerte\" id=\"s18_formation_assistant_corpo_sinon_offerte_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_formation_assistant_corpo_sinon_offerte\" id=\"s18_formation_assistant_corpo_sinon_offerte_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_formation_assistant_corpo_sinon_offerte\" id=\"s14_refere_clients_cabinet_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_formation_assistant_corpo_sinon_offerte\" id=\"s14_refere_clients_cabinet_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_formation_assistant_corpo_sinon_offerte\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_formation_assistant_corpo_sinon_offerte\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}
	
	function s18_connait_reso_gestion_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_connait_reso_gestion_bool\" id=\"s18_connait_reso_gestion_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_connait_reso_gestion_bool\" id=\"s18_connait_reso_gestion_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_connait_reso_gestion_bool\" id=\"s18_connait_reso_gestion_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_connait_reso_gestion_bool\" id=\"s18_connait_reso_gestion_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_connait_reso_gestion_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_connait_reso_gestion_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}
	
	function s18_utilise_reso_gestion_bool($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_utilise_reso_gestion_bool\" id=\"s18_utilise_reso_gestion_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_utilise_reso_gestion_bool\" id=\"s18_utilise_reso_gestion_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "read":
				$html .=
					"<input type=\"radio\" class=\"radio\" name=\"s18_utilise_reso_gestion_bool\" id=\"s18_utilise_reso_gestion_bool_1\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
					"<input type=\"radio\" class=\"radio\" name=\"s18_utilise_reso_gestion_bool\" id=\"s18_utilise_reso_gestion_bool_0\" value=\"0\" /><span class=\"radio_label\">Non</span>";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_utilise_reso_gestion_bool\" value=\"1\" /><span class=\"radio_label\">Oui</span>".
							"<input type=\"radio\" class=\"radio\" name=\"" . $compare_sid . "_s18_utilise_reso_gestion_bool\" value=\"0\" /><span class=\"radio_label\">Non</span>";
					}
				}
				break;
		}
		return $html;
	}

	function s18_utilise_reso_gestion_sinon($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_utilise_reso_gestion_sinon\" id=\"s18_utilise_reso_gestion_sinon\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"4\" cols=\"40\" name=\"s18_utilise_reso_gestion_sinon\" id=\"s18_utilise_reso_gestion_sinon\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$html .=
							"<textarea rows=\"4\" cols=\"40\" name=\"" . $compare_sid . "_s18_utilise_reso_gestion_sinon\" readonly=\"readonly\"></textarea>\n";
					}
				}
				break;
		}
		return $html;
	}
	
?>