<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL, $sid;
		$parameters = getParametersFromSession($sid);

		$_FORM =

			form_header($action, "6").
			
			"	<div class=\"entry_long\">\n".
			"		<label for=\"s6_pourcent_droit_affaires\">Veuillez indiquer en % de votre chiffre d'affaires, les activit&eacute;s occup&eacute;es par les trois domaines d'affaires au sein de l'&eacute;tude pour l'ann&eacute;e derni&egrave;re et les pr&eacute;visions pour l'ann&eacute;e en cours&nbsp;:</label>\n".
			"		" . s6_pourcent_droit_affaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<span>Indiquer les activit&eacute;s couvertes pour chaque domaine d'affaires&nbsp;:</span><br />\n".
			"	<br />\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s6_activites_droit_affaires\">Droit des affaires&nbsp;:</label>\n".
			"		" . s6_activites_droit_affaires($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s6_activites_droit_personnes\">Droit des personnes&nbsp;:</label>\n".
			"		" . s6_activites_droit_personnes($action, $parameters) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		<label for=\"s6_activites_droit_immobilier\">Droit immobilier&nbsp;:</label>\n".
			"		" . s6_activites_droit_immobilier($action, $parameters) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}
		
	function s6_pourcent_droit_affaires($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<div class=\"s6_pourcent_droit_affaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"30%\">&nbsp;</th>\n".
					"				<th width=\"35%\">Ann&eacute;e pr&eacute;c&eacute;dente (" . annee(-1) . ")</th>\n".
					"				<th width=\"35%\">Pr&eacute;vision pour cette ann&eacute;e (" . annee(0) . ")</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n".
					"			<tr>\n".
					"				<th>Droit des affaires</th>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_affaires_an_precedent\" id=\"s6_pourcent_droit_affaires_an_precedent\" value=\"\" />&nbsp;%</td>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_affaires_prev_an_actuel\" id=\"s6_pourcent_droit_affaires_prev_an_actuel\" value=\"\" />&nbsp;%</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>Droit des personnes</th>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_personnes_an_precedent\" id=\"s6_pourcent_droit_personnes_an_precedent\" value=\"\" />&nbsp;%</td>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_personnes_prev_an_actuel\" id=\"s6_pourcent_droit_personnes_prev_an_actuel\" value=\"\" />&nbsp;%</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>Droit immobilier</th>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_immobilier_prev_an_actuel\" id=\"s6_pourcent_droit_immobilier_prev_an_actuel\" value=\"\" />&nbsp;%</td>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_immobilier_an_precedent\" id=\"s6_pourcent_droit_immobilier_an_precedent\" value=\"\" />&nbsp;%</td>\n".
					"			</tr>\n".
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<div class=\"s6_pourcent_droit_affaires_entry\">\n".
					"	<table>\n".
					"		<thead>\n".
					"			<tr>\n".
					"				<th width=\"30%\">&nbsp;</th>\n".
					"				<th width=\"35%\">Ann&eacute;e pr&eacute;c&eacute;dente (" . annee(-1) . ")</th>\n".
					"				<th width=\"35%\">Pr&eacute;vision pour cette ann&eacute;e (" . annee(0) . ")</th>\n".
					"			</tr>\n".
					"		</thead>\n".
					"		<tbody>\n".
					"			<tr>\n".
					"				<th>Droit des affaires</th>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_affaires_an_precedent\" id=\"s6_pourcent_droit_affaires_an_precedent\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_affaires_prev_an_actuel\" id=\"s6_pourcent_droit_affaires_prev_an_actuel\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>Droit des personnes</th>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_personnes_an_precedent\" id=\"s6_pourcent_droit_personnes_an_precedent\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_personnes_prev_an_actuel\" id=\"s6_pourcent_droit_personnes_prev_an_actuel\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
					"			</tr>\n".
					"			<tr>\n".
					"				<th>Droit immobilier</th>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_immobilier_prev_an_actuel\" id=\"s6_pourcent_droit_immobilier_prev_an_actuel\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
					"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"s6_pourcent_droit_immobilier_an_precedent\" id=\"s6_pourcent_droit_immobilier_an_precedent\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
					"			</tr>\n".
					"		</tbody>\n".
					"	</table>\n".
					"</div>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"	<div class=\"s6_pourcent_droit_affaires_entry\">\n".
							"	<table>\n".
							"		<thead>\n".
							"			<tr>\n".
							"				<th width=\"30%\">&nbsp;</th>\n".
							"				<th width=\"35%\">Ann&eacute;e pr&eacute;c&eacute;dente (" . annee(-1, "compare", $compare_sid) . ")</th>\n".
							"				<th width=\"35%\">Pr&eacute;vision pour cette ann&eacute;e (" . annee(0, "compare", $compare_sid) . ")</th>\n".
							"			</tr>\n".
							"		</thead>\n".
							"		<tbody>\n".
							"			<tr>\n".
							"				<th>Droit des affaires</th>\n".
							"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s6_pourcent_droit_affaires_an_precedent\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
							"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s6_pourcent_droit_affaires_prev_an_actuel\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
							"			</tr>\n".
							"			<tr>\n".
							"				<th>Droit des personnes</th>\n".
							"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s6_pourcent_droit_personnes_an_precedent\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
							"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s6_pourcent_droit_personnes_prev_an_actuel\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
							"			</tr>\n".
							"			<tr>\n".
							"				<th>Droit immobilier</th>\n".
							"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s6_pourcent_droit_immobilier_prev_an_actuel\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
							"				<td><input type=\"text\" class=\"text\" style=\"width: 40px; text-align: right;\" name=\"" . $compare_sid . "_s6_pourcent_droit_immobilier_an_precedent\" value=\"\" readonly=\"readonly\" />&nbsp;%</td>\n".
							"			</tr>\n".
							"		</tbody>\n".
							"	</table>\n".
							"</div>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s6_activites_droit_affaires($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"6\" name=\"s6_activites_droit_affaires\" id=\"s6_activites_droit_affaires\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"6\" name=\"s6_activites_droit_affaires\" id=\"s6_activites_droit_affaires\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"6\" name=\"" . $compare_sid . "_s6_activites_droit_affaires\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s6_activites_droit_personnes($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"6\" name=\"s6_activites_droit_personnes\" id=\"s6_activites_droit_personnes\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"6\" name=\"s6_activites_droit_personnes\" id=\"s6_activites_droit_personnes\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"6\" name=\"" . $compare_sid . "_s6_activites_droit_personnes\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

	function s6_activites_droit_immobilier($action, $parameters) {
		$html = "";
		switch ($action) {	
			case "modify":
				$html .=
					"<textarea rows=\"6\" name=\"s6_activites_droit_immobilier\" id=\"s6_activites_droit_immobilier\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<textarea rows=\"6\" name=\"s6_activites_droit_immobilier\" id=\"s6_activites_droit_immobilier\" readonly=\"readonly\"></textarea>\n";
				break;

			case "compare":
				if (is_array($parameters) && count($parameters) > 0) {
					krsort($parameters);
					$compare_color = 0;
					foreach ($parameters as $compare_sid => $compare_parameters) {
						$compare_color++;
						$html .=
							"<div class=\"answer_compare_" . $compare_color . "\">\n".
							"<textarea rows=\"6\" name=\"" . $compare_sid . "_s6_activites_droit_immobilier\" readonly=\"readonly\"></textarea>\n".
							"</div>\n";
					}
				}
				break;
		}
		return $html;
	}

?>