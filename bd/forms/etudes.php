<?PHP

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL;

		$_FORM =

			form_header($action).

			lastmod($action).

			"	<div class=\"entry\">\n".
			"		" . nom($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . nombre_employes($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . nombre_notaires_associes($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . nombre_notaires_salaries($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . courriel($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . url_site_web($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . banner($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . facebook($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . linkedin($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . twitter($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . intro_services($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . droit_affaires($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . droit_personne($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . droit_immobilier($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . droit_agricole($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . new_droit($action) . "\n".
			"	</div>\n".


			"	<div class=\"entry_long\">\n".
			"		" . description($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . meta_description($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . image($action) . "\n".
			"	</div>\n".


			"	<div class=\"entry_long\">\n".
			"		" . image_description($action) . "\n".
			"	</div>\n".




			"	<div class=\"entry\">\n".
			"		" . employes_nom($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry\">\n".
			"		" . etudes_succursales_nom($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . ouverture($action) . "\n".
			"	</div>\n".

			"	<div class=\"entry_long\">\n".
			"		" . paiement($action) . "\n".
			"	</div>\n".

			form_footer($action);

		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<form name=\"etudes\" action=\"etudes.php\" method=\"post\" enctype=\"multipart/form-data\">\n".
#					"	<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"" . $ETUDES_IMG_MAX_SIZE . "\" />\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n";
				break;
			case "modify":
				$html .=
					"<form name=\"etudes\" action=\"etudes.php\" method=\"post\" enctype=\"multipart/form-data\">\n".
#					"	<input type=\"hidden\" name=\"MAX_FILE_SIZE\" value=\"" . $ETUDES_IMG_MAX_SIZE . "\" />\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"key\" value=\"\" />\n".
					"	<input type=\"hidden\" name=\"nom\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<div class=\"readform\">\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					//"	<input type=\"submit\" style=\"display: none;\" />\n".					onclick=\"javascript: hta_description.update(); document.etudes.submit();\"
					"	<input type=\"submit\" class=\"submit\" value=\"Soumettre\"  />\n".
					"</form>\n";
				break;
			case "modify":
				$html .=
					//"	<input type=\"submit\" style=\"display: none;\" />\n".			onclick=\"javascript: hta_description.update(); document.etudes.submit();\"
					"	<input type=\"submit\" class=\"submit\" value=\"Soumettre\"  />\n".
					"</form>\n";
				break;
			case "read":
				$html .=
					"</div>\n";
				break;
		}
		return $html;
	}


	function lastmod($action) {
		$html = "";
		switch ($action) {
			case "modify":
				$html .=
					"<div class=\"entry\">\n".
					"	<label for=\"lastmod\">Date de la derni&egrave;re modification&nbsp;:</label>\n".
					"	<input type=\"text\" class=\"text\" id=\"lastmod\" name=\"lastmod\" value=\"\" readonly=\"readonly\"/>\n".
					"</div>\n";
				break;
			case "read":
				$html .=
					"<div class=\"entry\">\n".
					"	<label>Date de la derni&egrave;re modification&nbsp;:</label>\n".
					"	<span class=\"answer\"><!---HIDDEN_lastmod /HIDDEN_lastmod---></span>\n".
					"</div>\n";
				break;
		}
		return $html;
	}

	function nom($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"nom\">Nom de l'&eacute;tude&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"nom\" name=\"nom\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"newnom\">Nom de l'&eacute;tude&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"newnom\" name=\"newnom\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Nom de l'&eacute;tude&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_nom /HIDDEN_nom---></span>";
				break;
		}
		return $html;
	}

	function nombre_employes($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"nombre_employes\">Nombre d'employ&eacute;s de l'&eacute;tude&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text_short\" id=\"nombre_employes\" name=\"nombre_employes\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"nombre_employes\">Nombre d'employ&eacute;s de l'&eacute;tude&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text_short\" id=\"nombre_employes\" name=\"nombre_employes\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Nombre d'employ&eacute;s de l'&eacute;tude&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_nombre_employes /HIDDEN_nombre_employes---></span>";
				break;
		}
		return $html;
	}

	function nombre_notaires_associes($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"nombre_employes\">Nombre de notaires associ&eacute;s&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text_short\" id=\"nombre_notaires_associes\" name=\"nombre_notaires_associes\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"nombre_employes\">Nombre de notaires associ&eacute;s&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text_short\" id=\"nombre_notaires_associes\" name=\"nombre_notaires_associes\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Nombre de notaires associ&eacute;s&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_nombre_notaires_associes /HIDDEN_nombre_notaires_associes---></span>";
				break;
		}
		return $html;
	}

	function nombre_notaires_salaries($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"nombre_employes\">Nombre de notaires salari&eacute;s&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text_short\" id=\"nombre_notaires_salaries\" name=\"nombre_notaires_salaries\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"nombre_employes\">Nombre de notaires salari&eacute;s&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text_short\" id=\"nombre_notaires_salaries\" name=\"nombre_notaires_salaries\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Nombre de notaires salari&eacute;s&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_nombre_notaires_salaries /HIDDEN_nombre_notaires_salaries---></span>";
				break;
		}
		return $html;
	}

	function courriel($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"courriel\">Courriel de l'&eacute;tude&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"courriel\" name=\"courriel\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"courriel\">Courriel de l'&eacute;tude&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"courriel\" name=\"courriel\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Courriel de l'&eacute;tude&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_courriel /HIDDEN_courriel---></span>";
				break;
		}
		return $html;
	}

	function url_site_web($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"url\">Adresse du site web de l'&eacute;tude&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"url_site_web\" name=\"url_site_web\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"url\">Adresse du site web de l'&eacute;tude&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"url_site_web\" name=\"url_site_web\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Adresse du site web de l'&eacute;tude&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_url_site_web /HIDDEN_url_site_web---></span>";
				break;
		}
		return $html;
	}

	function facebook($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"facebook\">Page Facebook&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"facebook\" name=\"facebook\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"facebook\">Page Facebook&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"facebook\" name=\"facebook\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Page Facebook&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_facebook /HIDDEN_facebook---></span>";
				break;
		}
		return $html;
	}

	function linkedin($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"linkedin\">Page LinkedIn&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"linkedin\" name=\"facebook\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"linkedin\">Page LinkedIn&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"linkedin\" name=\"linkedin\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Page LinkedIn&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_linkedin /HIDDEN_linkedin---></span>";
				break;
		}
		return $html;
	}

	function twitter($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"twitter\">Page Twitter&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"twitter\" name=\"twitter\" value=\"\" />\n";
				break;
			case "modify":
				$html .=
					"<label for=\"twitter\">Page Twitter&nbsp;:</label>\n".
					"<input type=\"text\" class=\"text\" id=\"twitter\" name=\"twitter\" value=\"\" />\n";
				break;
			case "read":
				$html .=
					"<label>Page Twitter&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_twitter /HIDDEN_twitter---></span>";
				break;
		}
		return $html;
	}


	function ouverture($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"heures_ouverture\">Heures d'ouverture&nbsp;:</label>\n".
					"<textarea id=\"heures_ouverture\" name=\"heures_ouverture\"></textarea>\n";
				break;
			case "modify":
				$html .=
					"<label for=\"heures_ouverture\">Heures d'ouverture&nbsp;:</label>\n".
					"<textarea id=\"heures_ouverture\" name=\"heures_ouverture\"></textarea>\n";
				break;
			case "read":
				$html .=
					"<label>Heures d'ouverture&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_heures_ouverture /HIDDEN_heures_ouverture---></span>";
				break;
		}
		return $html;
	}

	function paiement($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"mode_paiement\">Modes de paiement&nbsp;:</label>\n".
					"<textarea id=\"mode_paiement\" name=\"mode_paiement\"></textarea>\n";
				break;
			case "modify":
				$html .=
					"<label for=\"mode_paiement\">Modes de paiement&nbsp;:</label>\n".
					"<textarea id=\"mode_paiement\" name=\"mode_paiement\"></textarea>\n";
				break;
			case "read":
				$html .=
					"<label>Modes de paiement&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_mode_paiement /HIDDEN_mode_paiement---></span>";
				break;
		}
		return $html;
	}

	function meta_description($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"meta_description\">Meta Description&nbsp;:</label>\n".
					"<textarea id=\"meta_description\" name=\"meta_description\"></textarea>\n";
				break;
			case "modify":
				$html .=
					"<label for=\"meta_description\">Meta Description&nbsp;:</label>\n".
					"<textarea id=\"meta_description\" name=\"meta_description\"></textarea>\n";
				break;
			case "read":
				$html .=
					"<label>Meta Description&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_meta_description /HIDDEN_meta_description---></span>";
				break;
		}
		return $html;
	}

	function intro_services($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"intro_services\">Introduction services&nbsp;:</label>\n".
					"<textarea id=\"intro_services\" name=\"intro_services\"></textarea>\n";
				break;
			case "modify":
				$html .=
					"<label for=\"intro_services\">Introduction services&nbsp;:</label>\n".
					"<textarea id=\"intro_services\" name=\"intro_services\"></textarea>\n";
				break;
			case "read":
				$html .=
					"<label>Introduction services&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_intro_services /HIDDEN_intro_services---></span>";
				break;
		}
		return $html;
	}

	function droit_affaires($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<input id=\"droit_affaires_oui\" name=\"droit_affaires_oui\" type=\"checkbox\" value=\"1\" /> Afficher?<br />\n".
					"<label for=\"droit_affaires_desc\">Description du droit des affaires&nbsp;:</label>\n".
					"<textarea id=\"droit_affaires_desc\" name=\"droit_affaires_desc\"></textarea>\n";
				break;
			case "modify":
				$html .=
					"<input id=\"droit_affaires_oui\" name=\"droit_affaires_oui\" type=\"checkbox\" value=\"1\" /> Afficher?<br />\n".
					"<label for=\"droit_affaires_desc\">Description du droit des affaires&nbsp;:</label>\n".
					"<textarea id=\"droit_affaires_desc\" name=\"droit_affaires_desc\"></textarea>\n";
				break;
			case "read":
				$html .=
					"<label>Description du droit des affaires&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_droit_affaires_desc /HIDDEN_droit_affaires_desc---></span>";
				break;
		}
		return $html;
	}

	function droit_personne($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<input id=\"droit_personne_oui\" name=\"droit_personne_oui\" type=\"checkbox\" value=\"1\" /> Afficher?<br />\n".
					"<label for=\"droit_personne_desc\">Description du droit de la personne&nbsp;:</label>\n".
					"<textarea id=\"droit_personne_desc\" name=\"droit_personne_desc\"></textarea>\n";
				break;
			case "modify":
				$html .=
					"<input id=\"droit_personne_oui\" name=\"droit_personne_oui\" type=\"checkbox\" value=\"1\" /> Afficher?<br />\n".
					"<label for=\"droit_personne_desc\">Description du droit de la personne&nbsp;:</label>\n".
					"<textarea id=\"droit_personne_desc\" name=\"droit_personne_desc\"></textarea>\n";
				break;
			case "read":
				$html .=
					"<label>Description du droit de la personne&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_droit_personne_desc /HIDDEN_droit_personne_desc---></span>";
				break;
		}
		return $html;
	}

	function droit_immobilier($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<input id=\"droit_immobilier_oui\" name=\"droit_immobilier_oui\" type=\"checkbox\" value=\"1\" /> Afficher?<br />\n".
					"<label for=\"droit_immobilier_desc\">Description du droit immobilier&nbsp;:</label>\n".
					"<textarea id=\"droit_immobilier_desc\" name=\"droit_immobilier_desc\"></textarea>\n";
				break;
			case "modify":
				$html .=
					"<input id=\"droit_immobilier_oui\" name=\"droit_immobilier_oui\" type=\"checkbox\" value=\"1\" /> Afficher?<br />\n".
					"<label for=\"droit_immobilier_desc\">Description du droit immobilier&nbsp;:</label>\n".
					"<textarea id=\"droit_immobilier_desc\" name=\"droit_immobilier_desc\"></textarea>\n";
				break;
			case "read":
				$html .=
					"<label>Description du droit immobilier&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_droit_immobilier_desc /HIDDEN_droit_immobilier_desc---></span>";
				break;
		}
		return $html;
	}

	function droit_agricole($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<input id=\"droit_agricole_oui\" name=\"droit_agricole_oui\" type=\"checkbox\" value=\"1\" /> Afficher?<br />\n".
					"<label for=\"droit_agricole_desc\">Description du droit agricole&nbsp;:</label>\n".
					"<textarea id=\"droit_agricole_desc\" name=\"droit_agricole_desc\"></textarea>\n";
				break;
			case "modify":
				$html .=
					"<input id=\"droit_agricole_oui\" name=\"droit_agricole_oui\" type=\"checkbox\" value=\"1\" /> Afficher?<br />\n".
					"<label for=\"droit_agricole_desc\">Description du droit agricole&nbsp;:</label>\n".
					"<textarea id=\"droit_agricole_desc\" name=\"droit_agricole_desc\"></textarea>\n";
				break;
			case "read":
				$html .=
					"<label>Description du droit agricole&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_droit_agricole_desc /HIDDEN_droit_agricole_desc---></span>";
				break;
		}
		return $html;
	}

	function new_droit($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"new_service_title\">Titre nouveau service&nbsp;:</label>\n".
					"<input id=\"new_service_title\" name=\"new_service_title\" type=\"text\" value=\"\" />\n<br />".
					"<label for=\"new_service_description\">Description nouveau service&nbsp;:</label>\n".
					"<textarea id=\"new_service_description\" name=\"new_service_description\"></textarea>\n";
				break;
			case "modify":
				$html .=
					"<label for=\"new_service_title\">Titre nouveau service&nbsp;:</label>\n".
					"<input id=\"new_service_title\" name=\"new_service_title\" type=\"text\" value=\"\" />\n<br />".
					"<label for=\"new_service_description\">Description nouveau service&nbsp;:</label>\n".
					"<textarea id=\"new_service_description\" name=\"new_service_description\"></textarea>\n";
				break;
			case "read":
				$html .=
					"<label>Nouveau service&nbsp;:</label>\n".
					"<span class=\"answer\"><!---HIDDEN_new_service_title /HIDDEN_new_service_title---><br /><!---HIDDEN_new_service_description /HIDDEN_new_service_description---></span>";
				break;
		}
		return $html;
	}


	function description($action) {
		global $DB;
		$key = getorpost('key');
		$html = "";

		switch ($action) {
			case "add":
				$description = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('description')))));
				$description = getorpost('description');

				$html .=
					"<label for=\"description\">Description de l'&eacute;tude&nbsp;:</label>\n".
					// "<div id=\"hta_description_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_description = new HyperTextArea('etudes', 'description', '" . $description . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					"<textarea name=\"description\" id=\"description\" rows=\"15\">" . $description . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { $('#description').htmlarea(); }); \n".
					"</script>\n".

					"<br clear=\"all\" />\n";
					//"<span class=\"hta_chrcount\"><span id=\"description_chrcount\" class=\"description_chrcount\">0</span>&nbsp;/&nbsp;1750 caract&egrave;res maximum.</span>\n".
					//"<span style=\"float:right\";><input type=\"button\" class=\"hta_button\" value=\"Actualiser le nb de caract&egrave;res\" onclick=\"javascript: hta_description.updateChrCount();\" />&nbsp;&nbsp;<input type=\"button\" class=\"hta_button\" value=\"Tout effacer\" onclick=\"javascript: hta_description.clearText(); hta_description.updateChrCount();\" /></span>\n".
					//"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n";
				break;

			case "modify":
				$description = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('description')))));
				$description = getorpost('description');

				if ($description == "") {
					$DB->query(
						"SELECT `description` ".
						"FROM `etudes` ".
						"WHERE `key` = " . addslashes($key) . ";"
					);
					while ($DB->next_record()) {
						$description = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes($DB->getField('description')))));
						$description = $DB->getField('description');
					}
				}

				$html .=
					"<label>Description de l'&eacute;tude&nbsp;:</label>\n".
					// "<div id=\"hta_description_div\" class=\"hta_rw\">\n".
					// "	<script language=\"javaScript\" type=\"text/javascript\">\n".
					// "		hta_description = new HyperTextArea('etudes', 'description', '" . $description . "', 582, 300, '');\n".
					// "		chrcount(undefined);\n".
					// "	</script>\n".
					// "</div>\n".
					"<textarea name=\"description\" id=\"description\" rows=\"15\">" . $description . "</textarea>\n".
					"<script language=\"javaScript\" type=\"text/javascript\">\n".
					"	jQuery(document).ready(function($) { $('#description').htmlarea(); }); \n".
					"</script>\n".

					"<br clear=\"all\" />\n";
					//"<span class=\"hta_chrcount\"><span id=\"description_chrcount\" class=\"description_chrcount\">0</span>&nbsp;/&nbsp;1750 caract&egrave;res maximum.</span>\n".
					//"<span style=\"float:right\";><input type=\"button\" class=\"hta_button\" value=\"Actualiser le nb de caract&egrave;res\" onclick=\"javascript: hta_description.updateChrCount();\" />&nbsp;&nbsp;<input type=\"button\" class=\"hta_button\" value=\"Tout effacer\" onclick=\"javascript: hta_description.clearText(); hta_description.updateChrCount();\" /></span>\n".
					//"<br clear=\"all\" />\n".
					//"<div class=\"notice ie7fix\"><strong>Si vous utilisez Internet Explorer&nbsp;:</strong><br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez sur les touches <strong>&lt;Majuscule&gt;</strong> et <strong>&lt;Entr&eacute;e&gt;</strong> pour effectuer un <strong>retour de chariot</strong>.<br /><strong>&nbsp;&#149&nbsp;</strong>Appuyez uniquement sur la touche <strong>&lt;Entr&eacute;e&gt;</strong> pour cr&eacute;er un <strong>paragraphe</strong>.</div>\n";
				break;

			case "read":
				$html .=
					"<label>Description de l'&eacute;tude&nbsp;:</label>\n".
					"<div class=\"hta_ro\"><!---HIDDEN_description /HIDDEN_description---></div>\n";
				break;
		}
		return $html;
	}

	function image($action) {
		global $DB, $BASEURL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"image\">Photo de l'&eacute;quipe &agrave; activer sur le site&nbsp;:</label>\n".
					"<input type=\"file\" class=\"file\" name=\"image_up\" id=\"image_up\" /><br />\n";
					"<div class=\"notice\">Le format doit &ecirc;tre <strong>JPG</strong>, <strong>JPEG</strong>, <strong>GIF</strong> ou <strong>PNG</strong>.<br />Les dimensions doivent &ecirc;tre <strong>296</strong> par <strong>182</strong> pixels.</div>\n";
				break;

			case "modify":
				$key = getorpost('key');
				$image = "";
				$DB->query(
					"SELECT `image` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $key . "';"
				);
				while ($DB->next_record()) {
					$image = $DB->getField("image");
				}

				$html .=
					"<label for=\"image\">Photo de l'&eacute;quipe &agrave; activer sur le site&nbsp;:</label>\n".
					"<input type=\"hidden\" name=\"image\" id=\"image\" value=\"\" />\n".
					"<input type=\"file\" class=\"file\" name=\"image_up\" id=\"image_up\" /><br />\n".
					"<div class=\"notice\">Le format doit &ecirc;tre <strong>JPG</strong>, <strong>JPEG</strong>, <strong>GIF</strong> ou <strong>PNG</strong>.<br />Les dimensions doivent &ecirc;tre <strong>296</strong> par <strong>182</strong> pixels.</div>\n";

				if ($image != "") {
					$html .=
						"<br />\n".
						"Image pr&eacute;sentement dans les registres&nbsp;:<br />\n".
						"<br />\n".
						"<img class=\"answer\" src=\"" . $BASEURL . "docs/etudes_img/" . $image . "\" alt=\"photo de l'&eacute;quipe\" width=\"500\" /><br />\n".
						"<br />\n".
						"<input type=\"checkbox\" class=\"checkbox\" name=\"image_del\" id=\"image_del\" value=\"1\" /><span class=\"checkbox_label\">Supprimer l'image&nbsp;?</span>\n";
				} else {
					$html .=
						"<br />\n".
						"Aucune image n'est d&eacute;j&agrave; incluse.\n";
				}
				break;

			case "read":
				$key = getorpost('key');
				$image = "";
				$html .=
					"<label>Photo de l'&eacute;quipe&nbsp;:</label>\n";
				$DB->query(
					"SELECT `image` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $key . "';"
				);
				while ($DB->next_record()) {
					$image = $DB->getField("image");
				}
				if ($image != "") {
					$html .=
						"<img class=\"answer\" src=\"" . $BASEURL . "docs/etudes_img/<!---HIDDEN_image /HIDDEN_image--->\" alt=\"photo de l'&eacute;quipe\" />\n";
				} else {
					$html .=
						"<span class=\"answer\">(aucune)</span>\n";
				}
				break;
		}
		return $html;
	}


	function banner($action) {
		global $DB, $BASEURL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"banner\">Banniere page Web&nbsp;:</label>\n".
					"<input type=\"file\" class=\"file\" name=\"banner_up\" id=\"banner_up\" /><br />\n";
					"<div class=\"notice\">Le format doit &ecirc;tre <strong>JPG</strong>, <strong>JPEG</strong> ou <strong>PNG</strong>.</div>\n";
				break;

			case "modify":
				$key = getorpost('key');
				$banner = "";
				$DB->query(
					"SELECT `banner` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $key . "';"
				);
				while ($DB->next_record()) {
					$banner = $DB->getField("banner");
				}

				$html .=
					"<label for=\"banner\">Banniere page Web&nbsp;:</label>\n".
					"<input type=\"hidden\" name=\"banner\" id=\"banner\" value=\"\" />\n".
					"<input type=\"file\" class=\"file\" name=\"banner_up\" id=\"banner_up\" /><br />\n".
					"<div class=\"notice\">Le format doit &ecirc;tre <strong>JPG</strong>, <strong>JPEG</strong> ou <strong>PNG</strong>.</div>\n";

				if ($banner != "") {
					$html .=
						"<br />\n".
						"Image pr&eacute;sentement dans les registres&nbsp;:<br />\n".
						"<br />\n".
						"<img class=\"answer\" src=\"" . $BASEURL . "docs/etudes_img/" . $banner . "\" alt=\"\" width=\"500\" /><br />\n".
						"<br />\n".
						"<input type=\"checkbox\" class=\"checkbox\" name=\"banner_del\" id=\"banner_del\" value=\"1\" /><span class=\"checkbox_label\">Supprimer l'image&nbsp;?</span>\n";
				} else {
					$html .=
						"<br />\n".
						"Aucune image n'est d&eacute;j&agrave; incluse.\n";
				}
				break;

			case "read":
				$key = getorpost('key');
				$banner = "";
				$html .=
					"<label>Banniere page Web&nbsp;:</label>\n";
				$DB->query(
					"SELECT `banner` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $key . "';"
				);
				while ($DB->next_record()) {
					$banner = $DB->getField("banner");
				}
				if ($banner != "") {
					$html .=
						"<img class=\"answer\" src=\"" . $BASEURL . "docs/etudes_img/<!---HIDDEN_banner /HIDDEN_banner--->\" alt=\"\" />\n";
				} else {
					$html .=
						"<span class=\"answer\">(aucune)</span>\n";
				}
				break;
		}
		return $html;
	}

/*	function image_description($action) {
		$html = "";
		switch ($action) {
			case "add":
				$image_description = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes(getorpost('image_description')))));

				$html .=
					"<label for=\"image_description\">La description de l'image&nbsp;:</label>\n".
					"<div class=\"hta_rw\">\n".
					"	<script language=\"javaScript\" type=\"text/javascript\">\n".
					"		hta_image_description = new HyperTextArea('etudes', 'image_description', '" . $image_description . "', 582, 300, '');\n".
					"	</script>\n".
					"</div>\n";
				break;

			case "modify":
				global $DB;

				$DB->query(
					"SELECT `image_description` ".
					"FROM `etudes` ".
					"WHERE `key` = " . getorpost('key') . ";"
				);
				while ($DB->next_record()) {
					$image_description = trim(str_replace(array("\n", "\r", "\t"), array("", "", ""), addslashes(stripslashes($DB->getField('image_description')))));
				}

				$html .=
					"<label for=\"image_description\">La description de l'image (max: 250 mots)&nbsp;:</label>\n".
					"<div class=\"hta_rw\">\n".
					"	<script language=\"javaScript\" type=\"text/javascript\">\n".
					"		hta_image_description = new HyperTextArea('etudes', 'image_description', '" . $image_description . "', 582, 300, '');\n".
					"	</script>\n".
					"</div>\n";
				break;

			case "read":
				$html .=
					"<label>Description de l'image&nbsp;:</label>\n".
					"<span class=\"hta_ro\"><!---HIDDEN_image_description /HIDDEN_image_description---></span>\n";
				break;
		}
		return $html;
	}*/

	function image_description($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<label for=\"image_description\">Description de la photo&nbsp;:</label>\n".
					"<br />\n".
					"Au premier rang&nbsp;:<textarea name=\"image_description_1\" id=\"image_description_1\" cols=\"30\" rows=\"2\"></textarea><br />\n".
					"<br />\n".
					"Au deuxi&egrave;me rang&nbsp;:<textarea name=\"image_description_2\" id=\"image_description_2\" cols=\"30\" rows=\"2\"></textarea><br />\n".
					"<br />\n".
					"Au troisi&egrave;me rang&nbsp;:<textarea name=\"image_description_3\" id=\"image_description_3\" cols=\"30\" rows=\"2\"></textarea>\n";
				break;

			case "modify":
				$html .=
					"<label for=\"image_description\">Description de la photo&nbsp;:</label>\n".
					"<br />\n".
					"Au premier rang&nbsp;:<textarea name=\"image_description_1\" id=\"image_description_1\" cols=\"30\" rows=\"2\"></textarea><br />\n".
					"<br />\n".
					"Au deuxi&egrave;me rang&nbsp;:<textarea name=\"image_description_2\" id=\"image_description_2\" cols=\"30\" rows=\"2\"></textarea><br />\n".
					"<br />\n".
					"Au troisi&egrave;me rang&nbsp;:<textarea name=\"image_description_3\" id=\"image_description_3\" cols=\"30\" rows=\"2\"></textarea>\n";
				break;

			case "read":
				$html .=
					"<label for=\"image_description\">Description de la photo&nbsp;:</label>\n".
					"<br />\n".
					"Au premier rang&nbsp;:<br />\n".
					"<div class=\"hta_ro\"><!---HIDDEN_image_description_1 /HIDDEN_image_description_1---></div>\n".
					"<br />\n".
					"Au deuxi&egrave;me rang&nbsp;:<br />\n".
					"<div class=\"hta_ro\"><!---HIDDEN_image_description_2 /HIDDEN_image_description_2---></div>\n".
					"<br />\n".
					"Au troisi&egrave;me rang&nbsp;:<br />\n".
					"<div class=\"hta_ro\"><!---HIDDEN_image_description_3 /HIDDEN_image_description_3---></div>\n";
				break;
		}
		return $html;
	}

	function etudes_succursales_nom($action) {
		global $DB, $BASEURL;
		$key = getorpost('key');
		$html = "";
		switch ($action) {
			case "modify":
				$html .= "<label>Succursale(s) de l'&eacute;tude&nbsp;: </label>\n";

				$DB->query(
					"SELECT es.key AS etudes_succursales_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville, es.province AS province ".
					"FROM `etudes` AS e, `etudes_succursales` AS es ".
					"WHERE es.etudes_key = '" . $key . "' AND e.key = '" . $key . "' AND es.actif = '1';"
				);
				$html .= "<ul class=\"answer\">\n";
				while ($DB->next_record()) {
					$etudes_succursales_nom = $DB->getField("adresse") . ", " . $DB->getField("ville") . " (" . $DB->getField("province") . ") ";
					if ($DB->getField("siege_social") == "1") {
						$etudes_succursales_nom .= " [<strong>Si&egrave;ge social</strong>]";
					}
					$html .= "<li><a href=\"" . $BASEURL . "etudes/succursales.php?action=modify&begin=1&key=" . $DB->getField("etudes_succursales_key") . "\" title=\"Modifier\">" . $etudes_succursales_nom . "</a></li>\n";
				}
				$html .= "</ul>\n";
				if ($etudes_succursales_nom == "") {
					$html .= "<span class=\"answer\">(aucune)</span>";
				}
				break;

			case "read":
				$html .= "<label>Succursale(s) de l'&eacute;tude&nbsp;: </label>\n";

				$DB->query(
					"SELECT es.key AS etudes_succursales_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville, es.province AS province ".
					"FROM `etudes` AS e, `etudes_succursales` AS es ".
					"WHERE es.etudes_key = '" . $key . "' AND e.key = '" . $key . "' AND es.actif = '1';"
				);
				$html .= "<ul class=\"answer\">\n";
				while ($DB->next_record()) {
					$etudes_succursales_nom = $DB->getField("adresse") . ", " . $DB->getField("ville") . " (" . $DB->getField("province") . ") ";
					if ($DB->getField("siege_social") == "1") {
						$etudes_succursales_nom .= " [<strong>Si&egrave;ge social</strong>]";
					}
					$html .= "<li><a href=\"" . $BASEURL . "etudes/succursales.php?action=read&begin=1&key=" . $DB->getField("etudes_succursales_key") . "\" title=\"Consulter\">" . $etudes_succursales_nom . "</a></li>\n";
				}
				$html .= "</ul>\n";
				if ($etudes_succursales_nom == "") {
					$html .= "<span class=\"answer\">(aucune)</span>";
				}
				break;
		}
		return $html;
	}

	function employes_nom($action) {
		global $DB, $BASEURL;
		$key = getorpost('key');
		$html = "";
		switch ($action) {
			case "modify":
				$html .= "<label>Employ&eacute;(s) de l'&eacute;tude&nbsp;:</label>\n";

				$DB->query(
					"SELECT DISTINCT em.key AS employes_key, em.prenom AS prenom, em.nom AS nom, em.associe AS associe, f.nom AS fonctions_nom ".
					"FROM `employes` AS em, `etudes` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees, `employes_fonctions` AS ef, `fonctions` AS f ".
					"WHERE et.key = '" . $key . "' AND es.etudes_key = '" . $key . "' AND ees.employes_key = em.key AND em.key = ef.employes_key AND ef.fonctions_key = f.key AND ees.etudes_succursales_key = es.key AND em.actif = '1';"
				);
				$html .= "<ul class=\"answer\">\n";


				$staff = array();
				while ($DB->next_record()) {

					if (isset($staff["e" . $DB->getField("employes_key")])) {

						$html = str_replace($DB->getField("prenom") . " " . $DB->getField("nom"), $DB->getField("prenom") . " " . $DB->getField("nom") . ", " . $DB->getField("fonctions_nom"), $html);

					} else {

						$notaires_nom = $DB->getField("prenom") . " " . $DB->getField("nom") . ", " . $DB->getField("fonctions_nom");
						if ($DB->getField("associe") == "1") {
							$notaires_nom .= " [<strong>Associ&eacute;</strong>]";
						}
						$html .= "<li><a href=\"" . $BASEURL . "employes/employes.php?action=modify&begin=1&key=" . $DB->getField("employes_key") . "\" title=\"Modifier\">" . $notaires_nom . "</a></li>\n";

						$staff["e" . $DB->getField("employes_key")] = "1";

					}
				}
				$html .= "</ul>\n";
				if ($notaires_nom == "") {
					$html .= "<span class=\"answer\">(aucun)</span>";
				}
				break;

			case "read":
				$html .= "<label>Employ&eacute;(s) de l'&eacute;tude&nbsp;:</label>\n";

				$DB->query(
					"SELECT DISTINCT em.key AS employes_key, em.prenom AS prenom, em.nom AS nom, em.associe AS associe, f.nom AS fonctions_nom ".
					"FROM `employes` AS em, `etudes` AS et, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees, `employes_fonctions` AS ef, `fonctions` AS f ".
					"WHERE et.key = '" . $key . "' AND es.etudes_key = '" . $key . "' AND ees.employes_key = em.key AND em.key = ef.employes_key AND ef.fonctions_key = f.key AND ees.etudes_succursales_key = es.key AND em.actif = '1';"
				);
				$html .= "<ul class=\"answer\">\n";

				$staff = array();
				while ($DB->next_record()) {

					if (isset($staff["e" . $DB->getField("employes_key")])) {

						$html = str_replace($DB->getField("prenom") . " " . $DB->getField("nom"), $DB->getField("prenom") . " " . $DB->getField("nom") . ", " . $DB->getField("fonctions_nom"), $html);

					} else {
						$notaires_nom = $DB->getField("prenom") . " " . $DB->getField("nom") . ", " . $DB->getField("fonctions_nom");
						if ($DB->getField("associe") == "1") {
							$notaires_nom .= " [<strong>Associ&eacute;</strong>]";
						}
						$html .= "<li><a href=\"" . $BASEURL . "employes/employes.php?action=read&begin=1&key=" . $DB->getField("employes_key") . "\" title=\"Consulter\">" . $notaires_nom . "</a></li>\n";

						$staff["e" . $DB->getField("employes_key")] = "1";
					}
				}
				$html .= "</ul>\n";
				if ($notaires_nom == "") {
					$html .= "<span class=\"answer\">(aucun)</span>";
				}
				break;
		}
		return $html;
	}

