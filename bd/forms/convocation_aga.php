<?PHP
	global $DB;

	// LE FORMULAIRE //
	function getForm($action) {
		global $BASEURL;
		
		$_FORM =
			form_header($action).
			"<div id=\"convocation_aga\">\n".

			"		<a name=\"date\"></a><label for=\"date\">Date de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_debut($action) . "\n".
			"		</div>\n".

			"		<a name=\"date_limite\"></a><label for=\"date_limite\">Date limite&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . date_limite($action) . "\n".
			"		</div>\n".

			"		<a name=\"lieu\"></a><label for=\"lieu\">Lieu&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . lieu($action) . "\n".
			"		</div>\n".

			
			"		<label for=\"description\">Description de l'&eacute;v&eacute;nement&nbsp;: </label>\n".
			"		<div class=\"field\">\n".
			"			" . description($action) . "\n".
			"		</div>\n".

			"		<label for=\"actif\">Actif</label>\n".
			"		<div class=\"field\">\n".
			"			" . actif($action) . "\n".
			"		</div>\n".

			form_footer($action);
			
		return $_FORM;
	}

	// LES SOUS-ROUTINES POUR LES INPUTS //

	function form_header($action) {
		$html = 
			"<script type=\"text/javascript\" src=\"/skin/js/tinymce/tinymce.min.js\"></script>\n".
			"<script type=\"text/javascript\">\n".
			"	tinymce.init({\n".
    		"		selector: \"textarea\",\n".
    		"		height: 600\n".
 			"	});\n".
			"</script>\n";

		switch ($action) {
			case "add":					
				$html .=
					"<form name=\"evenements\" action=\"convocation_aga.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"add\" />\n".
					"	<input type=\"hidden\" name=\"id\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "modify":
				$html .=	
					"<form name=\"evenements\" action=\"convocation_aga.php\" method=\"post\">\n".
					"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
					"	<input type=\"hidden\" name=\"id\" value=\"\" />\n".
					"	<input type=\"hidden\" name=\"key\" value=\"\" />\n".
					"	<div class=\"form\">\n";
				break;
			case "read":
				$html .=
					"	<div class=\"readform\">\n".
					"		<a href=\"/evenements/alerte.php?table=convocation_aga&id=" . $_POST["id"] . "\" target=\"_blank\"><strong>Alerte courriel</strong></a><br />\n".
					"		<a href=\"/evenements/inscription.php?table=convocation_aga&id=" . $_POST["id"] . "\" target=\"_blank\"><strong>Inscriptions</strong></a><br />\n".
					"		<a href=\"/evenements/inscription.php?table=convocation_aga&id=" . $_POST["id"] . "&excel=1\" target=\"_blank\"><strong>Inscriptions Excel</strong></a><br /><br />\n";
				break;
		}
		return $html;
	}

	function form_footer($action) {
		$html = "";
		switch ($action) {
			case "add":					
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "modify":
				$html =
					"	<br /><br />\n".	
					"	</div>\n".
					"	<div class=\"submit\">\n".
					"		<input type=\"submit\" value=\"Soumettre\" class=\"bouton\" />\n".
					"	</div>\n".
					"</form>\n";
				break;
			case "read":
				$html =
					"	</div>\n";
				break;
		}
		return $html;
	}


	function date_limite($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"date_limite\" id=\"date_limite\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date_limite\" id=\"date_limite\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date_limite /HIDDEN_date_limite---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function actif($action){
		$html="";
		switch ($action) {	
			case "add":
				$html = 
					"<select name=\"actif\">\n".
					"	<option id=\"actif\" value=\"\"></option>\n".
					"	<option id=\"actif\" value=\"1\">Oui</option>\n".
					"	<option id=\"actif\" value=\"0\">Non</option>\n".
					"</select>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html =
					"<select name=\"actif\">\n".
					"	<option id=\"actif\" value=\"\"></option>\n".
					"	<option id=\"actif\" value=\"1\">Oui</option>\n".
					"	<option id=\"actif\" value=\"0\">Non</option>\n".
					"</select>\n".
					"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_actif /HIDDEN_actif---></span><br/><br/>\n";
				break;
		}
		return $html;
		
	}

	function date_debut($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"date\" id=\"date\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"date\" id=\"date\" value=\"\" /> (format AAAA-MM-JJ)\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_date /HIDDEN_date---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

	function lieu($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = "<input type=\"text\" name=\"lieu\" id=\"lieu\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "modify":
				$html = "<input type=\"text\" name=\"lieu\" id=\"lieu\" value=\"\" />\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_lieu /HIDDEN_lieu---></span><br/><br/>\n";
				break;
		}
		return $html;
	}


	function description($action) {

		$html = "";
		switch ($action) {	
			case "add":
				$html = 
					"<textarea rows=\"8\" cols=\"40\" name=\"description\">\n".
					"	<div style=\"width: 550px;\">\n".
					"		<img src=\"/skin/img/logo.png\" alt=\"PME Inter\" style=\"width: 80px; height: auto; float: left; padding-right: 10px; padding-bottom: 20px;\" />\n".
					"		<div style=\"background-color: #CC6633; padding: 10px 50px; text-align: center; width: 80%; height: 85px; color: #FFF; margin-left: 85px; font-size: 25px; line-height: 40px; font-weight: normal;\">AVIS DE CONVOCATION<br />&agrave; tous les notaires du r&eacute;seau</div>\n".
					"		<br />\n".
					"		<div>\n".
					"			<p>C'est avec plaisir que nous vous invitons &agrave; participer &agrave; <strong>l'assembl&eacute;e g&eacute;n&eacute;rale annuelle</strong> de PME INTER Notaires qui aura lieu <strong>de 17 h &agrave; 18 h 30 <u>le jeudi [DATE] [LIEU]</u></strong>. Un cocktail suivra l'assembl&eacute;e.</p>\n".
					"			<p>Nous vous prions de bien vouloir prendre connaissance de l'ordre du jour pour cette assembl&eacute;e.</p>\n".
					"		</div>\n".
					"		<div>\n".
					"			<h1 style=\"color: #CC6633;\">Ordre du jour</h1>\n".
					"			<ol>\n".
					"				<li>Mot de bienvenu</li>\n".
					"				<li>Rapport du pr&eacute;sident</li>\n".
					"				<li>Rapport du tr&eacute;sorier</li>\n".
					"				<li>Adoption des &eacute;tats financiers pour la p&eacute;riode couvrant le 1<sup>er</sup> novembre XXXX au 31 octobre XXXX</li>\n".
					"				<li>Adoption de la firme comptable</li>\n".
					"				<li>Varia</li>\n".
					"			</ol>\n".
					"			<p>Au plaisir de vous accueillir,</p>\n".
					"			<hr style=\"border: none; border-top: solid 2px #CC6633; border-bottom: solid 1px #CC6633;\" />\n".
					"		</div>\n".
					"		<div>\n".
					"			<p style=\"color: #CC6633;\">Confirmation de pr&eacute;sence</p>\n".
					"			[INSCRIPTIONS]\n".
					"			<p><em>Pri&egrave;re de nous retourner votre confirmation de pr&eacute;sence <u>avant le [DATE_LIMITE]</u> par t&eacute;l&eacute;copieur au (514) 874-9618 ou par courriel &agrave; <a href=\"mailto:info@pmeinter.com\">info@pmeinter.com</a>.</em></p> \n".
					"		</div>\n".
					"	</div>".
					"</textarea>\n".
					"<br/><br/>";
				break;
			case "modify":
				$html = "<textarea rows=\"3\" cols=\"40\" name=\"description\"></textarea>\n".
						"<br/><br/>";
				break;
			case "read":
				$html = 
					"<span class=\"answer\"><!---HIDDEN_description /HIDDEN_description---></span><br/><br/>\n";
				break;
		}
		return $html;
	}

?>