<?PHP
    //require_once "../conf/conf.inc.php";

    //session_start();

    // --------
    // SECURITÉ
    // --------
    // if (!isAuthenticated()) {
    //     $html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
    //     $Skin = new Skin($SKIN_PATH, $SKIN_URL, "dossiers");
    //     $Skin->assign("errors", $html);
    //     $Skin->display("dossiers.tpl");
    //     exit;
    // }

    // if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3)) {
    //     $html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
    //     $Skin = new Skin($SKIN_PATH, $SKIN_URL, "dossiers");
    //     $Skin->assign("errors", $html);
    //     $Skin->display("dossiers.tpl");
    //     exit;
    // }

    //importBD("com.quiboweb.form.Form");
    //importBD("com.pmeinter.Dossier");

    //$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


    // ------------------
    // LE TRAITEMENT HTML
    // ------------------

    $formString = "";
    $successString = "";
    $errorString = "";

    $begin = getorpost('begin');
    $action = getorpost('action');
    $key = getorpost('key');


    switch ($action) {
        case "add" :
            if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
                exit;
                break;
            }

            if ($begin) {
                importForm("dossiers");
                $FormObject = new Form("", "", getForm("add"));
                $formString = $FormObject->returnForm();

            } else {
                $errorString = validateFormValues("add", getParametersFromPOST());
                if ($errorString == "") {
                    $errorString = updateDBValues("add", getParametersFromPOST());
                    if ($errorString == "") {
                        $successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
                        $formString = "<a href=\"" . $BASEURL . "dossiers/index.php\">Retourner au menu pr&eacute;c&eacute;dent.</a>";
                        break;
                    }

                } else {
                    importForm("dossiers");
                    $FormObject = new Form("", getParametersFromPOST(), getForm("add"));
                    $formString = $FormObject->returnFilledForm();
                }
            }
            break;

        case "modify" :
            if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
                exit;
                break;
            }

            if ($begin) {
                importForm("dossiers");
                $FormObject = new Form("", getParametersFromDB("modify", $key), getForm("modify"));
                $FormObject->setAllHidden();
                $formString = $FormObject->returnFilledForm();

            } else {
                $errorString = validateFormValues("modify", getParametersFromPOST());
                if ($errorString == "") {
                    $errorString = updateDBValues("modify", getParametersFromPOST());
                    if ($errorString == "") {
                        $successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
                        $formString = "<a href=\"" . $BASEURL . "dossiers/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
                        break;
                    }

                } else {
                    importForm("dossiers");
                    $FormObject = new Form("", getParametersFromPOST(), getForm("modify"));
                    $FormObject->setAllHidden();
                    $formString = $FormObject->returnFilledForm();
                }
            }
            break;

        case "read" :
            importForm("dossiers");
            $FormObject = new Form("", getParametersFromDB("read", $key), getForm("read"));
            $FormObject->setAllHidden();
            $formString = $FormObject->returnFilledForm();
            break;

    }


    $Skin = new Skin($SKIN_PATH, $SKIN_URL, "dossiers");
    $Skin->assign("page", "dossiers");
    $Skin->assign("title", pageTitleString($action, $key));
    $Skin->assign("headcontent", headString($action));
    $Skin->assign("bodyparams", bodyString($action));
    $Skin->assign("form_title", formTitleString($action, $key));
    $Skin->assign("success", $successString);
    $Skin->assign("errors", $errorString);
    $Skin->assign("form", $formString);
    $Skin->assign("index", $indexString);
    $Skin->display("dossiers.tpl");

    $DB->close();



    function headString($action) {
        global $JS_URL;
        $html = "";
        switch ($action) {
            case "add":
                $html .=
                    "<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
                break;

            case "modify":
                $html .=
                    "<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
                break;

            case "read":
                break;
        }
        return $html;
    }

    function bodyString($action) {
        $html = "";
        switch ($action) {
            case "modify":
                break;

            case "modify":
                break;

            case "read":
                break;
        }
        return $html;
    }

    function formTitleString($action, $key) {
        switch ($action) {
            case "add":
                $title = "<h1>Ajout d'un dossier</h1>";
                break;
            case "modify":
                $title = "<h1>Modification d'un dossier</h1>";
                break;
            case "read":
                $title = "<h1>Consultation d'un dossier</h1>";
                break;
        }
        return $title;
    }

    function validateFormValues($action, $parameters) {
        global $DB, $EMAIL_FORMAT_PATTERN;
        $errorString = "";
        switch ($action) {
            case "add":
                if ($parameters['etudes_key'] == "-1" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('etudes_key');\">Veuillez indiquer l'&eacute;tude.</a>");
                }
                if ($parameters['deadline'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('deadline');\">Veuillez indiquer le d&eacute;lai requis.</a>");
                }
                if ($parameters['titre'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('titre');\">Veuillez indiquer un titre pour votre dossier.</a>");
                }
                if ($parameters['description'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('description');\">Veuillez indiquer la description de votre dossier.</a>");
                }
                break;

            case "modify":
                if ($parameters['deadline'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('deadline');\">Veuillez indiquer le d&eacute;lai requis.</a>");
                }
                if ($parameters['titre'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('titre');\">Veuillez indiquer un titre pour votre dossier.</a>");
                }
                if ($parameters['description'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('description');\">Veuillez indiquer la description de votre dossier.</a>");
                }
                break;
        }
        return $errorString;
    }

    function getParametersFromPOST() {
        $parameters = array();
        if (count($_POST) > 0) {
            foreach ($_POST as $field => $value) {
                if (is_array($value)) {
                    $parameters[$field] = $value;
                } else {
                    $parameters[$field] = trim($value);
                }
            }
        }
        return $parameters;
    }

    function getParametersFromDB($action, $key) {
        global $DB, $BASEURL;
        $parameters = array();
        switch ($action) {
            case "modify":
                $Dossier = new Dossier($DB, "", "");
                $Dossier->setQKey("key", $key);
                $Dossier->scanfields();
                $Dossier->loadAll();
                $parameters = $Dossier->getAll();

                $parameters["deadline"] = date("Y-m-d", strtotime($Dossier->get("deadline")));

                $DB->query(
                    "SELECT `key`, `nom` ".
                    "FROM `etudes` ".
                    "WHERE `key` = '" . $Dossier->get("etudes_key") . "' AND `actif` = '1';"
                );

                while ($DB->next_record()) { $parameters['etude_nom'] = $DB->getField("nom"); }

                $DB->query(
                    "SELECT `key`, `nom`, `prenom` ".
                    "FROM `employes` ".
                    "WHERE `key` = '" . $Dossier->get("employes_key") . "' AND `actif` = '1';"
                );

                while ($DB->next_record()) { $parameters['employe_nom'] = $DB->getField("nom") . ", " . $DB->getField("prenom"); }

                break;

            case "read":
                $Dossier = new Dossier($DB, "", "");
                $Dossier->setQKey("key", $key);
                $Dossier->scanfields();
                $Dossier->loadAll();
                $parameters = $Dossier->getAll();


                $parameters["deadline"] = date("Y-m-d", strtotime($Dossier->get("deadline")));

                switch ($Dossier->get("suivi")) {
                    case 1:
                        $parameters["suivi"] = "En cours";
                        break;

                    case 0:
                        $parameters["suivi"] = "Annul&eacute;";
                        break;

                    case 2:
                        $parameters["suivi"] = "Complété";
                        break;

                    default:
                        $parameters["suivi"] = "En cours";
                        break;
                }

                $secteurs = array();
                if ($Dossier->get("secteur_agricole")) {
                    $secteurs[] = "Droit agricole";
                }

                if ($Dossier->get("secteur_affaire")) {
                    $secteurs[] = "Droit des affaires";
                }

                if ($Dossier->get("secteur_personne")) {
                    $secteurs[] = "Droit de la personne";
                }

                if ($Dossier->get("secteur_immobilier")) {
                    $secteurs[] = "Droit immobilier";
                }

                if ($Dossier->get("secteur_fiducie")) {
                    $secteurs[] = "Fiducie";
                }

                if ($Dossier->get("secteur_autre")) {
                    $secteurs[] = "Autres";
                }

                $parameters["secteurs"] = implode(", ", $secteurs);


                $DB->query(
                    "SELECT `key`, `nom` ".
                    "FROM `etudes` ".
                    "WHERE `key` = '" . $Dossier->get("etudes_key") . "' AND `actif` = '1';"
                );

                while ($DB->next_record()) { $parameters['etude_nom'] = $DB->getField("nom"); }

                $DB->query(
                    "SELECT `key`, `nom`, `prenom` ".
                    "FROM `employes` ".
                    "WHERE `key` = '" . $Dossier->get("employes_key") . "' AND `actif` = '1';"
                );

                while ($DB->next_record()) { $parameters['employe_nom'] = $DB->getField("nom") . ", " . $DB->getField("prenom"); }

                break;
        }
        return $parameters;
    }

    function updateDBValues($action, $parameters) {
        global $DB;
        $errorString = "";

        $parameters = addslashesToValues($parameters);

        switch ($action) {
            case "add":
                //$parameters['code_postal'] = strtoupper($parameters['code_postal']);
                $Dossier = new Dossier($DB, "", 0);
                $Dossier->scanfields();
                $Dossier->insertmode();
                $Dossier->set("actif", "1");
                $Dossier->setAll($parameters);
                $Dossier->excludefield("action");
                if (!$Dossier->saveAll()) {
                    $errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
                }
                break;

            case "modify":
                //$parameters['code_postal'] = strtoupper($parameters['code_postal']);
                $Dossier = new Dossier($DB, "", 0);
                $Dossier->setQKey("key", $parameters['key']);
                $Dossier->scanfields();
                $Dossier->setAll($parameters);
                $Dossier->excludefield("action");
                $Dossier->excludefield("etudes_key");
                if (!$Dossier->saveAll()) {
                    $errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
                }
                break;
        }
        return $errorString;
    }

    function addslashesToValues($parameters = "") {
        if (is_array($parameters) && count($parameters) > 0) {
            foreach ($parameters as $field => $value) {
                if (is_array($value) && count($value) > 0) {
                    foreach ($value as $field2 => $value2) {
                        if (is_array($value2) && count($value2) > 0) {
                            foreach ($value2 as $field3 => $value3) {
                                $parameters[$field][$field2][$field3] = addslashes($value3);
                            }
                        } else {
                            $parameters[$field][$field2] = addslashes($value2);
                        }
                    }
                } else {
                    $parameters[$field] = addslashes($value);
                }
            }
        } else {
            $parameters = addslashes($parameters);
        }
        return $parameters;
    }

