<?PHP
/*

Répondre à un dossier actif (répondre à une offre active)

Commenter un dossier

Commentaires (pour gérer les questions réponses)

____Interface et formulaire de gestion d’un dossier_______> 1 jour


La liste des mandats serait filtrable par status et triable par budget et par date de tombée


Envoyer des alertes à tous les notaires permettant un reply-to à la personne ressource.

____Alertes courriels avec reply-to_____> 1 jour


Quand un nouveau mandat est inscrit une alerte courriel serait envoyé à tous les notaires qui pourraient répondre directement à l’auteur pour dire qu’il est intéressé.


*/

    // --------
    // SECURITÉ
    // --------
   if (!isAuthenticated()) {
       $html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
       $Skin = new Skin($SKIN_PATH, $SKIN_URL, "dossiers");
       $Skin->assign("errors", $html);
       $Skin->display("dossiers.tpl");
       exit;
   }

    if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3)) {
       $html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
        $Skin = new Skin($SKIN_PATH, $SKIN_URL, "dossiers");
        $Skin->assign("errors", $html);
        $Skin->display("dossiers.tpl");
        exit;
    }

    importBD("com.pmeinter.Etude");
    importBD("com.pmeinter.Dossier");
    importBD("com.pmeinter.Succursale");
    importBD("com.pmeinter.Emploi");
    importBD("com.quiboweb.form.Form");

    $DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


    // ------------------
    // LE TRAITEMENT HTML
    // ------------------

    $begin = getorpost('begin');
    $action = getorpost('action');
    $type = getorpost('type');
    $key = getorpost('key');

    $successString = "";
    $errorString = "";
    $formString = "";

    importForm("dossiers");
    importForm("comments");


    if (count($_POST) > 0) {

        switch ($_POST['action']) {
            case 'getComment':

                if (isset($_POST["key"])) {
                    getComments($_POST["key"]);
                    exit;
                }


                break;

            case "add":
                $errorString = validateFormValues("add", getParametersFromPOST());
                if ($errorString == "") {
                    $errorString = updateDBValues("add", getParametersFromPOST());

                    if ($errorString == "") {

                        $type = "new-mandat";
                        $from = "";
                        $to = "";
                        $data = array(
                            "titre" => "",
                            "etude" => "",
                            "deadline" => "",
                            "key" => ""
                        );

                        sendAlert($type, $from, $to, $data);

                        header("Location: /extranet/mandats/"); exit;
                    } else {
                        $FormObject = new Form("", getParametersFromPOST(), getForm("add"));
                        $formString = $FormObject->returnFilledForm();
                    }
                } else {
                    $FormObject = new Form("", getParametersFromPOST(), getForm("add"));
                    $formString = $FormObject->returnFilledForm();
                }
                break;

            case "edit":

                if (isset($_POST["key"]) && isset($_POST["etude"]) && isset($_POST["ajax"])) {
                    $FormObject = new Form("", getParametersFromDB("modify", $key), getForm("modify"));
                    $FormObject->setAllHidden();
                    $formString = $FormObject->returnFilledForm();
                    echo $formString;
                    exit;
                }

            case "modify":

                $errorString = validateFormValues("modify", getParametersFromPOST());
                if ($errorString == "") {
                    $errorString = updateDBValues("modify", getParametersFromPOST());
                    if ($errorString == "") {
                        header("Location: /extranet/mandats/"); exit;
                    } else {
                        $FormObject = new Form("", getParametersFromPOST(), getForm("modify"));
                        $FormObject->setAllHidden();
                        $formString = $FormObject->returnFilledForm();
                    }

                } else {
                    $FormObject = new Form("", getParametersFromPOST(), getForm("modify"));
                    $FormObject->setAllHidden();
                    $formString = $FormObject->returnFilledForm();
                }

                break;

            case "delete":

                if (isset($_POST["key"]) && isset($_POST["etude"]) && isset($_POST["ajax"])) {

                    if (isset($_SESSION["user_etudes_key"]) && $_SESSION["user_etudes_key"] == $_POST['etude']) {
                        deleteDossierFromDB($_POST["key"]);
                        exit;
                    } else if ($_SESSION['user_type'] == 1) {
                        deleteDossierFromDB($_POST["key"]);
                        exit;
                    }

                }

                break;

            case 'add-comment':

                if (isset($_POST["dossiers_key"]) && isset($_POST["employes_key"]) && isset($_POST["description"]) && isset($_POST["comments_key"])) {

                    if ($_POST["comments_key"] == "")  { $_POST["comments_key"] = 0; }

                    $DB->query(
                        "INSERT INTO comments (
                            dossiers_key,
                            employes_key,
                            description,
                            comments_key,
                            actif
                        ) VALUES (
                            " . preg_replace("/[^0-9]+/is", "", $_POST["dossiers_key"]) . ",
                            " . preg_replace("/[^0-9]+/is", "", $_POST["employes_key"]) . ",
                            '" . $_POST["description"] . "',
                            " . preg_replace("/[^0-9]+/is", "", $_POST["comments_key"]) . ",
                            1
                        )"
                    );

                    $type = "new-comment";
                    if ($_POST["comments_key"] != 0) { $type = "new-reply"; }

                    $from = "info@pmeinter.com";
                    $to = "";
                    $data = array(
                        "titre" => "",
                        "etude" => "",
                        "etude_nom" => "",
                        "key" => $_POST["dossiers_key"]
                    );


                    $DB->query("SELECT e.courriel, et.nom, d.titre, d.deadline FROM employes e INNER JOIN dossiers d ON d.employes_key = e.key AND d.key = '" . $_POST["dossiers_key"] . "' INNER JOIN etudes et ON et.key = d.etudes_key");
                    while ($DB->next_record()) {
                        $to = $DB->getField("courriel");
                        $data["etude_nom"] = $DB->getField("nom");
                        $data["titre"] = $DB->getField("titre");
                        $data["deadline"] = $DB->getField("deadline");
                    }

                    sendAlert($type, $from, $to, $data);

                }

                getComments($_POST["dossiers_key"]);

                exit;

                break;

            case 'apply':

                if (isset($_POST["dossiers_key"]) && isset($_POST["employes_key"]) && isset($_POST["description"]) ) {

                    $DB->query(
                        "INSERT INTO dossiers_applications (
                            dossiers_key,
                            employes_key,
                            description,
                            actif
                        ) VALUES (
                            " . preg_replace("/[^0-9]+/is", "", $_POST["dossiers_key"]) . ",
                            " . preg_replace("/[^0-9]+/is", "", $_POST["employes_key"]) . ",
                            '" . $_POST["description"] . "',
                            1
                        )"
                    );

                    $type = "new-applicant";
                    $from = "";
                    $to = "";
                    $data = array(
                        "titre" => "",
                        "etude" => "",
                        "etude_nom" => "",
                        "deadline" => "",
                        "key" => $_POST["dossiers_key"]
                    );

                    $DB->query("SELECT e.courriel, et.nom, d.titre, d.deadline FROM employes e INNER JOIN dossiers d ON d.employes_key = e.key AND d.key = '" . $_POST["dossiers_key"] . "' INNER JOIN etudes et ON et.key = d.etudes_key");
                    while ($DB->next_record()) {
                        $to = $DB->getField("courriel");
                        $data["etude_nom"] = $DB->getField("nom");
                        $data["titre"] = $DB->getField("titre");
                        $data["deadline"] = $DB->getField("deadline");
                    }

                    $DB->query("SELECT e.courriel FROM employes e WHERE `key` = '" . $_POST["employes_key"] . "'");
                    while ($DB->next_record()) {
                        $from = $DB->getField("courriel");
                    }

                    sendAlert($type, $from, $to, $data);

                }

                exit;

                break;

            case 'delete-comment':

                if (isset($_POST["key"])) {
                    $DB->query("UPDATE comments SET actif = 0 WHERE `key` = " . preg_replace("/[^0-9]+/is", "", $_POST["key"]));

                    getComments($_POST["dossiers_key"]);
                }

                exit;

                break;

        }

    } else {
        $FormObject = new Form("", "", getForm("add"));
        $formString = $FormObject->returnForm();
    }





    $Skin = new Skin($SKIN_PATH, $SKIN_URL, "dossiers");
    $Skin->assign("page", "dossiers");
    $Skin->assign("title", "Consulation les mandats");
    $Skin->assign("menu", menuActionString());
    //$Skin->assign("success", $successString);
    //$Skin->assign("errors", $errorString);
    //$Skin->assign("form", $formString);
    $Skin->display("dossiers.tpl");

    $DB->close();


    // -------------
    // LES FONCTIONS
    // -------------

    function menuActionString() {
        global $DB, $formString;

        if (isset($_GET["ajouter"])) {
            $html = "<h1>Publier un appel d'offre</h1>\n";
        } else {
            $html = "<h1>Consulter les appels d'offres</h1>\n";
        }

        if (!isset($_GET["suivi"])) { $_GET["suivi"][] = 1; }

        $suivi_cond = array();
        $suivi_active = "";
        if ((isset($_GET["suivi"]) && !is_array($_GET["suivi"]) && $_GET["suivi"] == 1) || (isset($_GET["suivi"]) && is_array($_GET["suivi"]) && in_array(1, $_GET["suivi"])) ) { $suivi_active = ' checked="checked" '; $suivi_cond[] = '1'; }

        $suivi_completed = "";
        if ((isset($_GET["suivi"]) && !is_array($_GET["suivi"]) && $_GET["suivi"] == 2) || (isset($_GET["suivi"]) && is_array($_GET["suivi"]) && in_array(2, $_GET["suivi"])) ) { $suivi_completed = ' checked="checked" '; $suivi_cond[] = '2'; }

        $suivi_cancelled = "";
        if ((isset($_GET["suivi"]) && !is_array($_GET["suivi"]) && $_GET["suivi"] == 3) || (isset($_GET["suivi"]) && is_array($_GET["suivi"]) && in_array(3, $_GET["suivi"])) ) { $suivi_cancelled = ' checked="checked" '; $suivi_cond[] = '3'; }

        if (count($suivi_cond) > 0) {
            $suivi_cond = " AND d.suivi IN (" . implode(",", $suivi_cond) . ") ";
        }

        $dossiers_keys = array();

        $DB->query("UPDATE `dossiers` SET suivi = 3 WHERE actif = 1 AND suivi = 1 AND deadline < '" . date("Y-m-d") . "' ");

        $DB->query(
            "SELECT d.*, e.nom AS etude_nom, em.nom, em.prenom, es.adresse, es.ville, es.province, es.code_postal ".
            "FROM `dossiers` d INNER JOIN `employes_etudes_succursales` ees ON d.employes_key = ees.employes_key ".
            "INNER JOIN `etudes_succursales` es ON es.key = ees.etudes_succursales_key ".
            "INNER JOIN `etudes` e ON es.etudes_key = e.key ".
            "INNER JOIN `employes` em ON em.key = ees.employes_key ".
            "WHERE e.actif = 1 AND es.actif = '1' ".
            "AND d.actif = 1 ".
            $suivi_cond .
            "ORDER BY d.suivi ASC, d.deadline ASC, d.date, e.nom, d.titre;"
        );



        $html .=
            '<div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5">
                        <ul class="filter-suivi">
                            <li class="active"><input type="checkbox" value="1" id="suivi-active" name="suivi[]" ' . $suivi_active . ' /> <label for="suivi-active">En cours</label></li>
                            <li class="completed"><input type="checkbox" value="2" id="suivi-completed" name="suivi[]" ' . $suivi_completed . ' /> <label for="suivi-completed">Complété</label></li>
                            <li class="cancelled"><input type="checkbox" value="3" id="suivi-cancelled" name="suivi[]" ' . $suivi_cancelled . ' /> <label for="suivi-cancelled">Annulé</label></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-4">
                        <input id="search" type="text" placeholder="Recherche" value="" />
                    </div>
                    <div class="col-xs-12 col-sm-3">
                        <a id="add-button"><span class="fa fa-plus-square-o"></span> Publier un mandat</a>
                    </div>
                </div>
                ' .  $formString . '

                <div class="dossiers-list">';

        if (isset($_GET["ajouter"])) {
            $html .=
                '<script>
                    jQuery(document).ready(function($) {
                        $("#add-button").trigger("click");
                    });
                </script>';
        }

        //if ($_SESSION["user_key"] == "root") { $_SESSION["user_key"] = 575; }

        while ($row = $DB->next_record()) {

            if (!in_array($row["key"], $dossiers_keys)) {
                $dossiers_keys[] = $row["key"];

                $DB->query(
                    "SELECT COUNT(*) AS nb ".
                    "FROM `dossiers_applications` ".
                    "WHERE employes_key = '" . preg_replace("/[^0-9]+/is", "", $_SESSION['employes_key']) . "' ".
                    "AND actif = 1 ".
                    "AND dossiers_key = " . preg_replace("/[^0-9]+/is", "", $row["key"])
                );

                $hideApplication = 0;
                $application_class = "";
                while ($DB->next_record()) {
                    if ($DB->getField("nb") > 0) { 
                        $application_class = ' applique '; 
                        $hideApplication = 1; 
                    }
                }

                $secteurs_classes =
                    ($row["secteur_agricole"] ? ' secteur_agricole ' : '').
                    ($row["secteur_affaire"] ? ' secteur_affaire ' : '').
                    ($row["secteur_personne"] ? ' secteur_personne ' : '').
                    ($row["secteur_immobilier"] ? ' secteur_immobilier ' : '').
                    ($row["secteur_fiducie"] ? ' secteur_fiducie ' : '').
                    ($row["secteur_prd"] ? ' secteur_prd ' : '').
                    ($row["secteur_autre"] ? ' secteur_autre ' : '');

                $secteurs =
                    ($row["secteur_agricole"] ? 'Droit agricole<br />' : '').
                    ($row["secteur_affaire"] ? 'Droit des affaires<br />' : '').
                    ($row["secteur_personne"] ? 'Droit de la personne<br />' : '').
                    ($row["secteur_immobilier"] ? 'Droit immobilier<br />' : '').
                    ($row["secteur_fiducie"] ? 'Fiducie<br />' : '').
                    ($row["secteur_prd"] ? 'PRD<br />' : '').
                    ($row["secteur_autre"] ? 'Autres<br />' : '');

                // if (trim($row["budget_min"]) == "") { $row["budget_min"] = "..."; }
                // if (trim($row["budget_max"]) == "") { $row["budget_max"] = "..."; }

                $suivi = ($row["suivi"] == 1 ? "En cours" : ( $row["suivi"] == 3 ? "Annulé" : ($row["suivi"] == 2 ? "Complété" : '')));

                $suivi_class = ($row["suivi"] == 1 ? " active " : ( $row["suivi"] == 3 ? " cancelled " : ($row["suivi"] == 2 ? " completed " : '')));

                $CommentFormObject = new Form("", "", getCommentForm("add", $row["key"]));
                $CommentFormString = $CommentFormObject->returnForm();

                $has_applications = 0;
                $comments_list = "";
                $applications = "";
                
                if ($row["employes_key"] == $_SESSION['employes_key']) {
                    $DB->query(
                        "SELECT da.*, em.nom, em.prenom, em.courriel, es.ville, et.nom AS etude
                        FROM `dossiers_applications` da
                        INNER JOIN `employes` em ON em.key = da.employes_key
                        INNER JOIN `employes_etudes_succursales` ees ON ees.employes_key = em.key
                        INNER JOIN `etudes_succursales` es ON ees.etudes_succursales_key = es.key
                        INNER JOIN `etudes` et ON et.key = es.etudes_key
                        WHERE et.actif = '1'
                        AND es.actif = '1'
                        AND em.actif = '1'
                        AND da.actif = '1'
                        AND da.dossiers_key = " . $row["key"] . "
                        ORDER BY da.date ASC, em.nom, em.prenom"
                    );

                    /**/

                    while ($application = $DB->next_record()) {
                        $applications .=
                            '<div class="application"><p class="by">Par: <a href="mailto:' . $application["courriel"] . '">' . $application["prenom"] . " " . $application["nom"] . '</a><br />Date: ' . $application["date"] .  '<br />Ville: ' . $application["ville"] . '<br />Étude: ' . $application["etude"] . '</p>
                                <div class="description">
                                ' . $application["description"] . '
                                </div>
                                <hr />
                            </div>';
                        $has_applications++;
                    }

                    if (trim($applications) != "") {
                        $applications =
                            '<div id="application' . $row["key"] . '" class="applications">
                                <a class="close"><span class="fa fa-close"></span></a>
                                <div class="scroll">' . $applications . '</div>
                            </div>';
                    }

                }


                $html .= $applications . '
                    <div id="mandat' . $row["key"] . '" class="row mandat ' . $suivi_class . $secteurs_classes . $application_class . ' user' . ($row["employes_key"] == $_SESSION['employes_key'] || $row["etudes_key"] == $_SESSION["user_etudes_key"] ? '_can_edit' : '_read') . '" data-key="' . $row["key"] . '" data-etudes_key="' . $row["etudes_key"] . '" data-employes_key="' . $row["employes_key"] . '">

                        <div class="tools">
                            <a class="edit"><span class="fa fa-pencil-square-o"></span></a>
                            <a class="delete"><span class="fa fa-trash-o"></span></a>
                            ' . ($has_applications ? '<a class="files"><span class="fa fa-file-o"></span> <small>(' . $has_applications . ')</small></a>': '') . '
                        </div>
                        <div class="suivi"><span>' . $suivi . '</span></div>
                        <div class="edit-form col-sm-12 col-xs-12">

                        </div>
                        <div class="col-xs-12 col-sm-8">
                            <div class="row">
                                <div class="responsable col-xs-8 col-sm-6"><strong>Publié par :</strong> ' . $row["nom"] . ", " . $row["prenom"] . '</div>
                                <div class="date col-xs-4 col-sm-6"><strong>Le :</strong> ' . date("Y-m-d", strtotime($row["date"])) . '</div>
                                <div class="etude col-xs-12 col-sm-12">' . $row["etude_nom"] . '</div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h2>' . stripslashes(stripslashes($row["titre"])) . '</h2>
                                    <div class="description">
                                        ' . nl2br(stripslashes(stripslashes($row["description"]))) . '
                                    </div>
                                    <div class="fichiers">
                                        <script type="text/javascript" src="/extranet/bd/skin/js/dropzone.js"></script>
                                        <input type="hidden" id="fichiers" name="fichiers" class="file_uploader fr" data-id="mandat' . $row["key"] . '" data-editable="0" data-qty="5" value=\'' . stripslashes(stripslashes($row["fichiers"])) . '\'" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-4">
                            <p class="ville"><strong>Ville :</strong> ' . $row['ville'] . '</p>
                            <p class="deadline"><strong>Date d\'échéance :</strong> ' . date("Y-m-d", strtotime($row["deadline"])) . '</p>
                            <!--p class="budget"><strong>Budget :</strong> Entre ' . $row["budget_min"] . '$ et ' . $row["budget_max"] . '$</p-->
                            <p class="budget"><strong>Montant alloué pour l’exécution du présent mandat :</strong> ' . $row["budget"] . '</p>
                            <div class="secteurs"><strong>Secteur(s) :</strong><br />' . $secteurs . '</div>
                        </div>
                        <div class="col-xs-12 col-sm-12 comments-list">
                            <a class="comment"><span class="fa fa-comment-o"></span> &nbsp;&nbsp;Questions / Réponses</a>
                            <div class="comments">
                                
                            </div>
                            ' . $CommentFormString . '
                        </div>
                        <div class="apply-bar">' .
                        ($hideApplication ? '<a class="apply" style="color: green;background-color: #FFF;width: 90%;height: 100%;display: block;margin-left: auto; margin-right: auto;"><span class="fa fa-check-circle-o"></span> &nbsp;&nbsp; Répondue</a>' : '
                            <a class="apply"><span class="fa fa-check-circle-o"></span> &nbsp;&nbsp; Répondre à cet appel d\'offre</a>
                            ' . convertToApplication($CommentFormString) ) . '
                        </div>
                    </div>';





            }
        }

        $html .= '</div></div>';

        return $html;
    }


    function deleteDossierFromDB($key) {
        // if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
        //     exit;
        //     break;
        // }

        global $DB;
        $errorString = "";
        $dossier = new Dossier($DB, "", 1);
        $dossier->setQKey("key", $key);
        if (!$dossier->set("actif", "0")) {
            $errorString .= _error("Une erreur s'est produite lors de la suppression des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
        }
        return $errorString;
    }

    function validateFormValues($action, $parameters) {
        global $DB, $EMAIL_FORMAT_PATTERN;
        $errorString = "";
        switch ($action) {
            case "add":
                if ($parameters['etudes_key'] == "-1" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('etudes_key');\">Veuillez indiquer l'&eacute;tude.</a>");
                }
                if ($parameters['deadline'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('deadline');\">Veuillez indiquer la date d'&eacute;ch&eacute;ance.</a>");
                }
                if ($parameters['titre'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('titre');\">Veuillez indiquer un titre pour votre mandat.</a>");
                }
                if ($parameters['description'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('description');\">Veuillez indiquer la description de votre mandat.</a>");
                }
                break;

            case "modify":
                if ($parameters['deadline'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('deadline');\">Veuillez indiquer la date d'&eacute;ch&eacute;ance.</a>");
                }
                if ($parameters['titre'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('titre');\">Veuillez indiquer un titre pour votre mandat.</a>");
                }
                if ($parameters['description'] == "" ) {
                    $errorString .= _error("<a href=\"javascript: showErrorField('description');\">Veuillez indiquer la description de votre mandat.</a>");
                }
                break;
        }
        return $errorString;
    }

    function getParametersFromPOST() {
        $parameters = array();
        if (count($_POST) > 0) {
            foreach ($_POST as $field => $value) {
                if (is_array($value)) {
                    $parameters[$field] = $value;
                } else {
                    $parameters[$field] = trim($value);
                }
            }
        }
        return $parameters;
    }

    function getParametersFromDB($action, $key) {
        global $DB, $BASEURL;
        $parameters = array();
        switch ($action) {
            case "modify":
                $Dossier = new Dossier($DB, "", "");
                $Dossier->setQKey("key", $key);
                $Dossier->scanfields();
                $Dossier->loadAll();
                $parameters = $Dossier->getAll();

                $parameters["deadline"] = date("Y-m-d", strtotime($Dossier->get("deadline")));

                $parameters["fichiers"] = stripslashes($Dossier->get("fichiers"));

                $DB->query(
                    "SELECT `key`, `nom` ".
                    "FROM `etudes` ".
                    "WHERE `key` = '" . $Dossier->get("etudes_key") . "' AND `actif` = '1';"
                );

                while ($DB->next_record()) { $parameters['etude_nom'] = $DB->getField("nom"); }

                $DB->query(
                    "SELECT `key`, `nom`, `prenom` ".
                    "FROM `employes` ".
                    "WHERE `key` = '" . $Dossier->get("employes_key") . "' AND `actif` = '1';"
                );

                while ($DB->next_record()) { $parameters['employe_nom'] = $DB->getField("nom") . ", " . $DB->getField("prenom"); }

                break;

            case "read":
                $Dossier = new Dossier($DB, "", "");
                $Dossier->setQKey("key", $key);
                $Dossier->scanfields();
                $Dossier->loadAll();
                $parameters = $Dossier->getAll();


                $parameters["deadline"] = date("Y-m-d", strtotime($Dossier->get("deadline")));

                switch ($Dossier->get("suivi")) {
                    case 1:
                        $parameters["suivi"] = "En cours";
                        break;

                    case 0:
                        $parameters["suivi"] = "Annul&eacute;";
                        break;

                    case 2:
                        $parameters["suivi"] = "Complété";
                        break;

                    default:
                        $parameters["suivi"] = "En cours";
                        break;
                }

                $secteurs = array();
                if ($Dossier->get("secteur_agricole")) {
                    $secteurs[] = "Droit agricole";
                }

                if ($Dossier->get("secteur_affaire")) {
                    $secteurs[] = "Droit des affaires";
                }

                if ($Dossier->get("secteur_personne")) {
                    $secteurs[] = "Droit de la personne";
                }

                if ($Dossier->get("secteur_immobilier")) {
                    $secteurs[] = "Droit immobilier";
                }

                if ($Dossier->get("secteur_fiducie")) {
                    $secteurs[] = "Fiducie";
                }

                if ($Dossier->get("secteur_prd")) {
                    $secteurs[] = "PRD";
                }

                if ($Dossier->get("secteur_autre")) {
                    $secteurs[] = "Autres";
                }

                $parameters["secteurs"] = implode(", ", $secteurs);


                $DB->query(
                    "SELECT `key`, `nom` ".
                    "FROM `etudes` ".
                    "WHERE `key` = '" . $Dossier->get("etudes_key") . "' AND `actif` = '1';"
                );

                while ($DB->next_record()) { $parameters['etude_nom'] = $DB->getField("nom"); }

                $DB->query(
                    "SELECT `key`, `nom`, `prenom` ".
                    "FROM `employes` ".
                    "WHERE `key` = '" . $Dossier->get("employes_key") . "' AND `actif` = '1';"
                );

                while ($DB->next_record()) { $parameters['employe_nom'] = $DB->getField("nom") . ", " . $DB->getField("prenom"); }

                break;
        }
        return $parameters;
    }

    function getComments($key) {
        global $DB;

        $DB->query(
            "SELECT c.*, em.nom, em.prenom ".
            "FROM `comments` c INNER JOIN `employes` em ON em.key = c.employes_key ".
            "WHERE c.actif = '1' ". //em.actif = 1 AND
            "AND c.dossiers_key = '" . $key . "' ".
            "ORDER BY c.comments_key, c.date ASC"
        );

        $comments = array();
        while ($row = $DB->next_record()) {
            if ($row["comments_key"] > 0) {
                $comments[$row["comments_key"]][$row["key"]] = $row;
            } else {
                $comments[$row["key"]][0] = $row;
            }
        }

        $html = '';

        foreach ($comments as $key => $discussion) {

            foreach ($discussion as $i => $comment) {

                $class = "enfant";
                if ($i == 0) { $class = " parent "; }

                if ($_SESSION["user_key"] == $comment["employes_key"] || $_SESSION['user_type'] == 1) { $class .= " deletable"; }

                $html .=
                    '<div class="comment ' . $class . '" data-key="' . $comment["key"] . '" data-comments_key="' . $comment["comments_key"] . '">
                        <div class="tools">
                            <a href="#add-comment-form-' . $key . '" class="reply"><span class="fa fa-mail-reply"></span></a>
                            <a class="delete-comment"><span class="fa fa-trash-o"></span></a>
                        </div>
                        <p class="by">Par: ' . $comment["prenom"] . " " . $comment["nom"] . ' (' . $comment["date"] .  ')</p>
                        <div class="description">
                            ' . $comment["description"] . '
                        </div>
                    </div>';
            }

        }

        echo $html;
    }

    function updateDBValues($action, $parameters) {
        global $DB;
        $errorString = "";

        $parameters = addslashesToValues($parameters);

        //if (isset($parameters["choix_etude"]) && is_array($parameters["choix_etude"])) {
            //$parameters["partager_avec"] = implode(",", $parameters["choix_etude"]);
            unset($parameters["choix_etude"]);
        //}

        switch ($action) {
            case "add":
                //$parameters['code_postal'] = strtoupper($parameters['code_postal']);
                $Dossier = new Dossier($DB, "", 0);
                $Dossier->scanfields();
                $Dossier->insertmode();
                $Dossier->set("actif", "1");
                $Dossier->setAll($parameters);
                $Dossier->set("suivi", "1");
                $Dossier->excludefield("action");
                if (!$Dossier->saveAll()) {
                    $errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
                } else {
                    $parameters["key"] = $Dossier->insertId;
                    sendAlerts($parameters);
                }

                break;

            case "modify":
                //$parameters['code_postal'] = strtoupper($parameters['code_postal']);
                $Dossier = new Dossier($DB, "", 0);
                $Dossier->setQKey("key", $parameters['key']);
                $Dossier->scanfields();

                $Dossier->setAll($parameters);

                $Dossier->excludefield("action");
                $Dossier->excludefield("etudes_key");
                if (!$Dossier->saveAll()) {
                    $errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
                } else {
                    sendAlerts($parameters);
                }
                break;
        }
        return $errorString;
    }

    function addslashesToValues($parameters = "") {
        if (is_array($parameters) && count($parameters) > 0) {
            foreach ($parameters as $field => $value) {
                if (is_array($value) && count($value) > 0) {
                    foreach ($value as $field2 => $value2) {
                        if (is_array($value2) && count($value2) > 0) {
                            foreach ($value2 as $field3 => $value3) {
                                $parameters[$field][$field2][$field3] = addslashes($value3);
                            }
                        } else {
                            $parameters[$field][$field2] = addslashes($value2);
                        }
                    }
                } else {
                    $parameters[$field] = addslashes($value);
                }
            }
        } else {
            $parameters = addslashes($parameters);
        }
        return $parameters;
    }

    function sendAlerts($parameters) {
        global $DB;

        $alreadysent = array();
        $DB->query(
            "SELECT DISTINCT email
            FROM `dossiers_alertes`
            WHERE dossiers_key = '" . $parameters["key"] . "'"
        );

        while ($DB->next_record()) {
            $alreadysent[strtolower($DB->getField("email"))] = 1;
        }


        if (isset($parameters["partager_avec"]) && trim($parameters["partager_avec"]) != "") {
            //$parameters["partager_avec"] = implode(",", $parameters["choix_etude"]);

            $etudes = array();
            $notaires = array();

            foreach (explode(",", $parameters["partager_avec"]) as $i => $id) {
                if (strpos($id, "e") !== false) {
                    $etudes[] = intval(str_replace("e", "", $id));
                }

                if (strpos($id, "u") !== false) {
                    $notaires[] = intval(str_replace("u", "", $id));
                }
            }

            $DB->query(
                "SELECT DISTINCT em.key AS employes_key, em.prenom AS prenom, em.nom AS nom, em.courriel, em.associe AS associe, et.nom, et.key AS etude_key
                FROM `employes` em INNER JOIN `employes_etudes_succursales` ees ON ees.employes_key = em.key
                INNER JOIN `etudes_succursales` es ON es.key = ees.etudes_succursales_key
                INNER JOIN `etudes` et ON et.key = es.etudes_key
                LEFT JOIN employes_fonctions ef ON em.key = ef.employes_key AND ef.fonctions_key IN (15,8,12)
                WHERE et.actif = '1'
                AND es.actif = '1'
                AND em.actif = '1'
                " . (count($notaires) ? " AND em.key IN (" . implode(",", $notaires) . ") " : '') . "
                ORDER BY et.nom ASC"
            );

            //" . (count($etudes) ? " AND et.key IN (" . implode(",", $etudes) . ") " : '') . "

        } else {
            $DB->query(
                "SELECT DISTINCT em.key AS employes_key, em.prenom AS prenom, em.nom AS nom, em.courriel, em.associe AS associe, et.nom, et.key AS etude_key
                FROM `employes` em INNER JOIN `employes_etudes_succursales` ees ON ees.employes_key = em.key
                INNER JOIN `etudes_succursales` es ON es.key = ees.etudes_succursales_key
                INNER JOIN `etudes` et ON et.key = es.etudes_key
                LEFT JOIN employes_fonctions ef ON em.key = ef.employes_key AND ef.fonctions_key IN (15,8,12)
                WHERE et.actif = '1'
                AND es.actif = '1'
                AND em.actif = '1'
                ORDER BY et.nom ASC"
            );
        }

        $tosent = array();
        $sql = "";
        while ($DB->next_record()) {
            if (!isset($alreadysent[strtolower($DB->getField("courriel"))])) {
                $alreadysent[strtolower($DB->getField("courriel"))] = 1;
                $tosent[strtolower($DB->getField("courriel"))] = 1;
            }
        }

        foreach ($tosent as $email => $go) {
            $DB->query("INSERT INTO dossiers_alertes (`date`, `dossiers_key`, `description`, `email`) VALUES ('" . date("Y-m-d H:i:s") . "', " . $parameters["key"] . ", 'new-mandat', '" .strtolower($email)  . "')");

            sendAlert("new-mandat", "info@pmeinter.com", $mail, $parameters);
        }

    }

    function sendAlert($type, $from, $to, $data) {
        global $DB;

        $subject = "";
        $message = "";
        $old_to = $to;

        switch ($type) {

            case 'new-mandat':
                $subject = "Nouveau mandat publié sur l'extranet PME Inter";
                $message =
                    '<p>Bonjour,<br /><br />
                    Un nouveau mandat est disponible sur l’extranet dans la section <a href="http://www.pmeinter.com/extranet/mandats/">Mandats</a> par ' . $data["etude_nom"] . ' et ayant pour titre ' . $data["titre"] . '.<br /><br />
                    Le dossier doit être complété avant : ' . $data["deadline"] . '<br /><br />
                    Pour tous les détails, écrire au demandeur et/ou répondre à cette demande, <a href="http://www.pmeinter.com/extranet/mandats/#mandat' . $data["key"] . '">cliquez ici</a>.<br /><br />
                    Le réseau PME INTER Notaires, un réseau d’experts au service les uns des autres!<br /><br />
                    Merci de votre précieuse collaboration,<br /><br />
                    L’équipe du siège social</p>';

                break;

            case 'new-comment':
                $subject = "Nouveau commentaire sur votre offre de mandat sur l'extranet PME Inter";
                $message =
                    '<p>Bonjour,<br /><br />
                    Un nouveau commentaire a été rédigé pour le mandat ' . $data["titre"] . ' publié par ' . $data["etude_nom"] . ' sur notre système de référencement de mandat.<br /><br />
                    Pour le consulter et/ou répondre à ce dernier, <a href="http://www.pmeinter.com/extranet/mandats/#mandat' . $data["key"] . '">cliquez ici</a>.<br /><br />
                    Le réseau PME INTER Notaires, un réseau d’experts au service les uns des autres!<br /><br />
                    Merci de votre précieuse collaboration,<br /><br />
                    L’équipe du siège social</p>';

                break;

            case 'new-reply':
                $subject = "Nouvelle réponse sur votre commentaire sur l'extranet PME Inter";

                $message =
                    '<p>Bonjour,<br /><br />
                    Un nouveau commentaire a été rédigé pour le mandat ' . $data["titre"] . ' publié par ' . $data["etude_nom"] . ' sur notre système de référencement de mandat.<br /><br />
                    Pour le consulter et/ou répondre à ce dernier, <a href="http://www.pmeinter.com/extranet/mandats/#mandat' . $data["key"] . '">cliquez ici</a>.<br /><br />
                    Le réseau PME INTER Notaires, un réseau d’experts au service les uns des autres!<br /><br />
                    Merci de votre précieuse collaboration,<br /><br />
                    L’équipe du siège social</p>';

                break;


            case 'new-applicant':

                $subject = "Application sur votre mandat sur l'extranet PME Inter";

                $message =
                    '<p>Bonjour,<br /><br />
                    L’étude ' . $data["etude_nom"] . ' a appliqué pour le mandat suivant ' . $data["titre"] . ' devant être complété avant ' . $data["deadline"] . '.<br /><br />
                    Pour tous les détails, et/ou répondre à cette offre, <a href="http://www.pmeinter.com/extranet/mandats/#mandat' . $data["key"] . '">cliquez ici</a>.<br /><br />
                    Si vous n’avez plus besoin de ressources externes pour ce mandat, merci de supprimer, annuler ou marquer la demande comme complétée.<br /><br />
                    Le réseau PME INTER Notaires, un réseau d’experts au service les uns des autres!<br /><br />
                    Merci de votre précieuse collaboration,<br /><br />
                    L’équipe du siège social</p>';
                    //<p>De : ' . $from . '</p>

                break;

        }

        // $ok_emails = array("tfrechette@notairesmontreal.pro", "btanguay@notarius.net", "apomerleau@notarius.net", "robert.williamson@pfdnotaires.com", "jlapierre@pmegatineau.ca", "manon.tousignant@notarius.net", "mtremblay@ptfl.ca", "info@pmeinter.com", "gdeblois@pmeinter.com", "lgravel@pmeinter.com", "viensf@gmail.com");
        // $ok_emails = array("viensf@gmail.com");

        // //if (!in_array($to, $ok_emails)) { $to = "info@pmeinter.com"; }//"viensf@gmail.com"; }
        // if (!in_array($to, $ok_emails)) { $to = "viensf@gmail.com"; }

        $headers = array("Content-Type: text/html; charset=UTF-8");
        //$headers[] = 'Bcc: viensf@gmail.com';

        wp_mail($to, $subject, $message, $headers);

    }


    function convertToApplication($string) {

        $string = str_replace("add-comment", "apply", $string);
        $string = str_replace("Envoyer", "Appliquer", $string);
        return $string;
    }

















