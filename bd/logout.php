<?PHP

	session_start();
	
	require "conf/conf.inc.php";

	$_SESSION = array();

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "logout");

	$successString .= _success("Vous vous &ecirc;tes d&eacute;connect&eacute; avec succ&egrave;s.");
	$contentString .= "<a href=\"" . $BASEURL . "index.php\">Retourner &agrave; l'accueil.</a>\n";

	$Skin->assign("BASEURL", $BASEURL);
	$Skin->assign("page", "logout");
	$Skin->assign("title", "D&eacute;connexion");
	$Skin->assign("success", $successString);
	$Skin->assign("content", $contentString);
	$Skin->display("logout.tpl");


?>