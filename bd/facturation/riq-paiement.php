<?PHP
	require "../conf/conf.inc.php";

	session_start();
	
	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1)) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous ne poss&eacute;dez pas les droits requis pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$form = getorpost('form');
	$action = getorpost('action');
	$paiementKey = getorpost('paiementKey');

	$noticeArray = array();
	$errorArray = array();

	switch (strtolower($action)) {
		case 'ajouter':
			$errorArray = getErrorArray(getValuesFromPost());
			if (count($errorArray) > 0) {
				$Skin->assign('errorArray', $errorArray);
			} else {
				$save = addPaiementIntoDb(getValuesFromPost());
				if ($save) {
					$noticeArray[] = "Le paiement a &eacute;t&eacute; ajout&eacute; avec succ&egrave;s.";
				} else {
					$errorArray[] = "Une erreur s'est produite lors de l'enregistrement du paiement.";
				}
			}		
			break;

		case 'supprimer':
			$errorArray = getErrorArray(getValuesFromPost());
			if (count($errorArray) > 0) {
				$Skin->assign('errorArray', $errorArray);
			} else {
				$del = deletePaiementFromDb($paiementKey);
				if ($del) {
					$noticeArray[] = "Le paiement a &eacute;t&eacute; supprim&eacute; avec succ&egrave;s.";
				} else {
					$errorArray[] = "Une erreur s'est produite lors de la suppression du paiement.";
				}
			}		
			break;
	}

	if ($action == 'ajout') {
		$Skin->assign("title", "Ajout de paiement - Facturation RIQ - R&eacute;seau IQ");
	} elseif ($action == 'suppression') {
		$Skin->assign("title", "Suppression de paiement - Facturation RIQ - R&eacute;seau IQ");
	} else {
		$Skin->assign("title", "Paiements - Facturation RIQ - R&eacute;seau IQ");
	}

	$Skin->assign("JS", array('facturation'));
	$Skin->assign("CSS", array('facturation'));
	$Skin->assign('BASEURL', $BASEURL);
	$Skin->assign('paiementsArray', getPaiementsArray());
	$Skin->assign('anneesArray', getAnneesArray());
	$Skin->assign('params', getValuesFromPost());
	$Skin->assign('errorArray', $errorArray);
	$Skin->assign('noticeArray', $noticeArray);

	if ($form == 'suppression') {
		$bodyparams = "onLoad=\"javascript: loadPaiements('Del');\"";
		$Skin->assign('bodyparams', $bodyparams);
	}

	$Skin->display("facturation/riq-paiement-" . $form . ".tpl");

	closeDbLinks();
	exit();



	// -------------
	// LES FONCTIONS
	// -------------

	function getValuesFromPost() {
		$values = array();
		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$values[$field] = $value;
				} else {
					$values[$field] = trim(stripslashes($value));
				}
			}
		}
		return $values;
	}

	function getAnneesArray() {
		$minYear = 2000;
		$maxYear = date('Y') + 2;
		
		$anneesArray = array();
		for ($i = $minYear; $i <= $maxYear; $i++) {
			$anneesArray[$i] = $i;
		}
		
		return $anneesArray;
	}

	function getPaiementsArray() {
		global $DB;
		$paiementsArray = array();
		$query = "SELECT `key`, `date`, `montant_paiement`, `numero_facture`, `numero_cheque` FROM `reseauiq_admin_riq_paiements` ORDER BY `date` DESC;";
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$annee = substr($record['date'], 0, 4);
			$paiementsArray[$annee][$record['key']] = $record['date'] . " - " . $record['montant_paiement'] . "\$ " . "(" . $record['numero_facture'] . " / " . $record['numero_cheque'] . ")";
		}
		return $paiementsArray;
	}

	function addPaiementIntoDb($params = array()) {
		global $DB;

		$query =
			"INSERT INTO `reseauiq_admin_riq_paiements` (".
			"	`date`, ".
			"	`montant_paiement`, ".
			"	`numero_facture`, ".
			"	`numero_cheque` ".
			") VALUES (".
			"	'" . mysql_real_escape_string($params['date']) . "', ".
			"	'" . mysql_real_escape_string($params['montant_paiement']) . "', ".
			"	'" . mysql_real_escape_string($params['numero_facture']) . "', ".
			"	'" . mysql_real_escape_string($params['numero_cheque']) . "' ".
			");";
			
		return $DB->query($query);
	}

	function deletePaiementFromDb($paiementKey = '') {
		global $DB;

		if (trim($paiementKey) != '') {
			$query = "DELETE FROM `reseauiq_admin_riq_paiements` WHERE `key` = '" . mysql_real_escape_string($paiementKey) . "';";
			$DB->query($query);
			return true;
		}

		return false;
	}

	function getErrorArray($values = array()) {
		$errorArray = array();

		return $errorArray;
	}

	function closeDbLinks() {
		global $DB, $DB2, $DB3;
		if ($DB != "") {
			$DB->close();
		}
		if ($DB2 != "") {
			$DB2->close();
		}
		if ($DB3 != "") {
			$DB3->close();
		}
	}

?>
