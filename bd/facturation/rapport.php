<?PHP

	require "../conf/conf.inc.php";

	session_start();
	
	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1)) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous ne poss&eacute;dez pas les droits requis pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$type = getorpost('type');
	$action = getorpost('action');
	$dateDebut = trim(getorpost('date_debut'));
	$dateFin = trim(getorpost('date_fin'));
	$taxes['TPS'] = getorpost('taxe_tps');
	$taxes['TVQ'] = getorpost('taxe_tvq');

	$noticeArray = array();
	$errorArray = array();

	switch (strtolower($action)) {
		case 'display':
			$errorArray = getErrorArray(getValuesFromPost());
			if (count($errorArray) > 0) {
				$template = 'rapport-form';
			} else {
				switch ($type) {
					case 'global':
						$Skin->assign('reportArray', getRapportGlobalArray(getEtudesArray(), $dateDebut, $dateFin, $taxes));
						$Skin->assign('dateDebut', $dateDebut);
						$Skin->assign('dateFin', $dateFin);
						$Skin->assign('wider', 1);
						$template = 'rapport-global';
						break;

					case 'factures':
						$Skin->assign('reportArray', getRapportFacturesArray(getEtudesArray(), $dateDebut, $dateFin));
						$Skin->assign('dateDebut', $dateDebut);
						$Skin->assign('dateFin', $dateFin);
						$Skin->assign('wider', 1);
						$template = 'rapport-factures';
						break;
				}
			}
			break;

		default:
			$Skin->assign('etudesArray', getEtudesArray());
			$template = 'rapport-form';
	}

	$Skin->assign("title", "Rapport - Facturation - R&eacute;seau IQ");
	$Skin->assign("JS", array('facturation'));
	$Skin->assign("CSS", array('facturation'));
	$Skin->assign("CSS_PRINT", array('print-facturation-rapport'));
	$Skin->assign('BASEURL', $BASEURL);
	$Skin->assign('errorArray', $errorArray);

	$Skin->display("facturation/" . $template . ".tpl");

	closeDbLinks();
	exit();



	// -------------
	// LES FONCTIONS
	// -------------

	function getValuesFromPost() {
		$values = array();
		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$values[$field] = $value;
				} else {
					$values[$field] = trim(stripslashes($value));
				}
			}
		}
		return $values;
	}

	function getEtudesArray() {
		global $DB;
		$query = "SELECT `key`, `nom` FROM `etudes` WHERE `actif` = '1';";
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$etudesArray[$record['key']] = $record['nom'];
		}
		return $etudesArray;
	}

	function getRapportGlobalArray($etudesArray = array(), $dateDebut = '', $dateFin = '', $taxes = array()) {
		global $DB;
		
		$taxeTPS = toFloat($taxes['TPS']);
		$taxeTVQ = toFloat($taxes['TVQ']);
		$reportArray = array();
		
		if (
			is_array($etudesArray) && count($etudesArray) > 0 &&
			isDateValid($dateDebut) && isDateValid($dateFin)
		) {
			
			foreach ($etudesArray as $etudeKey => $etudeNom) {
				$reportArray[$etudeKey] = array();
				$reportArray[$etudeKey]['info'] = array();
				$reportArray[$etudeKey]['paiements'] = array();
				
				$reportArray[$etudeKey]['info']['etude_nom'] = $etudeNom;
				$reportArray[$etudeKey]['info']['etude_key'] = $etudeKey;
				//$reportArray[$etudeKey]['info']['solde_precedent'] = getSoldePrecedent($etudeKey, $dateDebut);
				$reportArray[$etudeKey]['info']['soustotal_ristournes'] = getSousTotalRistournes($etudeKey, $dateDebut, $dateFin);
				$reportArray[$etudeKey]['info']['tps_pourcent'] = $taxeTPS;
				$reportArray[$etudeKey]['info']['tvq_pourcent'] = $taxeTVQ;
				$reportArray[$etudeKey]['info']['tps_amount'] = round(($reportArray[$etudeKey]['info']['soustotal_ristournes'] * (1 + $taxeTPS / 100)) - $reportArray[$etudeKey]['info']['soustotal_ristournes'], 2, PHP_ROUND_HALF_UP);
				$reportArray[$etudeKey]['info']['tvq_amount'] = round((($reportArray[$etudeKey]['info']['soustotal_ristournes'] + $reportArray[$etudeKey]['info']['tps_amount']) * (1 + $taxeTVQ / 100)) - ($reportArray[$etudeKey]['info']['soustotal_ristournes'] + $reportArray[$etudeKey]['info']['tps_amount']), 2, PHP_ROUND_HALF_UP);
				$reportArray[$etudeKey]['info']['total_ristournes'] = $reportArray[$etudeKey]['info']['soustotal_ristournes'] + $reportArray[$etudeKey]['info']['tps_amount'] + $reportArray[$etudeKey]['info']['tvq_amount'];
				//$reportArray[$etudeKey]['info']['solde_initial'] = $reportArray[$etudeKey]['info']['solde_precedent'] + $reportArray[$etudeKey]['info']['total_ristournes'];
				$reportArray[$etudeKey]['info']['solde_initial'] = $reportArray[$etudeKey]['info']['total_ristournes'];
				$reportArray[$etudeKey]['info']['solde_final'] = $reportArray[$etudeKey]['info']['solde_initial'];
								
				$query =
					"SELECT * ".
					"FROM `reseauiq_admin_paiements` ".
					"WHERE ".
					"	`etudes_key` = '" . mysql_real_escape_string($etudeKey) . "' AND ".
					"	`date_debut` >= '" . mysql_real_escape_string($dateDebut) . "' AND ".
					"	`date_fin` <= '" . mysql_real_escape_string($dateFin) . "' ".
					"ORDER BY `numero_cheque`;";

				$DB->query($query);

				$i = 0;
				while ($record = $DB->next_record()) {
					$paiement = toFloat($record['montant_paiement']);
					//$solde = $reportArray[$etudeKey]['info']['solde_precedent'] + $reportArray[$etudeKey]['info']['total_ristournes'] - $paiement;
					$solde = $reportArray[$etudeKey]['info']['total_ristournes'] - $paiement;

					$reportArray[$etudeKey]['paiements'][$i] = array();
					$reportArray[$etudeKey]['paiements'][$i]['numero_facture'] = $record['reseauiq_admin_factures_key'];
					$reportArray[$etudeKey]['paiements'][$i]['numero_cheque'] = $record['numero_cheque'];
					$reportArray[$etudeKey]['paiements'][$i]['montant_paiement'] = $paiement;
					$reportArray[$etudeKey]['paiements'][$i]['solde'] = $solde;
					$reportArray[$etudeKey]['info']['solde_final'] -= $paiement;
					$i++;
				}
			}
		}
		
		return $reportArray;
	}

	function getSousTotalRistournes($etudeKey = '', $dateDebut = '', $dateFin = '') {
		global $DB;

		$totalRistournes = 0.00;

		if ($etudeKey != '' && isDateValid($dateDebut) && isDateValid($dateFin))  {

			$query =
				"SELECT `montant_ristourne` ".
				"FROM `reseauiq_factures` ".
				"WHERE ".
				"	`etudes_key` = '" . mysql_real_escape_string($etudeKey) . "' AND ".
				"	`date_facture` >= '" . mysql_real_escape_string($dateDebut) . "' AND ".
				"	`date_facture` <= '" . mysql_real_escape_string($dateFin) . "';";

			$DB->query($query);

			while ($record = $DB->next_record()) {
				$totalRistournes += toFloat($record['montant_ristourne']);
			}
		}
		
		return $totalRistournes;
	}

	/*function getSoldePrecedent($etudeKey = '', $dateDebut = '') {
		global $DB;

		$soldePrecedent = 0.00;

		if ($etudeKey != '' && isDateValid($dateDebut))  {

			$query =
				"SELECT `montant_ristourne` ".
				"FROM `reseauiq_factures` ".
				"WHERE ".
				"	`etudes_key` = '" . mysql_real_escape_string($etudeKey) . "' AND ".
				"	`date_facture` < '" . mysql_real_escape_string($dateDebut) . "';";
			
			$DB->query($query);

			while ($record = $DB->next_record()) {
				$soldePrecedent += toFloat($record['montant_ristourne']);
			}

			$DB->reset();
				
			$query =
				"SELECT `montant_paiement` ".
				"FROM `reseauiq_admin_paiements` ".
				"WHERE ".
				"	`etudes_key` = '" . mysql_real_escape_string($etudeKey) . "' AND ".
				"	`date_debut` < '" . mysql_real_escape_string($dateDebut) . "';";

			$DB->query($query);

			while ($record = $DB->next_record()) {
				$soldePrecedent -= toFloat($record['montant_paiement']);
			}
		}
		
		return $soldePrecedent;
	}*/

	function getRapportFacturesArray($etudesArray = array(), $dateDebut = '', $dateFin = '') {
		global $DB;
		
		$reportArray = array();
		
		if (
			is_array($etudesArray) && count($etudesArray) > 0 &&
			isDateValid($dateDebut) && isDateValid($dateFin)
		) {
			
			foreach ($etudesArray as $etudeKey => $etudeNom) {
				$reportArray[$etudeKey] = array();
				$reportArray[$etudeKey]['info'] = array();
				$reportArray[$etudeKey]['factures'] = array();
				
				$reportArray[$etudeKey]['info']['etude_nom'] = $etudeNom;
				$reportArray[$etudeKey]['info']['etude_key'] = $etudeKey;
								
				$query =
					"SELECT * ".
					"FROM `reseauiq_admin_factures` ".
					"WHERE ".
					"	`etudes_key` = '" . mysql_real_escape_string($etudeKey) . "' AND ".
					"	`date_debut` >= '" . mysql_real_escape_string($dateDebut) . "' AND ".
					"	`date_fin` <= '" . mysql_real_escape_string($dateFin) . "' ".
					"ORDER BY `key`;";

				$DB->query($query);

				$i = 0;
				while ($record = $DB->next_record()) {
					$reportArray[$etudeKey]['factures'][$i] = array();
					$reportArray[$etudeKey]['factures'][$i]['numero_facture'] = $record['key'];
					$reportArray[$etudeKey]['factures'][$i]['montant_facture'] = toFloat($record['montant_facture']);
					$i++;
				}
			}
		}
		
		return $reportArray;
	}


	function toMoneyFormat($str = '') {
		if (trim($str) == '') {
			return('0,00&nbsp;$');
		}

		$str = str_replace(' ', '', $str);
		$str = str_replace('&nbsp;', '', $str);
		$str = str_replace(',', '.', $str);
				
		$str = number_format($str, 2, ',', ' ');
		if (strpos($str, ',') === false) {
			$str = $str . ',';
		}

		$str = $str . str_repeat('0', 2 - strpos(strrev($str), ','));
		$str = str_replace(' ', '&nbsp;', $str);
		$str = $str . '&nbsp;$';

		return $str;
	}

	function truncateString($str = '', $len = 0, $dots = 1) {
		if (strlen($str) > $len) {
			if ($dots) {
				return trim(substr($str, 0, $len)) . '...';
			} else {
				return trim(substr($str, 0, $len));
			}
		}
		return $str;
	}

	function isDateValid($dateStr = '') {
			$dateRegex = '/^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/';
			if ($dateStr == '' || !preg_match($dateRegex, $dateStr, $matches)) {
				return false;
			}
			return true;
	}

	function toFloat($str) {
		return((float)str_replace(" ", "", str_replace(",", ".", $str)));
	}

	function getErrorArray($values = array()) {
		$errorArray = array();

		if ($values['type'] != 'global' && $values['type'] != 'factures') {
			$errorArray[] = "D&eacute;sol&eacute;, ce type de rapport n'est pas encore disponible.";
		}

		if (!isDateValid($values['date_debut'])) {
			$errorArray[] = "La date de d&eacute;but est invalide.";
		}

		if (!isDateValid($values['date_fin'])) {
			$errorArray[] = "La date de fin est invalide.";
		}

		return $errorArray;
	}

	function closeDbLinks() {
		global $DB, $DB2, $DB3;
		if ($DB != "") {
			$DB->close();
		}
		if ($DB2 != "") {
			$DB2->close();
		}
		if ($DB3 != "") {
			$DB3->close();
		}
	}

?>
