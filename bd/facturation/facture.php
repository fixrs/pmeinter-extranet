<?PHP
	require "../conf/conf.inc.php";

	session_start();
	
	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1)) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous ne poss&eacute;dez pas les droits requis pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$form = getorpost('form');
	$action = getorpost('action');
	$etudeKey = getorpost('etudeKey');
	$factureKey = getorpost('factureKey');

	$noticeArray = array();
	$errorArray = array();

	switch (strtolower($action)) {
		case 'ajouter':
			$errorArray = getErrorArray(getValuesFromPost());
			if (count($errorArray) > 0) {
				$Skin->assign('errorArray', $errorArray);
			} else {
				$save = addFactureIntoDb(getValuesFromPost());
				if ($save) {
					$noticeArray[] = "La facture a &eacute;t&eacute; ajout&eacute;e avec succ&egrave;s.";
				} else {
					$errorArray[] = "Une erreur s'est produite lors de l'enregistrement de la facture.";
				}
			}		
			break;

		case 'supprimer':
			$errorArray = getErrorArray(getValuesFromPost());
			if (count($errorArray) > 0) {
				$Skin->assign('errorArray', $errorArray);
			} else {
				$del = deleteFactureFromDb($factureKey);
				if ($del) {
					$noticeArray[] = "La facture a &eacute;t&eacute; supprim&eacute;e avec succ&egrave;s.";
				} else {
					$errorArray[] = "Une erreur s'est produite lors de la suppression de la facture.";
				}
			}		
			break;
	}

	if ($action == 'ajout') {
		$Skin->assign("title", "Ajout de facture - Facturation - R&eacute;seau IQ");
	} elseif ($action == 'suppression') {
		$Skin->assign("title", "Suppression de facture - Facturation - R&eacute;seau IQ");
	} else {
		$Skin->assign("title", "Facturation - R&eacute;seau IQ");
	}

	$Skin->assign("JS", array('facturation'));
	$Skin->assign("CSS", array('facturation'));
	$Skin->assign('BASEURL', $BASEURL);
	$Skin->assign('etudesArray', getEtudesArray());
	$Skin->assign('facturesArray', getFacturesArray());
	$Skin->assign('datesArray', getDatesArray());
	$Skin->assign('params', getValuesFromPost());
	$Skin->assign('etudeKey2', $etudeKey);
	$Skin->assign('errorArray', $errorArray);
	$Skin->assign('noticeArray', $noticeArray);

	$Skin->display("facturation/facture-" . $form . ".tpl");

	closeDbLinks();
	exit();



	// -------------
	// LES FONCTIONS
	// -------------

	function getValuesFromPost() {
		$values = array();
		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$values[$field] = $value;
				} else {
					$values[$field] = trim(stripslashes($value));
				}
			}
		}
		return $values;
	}

	function getEtudesArray() {
		global $DB;
		$query = "SELECT `key`, `nom` FROM `etudes` WHERE `actif` = '1';";
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$etudesArray[$record['key']] = $record['nom'];
		}
		return $etudesArray;
	}

	function getFacturesArray() {
		global $DB;
		$facturesArray = array();
		$query = "SELECT `key`, `etudes_key`, `date_debut`, `date_fin`, `montant_facture` FROM `reseauiq_admin_factures` ORDER BY `date_debut` DESC;";
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$facturesArray[$record['etudes_key']][$record['key']] = $record['key'] . " - " . $record['montant_facture'] . "\$ " . "(" . $record['date_debut'] . " au " . $record['date_fin'] . ")";
		}
		return $facturesArray;
	}

	function getDatesArray() {
		global $DB;
		$datesArray = array();
		$query = "SELECT `key`, `date_debut`, `date_fin`, `date_fin` FROM `reseauiq_admin_factures` ORDER BY `date_debut` DESC;";
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$datesArray[$record['key']]['date_debut'] = $record['date_debut'];
			$datesArray[$record['key']]['date_fin'] = $record['date_fin'];
		}
		return $datesArray;
	}

	function addFactureIntoDb($params = array()) {
		global $DB;

		$query =
			"INSERT INTO `reseauiq_admin_factures` (".
			"	`key`, ".
			"	`etudes_key`, ".
			"	`date_inscription`, ".
			"	`date_debut`, ".
			"	`date_fin`, ".
			"	`montant_facture` ".
			") VALUES (".
			"	'" . mysql_real_escape_string($params['factureKey']) . "', ".
			"	'" . mysql_real_escape_string($params['etudeKey']) . "', ".
			"	'" . mysql_real_escape_string(date('Y-m-d')) . "', ".
			"	'" . mysql_real_escape_string($params['date_debut']) . "', ".
			"	'" . mysql_real_escape_string($params['date_fin']) . "', ".
			"	'" . mysql_real_escape_string($params['montant_facture']) . "' ".
			");";
			
		return $DB->query($query);
	}

	function deleteFactureFromDb($factureKey = '') {
		global $DB;

		if (trim($factureKey) != '') {
			$query = "DELETE FROM `reseauiq_admin_factures` WHERE `key` = '" . mysql_real_escape_string($factureKey) . "';";
			$DB->query($query);
			return true;
		}

		return false;
	}

	function getErrorArray($values = array()) {
		$errorArray = array();

		return $errorArray;
	}

	function closeDbLinks() {
		global $DB, $DB2, $DB3;
		if ($DB != "") {
			$DB->close();
		}
		if ($DB2 != "") {
			$DB2->close();
		}
		if ($DB3 != "") {
			$DB3->close();
		}
	}

?>
