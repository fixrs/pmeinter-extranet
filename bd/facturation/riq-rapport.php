<?PHP
	require "../conf/conf.inc.php";

	session_start();
	
	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1)) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous ne poss&eacute;dez pas les droits requis pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$type = getorpost('type');
	$action = getorpost('action');
	$dateDebut = trim(getorpost('date_debut'));
	$dateFin = trim(getorpost('date_fin'));
	$cols2show = $_POST['cols'];
	$rows2show = $_POST['rows'];
	$taxes['TPS'] = getorpost('taxe_tps');
	$taxes['TVQ'] = getorpost('taxe_tvq');
	
	$noticeArray = array();
	$errorArray = array();

	switch (strtolower($type)) {
		default:
			switch (strtolower($action)) {
				case 'display':
					$errorArray = getErrorArray(getValuesFromPost());
					if (count($errorArray) > 0) {
						$template = 'riq-rapport-form';
					} else {
						assignReportValues(getEtudesArray(), $dateDebut, $dateFin, $cols2show, $rows2show, $taxes);
						$Skin->assign('dateDebut', $dateDebut);
						$Skin->assign('dateFin', $dateFin);
						$Skin->assign('wider', 1);
						$template = 'riq-rapport-' . $type;
					}		
					break;
		
				default:
					$Skin->assign('etudesArray', getEtudesArray());
					$template = 'riq-rapport-form';
			}
			break;
	}

	$Skin->assign("title", "Rapport - Facturation RIQ - R&eacute;seau IQ");
	$Skin->assign("JS", array('facturation'));
	$Skin->assign("CSS", array('riq-facturation'));
	$Skin->assign("CSS_PRINT", array('print-riq-facturation-rapport'));
	$Skin->assign('BASEURL', $BASEURL);
	$Skin->assign('errorArray', $errorArray);

	$Skin->display("facturation/" . $template . ".tpl");

	closeDbLinks();
	exit();



	// -------------
	// LES FONCTIONS
	// -------------

	function getValuesFromPost() {
		$values = array();
		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$values[$field] = $value;
				} else {
					$values[$field] = trim(stripslashes($value));
				}
			}
		}
		return $values;
	}

	function getEtudesArray() {
		global $DB;
		$query = "SELECT `key`, `nom` FROM `etudes` WHERE `actif` = '1';";
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$etudesArray[$record['key']] = $record['nom'];
		}
		return $etudesArray;
	}

	function assignReportValues($etudesArray = array(), $dateDebut = '', $dateFin = '', $cols2show = array(), $rows2show = array(), $taxes = array()) {
		global $DB, $Skin;
		
		$entriesArray = array();
		$sousTotalArray = array();
		$totalArray = array();
		$paiementsRecusArray = array();
		$paiementsEmisArray = array();
		$cols = array();
		$rows = array();
		$solde_a_recevoir = 0.00;
		$solde_a_verser = 0.00;
		$taxeTPS = toFloat($taxes['TPS']);
		$taxeTVQ = toFloat($taxes['TVQ']);

		$sousTotalArray['honoraires_eligibles'] = 0.00;
		$sousTotalArray['honoraires_non_eligibles'] = 0.00;
		$sousTotalArray['debourses'] = 0.00;
		$sousTotalArray['ristourne_a_verser'] = 0.00;
		$sousTotalArray['ristourne_a_recevoir'] = 0.00;	
		$sousTotalArray['paiements_recus'] = 0.00;
		$sousTotalArray['paiements_emis'] = 0.00;

		if (is_array($cols2show) && count($cols2show) > 0) {
			foreach ($cols2show as $col) {
				$cols[$col] = 1;
			}
		}

		if (is_array($rows2show) && count($rows2show) > 0) {
			foreach ($rows2show as $row) {
				$rows[$row] = 1;
			}
		}

		if (
			is_array($etudesArray) && count($etudesArray) > 0 &&
			isDateValid($dateDebut) && isDateValid($dateFin)
		) {
			
			foreach ($etudesArray as $etudeKey => $etudeNom) {

				$query =
					"SELECT ".
					"	`key`, ".
					"	`montant_ristourne_eligible`, ".			
					"	`montant_honoraires`, ".
					"	`montant_debourses`, ".
					"	`montant_ristourne` ".
					"FROM ".
					"	`reseauiq_factures` ".
					"WHERE ".
					"	`etudes_key` = '" . mysql_real_escape_string($etudeKey) . "' ".
					"	AND `date_facture` >= '" . mysql_real_escape_string($dateDebut) . "' ".
					"	AND `date_facture` <= '" . mysql_real_escape_string($dateFin) . "';";

				$DB->query($query);

				$tmp_entriesArray[$etudeKey]['etude_nom'] = '';
				$tmp_entriesArray[$etudeKey]['honoraires_eligibles'] = 0.00;
				$tmp_entriesArray[$etudeKey]['honoraires_non_eligibles'] = 0.00;
				$tmp_entriesArray[$etudeKey]['debourses'] = 0.00;
				$tmp_entriesArray[$etudeKey]['ristourne_a_recevoir'] = 0.00;
				$tmp_entriesArray[$etudeKey]['ristourne_a_verser'] = 0.00;

				while ($record = $DB->next_record()) {

					$tmp_entriesArray[$etudeKey]['etude_nom'] = $etudeNom;
					$tmp_entriesArray[$etudeKey]['honoraires_eligibles'] += toFloat($record['montant_ristourne_eligible']);
					$tmp_entriesArray[$etudeKey]['honoraires_non_eligibles'] += toFloat($record['montant_honoraires']);
					$tmp_entriesArray[$etudeKey]['debourses'] += toFloat($record['montant_debourses']);
					$tmp_entriesArray[$etudeKey]['ristourne_a_recevoir'] += toFloat($record['montant_ristourne']);
					$tmp_entriesArray[$etudeKey]['ristourne_a_verser'] += toFloat($record['montant_ristourne']) / 5 * 3;
				}

				if ($tmp_entriesArray[$etudeKey]['ristourne_a_recevoir'] > 0) {

					$entriesArray[$etudeKey]['etude_nom'] = $tmp_entriesArray[$etudeKey]['etude_nom'];
					$entriesArray[$etudeKey]['honoraires_eligibles'] = $tmp_entriesArray[$etudeKey]['honoraires_eligibles'];
					$entriesArray[$etudeKey]['honoraires_non_eligibles'] = $tmp_entriesArray[$etudeKey]['honoraires_non_eligibles'];
					$entriesArray[$etudeKey]['debourses'] = $tmp_entriesArray[$etudeKey]['debourses'];
					$entriesArray[$etudeKey]['ristourne_a_recevoir'] = $tmp_entriesArray[$etudeKey]['ristourne_a_recevoir'];
					$entriesArray[$etudeKey]['ristourne_a_verser'] = $tmp_entriesArray[$etudeKey]['ristourne_a_verser'];

					$sousTotalArray['honoraires_eligibles'] += $entriesArray[$etudeKey]['honoraires_eligibles'];
					$sousTotalArray['honoraires_non_eligibles'] += $entriesArray[$etudeKey]['honoraires_non_eligibles'];
					$sousTotalArray['debourses'] += $entriesArray[$etudeKey]['debourses'];
					$sousTotalArray['ristourne_a_recevoir'] += $entriesArray[$etudeKey]['ristourne_a_recevoir'];															
					$sousTotalArray['ristourne_a_verser'] += $entriesArray[$etudeKey]['ristourne_a_verser'];
				}
				
				$DB->reset();
			}

			$query =
				"SELECT ".
				"	* ".
				"FROM ".
				"	`reseauiq_admin_paiements` ".
				"WHERE ".
				"	`date_debut` >= '" . mysql_real_escape_string($dateDebut) . "' ".
				"	AND `date_fin` <= '" . mysql_real_escape_string($dateFin) . "';";

			$DB->query($query);

			while ($record = $DB->next_record()) {
				$paiementsRecusArray[$record['key']]['date_debut'] = $record['date_debut'];
				$paiementsRecusArray[$record['key']]['date_fin'] = $record['date_fin'];
				$paiementsRecusArray[$record['key']]['montant'] = toFloat($record['montant_paiement']);
				$paiementsRecusArray[$record['key']]['etude_key'] = $record['etudes_key'];

				$totalArray['paiements_recus'] += toFloat($record['montant_paiement']);
			}
			
			$query =
				"SELECT ".
				"	* ".
				"FROM ".
				"	`reseauiq_admin_riq_paiements` ".
				"WHERE ".
				"	`date` >= '" . mysql_real_escape_string($dateDebut) . "' ".
				"	AND `date` <= '" . mysql_real_escape_string($dateFin) . "' ".
				"ORDER BY `date` ASC;";

			$DB->query($query);

			while ($record = $DB->next_record()) {
				$paiementsEmisArray[$record['key']]['date'] = $record['date'];
				$paiementsEmisArray[$record['key']]['montant'] = toFloat($record['montant_paiement']);
				$paiementsEmisArray[$record['key']]['numero_facture'] = $record['numero_facture'];
				$paiementsEmisArray[$record['key']]['numero_cheque'] = $record['numero_cheque'];

				$totalArray['paiements_emis'] += toFloat($record['montant_paiement']);
			}
			
			$taxesArray['TPS']['ristourne_a_recevoir'] = round(($sousTotalArray['ristourne_a_recevoir'] * (1 + $taxeTPS / 100)) - $sousTotalArray['ristourne_a_recevoir'], 2, PHP_ROUND_HALF_UP);
			$taxesArray['TPS']['ristourne_a_verser'] = round(($sousTotalArray['ristourne_a_verser'] * (1 + $taxeTPS / 100)) - $sousTotalArray['ristourne_a_verser'], 2, PHP_ROUND_HALF_UP);
			$taxesArray['TVQ']['ristourne_a_recevoir'] = round((($sousTotalArray['ristourne_a_recevoir'] + $taxesArray['TPS']['ristourne_a_recevoir']) * (1 + $taxeTVQ / 100)) - ($sousTotalArray['ristourne_a_recevoir'] + $taxesArray['TPS']['ristourne_a_recevoir']), 2, PHP_ROUND_HALF_UP);
			$taxesArray['TVQ']['ristourne_a_verser'] = round((($sousTotalArray['ristourne_a_verser'] + $taxesArray['TPS']['ristourne_a_verser']) * (1 + $taxeTVQ / 100)) - ($sousTotalArray['ristourne_a_verser'] + $taxesArray['TPS']['ristourne_a_verser']), 2, PHP_ROUND_HALF_UP);

			$totalArray['ristourne_a_recevoir'] = $sousTotalArray['ristourne_a_recevoir'] + $taxesArray['TPS']['ristourne_a_recevoir'] + $taxesArray['TVQ']['ristourne_a_recevoir'];
			$totalArray['ristourne_a_verser'] = $sousTotalArray['ristourne_a_verser'] + $taxesArray['TPS']['ristourne_a_verser'] + $taxesArray['TVQ']['ristourne_a_verser'];

			$solde_a_recevoir = toFloat($totalArray['ristourne_a_recevoir'] - $totalArray['paiements_recus']);			
			$solde_a_verser = toFloat($totalArray['ristourne_a_verser'] - $totalArray['paiements_emis']);			
		}
		
		$Skin->assign("cols", $cols);
		$Skin->assign("rows", $rows);
		$Skin->assign("colspan", count($cols)-1);
		$Skin->assign("entriesArray", $entriesArray);
		$Skin->assign("taxeTPS", $taxeTPS);
		$Skin->assign("taxeTVQ", $taxeTVQ);
		$Skin->assign("taxesArray", $taxesArray);
		$Skin->assign("sousTotalArray", $sousTotalArray);
		$Skin->assign("totalArray", $totalArray);
		$Skin->assign("paiementsRecusArray", $paiementsRecusArray);
		$Skin->assign("paiementsEmisArray", $paiementsEmisArray);
		$Skin->assign("solde_a_recevoir", $solde_a_recevoir);
		$Skin->assign("solde_a_verser", $solde_a_verser);
	}

	function toMoneyFormat($str = '') {
		if (trim($str) == '') {
			return('0,00&nbsp;$');
		}

		$str = str_replace(' ', '', $str);
		$str = str_replace('&nbsp;', '', $str);
		$str = str_replace(',', '.', $str);
				
		$str = number_format($str, 2, ',', ' ');
		if (strpos($str, ',') === false) {
			$str = $str . ',';
		}

		$str = $str . str_repeat('0', 2 - strpos(strrev($str), ','));
		$str = str_replace(' ', '&nbsp;', $str);
		$str = $str . '&nbsp;$';

		return $str;
	}

	function truncateString($str = '', $len = 0, $dots = 1) {
		if (strlen($str) > $len) {
			if ($dots) {
				return trim(substr($str, 0, $len)) . '...';
			} else {
				return trim(substr($str, 0, $len));
			}
		}
		return $str;
	}

	function isDateValid($dateStr = '') {
			$dateRegex = '/^[0-9]{4}-(((0[13578]|(10|12))-(0[1-9]|[1-2][0-9]|3[0-1]))|(02-(0[1-9]|[1-2][0-9]))|((0[469]|11)-(0[1-9]|[1-2][0-9]|30)))$/';
			if ($dateStr == '' || !preg_match($dateRegex, $dateStr, $matches)) {
				return false;
			}
			return true;
	}

	function toFloat($str) {
		return((float)str_replace(" ", "", str_replace(",", ".", $str)));
	}

	function getErrorArray($values = array()) {
		$errorArray = array();

		if ($values['type'] != 'global') {
			$errorArray[] = "D&eacute;sol&eacute;, ce type de rapport n'est pas encore disponible.";
		}

		if (!isDateValid($values['date_debut'])) {
			$errorArray[] = "La date de d&eacute;but est invalide.";
		}

		if (!isDateValid($values['date_fin'])) {
			$errorArray[] = "La date de fin est invalide.";
		}

		return $errorArray;
	}

	function closeDbLinks() {
		global $DB, $DB2, $DB3;
		if ($DB != "") {
			$DB->close();
		}
		if ($DB2 != "") {
			$DB2->close();
		}
		if ($DB3 != "") {
			$DB3->close();
		}
	}

?>
