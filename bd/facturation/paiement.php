<?PHP
	require "../conf/conf.inc.php";

	session_start();
	
	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1)) {
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");
		$Skin->assign("nologin", 1);
		$Skin->assign("errors", _error("Vous ne poss&eacute;dez pas les droits requis pour acc&eacute;der &agrave; cette section."));
		$Skin->display("reseauiq/index.tpl");
		exit;
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "reseauiq");


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$form = getorpost('form');
	$action = getorpost('action');
	$etudeKey = getorpost('etudeKey');
	$paiementKey = getorpost('paiementKey');

	$noticeArray = array();
	$errorArray = array();

	switch (strtolower($action)) {
		case 'ajouter':
			$errorArray = getErrorArray(getValuesFromPost());
			if (count($errorArray) > 0) {
				$Skin->assign('errorArray', $errorArray);
			} else {
				$save = addPaiementIntoDb(getValuesFromPost());
				if ($save) {
					$noticeArray[] = "Le paiement a &eacute;t&eacute; ajout&eacute; avec succ&egrave;s.";
				} else {
					$errorArray[] = "Une erreur s'est produite lors de l'enregistrement du paiement.";
				}
			}		
			break;

		case 'supprimer':
			$errorArray = getErrorArray(getValuesFromPost());
			if (count($errorArray) > 0) {
				$Skin->assign('errorArray', $errorArray);
			} else {
				$del = deletePaiementFromDb($paiementKey);
				if ($del) {
					$noticeArray[] = "Le paiement a &eacute;t&eacute; supprim&eacute; avec succ&egrave;s.";
				} else {
					$errorArray[] = "Une erreur s'est produite lors de la suppression du paiement.";
				}
			}		
			break;
	}

	if ($action == 'ajout') {
		$Skin->assign("title", "Ajout de paiement - Facturation - R&eacute;seau IQ");
	} elseif ($action == 'suppression') {
		$Skin->assign("title", "Suppression de paiement - Facturation - R&eacute;seau IQ");
	} else {
		$Skin->assign("title", "Facturation - R&eacute;seau IQ");
	}

	$Skin->assign("JS", array('facturation'));
	$Skin->assign("CSS", array('facturation'));
	$Skin->assign('BASEURL', $BASEURL);
	$Skin->assign('etudesArray', getEtudesArray());
	$Skin->assign('facturesArray', getFacturesArray());
	$Skin->assign('paiementsArray', getPaiementsArray());
	$Skin->assign('datesArray', getDatesArray());
	$Skin->assign('params', getValuesFromPost());
	$Skin->assign('etudeKey2', $etudeKey);
	$Skin->assign('errorArray', $errorArray);
	$Skin->assign('noticeArray', $noticeArray);

	if ($etudeKey) {
		if ($form == 'ajout') {
			$bodyparams = "onLoad=\"javascript: loadFactures('Add');\"";
			$Skin->assign('bodyparams', $bodyparams);
		} elseif ($form == 'suppression') {
			$bodyparams = "onLoad=\"javascript: loadPaiements('Del');\"";
			$Skin->assign('bodyparams', $bodyparams);
		}
	}

	$Skin->display("facturation/paiement-" . $form . ".tpl");

	closeDbLinks();
	exit();



	// -------------
	// LES FONCTIONS
	// -------------

	function getValuesFromPost() {
		$values = array();
		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$values[$field] = $value;
				} else {
					$values[$field] = trim(stripslashes($value));
				}
			}
		}
		return $values;
	}

	function getEtudesArray() {
		global $DB;
		$query = "SELECT `key`, `nom` FROM `etudes` WHERE `actif` = '1';";
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$etudesArray[$record['key']] = $record['nom'];
		}
		return $etudesArray;
	}

	function getFacturesArray() {
		global $DB;
		$facturesArray = array();
		$query = "SELECT `key`, `etudes_key`, `date_debut`, `date_fin`, `montant_facture` FROM `reseauiq_admin_factures` ORDER BY `date_debut` DESC;";
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$facturesArray[$record['etudes_key']][$record['key']] = $record['key'] . " - " . $record['montant_facture'] . "\$ " . "(" . $record['date_debut'] . " au " . $record['date_fin'] . ")";
		}
		return $facturesArray;
	}

	function getDatesArray() {
		global $DB;
		$datesArray = array();
		$query = "SELECT `key`, `date_debut`, `date_fin`, `date_fin` FROM `reseauiq_admin_factures` ORDER BY `key` DESC;";
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$datesArray[$record['key']]['date_debut'] = $record['date_debut'];
			$datesArray[$record['key']]['date_fin'] = $record['date_fin'];
		}
		return $datesArray;
	}

	function getPaiementsArray() {
		global $DB;
		$paiementsArray = array();
		$query = "SELECT `key`, `etudes_key`, `reseauiq_admin_factures_key`, `date_debut`, `date_fin`, `montant_paiement` FROM `reseauiq_admin_paiements` ORDER BY `date_debut` DESC;";
		$DB->query($query);
		while ($record = $DB->next_record()) {
			$paiementsArray[$record['etudes_key']][$record['key']] = $record['reseauiq_admin_factures_key'] . " - " . $record['montant_paiement'] . "\$ " . "(" . $record['date_debut'] . " au " . $record['date_fin'] . ")";
		}
		return $paiementsArray;
	}

	function addPaiementIntoDb($params = array()) {
		global $DB;

		$query =
			"INSERT INTO `reseauiq_admin_paiements` (".
			"	`etudes_key`, ".
			"	`reseauiq_admin_factures_key`, ".
			"	`date_debut`, ".
			"	`date_fin`, ".
			"	`montant_paiement`, ".
			"	`numero_cheque` ".
			") VALUES (".
			"	'" . mysql_real_escape_string($params['etudeKey']) . "', ".
			"	'" . mysql_real_escape_string($params['factureKey']) . "', ".
			"	'" . mysql_real_escape_string($params['date_debut']) . "', ".
			"	'" . mysql_real_escape_string($params['date_fin']) . "', ".
			"	'" . mysql_real_escape_string($params['montant_paiement']) . "', ".
			"	'" . mysql_real_escape_string($params['numero_cheque']) . "' ".
			");";
			
		return $DB->query($query);
	}

	function deletePaiementFromDb($paiementKey = '') {
		global $DB;

		if (trim($paiementKey) != '') {
			$query = "DELETE FROM `reseauiq_admin_paiements` WHERE `key` = '" . mysql_real_escape_string($paiementKey) . "';";
			$DB->query($query);
			return true;
		}

		return false;
	}

	function getErrorArray($values = array()) {
		$errorArray = array();

		return $errorArray;
	}

	function closeDbLinks() {
		global $DB, $DB2, $DB3;
		if ($DB != "") {
			$DB->close();
		}
		if ($DB2 != "") {
			$DB2->close();
		}
		if ($DB3 != "") {
			$DB3->close();
		}
	}

?>
