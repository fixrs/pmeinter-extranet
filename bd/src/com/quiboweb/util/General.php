<?PHP

/*
	Copyright (C) 2007 Guillaume Legault, QuiboWeb Inc.
	WWW.QUIBOWEB.CA

	Ce logiciel est la propriete de QuiboWeb Inc.
	Il vous est possible de le modifier. Seul QuiboWeb est autorise
	a modifier le logiciel si vous desirez conserver la garantie
	offerte par QuiboWeb. Seul QuiboWeb est autorise a distribuer
	le logiciel.

	This software is the property of QuiboWeb Inc. You can modify it.
	QuiboWeb only is authorized to modify the software if you want
	to preserve the warranty offered by QuiboWeb. QuiboWeb only is
	authorized to distribute the software.
*/

	function getorpost($label) {
		$value = "";
		if (isset($_GET[$label]) && $_GET[$label] != "") {
			$value = $_GET[$label];
		} else {
			if (isset($_POST[$label])) {
				$value = $_POST[$label];
			}
		}
		return $value;
	}

	function postorget($label) {
		$value = "";
		if (isset($_POST[$label]) && $_POST[$label] != "") {
			$value = $_POST[$label];
		} else {
			if (isset($_GET[$label])) {
				$value = $_GET[$label];
			}
		}
		return $value;
	}

	function _error($string, $alt = "") {
		if ($alt) {
			$errorString = "<div class=\"error_" . $alt . "\">" . $string . "</div>\n";
		} else {
			$errorString = "<div class=\"error\">" . $string . "</div>\n";
		}
		return $errorString;
	}

	function _success($string, $alt = "") {
		if ($alt) {
			$successString = "<div class=\"success_" . $alt . "\">" . $string . "</div>\n";
		} else {
			$successString = "<div class=\"success\">" . $string . "</div>\n";
		}
		return $successString;
	}

	function return_bytes($val) {
		$val = trim($val);
		$last = strtolower($val{strlen($val)-1});
		switch ($last) {
			case 'g':
				$val *= 1024;
			case 'm':
				$val *= 1024;
			case 'k':
				$val *= 1024;
		}
	    return $val;
	}

	function getGetParamsToUrlString($leadingQmark = 0, $endingAmp = 0) {
		$getstring = "?";
		if (is_array($_GET) && count($_GET) > 0) {
			foreach ($_GET as $name => $value) {
				$getstring .= $name . "=" . $value . "&amp;";
			}
			if (!$endingAmp) {
				$getstring = substr($getstring, 0, strlen($getstring)-5);
			}
		}
		if (!$leadingQmark) {
			$getstring = substr($getstring, 1, strlen($getstring));
		}
		return $getstring;
	}

	function getPostParamsToUrlString($leadingQmark = 0, $endingAmp = 0) {
		$getstring = "?";
		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $name => $value) {
				$getstring .= $name . "=" . $value . "&amp;";
			}
			if (!$endingAmp) {
				$getstring = substr($getstring, 0, strlen($getstring)-5);
			}
		}
		if (!$leadingQmark) {
			$getstring = substr($getstring, 1, strlen($getstring));
		}
		return $getstring;
	}

	function pr($var) {
		echo "<pre>";
		print_r($var);
		echo "</pre>";
	}

	function fffefix($path = "") {
		if ($path != "") {
			$file = file_get_contents($path);
			$file = trim($file, "\xFF");
			$file = trim($file, "\xFE");
			$fh = fopen($path, 'w');
		    fwrite($fh, $file);
    		fclose($fh);
    		return TRUE;
		}
		return FALSE;
	}

	function noMagicQuotes() {
		if (!ini_get("safe_mode")) {
			ini_set("magic_quotes_gpc", 0);
			ini_set("magic_quotes_sybase", 0);
			ini_set("magic_quotes_runtime", 0);

		} else {
			// Note: Note that when magic_quotes_sybase is ON it completely overrides magic_quotes_gpc.
			// In this case even when magic_quotes_gpc is enabled neither double quotes, backslashes or NULL's will be escaped.
			if (ini_get('magic_quotes_sybase')) {

			}

			if (ini_get('magic_quotes_gpc')) {
				foreach ($_POST as $key => $value) {
					if ($_POST[$key] != "" && strtolower($_POST[$key]) != "array") {
						$_POST[$key] = stripslashes($value);
					}
				}

				foreach ($_GET as $key => $value) {
					if ($_GET[$key] != "" && strtolower($_GET[$key]) != "array") {
						$_GET[$key] = stripslashes($value);
					}
				}
			}

			if (ini_get('magic_quotes_runtime')) {

			}
		}
	}

	function dirsize($directory) {
		if (!is_dir($directory)) {
			return -1;
		}

		$size = 0;

		if ($DIR = opendir($directory)) {
			while (($dirfile = readdir($DIR)) !== FALSE) {
				if (is_link($directory . '/' . $dirfile) || $dirfile == '.' || $dirfile == '..') {
					continue;
				}
				if (is_file($directory . '/' . $dirfile)) {
					$size += filesize($directory . '/' . $dirfile);
				} else {
					if (is_dir($directory . '/' . $dirfile)) {
						$dirSize = dirsize($directory . '/' . $dirfile);
						if ($dirSize >= 0) {
							$size += $dirSize;
						} else {
							return -1;
						}
					}
				}
			}
			closedir($DIR);
		}

		return $size;
	}

	function format_size($rawSize) {
		if ($rawSize / 1048576 > 1) {
			$verboseSize = round($rawSize / 1048576, 1) . 'MB';
		} else {
			if ($rawSize / 1024 > 1) {
				$verboseSize = round($rawSize / 1024, 1) . 'KB';
			} else {
				$verboseSize = round($rawSize, 1) . 'bytes';
			}
		}
		return $verboseSize;
	}

	function unzip($zipfile) {
		$zip = zip_open($zipfile);
		while ($zip_entry = zip_read($zip)) {
			zip_entry_open($zip, $zip_entry);
			if (substr(zip_entry_name($zip_entry), -1) == '/') {
				$zdir = substr(zip_entry_name($zip_entry), 0, -1);
				if (file_exists($zdir)) {
					trigger_error("Directory <strong>" . $zdir . "</strong> already exists.", E_USER_ERROR);
            		return FALSE;
				}
				mkdir($zdir);
			} else {
				$name = zip_entry_name($zip_entry);
				if (file_exists($name)) {
					trigger_error("File <strong>" . $name . "</strong> already exists.", E_USER_ERROR);
					return FALSE;
				}
				$fopen = fopen($name, "w");
				fwrite($fopen, zip_entry_read($zip_entry, zip_entry_filesize($zip_entry)), zip_entry_filesize($zip_entry));
			}
			zip_entry_close($zip_entry);
		}
		zip_close($zip);
		return TRUE;
	}

	function getFileNameFromPath($path) {
		return strrev(substr(strrev($path), 0, strpos(strrev($path), "/")));
	}

