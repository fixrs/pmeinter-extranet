<?PHP

/*
	Copyright (C) 2004 Francois Viens, QuiboWeb Inc.
	WWW.QUIBOWEB.CA

	Ce logiciel est la propriete de QuiboWeb Inc.
	Il vous est possible de le modifier. Seul QuiboWeb est autorise
	a modifier le logiciel si vous desirez conserver la garantie
	offerte par QuiboWeb. Seul QuiboWeb est autorise a distribuer
	le logiciel.

	This software is the property of QuiboWeb Inc. You can modify it.
	QuiboWeb only is authorized to modify the software if you want
	to preserve the warranty offered by QuiboWeb. QuiboWeb only is
	authorized to distribute the software.
*/

function importForm($file) {
	global $FORMS_PATH;
	$path = $FORMS_PATH;
	$path .= DIRECTORY_SEPARATOR . $file. ".php";
	require_once("$path");
}

function forceImportForm($package, $file) {
	global $FORMS_PATH, $FORM_;
	$packages = explode(".", $package);
	$path = $FORMS_PATH;

	for ($i = 0; $i < count($packages); $i++) {
		if (!preg_match("/(quibo|kolibri)/i", $packages[$i])) {
			$path .= DIRECTORY_SEPARATOR.$packages[$i];
		}
	}

	$path .= DIRECTORY_SEPARATOR.$file;
	require("$path");
}


function importSearchDef($module) {
	global $SEARCH_PATH;
	$filename = $SEARCH_PATH . "/" . $module . ".php";
	if (!file_exists($filename)) {
		print ("<p>Le module de recherche sp&eacute;cifi&eacute; est introuvable.</p>\n");
	}
	else {
		include_once($filename);
	}
}

function importBD($package) {
	global $PACKAGES_PATH;
	$packages = explode(".", $package);
	$path = $PACKAGES_PATH;

	for ($i = 0; $i < count($packages); $i++) {
		$path .= DIRECTORY_SEPARATOR.$packages[$i];
	}

	if (file_exists($path . ".php")) {
		$path .= ".php";
	} else if (file_exists($path . ".class.php")) {
		$path .= ".class.php";
	}

	require_once("$path");

}

function testImport($package) {
	global $PACKAGES_PATH;
	$packages = explode(".", $package);
	$path = $PACKAGES_PATH;

	for ($i = 0; $i < count($packages); $i++) {
		$path .= DIRECTORY_SEPARATOR.$packages[$i];
	}

	$path .= ".php";
	if (file_exists($path)) {
		return TRUE;
	}
	else {
		return FALSE;
	}
}

function select($resultSet, $selectName, $valueCol, $labelCol, $multiple, $emptyOption = 0, $emptyLabel = "", $current_value = "", $tabIndex = "", $javascript = "") {
	$select = "";

	if ($tabIndex != "") { $tabIndex = " tabindex=\"" . $tabIndex . "\""; }

	if ($multiple != 0 && $multiple != "") {
		$select .=
			"<select name=\"" . $selectName . "[]\" size=\"" . $multiple . "\" multiple" . $tabIndex . " " . $javascript . ">\n";
	} else {
		$select .=
			"<select name=\"" . $selectName . "\"" . $tabIndex . " " . $javascript . ">\n";
	}

	if ($emptyOption == 1) {
		$select .=
			"	<option id=\"" . $selectName . "\" value=\"\">" . $emptyLabel . "</option>\n";
	}

	while($resultSet->next_record()) {

		if ($current_value == $resultSet->getField($valueCol)) {
			$select .=
				"	<option id=\"" . $selectName . "\" value=\"" . $resultSet->getField($valueCol) . "\" selected>" . $resultSet->getField($labelCol) . "</option>\n";
		} else {
			$select .=
				"	<option id=\"" . $selectName . "\" value=\"" . $resultSet->getField($valueCol) . "\">" . $resultSet->getField($labelCol) . "</option>\n";
		}
	}

	if (gettype($current_value) == "array") {
		for ($g = 0; $g < count($current_value); $g++) {
			$select = preg_replace("/(value=\"" . $current_value[$g] . "\")/", "\$1 selected", $select);
		}
	}

	$select .= "</select>\n";

	return $select;
}

