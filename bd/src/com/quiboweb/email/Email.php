<?PHP

    /*
    Copyright (C) 2004 Francois Viens, QuiboWeb Inc.
    WWW.QUIBOWEB.CA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
    */
    

    /**
     * The Email Object allows to create and send Email
     *
     * @version 1.0
     * @author Francois Viens <viensf@quiboweb.ca>
	 * @package	Quibo
     */
    class Email {
		
		var $Headers;
		var $PartsArray = array();
		var $Parts;
		var $TextMessage;
		
        function Email($FromAddress, $ToAddress, $Subject, $Message, $EOL = "\n") { 
            
			$this->EOL = $EOL;

			$Message = $this->quotedPrintableEncode($this->noslash($Message));

			$this->FromAddress = $FromAddress;
			$this->ReplyTo = $FromAddress;
            $this->ToAddress = $ToAddress;
            $this->Subject = $this->noslash($Subject);
			$this->Message = $Message;
			$this->MIMEVersion = "1.0";
			$this->TextMessage = $Message;
			$this->Priority = 3;
        }

		function send() {			
			mail(
				$this->ToName . "<" . $this->ToAddress . ">", 
				$this->Subject, 
				$this->Message . $this->Parts, 
				$this->Headers
			); 
		}

		function build() {
			$this->buildParts();
			$this->buildHeaders();
		}

		function setFromName($FromName) {
			$this->FromName = $FromName;
		}

		function setToName($ToName) {
			$this->ToName = $ToName;
		}

		function setToAddress($ToAddress) {
			$this->ToAddress = $ToAddress;
		}

		function setReplyTo($ReplyToAddress) {
			$this->ReplyTo = $ReplyToAddress;
		}

		function setMIMEVersion($MIMEVersion) {
			$this->MIMEVersion = $MIMEVersion;
		}

		function setPriority($Priority) {
			$this->Priority = $Priority;
		}

		function addHTMLContent($html) {
			$html = $this->quotedPrintableEncode($html);
			$this->Message = $html;
		}
		
		function addImage($imageName, $name) {
			$this->addPart(
				$this->getBase64Content($imageName), 
				$this->getContentType($this->getExtension($imageName)), 
				$this->EOL . "\tname=\"" . $name . "\"", 
				"base64", 
				"", 
				"Content-ID: <" . MD5($name) . ">" . $this->EOL . $this->EOL
			);	
		}

		function addAttachment($fileName, $name) {
			$this->addPart(
				$this->getBase64Content($fileName), 
				$this->getContentType($this->getExtension($fileName)), 
				$this->EOL . "\tname=\"" . $name . "\"", 
				"base64", 
				"", 
				"", 
				"Content-Disposition: attachment;" . $this->EOL . "\tfilename=\"" . $name . "\"" . $this->EOL . $this->EOL
			);	
		}

		function addPart(
			$part, 
			$content_type, 
			$info, 
			$content_transfert_encoding = "quoted-printable", 
			$content_location = "", 
			$content_id = "", 
			$content_position = ""
		) {
			
			$this->PartsArray[count($this->PartsArray)] =	
				$this->EOL . "------=MIME_BOUNDRY_main_message" . $this->EOL . 
				"Content-Type: " . $content_type . "; " . $info . " " . $this->EOL . 
				"Content-Transfer-Encoding: " . $content_transfert_encoding . $this->EOL . 
				$content_location . 
				$content_id . 
				$content_position . 
				$part . $this->EOL . $this->EOL;
		}

		function getExtension($imageName) {
			$imageName = trim($imageName);
			return preg_replace("/^.*\.(\w+)$/", "\$1", $imageName);
		}

		function shrinkContent($content) {
			return rtrim(chunk_split($content, 76), $this->EOL);
		}

		function getFileContent($fileName) {
			$fileName = trim($fileName);
			$file_content = file($fileName);
			$file = "";
			for ($i = 0; $i < count($file_content); $i++) { 
				$file .= $file_content[$i]; 
			}

			return $this->shrinkContent($file);
		}

		function getBase64Content($imageName) {
			$imageName = trim($imageName);
			$image_content = file($imageName);
			$image = "";
			for ($i = 0; $i < count($image_content); $i++) { 
				$image .= $image_content[$i]; 
			}

			return $this->shrinkContent(base64_encode($image));
		}

		function getContentType($extension) {
			switch ($extension) {
				case "gif":
					return "image/gif";
				case "jpg":
					return "image/jpeg";
				case "jpeg":
					return "image/jpeg";
				case "jpe":
					return "image/jpeg";
				case "html":
					return "text/html";
				case "htm":
					return "text/html";
				case "pdf":
					return "application/pdf";
				case "doc":
					return "application/msword";
				case "css":
					return "text/css";
				case "png":
					return "image/x-png";
				case "tiff":
					return "image/tiff";
				case "avi":
					return "video/x-msvideo";
				case "mpeg":
					return "video/mpeg";
				case "mpg":
					return "video/mpeg";
				case "mpe":
					return "video/mpeg";
				case "rtf":
					return "application/rtf";
				case "zip":
					return "application/zip";
				case "exe":
					return "application/octet-stream";
				case "js":
					return "text/javascript";
				case "pl":
					return "application/x-perl";
				case "pm":
					return "application/x-perl";
				default:
					return "text/plain";
			}
		}


		function buildHeaders() {
			$headers =	
				"From: " . $this->FromName . "<" . $this->FromAddress . ">" . $this->EOL . 
				"Reply-To: <" . $this->ReplyTo . ">" . $this->EOL . 
				"MIME-Version: " . $this->MIMEVersion . $this->EOL . 
				"Content-Type: multipart/related; " . $this->EOL .
				"	type=\"multipart/alternative\";" . $this->EOL . 
				"	boundary=\"----=MIME_BOUNDRY_main_message\"" . $this->EOL . 
				"X-Sender: " . $this->FromName . "<" . $this->FromAddress . ">" . $this->EOL . 
				"X-Mailer: PHP4" . $this->EOL . 
				"X-Priority: " . $this->Priority . $this->EOL . 
				"Return-Path: <" . $this->FromAddress . ">" . $this->EOL . $this->EOL . 

				"This is a multi-part message in MIME format." . $this->EOL . $this->EOL . 
				
				"------=MIME_BOUNDRY_main_message" . $this->EOL . 
				"Content-Type: multipart/alternative;" . $this->EOL . 
				"	boundary=\"----=MIME_BOUNDRY_message_parts\"" . $this->EOL . $this->EOL;
			
			$headers .=
				"_ " . $this->EOL . 
				"------=MIME_BOUNDRY_message_parts" . $this->EOL . 
				"Content-Type: text/plain; charset=ISO-8859-1;" . $this->EOL .
				"Content-Transfer-Encoding: quoted-printable" . $this->EOL . $this->EOL . $this->EOL . 
				$this->TextMessage . $this->EOL . $this->EOL;

			$headers .=				
				"------=MIME_BOUNDRY_message_parts" . $this->EOL . 
				"Content-Type: text/html; charset=\"iso-8859-1\"" . $this->EOL . 
				"Content-Transfer-Encoding: quoted-printable" . $this->EOL;

			$this->Headers = $headers;
		}

		function buildParts() {
			
			$this->Parts .=	$this->EOL . "------=MIME_BOUNDRY_message_parts--" . $this->EOL . $this->EOL;
		
			for ($i = 0; $i < count($this->PartsArray); $i++) {
				$this->Parts .= $this->PartsArray[$i];
			}

			$this->Parts .= "------=MIME_BOUNDRY_main_message--" . $this->EOL . $this->EOL;
		}

		function checkAddress($address) {
			if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $address)) { 
				return 0; 
			} 
			return 1;
		} 

		function quotedPrintableEncode($input , $line_max = 76) {
			$lines  = preg_split("/\r?\n/", $input);
			$eol    = $this->EOL;
			$escape = '=';
			$output = '';

			while(list(, $line) = each($lines)){

				$linlen     = strlen($line);
				$newline = '';

				for ($i = 0; $i < $linlen; $i++) {
					$char = substr($line, $i, 1);
					$dec  = ord($char);

					if (($dec == 32) && ($i == ($linlen - 1))) {    // convert space at eol only
						$char = '=20';

					} else if($dec == 9) {
						// Do nothing if a tab.
					} else if(($dec == 61) || ($dec < 32 ) || ($dec > 126)) {
						$char = $escape . strtoupper(sprintf('%02s', dechex($dec)));
					}

					if ((strlen($newline) + strlen($char)) >= $line_max) {        // MAIL_MIMEPART_CRLF is not counted
						$output  .= $newline . $escape . $eol;                    // soft line break; " =\r\n" is okay
						$newline  = '';
					}
					$newline .= $char;
				} // end of for
				$output .= $newline . $eol;
			}
			
			$output = substr($output, 0, -1 * strlen($eol)); // Don't want last crlf
			return $output;
		}

		function noslash($retour) {
			$retour = preg_replace("/\\\\/", "", $retour);
			return $retour;
		}

    }
?>