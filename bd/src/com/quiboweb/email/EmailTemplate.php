<?PHP
	
	import("com.quiboweb.Email");

	class EmailTemplate {
		
		var $Template = "";
		var $Title = "";
		var $Content = "";
		var $Left = "";
		var $Right = "";
		var $Text = "";
		var $Email;

		function EmailTemplate($subject, $message) {
			


				$this->load($subject, $message);
		}


		function load($subject, $message) {
			$this->Text = $this->stripTags($message);
			
			$this->Email = new Email("info@pmeinter.com", "info@pmeinter.com", $subject, $this->Text);
			$this->Email->setFromName("PME Inter");

			$this->setContent($this->processTags($message));
			$this->prepare();

			$this->Email->addHTMLContent($this->Template);
			$this->Email->build();
		}

		function processTags($string) {
			$string = preg_replace("/\'\'\'(.*?)\'\'\'/", "<b>\$1</b>", $string);
			$string = preg_replace("/\'\'(.*?)\'\'/", "<i>\$1</i>", $string);
			
			$string = preg_replace("/\[LINK\s([^\s]*)\s([^\]]*)\]/", "<a href=\"\$1\" style=\"font-family: helvetica, arial, sans-serif; font-size: 12px; color: #2E930B;\">\$2</a>", $string);
			$string = preg_replace("/<a href=\"([\w_\.-]+\@[\w_-]+\.[\w_-]+(\.[\w_-]+)?(\.[\w_-]+)?(\.[\w_-]+)?(\.[w_\-]+)?)\"/", "<a href=\"mailto:\$1\"", $string);

			$string = preg_replace("/\[COLOR\s([^\]]*)\]/", "<span style=\"color: #2E930B;\">\$1</span>", $string);
			$string = preg_replace("/\[CENTER\s([^\]]*)\]/", "<div align=\"center\" style=\"font-family: helvetica, arial, sans-serif; font-size: 12px; color: #000000;\">\$1</div>", $string);
			$string = preg_replace("/\[RIGHT\s([^\]]*)\]/", "<div align=\"right\" style=\"font-family: helvetica, arial, sans-serif; font-size: 12px; color: #000000;\">\$1</div>", $string);
						
			$lines_array = split("\n", $string);

			$first = 1;
			$last = 0;
			$content = "";
			for ($i = 0; $i < count($lines_array); $i++) {
				$line = $lines_array[$i];
				if (preg_match("/^-/", $line)) {
					if ($first == 1) {
						$first = 0;
						$last = $i;
					} else {
						$first = 0;
						$last = $i;
					}

					$content .= $line;
				} else {
					if ($last == ($i - 1) && $first == 0) {
						$line = "</ul>" . $line;
						$first = 1;
					}

					$content .= $line . "\n";
				}
			}

			$content = nl2br("\n" . trim($content));
			$content = preg_replace("/<\/ul>\s*<br[^>]*>/", "</ul>", $content);
			$content = preg_replace("/<br[^>]*>\s*(<ul[^>]+>)\s*<br[^>]*>/", "\$1", $content);
			$content = preg_replace("/<\/li>/", "</li>\n", $content);

			$match_array = array();
			preg_match("/\[TITLE\s([^\]]*)\]/", $content, $match_array);
			$this->setTitle($match_array[1]);
			$content = preg_replace("/\[TITLE\s([^\]]*)\]/", "", $content);

			$content = preg_replace("/<br[^>]*>\s*<br[^>]*>\s*$/", "", $content);
			$content = preg_replace("/^<br[^>]*>\s*<br[^>]*>\s*/", "", $content);

			return $this->ansi2entity($content, 1);
		}

		function stripTags($string) {
			$string = preg_replace("/\'\'\'/", "", $string);
			$string = preg_replace("/\'\'/", "", $string);
			$string = preg_replace("/\[TITLE\s([^\]]*)\]/", "\$1", $string);
			$string = preg_replace("/\[LINK\s([^\s]*)\s([^\]]*)\]/", "\$2 (\$1)", $string);
			$string = preg_replace("/\[COLOR\s([^\]]*)\]/", "\$1", $string);
			$string = preg_replace("/\[CENTER\s([^\]]*)\]/", "\$1", $string);
			$string = preg_replace("/\[RIGHT\s([^\]]*)\]/", "\$1", $string);
			$string = preg_replace("/\r/", " ", $string);
			
			return "\n" . $string . "\n";
		}

		function setTitle($value) {
			$this->Title = $value;
		}

		function getTitle() {
			return $this->Title;
		}

		function setContent($value) {
			$this->Content = $value;
		}

		function getContent() {
			return $this->Content;
		}

		function prepare() {
			$this->Template = preg_replace("/\{TITLE\}/", $this->getTitle(), $this->Template);
			$this->Template = preg_replace("/\{CONTENT\}/", $this->getContent(), $this->Template);
		}

		function send($email, $name) {
			$this->Email->setToAddress($email);
			$this->Email->setToName($name);
			$this->Email->buildHeaders();
			$this->Email->send();
		}

		function ansi2entity($string, $acceptHTML = 0) {
			$string = htmlentities($string, ENT_NOQUOTES);
			
			if ($acceptHTML == 1) {
				$string = preg_replace("/&lt;/i", "<", $string);
				$string = preg_replace("/&gt;/i", ">", $string);
			}
			
			$string = preg_replace("/&amp;/i", "&", $string);
			$string = preg_replace("/&quot;/i", "\"", $string);
			return $string;
		}

	}
?>
