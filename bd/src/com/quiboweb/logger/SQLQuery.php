<?php
	/*
		Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/
	
	/**
     *
     * @version 1.0
     * @author Jean-Philippe Ricard <ricardjp@quiboweb.ca>
	 * @package	Quibo::logger
     */
	 
	class SQLQuery {

		var $table;
		var $query;
		var $type;

		var $fields;
		var $values;

		var $condition;

		var $STOPWORDS;

		var $_strInvalidQuery = "This query is invalid";

		function SQLQuery($query) {
			$this->STOPWORDS = array(
						"LIMIT",
						"OFFSET",
						"GROUP",
						"ORDER"
					);
			$this->query = $query;
		}

		function isTypeValid() {
			$terms = explode(" ",$this->query);
			if (strtolower($terms[0]) == $this->type) {
				return TRUE;
			}
			return FALSE;
		}

		function trimSingleQuotes($str) {
			if ($str{0} == "'" || $str{0} == "`") {
				$str = substr($str, 1);
			}
			if ((substr($str, -1) == "'" || substr($str, -1) == "`") && $str{strlen($str)-2} != "\\") {
				$str = substr($str, 0, -1);
			}
			return $str;
		}
		
		function getParamsFields($str) {
			$array =  explode(",",$str);
			for ($i = 0; $i < sizeof($array); $i++) {
				$array[$i] = trim($this->trimSingleQuotes(trim($array[$i])));
			}
			return $array;
		}
		
		/* split a string according to params ('param','param', etc) */
		function getParams($str) {
			$inword = FALSE;
			$token = TRUE;
			$word = "";
			$words = array();
			$char = "";
			
			for ($i = 0; $i < strlen($str); $i++) {
				$prev = $char;
				$char = substr($str, $i, 1);
				if ($char == "'") {	
					// escaped quote, is not a tokenizer
					if ($prev == "\\") {
						$word .= $char;
					}
					// we end the word
					else if ($inword) {
						$inword = FALSE;
						array_push($words, $this->trimSingleQuotes($word));
						$word = "";
					}
					// we begin a new word
					else {
						if ($token == TRUE) {
							$token = FALSE;
							$inword = TRUE;
						}
						else {
							die("Malformed query exception");
						}
					}
				}
				// tokenizer between two params
				else if ($char == ",") {
					if (!$inword) {
						$token = TRUE;
					}
					else {
						$word .= $char;
					}
				}
				else if ($char == " ") {
					if ($inword) {
						$word .= $char;
					}
					else {
						// continue, space between commas are tolerated
					}
				}
				// any other char
				else if ($inword) {
					$word .= $char;
				} else if ($char == "N" || $char == "U" || $char == "L") {
					// tolerate
				} 
				else {
					die ("Malformed query exception : found " . $char . " instead of a token");
				}
			}

			return $words;
		}
		
		function getUpdateParams($str) {
			
			$inKey = TRUE;
			$inValue = FALSE;
			$tokenKey = TRUE;	
			$tokenValue = FALSE;
			
			$field = "";
			$value = "";
			
			$fields = array();
			$values = array();
			$char = "";
			
			for ($i = 0; $i < strlen($str); $i++) {
				$prev = $char;
				$char = substr($str, $i, 1);
				
				if ($char == "'") {
					// escaped quote, is not a tokenizer
					if ($prev == "\\") {
						if ($inKey) {
							$field .= $char;
						}
						else if ($inValue) {
							$value .= $char;
						}
					}
					// we end the word
					else if ($inKey) {
						die("Malformed query exception 1 at " . $i . "(" . $char . ")");
					}
					else if ($inValue) {
						$inValue = FALSE;
						array_push($values, $this->trimSingleQuotes($value));
						$value = "";
					}
					// we begin a new word
					else {
						if ($tokenValue == TRUE) {
							$tokenValue = FALSE;
							$inValue = TRUE;
						}
						else {
							die("Malformed query exception 2 at " . $i);
						}
					}
				}
				
				else if ($char == ",") {
					if (!$inKey && !$inValue) {
						$tokenKey = TRUE;
					}
					// key with a comma after ? exit (= should follow)
					else if ($inKey) {
						die("Malformed query exception 3");
					}
					else if ($inValue) {
						$value .= $char;
					}
				}
				
				else if ($char == "=") {
					// = found near a comma or two = consecutive?
					if (!$inKey && !inValue) {
						die("Malformed query exception 4");
					}
					else if ($inKey) {
						$inKey = FALSE;
						$tokenValue = TRUE;
						array_push($fields, $field);
						$field = "";
						
					}
					else if ($inValue) {
						$value .= $char;
					}
				}
				
				else if ($char == " ") {
					if ($inValue) {
						$value .= $char;
					}
					else {
						// continue, space between commas and equals are tolerated
					}
				}
				
				else {
					if ($inKey) {
						$field .= $char;
					}
					else if ($inValue) {
						$value .= $char;
					}
					else {
						if ($tokenKey == TRUE) {
							$tokenKey = FALSE;
							$inKey = TRUE;
							$field .= $char;
						} else if ($char == "N" || $char == "U" || $char == "L") {
							// tolerate
						} 
						else {
							die("Malformed query exception at " . $i . "(" . $char . ")");
						}
					}
				}
			}
			return array($fields, $values);
		}
		
		function getParenthesisTerms($str) {
			$inparenthesis = FALSE;
			$inword = FALSE;
			$token = TRUE;
			$word = "";
			$words = array();
			$char = "";
			
			for ($i = 0; $i < strlen($str); $i++) {
				$prev = $char;
				$char = substr($str, $i, 1);
				if ($char == "(") {
					if ($inparenthesis) {
						if ($inword) {
							$word .= $char;
						}
						else {
							die ("Malformed query exception 1 at " . $i . "(" . $char . ")");
						}
					}
					else {
						if ($inword) {
							$word .= $char;
						}
						else {
							$inparenthesis = TRUE;
						}
					}
				}
				else if ($char == ")") {
					if ($inparenthesis) {
						if ($inword) {
							$word .= $char;
						}
						else {
							$inparenthesis = FALSE;
							array_push($words, $word);
							$word = "";
							$token = TRUE;
						}
					}
					else {
						if ($inword) {
							$word .= $char;
						}
						else {
							die ("Malformed query exception 2");
						}
					}
				}
				else if ($char == "'" && $inparenthesis) {	
					// escaped quote, is not a tokenizer
					if ($prev == "\\") {
						$word .= $char;
					}
					// we end the word
					else if ($inword) {
						$inword = FALSE;
						$word .= $char;
					}
					// we begin a new word
					else {
						if ($token == TRUE) {
							$token = FALSE;
							$inword = TRUE;
							$word .= $char;
						}
						else {
							die("Malformed query exception 3 at " . $i);
						}
					}
				}
				// tokenizer between two params
				else if ($char == "," && $inparenthesis) {
					$word .= $char;
					if (!$inword) {
						$token = TRUE;
					}
				}
				// any other char
				else if ($inparenthesis) {
					$word .= $char;
				}
				
			}

			return $words;			
		}

		function getTable() {
			return $this->table;
		}

		function getType() {
			return $this->type;
		}

		function getValues() {
			return $this->values;
		}

		function getFields() {
			return $this->fields;
		}
	}
	
?>