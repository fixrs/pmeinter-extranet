<?php
	/*
		Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/
	
	/**
     *
     * @version 1.0
     * @author Jean-Philippe Ricard <ricardjp@quiboweb.ca>
	 * @package	Quibo::logger
     */
	 
	import("com.quiboweb.logger.SQLQuery");

	//support - update table set field=value [, field=value] [condition statement]
	// the condition parsing is real bad at this state
	class SQLUpdateQuery extends SQLQuery {

		function SQLUpdateQuery($query) {
			parent::SQLQuery($query);
			$this->type = "update";

			if (!$this->isTypeValid()) {
				die($this->_strInvalidQuery);
			}

			$this->parseQuery();
		}

		function parseQuery() {

			// find the table name
			$terms = explode(" ",$this->query);
			$this->table = $this->trimSingleQuotes($terms[1]);

			// find fields and values
			preg_match("/\bset\b\s(.*?)($|where)/i", $this->query, $matches);
			$assoc = $this->getUpdateParams($matches[1]);
			$this->fields = $assoc[0];
			$this->values = $assoc[1];

			// find the condition - the whole where clause
			preg_match("/\bwhere\b\s.*/i", $this->query, $matches);
			if (isset($matches[0])) {

				// look for malformed query
				$tokens = explode(" ", $matches[0]);
				foreach ($this->STOPWORDS as $word) {
					foreach($tokens as $token) {
						if ($word == strtoupper($token)) {
							die($this->_strInvalidQuery);
						}
					}
				}
				$this->condition = $matches[0];
			}
		}

		function toString() {
			$query = $this->type . " " . $this->table . " SET ";
			for ($i = 0; $i < sizeof($this->fields); $i++) {
				$query .= $this->fields[$i] . "='" . $this->values[$i] . "', ";
			}
			$query = substr($query,0,-2);
			if (isset($this->condition)) {
				$query .= " " . $this->condition;
			}
			return $query;
		}
	}

?>