<?php

/*
	Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
	WWW.QUIBOWEB.CA

	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
*/

import("com.quiboweb.sql.DB");
import("com.quiboweb.logger.Logger");
import("com.quiboweb.logger.SQLQueryParser");

$str['eng']['error_query']		= "The query has failed : ";
$str['eng']['error_connect']	= "Error connecting to the database : ";
$str['eng']['error_select_db']	= "Unable to select specified database : ";
$str['eng']['error_close']		= "Error closing database link : ";


/**
 *
 * @version 1.0
 * @author Jean-Philippe Ricard <ricardjp@quiboweb.ca>
 * @package	Kolibri
 */
class DB_logger {

	var $logger;
	var $str;		// The array used to display messages
	var $db;		// The DB link
	var $result = array();	// The result set returned by the last query
	var $row = array();		// The actual working row of the result set
	var $DB_;
	var $originalInsertedId; // The inserted id of the original table when there is a logger

	function DB_logger($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS = "MySQL") {
		global $str;
		
		$this->str = $str['eng'];			
		import("com.quiboweb.sql.DB");
		
		$this->DB_ = new DB(
			$DB_HOSTNAME, 
			$DB_USER, 
			$DB_PASSWORD, 
			$DB_NAME, 
			$DBMS
		);
	}

	function query($query, $info = NULL) {

		// dont want to log, normal DB query
		if (!$info) {
			return $this->DB_->query($query);
		}

		// we want to log this event
		else {
			
			$parser = new SQLQueryParser($query);
			
			$SQLQuery = $parser->getSQLQuery();

			switch($SQLQuery->type) {
				case "select":
					// we dont log select, proceed as usual
					$this->DB_->query($query);
					break;
				case "insert":
					// we proceed with the insert statement
					$this->DB_->query($query);
					$id = $this->DB_->getInsertId();
					$this->originalInsertedId = $id;
					$this->DB_->reset();

					// we use the fields from the select on this ID
					// in order to fill in the blanks for default fields
					if ($id != 0) {
						$autoField = "";
						// must find the name of the auto increment field
						$this->DB_->query("DESC " . $SQLQuery->getTable());
						while ($this->DB_->next_record()) {
							if ($this->DB_->getField("Extra") == "auto_increment") {
								$autoField = $this->DB_->getField("Field");
							}
						}
						$this->DB_->reset();

						// getting the complete row just inserted
						$this->DB_->query("SELECT * FROM " . $SQLQuery->getTable() . " WHERE `" . $autoField . "` = '" . $id . "'");
						$this->DB_->next_record();
						$row = $this->DB_->getRow();

						// construct a new insert for the logs
						$newquery = "INSERT INTO " . $SQLQuery->getTable() . " VALUES (";
						for ($i = 0; $i < (sizeof($row) / 2); $i++) {
							$newquery .= "'" . addslashes($row[$i]) . "',";
						}
						$newquery = substr($newquery,0,-1);
						$newquery .= ")";
						
						$this->DB_->reset();
					}

					// no auto increment field we describe the table
					// and fill in the blanks
					else {
						$newvalues = array();

						$fields = $SQLQuery->getFields();
						$values = $SQLQuery->getValues();

						$i = 0;
						
						$this->DB_->query("DESC " . $SQLQuery->getTable());
						
						while ($this->DB_->next_record()) {
														
							if (!in_array($this->DB_->getField("Field"), $fields)) {
								$default = $this->DB_->getField("Default");
								if (isset($default)) {
									array_push($newvalues, $this->DB_->getField("Default"));
								}
								else {
									die("Impossible to log query, missing fields in insert");
								}
							}
							else {
								array_push($newvalues, $values[$i]);
								$i++;
							}
						}
						$this->DB_->reset();

						// values are now in order and ready to insert
						$newquery = "INSERT INTO " . $SQLQuery->getTable() . " VALUES (";
						for ($i = 0; $i < sizeof($newvalues); $i++) {
							$newquery .= "'" . addslashes($newvalues[$i]) . "',";
						}
						$newquery = substr($newquery,0,-1);
						$newquery .= ")";
					}

					$this->DB_->query($this->logger->log($newquery, $info));
					break;
				case "delete":
					// we log before since the row will be deleted
					// we do a select on the primary key deleted to retrieve information
					$newquery = "SELECT * FROM " . $SQLQuery->getTable() . " " . $SQLQuery->condition;
					$this->DB_->query($newquery);
					$row = $this->next_record();
					$insertQuery = SQLInsertQuery::getQuery($SQLQuery->getTable(), $row);
					$this->DB_->query($this->logger->log($insertQuery->toString(), $info));
					$this->DB_->reset();
					$this->DB_->query($query);
					break;
				case "update":
					// we log before since the row will be modified
					// we do a select on the primary key deleted to retrieve information
					$this->DB_->query($query);
					
					$newquery = "SELECT * FROM " . $SQLQuery->getTable() . " " . $SQLQuery->condition;
					$this->DB_->query($newquery);
					$row = $this->next_record();
					$insertQuery = SQLInsertQuery::getQuery($SQLQuery->getTable(), $row);
					$this->DB_->query($this->logger->log($insertQuery->toString(), $info));
					
					break;
			}
		}
	}

	function setLogger($logger) {
		$this->logger = $logger;
	}
	
	function getField($field) {
		return $this->DB_->getField($field);
	}

	function reset() {
		$this->DB_->reset();
	}

	function next_record() {
		return $this->DB_->next_record();
	}

	function getRow() {
		return $this->DB_->getRow();
	}

	function num_rows() {
		return $this->DB_->num_rows();
	}
	
	function getInsertId() {
		return $this->DB_->getInsertId();
	}

	function getOriginalInsertedId() {
		return $this->originalInsertedId;
	}

	function close() {
		$this->DB_->close();
	}
}
?>
