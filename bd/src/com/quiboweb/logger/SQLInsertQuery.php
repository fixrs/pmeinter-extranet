<?php
	/*
		Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/
	
	/**
     *
     * @version 1.0
     * @author Jean-Philippe Ricard <ricardjp@quiboweb.ca>
	 * @package	Quibo::logger
     */
	 
	import("com.quiboweb.logger.SQLQuery");

	// only support basic queries - insert into table (fields) values (values)
	//                            - insert into table values (values)
	class SQLInsertQuery extends SQLQuery {

		var $fieldList = TRUE;

		function SQLInsertQuery($query = null) {
			$this->type = "insert";
			if ($query) {
				parent::SQLQuery($query);

				if (!$this->isTypeValid()) {
					die($this->_strInvalidQuery);
				}

				$this->parseQuery();
			}
		}

		// from an array of field=>value, return a new insert statement
		function getQuery($table, $array) {
			$query = new SQLInsertQuery();
			$query->table = $table;
			$query->fieldList = FALSE;
			$values = array();
			for($i = 0; $i < sizeof($array) / 2;  $i++) {
				array_push($values,addslashes(trim($array[$i])));
			}
			$query->values = $values;
			return $query;
		}

		function parseQuery() {

			// find the table name
			$terms = explode(" ",$this->query);
			$this->table = $this->trimSingleQuotes($terms[2]);

			// whether fields are listed or not
			if (strtolower($terms[3]) == "values") {
				$this->fieldList = FALSE;
			}

			// associative array between fields and values

			$parenthesisContent = $this->getParenthesisTerms($this->query);
			//print_r($parenthesisContent);
			
			
			if($this->fieldList) {
				$this->fields = $this->getParamsFields($parenthesisContent[0]);
				#print_r($this->fields);
				$this->values = $this->getParams($parenthesisContent[1]);
				#print_r($this->values);
			}
			else {
				$this->values = $this->getParams($parenthesisContent[0]);			
			}
		}

		function toString() {
			$query = $this->type . " into " . $this->table;
			if ($this->fieldList) {
				$fields = "(";
				$values = "(";
				for($i = 0; $i < sizeof($this->fields); $i++) {
					$fields .= "'" . trim($this->fields[$i]) . "',";
					$values .= "'" . $this->values[$i] . "',";
				}
				$fields = trim(substr($fields,0,-1)) . ")";
				$values = substr($values,0,-1) . ")";
				$query .= $fields . " values " . $values;
			}
			else {
				$values = "(";
				foreach($this->values as $value) {
					$values .= "'" . $value . "',";
				}
				$values = substr($values,0,-1) . ")";
				$query .= " values " . $values;
			}
			
			return $query;
		}
	}

?>