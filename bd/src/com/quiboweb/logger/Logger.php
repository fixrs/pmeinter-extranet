<?php
	/*
		Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/
	
	/**
     *
     * @version 1.0
     * @author Jean-Philippe Ricard <ricardjp@quiboweb.ca>
	 * @package	Quibo::logger
     */
	 
	import("com.quiboweb.logger.SQLQueryParser");

	class Logger {

		var $db;
		var $user;
		
		function Logger($db, $user) {
			$this->db = $db;
			$this->user = $user;
		}

		function log($query, $info) {
			$parser = new SQLQueryParser($query);
			$SQLQuery = $parser->getSQLQuery();

			// find the table to log
			$table = $SQLQuery->getTable() . "_logs";


			// first place - auto increment number
			// second place - the refering ID from the user who committed an update
			$userID = $this->user;
			// third place - the date of the modification of the original table
			$date = date("YmdHis");
			// fourth place - info about the modification

			// subsquent fields are the one contained in query
			$logQuery = "INSERT INTO " . $table . " VALUES (";
			$logQuery .= "'','" . $userID . "','" . $date . "','" . $info . "'";

			// detecting assoc logs keys

			$this->db->query("DESCRIBE " . $table);
			$keys = array();
			$i = 0;
			while ($key = $this->db->next_record()) {
				if ($i >= 4) {
					array_push($keys, $key[0]);
				}
				$i++;
			}
			$this->db->reset();

			$values = $SQLQuery->getValues();
			$i = 0;
			foreach ($values as $key => $value) {
				if (preg_match("/_logs_key$/i",$keys[$i])) {
					$matches = "";
					preg_match("/(.*?)_logs_key$/i",$keys[$i], $matches);
					
					$this->db->query(
						"SELECT `key` ".
						"FROM " . $matches[1] . "_logs ".
						"WHERE " .  $matches[1] . "_key = '" . $value . "' ".
						"ORDER BY `key` DESC"
					);
										
					$row = $this->db->next_record();
					$logQuery .= ",'" . $row[0] . "'";
				}
				else {
					$logQuery .=  ",'" . $value . "'";
				}
				$i++;
			}
			$logQuery .= ")";

			return $logQuery;
		}

	}

?>