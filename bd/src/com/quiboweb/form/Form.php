<?PHP

    /*
    Copyright (C) 2004 Francois Viens, QuiboWeb Inc.
    WWW.QUIBOWEB.CA

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
    */


    /**
     * The Form Object allows to manage a HTML form by offering methods to modify and validate it.
     *
     * To be able to use this object, you have to learn the syntax for creating appropriate HTML forms.
     *
     * @version 1.2
     * @author Francois Viens <viensf@quiboweb.ca>
	 * @package	Quibo
     */
    class Form {


        /**
         * @var		string   - A complete path and filename to a HTML form
		 * @access	private
         */
		var $FormFileName;


		/**
         * @var		array   - An associative array containing form's fields names as keys and form's fields values as values
		 * @access	private
         */
        var $Parameters;

		/**
         * @var		array   - array containing form's fields from $this->Form
		 * @access	private
         */
		var $formFieldsArray;

		/**
         * @var		string   - A HTML Form string
		 * @access	private
         */
        var $Form;


        /**
         * Object Constructor
         *
         * There are 2 different ways you can use this object
		 * <ul>
         *     <li>$FormObject = new Form($COMPLETE_FILENAME, $PARAMETERS, "");</li>
		 *     <li>$FormObject = new Form("", $PARAMETERS, $HTML_FORM_STRING);</li>
		 * </ul>
         *
         * The second way is a lot more powerful because it allows you to build dynamically your HTML Form
         *
         * @access  public
         * @return  void
         * @param   string   - A complete path and filename to a HTML form
         * @param   array	 - An associative array containing form's fields names as keys and form's fields values as values
		 * @param   string	 - A HTML Form string
         */
		function Form($FormFileName = "", $Parameters = "", $Form = "") {
			if ($FormFileName != "") {
				$this->FormFileName = $FormFileName;
			}
			if ($Form != "") {
				$this->Form = $Form;
				$this->setFormFields($Form);
            }
			if ($Parameters != "") {
				$this->setParameters($Parameters);
			}

			if ($FormFileName != "" && $Form == "") {
				$this->load();
			}
		}


        /**
         * Loads a file into a string
         *
         * @access  private
         * @return  void
         */
        function load() {
            if (file_exists($this->FormFileName)) {
                $this->Form = file_get_contents($this->FormFileName);
            } else {
                die ("Error : Bad File Name: ".$this->FormFileName);
            }
        }


        /**
         * Set fields names from this->Form
         *
         * @access  private
         * @return  void
         */
		function setFormFields($Form) {
			$pregArray = array();
			$formFieldsArray = array();
			preg_match_all('/<(textarea|input|select)[^>]*name=\"([^\"]+)\"/', $Form, $pregArray, PREG_SET_ORDER);
			foreach($pregArray as $nb => $subArray) {
				if (substr($subArray[2], strlen($subArray[2]) - 2) == "[]") {
					$subArray[2] = substr($subArray[2], 0, strlen($subArray[2]) - 2);
				}
				$formFieldsArray[] = $subArray[2];

			}
			$this->formFieldsArray = $formFieldsArray;


		}


        /**
         * Prepares the display of an hidden section<br><br>
         *
		 * If you want to display an hidden section, all you have to do is to add this line into your HTML Form :<br><br>
		 *
		 * <!---HIDDEN_THE_NAME_OF_THE_HIDDEN_SECTION <b>HTML_HIDDEN_SECTION</b> /HIDDEN_THE_NAME_OF_THE_HIDDEN_SECTION---><br><br>
		 *
		 * Change HTML_HIDDEN_SECTION by the HTML code you want to hide unless you call this method<br><br>
		 * Change THE_NAME_OF_THE_HIDDEN_SECTION by the name you want to use for this hidden section<br><br>
		 *
		 * In the PHP code, try this :<br><br>
		 *
		 * $FormObject->displayHidden("THE_NAME_OF_THE_HIDDEN_SECTION");
		 *
         * @access  public
         * @return  void
		 * @param	string	- The name of the hidden section you want to display
		 * @see	display()
		 * @see displayFilled()
		 * @see displayError()
		 * @see setHiddenValue()
         */
        function displayHidden($name) {
            $this->Form = preg_replace("/<\!---HIDDEN_".$name."(.*?)\/HIDDEN_".$name."--->/si", "\$1", $this->Form);
        }


        /**
         * Prepares the display of an hidden section and assign it a value<br><br>
         *
		 * If you want to display an hidden section and you want to assign it a value, all you have to do is to add this line into your HTML Form :<br><br>
		 *
		 * <!---HIDDEN_THE_NAME_OF_THE_HIDDEN_SECTION  /HIDDEN_THE_NAME_OF_THE_HIDDEN_SECTION---><br><br>
		 *
		 * Change THE_NAME_OF_THE_HIDDEN_SECTION by the name you want to use for this hidden section<br><br>
		 *
		 * In the PHP code, try this :<br><br>
		 *
		 * $FormObject->setHiddenValue("THE_NAME_OF_THE_HIDDEN_SECTION", "Hello World!");
		 *
         * @access  public
         * @return  void
		 * @param	string	- The name of the hidden section you want to display
		 * @param	string	- The value you want to assign to the hidden section you want to display
		 * @see	display()
		 * @see displayFilled()
		 * @see displayError()
		 * @see displayHidden()
         */
        function setHiddenValue($name, $value) {
		$value = stripslashes($value);
		$this->Form = preg_replace("/<\!---HIDDEN_".$name."(.*?)\/HIDDEN_".$name."--->/si", $value, $this->Form);
        }


	function setAllHidden() {
		foreach ($this->Parameters as $key => $value) {
			$value = stripslashes($value);
			$this->setHiddenValue($key, $value);
		}
	}


        /**
         * Return the HTML Form filled with the information contained inside the variable Parameters<br><br>
         *
		 * What is nice with this method is that you can create an array from a SQL query or by any other way. This array has for keys the HTML form's fields' name and the HTML form's fields' values for values.<br><br>
		 *
		 * In the PHP code, try this :<br><br>
		 *
		 * $FormObject->returnFilledForm();<br><br>
		 *
         * @access  public
         * @return  !void
		 * @see display()
		 * @see displayError()
         */
		function returnFilledForm() {
			foreach ($this->formFieldsArray as $key => $field) {
				if (!preg_match("/(_VALID_|_INTEGRITY_|_DEFAULT_)/", $field)) {
					if (isset($this->Parameters[$field])) {
						$this->setValue($field, $this->Parameters[$field]);
					} else {
						if (preg_match("/\[[^\]]*\]/", $field)) {
							$key2 = preg_replace("/^(.*?)\[([^\]]*)\].*?/", "\$1", $field);
							$value2 = preg_replace("/^(.*?)\[([^\]]*)\].*?/", "\$2", $field);
							$this->setValue($field, $this->Parameters[$key2][$value2]);
						}
					}
				}
			}
			$this->cleanSource();
			return $this->Form;
		}


		/**
         * Return the HTML Form as it.<br><br>
		 *
		 * In the PHP code, try this :<br><br>
		 *
		 * $FormObject->returnForm();<br><br>
		 *
         * @access  public
         * @return  !void
		 * @see displayFilled()
		 * @see displayError()
         */
        function returnForm() {
            $this->cleanSource();
            return $this->Form;
        }


        /**
         * Changes the value of an input type, select and textarea<br><br>
         *
		 * With this method, you can set the value of a HTML form field.<br><br>
		 *
		 * It is better to give a name to your fields : <input type="text" name="test" value=""><br><br>
		 *
		 * In the PHP code, try this :<br><br>
		 *
		 * $FormObject->setValue("test", "This is a test");
		 *
         * @access  public
         * @return  void
		 * @param	string	- The name of the HTML form field you want to modify
		 * @param	string	- The value you want to assign to is
		 * @see	display()
		 * @see displayFilled()
		 * @see displayError()
         */
        function setValue($key, $value) {
            $form = $this->Form;

            if ((isset($this->Parameters["_IGNORE_" . $key]) && $this->Parameters["_IGNORE_" . $key] == 1) || preg_match("/_IGNORE_/", $key)) {
            	return "";
            }

            if ($key != "") {
                if (gettype($value) != "array") {
					$value = preg_replace("/\"/", "&quot;", $value);
					$value = stripslashes($value);
					$value = preg_replace("/[\$]/", "&#036;", $value);
				}

                $form = preg_replace(
					"/(<input[^>]*?)(name=\"" . $this->escapeREchar($key) . "\"[^>]*?)(value=\")()(\"[^>]*?)(type=\"(text|hidden)\")/si",
					"\$1\$2\${3}" . $value . "\$5\$6 ",
					$form
				);

                $form = preg_replace(
					"/(<input[^>]*?)(name=\"" . $this->escapeREchar($key) . "\"[^>]*?)(type=\"(text|hidden)\"[^>]*?)(value=\")()(\")/si",
					"\$1\$2\$3\${5}" . $value . "\$7 ",
					$form
				);

                $form = preg_replace(
					"/(<input[^>]*?)(value=\")()(\"[^>]*?)(name=\"" . $this->escapeREchar($key) . "\"[^>]*?)(type=\"(text|hidden)\")/si",
					"\$1\${2}" . $value . "\$4\$5\$6 ",
					$form
				);

                $form = preg_replace(
					"/(<input[^>]*?)(value=\")()(\"[^>]*?)(type=\"(text|hidden)\"[^>]*?)(name=\"" . $this->escapeREchar($key) . "\")/si",
					"\$1\${2}" . $value . "\$4\$5\$7 ",
					$form
				);

                $form = preg_replace(
					"/(<input[^>]*?)(type=\"(text|hidden)\"[^>]*?)(name=\"" . $this->escapeREchar($key) . "\"[^>]*?)(value=\")()(\")/si",
					"\$1\$2\$4\${5}" . $value . "\$7 ",
					$form
				);

                $form = preg_replace(
					"/(<input[^>]*?)(type=\"(text|hidden)\"[^>]*?)(value=\")()(\"[^>]*?)(name=\"" . $this->escapeREchar($key) . "\")/si",
					"\$1\$2\${4}" . $value . "\$6\$7 ",
					$form
				);

                //Supporte les checkbox et les radio button
                $form = preg_replace("/(<input[^>]*?)(name=\"".$this->escapeREchar($key)."\"[^>]*?)(value=\"".$this->escapeREchar($value)."\"[^>]*?)(type=\"(checkbox|radio)\")/si", "\$1\$2\$3\$4 checked ", $form);
                $form = preg_replace("/(<input[^>]*?)(name=\"".$this->escapeREchar($key)."\"[^>]*?)(type=\"(checkbox|radio)\"[^>]*?)(value=\"".$this->escapeREchar($value)."\")/si", "\$1\$2\$3\$5 checked ", $form);
                $form = preg_replace("/(<input[^>]*?)(value=\"".$this->escapeREchar($value)."\"[^>]*?)(name=\"".$this->escapeREchar($key)."\"[^>]*?)(type=\"(checkbox|radio)\")/si", "\$1\$2\$3\$4 checked ", $form);
                $form = preg_replace("/(<input[^>]*?)(value=\"".$this->escapeREchar($value)."\"[^>]*?)(type=\"(checkbox|radio)\"[^>]*?)(name=\"".$this->escapeREchar($key)."\")/si", "\$1\$2\$3\$5 checked ", $form);
                $form = preg_replace("/(<input[^>]*?)(type=\"(checkbox|radio)\"[^>]*?)(name=\"".$this->escapeREchar($key)."\"[^>]*?)(value=\"".$this->escapeREchar($value)."\")/si", "\$1\$2\$4\$5 checked ", $form);
                $form = preg_replace("/(<input[^>]*?)(type=\"(checkbox|radio)\"[^>]*?)(value=\"".$this->escapeREchar($value)."\"[^>]*?)(name=\"".$this->escapeREchar($key)."\")/si", "\$1\$2\$4\$5 checked ", $form);

                //Supporte les select et les select multiple
                if (gettype($value) == "array") {

                    for ($i = 0; $i < count($value); $i++) {
                        $thevalue = $value[$i];
						$thevalue = preg_replace("/\"/", "&quot;", $thevalue);
						$thevalue = stripslashes($thevalue);

						if (!preg_match("/(<option[^>]*?)((id|selectname)=\"".$this->escapeREchar($key)."\"[^>]*?)(value=\"".$this->escapeREchar($thevalue)."\"[^>]*?)selected/si", $form) && !preg_match("/(<option[^>]*?)(value=\"".$this->escapeREchar($thevalue)."\"[^>]*?)((id|selectname)=\"".$this->escapeREchar($key)."\"[^>]*?)selected/si", $form)) {
							$form = preg_replace("/(<option[^>]*?)((id|selectname)=\"".$this->escapeREchar($key)."\"[^>]*?)(value=\"".$this->escapeREchar($thevalue)."\")/si", "\$1\$2\$4 selected ", $form);
	                       $form = preg_replace("/(<option[^>]*?)(value=\"".$this->escapeREchar($thevalue)."\"[^>]*?)((id|selectname)=\"".$this->escapeREchar($key)."\")/si", "\$1\$2\$3 selected ", $form);
						}


                        // if (!preg_match("/(<input[^>]*?)((id|selectname)=\"".$this->escapeREchar($key)."\"[^>]*?)(value=\"".$this->escapeREchar($thevalue)."\"[^>]*?)checked/si", $form) && !preg_match("/(<input[^>]*?)(value=\"".$this->escapeREchar($thevalue)."\"[^>]*?)((id|selectname)=\"".$this->escapeREchar($key)."\"[^>]*?)selected/si", $form)) {
                        //         $form = preg_replace("/(<input[^>]*?)((id|selectname)=\"".$this->escapeREchar($key)."\"[^>]*?)(value=\"".$this->escapeREchar($thevalue)."\")/si", "\$1\$2\$4 checked ", $form);
                        //         $form = preg_replace("/(<input[^>]*?)(value=\"".$this->escapeREchar($thevalue)."\"[^>]*?)((id|selectname)=\"".$this->escapeREchar($key)."\")/si", "\$1\$2\$3 checked ", $form);
                        // }

                    }
                } else {
                    if (!preg_match("/(<option[^>]*?)((id|selectname)=\"".$key."\"[^>]*?)(value=\"".$this->escapeREchar($value)."\"[^>]*?)selected/si", $form) && !preg_match("/(<option[^>]*?)(value=\"".$this->escapeREchar($value)."\"[^>]*?)((id|selectname)=\"".$key."\"[^>]*?)selected/si", $form)) {
						$form = preg_replace("/(<option[^>]*?)((id|selectname)=\"".$key."\"[^>]*?)(value=\"".$this->escapeREchar($value)."\")/si", "\$1\$2\$4 selected ", $form);
						$form = preg_replace("/(<option[^>]*?)(value=\"".$this->escapeREchar($value)."\"[^>]*?)((id|selectname)=\"".$key."\")/si", "\$1\$2\$3 selected ", $form);
					}
                }

                //Supporte les textarea
                $form = preg_replace("/(<textarea[^>]*?)(name=\"".$key."\")([^>]*?)>()<\/textarea>/si", "\$1\$2\$3>".stripslashes($value)."</textarea>", $form);

            }
            $this->Form = $form;
            return 1;
        }


		/**
         * Takes off every special comments before the display<br><br>
         *
		 * This method is called automatically when you call display(), displayFilled() or displayError() .<br><br>
		 *
         * @access  private
         * @return  void
		 * @see	display()
		 * @see displayFilled()
		 * @see displayError()
         */
        function cleanSource($Form = "") {
            $this->Form = preg_replace("/<\!---HIDDEN_(\w+).*?\/HIDDEN_\\1--->/si", "", $this->Form);
	        $this->Form = preg_replace("/<\!---ERROR_(\w+).*?\/ERROR_\\1--->/si", "", $this->Form);
	        $this->Form = preg_replace("/(<option[^>]*?)((selectname)=\"[^\"]*\")/si", "\$1", $this->Form);

        }


		/**
         * Escapes special caracters in a regular expression.<br><br>
         *
		 * This method is called automatically by the method isValid() .<br><br>
		 *
		 * In the PHP code, try this :<br><br>
		 *
		 * $FormObject->setDisabled("test");
		 *
         * @access  private
         * @return  string
		 * @param	string	- The regular expression you want to escape
		 * @see	isValid()
         */
        function escapeREchar($string) {
            $string = preg_replace("/([\[\]\(\)\*\+\^\$\?\'\"\/\\\.])/", "\\\\$1", $string);
            return $string;
        }


		/**
         * Assigns a value to the variable $Parameters<br><br>
         *
		 * This method is called automatically by the method isValid() .<br><br>
		 *
		 * In the HTML code, try this : <br><br>
		 *
		 * <input type="text" name="test" value=""><br><br>
		 *
		 * In the PHP code, try this :<br><br>
		 *
		 * $FormObject->setParameters(array('test', 'This is a test'));<br>
		 * $FormObject->displayFilled();
		 *
         * @access  public
         * @return  void
		 * @param	string	- The regular expression you want to escape
		 * @see	displayFilled()
         */
        function setParameters($parameters) {
            $this->Parameters = $parameters;
        }
    }
?>
