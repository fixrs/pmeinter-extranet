<?php

	/*
		Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/

	$str['eng']['error_query']		= "The query has failed : ";
	$str['eng']['error_connect']	= "Error connecting to the database : ";
	$str['eng']['error_select_db']	= "Unable to select specified database : ";
	$str['eng']['error_close']		= "Error closing database link : ";


	/**
     *
     * @version 1.1
     * @author Francois Viens <viensf@quiboweb.ca>
	 * @package	Quibo
     */
	class MySQL_i extends DB {
		
		function MySQL_i($dbhost, $dbuser, $dbpass, $dbname) {
			$this->str = $GLOBALS['str']['eng'];
			
			$this->db = mysqli_connect($dbhost, $dbuser, $dbpass);// or die($this->str['error_connect'] . mysql_error($this->db));
			mysqli_select_db($this->db, $dbname) or die($this->str['error_select_db'] . mysqli_error($this->db));
			return $this->db;
		}
		

		function query($query) {				
			@$this->result[count($this->result)] = mysqli_query($query, $this->db);
			
			if (!$this->result[count($this->result)-1]) {
				@die($this->str["error_query"] . mysqli_error($this->db));
			}
			
			return $this->result[count($this->result)-1];
		}
		
		function close() {
			parent::reset();
			if (!mysqli_close($this->db)) {
				die($this->str['error_close'] . mysqli_error($this->db));
			}
		}

		function next_record() {
			$last_result = count($this->result)-1;
			if ($last_result < 0) { return FALSE; }
			
			$last_row = count($this->row[$last_result])-1;
						
			$this->row[$last_result][$last_row + 1] = mysqli_fetch_array($this->result[$last_result]);
		
			if (!$this->row[$last_result][$last_row + 1]) {
				parent::reset();
				return FALSE;
			} else {
				return $this->row[$last_result][$last_row + 1];
			}
		}

		function getNumRows() {
			return mysqli_num_rows($this->result[count($this->result)-1]);
		}

		function getNumFields() {
			return mysqli_field_count($this->db);
		}
		
		function getInsertId() {
			return mysqli_insert_id($this->db);
		}

		function getQueryInfo() {
			return mysqli_info($this->db);
		}

		function getFieldName($i) {
			return mysqli_fetch_field_direct($this->result[count($this->result)-1], $i);	
		}
		
		function getFieldDesc($field) {
			return mysqli_query("DESCRIBE `" . $field . "`");
		}
		
		function getAffectedRows() {
			return mysqli_affected_rows($this->db);
		}
		
		function getFieldsList($table) {
			return mysqli_query("SHOW COLUMNS FROM `" . $table . "`");
		}

		function getTableList($like) {
			return mysqli_query("SHOW TABLES FROM `" . $this->dbname . "` LIKE " . $like);
		}
	}
?>
