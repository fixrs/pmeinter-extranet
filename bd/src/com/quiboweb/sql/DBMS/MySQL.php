<?php

	/*
		Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/

	$str['eng']['error_query']		= "The query has failed : ";
	$str['eng']['error_connect']	= "Error connecting to the database : ";
	$str['eng']['error_select_db']	= "Unable to select specified database : ";
	$str['eng']['error_close']		= "Error closing database link : ";


	/**
     *
     * @version 1.1
     * @author Jean-Philippe Ricard <ricardjp@quiboweb.ca>
	 * @package	Quibo
     */
	class MySQL extends DB {
		
		function MySQL($dbhost, $dbuser, $dbpass, $dbname) {
			$this->str = $GLOBALS['str']['eng'];
			$this->db = mysql_pconnect($dbhost, $dbuser, $dbpass) or die($this->str['error_connect'] . mysql_error($this->db));
			mysql_select_db($dbname, $this->db) or die($this->str['error_select_db'] . mysql_error($this->db));
			return $this->db;
		}
		
		function query($query) {				
			@$this->result[count($this->result)] = mysql_query($query, $this->db);
			
			if (!$this->result[count($this->result)-1]) {
				@die($this->str["error_query"] . mysql_error($this->db));
			}
			
			return $this->result[count($this->result)-1];
		}
		
		function close() {
			parent::reset();
			if (!mysql_close($this->db)) {
				die($this->str['error_close'] . mysql_error($this->db));
			}
		}

		function next_record() {
			$last_result = count($this->result)-1;
			if ($last_result < 0) { return FALSE; }
			
			$last_row = count($this->row[$last_result])-1;
						
			$this->row[$last_result][$last_row + 1] = mysql_fetch_array($this->result[$last_result]);
		
			if (!$this->row[$last_result][$last_row + 1]) {
				parent::reset();
				return FALSE;
			} else {
				return $this->row[$last_result][$last_row + 1];
			}
		}

		function getNumRows() {
			return mysql_num_rows($this->result[count($this->result)-1]);
		}

		function getNumFields() {
			return mysql_num_fields($this->result[count($this->result)-1]);
		}
		
		function getInsertId() {
			return mysql_insert_id($this->db);
		}

		function getQueryInfo() {
			return mysql_info($this->db);
		}

		function getFieldName($i) {
			return mysql_field_name($this->result[count($this->result)-1], $i);	
		}
		
		function getFieldDesc($field) {
			return mysql_query("DESCRIBE `" . $field . "`");
		}
		
		function getAffectedRows() {
			return mysql_affected_rows($this->db);
		}
		
		function getFieldsList($table) {
			return mysql_query("SHOW COLUMNS FROM `" . $table . "`");
		}

		function getTableList($like) {
			return mysql_query("SHOW TABLES FROM `" . $this->dbname . "` LIKE " . $like);
		}

	}
?>
