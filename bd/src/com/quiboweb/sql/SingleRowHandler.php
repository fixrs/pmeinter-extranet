<?PHP

	/*
		Copyright (C) 2007 Guillaume Legault, QuiboWeb Inc.
		WWW.QUIBOWEB.COM

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/


	/*
		DEPENDENCIES
		------------

		- Object DB from Quibo Package is required.
		- Object DB_Logger from Quibo Package is required for some functions
		  (see loggermode).


		QUICK USAGE INFO
		----------------
		This object is used to handle a single row while optimizing DB queries.

		This object is PHP4 compatible, therefore, every vars and functions
		are implicitly public.

		$this->qkeyArray():
		The query key (QKey) is used in every queries in the WHERE clause.
		Ex: ...WHERE QKeyArray['field'] = QKeyArray['value']

		$this->fieldsArray():
		The fields array (fieldsArray) contains information about table fields.
		It also contains field's value.
		All the save and load type functions take their values from this array.

		$this->scanfields():
		The function scanfields() should be performed at least once.
		This will fill the fields array with all the info needed by other
		functions (save(), load(), etc.)

		There are several working modes at your disposal.
		Here's a brief description of each:

		QUICKMODE:
		This mode is usefull when you don't need to load or save large amount of rows.
		When you use quickmode(), it will do a scanfields().
		When working in quickmode, the object bypass the fields buffering.
		So anything you set(), will be saved right after.
		And anything you want to get() will be loaded right before.

		INSERTMODE:
		When working in 'insertmode', the save() and saveAll() function will generate
		insert queries instead of update (which is by default).
		Once an insert has been made, the mode turns off automatically, so the next
		values will be updated.

		AUTOSLASHES:
		This mode will add slashes to any value contained in a save or saveAll query.

		FORCEMODE:
		This mode override the default object behavior that prevent unexisting fields
		to be included in the queries. Usefull if you don't mind performaing a
		scanfields(). Use this mode with caution.

		LOGGERMODE:
		This mode is still not stable because it is intimely related to an unfinished
		object (DB_Logger).

		DEBUGMODE:
		This mode will print out every save/saveAll queries before parsing them.

		SOME USAGE EXAMPLES
		-------------------
		To be continued...

	*/


	/**
	 * @version 1.0
	 * @author Guillaume Legault <legaultg@quiboweb.com>
	 * @package Quibo
	 */

    class SingleRowHandler {

		// ======================================
		//     C L A S S    V A R I A B L E S
		// ======================================

		var $DB;						// The DB link object						=> object
		var $table;						// The actual working table name			=> "*"
		var $fieldsArray = array();		// The atcual working row array				=> array(* see structure below)
		var $qkeyArray = array();		// The actual working query key array		=> array(* see structure below)
		var $insertId;					// The insert id of the last insert			=> "*"
		var $insertmode;				// The insertmode state						=> 0 | 1
		var $quickmode;					// The quickmode state						=> 0 | 1
		var $forcemode;					// The forcemode state						=> 0 | 1
		var $loggermode;				// The loggermode state						=> 0 | 1
		var $autoslashes = 1;				// The autoslashes state					=> 0 | 1
		var $debugmode;					// The debugmode state						=> 0 | 1
		var $insertLogInfo;				// The log info value for insert queries	=> "*" | default = "Ajout"
		var $updateLogInfo;				// The log info value for update queries	=> "*" | default = "Modification"
		var $deleteLogInfo;				// The log info value for delete queries	=> "*" | default = "Suppression"

		/*
		 *	$fieldsArray => Array (
		 *		[%FIELDNAME%]	=> Array (	// The field name						=> array()
		 *			['value']	=>			// The field value						=> "*"
		 *			['fromdb']	=>			// The field fromdb flag				=> 0 | 1
		 *			['exclude']	=>			// The field exclude flag				=> 0 | 1
		 *			['noquote']	=>			// The field noquote flag				=> 0 | 1
		 *			['type']	=>			// The field type property				=> "" | "varchar(255") | "timestamp" | ...
		 *			['null']	=>			// The field null tolerance property	=> "" | "YES"
		 *			['key']		=>			// The field key property				=> "" | "PRI" | "UNI" | "MUL"
		 *			['default']	=>			// The field default value property		=> "" | "CURRENT_TIMESTAMP" | ...
		 *			['extra']	=>			// The field extra info					=> "" | "auto_increment" | ...
		 *		)
		 *		[%FIELDNAME%]	=> Array (
		 *			...
		 *		)
		 *	)
		 *
		 *	$qkeyArray => Array (
		 *		['field']	=>		// The key field name	=> "*"
		 *		['value']	=>		// The key field value	=> "*"
		 *	)
		 *
		 */


		// ============================================
		//     C O N S T R U C T O R    M E T H O D
		// ============================================

		/**
		* Constructor function
		* Should be overrided by children classes
		*
		* @access public
		* @param object $DB : db object
		* @param array $fieldsArray optional : fields array to begin working with
		* @param int $quickmode optional : set quickmode state (0 | 1), default is 0
		*
		* @return void
		*/
		function SingleRowHandler($DB = "", $table = "", $fieldsArray = "", $quickmode = 0) {
        	if ($DB != "") {
        		$this->DB = $DB;
        	}
        	if ($table != "") {
        		$this->table = $table;
        	}
        	if ($fieldsArray != "") {
        		$this->fieldsArray = $fieldsArray;
        	}
        	if ($quickmode) {
        		$this->quickmode();
        	}
        	$this->insertLogInfo = "Ajout";
        	$this->updateLogInfo = "Modification";
        	$this->deleteLogInfo = "Suppression";
        }


		// ====================================
		//     P U B L I C    M E T H O D S
		// ====================================

		// --------------------
		// 'SET' TYPE FUNCTIONS
		// --------------------

		/**
		* Set a given field with a given value
		* If working in quickmode, the value is directly inserted in the DB
		* If not, the value is only inserted in the fields array
		*
		* @access public
		* @param string $field : name of the field to set the value in
		* @param string $value : value to be set in the field
		* @param int $noquote : set to 1 to flag the field value as a quoteless value (ex: CURRENT_TIMESTAMP, NULL)
 		* @return boolean : only if working in quickmode, returns false if an error occured
		*/
		function set($field = "", $value = "", $noquote = 0) {
			if ($field != "") {
				if (isset($this->fieldsArray[$field])) {
					$this->fieldsArray[$field]['value'] = $value;
					if ($noquote == 1) {
						$this->fieldsArray[$field]['noquote'] = 1;
					}
					if ($this->isQuickmode()) {
						return $this->save($field);
					}
				}
			}
		}

		/**
		* Set the value of all fields
		* If working in quickmode, the values are directly inserted in the DB
		* If not, the values are only inserted in the fields array
		*
		* @access public
		* @param array $fields : "field" => "value"
		* @return string : if working in quickmode and insertmode, returns the insert ID
 		* @return boolean : returns false if an error occured
		*/
		function setAll($fields = "") {
			if ($fields != "") {
				foreach ($fields as $field => $value) {
					if (isset($value)) {
						$this->fieldsArray[$field]['value'] = $value;
					}
				}
				if ($this->isQuickmode()) {
					return $this->saveAll();
				}
			}
		}

		/**
		* Set the fields array ($this->fieldsArray)
		*
		* @access public
		* @param array $fieldsArray : fields array that may content unlimited amount of flags
		* @return void
		*/
		function setFieldsArray($fieldsArray = "") {
			if ($fieldsArray != "") {
				$this->fieldsArray = $fieldsArray;
			}
		}

		/**
		* Set the field name and value of the query key ($this->qkeyArray)
		*
		* @access public
		* @param string $field : field name of the query key
 		* @param string $value : value of the query key
 		* @return void
		*/
		function setQKey($field = "", $value = "") {
			if ($field != "") {
				$this->qkeyArray['field'] = $field;
			}
			if ($value != "") {
				$this->qkeyArray['value'] = $value;
			}
		}

		/**
		* Set a given flag of a given field with a given value
		*
		* @access public
		* @param string $field : the field to set the flag to
		* @param string $flaglabel : the flag to set
		* @param string $flagvalue : the flag value
		* @return void
		*/
		function setFieldFlag($field = "", $flaglabel = "", $flagvalue = "") {
			if ($field != "" && $flaglabel != "") {
				$this->fieldsArray[$field][$flaglabel] = $flagvalue;
			}
		}

		/**
		* Set all the given flags of a given field
		* Should only be used for specific purpose
		*
		* @access public
		* @param string $field : the field to set the flags to
		* @param array $flags : "flag" => "value"
		* @return void
		*/
		function setFieldFlags($field = "", $flags = "") {
			if ($field != "" && $flags != "") {
				foreach ($flags as $flag => $value) {
					$this->fieldsArray[$field][$flag] = $value;
				}
			}
		}

		/**
		* Set the name of the working table ($this->table)
		*
		* @access public
		* @param string $table
		* @return void
		*/
		function setTable($table = "") {
			if ($table != "") {
				$this->table = $table;
			}
		}

		/**
		* Set the db objet ($this->DB)
		*
		* @access public
		* @param object $DB
		* @return void
		*/
		function setDB($DB = "") {
			if ($DB != "") {
				$this->DB = $DB;
			}
		}

		/**
		* Set the state of insertmode ($this->insertmode)
		*
		* @access public
		* @param int $state : on = 1 | off = 0
		* @return void
		*/
		function setInsertmode($state = 1) {
			$this->insertmode = $state;
		}

		/**
		* Set the state of quickmode ($this->quickmode)
		*
		* @access public
		* @param int $state : on = 1 | off = 0
		* @return void
		*/
		function setQuickmode($state = 1) {
			$this->quickmode = $state;
		}

		/**
		* Set the state of forcemode ($this->forcemode)
		*
		* @access public
		* @param int $state : on = 1 | off = 0
		* @return void
		*/
		function setForcemode($state = 1) {
			$this->forcemode = $state;
		}

		/**
		* Set the state of debugmode ($this->debugmode)
		*
		* @access public
		* @param int $state : on = 1 | off = 0
		* @return void
		*/
		function setDebugmode($state = 1) {
			$this->debugmode = $state;
		}

		/**
		* Set the state of autoslashes ($this->autoslashes)
		*
		* @access public
		* @param int $state : on = 1 | off = 0
		* @return void
		*/
		function setAutoslashes($state = 1) {
			$this->autoslashes = $state;
		}

		/**
		* Set the state of loggermode ($this->loggermode)
		*
		* @access public
		* @param int $state : on = 1 | off = 0
		* @return void
		*/
		function setLoggermode($state = 1) {
			$this->loggermode = $state;
		}

		/**
		* Set the log info for insert queries
		*
		* @access public
		* @param string
		* @return void
		*/
		function setInsertLogInfo($info) {
			$this->insertLogInfo = $info;
		}

		/**
		* Set the log info for insert queries
		*
		* @access public
		* @param string
		* @return void
		*/
		function setUpdateLogInfo($info) {
			$this->updateLogInfo = $info;
		}

		/**
		* Set the log info for insert queries
		*
		* @access public
		* @param string
		* @return void
		*/
		function setDeleteLogInfo($info) {
			$this->deleteLogInfo = $info;
		}


		// ----------------------
		// 'UNSET' TYPE FUNCTIONS
		// ----------------------

		/**
		* Unset the value of a given field
		*
		* @access public
		* @param string $field : the field to unset the value from
		* @return void
		*/
		function unsetField($field = "") {
			if ($field != "") {
				if (isset($this->fieldsArray[$field])) {
					unset($this->fieldsArray[$field]['value']);
				}
			}
		}

		/**
		* Unset the value of all fields
		* The pregmatch string will filter out the fields
		*
		* @access public
		* @param string pregmatch : preg match string
		* @return void
		*/
		function unsetAll($pregmatch = "") {
			foreach ($this->fieldsArray as $field => $subarray) {
				if ($pregmatch == "" || preg_match($pregmatch, $field)) {
					unset($this->fieldsArray[$field]['value']);
				}
			}
		}

		/**
		* Unset the field name and value of the query key
		*
		* @access public
		* @param void
		* @return void
		*/
		 function unsetQKey() {
			unset($this->qkeyArray['field']);
			unset($this->qkeyArray['value']);
		}

		/**
		* Unset the field of the query key
		*
		* @access public
		* @param void
		* @return void
		*/
		function unsetQKeyField() {
			unset($this->qkeyArray['field']);
		}

		/**
		* Unset the value of the query key
		*
		* @access public
		* @param void
		* @return void
		*/
		function unsetQKeyValue() {
			unset($this->qkeyArray['value']);
		}

		/**
		* Unset a given flag of a given field
		* Should only be used for specific purpose
		*
		* @access public
		* @param string $field : the field to unset the flag from
 		* @param string $flag : the flag to unset from the field
 		* @return void
		*/
		function unsetFieldFlag($field = "", $flag = "") {
			if ($field != "" && $flag != "") {
				if (isset($this->fieldsArray[$field])) {
					unset($this->fieldsArray[$field][$flag]);
				}
			}
		}

		/**
		* Unset all the flags of a given field
		* Should only be used for specific purpose
		*
		* @access public
		* @param string $field : the field to unset the flags from
		* @return void
		*/
		function unsetFieldFlags($field = "") {
			if ($field != "") {
				if (isset($this->fieldsArray[$field])) {
					foreach ($this->fieldsArray[$field] as $flag => $value) {
						if ($flag != 'value') {
							unset($this->fieldsArray[$field][$flag]);
						}
					}
				}
			}
		}


		// --------------------
		// 'GET' TYPE FUNCTIONS
		// --------------------

		/**
		* Get the value of a given field
		* If working in quickmode, the value is first loaded from the DB
		* If not, the value is retrieved from $this->fieldsArray
		*
		* @access public
		* @param string $field : name of the field
		* @return string : value of the field
		*/
		function get($field) {
			if ($this->isQuickmode()) {
				$this->load($field);
			}
			return $this->fieldsArray[$field]['value'];
		}

		/**
		* Get the value of all fields
		* If working in quickmode, the values are first loaded from the DB
		* If not, the values are retrieved from $this->fieldsArray
		* The pregmatch string will filter out the fields
		*
		* @access public
		* @param string $pregmatch : preg match string
		* @return array : "field" => "value"
		*/
		function getAll($pregmatch = "") {
			if ($this->isQuickmode()) {
				$this->loadAll();
			}
			$parameters = array();
			foreach ($this->fieldsArray as $field => $subarray) {
				if ($pregmatch == "" || preg_match($pregmatch, $field)) {
					$parameters[$field] = $subarray['value'];
				}
			}
			return $parameters;
		}

		/**
		* Get the name of the working table from $this->table
		*
		* @access public
		* @param void
		* @return string : see class variables above
		*/
		function getTable() {
			return $this->table;
		}

		/**
		* Get the query key array from $this->$qkeyArray
		*
		* @access public
		* @param void
		* @return array : see class variables above
		*/
		function getQKeyArray() {
			return $this->qkeyArray;
		}

		/**
		* Get the query key field name
		*
		* @access public
		* @param void
		* @return array : see class variables above
		*/
		function getQKeyField() {
			return $this->qkeyArray['field'];
		}

		/**
		* Get the query key field value
		*
		* @access public
		* @param void
		* @return array : see class variables above
		*/
		function getQKeyValue() {
			return $this->qkeyArray['value'];
		}

		/**
		* Get the fields array from $this->$fieldsArray
		*
		* @access public
		* @param void
		* @return array : see class variables above
		*/
		function getFieldsArray() {
			return $this->fieldsArray;
		}

		/**
		* Get the insert id of the last insert
		*
		* @access public
		* @param void
		* @return string
		*/
		function getInsertId() {
			return $this->insertId;
		}


		// ---------------------
		// 'BOOL' TYPE FUNCTIONS
		// ---------------------

		/**
		* Returns the state (set or not) of a given field value in $this->fieldsArray
		*
		* @access public
		* @param string $field : the field to look for its value in $this->fieldsArray
		* @return boolean
		*/
		function isFieldSet($field) {
			if (isset($this->fieldsArray[$field]['value'])) {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (set or not) of the query key field and value
		* Both field and value must be set to return true
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isQKeySet() {
			if ($this->qkeyArray['value'] != "" && $this->qkeyArray['field'] != "") {
				return TRUE;
			}
			return FALSE;
		}

		function isQKeyFieldSet() {
			if ($this->qkeyArray['field'] != "") {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (set or not) for the query key value
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isQKeyValueSet() {
			if ($this->qkeyArray['value'] != "") {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (set or not) for $this->table
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isTableSet() {
			if ($this->table != "") {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns bool value of the fromdb flag of a given field
		*
		* @access public
		* @param string $field : the field to look at
		* @return boolean
		*/
		function isFieldFromDB($field) {
			if (isset($this->fieldsArray[$field]['fromdb']) && $this->fieldsArray[$field]['fromdb'] == 1) {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns bool value of the exclude flag of a given field
		*
		* @access public
		* @param string $field : the field to look at
		* @return boolean
		*/
		function isFieldExcluded($field) {
			if (isset($this->fieldsArray[$field]['exclude']) && $this->fieldsArray[$field]['exclude'] == 1) {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns bool value of the noquote flag of a given field
		*
		* @access public
		* @param string $field : the field to look at
		* @return boolean
		*/
		function isValueUnquoted($field) {
			if (isset($this->fieldsArray[$field]['noquote']) && $this->fieldsArray[$field]['noquote'] == 1) {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (yes or no) of insertmode
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isInsertmode() {
			if ($this->insertmode == 1) {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (yes or no) of quickmode
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isQuickmode() {
			if ($this->quickmode == 1) {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (yes or no) of forcemode
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isForcemode() {
			if ($this->forcemode == 1) {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (yes or no) of debugmode
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isDebugmode() {
			if ($this->debugmode == 1) {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (yes or no) of autoslashes
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isAutoslashes() {
			if ($this->autoslashes == 1) {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (yes or no) of loggermode
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isLoggermode() {
			if ($this->loggermode == 1) {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (set or not) of $this->insertLogInfo
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isInsertLogInfoSet() {
			if ($this->insertLogInfo != "") {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (set or not) of $this->updateLogInfo
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isUpdateLogInfoSet() {
			if ($this->updateLogInfo != "") {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns the state (set or not) of $this->deleteLogInfo
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function isDeleteLogInfoSet() {
			if ($this->deleteLogInfo != "") {
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Returns true if an entry already exist in $this->table WHERE QKeyField = QKeyValue
		*
		* @access public
		* @param void
		* @return boolean
		*/
		function entryExists($field = "", $value = "") {
			if ($field != "") {
				$qfield = $field;
			} else {
				$qfield = $this->qkeyArray['field'];
			}
			if ($value != "") {
				$qvalue = $value;
			} else {
				$qvalue = $this->qkeyArray['value'];
			}
			$query =
				"SELECT COUNT(*) AS `total` ".
				"FROM `" . $this->table . "` ".
				"WHERE `" . $qfield . "` = '" . $qvalue . "'";
			$this->DB->query($query);
			$this->DB->next_record();
			$total = $this->DB->getField('total');
			$this->DB->reset();
			if ($total > 0) {
				return TRUE;
			}
			return FALSE;
		}


		// -------------------
		// 'DO' TYPE FUNCTIONS
		// -------------------

		/**
		* Set the state of insertmode to 1
		*
 		* @access public
 		* @param void
 		* @return void
		*/
		function insertmode() {
			$this->insertmode = 1;
		}

		/**
		* Set the state of quickmode to 1 and scan fields
		*
 		* @access public
 		* @param void
 		* @return void
		*/
		function quickmode() {
			$this->quickmode = 1;
			$this->scanfields();
		}

		/**
		* Set the state of forcemode to 1
		*
 		* @access public
 		* @param void
 		* @return void
		*/
		function forcemode() {
			$this->forcemode = 1;
		}

		/**
		* Set the state of debugmode to 1
		*
 		* @access public
 		* @param void
 		* @return void
		*/
		function debugmode() {
			$this->debugmode = 1;
		}

		/**
		* Set the state of autoslashes to 1
		*
 		* @access public
 		* @param void
 		* @return void
		*/
		function autoslashes() {
			$this->autoslashes = 1;
		}

		/**
		* Set the state of loggermode to 1
		*
 		* @access public
 		* @param void
 		* @return void
		*/
		function loggermode() {
			$this->loggermode = 1;
		}

		/**
		* Set the state of insertmode to 1 and unset the query key and all fields value
		*
 		* @access public
 		* @param void
 		* @return void
		*/
		function createnew() {
			$this->insertmode();
			$this->unsetQKey();
			$this->unsetAll();
		}

		/**
		* Set the noquote flag of a given field to 1
		* The noquote flagged field will keep its value unquoted in insert|update query
		* Usefull for field containing value like CURRENT_TIMESTAMP
		*
 		* @access public
 		* @param string $field : the field to add the unquote flag
 		* @return void
		*/
		function unquotevalue($field = "") {
			if ($field != "") {
				$this->setFieldFlag($field, "noquote", 1);
			}
		}

		/**
		* Set the noquote flag of a given field to 0
		*
 		* @access public
 		* @param string $field : the field to remove the unquote flag
 		* @return void
		*/
		function quotevalue($field = "") {
			if ($field != "") {
				$this->setFieldFlag($field, "noquote", 0);
			}
		}

		/**
		* Set the exclude flag of a given field to 1
		* The exclude flagged field won't be part of a any select|insert|update query
		*
 		* @access public
 		* @param string $field : the field to add the exclude flag
 		* @return void
		*/
		function excludefield($field = "") {
			if ($field != "") {
				$this->setFieldFlag($field, "exclude", 1);
			}
		}

		/**
		* Set the exclude flag of a given field to 0
		*
 		* @access public
 		* @param string $field : the field to remove the exclude flag
 		* @return void
		*/
		function includefield($field = "") {
			if ($field != "") {
				$this->setFieldFlag($field, "exclude", 0);
			}
		}

		/**
		* Retrieves numerous fields info to optimize future queries
		* The info is stored in $this->fieldsArray
		* The table must be set
		*
 		* @access public
 		* @param void
 		* @return boolean : returns false only if the table is not set
		*/
		function scanfields() {
			if (!$this->isTableSet()) {
				return FALSE;
			}
			$this->DB->query("DESCRIBE `" . $this->table . "`");
			while ($this->DB->next_record()) {
				$field = $this->DB->getField("Field");
				$this->setFieldFlag($field, "fromdb", 1);
				$this->setFieldFlag($field, "type", trim($this->DB->getField("Type")));
				$this->setFieldFlag($field, "null", trim($this->DB->getField("Null")));
				$this->setFieldFlag($field, "key", trim($this->DB->getField("Key")));
				$this->setFieldFlag($field, "default", trim($this->DB->getField("Default")));
				$this->setFieldFlag($field, "extra", trim($this->DB->getField("Extra")));
			}
			return TRUE;
		}


		// SAVE RELATED FUNCTIONS //

		/**
		* Save the value of a given field from $this->fieldsArray to the DB.
		* $this->scanfields() should have been called before to avoid bogus query.
		* If the query is bogus (meaning it is going to produce a DB error),
		* then the query is NOT sent to the DB and a FALSE is returned.
		* The table must be set
		* The query key must be set if not working in insertmode
		*
		* @access public
		* @param string $field : field to save the value in
		* @return boolean : returns false only if the generated query is bogus
		*/
		function save($field) {
			if (!isset($this->fieldsArray[$field]['exclude'])) { $this->fieldsArray[$field]['exclude'] = ''; }
			if (!isset($this->fieldsArray[$field]['extra'])) { $this->fieldsArray[$field]['extra'] = ''; }
			if (!isset($this->fieldsArray[$field]['null'])) { $this->fieldsArray[$field]['null'] = ''; }
			if (!isset($this->fieldsArray[$field]['default'])) { $this->fieldsArray[$field]['default'] = ''; }
			if (!isset($this->fieldsArray[$field]['value'])) { $this->fieldsArray[$field]['value'] = ''; }

			if ($field == "" || !$this->isTableSet() || !$this->isFieldSet($field) || $this->fieldsArray[$field]['exclude'] || $this->fieldsArray[$field]['extra'] == "auto_increment" || ($this->fieldsArray[$field]['null'] == "" && $this->fieldsArray[$field]['default'] == "" && $this->fieldsArray[$field]['value'] == "")) {
				return FALSE;
			}

			$ignore = FALSE;
			if (!$this->isForcemode()) {
				if (!$this->isFieldFromDB($field)) {
					$ignore = TRUE;
				};
			}

			if (!$ignore) {
				if (!isset($this->isInsertmode)) { $this->isInsertmode = 0; }
				if ($this->isInsertmode) {

					$revertnull = 0;
					if ($this->fieldsArray[$field]['value'] == "" && $this->fieldsArray[$field]['null'] != "") {
						$fieldsArray_revert['noquote'] = $this->fieldsArray[$field]['noquote'];
						$fieldsArray_revert['value'] = $this->fieldsArray[$field]['value'];
						$this->fieldsArray[$field]['noquote'] = "1";
						$this->fieldsArray[$field]['value'] = "NULL";
						$revertnull = 1;
					}

					if ($this->fieldsArray[$field]['noquote'] == 1) {
						$value = $this->fieldsArray[$field]['value'];
					} else {
						if ($this->autoslashes) {
							$value = "'" . addslashes($this->fieldsArray[$field]['value']) . "'";
						} else {
							$value = "'" . $this->fieldsArray[$field]['value'] . "'";
						}
					}

					if ($revertnull) {
						$subarray['noquote'] = $this->fieldsArray[$field]['noquote'];
						$subarray['value'] = $this->fieldsArray[$field]['value'];
					}

					$query =
						"INSERT INTO `" . $this->table . "` ".
						"(`" . $field . "`) ".
						"VALUES (" . $value . ")";


					if ($this->debugmode) {
						print_r($query);
					}

					if ($this->isLoggermode() && $this->isInsertLogInfoSet()) {
						$this->DB->query($query, $this->insertLogInfo);
						$this->insertId = $this->DB->getOriginalInsertId();
					} else {
						$this->DB->query($query);
						$this->insertId = $this->DB->getInsertId();
					}

					$this->DB->reset();
					$this->insertmode = 0;
					return TRUE;

				} else {

					if (!$this->isQKeySet()) {
						return FALSE;
					}
					$revertnull = 0;
					if ($this->fieldsArray[$field]['value'] == "" && $this->fieldsArray[$field]['null'] != "") {
						$fieldsArray_revert['noquote'] = $this->fieldsArray[$field]['noquote'];
						$fieldsArray_revert['value'] = $this->fieldsArray[$field]['value'];
						$this->fieldsArray[$field]['noquote'] = "1";
						$this->fieldsArray[$field]['value'] = "NULL";
						$revertnull = 1;
					}

					if ($this->fieldsArray[$field]['noquote'] == 1) {
						$value = $this->fieldsArray[$field]['value'];
					} else {
						if ($this->autoslashes) {
							$value = "'" . addslashes($this->fieldsArray[$field]['value']) . "'";
						} else {
							$value = "'" . $this->fieldsArray[$field]['value'] . "'";
						}
					}

					if ($revertnull) {
						$subarray['noquote'] = $this->fieldsArray[$field]['noquote'];
						$subarray['value'] = $this->fieldsArray[$field]['value'];
					}

					$query =
						"UPDATE `" . $this->table . "` ".
						"SET `" . $field . "` = $value ".
						"WHERE `" . $this->qkeyArray['field'] . "` = '" . $this->qkeyArray['value'] . "'";

					if ($this->debugmode) {
						print_r($query);
					}

					if ($this->isLoggermode() && $this->isUpdateLogInfoSet()) {
						$this->DB->query($query, $this->updateLogInfo);
					} else {
						$this->DB->query($query);
					}
					return TRUE;
				}

			} else {
				return FALSE;
			}
		}

		/**
		* Save all the values from $this->fieldsArray to the DB
		* $this->scanfields() should have been called before to avoid bogus query.
		* If the query is bogus (meaning it is going to produce a DB error),
		* then the query is NOT sent to the DB.
		* The pregmatch string will filter out the fields
		* The table must be set
		* The query key must be set if not working in insertmode
		*
		* @access public
		* @param string $pregmatch : preg_match string
		* @return boolean : returns false only if the generated query is bogus
		*/
		function saveAll($pregmatch = "") {
			if ($this->isInsertmode()) {
				$query = "";
				$query_string1 = "";
				$query_string2 = "";

				foreach ($this->fieldsArray as $field => $subarray) {
					if ($pregmatch == "" || preg_match($pregmatch, $field)) {
						$ignore = FALSE;
						if (!$this->isForcemode()) {
							if (!($this->isFieldFromDB($field) && $this->isFieldSet($field) && $subarray['extra'] != "auto_increment")) {
								$ignore = TRUE;
							}
						}

						if (!$ignore) {

							if (!$this->isFieldExcluded($field)) {
								$revertnull = 0;
								if ($subarray['value'] == "" && $subarray['null'] != "") {
									if (isset($subarray['noquote'])) {
										$subarray_revert['noquote'] = $subarray['noquote'];
									} else {
										$subarray_revert['noquote'] = '';
									}
									$subarray_revert['value'] = $subarray['value'];
									$subarray['noquote'] = "1";
									$subarray['value'] = "NULL";
									$revertnull = 1;
								}

								if (isset($subarray['noquote']) && $subarray['noquote'] == 1) {
									$value = $subarray['value'];
								} else {
									if ($this->autoslashes) {
										$value = "'" . addslashes($subarray['value']) . "'";
									} else {
										$value = "'" . $subarray['value'] . "'";
									}
								}

								$query_string1 .= "`" . $field . "`, ";
								$query_string2 .= "" . $value . ", ";

								if ($revertnull) {
									$subarray['noquote'] = $subarray_revert['noquote'];
									$subarray['value'] = $subarray_revert['value'];
								}
							}
						}
					}
				}

				if ($query_string1 == "" || $query_string2 == "") {
					return FALSE;
				}

				$query_string1 = substr($query_string1, 0, strlen($query_string1)-2);
				$query_string2 = substr($query_string2, 0, strlen($query_string2)-2);



				$query =
					"INSERT INTO `" . $this->table . "` ".
					"(" . $query_string1 . ") ".
					"VALUES (" . $query_string2 . ") ";


				if ($this->debugmode) {
					print_r($query);
				}

				if ($this->isLoggermode() && $this->isInsertLogInfoSet()) {
					$this->DB->query($query, $this->insertLogInfo);
					$this->insertId = $this->DB->getOriginalInsertId();
				} else {
					$this->DB->query($query);
					$this->insertId = $this->DB->getInsertId();
				}

				$this->DB->reset();
				$this->insertmode = 0;
				return TRUE;

			} else {
				$query = "";
				$query_string1 = "";

				if (!$this->isQKeySet() || !$this->isTableSet()) {
					return FALSE;
				}

				foreach ($this->fieldsArray as $field => $subarray) {
					if ($pregmatch == "" || preg_match($pregmatch, $field)) {
						$ignore = FALSE;
						if (!$this->isForcemode()) {
							if (!($this->isFieldFromDB($field) && $this->isFieldSet($field) && $subarray['extra'] != "auto_increment")) {
								$ignore = TRUE;
							}
						}

						if (!$ignore) {

							if ($subarray['null'] == "" && $subarray['default'] == "" && $subarray['value'] == "") {
								return FALSE;
							}

							if (!$this->isFieldExcluded($field)) {
								$revertnull = 0;
								if ($subarray['value'] == "" && $subarray['null'] != "") {
									$subarray_revert['noquote'] = $subarray['noquote'];
									$subarray_revert['value'] = $subarray['value'];
									$subarray['noquote'] = "1";
									$subarray['value'] = "NULL";
									$revertnull = 1;
								}

								if ($subarray['noquote'] == 1) {
									$value = $subarray['value'];
								} else {
									if ($this->autoslashes) {
										$value = "'" . addslashes($subarray['value']) . "'";
									} else {
										$value = "'" . $subarray['value'] . "'";
									}
								}

								$query_string1 .= "`" . $field . "` = " . $value . ", ";

								if ($revertnull) {
									$subarray['noquote'] = $subarray_revert['noquote'];
									$subarray['value'] = $subarray_revert['value'];
								}
							}
						}
					}
				}

				if ($query_string1 == "") {
					return FALSE;
				}

				$query_string1 = substr($query_string1, 0, strlen($query_string1)-2);

				$query =
					"UPDATE `" . $this->table . "` ".
					"SET " . $query_string1 . " ".
					"WHERE `" . $this->qkeyArray['field'] . "` = '" . $this->qkeyArray['value'] . "'";

				if ($this->automode) {
					print_r($query);
				}

				if ($this->isLoggermode() && $this->isUpdateLogInfoSet()) {
					$this->DB->query($query, $this->updateLogInfo);
				} else {
					$this->DB->query($query);
				}

				return TRUE;
			}
		}


		// LOAD RELATED FUNCTIONS //

		/**
		* Load the value of a given field from the DB
		* (WHERE $this->qkeyArray['field'] = $this->qkeyArray['value']) to $this->fieldsArray.
		* The query key and the table must be set
		*
		* @access public
		* @param string $field : field to load the value from
		* @return boolean : returns false only if the generated query is bogus
		*/
		function load($field = "") {
			if ($field == "" || !$this->isQKeySet() || $this->isFieldExcluded($field)) {
				return FALSE;
			}
			$ignore = FALSE;
			if (!$this->isForcemode()) {
				if (!$this->isFieldFromDB($field)) {
					$ignore = TRUE;
				}
			}
			if (!$ignore) {
				$value = "";
				$query =
					"SELECT `" . $field . "` ".
					"FROM `" . $this->table . "` ".
					"WHERE `" . $this->qkeyArray['field'] . "` = '" . $this->qkeyArray['value'] . "'";
				$this->DB->query($query);
				while ($this->DB->next_record()) {
					$this->fieldsArray[$field]['value'] = $this->DB->getField($field);
				}
				return TRUE;
			}
			return FALSE;
		}

		/**
		* Load the values from the DB (WHERE $this->qkeyArray['field'] = $this->qkeyArray['value'])
		* for all listed fields in $this->fieldsArray, except for those flagged 'exclude'.
		* The values are then stored in $this->fieldsArray
		* The pregmatch string will filter out the fields
		* The query key and the table must be set
		* $this->fieldsArray must contain at least one field
		*
		* @access public
		* @param string $pregmatch : preg match string
		* @return boolean : returns false if the generated query is bogus
		*/
		function loadAll($pregmatch = "") {
			if (!$this->isQKeySet() || !$this->isTableSet() || count($this->fieldsArray) == 0) {
				return FALSE;
			}
			$queryString = "";
			foreach ($this->fieldsArray as $field => $subarray) {
				if ($pregmatch == "" || preg_match($pregmatch, $field)) {
					$ignore = FALSE;
					if (!$this->isForcemode()) {
						if (!$this->isFieldFromDB($field)) {
							$ignore = TRUE;
						}
					}
					if (!$ignore) {
						if (!isset($subarray['exclude']) || (isset($subarray['exclude']) && $subarray['exclude'] != 1)) {
							$queryString .= "`" . $field . "`, ";
						}
					}
				}
			}
			$queryString = substr($queryString, 0, strlen($queryString)-2);
			$query =
				"SELECT " . $queryString . " ".
				"FROM `" . $this->table . "` ".
				"WHERE `" . $this->qkeyArray['field'] . "` = '" . $this->qkeyArray['value'] . "'";

			$this->DB->query($query);
			$this->DB->next_record();
			foreach ($this->fieldsArray as $field => $subarray) {
				$this->fieldsArray[$field]['value'] = $this->DB->getField($field);
			}
			return TRUE;
		}


		// DELETE RELATED FUNCTIONS //

		/**
		* Delete the working row from the DB
		* The query key and table must be set
		*
		* @access public
		* @param void
		* @return boolean : returns false only if the generated query is bogus
		*/
		function delete() {
			if (!$this->isQKeySet() || !$this->isTableSet()) {
				return FALSE;
			}
			$query =
				"DELETE ".
				"FROM `" . $this->table . "` ".
				"WHERE `" . $this->qkeyArray['field'] . "` = '" . $this->qkeyArray['value'] . "'";

			if ($this->isLoggermode() && $this->isDeleteLogInfoSet()) {
				$this->DB->query($query, $this->deleteLogInfo);
			} else {
				$this->DB->query($query);
			}
			return TRUE;
		}


    }
