<?PHP

	importBD("com.quiboweb.sql.SingleRowHandler");

    class User extends SingleRowHandler {

		// ---------
		// VARIABLES
		// ---------
		var $rightsArray = array();
		var $rightsTotal;

		// -----------
		// CONSTUCTEUR
		// -----------
		function User($DB = "", $fieldsArray = "", $quickmode = 0) {

			$this->setTable("utilisateurs");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}
			if ($quickmode) {
				$this->quickmode();
			}
   		}


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------

		function isActif() {
			if ($this->isQuickmode()) {
				$this->load("actif");
			}
			if ($this->get("actif") == 1) {
				return TRUE;
			}
			return FALSE;
		}
    }

