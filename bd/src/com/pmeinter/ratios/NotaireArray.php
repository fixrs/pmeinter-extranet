<?php
class NotaireArray {
	public $rows = array();
	private $currentIndex;
	
	function __construct($rows=''){
		if($rows!=''){
			$this->rows = $rows;
			$this->currentIndex=count($rows)-1;
			$this->current = $rows[$this->currentIndex];
		}
	}
	
	function addRow(NotaireRow $row){
		array_push($this->rows,$row);
	}
	
	function moveTo(int $int){
		if(isset($this->rows[$int])){
			$this->currentIndex = $int;
			return($this->getCurrent());
		}else{
			return(null);
		}
	}
	
	function next(){
		$this->currentIndex += 1;
		return($this->getCurrent());
	}
	
	function getCurrent(){
		if($this->rows[$this->currentIndex] instanceOf NotaireRow) return($this->rows[$this->currentIndex]);
		return(null);
	}
	
	function chunk($nb){
		if(empty($this->rows)) return(null);
		$nbChunks = count($this->rows[0]->chunk($nb));
		
		
		$NotaireArrays = array();
		
		for($i=0;$i<$nbChunks;$i++){
			array_push($NotaireArrays,new NotaireArray());
		}
		
		foreach($this->rows as $row){
			$newRows = $row->chunk($nb);
			foreach($newRows as $index=>$tmpRow){
				$NotaireArrays[$index]->addRow($tmpRow);
			}
		}
		return($NotaireArrays);
	}
}
?>