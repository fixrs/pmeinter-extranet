<?php

class RatioObject {
	public $key;
	public $DB;
	public $etudeKey;
	public $complete;
	public $completeDate;
	public $annee;
	public $nomUtilisateur;
	public $nbNotairesAssocies;
	public $nbNotairesSalaries;
	public $nbTotalNotaires;
	public $nbEmployes;
	public $nbStagiaires;
	public $nbSocietes;
	public $nbFeducies;
	public $nbDossiersAG;
	public $nbDossiersMC;
	public $nbDossiers;
	public $nbMinutes;
	public $tauxRemboursement;
	public $minutesExcluentRadiation;
	public $accepteMandatsExterieurs;
	public $numerisationPieces;
	public $particularitesAnnee;
	public $forfaitHonorairesResidentiel;
	public $forfaitHonorairesTestaments;
	public $forfaitHonorairesCorporatif;
	public $forfaitHonorairesFeducie;
	public $forfaitHonorairesAG;
	public $forfaitHonorairesMC;
	public $forfaitHonorairesHypotheque;
	public $forfaitHonorairesIncorporation;
	public $forfaitHonorairesHMN;
	public $forfaitHonorairesRPM;
	public $forfaitHonorairesHTN;
	public $tauxHoraireNotaireMoinsDe5ans;
	public $tauxHoraireNotaire5a10ans;
	public $tauxHoraireNotairePlusDe10ans;
	public $tauxHoraireCollaborateur;
	public $chiffreAffaire;
	public $deboursRefactures;
	public $beneficesNets;
	public $salairesColl;
	public $salairesNotaires;
	public $depenseLoyer;
	public $depenseDeboursesDossiers;
	public $depenseIntersPretsMarge;
	public $depenseAmortissement;
	public $depenseTelecom;
	public $depenseSoutien;
	public $depensePublicite;
	public $depenseRepresentation;
	public $autresDepenses;
	public $recevables;
	public $travauxEnCours;
	public $totalActifs;
	public $totalPassifs;
	public $capitalAssocies;
	public $nbHeuresFacturablesAnnee;
	public $nbHeuresFacturablesCollaborateursAnnee;
	public $secretaireJuridiqueJuniorMax;
	public $secretaireJuridiqueJuniorMin;
	public $secretaireJuridiqueSeniorMax;
	public $secretaireJuridiqueSeniorMin;
	public $technicienJuridiqueJuniorMax;
	public $technicienJuridiqueJuniorMin;
	public $technicienJuridiqueSeniorMax;
	public $technicienJuridiqueSeniorMin;
	public $receptionnisteMax;
	public $receptionnisteMin;
	public $adjointeAdministrativeMax;
	public $adjointeAdministrativeMin;
	public $notaireSalarieJuniorMax;
	public $notaireSalarieJuniorMin;
	public $notaireSalarieSeniorMax;
	public $notaireSalarieSeniorMin;
	public $stagiaireNotariatMax;
	public $stagiaireNotariatMin;
	public $stagiaireTechniquesJuridiquesMax;
	public $stagiaireTechniquesJuridiquesMin;
	public $immobilierExistant;
	public $immobilierNeuf;
	public $commercial;
	public $corporatif;
	public $immobilierCommInd;
	public $serviceCorporatif;
	public $testamentsMandats;
	public $donationsFiduciaires;
	public $sucessions;
	public $mediationFamiliale;
	public $proceduresNonContent;
	public $protectionPatrimoine;
	public $autresPersonneFamilial;
	public $agricole;
	public $municipalAdministratif;
	public $divers;
	public $diversTotal;
	public $facturation;
	public $depensesTotales;
	public $salaires;
	public $immobilierResidentiel;

	public $immobilierResidentielCondoExistant;
	public $immobilierResidentielCondoNeuf;
	public $constitutionServicesCondo;

	public $commercialCorporatif;
	public $personneSuccessionFamilial;
	public $agricoleMunicipaleAdministratif;
	public $nbDossiersParNotaire;
	public $nbMinutesParNotaire;
	public $honoraires;
	public $immobilierResidentielPourcent;
	public $commercialCorporatifPourcent;
	public $personneSuccessionFamilialPourcent;
	public $agricoleMunicipaleAdministratifPourcent;
	public $diversPourcent;
	public $beneficeNetSurCAPourcent;
	public $profitAvantPrelevementAvecSalairePourcent;
	public $profitApresPrelevementPourcent;
	public $fondRoulement;
	public $seuilRentabilite;
	public $pointMort;
	public $coefficient_multiplicateur_ressources;
	public $benefice_pour_15;
	public $delaiPerception;
	public $recevableSurFacturationPourcent;
	public $tecSurFacturationPourcent;
	public $salairesSurCAPourcent;
	public $salaireCollSurCAPourcent;
	public $salaireNotSurCAPourcent;
	public $salairesSurDepensesPourcent;
	public $salairesSurDepenses;
	public $depensesLoyerSurTotal;
	public $depensesDeboursDossierSurTotal;
	public $depensesInteretSurTotal;
	public $depensesAmortissementSurTotal;
	public $depensesTelecomSurTotal;
	public $depensesSoutienSurTotal;
	public $depensesPubliciteSurTotal;
	public $depensesRepresentationSurTotal;
	public $depensesAutresSurTotal;
	public $beneficeNetSurCA;
	public $salairesSurCA;
	public $depenseLoyerSurCA;
	public $depensesDeboursDossierSurCA;
	public $depensesInteretsSurCA;
	public $depensesAmortissementSurCA;
	public $depensesTelecomSurCA;
	public $depensesSoutienSurCA;
	public $depensesPubliciteSurCA;
	public $depensesRepresentationSurCA;
	public $depensesAutreSurCA;
	public $salaireMoyenEmploye;
	public $salaireMoyenCollabo;
	public $salaireMoyenNotaireSalarie;
	public $dossierParMois;
	public $minutesParMois;
	public $dossiersParNotaireParAnnee;
	public $minutesParNotaireParAnnee;
	public $coutMoyenParDossier;
	public $chiffreAffairesParDossier;
	public $coutMoyenParMinute;
	public $chiffreAffairesParMin;
	public $nbCollaborateursParNotaire;
	public $chiffreAffairesParNotaire;
	public $chiffreAffairesParCollabo;
	public $coutParHeure;
	public $tauxHoraireMoyen;


	function __construct(Array $record) {
		$this->DB = $record["db"];
		$this->key = $record['key'];
		$this->etudeKey = $record['etudes_key'];
		$this->annee = $record['annee'];
		$this->complete = $record['complete'];
		$this->completeDate = $record['complete_date'];
		$this->nomUtilisateur = $record['nom_utilisateur'];

		// DONNÉES GÉNÉRALES => Nombre de notaires associés
		$this->nbNotairesAssocies = $this->toFloat($record['nb_notaires_associes']);

		// DONNÉES GÉNÉRALES => Nombre de notaires salariés
		$this->nbNotairesSalaries = $this->toFloat($record['nb_notaires_salaries']);

		// DONNÉES GÉNÉRALES => Nombre total de notaires
		$this->nbTotalNotaires = $record['nb_total_notaires'];

		// DONNÉES GÉNÉRALES => Nombre d'employés (notaires exclus) <-- Collaborateurs(trices)
		$this->nbEmployes = $this->toFloat($record['nb_employes']);

		// DONNÉES GÉNÉRALES => Nombre de stagiaires
		$this->nbStagiaires = $this->toFloat($record['nb_stagieres']);

		// DONNÉES GÉNÉRALES => Nombre de sociétés membres du service corpo
		$this->nbSocietes = $this->toFloat($record['nb_societes']);
		$this->nbFeducies = $this->toFloat($record['nb_feducies']);
		$this->nbDossiersAG = $this->toFloat($record['nb_dossiers_ag']);
		$this->nbDossiersMC = $this->toFloat($record['nb_dossiers_mc']);

		// DONNÉES GÉNÉRALES => Nombre de dossiers
		$this->nbDossiers = $this->toFloat($record['nb_dossiers']);

		// DONNÉES GÉNÉRALES => Nombre de minutes
		$this->nbMinutes = $this->toFloat($record['nb_minutes']);

		// DONNÉES GÉNÉRALES => Taux de remboursement par kilomètre
		$this->tauxRemboursement = $this->toFloat($record['taux_remboursement']);

		// DONNÉES GÉNÉRALES => Les minutes excluent-elles les actes de radiation ?
		$this->minutesExcluentRadiation = $this->toFloat($record['minutes_excluent_radiation']);

		// DONNÉES GÉNÉRALES => Accepte les mandats provenant de centres de traitement des dossiers immobiliers (FCT)
		$this->accepteMandatsExterieurs = $this->toFloat($record['accepte_mandats_exterieurs']);

		// DONNÉES GÉNÉRALES => Numérisation des pièces d'identité et autres documents
		$this->numerisationPieces = $this->toFloat($record['numerisation_pieces']);
		$this->particularitesAnnee = $record['particularites_annee'];

		// TARIFICATION => Forfait honoraires (sans frais et taxes) pour un prêt/vente résidentiel pour une propriété d'une valeur de 200 000 $ et moins
		$this->forfaitHonorairesResidentiel = $this->toFloat($record['forfait_honoraires_residentiel']);

		// TARIFICATION => Forfait honoraires (sans frais et taxes) pour deux testaments et mandats (couple) simples
		$this->forfaitHonorairesTestaments = $this->toFloat($record['forfait_honoraires_testaments']);

		// TARIFICATION => Forfait honoraires (sans frais et taxes) pour le service corporatif de base
		$this->forfaitHonorairesCorporatif = $this->toFloat($record['forfait_honoraires_corporatif']);
		$this->forfaitHonorairesFeducie = $this->toFloat($record['forfait_honoraires_feducie']);
		$this->forfaitHonorairesAG = $this->toFloat($record['forfait_honoraires_ag']);
		$this->forfaitHonorairesMC = $this->toFloat($record['forfait_honoraires_mc']);

		// TARIFICATION => Forfait honoraires (sans frais et taxes) pour une quittance ou mainlevée - hypothèque d'un vendeur d'une propriété résidentiel
		$this->forfaitHonorairesHypotheque = $this->toFloat($record['forfait_honoraires_hypotheque']);

		// TARIFICATION => Forfait honoraires (sans frais et taxes) pour une nouvelle incorporation
		$this->forfaitHonorairesIncorporation = $this->toFloat($record['forfait_honoraires_incorporation']);
		$this->forfaitHonorairesHMN = $this->toFloat($record['forfait_honoraires_hmn']);
		$this->forfaitHonorairesRPM = $this->toFloat($record['forfait_honoraires_rpm']);
		$this->forfaitHonorairesHTN = $this->toFloat($record['forfait_honoraires_htn']);

		// TARIFICATION => Taux horaire pour notaire de moins de 5 ans d'expérience
		$this->tauxHoraireNotaireMoinsDe5ans = $this->toFloat($record['taux_horaire_notaire_moins_de_5_ans']);

		// TARIFICATION => Taux horaire pour notaire de 5 à 10 ans d'expérience
		$this->tauxHoraireNotaire5a10ans = $this->toFloat($record['taux_horaire_notaire_5_a_10_ans']);

		// TARIFICATION => Taux horaire pour notaire de plus de 10 ans d'expérience
		$this->tauxHoraireNotairePlusDe10ans = $this->toFloat($record['taux_horaire_notaire_plus_de_10_ans']);

		// TARIFICATION => Taux horaire pour collaborateur(rices)s
		$this->tauxHoraireCollaborateur = $this->toFloat($record['taux_horaire_collaborateur']);

		// RÉPARTITION DES HONORAIRES => Immobilier résidentiel => Immobilier existant
		$this->immobilierExistant = $this->toFloat($record['immobilier_existant']);

		// RÉPARTITION DES HONORAIRES => Immobilier résidentiel => Immobilier neuf
		$this->immobilierNeuf = $this->toFloat($record['immobilier_neuf']);

		// RÉPARTITION DES HONORAIRES => Immobilier résidentiel => TOTAL
		$this->immobilierResidentiel = $this->toFloat($record['immobilier_residentiel']);

		$this->immobilierResidentielCondoExistant = $this->toFloat($record['immobilier_res_ce']);
		$this->immobilierResidentielCondoNeuf = $this->toFloat($record['immobilier_res_cn']);
		$this->constitutionServicesCondo = $this->toFloat($record['constitution_services_co']);


		// RÉPARTITION DES HONORAIRES => Commercial & corporatif => Commercial
		$this->commercial = $this->toFloat($record['commercial']);

		// RÉPARTITION DES HONORAIRES => Commercial & corporatif => Corporatif
		$this->corporatif = $this->toFloat($record['corporatif']);

		// RÉPARTITION DES HONORAIRES => Commercial & corporatif => Immobilier commercial et industriel
		$this->immobilierCommInd = $this->toFloat($record['immobilier_comm_ind']);

		// RÉPARTITION DES HONORAIRES => Commercial & corporatif => Service corporatif
		$this->serviceCorporatif = $this->toFloat($record['service_corporatif']);

		// RÉPARTITION DES HONORAIRES => Commercial & corporatif => TOTAL
		$this->commercialCorporatif = $this->toFloat($record['commercial_corporatif']);

		// RÉPARTITION DES HONORAIRES => Personne, succession & familial => Testaments et mandats
		$this->testamentsMandats = $this->toFloat($record['testaments_mandats']);

		// RÉPARTITION DES HONORAIRES => Personne, succession & familial => Donations fiducières
		$this->donationsFiduciaires = $this->toFloat($record['donations_fiduciaires']);

		// RÉPARTITION DES HONORAIRES => Personne, succession & familial => Successions
		$this->sucessions = $this->toFloat($record['sucessions']);

		// RÉPARTITION DES HONORAIRES => Personne, succession & familial => Médiation familiale
		$this->mediationFamiliale = $this->toFloat($record['mediation_familiale']);

		// RÉPARTITION DES HONORAIRES => Personne, succession & familial => Procédures non contentieuses
		$this->proceduresNonContent = $this->toFloat($record['procedures_non_content']);

		// RÉPARTITION DES HONORAIRES => Personne, succession & familial => Protection du patrimoine
		$this->protectionPatrimoine = $this->toFloat($record['protection_patrimoine']);

		// RÉPARTITION DES HONORAIRES => Personne, succession & familial => Autres - personnes et familial
		$this->autresPersonneFamilial = $this->toFloat($record['autres_personne_familial']);

		// RÉPARTITION DES HONORAIRES => Personne, succession & familial => TOTAL
		$this->personneSuccessionFamilial = $this->toFloat($record['personne_succession_familial']);

		// RÉPARTITION DES HONORAIRES => Agricole, municipale & administratif => Agricole
		$this->agricole = $this->toFloat($record['agricole']);

		// RÉPARTITION DES HONORAIRES => Agricole, municipale & administratif => Municipale et administratif
		$this->municipalAdministratif = $this->toFloat($record['municipal_administratif']);

		// RÉPARTITION DES HONORAIRES => Agricole, municipale & administratif => TOTAL
		$this->agricoleMunicipaleAdministratif = $this->toFloat($record['agricole_municipale_administratif']);

		// RÉPARTITION DES HONORAIRES => Divers => Divers
		$this->divers = $this->toFloat($record['divers']);

		// RÉPARTITION DES HONORAIRES => Divers => TOTAL
		$this->diversTotal = $this->toFloat($record['divers_total']);

		// STATISTIQUES SALARIALES => Salaire - Secrétaire juridique junior min
		$this->secretaireJuridiqueJuniorMin = $this->toFloat($record['secretaire_juridique_junior_min']);

		// STATISTIQUES SALARIALES => Salaire - Secrétaire juridique junior max
		$this->secretaireJuridiqueJuniorMax = $this->toFloat($record['secretaire_juridique_junior']);

		// STATISTIQUES SALARIALES => Salaire - Secrétaire juridique sénior min
		$this->secretaireJuridiqueSeniorMin = $this->toFloat($record['secretaire_juridique_senior_min']);

		// STATISTIQUES SALARIALES => Salaire - Secrétaire juridique sénior max
		$this->secretaireJuridiqueSeniorMax = $this->toFloat($record['secretaire_juridique_senior']);

		// STATISTIQUES SALARIALES => Salaire - Technicien juridique junior min
		$this->technicienJuridiqueJuniorMin = $this->toFloat($record['technicien_juridique_junior_min']);

		// STATISTIQUES SALARIALES => Salaire - Technicien juridique junior max
		$this->technicienJuridiqueJuniorMax = $this->toFloat($record['technicien_juridique_junior']);

		// STATISTIQUES SALARIALES => Salaire - Technicien juridique senior min
		$this->technicienJuridiqueSeniorMin = $this->toFloat($record['technicien_juridique_senior_min']);

		// STATISTIQUES SALARIALES => Salaire - Technicien juridique senior max
		$this->technicienJuridiqueSeniorMax = $this->toFloat($record['technicien_juridique_senior']);

		// STATISTIQUES SALARIALES => Salaire - Réceptionniste min
		$this->receptionnisteMin = $this->toFloat($record['receptionniste_min']);

		// STATISTIQUES SALARIALES => Salaire - Réceptionniste max
		$this->receptionnisteMax = $this->toFloat($record['receptionniste']);

		// STATISTIQUES SALARIALES => Salaire - Adjointe administrative min
		$this->adjointeAdministrativeMin = $this->toFloat($record['adjointe_administrative_min']);

		// STATISTIQUES SALARIALES => Salaire - Adjointe administrative max
		$this->adjointeAdministrativeMax = $this->toFloat($record['adjointe_administrative']);

		// STATISTIQUES SALARIALES => Salaire - Notaire salarié junior min
		$this->notaireSalarieJuniorMin = $this->toFloat($record['notaire_salarie_junior_min']);

		// STATISTIQUES SALARIALES => Salaire - Notaire salarié junior max
		$this->notaireSalarieJuniorMax = $this->toFloat($record['notaire_salarie_junior']);

		// STATISTIQUES SALARIALES => Salaire - Notaire salarié senior min
		$this->notaireSalarieSeniorMin = $this->toFloat($record['notaire_salarie_senior_min']);

		// STATISTIQUES SALARIALES => Salaire - Notaire salarié senior max
		$this->notaireSalarieSeniorMax = $this->toFloat($record['notaire_salarie_senior']);

		// STATISTIQUES SALARIALES => Salaire - Stagiaire en notariat min
		$this->stagiaireNotariatMin = $this->toFloat($record['stagiaire_notariat_min']);

		// STATISTIQUES SALARIALES => Salaire - Stagiaire en notariat max
		$this->stagiaireNotariatMax = $this->toFloat($record['stagiaire_notariat']);

		// STATISTIQUES SALARIALES => Salaire - Stagiaire en techniques juridiques min
		$this->stagiaireTechniquesJuridiquesMin = $this->toFloat($record['stagiaire_techniques_juridiques_min']);

		// STATISTIQUES SALARIALES => Salaire - Stagiaire en techniques juridiques max
		$this->stagiaireTechniquesJuridiquesMax = $this->toFloat($record['stagiaire_techniques_juridiques']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Nombre d'employés
		$this->nbEmployes = $this->toFloat($record['nb_employes']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Facturation
		$this->facturation = $this->toFloat($record['facturation']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Chiffre d'affaire
		$this->chiffreAffaire = $this->toFloat($record['chiffre_affaires']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Débours refacturés
		$this->deboursRefactures = $this->toFloat($record['debours_refactures']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Bénéfices nets
		$this->beneficesNets = $this->toFloat($record['benefices_nets']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Salaires collaborateurs
		$this->salairesColl = $this->toFloat($record['salaires_coll']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Salaires des notaires
		$this->salairesNotaires = $this->toFloat($record['salaires_notaires']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Salaires
		$this->salaires = $this->toFloat($record['salaires']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Dépenses de loyer
		$this->depenseLoyer = $this->toFloat($record['depense_loyer']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Dépense - Débours de dossier
		$this->depenseDeboursesDossiers = $this->toFloat($record['depense_debourses_dossiers']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Dépense - Intérêts sur prêts et marge
		$this->depenseIntersPretsMarge = $this->toFloat($record['depense_inters_prets_marge']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Dépense - Amortissement
		$this->depenseAmortissement = $this->toFloat($record['depense_amortissement']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Dépense - Telecom
		$this->depenseTelecom = $this->toFloat($record['depense_telecom']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Dépense - Soutien
		$this->depenseSoutien = $this->toFloat($record['depense_soutien']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Dépense - Soutien
		$this->depensePublicite = $this->toFloat($record['depense_publicite']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Dépense - Soutien
		$this->depenseRepresentation = $this->toFloat($record['depense_representation']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Dépense - Autres
		$this->autresDepenses = $this->toFloat($record['autres_depenses']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Dépenses totales
		$this->depensesTotales = $this->toFloat($record['depenses_totales']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Recevables à la fin de la période
		$this->recevables = $this->toFloat($record['recevables']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Travaux en cours à la fin de la période
		$this->travauxEnCours = $this->toFloat($record['travaux_en_cours']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Total des actifs à court terme - fin période
		$this->totalActifs = $this->toFloat($record['total_actifs']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Total des passifs à court terme - fin période
		$this->totalPassifs = $this->toFloat($record['total_passifs']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Capital ou avoir des associés
		$this->capitalAssocies = $this->toFloat($record['capital_associes']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Nombre d'heures facturables notaires/année
		$this->nbHeuresFacturablesAnnee = $this->toFloat($record['nb_heures_facturables_annee']);

		// RAPPORT DE LA SAISIE DES DONNÉES => Nombre d'heures facturables collaborateur/année
		$this->nbHeuresFacturablesCollaborateursAnnee = $this->toFloat($record['nb_heures_facturables_collaborateurs_annee']);

		$prelevements = 119810;
		if (isset($this->DB) && $this->DB != "") {
			$this->DB->query("SELECT * FROM `ratios_controls` WHERE `annee` = '" . $this->annee . "'");

			while ($this->DB->next_record()) {
				$prelevements = $this->DB->getField("prelevements");
			}
		}

		if ($this->annee < 2013) {
			$prelevements = 72500;
		}

		// DONNÉES GÉNÉRALES => Nombre de dossiers par notaire
		if ($this->nbTotalNotaires !== NULL && $this->nbTotalNotaires != 0) {
			$this->nbDossiersParNotaire = $this->toString($this->nbDossiers / $this->nbTotalNotaires);
		} else {
			$this->nbDossiersParNotaire = NULL;
		}

		// DONNÉES GÉNÉRALES => Nombre de minutes par notaire
		if ($this->nbTotalNotaires !== NULL && $this->nbTotalNotaires != 0) {
			$this->nbMinutesParNotaire = $this->toString($this->nbMinutes / $this->nbTotalNotaires);
		} else {
			$this->nbMinutesParNotaire = NULL;
		}

		// RÉPARTITION DES HONORAIRES => Total des honoraires
		$this->honoraires = $this->toFloat($this->immobilierResidentiel + $this->commercialCorporatif + $this->personneSuccessionFamilial + $this->agricoleMunicipaleAdministratif + $this->diversTotal);

		// RÉPARTITION DES HONORAIRES => Immobilier résidentiel => Pourecentage
		if ($this->honoraires != 0) {
			$this->immobilierResidentielPourcent = $this->toPercent($this->immobilierResidentiel / $this->honoraires);
		}

		// RÉPARTITION DES HONORAIRES => Commercial & corporatif => Pourcentage
		if ($this->honoraires != 0) {
			$this->commercialCorporatifPourcent = $this->toPercent($this->commercialCorporatif / $this->honoraires);
		}

		// RÉPARTITION DES HONORAIRES => Personne, succession & familial => Pourcentage
		if ($this->honoraires != 0) {
			$this->personneSuccessionFamilialPourcent = $this->toPercent($this->personneSuccessionFamilial / $this->honoraires);
		}

		// RÉPARTITION DES HONORAIRES => Agricole, municipale & administratif => Pourcentage
		if ($this->honoraires != 0) {
			$this->agricoleMunicipaleAdministratifPourcent = $this->toPercent($this->agricoleMunicipaleAdministratif / $this->honoraires);
		}

		// RÉPARTITION DES HONORAIRES => Divers => Pourcentage
		if ($this->honoraires != 0) {
			$this->diversPourcent = $this->toPercent($this->diversTotal / $this->honoraires);
		}

		// RATIOS DE RENTABILITÉ ET DE LIQUIDITÉ => % profit (bénéfice) avant prélèvement
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->beneficeNetSurCAPourcent = $this->toPercent($this->beneficesNets / $this->chiffreAffaire);
		} else {
			$this->beneficeNetSurCAPourcent = NULL;
		}

		// RATIOS DE RENTABILITÉ ET DE LIQUIDITÉ => % profit (bénéfice) en excluant des dépenses, les salaires notaires (SN)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->profitAvantPrelevementAvecSalairePourcent = $this->toPercent(($this->beneficesNets + $this->salairesNotaires) / $this->chiffreAffaire);
		} else {
			$this->profitAvantPrelevementAvecSalairePourcent = NULL;
		}

		// RATIOS DE RENTABILITÉ ET DE LIQUIDITÉ => % profit (bénéfice) après prélèvement (P)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {

			$this->profitApresPrelevementPourcent = $this->toPercent(($this->beneficesNets - ($this->nbNotairesAssocies * $prelevements)) / $this->chiffreAffaire);

			// switch ($this->annee) {
			// 	case 2007:
			// 		$this->profitApresPrelevementPourcent = $this->toPercent(($this->beneficesNets - ($this->nbNotairesAssocies * 65000)) / $this->chiffreAffaire);
			// 		break;
			// 	case 2008:
			// 		$this->profitApresPrelevementPourcent = $this->toPercent(($this->beneficesNets - ($this->nbNotairesAssocies * 72500)) / $this->chiffreAffaire);
			// 		break;
			// 	case 2009:
			// 		$this->profitApresPrelevementPourcent = $this->toPercent(($this->beneficesNets - ($this->nbNotairesAssocies * 72500)) / $this->chiffreAffaire);
			// 		break;
			// 	case 2010:
			// 		$this->profitApresPrelevementPourcent = $this->toPercent(($this->beneficesNets - ($this->nbNotairesAssocies * 72500)) / $this->chiffreAffaire);
			// 		break;
			// 	case 2011:
			// 		$this->profitApresPrelevementPourcent = $this->toPercent(($this->beneficesNets - ($this->nbNotairesAssocies * 72500)) / $this->chiffreAffaire);
			// 		break;
			// 	case 2012:
			// 		$this->profitApresPrelevementPourcent = $this->toPercent(($this->beneficesNets - ($this->nbNotairesAssocies * 72500)) / $this->chiffreAffaire);
			// 		break;
			// 	default:
			// 		$this->profitApresPrelevementPourcent = $this->toPercent(($this->beneficesNets - ($this->nbNotairesAssocies * 65000)) / $this->chiffreAffaire);
			// 		break;
			// }
		} else {
			$this->profitApresPrelevementPourcent = NULL;
		}

		// RATIOS DE RENTABILITÉ ET DE LIQUIDITÉ => Fonds de roulement
		if ($this->totalPassifs !== NULL && $this->totalPassifs != 0) {
			$this->fondRoulement = $this->toString($this->totalActifs / $this->totalPassifs);
		} else {
			$this->fondRoulement = NULL;
		}

		// RATIOS DE RENTABILITÉ ET DE LIQUIDITÉ => Seuil de rentabilite
		if ($this->chiffreAffaire >= 0) {
			//if ($prelevements)
			$this->seuilRentabilite = $this->toString(
				((($this->nbNotairesAssocies * $prelevements) + $this->depenseLoyer + $this->depenseIntersPretsMarge + $this->depenseAmortissement) * $this->chiffreAffaire) / ($this->chiffreAffaire - ($this->salaires + $this->depenseDeboursesDossiers + $this->depenseTelecom + $this->depenseSoutien + $this->depensePublicite + $this->depenseRepresentation + $this->autresDepenses))
				);

		} else {
			$this->seuilRentabilite = NULL;
		}


		/*
			Point mort :
			Seuil de rentabilité (SR) * 360
			/Chiffre d'affaires (CA)

		*/
		// RATIOS DE RENTABILITÉ ET DE LIQUIDITÉ => Point mort
		if ($this->seuilRentabilite >= 0) {
			$this->pointMort = (($this->seuilRentabilite * 360) / $this->chiffreAffaire);
		} else {
			$this->pointMort = NULL;
		}

		/*
			Coefficient multiplicateur des ressources :
			Chiffre d'affaires (CA)
			(Salaires totaux (S) + Prélèvements (P))
		*/
		if ($this->chiffreAffaire >= 0 && $this->salaires >= 0 && $prelevements >= 0) {
			$this->coefficient_multiplicateur_ressources = ($this->chiffreAffaire / ($this->salaires + ($prelevements * $this->nbNotairesAssocies)));
			// echo "CA " . $this->annee . " = ". $this->chiffreAffaire . "<br />";
			// echo "S " . $this->annee . " = ". $this->salaires . "<br />";
			// echo "P " . $this->annee . " = ". $prelevements . "<br />";
		} else {
			$this->coefficient_multiplicateur_ressources = NULL;
		}

		// CA qui aurait permis de dégager un bénéfice net de 15% après les prélèvements :
		// (Dépenses totales + Prélèvements (P))
		// (1 - 15%)
		if ($this->depensesTotales >= 0 && $prelevements >= 0) {
			$this->benefice_pour_15 = (($this->depensesTotales + ($prelevements * $this->nbNotairesAssocies)) / 0.85);
			//echo "D " . $this->annee . " = ". $this->depensesTotales . "<br />";
		} else {
			$this->benefice_pour_15 = NULL;
		}


		// RATIOS DES RECEVABLES ET DES SALAIRES => Délai de perception
		if ($this->facturation !== NULL && $this->facturation != 0) {
			$this->delaiPerception = $this->toString(($this->recevables / $this->facturation) * 365);
		} else {
			$this->delaiPerception = NULL;
		}

		// RATIOS DES RECEVABLES ET DES SALAIRES => % des recevables sur facturation
		if ($this->facturation !== NULL && $this->facturation != 0) {
			$this->recevableSurFacturationPourcent = $this->toPercent($this->recevables / $this->facturation);
		} else {
			$this->recevableSurFacturationPourcent = NULL;
		}

		// RATIOS DES RECEVABLES ET DES SALAIRES => % des travaux en cours (TEC) sur facturation
		if ($this->facturation !== NULL && $this->facturation != 0) {
			$this->tecSurFacturationPourcent = $this->toPercent($this->travauxEnCours / $this->facturation);
		} else {
			$this->tecSurFacturationPourcent = NULL;
		}

		// RATIOS DES RECEVABLES ET DES SALAIRES => % salaires sur chiffre d'affaires
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->salairesSurCAPourcent = $this->toPercent($this->salaires / $this->chiffreAffaire);
		} else {
			$this->salairesSurCAPourcent = NULL;
		}

		// RATIOS DES RECEVABLES ET DES SALAIRES => % salaires collabo. sur chiffre d'affaires
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->salaireCollSurCAPourcent = $this->toPercent($this->salairesColl / $this->chiffreAffaire);
		} else {
			$this->salaireCollSurCAPourcent = NULL;
		}

		// RATIOS DES RECEVABLES ET DES SALAIRES => % salaires notaires sur chiffre d'affaires
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->salaireNotSurCAPourcent = $this->toPercent($this->salairesNotaires / $this->chiffreAffaire);
		} else {
			$this->salaireNotSurCAPourcent = NULL;
		}

		// RATIOS DES RECEVABLES ET DES SALAIRES => % salaires sur dépenses totales
		if ($this->depensesTotales !== NULL && $this->depensesTotales != 0) {
			$this->salairesSurDepensesPourcent = $this->toPercent($this->salaires / $this->depensesTotales);
		} else {
			$this->salairesSurDepensesPourcent = NULL;
		}

		// RATIOS DES DÉPENSES => Salaires (S) / Dépenses totales
		if ($this->depensesTotales !== NULL && $this->depensesTotales != 0) {
			$this->salairesSurDepenses = $this->toPercent($this->salaires / $this->depensesTotales);
		} else {
			$this->salairesSurDepenses = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses de loyer / Dépenses totales
		if ($this->depensesTotales !== NULL && $this->depensesTotales != 0) {
			$this->depensesLoyerSurTotal = $this->toPercent($this->depenseLoyer / $this->depensesTotales);
		} else {
			$this->depensesLoyerSurTotal = NULL;
		}

		// RATIOS DES DÉPENSES => Dépense de débours de dossier / Dépenses totales
		if ($this->depensesTotales !== NULL && $this->depensesTotales != 0) {
			$this->depensesDeboursDossierSurTotal = $this->toPercent($this->depenseDeboursesDossiers / $this->depensesTotales);
		} else {
			$this->depensesDeboursDossierSurTotal = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses d'intérêt / Dépenses totales
		if ($this->depensesTotales !== NULL && $this->depensesTotales != 0) {
			$this->depensesInteretSurTotal = $this->toPercent($this->depenseIntersPretsMarge / $this->depensesTotales);
		} else {
			$this->depensesInteretSurTotal = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses d'amortissement / Dépenses totales
		if ($this->depensesTotales !== NULL && $this->depensesTotales != 0) {
			$this->depensesAmortissementSurTotal = $this->toPercent($this->depenseAmortissement / $this->depensesTotales);
		} else {
			$this->depensesAmortissementSurTotal = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses de télécom / Dépenses totales
		if ($this->depensesTotales !== NULL && $this->depensesTotales != 0) {
			$this->depensesTelecomSurTotal = $this->toPercent($this->depenseTelecom / $this->depensesTotales);
		} else {
			$this->depensesTelecomSurTotal = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses de soutien / Dépenses totales
		if ($this->depensesTotales !== NULL && $this->depensesTotales != 0) {
			$this->depensesSoutienSurTotal = $this->toPercent($this->depenseSoutien / $this->depensesTotales);
		} else {
			$this->depensesSoutienSurTotal = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses de publicite / Dépenses totales
		if ($this->depensesTotales !== NULL && $this->depensesTotales != 0) {
			$this->depensesPubliciteSurTotal = $this->toPercent($this->depensePublicite / $this->depensesTotales);
		} else {
			$this->depensesPubliciteSurTotal = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses de representation / Dépenses totales
		if ($this->depensesTotales !== NULL && $this->depensesTotales != 0) {
			$this->depensesRepresentationSurTotal = $this->toPercent($this->depenseRepresentation / $this->depensesTotales);
		} else {
			$this->depensesRepresentationSurTotal = NULL;
		}

		// RATIOS DES DÉPENSES => Autres dépenses / Dépenses totales
		if ($this->depensesTotales !== NULL && $this->depensesTotales != 0) {
			$this->depensesAutresSurTotal = $this->toPercent($this->autresDepenses / $this->depensesTotales);
		} else {
			$this->depensesAutresSurTotal = NULL;
		}

		// RATIOS DES DÉPENSES => Bénéfice Net (BN) / Chiffre d'affaires (CA)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->beneficeNetSurCA = $this->toPercent($this->beneficesNets / $this->chiffreAffaire);
		} else {
			$this->beneficeNetSurCA = NULL;
		}

		// RATIOS DES DÉPENSES => Salaires (S) / Chiffre d'affaires (CA)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->salairesSurCA = $this->toPercent($this->salaires / $this->chiffreAffaire);
		} else {
			$this->salairesSurCA = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses de loyer / Chiffre d'affaires (CA)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->depenseLoyerSurCA = $this->toPercent($this->depenseLoyer / $this->chiffreAffaire);
		} else {
			$this->depenseLoyerSurCA = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses de débours de dossier / Chiffre d'affaires (CA)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->depensesDeboursDossierSurCA = $this->toPercent($this->depenseDeboursesDossiers / $this->chiffreAffaire);
		} else {
			$this->depensesDeboursDossierSurCA = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses d'intérêt / Chiffre d'affaires (CA)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->depensesInteretsSurCA = $this->toPercent($this->depenseIntersPretsMarge / $this->chiffreAffaire);
		} else {
			$this->depensesInteretsSurCA =NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses d'amortissement / Chiffre d'affaires (CA)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->depensesAmortissementSurCA = $this->toPercent($this->depenseAmortissement / $this->chiffreAffaire);
		} else {
			$this->depensesAmortissementSurCA = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses de télécom / Chiffre d'affaires (CA)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->depensesTelecomSurCA = $this->toPercent($this->depenseTelecom / $this->chiffreAffaire);
		} else {
			$this->depensesTelecomSurCA = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses de soutien / Chiffre d'affaires (CA)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->depensesSoutienSurCA = $this->toPercent($this->depenseSoutien / $this->chiffreAffaire);
		} else {
			$this->depensesSoutienSurCA = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses de publicite / Chiffre d'affaires (CA)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->depensesPubliciteSurCA = $this->toPercent($this->depensePublicite / $this->chiffreAffaire);
		} else {
			$this->depensesPubliciteSurCA = NULL;
		}

		// RATIOS DES DÉPENSES => Dépenses de soutien / Chiffre d'affaires (CA)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->depensesRepresentationSurCA = $this->toPercent($this->depenseRepresentation / $this->chiffreAffaire);
		} else {
			$this->depensesRepresentationSurCA = NULL;
		}

		// RATIOS DES DÉPENSES => Autres dépenses / Chiffre d'affaires (CA)
		if ($this->chiffreAffaire !== NULL && $this->chiffreAffaire != 0) {
			$this->depensesAutreSurCA = $this->toPercent($this->autresDepenses / $this->chiffreAffaire);
		} else {
			$this->depensesAutreSurCA = NULL;
		}

		// RATIOS DES DÉPENSES => Salaires et avantages - moyenne par employé
		if ($this->nbEmployes !== NULL && ($this->nbEmployes + $this->nbNotairesSalaries) != 0) {
			$this->salaireMoyenEmploye = $this->toFloat($this->salaires / ($this->nbEmployes + $this->nbNotairesSalaries));
		} else {
			$this->salaireMoyenEmploye = NULL;
		}

		// RATIOS DES DÉPENSES => Salaires et avantages - moyenne par collabo.
		if ($this->nbNotairesAssocies && $this->nbEmployes != 0) {
			$this->salaireMoyenCollabo = $this->toFloat($this->salairesColl / $this->nbEmployes);
		} else {
			$this->salaireMoyenCollabo = NULL;
		}

		// RATIOS DES DÉPENSES => Salaires et avantages - moyenne par notaire salarié
		if ($this->nbNotairesSalaries && $this->nbNotairesSalaries != 0) {
			$this->salaireMoyenNotaireSalarie = $this->toFloat($this->toFloat($this->salairesNotaires) / $this->toFloat($this->nbNotairesSalaries));
		} else {
			$this->salaireMoyenNotaireSalarie = NULL;
		}

		// RATIOS DE PRODUCTION => Dossiers / mois
		if ($this->nbDossiers !== NULL && $this->nbDossiers != 0) {
			$this->dossierParMois = $this->toString($this->nbDossiers / 12,3);
		} else {
			$this->dossierParMois = NULL;
		}

		// RATIOS DE PRODUCTION => Minutes / mois
		if ($this->nbMinutes !== NULL && $this->nbMinutes != 0) {
			$this->minutesParMois = $this->toString($this->nbMinutes / 12,3);
		} else {
			$this->minutesParMois = NULL;
		}

		// RATIOS DE PRODUCTION => Dossiers / notaire / année
		if ($this->nbMinutes !== NULL && $this->nbTotalNotaires != 0) {
			$this->dossiersParNotaireParAnnee = $this->toString($this->nbDossiers / $this->nbTotalNotaires);
		} else {
			$this->dossiersParNotaireParAnnee = NULL;
		}

		// RATIOS DE PRODUCTION => Minutes / notaire / année
		if ($this->nbMinutes !== NULL && $this->nbTotalNotaires != 0) {
			$this->minutesParNotaireParAnnee =  $this->toString($this->nbMinutes / $this->nbTotalNotaires);
		} else {
			$this->minutesParNotaireParAnnee = NULL;
		}

		// RATIOS DE PRODUCTION => Coût moyen / dossier
		if ($this->nbDossiers !== NULL && $this->nbDossiers != 0) {
			$this->coutMoyenParDossier = $this->toFloat($this->depensesTotales / $this->nbDossiers);
		} else {
			$this->coutMoyenParDossier = NULL;
		}

		// RATIOS DE PRODUCTION => Chiffre d'affaires (CA) / dossier
		if ($this->nbDossiers !== NULL && $this->nbDossiers != 0) {
			$this->chiffreAffairesParDossier = $this->toFloat($this->chiffreAffaire / $this->nbDossiers);
		} else {
			$this->chiffreAffairesParDossier = NULL;
		}

		// RATIOS DE PRODUCTION => Coût moyen / minute
		if ($this->nbMinutes !== NULL && $this->nbMinutes != 0) {
			$this->coutMoyenParMinute = $this->toFloat($this->depensesTotales / $this->nbMinutes);
		} else {
			$this->coutMoyenParMinute = NULL;
		}

		// RATIOS DE PRODUCTION => Chiffre d'affaires (CA) / minute
		if ($this->nbMinutes !== NULL && $this->nbMinutes != 0) {
			$this->chiffreAffairesParMin = $this->toFloat($this->chiffreAffaire / $this->nbMinutes);
		} else {
			$this->chiffreAffairesParMin = NULL;
		}

		// RATIOS DE PRODUCTION => Nombre de collaborateurs par notaire
		if ($this->nbMinutes !== NULL && $this->nbTotalNotaires != 0) {
			$this->nbCollaborateursParNotaire = $this->toString($this->nbEmployes / $this->nbTotalNotaires);
		} else {
			$this->nbCollaborateursParNotaire = NULL;
		}

		// RATIOS DE PRODUCTION => Chiffre d'affaires moyen par notaire
		if ($this->nbMinutes !== NULL && $this->nbTotalNotaires != 0) {
			$this->chiffreAffairesParNotaire = $this->toFloat($this->chiffreAffaire / $this->nbTotalNotaires);
		} else {
			$this->chiffreAffairesParNotaire = NULL;
		}

		// RATIOS DE PRODUCTION => Chiffre d'affaires moyen par collaborateur (trice)
		if ($this->nbNotairesAssocies !== NULL && $this->nbEmployes != 0) {
			$this->chiffreAffairesParCollabo = $this->toFloat($this->chiffreAffaire / $this->nbEmployes);
		}

		// RATIOS DE PRODUCTION => Coût par heure facturable
		if ($this->nbHeuresFacturablesAnnee + $this->nbHeuresFacturablesCollaborateursAnnee != 0) {
			$this->coutParHeure = $this->toFloat(($this->depensesTotales - $this->deboursRefactures) / ($this->nbHeuresFacturablesAnnee + $this->nbHeuresFacturablesCollaborateursAnnee));
		} else {
			$this->coutParHeure = NULL;
		}

		// RATIOS DE PRODUCTION => Taux horaire moyen (honoraires par heure facturable)
		if ($this->nbHeuresFacturablesAnnee + $this->nbHeuresFacturablesCollaborateursAnnee != 0) {
			$this->tauxHoraireMoyen = ($this->chiffreAffaire - $this->deboursRefactures) / ($this->nbHeuresFacturablesAnnee + $this->nbHeuresFacturablesCollaborateursAnnee);
		} else {
			$this->tauxHoraireMoyen = 0;
		}
	}


	function toPercent($x) {
		return $this->toFloat($x * 100);
	}

	function toString($str, $decimal = 2) {
		$str = round($str, $decimal);
		$str = str_replace(".", ",", $str);
		return((string) $str);
	}

	function toFloat($str) {
		if ($str == '1') return(true);
		if ($str == '0') return(false);
		if ($str == '' || $str == '0,00') return(NULL);
		return((float) str_replace(" ", "", str_replace(",", ".", $str)));
	}
}
?>
