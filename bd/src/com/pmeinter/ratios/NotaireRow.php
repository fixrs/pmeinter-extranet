<?php
class NotaireRow {

	public $description;
	public $data = array();
	public $unit;
	public $type;
	public $type2;
	public $type3;

	protected $average;
	protected $total;

	function __construct($_description, $_unit = '', $_type = 'normal', $_type2 = '', $_type3 = '') {
		$this->description = $_description;
		$this->unit = $_unit;		
		$this->type = $_type;
		$this->type2 = $_type2;
		$this->type3 = $_type3;
	}
	
	function addData($str) {
		array_push($this->data, $str);
	}
	
	function setData($array) {
		$this->data = $array;
	}
	
	function getAverage($formated = FALSE) {
		return($this->average);
	}

	function setAverage($data = array()) {
		if (empty($data)) {
			$data = $this->data;
		}

if ($_GET['test']) var_dump($data);

		$pourcent = false;
		$string = false;
		$totalItem = 0;
		$total = 0;
		
		$totalNon = 0;
		$totalOui = 0;

		foreach ($data as $int){
			if ($int === true || $int === false) {
				if ($int === true) $totalOui++;
				if ($int === false) $totalNon++;
			} elseif (substr($int, count($int)-2, 1) === '%') {
				$total += $this->preparePourcent($int);
				$totalItem++;	
				$pourcent = true;
			} else {
				if ($int != '') {
					$total += $this->toFloat($int);
					$totalItem++;	
					if ((float) $int !== $int) $string = true;
				}				
			}
		}
		
		if (($totalNon != 0 || $totalOui != 0) && $total == 0) {
			$this->average = ($totalOui > $totalNon ? true : false);
			return;
		}

		if ($totalItem == 0 || $total == 0) {
			$this->average = (0);
			return;
		}
		
		if ((float) $total === $total) {
			if ($pourcent === true) { 
				$this->average = $this->addPercent($total / $totalItem);
				return;
			} elseif ($string === true) { 
				//$this->average = ((string) round($total / $totalItem, 2));
				$this->average = ((string) $total / $totalItem);
				return;
			} else {
				//$this->average = (round($total / $totalItem, 2));
				$this->average = ($total / $totalItem);
				return;
			}
		} else {
			//$this->average = ($totalItem === 0 ? 0 : ((int) round($total / $totalItem)));
			$this->average = ($totalItem === 0 ? 0 : ((int) $total / $totalItem));
			return;
		}
	}

	function getTotal($formated = FALSE) {
		return($this->total);
	}

	function setTotal($data = array()) {
		if (empty($data)) {
			$data = $this->data;
		}

		$pourcent = false;
		$string = false;
		$total = 0;
		
		$totalNon = 0;
		$totalOui = 0;

		foreach ($data as $int){
			if ($int === true || $int === false) {
				$total = null;
			} elseif (substr($int, count($int)-2, 1) === '%') {
				$total += $this->preparePourcent($int);
				$pourcent = true;
			} else {
				if ($int != '') {
					$total += $this->toFloat($int);
					if ((float) $int !== $int) $string = true;
				}				
			}
		}
		
		if (($totalNon != 0 || $totalOui != 0) && $total == 0) $this->total = ($totalOui > $totalNon ? true : false);
		if ($total == 0) $this->total = (0);
		
		if ((float) $total === $total) {
			if ($pourcent === true) { 
				$this->total = $this->addPercent($total);
			} elseif ($string === true) { 
//				$this->total = ((string) round($total, 2));
				$this->total = ((string) $total);
			} else {
//				$this->total = (round($total, 2));
				$this->total = ($total);
			}
		} else {
//			$this->total = (int) round($total);
			$this->total = (int) $total;
		}
	}

	function addPercent($str) {
		$str = str_replace(".", ",", $str);
		return($str);
	}
	
	function preparePourcent($str) {
		$str = substr($str, 0, strlen($str));
		$str = str_replace(",", ".", $str);
		return((float) $str);
	}
	
	function toFloat($str) {
		if ($str == '1') return(true);
		if ($str == '0') return(false);
		if ($str == '' || $str == '0,00') return(null);
		return((float) str_replace(" ", "", str_replace(",", ".", $str)));
	}
	
	function toFrenchFormatHtml($str = "", $dec = "") {
		if ($str == "") {
			return("0,00");
		}
	
		if ($dec == "") {
			$dec = 2;
		}
	
		if (strpos($str, chr(37)) !== FALSE) {
			$pourcent = 1;
			$str = str_replace(chr(37), '', $str);
		}

		$str = str_replace(' ', '', $str);
		$str = str_replace('&nbsp;', '', $str);
		$str = str_replace(',', '.', $str);
				
		$str = number_format($str, $dec, ',', ' ');
		if (strpos($str, ',') === FALSE) {
			$str = $str . ',';
		}

		$str = $str . str_repeat('0', 2 - strpos(strrev($str), ','));
		
		$str = str_replace(' ', '&nbsp;', $str);

		return $str;
	}
	
	function chunk($nb = 15) {
		$nbChunks = ceil(count($this->data) / $nb);
		$indexChunks = 0;
		
		$tmpArray = array_chunk($this->data, $nb);
		$rows = array();
		foreach ($tmpArray as $data) {
			$tmp = new NotaireRow($this->description, $this->type);
			$tmp->setData($data);
			$tmp->average = $this->average;
			array_push($rows, $tmp);
		}
		
		return($rows);
	}
	
}
?>
