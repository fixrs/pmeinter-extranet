<?PHP

	import("com.quiboweb.sql.SingleRowHandler");

    class Convocation_AG_event extends SingleRowHandler {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Convocation_AG_event($DB = "", $fieldsArray = "", $quickmode = 0) { 

			$this->setTable("convocation_ag_event");
        	$this->setInsertLogInfo("Ajout");
        	$this->setUpdateLogInfo("Modification");
        	$this->setDeleteLogInfo("Suppression");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}		
			if ($quickmode) {
				$this->quickmode();
			}
			
        }


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------

		
    }

?>