<?PHP

	import("com.quiboweb.sql.SingleRowHandler");

    class Direction_de_travail_event extends SingleRowHandler {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Direction_de_travail_event($DB = "", $fieldsArray = "", $quickmode = 0) { 

			$this->setTable("directions_de_travail_event");
        	$this->setInsertLogInfo("Ajout");
        	$this->setUpdateLogInfo("Modification");
        	$this->setDeleteLogInfo("Suppression");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}		
			if ($quickmode) {
				$this->quickmode();
			}
			
        }


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------

		
    }

?>