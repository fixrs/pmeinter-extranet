<?PHP

	import("com.quiboweb.sql.SingleRowHandler");

    class Categorie extends SingleRowHandler {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Categorie($DB = "", $fieldsArray = "", $quickmode = 0) { 
			$this->setTable("produits_categories");
        	$this->setInsertLogInfo("Ajout");
        	$this->setUpdateLogInfo("Modification");
        	$this->setDeleteLogInfo("Suppression");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}		
			if ($quickmode) {
				$this->quickmode();
			}
        }


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------

		
    }

?>