<?PHP

	import("com.quiboweb.sql.SingleRowHandler");

    class Evaluation extends SingleRowHandler {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Evaluation($DB = "", $fieldsArray = "", $quickmode = 0) {
			$this->setTable("evaluations");
        	$this->setInsertLogInfo("Ajout");
        	$this->setUpdateLogInfo("Modification");
        	$this->setDeleteLogInfo("Suppression");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}		
			if ($quickmode) {
				$this->quickmode();
			}
   		}


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------


		
    }

?>