<?PHP

	importBD("net.php.Smarty.Smarty");

	class Skin {

		var $SECTION;

		function Skin($SKIN_PATH = "", $SKIN_URL = "", $SECTION = "") {
			global $BASEURL;

			$this->SECTION = $SECTION;

			$this->smarty = new Smarty();

			$this->smarty->template_dir = $SKIN_PATH . '/templates/';
			$this->smarty->compile_dir = $SKIN_PATH . '/templates_c/';
			$this->smarty->config_dir = $SKIN_PATH . '/configs/';
			$this->smarty->cache_dir = $SKIN_PATH . '/cache/';

			$this->smarty->assign("SKIN_URL", $SKIN_URL);
			$this->smarty->assign("BASEURL", $BASEURL);
			$this->smarty->assign("SECTION", $SECTION);
			$this->smarty->assign("YEAR", date("Y"));

			$this->smarty->assign("MAINMENU", $this->mainmenuString());
			$this->smarty->assign("LOGINTAB", $this->logintabString());
			$this->smarty->assign("LIVETAB", $this->livetabString());
		}

		function assign($name, $value) {
			global $DEV;
			if ($name == "title") {
				if ($DEV) {
					$value .= " - PME INTER Notaires * Version de développement * ";
				} else {
					$value .= " - PME INTER Notaires";
				}
			}
			$this->smarty->assign($name, $value);
		}

		function display($template) {
			$this->smarty->display($template);
		}

		function toString($string) {
			$string = preg_replace("/\'\'\'\'\'(.*?)\'\'\'\'\'/", "<b><i>\$1</i></b>", $string);
			$string = preg_replace("/\'\'\'(.*?)\'\'\'/", "<b>\$1</b>", $string);
			$string = preg_replace("/\'\'(.*?)\'\'/", "<i>\$1</i>", $string);

			$string = preg_replace("/\[LINK\s([^\s]*)\s([^\]]*)\]/", "<a href=\"\$1\">\$2</a>", $string);
			$string = preg_replace("/<a href=\"([\w_\.-]+\@[\w_-]+\.[\w_-]+(\.[\w_-]+)?(\.[\w_-]+)?(\.[\w_-]+)?(\.[w_\-]+)?)\"/", "<a href=\"mailto:\$1\"", $string);

			$string = preg_replace("/\[COLOR\s([^\]]*)\]/", "<span style=\"color: #16497A;\">\$1</span>", $string);
			$string = preg_replace("/\[CENTER\s([^\]]*)\]/", "</p><div style=\"text-align: center;\">\$1</div><p>", $string);
			$string = preg_replace("/\[RIGHT\s([^\]]*)\]/", "</p><div style=\"text-align: right;\">\$1</div><p>", $string);

			$lines_array = split("\n", $string);

			$first = 1;
			$last = 0;
			$ul_closed = 0;
			$content = "";
			for ($i = 0; $i < count($lines_array); $i++) {
			        $line = $lines_array[$i];
			        if (preg_match("/^-/", $line)) {
			                if ($first == 1) {
			                        $line = preg_replace("/^-(.*)$/", "</p><ul class=\"infos\">\n<li>\$1</li>", $line);
			                        $first = 0;
			                        $last = $i;
			                } else {
			                        $line = preg_replace("/^-(.*)$/", "<li>\$1</li>", $line);
			                        $first = 0;
			                        $last = $i;
			                }
			                $content .= $line;
			        } else {
			                if ($last == ($i - 1) && $first == 0) {
			                        $line = "</ul>" . $line;
			                        $first = 1;
			                        $ul_closed = 1;
			                }
			                $content .= $line . "\n";
			        }
			}

			if ($last > 0 && $ul_closed != 1) {
			        $content = preg_replace("/(.*<\/li>)/", "\$1\n</ul>\n", $content);
			}

			$content = nl2br(trim($content));
			$content = preg_replace("/<\/ul>\s*<br[^>]*>/", "</ul>", $content);
			$content = preg_replace("/<br[^>]*>\s*(<ul[^>]*>)\s*<br[^>]*>/", "\$1", $content);
			$content = preg_replace("/<br[^>]*>\s*(<\/li[^>]*>)/", "\$1\n", $content);
			$content = preg_replace("/<br[^>]*>\s*(<\/ul[^>]*>)/", "\$1\n", $content);
			$content = preg_replace("/(<ul[^>]*>)\s*<br[^>]*>/", "\$1", $content);
			$content = preg_replace("/<\/li>/", "</li>\n", $content);
			$content = preg_replace("/<\/ul>/", "</ul><p>", $content);
			$content = preg_replace("/<\/form>/", "</form><p>", $content);
			$content = preg_replace("/(<form[^>]*>)/", "</p>\$1", $content);

			$content = preg_replace("/<br[^>]*>\s*<\/p>/", "</p>", $content);
			$content = preg_replace("/<br[^>]*>\s*<\/p>/", "</p>", $content);

			$content = preg_replace("/<p[^>]*>\s*<br[^>]*>/", "<p>", $content);
			$content = preg_replace("/<p[^>]*>\s*<br[^>]*>/", "<p>", $content);

			$content = preg_replace("/<br[^>]*>\s*<br[^>]*>\s*$/", "", $content);
			$content = preg_replace("/^<br[^>]*>\s*<br[^>]*>\s*/", "", $content);


			while (preg_match("/(<script.*?)(<br[^>]*>)(.*?)(<\/script>)/s", $content)) {
				$content = preg_replace("/(<script.*?)(<br[^>]*>)(.*?)(<\/script>)/s", "\$1\n\$3\$4", $content);
			}

			$content = preg_replace("/(<scrip[^>]*>)\s*<br[^>]*>/", "\$1", $content);
			$content = preg_replace("/(<\/script>)\s*<br[^>]*>/", "\$1", $content);

			return $this->ansi2entity($content, 1);
    		}

		function ansi2entity($string, $acceptHTML = 0) {
			$string = htmlentities($string, ENT_NOQUOTES, "UTF-8");

			if ($acceptHTML == 1) {
				$string = preg_replace("/&lt;/i", "<", $string);
				$string = preg_replace("/&gt;/i", ">", $string);
			}

			$string = preg_replace("/&amp;/i", "&", $string);
			$string = preg_replace("/&quot;/i", "\"", $string);

			return $string;
	    	}

		function mainmenuString() {
			global $BASEURL, $FULLURL;

			$html = '';
//			if ($this->SECTION == "evaluations" && $_SERVER['SCRIPT_NAME'] == "/evaluations.php") {


// 			if ($NULL) {
// 				$html =
// 					"<ul class=\"page-sidebar-menu\" data-auto-scroll=\"true\" data-slide-speed=\"200\">\n".
// 					// "	<li id=\"menu-item-9\" class=\"accueil menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-9\">\n".
// 					// "		<a href=\"javascript: submitEvalForm('', '', '', '', '', '', '', '', '', '" . $FULLURL . "index.php');\"><i class=\"fa\"></i><span>Accueil<span class=\"arrow\"></span></span></a>\n".
// 					// "	</li>\n".
// 					"	<li class=\"administration conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"javascript: submitEvalForm('', '', '', '', '', '', '', '', '', '" . $FULLURL . "administration/index.php');\"><i class=\"fa\"></i><span>Administration<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 					"	<li class=\"etudes conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"javascript: submitEvalForm('', '', '', '', '', '', '', '', '', '" . $FULLURL . "etudes/index.php');\"><i class=\"fa\"></i><span>&Eacute;tudes<span class=\"arrow\"></span></span></a\n".
// 					"	</li>\n".
// 					"	<li class=\"employes conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"javascript: submitEvalForm('', '', '', '', '', '', '', '', '', '" . $FULLURL . "employes/index.php');\"><i class=\"fa\"></i><span>Employ&eacute;s<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 					"	<li class=\"dossiers conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"javascript: submitEvalForm('', '', '', '', '', '', '', '', '', '" . $FULLURL . "dossiers/index.php');\"><i class=\"fa\"></i><span>Dossiers<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 					//"	<li class=\"formations\">\n".
// 					//"		<a href=\"javascript: submitEvalForm('', '', '', '', '', '', '', '', '', '" . $FULLURL . "formations/index.php');\"><span>Formations</span></a>\n".
// 					//"	</li>\n".
// 					"	<li class=\"evenements conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"javascript: submitEvalForm('', '', '', '', '', '', '', '', '', '" . $FULLURL . "evenements/index.php');\"><i class=\"fa\"></i><span>&Eacute;v&eacute;nements<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 					"	<li class=\"produits conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"javascript: submitEvalForm('', '', '', '', '', '', '', '', '', '" . $FULLURL . "produits/index.php');\"><i class=\"fa\"></i><span>Produits<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 					"	<li class=\"evaluations current conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"javascript: submitEvalForm('', '', '', '', '', '', '', '', '', '" . $FULLURL . "evaluations/index.php');\"><i class=\"fa\"></i><span>&Eacute;valuations<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// //					"	<li class=\"reseauiq\">\n".
// //					"		<a href=\"javascript: submitEvalForm('', '', '', '', '', '', '', '', '', '" . $FULLURL . "reseauiq/index.php');\"><span>R&eacute;seauIQ</span></a>\n".
// //					"	</li>\n".
// 					"	<li class=\"ratios conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"javascript: submitEvalForm('', '', '', '', '', '', '', '', '', '" . $FULLURL . "ratios/index.php');\"><i class=\"fa\"></i><span>Ratios<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 				"</ul>\n";

// 			} else {

// 				switch ($this->SECTION) {
// 					case "accueil":
// 						$accueil_current = "current";
// 						break;
// 					case "administration":
// 						$administration_current = "current";
// 						break;
// 					case "etudes":
// 						$etudes_current = "current";
// 						break;
// 					case "employes":
// 						$employes_current = "current";
// 						break;
// 					case "dossiers":
// 						$dossiers_current = "current";
// 						break;
// 					case "formations":
// 						$evenements_current = "current";
// 						break;
// 					case "evenements":
// 						$evenements_current = "current";
// 						break;
// 					case "produits":
// 						$produits_current = "current";
// 						break;
// 					case "evaluations":
// 						$evaluations_current = "current";
// 						break;
// 					case "reseauiq":
// 						$reseauiq_current = "current";
// 						break;
// 					case "ratios":
// 						$ratios_current = "current";
// 						break;
// 				}

// 				$html =
// 					"<ul class=\"page-sidebar-menu\" data-auto-scroll=\"true\" data-slide-speed=\"200\">\n".
// 					// "	<li id=\"menu-item-9\" class=\"accueil menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-9\">\n".
// 					// "		<a href=\"" . $BASEURL . "index.php\" class=\"" . $accueil_current . "\"><i class=\"fa\"></i><span>Accueil<span class=\"arrow\"></span></span></a>\n".
// 					// "	</li>\n".
// 					"	<li class=\"administration conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"" . $BASEURL . "administration/index.php\" class=\"" . $administration_current . "\"><i class=\"fa\"></i><span>Administration<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 					"	<li class=\"etudes conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"" . $BASEURL . "etudes/index.php\" class=\"" . $etudes_current . "\"><i class=\"fa\"></i><span>&Eacute;tudes<span class=\"arrow\"></span></span></a\n".
// 					"	</li>\n".
// 					"	<li class=\"employes conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"" . $BASEURL . "employes/index.php\" class=\"" . $employes_current . "\"><i class=\"fa\"></i><span>Employ&eacute;s<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 					"	<li class=\"dossiers conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"" . $BASEURL . "dossiers/index.php\" class=\"" . $dossiers_current . "\"><i class=\"fa\"></i><span>Dossiers<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 					//"	<li class=\"formations\">\n".
// 					//"		<a href=\"" . $BASEURL . "formations/index.php\" class=\"" . $formations_current . "\"><span>Formations</span></a>\n".
// 					//"	</li>\n".
// 					"	<li class=\"evenements conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"" . $BASEURL . "evenements/index.php\" class=\"" . $evenements_current . "\"><i class=\"fa\"></i><span>&Eacute;v&eacute;nements<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 					"	<li class=\"produits conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"" . $BASEURL . "produits/index.php\" class=\"" . $produits_current . "\"><i class=\"fa\"></i><span>Produits<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 					"	<li class=\"evaluations conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"" . $BASEURL . "evaluations/index.php\" class=\"" . $evaluations_current . "\"><i class=\"fa\"></i><span>&Eacute;valuations<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// //					"	<li class=\"reseauiq\">\n".
// //					"		<a href=\"" . $BASEURL . "reseauiq/index.php\" class=\"" . $reseauiq_current . "\"><span>R&eacute;seauIQ</span></a>\n".
// //					"	</li>\n".
// 					"	<li class=\"ratios conseil menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6926\">\n".
// 					"		<a href=\"" . $BASEURL . "ratios/index.php\" class=\"" . $ratios_current . "\"><i class=\"fa\"></i><span>Ratios<span class=\"arrow\"></span></span></a>\n".
// 					"	</li>\n".
// 					"</ul>\n";
// 			}

			return $html;
		}

	    function logintabString() {
			global $BASEURL;

			$html = "";

			// $userkey = $_SESSION['user_key'];
			// $fullname = trim($_SESSION['user_fullname']);
			// $lastlogon = $_SESSION['user_lastlogon'];

			// if ((strlen($fullname) + strlen($userkey)) > 25) {
			// 	$fullname = substr($fullname, 0, 1) . ". " . substr($fullname, strpos($fullname, " "));
			// 	if ((strlen($fullname) + strlen($userkey)) > 25) {
			// 		$fullname = substr($fullname, 0, 25 - strlen($userkey)) . "...";
			// 	}
			// }

			// if (strlen($fullname) > 0) {
			// 	$fullnameSpan = "(<span class=\"g\">" . $fullname . "</span>)";
			// } else {
			// 	$fullnameSpan = "";
			// }

			// if ($userkey != "") {
			// 	$html .=
			// 		"<script type=\"text/javascript\" language=\"javascript\">\n".
			// 		"	function submitLogout() {\n".
			// 		"		var evals_info = new Array();\n".
			// 		"		var openEvalListStr = '';\n";

			// 	if (is_array($_SESSION['evaluations']) && count($_SESSION['evaluations']) > 0) {
			// 		$n = 0;
			// 		foreach ($_SESSION['evaluations'] as $sid_n => $detail) {
			// 			if ($detail['action'] == "modify") {
			// 				$html .=
			// 					"evals_info[" . $n . "] = '- " . addslashes($detail['etudes_nom'] . " (" . $detail['s0_date_visite'] . ")") . " sauvegardée le " . addslashes(substr($detail['updatedOn'], 0, 10) . " à " . substr($detail['updatedOn'], 11)) . ".';\n";
			// 			}
			// 			$n++;
			// 		}
			// 	}

			// 	$html .=
			// 		"		for (i = 0; i < evals_info.length; i++) {\n".
			// 		"			openEvalListStr += evals_info[i] + '\\n';\n".
			// 		"		}\n".
			// 		"		if (evals_info.length > 0) {\n".
			// 		"			if (confirm('Êtes-vous certain de vouloir vous déconnecter ?\\n\\nCertaine(s) évaluation(s) sont en cours de modification :\\n' + openEvalListStr)) {\n".
			// 		"				return true;\n".
			// 		"			} else {\n".
			// 		"				return false;\n".
			// 		"			}\n".
			// 		"		}\n".
			// 		"		return true;\n".
			// 		"	}\n".
			// 		"</script>\n";

			// 	// $html .=
			// 	// 	"<form name=\"logout\" action=\"" . $BASEURL . "logout.php\" method=\"post\" onsubmit=\"javascript: return submitLogout();\">\n".
			// 	// 	"	Connect&eacute; en tant que&nbsp;: <span class=\"g\">" . $userkey . " </span>" . $fullnameSpan . "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Derni&egrave;re connexion : <span class=\"g\">" . $lastlogon . "</span>\n".
			// 	// 	"	<input type=\"submit\" class=\"submit\" value=\"d&eacute;connexion\" />\n".
			// 	// 	"</form>\n";


			// 	$html .=
			// 		'<div class="header navbar navbar-fixed-top">
			// 			<div class="header-inner">
			//             	<div class="searchbox">
			//             		<a id="search-handle"><span></span></a>
			//             		<form role="search" action="http://www.pmeinter.com/extranet" class="search-form" method="get">
			//     					<div class="form-container">
			// 							<div class="input-box">
			// 								<a class="remove" href="javascript:;"></a>
			// 								<input class="search-field" name="s" type="text" placeholder="Recherche...">
			// 								<input type="button" value="" class="submit">
			// 							</div>
			// 						</div>
			// 					</form>
			//             	</div>
			// 				<ul class="nav navbar-nav pull-right">
			// 					<li class="dropdown user">
			// 						<a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
			// 							<i class="fa fa-user"></i>
			// 							<span class="username">' . $fullname . '</span>
			// 							<i class="fa fa-angle-down"></i>
			// 						</a>
			// 						<ul class="dropdown-menu">
			// 							<li>
			// 								<a href="/extranet/wp-admin/profile.php">
			// 									<i class="fa fa-user"></i> Mon Profil
			// 								</a>
			// 							</li>
			// 							<li>
			// 								<form name="logout" action="/logout.php" method="post" onsubmit="javascript: return submitLogout();">
			// 									<input type="submit" class="submit" value="D&eacute;connexion" />
			// 								</form>
			// 							</li>
			// 						</ul>
			// 					</li>
			// 				</ul>
			// 			</div>
			// 		</div>';


			// } else {
			// 	$html .=
			// 		'<div class="header navbar navbar-fixed-top">
			// 			<div class="header-inner">
			//             	<form name="login" action="/login.php" method="post">
			// 					<!--label for="loginkey">Nom d\'usager&nbsp;:</label-->
			// 					<input type="text" id="loginkey" placeholder="Nom d\'utilisateur" name="loginkey" />
			// 					<!--label for="loginpass">Mot de passe&nbsp;</label-->
			// 					<input type="password" id="loginpass" placeholder="Mot de passe" name="loginpass" />
			// 					<input type="hidden" value="_IGNORE_loginkey" />
			// 					<input type="hidden" value="_IGNORE_loginpass" />
			// 					<input type="submit" class="submit" value="Connexion" />
			// 				</form>
			// 			</div>
			// 		</div>';
			// }





			return $html;
		}

		function livetabString() {
			global $BASEURL;

			$userkey = $_SESSION['user_key'];

			$this->fixEmptySID();

			$html = "";
			if ($userkey != "" && isset($_SESSION['evaluations']) && is_array($_SESSION['evaluations']) && count($_SESSION['evaluations']) > 0) {

				$html .= $this->evalJavaScriptString();

				$html .=
					"<div id=\"livetab\">\n".
					"	<table id=\"evals\">\n".
					"		<tr>\n".
					"			<th class=\"eval_list\">&Eacute;valuations charg&eacute;es&nbsp;:</th>\n";

				$sid = getorpost('sid');
				$eval_params = $_SESSION['evaluations'];
				krsort($eval_params);

				if (is_array($eval_params) && count($eval_params) > 0) {
					$nb = 0;
					foreach ($eval_params as $requestSID => $subArray) {
						$nb++;
						if ($requestSID == "compare") {
							$action = "compare";
						} else {
							$action = $subArray['action'];
						}
						$currentForm = getorpost('currentForm');
						$requestForm = getorpost('requestForm');
						if ($requestForm == "") {
							$requestForm = "1";
						}

						switch ($action) {
							case "modify":
								$eval_date = substr($subArray['s0_date_visite'], 2, 8);
								$etude_nom = $subArray['etudes_nom'];
								if (strlen($etude_nom) > 8) {
									$etude_nom_short = substr($etude_nom, 0, 6) . "...";
								}
								if ($requestSID == $sid) {
									if ($nb == 4) {
										$html .=
											"<td class=\"eval_list eval_current last\">\n".
											"	<span title=\"" . $etude_nom . " (" . $eval_date . ")\">" . $etude_nom_short . " (" . $eval_date . ")</span>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer " . $etude_nom . " (" . $eval_date . ")\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									} else {
										$html .=
											"<td class=\"eval_list eval_current\">\n".
											"	<span title=\"" . $etude_nom . " (" . $eval_date . ")\">" . $etude_nom_short . " (" . $eval_date . ")</span>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer " . $etude_nom . " (" . $eval_date . ")\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									}
								} else {
									if ($nb == 4) {
										$html .=
											"<td class=\"eval_list last\">\n".
											"	<a href=\"javascript: submitEvalForm('" . $sid . "', '" . $requestSID . "', '" . $requestForm . "');\" title=\"Afficher " . $etude_nom . " (" . $eval_date . ")\">" . $etude_nom_short . " (" . $eval_date . ")</a>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer " . $etude_nom . " (" . $eval_date . ")\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									} else {
										$html .=
											"<td class=\"eval_list\">\n".
											"	<a href=\"javascript: submitEvalForm('" . $sid . "', '" . $requestSID . "', '" . $requestForm . "');\" title=\"Afficher " . $etude_nom . " (" . $eval_date . ")\">" . $etude_nom_short . " (" . $eval_date . ")</a>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer " . $etude_nom . " (" . $eval_date . ")\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									}
								}
								break;

							case "read":
								$eval_date = substr($subArray['s0_date_visite'], 2, 8);
								$etude_nom = $subArray['etudes_nom'];
								if (strlen($etude_nom) > 8) {
									$etude_nom_short = substr($etude_nom, 0, 6) . "...";
								}
								if ($requestSID == $sid) {
									if ($nb == 4) {
										$html .=
											"<td class=\"eval_list eval_current last\">\n".
											"	<span title=\"" . $etude_nom . " (" . $eval_date . ")\">" . $etude_nom_short . " (" . $eval_date . ")</span>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer " . $etude_nom . " (" . $eval_date . ")\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									} else {
										$html .=
											"<td class=\"eval_list eval_current\">\n".
											"	<span title=\"" . $etude_nom . " (" . $eval_date . ")\">" . $etude_nom_short . " (" . $eval_date . ")</span>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer " . $etude_nom . " (" . $eval_date . ")\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									}
								} else {
									if ($nb == 4) {
										$html .=
											"<td class=\"eval_list last\">\n".
											"	<a href=\"javascript: submitEvalForm('" . $sid . "', '" . $requestSID . "', '" . $requestForm . "');\" title=\"Afficher " . $etude_nom . " (" . $eval_date . ")\">" . $etude_nom_short . " (" . $eval_date . ")</a>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer " . $etude_nom . " (" . $eval_date . ")\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									} else {
										$html .=
											"<td class=\"eval_list\">\n".
											"	<a href=\"javascript: submitEvalForm('" . $sid . "', '" . $requestSID . "', '" . $requestForm . "');\" title=\"Afficher " . $etude_nom . " (" . $eval_date . ")\">" . $etude_nom_short . " (" . $eval_date . ")</a>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer " . $etude_nom . " (" . $eval_date . ")\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									}
								}
								break;

							case "compare":
								if ($requestSID == $sid) {
									if ($nb == 4) {
										$html .=
											"<td class=\"eval_list eval_current last\">\n".
											"	<span class=\"comparatif\">Comparatif</span>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer le comaratif\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									} else {
										$html .=
											"<td class=\"eval_list eval_current\">\n".
											"	<span class=\"comparatif\">Comparatif</span>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer le comparatif\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									}
								} else {
									if ($nb == 4) {
										$html .=
											"<td class=\"eval_list last\">\n".
											"	<a class=\"comparatif\" href=\"javascript: submitEvalForm('" . $sid . "', '" . $requestSID . "', '" . $requestForm . "');\" title=\"Afficher le comparatif\">Comparatif</a>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer le comparatif\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									} else {
										$html .=
											"<td class=\"eval_list\">\n".
											"	<a class=\"comparatif\" href=\"javascript: submitEvalForm('" . $sid . "', '" . $requestSID . "', '" . $requestForm . "');\" title=\"Afficher le comparatif\">Comparatif</a>&nbsp;&nbsp;[<a href=\"javascript: submitEvalForm('" . $requestSID . "', '" . $requestSID . "', '" . $requestForm . "', '', '', '', '', '1');\" title=\"Fermer le comparatif\">&nbsp;x&nbsp;</a>]\n".
											"</td>\n";
									}
								}
								break;
						}
					}
				}

				if ($nb < 4) {
					for ($i = $nb; $i < 4; $i++) {
						$html .=
							"<td class=\"eval_list\">&nbsp;</td>\n";
					}
				}
				$html .=
					"</tr>\n";

				if ($sid != "") {
					if ($sid == "compare") {
						$action = "compare";
					} else {
						$action = $eval_params[$sid]['action'];
					}

					switch ($action) {
						case "modify":
							$createdOn_date = substr($_SESSION['evaluations'][$sid]['createdOn'], 0, 10);
							$createdOn_time = substr($_SESSION['evaluations'][$sid]['createdOn'], 11);
							$createdBy = $_SESSION['evaluations'][$sid]['createdBy'];
							$updatedOn_date = substr($_SESSION['evaluations'][$sid]['updatedOn'], 0, 10);
							$updatedOn_time = substr($_SESSION['evaluations'][$sid]['updatedOn'], 11);
							$updatedBy = $_SESSION['evaluations'][$sid]['updatedBy'];
							$currentForm = getorpost('requestForm');
							if ($currentForm == "") {
								$currentForm = "1";
							}

							$html .=
								"<tr>\n".
								"	<th class=\"eval_info\">\n".
								"		Modification en cours&nbsp;:\n".
								"	</th>\n".
								"	<td class=\"eval_info\" colspan=\"4\">\n";

							if ($updatedOn_date == "0000-00-00") {
								$html .=
									"<span class=\"eval_info_item\">Aucune modification apport&eacute;e</span>\n";
							} else {
								$html .=
									"<span class=\"eval_info_item\">Modifi&eacute;e le <span class=\"g\">" . $updatedOn_date . "</span> &agrave; <span class=\"g\">" . $updatedOn_time . "</span> par <span class=\"g\">" . $updatedBy . "</span></span>\n";
							}

							$html .=
								"		<span class=\"eval_info_item\">-</span>\n".
								"		<span class=\"eval_info_item\">Cr&eacute;&eacute;e le <span class=\"g\">" . $createdOn_date . "</span> &agrave; <span class=\"g\">" . $createdOn_time . "</span> par <span class=\"g\">" . $createdBy . "</span></span>\n".
								"	</td>\n".
								"</tr>\n".
								"<tr>\n".
								"	<th class=\"eval_actions\">\n".
								"		Gestion de l'&eacute;valuation&nbsp;:\n".
								"	</th>\n".
								"	<td class=\"eval_actions\" colspan=\"4\">\n".
								"		<span class=\"eval_actions_item\">[&nbsp;<a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $requestForm . "', '1');\" title=\"Enregistrer toutes les pages\">Enregistrer les pages 1-22</a>&nbsp;]</span>\n".
								"		<span class=\"eval_actions_item\">[&nbsp;<a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $requestForm . "', '', '1');\" title=\"Enregistrer la page courante\">Enregistrer la page " . $currentForm . "</a>&nbsp;]</span>\n".
								"		<span class=\"eval_actions_item\">[&nbsp;<a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $requestForm . "', '', '', '', '1');\" title=\"Recharger toutes les pages\">Recharger les pages 1-22</a>&nbsp;]</span>\n".
								"		<span class=\"eval_actions_item\">[&nbsp;<a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $requestForm . "', '', '', '1');\" title=\"Recharger la page courante\">Recharger la page " . $currentForm . "</a>&nbsp;]</span>\n".
								"	</td>\n".
								"</tr>\n";

							if ($eval_params[$sid]['feedback'] != "") {
								$html .=
									"<tr>\n".
									"	<th class=\"eval_feedback\">\n".
									"		Messages du serveur&nbsp;:\n".
									"	</th>\n".
									"	<td class=\"eval_feedback\" colspan=\"4\">\n".
									"		" . $eval_params[$sid]['feedback'] . "\n".
									"	</td>\n".
									"</tr>\n";
							}
							break;

						case "read":
							$createdOn_date = substr($_SESSION['evaluations'][$sid]['createdOn'], 0, 10);
							$createdOn_time = substr($_SESSION['evaluations'][$sid]['createdOn'], 11);
							$createdBy = $_SESSION['evaluations'][$sid]['createdBy'];
							$updatedOn_date = substr($_SESSION['evaluations'][$sid]['updatedOn'], 0, 10);
							$updatedOn_time = substr($_SESSION['evaluations'][$sid]['updatedOn'], 11);
							$updatedBy = $_SESSION['evaluations'][$sid]['updatedBy'];
							$currentForm = getorpost('requestForm');
							if ($currentForm == "") {
								$currentForm = "1";
							}

							$html .=
								"<tr>\n".
								"	<th class=\"eval_info\">\n".
								"		Consultation en cours&nbsp;:\n".
								"	</th>\n".
								"	<td class=\"eval_info\" colspan=\"4\">\n";

							if ($updatedOn_date == "0000-00-00") {
								$html .=
									"<span class=\"eval_info_item\">Aucune modification apport&eacute;e</span>\n";
							} else {
								$html .=
									"<span class=\"eval_info_item\">Modifi&eacute;e le <span class=\"g\">" . $updatedOn_date . "</span> &agrave; <span class=\"g\">" . $updatedOn_time . "</span> par <span class=\"g\">" . $updatedBy . "</span></span>\n";
							}

							$html .=
								"		<span class=\"eval_info_item\">-</span>\n".
								"		<span class=\"eval_info_item\">Cr&eacute;&eacute;e le <span class=\"g\">" . $createdOn_date . "</span> &agrave; <span class=\"g\">" . $createdOn_time . "</span> par <span class=\"g\">" . $createdBy . "</span></span>\n".
								"	</td>\n".
								"</tr>\n".
								"<tr>\n".
								"	<th class=\"eval_actions\">\n".
								"		Gestion de l'&eacute;valuation&nbsp;:\n".
								"	</th>\n".
								"	<td class=\"eval_actions\" colspan=\"4\">\n".
								"		<span class=\"eval_actions_item\">[&nbsp;<a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $requestForm . "', '', '', '', '1');\">Recharger les pages 1-22</a>&nbsp;]</span>\n".
								"		<span class=\"eval_actions_item\">[&nbsp;<a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $requestForm . "', '', '', '1');\">Recharger la page " . $currentForm . "</a>&nbsp;]</span>\n".
								"	</td>\n".
								"</tr>\n";

							if ($eval_params[$sid]['feedback'] != "") {
								$html .=
									"<tr>\n".
									"	<th class=\"eval_feedback\">\n".
									"		Messages du serveur&nbsp;:\n".
									"	</th>\n".
									"	<td class=\"eval_feedback\" colspan=\"4\">\n".
									"		" . $eval_params[$sid]['feedback'] . "\n".
									"	</td>\n".
									"</tr>\n";
							}
							break;

						case "compare":
							$currentForm = getorpost('requestForm');
							if ($currentForm == "") {
								$currentForm = "1";
							}

							$html .=
								"<tr>\n".
								"	<th class=\"eval_info\">\n".
								"		Comparaison en cours&nbsp;:\n".
								"	</th>\n".
								"	<td class=\"eval_info\" colspan=\"4\">\n";

							$eval_compare_params = $_SESSION['evaluations']['compare'];
							krsort($eval_compare_params);

							if (is_array($eval_compare_params) && count($eval_compare_params) > 0) {
								$nb = 0;
								foreach ($eval_compare_params as $eval_key => $subArray) {
									$nb++;
									$eval_date = substr($subArray['s0_date_visite'], 2, 8);
									$etude_nom = $subArray['etudes_nom'];
									if (strlen($etude_nom) > 12) {
										$etude_nom_short = substr($etude_nom, 0, 10) . "...";
									} else {
										$etude_nom_short = $etude_nom;
									}
									if ($nb == 4) {
										$html .=
											"<span class=\"eval_compare_" . $nb . "\" title=\"" . $etude_nom . " (" . $eval_date . ")\">" . $etude_nom_short . "&nbsp;(" . $eval_date . ")</span>";
									} else {
										$html .=
											"<span class=\"eval_compare_" . $nb . "\" title=\"" . $etude_nom . " (" . $eval_date . ")\">" . $etude_nom_short . "&nbsp;(" . $eval_date . ")</span> &nbsp;&nbsp;&nbsp;&nbsp;";
									}
								}
							} else {
								$html .= "&nbsp";
							}

							$html .=
								"	</td>\n".
								"</tr>\n".
								"<tr>\n".
								"	<th class=\"eval_actions\">\n".
								"		Gestion de l'&eacute;valuation&nbsp;:\n".
								"	</th>\n".
								"	<td class=\"eval_actions\" colspan=\"4\">\n".
								"		<span class=\"eval_actions_item\">[&nbsp;<a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $requestForm . "', '', '', '', '1');\">Recharger les pages 1-22</a>&nbsp;]</span>\n".
								"		<span class=\"eval_actions_item\">[&nbsp;<a href=\"javascript: submitEvalForm('" . $sid . "', '', '" . $requestForm . "', '', '', '1');\">Recharger la page " . $currentForm . "</a>&nbsp;]</span>\n".
								"	</td>\n".
								"</tr>\n";

							if ($eval_params[$sid]['feedback'] != "") {
								$html .=
									"<tr>\n".
									"	<th class=\"eval_feedback\">\n".
									"		Messages du serveur&nbsp;:\n".
									"	</th>\n".
									"	<td class=\"eval_feedback\" colspan=\"4\">\n".
									"		" . $eval_params[$sid]['feedback'] . "\n".
									"	</td>\n".
									"</tr>\n";
							}
							break;
					}
				}

				$html .=
					"	</table>\n".
					"</div>\n";
			}
			return $html;
		}

		function evalJavaScriptString() {
			global $BASEURL, $FULLURL;

			$html =
				"<script type=\"text/javascript\" language=\"javascript\">\n".
				"	function submitEvalForm(sid, requestSID, requestForm, saveAll, save, reload, reloadAll, close, begin, requestURL) {\n".
				"		var eval_title = new Array();\n".
				"		var eval_action = new Array();\n".
				"		var eval_lastupdate = new Array();\n";

			if (is_array($_SESSION['evaluations']) && count($_SESSION['evaluations']) > 0) {
				foreach ($_SESSION['evaluations'] as $sid_n => $detail) {
					if ($sid_n == "compare") {
						$html .=
							"eval_title['compare'] = 'Comparatif';\n".
							"eval_action['compare'] = 'compare';\n".
							"eval_lastupdate['compare'] = '';\n";
					} else {
						$html .=
							"eval_title['" . $sid_n . "'] = '" . addslashes($detail['etudes_nom'] . " (" . $detail['s0_date_visite'] . ")") . "';\n".
							"eval_action['" . $sid_n . "'] = '" . $detail['action'] . "';\n".
							"eval_lastupdate['" . $sid_n . "'] = '" . addslashes(substr($detail['updatedOn'], 0, 10) . ' ' . substr($detail['updatedOn'], 11)) . "';\n";
					}
				}
			}

			$html .=
				"		if (sid == null) { sid = ''; }\n".
				"		if (requestSID == null) { requestSID = ''; }\n".
				"		if (saveAll == null) { saveAll = ''; }\n".
				"		if (save == null) { save = ''; }\n".
				"		if (reload == null) { reload = ''; }\n".
				"		if (reloadAll == null) { reloadAll = ''; }\n".
				"		if (close == null) { close = ''; }\n".
				"		if (begin == null) { begin = ''; }\n".
				"		if (requestURL == null) { requestURL = ''; }\n".
				"		if (document.getElementById('_eval_sid')) {\n".
				"			if (requestSID != '') { document.getElementById('_eval_requestSID').value = requestSID; }\n".
				"			if (requestForm != '') { document.getElementById('_eval_requestForm').value = requestForm; }\n".
				"			if (saveAll != '') { document.getElementById('_eval_saveAll').value = saveAll; }\n".
				"			if (save != '') { document.getElementById('_eval_save').value = save; }\n".
				"			if (reload != '') { document.getElementById('_eval_reload').value = reload; }\n".
				"			if (reloadAll != '') { document.getElementById('_eval_reloadAll').value = reloadAll; }\n".
				"			if (requestURL != '') { document.getElementById('_eval_requestURL').value = requestURL; }\n".
				"			if (close != '') {\n".
				"				if (eval_action[requestSID] == 'modify') {\n".
				"					if (confirm('Êtes-vous certain de vouloir fermer l\'évaluation :\\n' + stripslashes(eval_title[requestSID]) + ' ?\\n\\nDate et heure de la dernière sauvegarde : ' + stripslashes(eval_lastupdate[requestSID]))) {;\n".
				"						document.getElementById('_eval_close').value = close;\n".
				"					} else {\n".
				"				 		return;\n".
				"					}\n".
				"				} else {\n".
				"					document.getElementById('_eval_close').value = close;\n".
				"				}\n".
				"			}\n".
				"			document.evaluation.submit();\n".
				"		} else {\n".
				"			if (close != '') {\n".
				"				if (eval_action[requestSID] == 'modify') {\n".
				"					if (confirm('Êtes-vous certain de vouloir fermer l\'évaluation :\\n' + stripslashes(eval_title[requestSID]) + ' ?\\n\\nDate et heure de la dernière sauvegarde : ' + stripslashes(eval_lastupdate[requestSID]))) {;\n".
				"						close = 1;\n".
				"					} else {\n".
				"				 		return;\n".
				"					}\n".
				"				}\n".
				"			}\n".
				"			window.location.href = '" . $FULLURL . "evaluations/evaluations.php?sid=' + sid + '&requestSID=' + requestSID + '&requestForm=' + requestForm + '&saveAll=' + saveAll + '&save=' + save + '&reloadAll=' + reloadAll + '&reload=' + reload + '&close=' + close + '&begin=' + begin + '&requestURL=' + requestURL;\n".
				"		}\n".
				"	}\n".
				"</script>\n";

			return $html;
		}

		function fixEmptySID() {

			if (isset($_SESSION['evaluations'])) {
				if (is_array($_SESSION['evaluations'][NULL])) {
					unset($_SESSION['evaluations'][NULL]);
				}
				if (count($_SESSION['evaluations']) == 0) {
					unset($_SESSION['evaluations']);
				}
			}
		}

		function fetch($template){
			return($this->smarty->fetch($template));
		}

	}

