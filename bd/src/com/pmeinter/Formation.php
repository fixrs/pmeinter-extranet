<?PHP

	import("com.quiboweb.sql.SingleRowHandler");

    class Formation extends SingleRowHandler {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Formation($DB = "", $fieldsArray = "", $quickmode = 0) { 

			$this->setTable("formations");
        	$this->setInsertLogInfo("Ajout");
        	$this->setUpdateLogInfo("Modification");
        	$this->setDeleteLogInfo("Suppression");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}		
			if ($quickmode) {
				$this->quickmode();
			}
        }


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------

		
    }

?>