<?PHP

	import("com.quiboweb.sql.SingleRowHandler");

    class Journee_Formation_event extends SingleRowHandler {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Journee_Formation_event($DB = "", $fieldsArray = "", $quickmode = 0) { 

			$this->setTable("journee_formation_event");
        	$this->setInsertLogInfo("Ajout");
        	$this->setUpdateLogInfo("Modification");
        	$this->setDeleteLogInfo("Suppression");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}		
			if ($quickmode) {
				$this->quickmode();
			}
			
        }


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------

		
    }

?>