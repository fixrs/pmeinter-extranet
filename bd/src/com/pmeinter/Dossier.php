<?PHP

    import("com.quiboweb.sql.SingleRowHandler");

    class Dossier extends SingleRowHandler {

        // ---------
        // VARIABLES
        // ---------


        // -----------
        // CONSTUCTEUR
        // -----------
        function Dossier($DB = "", $fieldsArray = "", $quickmode = 0) {
            $this->setTable("dossiers");
            $this->setInsertLogInfo("Ajout");
            $this->setUpdateLogInfo("Modification");
            $this->setDeleteLogInfo("Suppression");

            if ($DB != "") {
                $this->setDB($DB);
            }
            if ($fieldsArray != "") {
                $this->setFieldsArray($fieldsArray);
            }
            if ($quickmode) {
                $this->quickmode();
            }
        }


        // ------------------
        // FONCTIONS ÉTENDUES
        // ------------------


    }

?>
