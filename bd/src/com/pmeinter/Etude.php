<?PHP

	import("com.quiboweb.sql.SingleRowHandler");

    class Etude extends SingleRowHandler {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Etude($DB = "", $fieldsArray = "", $quickmode = 0) { 
			
			$this->setTable("etudes");
        	$this->setInsertLogInfo("Ajout");
        	$this->setUpdateLogInfo("Modification");
        	$this->setDeleteLogInfo("Suppression");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}		
			if ($quickmode) {
				$this->quickmode();
			}
   		}


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------
		
    }

?>