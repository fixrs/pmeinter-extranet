<?PHP

	import("com.quiboweb.sql.SingleRowHandler");

    class Congres_event extends SingleRowHandler {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Congres_event($DB = "", $fieldsArray = "", $quickmode = 0) { 

			$this->setTable("congres_event");
        	$this->setInsertLogInfo("Ajout");
        	$this->setUpdateLogInfo("Modification");
        	$this->setDeleteLogInfo("Suppression");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}		
			if ($quickmode) {
				$this->quickmode();
			}
			
        }


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------

		
    }

?>