<?PHP

	import("com.quiboweb.sql.BasicDBIOCacher");

    class User extends BasicDBIOCacher {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Nouvelle($DB = "", $fieldsArray = "", $quickmode = 0) { 

        	if ($DB != "") {
        		$this->setDB($DB);
        	}
        	if ($fieldsArray != "") {
        		$this->setFieldsArray($fieldsArray);
        	}
        	if ($quickmode) {
        		$this->quickmode();
        	}
	
			$this->setTable("nouvelles");
        }


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------


    }

?>