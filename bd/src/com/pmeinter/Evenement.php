<?PHP

	import("com.quiboweb.sql.SingleRowHandler");

    class Evenement extends SingleRowHandler {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Evenement($DB = "", $fieldsArray = "", $quickmode = 0) { 

			$this->setTable("evenements");
        	$this->setInsertLogInfo("Ajout");
        	$this->setUpdateLogInfo("Modification");
        	$this->setDeleteLogInfo("Suppression");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}		
			if ($quickmode) {
				$this->quickmode();
			}
			
        }


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------

		
    }

?>