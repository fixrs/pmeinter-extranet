<?PHP

	import("com.quiboweb.sql.SingleRowHandler");

    class Emploi extends SingleRowHandler {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Succursale($DB = "", $fieldsArray = "", $quickmode = 0) { 
			$this->setTable("etudes_emplois");
        	$this->setInsertLogInfo("Ajout");
        	$this->setUpdateLogInfo("Modification");
        	$this->setDeleteLogInfo("Suppression");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}		
			if ($quickmode) {
				$this->quickmode();
			}
   		}


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------

		
    }

?>