<?PHP

	import("com.quiboweb.sql.SingleRowHandler");

    class Produit extends SingleRowHandler {
		
		// ---------
		// VARIABLES
		// ---------


		// -----------
		// CONSTUCTEUR
		// -----------
		function Produit($DB = "", $fieldsArray = "", $quickmode = 0) { 

			$this->setTable("produits");
        	$this->setInsertLogInfo("Ajout");
        	$this->setUpdateLogInfo("Modification");
        	$this->setDeleteLogInfo("Suppression");

			if ($DB != "") {
				$this->setDB($DB);
			}
			if ($fieldsArray != "") {
				$this->setFieldsArray($fieldsArray);
			}		
			if ($quickmode) {
				$this->quickmode();
			}
        }


		// ------------------
		// FONCTIONS ÉTENDUES
		// ------------------



    }

?>