<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty string_format modifier plugin
 *
 * Type:     modifier<br>
 * Name:     string_format<br>
 * Purpose:  format strings via sprintf
 * @link http://smarty.php.net/manual/en/language.modifier.string.format.php
 *          string_format (Smarty online manual)
 * @author   Monte Ohrt <monte at ohrt dot com>
 * @param string
 * @param string
 * @return string
 */
function smarty_modifier_string_format($string, $format, $number=false)
{
	if($number!=false){
		if(substr($string,count($string)-2,1)==='%') {return($string);}
		if($format=='full')
			return(number_format($string,2,","," "));
		if($format=='simple')
			return(number_format($string,0,","," "));
	}
    return sprintf($format, $string);
}

/* vim: set expandtab: */

?>