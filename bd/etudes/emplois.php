<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("emplois.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 /*|| $_SESSION['user_type'] == 3*/ || $_SESSION['user_type'] == 4)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("emplois.tpl");
		exit;
	}

	import("com.quiboweb.form.Form");
	import("com.pmeinter.Emploi");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$formString = "";
	$successString = "";
	$errorString = "";

	$begin = getorpost('begin');
	$action = getorpost('action');
	$key = getorpost('key');

	switch ($action) {
		case "add" :
			if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2 /*&& $_SESSION['user_type'] != 3*/  && $_SESSION['user_type'] != 4) {
				exit;
				break;
			}

			if ($begin) {
				importForm("emplois");
				$FormObject = new Form("", "", getForm("add"));
				$formString = $FormObject->returnForm();

			} else {
				$errorString = validateFormValues("add", getParametersFromPOST());
				if ($errorString == "") {
					$errorString = updateDBValues("add", getParametersFromPOST());
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "etudes/index.php\">Retourner au menu pr&eacute;c&eacute;dent.</a>";
						break;
					}

				} else {
					importForm("emplois");
					$FormObject = new Form("", getParametersFromPOST(), getForm("add"));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "modify" :
			if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2 /*&& $_SESSION['user_type'] != 3*/  && $_SESSION['user_type'] != 4) {
				exit;
				break;
			}

			if ($begin) {
				importForm("emplois");
				$FormObject = new Form("", getParametersFromDB("modify", $key), getForm("modify"));
				$FormObject->setAllHidden();
				$formString = $FormObject->returnFilledForm();

			} else {
				$errorString = validateFormValues("modify", getParametersFromPOST());
				if ($errorString == "") {
					$errorString = updateDBValues("modify", getParametersFromPOST());
					if ($errorString == "") {
						$successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "etudes/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
						break;
					}

				} else {
					importForm("emplois");
					$FormObject = new Form("", getParametersFromPOST(), getForm("modify"));
					$FormObject->setAllHidden();
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "read" :
			importForm("emplois");
			$FormObject = new Form("", getParametersFromDB("read", $key), getForm("read"));
			$FormObject->setAllHidden();
			$formString = $FormObject->returnFilledForm();
			break;

	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
	$Skin->assign("page", "emplois");
	$Skin->assign("title", pageTitleString($action, $key));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("form_title", formTitleString($action, $key));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->assign("index", $indexString);
	$Skin->display("emplois.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function pageTitleString($action, $key) {
		switch ($action) {
			case "add":
				$title = "Ajout d'une offre d'emploi";
				break;
			case "modify":
				$title = "Modification d'une offre d'emploi";
				break;
			case "read":
				$title = "Consultation d'une offre d'emploi";
				break;
			default:
				$title = "Gestion des offres d'emploi";
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/jquery-1.3.2.min.js\" type=\"text/javascript\"></script>\n".
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/hta/hta.js\" type=\"text/javascript\"></script>\n".
					"<script type=\"text/javascript\" src=\"" . $JS_URL . "/jHtmlArea/scripts/jHtmlArea-0.8.min.js\"></script>\n".
    				"<link rel=\"Stylesheet\" type=\"text/css\" href=\"" . $JS_URL . "/jHtmlArea/style/jHtmlArea.css\" />\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/emplois.js\" type=\"text/javascript\"></script>\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>\n";
				break;
			case "modify":
				$html .=
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/jquery-1.3.2.min.js\" type=\"text/javascript\"></script>\n".
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/hta/hta.js\" type=\"text/javascript\"></script>\n".
					"<script type=\"text/javascript\" src=\"" . $JS_URL . "/jHtmlArea/scripts/jHtmlArea-0.8.min.js\"></script>\n".
    				"<link rel=\"Stylesheet\" type=\"text/css\" href=\"" . $JS_URL . "/jHtmlArea/style/jHtmlArea.css\" />\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/emplois.js\" type=\"text/javascript\"></script>\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>\n";
				break;
			case "read":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/emplois.js\" type=\"text/javascript\"></script>\n";
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "add":
//				$html .= "onload=\"javascript: initShowChrCount();\"";
				break;
			case "modify":
//				$html .= "onload=\"javascript: initShowChrCount();\"";
				break;
			case "read":
				break;
		}
		return $html;
	}

	function formTitleString($action, $key) {
		switch ($action) {
			case "add":
				$title = "<h1>Ajout d'une offre d'emploi</h1>";
				break;
			case "modify":
				$title = "<h1>Modification d'une offre d'emploi</h1>";
				break;
			case "read":
				$title = "<h1>Consultation d'une offre d'emploi</h1>";
				break;
		}
		return $title;
	}

	function validateFormValues($action, $parameters) {
		global $DB;
		$errorString = "";

		switch ($action) {
			case "add":
				if ($parameters['etudes_key'] == "-1" ) {
					$errorString .= _error("<a href=\"javascript: showErrorField('etudes_key');\">Veuillez indiquer l'&eacute;tude.</a>");
				}
				if ($parameters['afficher']) {
					if (strlen($parameters['poste']) < 4 || strlen($parameters['exigences']) < 4 || strlen($parameters['remuneration']) < 4 || strlen($parameters['contact']) < 4 || strlen($parameters['date_entree']) < 4 || strlen($parameters['date_affichage']) != 10) {
						$errorString .= _error("Veuillez remplir tous les champs si vous d&eacute;sirez afficher cette offre.");
					}
				}
				break;

			case "modify":
				if ($parameters['afficher']) {
					if (strlen($parameters['poste']) < 4 || strlen($parameters['exigences']) < 4 || strlen($parameters['remuneration']) < 4 || strlen($parameters['contact']) < 4 || strlen($parameters['date_entree']) < 4 || strlen($parameters['date_affichage']) != 10) {
						$errorString .= _error("Veuillez remplir tous les champs si vous d&eacute;sirez afficher cette offre.");
					}
				}
				break;
		}
		return $errorString;
	}

	function getParametersFromPOST() {
		$parameters = array();
		if (count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$parameters[$field] = $value;
				} else {
					$parameters[$field] = trim($value);
				}
			}
		}
		return $parameters;
	}

	function getParametersFromDB($action, $key) {
		global $DB, $BASEURL;
		$parameters = array();
		switch ($action) {
			case "modify":
/*				$Emploi = new Emploi($DB, "", "");
				$Emploi->setQKey("key", $key);
				$Emploi->scanfields();
				$Emploi->loadAll();
				$parameters = $Emploi->getAll();
*/
				$DB->query(
					"SELECT * ".
					"FROM `etudes_emplois` ".
					"WHERE `key` = '" . addslashes($key) . "';"
				);
				while ($DB->next_record()) {
					$parameters['etudes_key'] = $DB->getField("etudes_key");
					$parameters['poste'] = $DB->getField("poste");
					$parameters['date_entree'] = $DB->getField("date_entree");
					$parameters['date_affichage'] = $DB->getField("date_affichage");
					$parameters['exigences'] = $DB->getField("exigences");
					$parameters['remuneration'] = $DB->getField("remuneration");
					$parameters['contact'] = $DB->getField("contact");
					$parameters['afficher'] = $DB->getField("afficher");
				}
				$DB->query(
					"SELECT `key`, `nom` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $parameters['etudes_key'] . "' AND `actif` = '1';"
				);
				while ($DB->next_record()) {
					$parameters['etude_nom'] = $DB->getField("nom");
				}
				break;

			case "read":
/*				$Emploi = new Emploi($DB, "", "");
				$Emploi->setQKey("key", $key);
				$Emploi->scanfields();
				$Emploi->loadAll();
				$parameters = $Emploi->getAll();
*/
				$DB->query(
					"SELECT * ".
					"FROM `etudes_emplois` ".
					"WHERE `key` = '" . addslashes($key) . "';"
				);
				while ($DB->next_record()) {
					$parameters['etudes_key'] = $DB->getField("etudes_key");
					$parameters['poste'] = $DB->getField("poste");
					$parameters['date_entree'] = $DB->getField("date_entree");
					$parameters['date_affichage'] = $DB->getField("date_affichage");
					$parameters['exigences'] = $DB->getField("exigences");
					$parameters['remuneration'] = $DB->getField("remuneration");
					$parameters['contact'] = $DB->getField("contact");
					$parameters['afficher'] = $DB->getField("afficher");
				}
				$DB->query(
					"SELECT `key`, `nom`, `courriel` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $parameters['etudes_key'] . "' AND `actif` = '1';"
				);
				while ($DB->next_record()) {
					$parameters['etude_nom'] = $DB->getField("nom");
				}
				break;
		}
		return $parameters;
	}

	function updateDBValues($action, $parameters) {
		global $DB;
		$errorString = "";

		$parameters = addslashesToValues($parameters);

		switch ($action) {
			case "add":
/*				$Emploi = new Emploi($DB, "", 0);
				$Emploi->scanfields();
				$Emploi->insertmode();
				$Emploi->setAll($parameters);
				$Emploi->excludefield("action");
				if (!$Emploi->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
				}
*/
				$insert = $DB->query(
					"INSERT INTO `etudes_emplois` (".
					"`etudes_key`, ".
					"`poste`, ".
					"`date_entree`, ".
					"`date_affichage`, ".
					"`exigences`, ".
					"`remuneration`, ".
					"`contact`, ".
					"`afficher` ".
					") VALUES (".
					"'" . $parameters['etudes_key'] . "', ".
					"'" . $parameters['poste'] . "', ".
					"'" . $parameters['date_entree'] . "', ".
					"'" . $parameters['date_affichage'] . "', ".
					"'" . $parameters['exigences'] . "', ".
					"'" . $parameters['remuneration'] . "', ".
					"'" . $parameters['contact'] . "', ".
					"'" . $parameters['afficher'] . "' ".
					");"
				);
				if (!$insert) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
				}
				break;

			case "modify":
/*				$Emploi = new Emploi($DB, "", 0);
				$Emploi->setQKey("key", $parameters['key']);
				$Emploi->scanfields();
				$Emploi->setAll($parameters);
				$Emploi->excludefield("action");
				if (!$Emploi->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
				}
*/
				$update = $DB->query(
					"UPDATE `etudes_emplois` SET ".
					"`etudes_key`='" . $parameters['etudes_key'] . "', ".
					"`poste`='" . $parameters['poste'] . "', ".
					"`date_entree`='" . $parameters['date_entree'] . "', ".
					"`date_affichage`='" . $parameters['date_affichage'] . "', ".
					"`exigences`='" . $parameters['exigences'] . "', ".
					"`remuneration`='" . $parameters['remuneration'] . "', ".
					"`contact`='" . $parameters['contact'] . "',".
					"`afficher`='" . $parameters['afficher'] . "' ".
					"WHERE `key`='" . $parameters['key'] . "';"
				);
				if (!$update) {
					$errorString .= _error("Une erreur s'est produite lors de la mise &agrave; jour des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
				}
				break;
		}
		return $errorString;
	}

	function addslashesToValues($parameters = "") {
		if (is_array($parameters) && count($parameters) > 0) {
			foreach ($parameters as $field => $value) {
				if (is_array($value) && count($value) > 0) {
					foreach ($value as $field2 => $value2) {
						if (is_array($value2) && count($value2) > 0) {
							foreach ($value2 as $field3 => $value3) {
								$parameters[$field][$field2][$field3] = addslashes($value3);
							}
						} else {
							$parameters[$field][$field2] = addslashes($value2);
						}
					}
				} else {
					$parameters[$field] = addslashes($value);
				}
			}
		} else {
			$parameters = addslashes($parameters);
		}
		return $parameters;
	}

?>
