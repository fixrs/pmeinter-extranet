<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("etudes.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("etudes.tpl");
		exit;
	}

	import("com.quiboweb.form.Form");
	import("com.pmeinter.Etude");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$formString = "";
	$successString = "";
	$errorString = "";

	$begin = getorpost('begin');
	$action = getorpost('action');
	$key = getorpost('key');

	switch ($action) {
		case "add" :
			if ($_SESSION['user_type'] != 1) {
				exit;
				break;
			}

			if ($begin) {
				importForm("etudes");
				$FormObject = new Form("", "", getForm("add"));
				$formString = $FormObject->returnForm();

			} else {
				$errorString = validateFormValues("add", getParametersFromPOST());
				if ($errorString == "") {
					$errorString = updateDBValues("add", getParametersFromPOST());
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "etudes/index.php\">Retourner au menu pr&eacute;c&eacute;dent.</a>";
						break;
					}

				} else {
					importForm("etudes");
					$FormObject = new Form("", getParametersFromPOST(), getForm("add"));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "modify" :
			if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
				exit;
				break;
			}

			if ($begin) {
				importForm("etudes");
				$FormObject = new Form("", getParametersFromDB("modify", $key), getForm("modify"));
				$FormObject->setAllHidden();
				$formString = $FormObject->returnFilledForm();

			} else {
				$errorString = validateFormValues("modify", getParametersFromPOST());
				if ($errorString == "") {
					$errorString = updateDBValues("modify", getParametersFromPOST());
					if ($errorString == "") {
						$successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "etudes/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
						break;
					}

				} else {
					importForm("etudes");
					$FormObject = new Form("", getParametersFromPOST(), getForm("modify"));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "read" :
			importForm("etudes");
			$FormObject = new Form("", getParametersFromDB("read", $key), getForm("read"));
			$FormObject->setAllHidden();
			$formString = $FormObject->returnFilledForm();
			break;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
	$Skin->assign("page", "etudes");
	$Skin->assign("title", pageTitleString($action, $key));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("form_title", formTitleString($action, $key));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->assign("index", $indexString);
	$Skin->display("etudes.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function pageTitleString($action, $key) {
		switch ($action) {
			case "add":
				$title = "Ajout d'une &eacute;tude";
				break;
			case "modify":
				$title = "Modification d'une &eacute;tude";
				break;
			case "read":
				$title = "Consultation d'une &eacute;tude";
				break;
			default:
				$title = "Gestion des &eacute;tudes";
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/jquery-1.3.2.min.js\" type=\"text/javascript\"></script>\n".
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/hta/hta.js\" type=\"text/javascript\"></script>\n".
					"<script type=\"text/javascript\" src=\"" . $JS_URL . "/jHtmlArea/scripts/jHtmlArea-0.8.min.js\"></script>\n".
    				"<link rel=\"Stylesheet\" type=\"text/css\" href=\"" . $JS_URL . "/jHtmlArea/style/jHtmlArea.css\" />\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>\n";
				break;

			case "modify":
				$html .=
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/jquery-1.3.2.min.js\" type=\"text/javascript\"></script>\n".
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/hta/hta.js\" type=\"text/javascript\"></script>\n".
					"<script type=\"text/javascript\" src=\"" . $JS_URL . "/jHtmlArea/scripts/jHtmlArea-0.8.min.js\"></script>\n".
    				"<link rel=\"Stylesheet\" type=\"text/css\" href=\"" . $JS_URL . "/jHtmlArea/style/jHtmlArea.css\" />\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>\n";
				break;

			case "read":
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "add":
				break;

			case "modify":
				break;

			case "read":
				break;
		}
		return $html;
	}

	function formTitleString($action, $key) {
		switch ($action) {
			case "add":
				$title = "<h1>Ajout d'une &eacute;tude</h1>";
				break;
			case "modify":
				$title = "<h1>Modification d'une &eacute;tude</h1>";
				break;
			case "read":
				$title = "<h1>Consultation d'une &eacute;tude</h1>";
				break;
		}
		return $title;
	}

	function validateFormValues($action, $parameters) {
		global $DB, $EMAIL_FORMAT_PATTERN, $HTTP_FORMAT_PATTERN, $ETUDES_IMG_MAX_SIZE;
		$errorString = "";
		switch ($action) {
			case "add":
				if ($parameters['nom'] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('nom');\">Veuillez indiquer le nom de l'&eacute;tude.</a>");
				}

				if ($parameters['courriel'] != "" && !preg_match($EMAIL_FORMAT_PATTERN, $parameters['courriel'])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('courriel');\">L'adresse courriel fournie n'est pas valide.</a>");
				}

				if ($parameters['url_site_web'] != "" && !preg_match($HTTP_FORMAT_PATTERN, $parameters['url_site_web'])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('url_site_web');\">L'adresse du site web fournie n'est pas valide.</a>");
				}

				if ($parameters['description'] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('description');\">Veuillez fournir une description &agrave; l'&eacute;tude.</a>");

				} elseif (strlen(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), html_entity_decode(strip_tags($parameters['description'])))) > 1800) {
					$errorString .= _error("<a href=\"javascript: showErrorField('description');\">La description de l'&eacute;tude est trop longue. (1750 caract&egrave;res maximum)</a>");
				}

				if ($_FILES['image_up']['name'] != "") {
					$file_error = $_FILES['image_up']['error'];
					$file_type = $_FILES['image_up']['type'];
					$file_size = $_FILES['image_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">Une erreur s'est produite avec la photo de l'&eacute;quipe.</a>");
					}
					if (!preg_match("/(gif|png|jpg|jpeg)/", $file_type)) {
						$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">Le format de la photo de l'&eacute;quipe n'est pas valide.</a>");
					} elseif ($file_size == 0 || $file_size > $ETUDES_IMG_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">La grosseur de la photo de l'&eacute;quipe est trop grande (maximum&nbsp;: " . format_size($ETUDES_IMG_MAX_SIZE) . ").</a>");
					}
				}

				if ($_FILES['banner_up']['name'] != "") {
					$file_error = $_FILES['banner_up']['error'];
					$file_type = $_FILES['banner_up']['type'];
					$file_size = $_FILES['banner_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('banner_up');\">Une erreur s'est produite avec la photo banni&egrave;re.</a>");
					}
					if (!preg_match("/(png|jpg|jpeg)/", $file_type)) {
						$errorString .= _error("<a href=\"javascript: showErrorField('banner_up');\">Le format de la photo banni&egrave;re n'est pas valide.</a>");
					} elseif ($file_size == 0 || $file_size > $ETUDES_IMG_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('banner_up');\">La grosseur de la photo banni&egrave;re est trop grande (maximum&nbsp;: " . format_size($ETUDES_IMG_MAX_SIZE) . ").</a>");
					}
				}
				break;

			case "modify":
				if ($parameters['newnom'] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('newnom');\">Veuillez indiquer le nom de l'&eacute;tude.</a>");
				}

				if ($parameters['courriel'] != "" && !preg_match($EMAIL_FORMAT_PATTERN, $parameters['courriel'])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('courriel');\">L'adresse courriel fournie n'est pas valide.</a>");
				}

				if ($parameters['url_site_web'] != "" && !preg_match($HTTP_FORMAT_PATTERN, $parameters['url_site_web'])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('url_site_web');\">L'adresse du site web fournie n'est pas valide.</a>");
				}

				if ($parameters['description'] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('description');\">Veuillez fournir une description &agrave; l'&eacute;tude.</a>");

				} elseif (strlen(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), html_entity_decode(strip_tags($parameters['description'])))) > 1800) {
					$errorString .= _error("<a href=\"javascript: showErrorField('description');\">La description de l'&eacute;tude est trop longue. (1750 caract&egrave;res maximum)</a>");
				}

				if ($_FILES['image_up']['name'] != "") {
					$file_error = $_FILES['image_up']['error'];
					$file_type = $_FILES['image_up']['type'];
					$file_size = $_FILES['image_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">Une erreur s'est produite avec la photo de l'&eacute;quipe.</a>");
					}
					if (!preg_match("/(gif|png|jpg|jpeg)/", $file_type)) {
						$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">Le format de la photo de l'&eacute;quipe n'est pas valide.</a>");
					} elseif ($file_size == 0 || $file_size > $ETUDES_IMG_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">La grosseur de la photo de l'&eacute;quipe est trop grande (maximum&nbsp;: " . format_size($ETUDES_IMG_MAX_SIZE) . ").</a>");
					}
				}

				if ($_FILES['banner_up']['name'] != "") {
					$file_error = $_FILES['banner_up']['error'];
					$file_type = $_FILES['banner_up']['type'];
					$file_size = $_FILES['banner_up']['size'];
					if ($file_error) {
						$errorString .= _error("<a href=\"javascript: showErrorField('banner_up');\">Une erreur s'est produite avec la photo banni&egrave;re.</a>");
					}
					if (!preg_match("/(png|jpg|jpeg)/", $file_type)) {
						$errorString .= _error("<a href=\"javascript: showErrorField('banner_up');\">Le format de la photo banni&egrave;re n'est pas valide.</a>");
					} elseif ($file_size == 0 || $file_size > $ETUDES_IMG_MAX_SIZE) {
						$errorString .= _error("<a href=\"javascript: showErrorField('banner_up');\">La grosseur de la photo banni&egrave;re est trop grande (maximum&nbsp;: " . format_size($ETUDES_IMG_MAX_SIZE) . ").</a>");
					}
				}
				break;
		}
		return $errorString;
	}

	function getParametersFromPOST() {
		$parameters = array();
		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$parameters[$field] = $value;
				} else {
					$parameters[$field] = trim($value);
				}
			}
		}
		return $parameters;
	}

	function getParametersFromDB($action, $key) {
		global $DB, $BASEURL;
		$parameters = array();
		switch ($action) {
			case "modify":
				if ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != $key) {
					$parameters = array();
				} else {
					$Etude = new Etude($DB, "", "");
					$Etude->setQKey("key", $key);
					$Etude->scanfields();
					$Etude->loadAll();
					$parameters = $Etude->getAll();
					$parameters['newnom'] = $parameters['nom'];
				}

				break;

			case "read":
				$Etude = new Etude($DB, "", "");
				$Etude->setQKey("key", $key);
				$Etude->scanfields();
				$Etude->loadAll();
				$parameters = $Etude->getAll();
				$parameters['courriel'] = "<a href=\"mailto:" . $parameters['courriel'] . "\" title=\"Envoyer un courriel\">" . $parameters['courriel'] . "</a>";
				$parameters['url_site_web'] = "<a href=\"" . $parameters['url_site_web'] . "\" title=\"Visiter le site\">" . $parameters['url_site_web'] . "</a>";

				break;
		}
		return $parameters;
	}

	function updateDBValues($action, $parameters) {
		global $DB, $BASEURL, $BASEPATH;
		$errorString = "";

		$parameters = addslashesToValues($parameters);

		switch ($action) {
			case "add":
				$Etude = new Etude($DB, "", 0);
				$Etude->scanfields();
				$Etude->insertmode();
				$Etude->set("nom", $parameters['nom']);
				$Etude->set("nombre_employes", $parameters['nombre_employes']);
				$Etude->set("nombre_notaires_associes", $parameters['nombre_notaires_associes']);
				$Etude->set("nombre_notaires_salaries", $parameters['nombre_notaires_salaries']);
				$Etude->set("courriel", $parameters['courriel']);
				$Etude->set("url_site_web", $parameters['url_site_web']);
				$Etude->set("description", $parameters['description']);

				if (!isset($parameters['droit_affaires_oui'])) { $parameters['droit_affaires_oui'] = 0; }
				if (!isset($parameters['droit_personne_oui'])) { $parameters['droit_personne_oui'] = 0; }
				if (!isset($parameters['droit_agricole_oui'])) { $parameters['droit_agricole_oui'] = 0; }
				if (!isset($parameters['droit_immobilier_oui'])) { $parameters['droit_immobilier_oui'] = 0; }

				$Etude->set("intro_services", $parameters['intro_services']);

				$Etude->set("droit_affaires_oui", $parameters['droit_affaires_oui']);
				$Etude->set("droit_affaires_desc", $parameters['droit_affaires_desc']);

				$Etude->set("droit_personne_oui", $parameters['droit_personne_oui']);
				$Etude->set("droit_personne_desc", $parameters['droit_personne_desc']);

				$Etude->set("droit_agricole_oui", $parameters['droit_agricole_oui']);
				$Etude->set("droit_agricole_desc", $parameters['droit_agricole_desc']);

				$Etude->set("droit_immobilier_oui", $parameters['droit_immobilier_oui']);
				$Etude->set("droit_immobilier_desc", $parameters['droit_immobilier_desc']);

				$Etude->set("new_service_title", $parameters['new_service_title']);
				$Etude->set("new_service_description", $parameters['new_service_description']);

				$Etude->set("meta_description", $parameters['meta_description']);
				$Etude->set("heures_ouverture", $parameters['heures_ouverture']);
				$Etude->set("mode_paiement", $parameters['mode_paiement']);
				$Etude->set("facebook", $parameters['facebook']);
				$Etude->set("twitter", $parameters['twitter']);
				$Etude->set("linkedin", $parameters['linkedin']);

				//$Etude->set("image_description", $parameters['image_description']);
				$Etude->set("image_description_1", $parameters['image_description_1']);
				$Etude->set("image_description_2", $parameters['image_description_2']);
				$Etude->set("image_description_3", $parameters['image_description_3']);
				$Etude->set("actif", "1");
				$Etude->set("lastmod", "CURRENT_TIMESTAMP", 1);

				if ($_FILES["image_up"]["name"] != "") {
					$tmp_name = $_FILES["image_up"]["tmp_name"];
					$name = $_FILES["image_up"]["name"];
					$extension = substr($name, strrpos($name, "."));
					$newname = $parameters['key'] . $extension;
					$move = move_uploaded_file($tmp_name, $BASEPATH . "docs/etudes_img/" . $newname);
					$Etude->set("image", $newname);
					if ($Etude->save("image")) {
						$errorString .= _error("Une erreur s'est produite lors de l'insertion de l'image sur le serveur.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
						break;
					}
				}

				if ($_FILES["banner_up"]["name"] != "") {
					$tmp_name = $_FILES["banner_up"]["tmp_name"];
					$name = $_FILES["banner_up"]["name"];
					$extension = substr($name, strrpos($name, "."));
					$newname = $parameters['key'] . $extension;
					$move = move_uploaded_file($tmp_name, $BASEPATH . "docs/etudes_img/banner-" . $newname);
					$Etude->set("banner", "banner-" . $newname);
					if ($Etude->save("banner")) {
						$errorString .= _error("Une erreur s'est produite lors de l'insertion de l'image sur le serveur.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
						break;
					}
				}

				if (!$Etude->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
				}
				break;

			case "modify":
				$Etude = new Etude($DB, "", 0);
				$Etude->scanfields();
				$Etude->set("lastmod", "CURRENT_TIMESTAMP", 1);

				if ($parameters['nom'] == $parameters['newnom']) {
					$Etude->excludefield("nom");
				}

				$Etude->setQKey("nom", $parameters['newnom']);
				if ($Etude->entryExists() && !$Etude->isFieldExcluded("nom")) {
					$errorString .= _error("<a href=\"javascript: showErrorField('nom');\">Le nom de cette &eacute;tude existe d&eacute;j&agrave;.</a>");

				} else {
					$Etude->setQKey("key", $parameters['key']);
					$Etude->set("nom", $parameters['newnom']);
					$Etude->set("nombre_employes", $parameters['nombre_employes']);
					$Etude->set("nombre_notaires_associes", $parameters['nombre_notaires_associes']);
					$Etude->set("nombre_notaires_salaries", $parameters['nombre_notaires_salaries']);
					$Etude->set("courriel", $parameters['courriel']);
					$Etude->set("url_site_web", $parameters['url_site_web']);
					$Etude->set("description", $parameters['description']);


					if (!isset($parameters['droit_affaires_oui'])) { $parameters['droit_affaires_oui'] = 0; }
					if (!isset($parameters['droit_personne_oui'])) { $parameters['droit_personne_oui'] = 0; }
					if (!isset($parameters['droit_agricole_oui'])) { $parameters['droit_agricole_oui'] = 0; }
					if (!isset($parameters['droit_immobilier_oui'])) { $parameters['droit_immobilier_oui'] = 0; }

					$Etude->set("intro_services", $parameters['intro_services']);

					$Etude->set("droit_affaires_oui", $parameters['droit_affaires_oui']);
					$Etude->set("droit_affaires_desc", $parameters['droit_affaires_desc']);

					$Etude->set("droit_personne_oui", $parameters['droit_personne_oui']);
					$Etude->set("droit_personne_desc", $parameters['droit_personne_desc']);

					$Etude->set("droit_agricole_oui", $parameters['droit_agricole_oui']);
					$Etude->set("droit_agricole_desc", $parameters['droit_agricole_desc']);

					$Etude->set("droit_immobilier_oui", $parameters['droit_immobilier_oui']);
					$Etude->set("droit_immobilier_desc", $parameters['droit_immobilier_desc']);

					$Etude->set("new_service_title", $parameters['new_service_title']);
					$Etude->set("new_service_description", $parameters['new_service_description']);

					$Etude->set("meta_description", $parameters['meta_description']);
					$Etude->set("heures_ouverture", $parameters['heures_ouverture']);
					$Etude->set("mode_paiement", $parameters['mode_paiement']);
					$Etude->set("facebook", $parameters['facebook']);
					$Etude->set("twitter", $parameters['twitter']);
					$Etude->set("linkedin", $parameters['linkedin']);

					//$Etude->set("image_description", $parameters['image_description']);
					$Etude->set("image_description_1", $parameters['image_description_1']);
					$Etude->set("image_description_2", $parameters['image_description_2']);
					$Etude->set("image_description_3", $parameters['image_description_3']);

					if ($_FILES["image_up"]["name"] != "") {
						$tmp_name = $_FILES["image_up"]["tmp_name"];
						$name = $_FILES["image_up"]["name"];
						$extension = substr($name, strrpos($name, "."));
						$newname = $parameters['key'] . $extension;
						$move = move_uploaded_file($tmp_name, $BASEPATH . "docs/etudes_img/" . $newname);
						$Etude->set("image", $newname);
						if (!$Etude->save("image")) {
							$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
							break;
						}
					} else {
						if ($parameters['image_del'] == "1") {
							$image = $parameters['image'];
							$image_path = $BASEPATH . "docs/etudes_img/" . $image;
							if (file_exists($image_path)) {
								unlink($image_path);
							}
							$Etude->set("image", "NULL", 1);
						} else {
							$Etude->excludefield("image");
						}
					}



					if ($_FILES["banner_up"]["name"] != "") {
						$tmp_name = $_FILES["banner_up"]["tmp_name"];
						$name = $_FILES["banner_up"]["name"];
						$extension = substr($name, strrpos($name, "."));
						$newname = $parameters['key'] . $extension;
						$move = move_uploaded_file($tmp_name, $BASEPATH . "docs/etudes_img/banner-" . $newname);
						$Etude->set("banner", "banner-" . $newname);
						if (!$Etude->save("banner")) {
							$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
							break;
						}
					} else {
						if ($parameters['banner_del'] == "1") {
							$image = $parameters['banner'];
							$image_path = $BASEPATH . "docs/etudes_img/banner-" . $image;
							if (file_exists($image_path)) {
								unlink($image_path);
							}
							$Etude->set("banner", "NULL", 1);
						} else {
							$Etude->excludefield("banner");
						}
					}

					if (!$Etude->saveAll()) {
						$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
					}
				}
				break;
		}
		return $errorString;
	}

	function addslashesToValues($parameters = "") {
		if (is_array($parameters) && count($parameters) > 0) {
			foreach ($parameters as $field => $value) {
				if (is_array($value) && count($value) > 0) {
					foreach ($value as $field2 => $value2) {
						if (is_array($value2) && count($value2) > 0) {
							foreach ($value2 as $field3 => $value3) {
								$parameters[$field][$field2][$field3] = addslashes($value3);
							}
						} else {
							$parameters[$field][$field2] = addslashes($value2);
						}
					}
				} else {
					$parameters[$field] = addslashes($value);
				}
			}
		} else {
			$parameters = addslashes($parameters);
		}
		return $parameters;
	}

?>
