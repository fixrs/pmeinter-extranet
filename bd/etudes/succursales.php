<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("succursales.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("succursales.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Succursale");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	

	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$formString = "";
	$successString = "";
	$errorString = "";
	
	$begin = getorpost('begin');
	$action = getorpost('action');
	$key = getorpost('key');

	switch ($action) {
		case "add" :
			if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
				exit;
				break;
			}

			if ($begin) {
				importForm("succursales");
				$FormObject = new Form("", "", getForm("add"));
				$formString = $FormObject->returnForm();

			} else {
				$errorString = validateFormValues("add", getParametersFromPOST());
				if ($errorString == "") {
					$errorString = updateDBValues("add", getParametersFromPOST());
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "etudes/index.php\">Retourner au menu pr&eacute;c&eacute;dent.</a>";
						break;
					} 

				} else {
					importForm("succursales");
					$FormObject = new Form("", getParametersFromPOST(), getForm("add"));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "modify" :
			if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
				exit;
				break;
			}

			if ($begin) {
				importForm("succursales");
				$FormObject = new Form("", getParametersFromDB("modify", $key), getForm("modify"));
				$FormObject->setAllHidden();
				$formString = $FormObject->returnFilledForm();

			} else {
				$errorString = validateFormValues("modify", getParametersFromPOST());
				if ($errorString == "") {
					$errorString = updateDBValues("modify", getParametersFromPOST());
					if ($errorString == "") {
						$successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "etudes/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
						break;
					}

				} else {
					importForm("succursales");
					$FormObject = new Form("", getParametersFromPOST(), getForm("modify"));
					$FormObject->setAllHidden();
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "read" :
			importForm("succursales");
			$FormObject = new Form("", getParametersFromDB("read", $key), getForm("read"));
			$FormObject->setAllHidden();
			$formString = $FormObject->returnFilledForm();
			break;

	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
	$Skin->assign("page", "succursales");
	$Skin->assign("title", pageTitleString($action, $key));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("form_title", formTitleString($action, $key));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->assign("index", $indexString);
	$Skin->display("succursales.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function pageTitleString($action, $key) { 
		switch ($action) {
			case "add":
				$title = "Ajout d'une succursale";
				break;
			case "modify":
				$title = "Modification d'une succursale";
				break;
			case "read":
				$title = "Consultation d'une succursale";
				break;
			default:
				$title = "Gestion des succursales";
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "modify":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "read":
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "modify":
				break;

			case "modify":
				break;

			case "read":
				break;
		}
		return $html;
	}

	function formTitleString($action, $key) { 
		switch ($action) {
			case "add":
				$title = "<h1>Ajout d'une succursale</h1>";
				break;
			case "modify":
				$title = "<h1>Modification d'une succursale</h1>";
				break;
			case "read":
				$title = "<h1>Consultation d'une succursale</h1>";
				break;
		}
		return $title;
	}	

	function validateFormValues($action, $parameters) {
		global $DB, $EMAIL_FORMAT_PATTERN;
		$errorString = "";
		switch ($action) {
			case "add":
				if ($parameters['etudes_key'] == "-1" ) {
					$errorString .= _error("<a href=\"javascript: showErrorField('etudes_key');\">Veuillez indiquer l'&eacute;tude.</a>");
				}
				if ($parameters['adresse'] == "" ) {
					$errorString .= _error("<a href=\"javascript: showErrorField('adresse');\">Veuillez indiquer l'adresse de la succursale.</a>");
				}
				if ($parameters['ville'] == "" ) {
					$errorString .= _error("<a href=\"javascript: showErrorField('ville');\">Veuillez indiquer dans quelle ville se trouve la succursale.</a>");
				}
				if ($parameters['province'] == "" ) {
					$errorString .= _error("<a href=\"javascript: showErrorField('province');\">Veuillez indiquer dans quelle province se trouve la succursale.</a>");
				}
				if (trim($parameters['courriel_etudes_succursales']) != "" && !preg_match($EMAIL_FORMAT_PATTERN, $parameters['courriel_etudes_succursales'])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('courriel_etudes_succursales');\">L'adresse courriel fournie n'est pas valide.</a>");
				}
				break;

			case "modify":
				if ($parameters['adresse'] == "" ) {
					$errorString .= _error("<a href=\"javascript: showErrorField('adresse');\">Veuillez indiquer l'adresse de la succursale.</a>");
				}
				if ($parameters['ville'] == "" ) {
					$errorString .= _error("<a href=\"javascript: showErrorField('ville');\">Veuillez indiquer dans quelle ville se trouve la succursale.</a>");
				}
				if ($parameters['province'] == "" ) {
					$errorString .= _error("<a href=\"javascript: showErrorField('province');\">Veuillez indiquer dans quelle province se trouve la succursale.</a>");
				}
				if (trim($parameters['courriel_etudes_succursales']) != "" && !preg_match($EMAIL_FORMAT_PATTERN, $parameters['courriel_etudes_succursales'])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('courriel_etudes_succursales');\">L'adresse courriel fournie n'est pas valide.</a>");
				}
				break;
		}
		return $errorString;
	}

	function getParametersFromPOST() {
		$parameters = array();
		if (count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$parameters[$field] = $value;
				} else {
					$parameters[$field] = trim($value);
				}
			}
		}
		return $parameters;
	}

	function getParametersFromDB($action, $key) {
		global $DB, $BASEURL;
		$parameters = array();
		switch ($action) {
			case "modify":
				$Succursale = new Succursale($DB, "", "");
				$Succursale->setQKey("key", $key);
				$Succursale->scanfields();
				$Succursale->loadAll();
				$parameters = $Succursale->getAll();
				$DB->query(
					"SELECT `key`, `nom` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $Succursale->get("etudes_key") . "' AND `actif` = '1';"
				);
				while ($DB->next_record()) {
					$parameters['etude_nom'] = "<a href=\"" . $BASEURL . "etudes/etudes.php?action=modify&begin=1&key=" . $DB->getField("key") . "\" title=\"Modifier\">" . $DB->getField("nom") . "</a>";
				}
				break;

			case "read":
				$Succursale = new Succursale($DB, "", "");
				$Succursale->setQKey("key", $key);
				$Succursale->scanfields();
				$Succursale->loadAll();
				$parameters = $Succursale->getAll();
				$DB->query(
					"SELECT `key`, `nom`, `courriel` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $Succursale->get("etudes_key") . "' AND `actif` = '1';"
				);
				while ($DB->next_record()) {
					$parameters['etude_nom'] = "<a href=\"" . $BASEURL . "etudes/etudes.php?action=read&begin=1&key=" . $DB->getField("key") . "\" title=\"Consulter\">" . $DB->getField("nom") . "</a>";
					if ($parameters['courriel_etudes_succursales'] == "") {
						$parameters['courriel_etudes_succursales'] = "<a href=\"mailto:" . $DB->getField("courriel") . "\" title=\"Envoyer un courriel\">" . $DB->getField("courriel") . "</a>";
					}
				}
				if ($parameters['siege_social'] == "1") {
					$parameters['siege_social'] = "Oui";
				} else {
					$parameters['siege_social'] = "Non";
				}
				break;
		}
		return $parameters;
	}
	
	function updateDBValues($action, $parameters) {
		global $DB;
		$errorString = "";

		$parameters = addslashesToValues($parameters);

		switch ($action) {
			case "add":
				$parameters['code_postal'] = strtoupper($parameters['code_postal']);
				$Succursale = new Succursale($DB, "", 0);
				$Succursale->scanfields();
				$Succursale->insertmode();
				$Succursale->set("actif", "1");
				$Succursale->setAll($parameters);
				$Succursale->excludefield("action");
				if (!$Succursale->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
				}
				break;
			
			case "modify":
				$parameters['code_postal'] = strtoupper($parameters['code_postal']);
				$Succursale = new Succursale($DB, "", 0);
				$Succursale->setQKey("key", $parameters['key']);
				$Succursale->scanfields();
				$Succursale->setAll($parameters);
				$Succursale->excludefield("action");
				$Succursale->excludefield("etudes_key");
				if (!$Succursale->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
				}
				break;
		}
		return $errorString;
	}

	function addslashesToValues($parameters = "") {
		if (is_array($parameters) && count($parameters) > 0) {
			foreach ($parameters as $field => $value) {
				if (is_array($value) && count($value) > 0) {
					foreach ($value as $field2 => $value2) {
						if (is_array($value2) && count($value2) > 0) {
							foreach ($value2 as $field3 => $value3) {
								$parameters[$field][$field2][$field3] = addslashes($value3);
							}
						} else {
							$parameters[$field][$field2] = addslashes($value2);
						}
					}
				} else {
					$parameters[$field] = addslashes($value);
				}
			}
		} else {
			$parameters = addslashes($parameters);
		}
		return $parameters;
	}

?>
