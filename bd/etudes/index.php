<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("etudes.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("etudes.tpl");
		exit;
	}

	import("com.pmeinter.Etude");
	import("com.pmeinter.Succursale");
	import("com.pmeinter.Emploi");
	import("com.quiboweb.form.Form");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$action = getorpost('action');
	$type = getorpost('type');
	$key = getorpost('key');

	$successString = "";
	$errorString = "";
	$formString = "";

	switch ($_GET['action']) {
		case "add":
			if ($type == "etude") {
				header("Location: etudes.php?action=add&begin=1");
				exit;
			}
			if ($type == "succursale") {
				header("Location: succursales.php?action=add&begin=1");
				exit;
			}
			if ($type == "emploi") {
				header("Location: emplois.php?action=add&begin=1");
				exit;
			}

			break;

		case "modify":
			if ($type == "etude") {
				$formString .= modifyEtudeString();
			}
			if ($type == "succursale") {
				$formString .= modifySuccursaleString();
			}
			if ($type == "emploi") {
				$formString .= modifyEmploiString();
			}
			if ($type == "nouvelle") {
				$formString .= modifyNouvelleString();
			}

			break;

		case "delete":
			if ($type == "etude") {
				$formString .= deleteEtudeString();
			}
			if ($type == "succursale") {
				$formString .= deleteSuccursaleString();
			}
			if ($type == "emploi") {
				$formString .= deleteEmploiString();
			}

			break;

		case "read":
			if ($type == "etude") {
				$formString .= readEtudeString();
			}
			if ($type == "succursale") {
				$formString .= readSuccursaleString();
			}
			if ($type == "emploi") {
				$formString .= readEmploiString();
			}

			break;

	}

	switch ($_POST['action']) {
		case "modify":
			if ($type == "etude") {
				if ($key == "-1") {
					$formString .= modifyEtudeString();
					$errorString .= _error("Veuillez choisir une &eacute;tude dans la liste");
				} else {
					header("Location: etudes.php?action=modify&begin=1&key=" . $key . "");
					exit;
				}
			}
			if ($type == "succursale") {
				if ($key == "-1") {
					$formString .= modifySuccursaleString();
					$errorString .= _error("Veuillez choisir une succursale dans la liste");
				} else {
					header("Location: succursales.php?action=modify&begin=1&key=" . $key . "");
					exit;
				}
			}
			if ($type == "emploi") {
				if ($key == "-1") {
					$formString .= modifyEmploiString();
					$errorString .= _error("Veuillez choisir une offre d'emploi dans la liste");
				} else {
					header("Location: emplois.php?action=modify&begin=1&key=" . $key . "");
					exit;
				}
			}
			if ($type == "nouvelle") {
				if ($key == "-1") {
					$formString .= modifyNouvelleString();
					$errorString .= _error("Veuillez choisir une &eacute;tude dans la liste");
				} else {
					header("Location: nouvelles.php?action=modify&begin=1&key=" . $key . "");
					exit;
				}
			}

			break;

		case "delete":
			if ($type == "etude") {
				if ($key == "-1") {
					$formString .= deleteEtudeString();
					$errorString .= _error("Veuillez choisir une &eacute;tude dans la liste");
				} else {
					if ($_POST['confirmation']) {
						$errorString .= deleteEtudeFromDB($key);
						if ($errorString == "") {
							$successString .= _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s");
						} else {
							$formString .= deleteEtudeString();
						}
					} else {
						$formString .= deleteEtudeString();
						$errorString .= _error("Veuillez confirmer la suppression");
					}
				}
			}
			if ($type == "succursale") {
				if ($key == "-1") {
					$formString .= deleteSuccursaleString();
					$errorString .= _error("Veuillez choisir une succursale dans la liste");
				} else {
					if ($_POST['confirmation']) {
						$errorString .= deleteSuccursaleFromDB($key);
						if ($errorString == "") {
							$successString .= _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s");
						} else {
							$formString .= deleteSuccursaleString();
						}
					} else {
						$formString .= deleteSuccursaleString();
						$errorString .= _error("Veuillez confirmer la suppression");
					}
				}
			}
			if ($type == "emploi") {
				if ($key == "-1") {
					$formString .= deleteEmploiString();
					$errorString .= _error("Veuillez choisir une offre d'emploi dans la liste");
				} else {
					if ($_POST['confirmation']) {
						$errorString .= deleteEmploiFromDB($key);
						if ($errorString == "") {
							$successString .= _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s");
						} else {
							$formString .= deleteEmploiString();
						}
					} else {
						$formString .= deleteEmploiString();
						$errorString .= _error("Veuillez confirmer la suppression");
					}
				}
			}

			break;

		case "read":
			if ($type == "etude") {
				if ($key == "-1") {
					$formString .= readEtudeString();
					$errorString .= _error("Veuillez choisir une &eacute;tude dans la liste");
				} else {
					header("Location: etudes.php?action=read&key=" . $key . "");
					exit;
				}
			}
			if ($type == "succursale") {
				if ($key == "-1") {
					$formString .= readSuccursaleString();
					$errorString .= _error("Veuillez choisir une succursale dans la liste");
				} else {
					header("Location: succursales.php?action=read&key=" . $key . "");
					exit;
				}
			}
			if ($type == "emploi") {
				if ($key == "-1") {
					$formString .= readEmploiString();
					$errorString .= _error("Veuillez choisir une offre d'emploi dans la liste");
				} else {
					header("Location: emplois.php?action=read&key=" . $key . "");
					exit;
				}
			}

			break;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
	$Skin->assign("page", "etudes");
	$Skin->assign("title", pageTitleString($action, $type));
	$Skin->assign("menu", menuActionString());
	$Skin->assign("menu_title", menuTitleString($action, $type));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("etudes.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function pageTitleString($action, $type) {
		switch ($action) {
			case "add":
				if ($type == "etude") {
					$title = "Ajout d'une &eacute;tude";
				}
				if ($type == "succursale") {
					$title = "Ajout d'une succursale";
				}
				if ($type == "emploi") {
					$title = "Ajout d'une offre d'emploi";
				}

				break;
			case "modify":
				if ($type == "etude") {
					$title = "Modification d'une &eacute;tude";
				}
				if ($type == "succursale") {
					$title = "Modification d'une succursale";
				}
				if ($type == "emploi") {
					$title = "Modification d'une offre d'emploi";
				}
				if ($type == "nouvelles") {
					$title = "Modification des nouvelles";
				}

				break;
			case "delete":
				if ($type == "etude") {
					$title = "Suppression d'une &eacute;tude";
				}
				if ($type == "succursale") {
					$title = "Suppression d'une succursale";
				}
				if ($type == "emploi") {
					$title = "Suppression d'une offre d'emploi";
				}

				break;
			case "read":
				if ($type == "etude") {
					$title = "Consulation d'une &eacute;tude";
				}
				if ($type == "succursale") {
					$title = "Consulation d'une succursale";
				}
				if ($type == "emploi") {
					$title = "Consulation d'une offre d'emploi";
				}

			default:
				$title = "Gestion des &eacute;tudes";
		}
		return $title;
	}

	function menuActionString() {
		$html = "";

		$html .=
			"<h1>Gestion des &Eacute;tudes</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n";

		if ($_SESSION['user_type'] == 1) {
			$html .= "<li><a href=\"index.php?action=add&type=etude\">Ajouter une &eacute;tude</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=modify&type=etude\">Modifier une &eacute;tude</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1) {
			$html .= "<li><a href=\"index.php?action=delete&type=etude\">Supprimer une &eacute;tude</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3) {
			$html .= "<li><a href=\"index.php?action=read&type=etude\">Consulter une &eacute;tude</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1) {
			$html .= "<li><a href=\"search.php?action=search\">Rechercher des &eacute;tudes</a></li>\n";
		}

		$html .=
			"	</ul>\n".
			"</div>\n";

		$html .=
			"<h1>Gestion des Succursales</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n";

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=add&type=succursale\">Ajouter une succursale</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=modify&type=succursale\">Modifier une succursale</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=delete&type=succursale\">Supprimer une succursale</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3) {
			$html .= "<li><a href=\"index.php?action=read&type=succursale\">Consulter une succursale</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1) {
			$html .= "<li><a href=\"search.php?action=search\">Rechercher des succursales</a></li>\n";
		}

		$html .=
			"	</ul>\n".
			"</div>\n";

		$html .=
			"<h1>Gestion des Offres d'Emploi</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n";

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=add&type=emploi\">Ajouter une offre d'emploi</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=modify&type=emploi\">Modifier une offre d'emploi</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=delete&type=emploi\">Supprimer une offre d'emploi</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=read&type=emploi\">Consulter une offre d'emploi</a></li>\n";
		}

		$html .=
			"	</ul>\n".
			"</div>\n";

		$html .=
			"<h1>Gestion des Nouvelles</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n";

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=modify&type=nouvelle\">Modifier les nouvelles</a></li>\n";
		}

		$html .=
			"	</ul>\n".
			"</div>\n";


		return $html;
	}

	function menuTitleString($action, $type) {
		$html = "";
		switch ($action) {
			case "modify":
				if ($type == "etude") {
					$html .= "<h2>Modifier une etude</h2>\n";
				}
				if ($type == "succursale") {
					$html .= "<h2>Modifier une succursale</h2>\n";
				}
				if ($type == "emploi") {
					$html .= "<h2>Modifier une offre d'emploi</h2>\n";
				}
				if ($type == "nouvelle") {
					$html .= "<h2>Modifier les nouvelles</h2>\n";
				}

				break;
			case "delete":
				if ($type == "etude") {
					$html .= "<h2>Supprimer une etude</h2>\n";
				}
				if ($type == "succursale") {
					$html .= "<h2>Supprimer une succursale</h2>\n";
				}
				if ($type == "emploi") {
					$html .= "<h2>Supprimer une offre d'emploi</h2>\n";
				}

				break;
			case "read":
				if ($type == "etude") {
					$html .= "<h2>Consulter une etude</h2>\n";
				}
				if ($type == "succursale") {
					$html .= "<h2>Consulter une succursale</h2>\n";
				}
				if ($type == "succursale") {
					$html .= "<h2>Consulter une offre d'emploi</h2>\n";
				}

				break;
		}
		return $html;
	}

	function modifyEtudeString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"	<input type=\"hidden\" name=\"type\" value=\"etude\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n";

		if ($_SESSION['user_type'] == 1) {
			$html .= "<option class=\"default\" value=\"-1\">- Choisir une &eacute;tude -</option>\n";
			$DB->query(
				"SELECT `key`, `nom` ".
				"FROM `etudes` ".
				"WHERE `actif` = '1' ".
				"ORDER BY `nom` ASC;"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$nom = $DB->getField("nom");
				$html .= "<option value=\"" . $key . "\">" . $nom . "</option>\n";
			}

		} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT `key`, `nom` ".
				"FROM `etudes` ".
				"WHERE `actif` = '1' AND `key` = '" . $_SESSION['user_etudes_key'] . "';"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$nom = $DB->getField("nom");
				$html .= "<option value=\"" . $key . "\">" . $nom . "</option>\n";
			}
		}

		$html .=
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function readEtudeString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"	<input type=\"hidden\" name=\"type\" value=\"etude\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n";

		if ($_SESSION['user_type'] == 1) {
			$html .= "<option class=\"default\" value=\"-1\">- Choisir une &eacute;tude -</option>\n";
			$DB->query(
				"SELECT `key`, `nom` ".
				"FROM `etudes` ".
				"WHERE `actif` = '1' ".
				"ORDER BY `nom` ASC;"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$nom = $DB->getField("nom");
				$html .= "<option value=\"" . $key . "\">" . $nom . "</option>\n";
			}

		} elseif (($_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3) && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT `key`, `nom` ".
				"FROM `etudes` ".
				"WHERE `actif` = '1' AND `key` = '" . $_SESSION['user_etudes_key'] . "';"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$nom = $DB->getField("nom");
				$html .= "<option value=\"" . $key . "\">" . $nom . "</option>\n";
			}
		}

		$html .=
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function deleteEtudeString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"	<input type=\"hidden\" name=\"type\" value=\"etude\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n";

		if ($_SESSION['user_type'] == 1) {
			$html .= "<option class=\"default\" value=\"-1\">- Choisir une &eacute;tude -</option>\n";
			$DB->query(
				"SELECT `key`, `nom` ".
				"FROM `etudes` ".
				"WHERE `actif` = '1' ".
				"ORDER BY `nom` ASC;"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$nom = $DB->getField("nom");
				$html .= "<option value=\"" . $key . "\">" . $nom . "</option>\n";
			}

		}

		$html .=
			"		</select><br />\n".
			"		<br />\n".
			"		Confirmer&nbsp: <input type=\"checkbox\" class=\"checkbox\" id=\"confirmation\" name=\"confirmation\">\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Supprimer\" />\n".
			"</form>\n";

		return $html;
	}

	function modifySuccursaleString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"	<input type=\"hidden\" name=\"type\" value=\"succursale\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir une succursale -</option>\n";

		if ($_SESSION['user_type'] == 1) {
			$DB->query(
				"SELECT e.nom AS etude_nom, es.key AS succursale_key, es.etudes_key AS etude_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville ".
				"FROM `etudes` AS e, `etudes_succursales` AS es ".
				"WHERE es.etudes_key = e.key AND es.actif = '1' ".
				"ORDER BY e.nom;"
			);

			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("succursale_key");
				$option = $DB->getField("adresse") . ", " . $DB->getField("ville");
				if ($DB->getField("siege_social") == 1) {
					$option .= " [SS]";
				}
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_succursales_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}

		} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT e.nom AS etude_nom, es.key AS succursale_key, es.etudes_key AS etude_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville ".
				"FROM `etudes` AS e, `etudes_succursales` AS es ".
				"WHERE e.key = '" . $_SESSION['user_etudes_key'] . "' AND es.etudes_key = e.key AND es.actif = '1' ".
				"ORDER BY e.nom;"
			);
			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("succursale_key");
				$option = $DB->getField("adresse") . ", " . $DB->getField("ville");
				if ($DB->getField("siege_social") == 1) {
					$option .= " [SS]";
				}
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_succursales_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}
		}

		$html .=
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function readSuccursaleString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"	<input type=\"hidden\" name=\"type\" value=\"succursale\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir une succursale -</option>\n";

			if ($_SESSION['user_type'] == 1) {
			$DB->query(
				"SELECT e.nom AS etude_nom, es.key AS succursale_key, es.etudes_key AS etude_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville ".
				"FROM `etudes` AS e, `etudes_succursales` AS es ".
				"WHERE es.etudes_key = e.key AND es.actif = '1' ".
				"ORDER BY e.nom;"
			);

			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("succursale_key");
				$option = $DB->getField("adresse") . ", " . $DB->getField("ville");
				if ($DB->getField("siege_social") == 1) {
					$option .= " [SS]";
				}
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_succursales_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}

		} elseif (($_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3) && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT e.nom AS etude_nom, es.key AS succursale_key, es.etudes_key AS etude_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville ".
				"FROM `etudes` AS e, `etudes_succursales` AS es ".
				"WHERE e.key = '" . $_SESSION['user_etudes_key'] . "' AND es.etudes_key = e.key AND es.actif = '1' ".
				"ORDER BY e.nom;"
			);
			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("succursale_key");
				$option = $DB->getField("adresse") . ", " . $DB->getField("ville");
				if ($DB->getField("siege_social") == 1) {
					$option .= " [SS]";
				}
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_succursales_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}
		}

		$html .=
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function deleteSuccursaleString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"	<input type=\"hidden\" name=\"type\" value=\"succursale\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir une succursale -</option>\n";

		if ($_SESSION['user_type'] == 1) {
			$DB->query(
				"SELECT e.nom AS etude_nom, es.key AS succursale_key, es.etudes_key AS etude_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville ".
				"FROM `etudes` AS e, `etudes_succursales` AS es ".
				"WHERE es.etudes_key = e.key AND es.actif = '1' ".
				"ORDER BY e.nom;"
			);

			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("succursale_key");
				$option = $DB->getField("adresse") . ", " . $DB->getField("ville");
				if ($DB->getField("siege_social") == 1) {
					$option .= " [SS]";
				}
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_succursales_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}

		} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT e.nom AS etude_nom, es.key AS succursale_key, es.etudes_key AS etude_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville ".
				"FROM `etudes` AS e, `etudes_succursales` AS es ".
				"WHERE e.key = '" . $_SESSION['user_etudes_key'] . "' AND es.etudes_key = e.key AND es.actif = '1' ".
				"ORDER BY e.nom;"
			);
			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("succursale_key");
				$option = $DB->getField("adresse") . ", " . $DB->getField("ville");
				if ($DB->getField("siege_social") == 1) {
					$option .= " [SS]";
				}
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_succursales_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}
		}

		$html .=
			"		</select><br />\n".
			"		<br />\n".
			"		Confirmer&nbsp: <input type=\"checkbox\" class=\"checkbox\" id=\"confirmation\" name=\"confirmation\">\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Supprimer\" />\n".
			"</form>\n";

		return $html;
	}




	function modifyEmploiString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"	<input type=\"hidden\" name=\"type\" value=\"emploi\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir une offre d'emploi -</option>\n";

		if ($_SESSION['user_type'] == 1) {
			$DB->query(
				"SELECT e.nom AS etude_nom, ee.key AS emploi_key, ee.etudes_key AS etude_key, ee.poste AS poste, ee.date_entree AS date_entree, ee.contact AS contact, ee.afficher AS afficher, ee.date_affichage ".
				"FROM `etudes` AS e, `etudes_emplois` AS ee ".
				"WHERE ee.etudes_key = e.key AND e.actif = '1' ".
				"ORDER BY e.nom;"
			);

			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("emploi_key");
				//switch ($DB->getField("poste")) {
				//	case 1:
				//		$poste = "Notaire";
				//		break;
				//	case 2:
				//		$poste = "Technicien";
				//		break;
				//	default:
				//		$poste = "Ind&eacute;termin&eacute;";
				//}

				$poste = $DB->getField("poste");

				if ($DB->getField("afficher")) {
					$afficher = "[AFFICH&Eacute;]";
				} else {
					$afficher = "";
				}
				$option = $poste . " (" . $DB->getField("date_affichage") . ") ... " . $afficher;
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_emplois_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}

		} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT e.nom AS etude_nom, ee.key AS emploi_key, ee.etudes_key AS etude_key, ee.poste AS poste, ee.date_entree AS date_entree, ee.contact AS contact, ee.afficher AS afficher, ee.date_affichage ".
				"FROM `etudes` AS e, `etudes_emplois` AS ee ".
				"WHERE e.key = '" . $_SESSION['user_etudes_key'] . "' AND ee.etudes_key = e.key ".
				"ORDER BY e.nom;"
			);
			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("emploi_key");
				//switch ($DB->getField("poste")) {
				//	case 1:
				//		$poste = "Notaire";
				//		break;
				//	case 2:
				//		$poste = "Technicien";
				//		break;
				//	default:
				//		$poste = "Ind&eacute;termin&eacute;";
				//}
				$poste = $DB->getField("poste");

				if ($DB->getField("afficher")) {
					$afficher = "[AFFICH&Eacute;]";
				} else {
					$afficher = "";
				}
				$option = $poste . " (" . $DB->getField("date_affichage") . ") ... " . $afficher;
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_emplois_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}
		}

		$html .=
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function readEmploiString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"	<input type=\"hidden\" name=\"type\" value=\"emploi\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir une offre d'emploi -</option>\n";

		if ($_SESSION['user_type'] == 1) {
			$DB->query(
				"SELECT e.nom AS etude_nom, ee.key AS emploi_key, ee.etudes_key AS etude_key, ee.poste AS poste, ee.date_entree AS date_entree, ee.contact AS contact, ee.afficher AS afficher, ee.date_affichage ".
				"FROM `etudes` AS e, `etudes_emplois` AS ee ".
				"WHERE ee.etudes_key = e.key AND e.actif = '1' ".
				"ORDER BY e.nom;"
			);

			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("emploi_key");
				//switch ($DB->getField("poste")) {
				//	case 1:
				//		$poste = "Notaire";
				//		break;
				//	case 2:
				//		$poste = "Technicien";
				//		break;
				//	default:
				//		$poste = "Ind&eacute;termin&eacute;";
				//}

				$poste = $DB->getField("poste");

				if ($DB->getField("afficher")) {
					$afficher = "[AFFICH&Eacute;]";
				} else {
					$afficher = "";
				}
				$option = $poste . " (" . $DB->getField("date_affichage") . ") ..." . $afficher;
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_emplois_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}

		} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT e.nom AS etude_nom, ee.key AS emploi_key, ee.etudes_key AS etude_key, ee.poste AS poste, ee.date_entree AS date_entree, ee.contact AS contact, ee.afficher AS afficher, ee.date_affichage ".
				"FROM `etudes` AS e, `etudes_emplois` AS ee ".
				"WHERE e.key = '" . $_SESSION['user_etudes_key'] . "' AND ee.etudes_key = e.key ".
				"ORDER BY e.nom;"
			);
			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("emploi_key");
				//switch ($DB->getField("poste")) {
				//	case 1:
				//		$poste = "Notaire";
				//		break;
				//	case 2:
				//		$poste = "Technicien";
				//		break;
				//	default:
				//		$poste = "Ind&eacute;termin&eacute;";
				//}

				$poste = $DB->getField("poste");

				if ($DB->getField("afficher")) {
					$afficher = "[AFFICH&Eacute;]";
				} else {
					$afficher = "";
				}
				$option = $poste . " (" . $DB->getField("date_affichage") . ") ..." . $afficher;
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_emplois_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}
		}

		$html .=
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function deleteEmploiString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"	<input type=\"hidden\" name=\"type\" value=\"emploi\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir une offre d'emploi -</option>\n";

		if ($_SESSION['user_type'] == 1) {
			$DB->query(
				"SELECT e.nom AS etude_nom, ee.key AS emploi_key, ee.etudes_key AS etude_key, ee.poste AS poste, ee.date_entree AS date_entree, ee.contact AS contact, ee.afficher AS afficher, ee.date_affichage ".
				"FROM `etudes` AS e, `etudes_emplois` AS ee ".
				"WHERE ee.etudes_key = e.key AND e.actif = '1' ".
				"ORDER BY e.nom;"
			);

			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("emploi_key");
				//switch ($DB->getField("poste")) {
				//	case 1:
				//		$poste = "Notaire";
				//		break;
				//	case 2:
				//		$poste = "Technicien";
				//		break;
				//	default:
				//		$poste = "Ind&eacute;termin&eacute;";
				//}

				$poste = $DB->getField("poste");

				if ($DB->getField("afficher")) {
					$afficher = "[AFFICH&Eacute;]";
				} else {
					$afficher = "";
				}
				$option = $poste . " (" . $DB->getField("date_affichage") . ") ..." . $afficher;
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_emplois_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}

		} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT e.nom AS etude_nom, ee.key AS emploi_key, ee.etudes_key AS etude_key, ee.poste AS poste, ee.date_entree AS date_entree, ee.contact AS contact, ee.afficher AS afficher, ee.date_affichage ".
				"FROM `etudes` AS e, `etudes_emplois` AS ee ".
				"WHERE e.key = '" . $_SESSION['user_etudes_key'] . "' AND ee.etudes_key = e.key ".
				"ORDER BY e.nom;"
			);
			$previous = "";
			$first = 1;
			while ($DB->next_record()) {
				$optgroup = $DB->getField("etude_nom");
				$value = $DB->getField("emploi_key");
				//switch ($DB->getField("poste")) {
				//	case 1:
				//		$poste = "Notaire";
				//		break;
				//	case 2:
				//		$poste = "Technicien";
				//		break;
				//	default:
				//		$poste = "Ind&eacute;termin&eacute;";
				//}

				$poste = $DB->getField("poste");

				if ($DB->getField("afficher")) {
					$afficher = "[AFFICH&Eacute;]";
				} else {
					$afficher = "";
				}
				$option = $poste . " (" . $DB->getField("date_affichage") . ") ..." . $afficher;
				if ($optgroup != $previous) {
					if (!$first) {
						$html .= "</optgroup>";
					}
					$html .= "<optgroup label=\"" . $optgroup . "\">";
				}
				$html .= "<option selectname=\"etudes_emplois_key\" value=\"" . $value . "\">" . $option . "</option>\n";
				$previous = $optgroup;
				$first = 0;
			}
		}

		$html .=
			"		</select><br />\n".
			"		<br />\n".
			"		Confirmer&nbsp: <input type=\"checkbox\" class=\"checkbox\" id=\"confirmation\" name=\"confirmation\">\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Supprimer\" />\n".
			"</form>\n";

		return $html;
	}

	function modifyNouvelleString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"	<input type=\"hidden\" name=\"type\" value=\"nouvelle\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n";

		if ($_SESSION['user_type'] == 1) {
			$html .= "<option class=\"default\" value=\"-1\">- Choisir une &eacute;tude -</option>\n";
			$DB->query(
				"SELECT `key`, `nom` ".
				"FROM `etudes` ".
				"WHERE `actif` = '1' ".
				"ORDER BY `nom` ASC;"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$nom = $DB->getField("nom");
				$html .= "<option value=\"" . $key . "\">" . $nom . "</option>\n";
			}

		} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT `key`, `nom` ".
				"FROM `etudes` ".
				"WHERE `actif` = '1' AND `key` = '" . $_SESSION['user_etudes_key'] . "';"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$nom = $DB->getField("nom");
				$html .= "<option value=\"" . $key . "\">" . $nom . "</option>\n";
			}
		}

		$html .=
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function deleteEtudeFromDB($key) {
		if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
			exit;
			break;
		}

		global $DB;
		$errorString = "";
		$Etude = new Etude($DB, "", 1);
		$Etude->setQKey("key", $key);
		$DB->query(
			"SELECT COUNT(*) AS nb ".
			"FROM `etudes_succursales` ".
			"WHERE `etudes_key` = '" . $key . "' AND `actif` = '1'"
		);
		while ($DB->next_record()) {
			$nb = $DB->getField("nb");
		}
		if ($nb > 0) {
			$errorString .= _error("Une ou plusieurs succursales sont encore rattach&eacute;es &agrave; cette &eacute;tude.<br />Veuillez d'abrod supprimer toutes les succursales de l'&eacute;tude.");
		} else {
			if (!$Etude->set("actif", "0")) {
				$errorString .= _error("Une erreur s'est produite lors de la suppression des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
			}
		}
		return $errorString;
	}

	function deleteSuccursaleFromDB($key) {
		if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
			exit;
			break;
		}

		global $DB;
		$errorString = "";
		$Succursale = new Succursale($DB, "", 1);
		$Succursale->setQKey("key", $key);
		if (!$Succursale->set("actif", "0")) {
			$errorString .= _error("Une erreur s'est produite lors de la suppression des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
		}
		return $errorString;
	}


	function deleteEmploiFromDB($key) {
		if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
			exit;
			break;
		}

		global $DB;
		$errorString = "";
		$delete = $DB->query(
				"DELETE ".
				"FROM `etudes_emplois` ".
				"WHERE `key` = '" . addslashes($key) . "';"
		);
		if (!$delete) {
			$errorString .= _error("Une erreur s'est produite lors de la suppression des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
		}
		return $errorString;
	}
