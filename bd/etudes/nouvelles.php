<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("nouvelles.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("nouvelles.tpl");
		exit;
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$key = getorpost('key');
	$action = getorpost('action');
	$begin = getorpost('begin');

	if ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != $key) {
		exit;
	}

	if ($begin) {
		$query = "SELECT ";

		for ($n = 1; $n <= $NB_NOUVELLES; $n++) {
			if ($n == $NB_NOUVELLES) {
				$query .= "`nouvelle_" . $n . "_titre`, `nouvelle_" . $n . "_doc`, `nouvelle_" . $n . "_visible`, `nouvelle_" . $n . "_video`, `nouvelle_" . $n . "_description`, `nouvelle_" . $n . "_img` ";
			} else {
				$query .= "`nouvelle_" . $n . "_titre`, `nouvelle_" . $n . "_doc`, `nouvelle_" . $n . "_visible`, `nouvelle_" . $n . "_video`, `nouvelle_" . $n . "_description`, `nouvelle_" . $n . "_img`, ";
			}
		}

		$query .=
			"FROM `etudes` ".
			"WHERE `key` = " . $key . ";";

		$DB->query($query);
		while ($DB->next_record()) {
			for ($n = 1; $n <= $NB_NOUVELLES; $n++) {
				$nouvelles[$n]['titre'] = $DB->getField("nouvelle_" . $n . "_titre");
				$nouvelles[$n]['doc'] = $DB->getField("nouvelle_" . $n . "_doc");
				$nouvelles[$n]['visible'] = $DB->getField("nouvelle_" . $n . "_visible");
				$nouvelles[$n]['video'] = $DB->getField("nouvelle_" . $n . "_video");
				$nouvelles[$n]['img'] = $DB->getField("nouvelle_" . $n . "_img");
				$nouvelles[$n]['description'] = $DB->getField("nouvelle_" . $n . "_description");
			}
		}

	} else {

		$err = 0;

		for ($n = 1; $n <= $NB_NOUVELLES; $n++) {

			if (getorpost('supprimer_doc_bool_' . $n)) {
				$DB->query(
					"SELECT `nouvelle_" . $n . "_doc` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $key . "';"
				);
				while($DB->next_record()) {
					@unlink($BASEPATH . "docs/etudes_nouvelles/" . $key . "/" . $DB->getField("nouvelle_" . $n . "_doc"));
				}

				$DB->query(
					"UPDATE `etudes` ".
					"SET `nouvelle_" . $n . "_doc` = NULL ".
					"WHERE `key` = '" . $key . "';"
				);
			}

			if (getorpost('supprimer_img_bool_' . $n)) {
				$DB->query(
					"SELECT `nouvelle_" . $n . "_img` ".
					"FROM `etudes` ".
					"WHERE `key` = '" . $key . "';"
				);
				while($DB->next_record()) {
					@unlink($BASEPATH . "docs/etudes_nouvelles/" . $key . "/t" . $DB->getField("nouvelle_" . $n . "_img"));
				}

				$DB->query(
					"UPDATE `etudes` ".
					"SET `nouvelle_" . $n . "_img` = NULL ".
					"WHERE `key` = '" . $key . "';"
				);
			}

			$doc_nb = "nouvelle_" . $n . "_doc";
			$doc_name = "";
			if ($_FILES[$doc_nb]['name'] != "") {
				$tmp_name = $_FILES[$doc_nb]['tmp_name'];
				$doc_name = $_FILES[$doc_nb]['name'];

				if (!is_dir($BASEPATH . "docs/etudes_nouvelles/" . $key)) {
					@mkdir($BASEPATH . "docs/etudes_nouvelles/" . $key);
				}

				if (file_exists($BASEPATH . "docs/etudes_nouvelles/" . $key . "/" . $doc_name)) {
					@unlink($BASEPATH . "docs/etudes_nouvelles/" . $key . "/" . $doc_name);
				}

				@$move_chk = move_uploaded_file($tmp_name, $BASEPATH . "docs/etudes_nouvelles/" . $key . "/" . $doc_name);
				@$copy_chk = file_exists($BASEPATH . "docs/etudes_nouvelles/" . $key . "/" . $doc_name);

				if (!$move_chk || !$copy_chk) {
					$err = 1;
				}
			}

			$img_nb = "nouvelle_" . $n . "_img";
			$img_name = "";
			if ($_FILES[$img_nb]['name'] != "") {
				$tmp_name = $_FILES[$img_nb]['tmp_name'];
				$img_name = $_FILES[$img_nb]['name'];

				if (!is_dir($BASEPATH . "docs/etudes_nouvelles/" . $key)) {
					@mkdir($BASEPATH . "docs/etudes_nouvelles/" . $key);
				}

				if (file_exists($BASEPATH . "docs/etudes_nouvelles/" . $key . "/t" . $img_name)) {
					@unlink($BASEPATH . "docs/etudes_nouvelles/" . $key . "/t" . $img_name);
				}

				@$move_chk = move_uploaded_file($tmp_name, $BASEPATH . "docs/etudes_nouvelles/" . $key . "/t" . $img_name);
				@$copy_chk = file_exists($BASEPATH . "docs/etudes_nouvelles/" . $key . "/t" . $img_name);

				if (!$move_chk || !$copy_chk) {
					$err = 1;
				}
			}

			$nouvelles[$n]['doc'] = $doc_name;
			$nouvelles[$n]['img'] = $img_name;
			$nouvelles[$n]['titre'] = trim(stripslashes(getorpost('nouvelle_' . $n . '_titre')));
			$nouvelles[$n]['description'] = trim(stripslashes(getorpost('nouvelle_' . $n . '_description')));
			$nouvelles[$n]['visible'] = trim(stripslashes(getorpost('nouvelle_' . $n . '_visible')));
			$nouvelles[$n]['video'] = trim(stripslashes(getorpost('nouvelle_' . $n . '_video')));

			$query =
				"UPDATE ".
				"	`etudes` ".
				"SET ";

			if ($nouvelles[$n]['doc'] != "") {
				$query .= "`nouvelle_" . $n . "_doc` = '" . addslashes($nouvelles[$n]['doc']) . "', ";
			}

			if ($nouvelles[$n]['img'] != "") {
				$query .= "`nouvelle_" . $n . "_img` = '" . addslashes($nouvelles[$n]['img']) . "', ";
			}

			$query .=
				"	`nouvelle_" . $n . "_titre` = '" . addslashes($nouvelles[$n]['titre']) . "', ".
				"	`nouvelle_" . $n . "_description` = '" . addslashes($nouvelles[$n]['description']) . "', ".
				"	`nouvelle_" . $n . "_video` = '" . addslashes($nouvelles[$n]['video']) . "', ".
				"	`nouvelle_" . $n . "_visible` = '" . addslashes($nouvelles[$n]['visible']) . "' ".
				"WHERE ".
				"	`key` = '" . $key . "';";

			$DB->query($query);
		}

		if ($err) {
			$feedback = _error("Un probl&egrave;me s'est produit lors de l'&eacute;criture d'un ou plusieurs fichiers sur le serveur.") . "<br />";
		} else {
			$feedback = _success("Les nouvelles ont &eacute;t&eacute; mises &agrave; jour avec succ&egrave;s.") . "<br />";
		}
	}

	$DB->query(
		"SELECT `nom` ".
		"FROM `etudes` ".
		"WHERE `key` = " . $key . ";"
	);
	while ($DB->next_record()) {
		$etude_nom = $DB->getField('nom');
	}

	$title =
		"<h1>Modification des nouvelles</h1>\n".
		"<h2>" . $etude_nom . "</h2>";

	$form = "<div class=\"notice\" style=\"width:90%; padding:8px;\"><strong>Important</strong>&nbsp;:<br/>Si vous soumettez un document pour une nouvelle, le nom du fichier de celui-ci ne doit pas comporter de caract&egrave;res sp&eacute;ciaux.<br/>Seuls les <strong>lettres</strong>, les <strong>chiffres</strong>, le caract&egrave;re de <strong>soulignement</strong> et le <strong>point</strong> sont supportés.</div><br/>\n";

	$form .=
		"<form name=\"nouvelles\" action=\"nouvelles.php\" method=\"post\" enctype=\"multipart/form-data\">\n".
		"	<input type=\"hidden\" name=\"key\" value=\"" . $key . "\">\n".
		"	<input type=\"hidden\" name=\"action\" value=\"modify\">\n";

	for ($n = 1; $n <= $NB_NOUVELLES; $n++) {
		$form .=
			"	<div class=\"entry boxed\">\n".
			"		<strong>Nouvelle #" . $n . "</strong><br />\n".
			"		<br />\n".
			"		<label for=\"nouvelle_" . $n . "_titre\" class=\"inline\">Titre&nbsp;:</label>\n".
			"		<input type=\"text\" name=\"nouvelle_" . $n . "_titre\" id=\"nouvelle_" . $n . "_titre\" value=\"" . $nouvelles[$n]['titre'] . "\" class=\"text\" style=\"width: 400px;\" /><br />\n".
			"		<br />\n".
			"		<label for=\"nouvelle_" . $n . "_description\" class=\"inline\">Description&nbsp;:</label>\n".
			"		<textarea name=\"nouvelle_" . $n . "_description\" id=\"nouvelle_" . $n . "_description\">" . $nouvelles[$n]['description'] . "</textarea><br />\n".
			"		<br /><hr /><br />\n".
			"		<label for=\"nouvelle_" . $n . "_doc\" class=\"inline\">Document &agrave; soumettre&nbsp;:</label>\n".
			"		<input type=\"file\" name=\"nouvelle_" . $n . "_doc\" id=\"nouvelle_" . $n . "_doc\" /><br />\n".

			getCurrentDocLink($n, $DB, $key).

			"		<br /><hr /><br />\n".
			"		<label for=\"nouvelle_" . $n . "_img\" class=\"inline\">Image &agrave; soumettre&nbsp;:</label>\n".
			"		<input type=\"file\" name=\"nouvelle_" . $n . "_img\" id=\"nouvelle_" . $n . "_img\" /><br />\n".

	 		getCurrentImgLink($n, $DB, $key).

			"		<br />\n".
			"		OU <br /><br /><label for=\"nouvelle_" . $n . "_video\" class=\"inline\">Vid&eacute;o Youtube &agrave; soumettre&nbsp;:</label>\n".
			"		<input type=\"text\" name=\"nouvelle_" . $n . "_video\" id=\"nouvelle_" . $n . "_video\" value=\"" . $nouvelles[$n]['video'] . "\" /><br />\n".
			"		<br /><hr />\n".
			"		<br />\n".
			"		<label for=\"nouvelle_" . $n . "_visible\" class=\"inline\">Activ&eacute;e&nbsp;:</label>\n".

			getVisibleSelect($n, $DB, $key).

			"	</div>\n";

	}

	$form .=
		"	<input type=\"submit\" class=\"submit\" value=\"Soumettre\" />\n".
		"</form>\n";

	$content = $title . $feedback . $form;

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
	$Skin->assign("page", "nouvelles");
	$Skin->assign("title", "Modification des nouvelles");
	$Skin->assign("content", $content);
	$Skin->display("nouvelles.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function getCurrentDocLink($n, $DB, $key) {
		global $BASEURL, $BASEPATH;
		$html = "";

		$DB->query(
			"SELECT `nouvelle_" . $n . "_doc` ".
			"FROM `etudes` ".
			"WHERE `key` = '" . $key . "';"
		);
		while($DB->next_record()) {
			$doc = $DB->getField("nouvelle_" . $n . "_doc");
		}

		$file = $BASEPATH . "docs/etudes_nouvelles/" . $key . "/" . $doc;

		if ($doc != "" && file_exists($file)) {
			$html .=
				"<br />\n".
				"Document pr&eacute;sent&nbsp;: <a href=\"" . $BASEURL . "docs/etudes_nouvelles/" . $key . "/" . $doc . "\" alt=\"Document de la nouvelle #" . $n ."\">" . $doc . "</a><br />\n".
				"<br />\n".
				"<label for=\"supprimer_doc_bool_" . $n . "\" class=\"inline\" >Supprimer le document pr&eacute;sent&nbsp;:</label><input type=\"checkbox\" id=\"supprimer_doc_bool_" . $n . "\" name=\"supprimer_doc_bool_" . $n . "\" class=\"checkbox\" /><br />";
		}

		return $html;
	}

	function getCurrentImgLink($n, $DB, $key) {
		global $BASEURL, $BASEPATH;
		$html = "";

		$DB->query(
			"SELECT `nouvelle_" . $n . "_img` ".
			"FROM `etudes` ".
			"WHERE `key` = '" . $key . "';"
		);
		while($DB->next_record()) {
			$doc = $DB->getField("nouvelle_" . $n . "_img");
		}

		$file = $BASEPATH . "docs/etudes_nouvelles/" . $key . "/t" . $doc;

		if ($doc != "" && file_exists($file)) {
			$html .=
				"<br />\n".
				"Image pr&eacute;sente&nbsp;: <a href=\"" . $BASEURL . "docs/etudes_nouvelles/" . $key . "/t" . $doc . "\" alt=\"Image de la nouvelle #" . $n ."\">" . $doc . "</a><br />\n".
				"<br />\n".
				"<label for=\"supprimer_img_bool_" . $n . "\" class=\"inline\" >Supprimer l'image pr&eacute;sente&nbsp;:</label><input type=\"checkbox\" id=\"supprimer_img_bool_" . $n . "\" name=\"supprimer_img_bool_" . $n . "\" class=\"checkbox\" /><br />";
		}

		return $html;
	}


	function getVisibleSelect($n, $DB, $key) {
		$html = "";

		$DB->query(
			"SELECT `nouvelle_" . $n . "_visible` ".
			"FROM `etudes` ".
			"WHERE `key` = '" . $key . "';"
		);
		$visible = 0;
		while($DB->next_record()) {
			if ($DB->getField("nouvelle_" . $n . "_visible")) {
				$visible = 1;
			}
		}

		if ($visible) {
			$html .=
				"<input type=\"radio\" name=\"nouvelle_" . $n . "_visible\" value=\"1\" class=\"radio\" checked=\"checked\" />&nbsp;<label for=\"nouvelle_" . $n . "_visible\" class=\"inline\">Oui</label>\n".
				"<input type=\"radio\" name=\"nouvelle_" . $n . "_visible\" value=\"0\" class=\"radio\" />&nbsp;<label for=\"nouvelle_" . $n . "_visible\" class=\"inline\">Non</label>\n";
		} else {
			$html .=
				"<input type=\"radio\" name=\"nouvelle_" . $n . "_visible\" value=\"1\" class=\"radio\" />&nbsp;<label for=\"nouvelle_" . $n . "_visible\" class=\"inline\">Oui</label>\n".
				"<input type=\"radio\" name=\"nouvelle_" . $n . "_visible\" value=\"0\" class=\"radio\" checked=\"checked\" />&nbsp;<label for=\"nouvelle_" . $n . "_visible\" class=\"inline\">Non</label>\n";
		}

		return $html;
	}
