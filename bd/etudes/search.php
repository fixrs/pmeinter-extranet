<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("etudes.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");
		$Skin->assign("errors", $html);
		$Skin->display("etudes.tpl");
		exit;	
	}
	
	import("com.pmeinter.Etude");
	import("com.pmeinter.Succursale");
	import("com.quiboweb.form.Form");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$successString = "";
	$errorString = "";
	$formString = "";
	$expertisesArray = array('droit_des_affaires_desc'=> 'Droit des affaires', 'droit_des_personnes_desc'=>'Droit des personnes', 'droit_immobilier_desc'=>'Droit immobilier', 'expertises_sectorielles_desc'=> 'Expertises sectorielles');
	$dropDownNombreEmployes = array('BETWEEN_1_AND_10'=> 'Entre 1 et 10', 'BETWEEN_11_AND_50'=>'Entre 11 et 50', 'BETWEEN_51_AND_100'=>'Entre 51 et 100', 'BETWEEN_100_AND_1000000'=> '100 et plus');
	
	$action = getorpost('action');
	$key = getorpost('key');

	switch ($_GET['action']) {
		case "search":
			$formString .= searchString();
			break;
		case "results":
			$formString .= searchResultsString();
			break;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "etudes");	
	$Skin->assign("page", "etudes");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("menu_title", menuTitleString($action));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("etudes.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function pageTitleString($action) { 
		switch ($action) {
			case "search":
				$title = "Recherche d'&eacute;tudes";
				break;
			case "results":
				$title = "R&eacute;sultats de recherche d'&eacute;tudes";
				break;
			default:
				$title = "Recherche d'&eacute;tudes";
		}
		return $title;
	}

	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "search":
				$html .= "<h1>Rechercher des &eacute;tudes</h1>\n";
				break;
			case "results":
				$html .= "<h1>Rechercher des &eacute;tudes</h1>\n";
				break;
		}
		return $html;
	}

	function getExpertisesList($expertise_key=""){

		global $expertisesArray;
		$html = 
			"	<select id=\"domaines_list\" name=\"domaines_list\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir un domaine d'expertise (facultatif) -</option>\n";

		if (count($expertisesArray) > 0) {
			foreach ($expertisesArray as $key => $value) {
				$selected = "";
				if ($expertise_key==$key){
					$selected = " selected=\"selected\" ";
				}
				$html .= "<option ".$selected."value=\"" . $key. "\">" . $value. "</option>\n";
			}
		}		
		
		$html .= 
			"		</select>\n";

		return $html;
	}

	function getNombreEmployesList($nombre_yes=""){

		global $dropDownNombreEmployes;
		$html = 
			"	<select id=\"nombre_employes_list\" name=\"nombre_employes_list\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir un nombre d'employ&eacute;s (facultatif) -</option>\n";

		if (count($dropDownNombreEmployes) > 0) {
			foreach ($dropDownNombreEmployes as $key => $value) {
				$selected = "";
				if ($nombre_yes==$key){
					$selected = " selected=\"selected\" ";
				}
				$html .= "<option ".$selected."value=\"" . $key. "\">" . $value. "</option>\n";
			}
		}		
		
		$html .= 
			"		</select>\n";

		return $html;
	}

	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function searchString() { 
		$nom = "";
		$nomNotaire = "";
		$adresse = "";
		$ville = "";
		$codePostal = "";
		$telephone = "";
		$telecopieur = "";
		$courriel = "";
		$siteWeb = "";
		$motCle = "";
		$expertisesSectorielles = "";
		$nombre_yes = "";
		$descriptionEtude = "";

		if (isset($_GET['search_etudes']) && trim($_GET['search_etudes'])!=""){
			$nom = trim($_GET['search_etudes']);
		}
		if (isset($_GET['adresse']) && trim($_GET['adresse'])!=""){
			$adresse = trim($_GET['adresse']);
		}
		if (isset($_GET['ville']) && trim($_GET['ville'])!=""){
			$ville = trim($_GET['ville']);
		}
		if (isset($_GET['code_postal']) && trim($_GET['code_postal'])!=""){
			$codePostal = trim($_GET['code_postal']);
		}
		if (isset($_GET['telephone']) && trim($_GET['telephone'])!=""){
			$telephone = trim($_GET['telephone']);
		}
		if (isset($_GET['telecopieur']) && trim($_GET['telecopieur'])!=""){
			$telecopieur = trim($_GET['telecopieur']);
		}
		if (isset($_GET['courriel']) && trim($_GET['courriel'])!=""){
			$courriel = trim($_GET['courriel']);
		}
		if (isset($_GET['site_web']) && trim($_GET['site_web'])!=""){
			$siteWeb = trim($_GET['site_web']);
		}
		if (isset($_GET['description_etude']) && trim($_GET['description_etude'])!=""){
			$descriptionEtude = trim($_GET['description_etude']);
		}
		if (isset($_GET['nom_notaire']) && trim($_GET['nom_notaire'])!="-1"){
			$nomNotaire = trim($_GET['nom_notaire']);
		}
		if (isset($_GET['domaines_list']) && trim($_GET['domaines_list'])!="-1"){
			$expertisesSectorielles = trim($_GET['domaines_list']);
		}
		if (isset($_GET['expertises_word']) && trim($_GET['expertises_word'])!=""){
			$motCle = trim($_GET['expertises_word']);
		}
		if (isset($_GET['nombre_employes_list']) && trim($_GET['nombre_employes_list'])!="-1"){
			$nombre_yes = trim($_GET['nombre_employes_list']);
		}
		$html =
			"<div class=\"form\">\n".
			"	<form id=\"recherche\" action=\"search.php?action=results\" method=\"get\">\n" .
			"		<input type=\"hidden\" id=\"action\" name=\"action\" value=\"results\" />".
			"		<table class=\"searchTable\">\n" .
			"			<tr>\n".
			"				<td>Le nom de l'&eacute;tude&nbsp;:</td> " .
			"				<td><input type=\"text\" id=\"search_etudes\" name=\"search_etudes\" value=\"".$nom."\" tabindex=\"20\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>L'adresse&nbsp;:</td> " .
			"				<td><input type=\"text\" id=\"adresse\" name=\"adresse\" value=\"".$adresse."\" tabindex=\"21\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>La ville&nbsp;:</td> " .
			"				<td><input type=\"text\" id=\"ville\" name=\"ville\" value=\"".$ville."\" tabindex=\"22\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Le code postal&nbsp;:</td> " .
			"				<td><input type=\"text\" id=\"code_postal\" name=\"code_postal\" value=\"".$codePostal."\" tabindex=\"23\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Le num&eacute;ro de t&eacute;l&eacute;phone&nbsp;:</td> " .
			"				<td><input type=\"text\" id=\"telephone\" name=\"telephone\" value=\"".$telephone."\" tabindex=\"24\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Le num&eacute;ro de t&eacute;l&eacute;copieur&nbsp;:</td> " .
			"				<td><input type=\"text\" id=\"telecopieur\" name=\"telecopieur\" value=\"".$telecopieur."\" tabindex=\"25\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Le courriel de l'&eacute;tude&nbsp;:</td> " .
			"				<td><input type=\"text\" id=\"courriel\" name=\"courriel\" value=\"".$courriel."\" tabindex=\"26\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Le site Web de l'&eacute;tude&nbsp;:</td> " .
			"				<td><input type=\"text\" id=\"site_web\" name=\"site_web\" value=\"".$siteWeb."\" tabindex=\"27\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n" .
			"				<td>Le(s) mot(s)-cl&eacute;(s) se retrouvant dans la description de l'&eacute;tude&nbsp;:</td> " .
			"				<td><input type=\"text\" id=\"description_etude\" name=\"description_etude\" value=\"".$descriptionEtude."\" /></td></td>\n".
			"			</tr>\n" .
			"			<tr>\n".
			"				<td>Un nom de notaire&nbsp;:</td> " .
			"				<td><input type=\"text\" id=\"nom_notaire\" name=\"nom_notaire\" value=\"".$nomNotaire."\" tabindex=\"28\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n" .
			"				<td>Le(s) mot(s)-cl&eacute;(s) se retrouvant dans la description du domaine d'expertises&nbsp;:</td> " .
			"				<td><input type=\"text\" id=\"expertises_word\" name=\"expertises_word\" value=\"".$motCle."\" /></td></td>\n".
			"			</tr>\n" .
			"			<tr>\n".
			"				<td>Le domaine d'expertises&nbsp;:</td> " .
			"				<td>".getExpertisesList($expertisesSectorielles)."</td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Le nombre d'employ&eacute;s&nbsp;:</td> " .
			"				<td>".getNombreEmployesList($nombre_yes)."</td>\n".
			"			</tr>\n".
			"		</table>\n".
			"		<br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"bouton\" tabindex=\"28\" />\n".
			"	</form>\n".
			"</div>".
			"<br /><br />\n";
		return $html;
	}

	function searchResultsString() {
		global $DB;
		$html = searchString();
		$html .= "<h2>R&eacute;sultats de recherche d'&eacute;tudes</h2>\n";
		$compteur = 0;
		$nb = 0;
		$sql = "";
		$queryNom = "";
		$queryExpertise = "";
		$queryAdresse = "";
		$queryVille = "";
		$queryCodePostal = "";
		$queryTelephone = "";
		$queryTelecopieur = "";
		$queryCourriel = "";
		$querySiteWeb = "";
		$queryDescEtude = "";
		$queryNomNotaire = "";

		if (trim($_GET["search_etudes"]) != "") {
			$terms = split(" ", trim($_GET["search_etudes"]));
			$queryNom = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$queryNom .= "AND `etudes`.`nom` LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}
		if (trim($_GET['expertises_word'])!=''){
			$terms = split(" ", trim($_GET["expertises_word"]));
			$queryExpertise = "AND `employes_titres`.`titres_key`='1' ";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$queryExpertise .= "AND CONCAT_WS(' ',  if( `employes`.`description` IS NULL , NULL , `employes`.`description` ),  if( `employes`.`droit_des_affaires_desc` IS NULL , NULL , `employes`.`droit_des_affaires_desc` ),  if( `employes`.`droit_des_personnes_desc` IS NULL , NULL , `employes`.`droit_des_personnes_desc` ),  if( `employes`.`droit_immobilier_desc` IS NULL , NULL , `employes`.`droit_immobilier_desc` ),  if( `employes`.`expertises_sectorielles_desc` IS NULL , NULL , `employes`.`expertises_sectorielles_desc` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		if (trim($_GET['adresse'])!=''){
			$terms = split(" ", trim($_GET["adresse"]));
			$queryAdresse = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$queryAdresse .= "AND CONCAT_WS(' ',  if( `etudes_succursales`.`adresse` IS NULL , NULL , `etudes_succursales`.`adresse` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		if (trim($_GET['ville'])!=''){
			$terms = split(" ", trim($_GET["ville"]));
			$queryVille = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$queryVille .= "AND CONCAT_WS(' ',  if( `etudes_succursales`.`ville` IS NULL , NULL , `etudes_succursales`.`ville` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		if (trim($_GET['code_postal'])!=''){
			$terms = split(" ", trim($_GET["code_postal"]));
			$queryCodePostal = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$queryCodePostal .= "AND CONCAT_WS(' ',  if( `etudes_succursales`.`code_postal` IS NULL , NULL , `etudes_succursales`.`code_postal` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		if (trim($_GET['telephone'])!=''){
			$terms = split(" ", trim($_GET["telephone"]));
			$queryTelephone = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i])) {
					$queryTelephone .= "AND CONCAT_WS(' ',  if( `etudes_succursales`.`telephone1` IS NULL , NULL , `etudes_succursales`.`telephone1` ),  if( `etudes_succursales`.`telephone2` IS NULL , NULL , `etudes_succursales`.`telephone2` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		if (trim($_GET['telecopieur'])!=''){
			$terms = split(" ", trim($_GET["telecopieur"]));
			$queryTelecopieur = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i])) {
					$queryTelecopieur .= "AND CONCAT_WS(' ',  if( `etudes_succursales`.`telecopieur` IS NULL , NULL , `etudes_succursales`.`telecopieur` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		if (trim($_GET['courriel'])!=''){
			$terms = split(" ", trim($_GET["courriel"]));
			$queryCourriel = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$queryCourriel .= "AND CONCAT_WS(' ',  if( `etudes`.`courriel` IS NULL , NULL , `etudes`.`courriel` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		if (trim($_GET['site_web'])!=''){
			$terms = split(" ", trim($_GET["site_web"]));
			$querySiteWeb = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$querySiteWeb .= "AND CONCAT_WS(' ',  if( `etudes`.`url_site_web` IS NULL , NULL , `etudes`.`url_site_web` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		if (trim($_GET['description_etude'])!=''){
			$terms = split(" ", trim($_GET["description_etude"]));
			$queryDescEtude = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$queryDescEtude .= "AND CONCAT_WS(' ',  if( `etudes`.`description` IS NULL , NULL , `etudes`.`description` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		if (trim($_GET['nom_notaire'])!=''){
			$terms = split(" ", trim($_GET["nom_notaire"]));
			$queryNomNotaire = "AND `employes_titres`.`titres_key`='1' ";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$queryNomNotaire .= "AND CONCAT_WS(' ',  if( `employes`.`nom` IS NULL , NULL , `employes`.`nom` ),  if( `employes`.`prenom` IS NULL , NULL , `employes`.`prenom` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		if (isset($_GET['domaines_list']) && trim($_GET['domaines_list'])!="-1"){
			$sql .= "AND `employes_titres`.`titres_key`='1' AND `employes`.`".trim($_GET['domaines_list'])."` IS NOT NULL ";
		}
		if (isset($_GET['nombre_employes_list']) && trim($_GET['nombre_employes_list'])!="-1"){
			$temp = ereg_replace('_', ' ', trim($_GET['nombre_employes_list']));
			$sql .= "AND `etudes`.`nombre_employes` ".$temp." ";
		}
		$query = "SELECT `etudes`.`nom` AS `etude_nom`, `etudes`.`key` AS `etude_key` ".
				"FROM `etudes` " .
				"LEFT JOIN `etudes_succursales` ON `etudes`.`key`=`etudes_succursales`.`etudes_key` ".
				"LEFT JOIN `employes_etudes_succursales` ON `etudes_succursales`.`key`=`employes_etudes_succursales`.`etudes_succursales_key` ".
				"LEFT JOIN `employes` ON `employes_etudes_succursales`.`employes_key`=`employes`.`key` ".
				"LEFT JOIN `employes_titres` ON `employes`.`key`=`employes_titres`.`employes_key` " .
				"WHERE `etudes`.`actif` = '1' ".
				(trim($_GET["search_etudes"]) != "" ? $queryNom : "").
				(trim($_GET["expertises_word"]) != "" ? $queryExpertise : "").
				(trim($_GET["adresse"]) != "" ? $queryAdresse : "").
				(trim($_GET["ville"]) != "" ? $queryVille : "").
				(trim($_GET["code_postal"]) != "" ? $queryCodePostal : "").
				(trim($_GET["telephone"]) != "" ? $queryTelephone : "").
				(trim($_GET["telecopieur"]) != "" ? $queryTelecopieur : "").
				(trim($_GET["courriel"]) != "" ? $queryCourriel : "").
				(trim($_GET["site_web"]) != "" ? $querySiteWeb : "").
				(trim($_GET["description_etude"]) != "" ? $queryDescEtude : "").
				(trim($_GET["nom_notaire"]) != "" ? $queryNomNotaire : "").
				$sql.
				"GROUP BY `etudes`.`key` ".
				"ORDER BY `etudes`.`nom` ASC "
		; 

		$DB->query(
			$query
			);
		$nb = $DB->getNumRows();

		if ($nb>0){
			$html .=
				"<table class=\"resultsTable\">\n" .
				"	<tr>\n".
				"	<th>Nom de l'&eacute;tude </th><th>&nbsp;</th><th>&nbsp;</th>\n".
				"	</tr>\n";
		}
			
		while ($DB->next_record()) {
			$key = $DB->getField("key");
			$color = "";
			if ($compteur%2==0) {
				$color = " class=\"altTr\"";			
			}
			$html .=
			"    <tr". $color .">\n<td><a href=\"etudes.php?action=read&begin=1&key=" . $DB->getField("etude_key") . "\" title=\"Pour visualiser\">".$DB->getField("etude_nom"). "</a></td>\n" .
					"<td>&nbsp;</td>\n" .
					"<td><a href=\"etudes.php?action=read&begin=1&key=" . $DB->getField("etude_key") . "\" title=\"Pour visualiser\">Visualiser</a> | <a href=\"etudes.php?action=modify&begin=1&key=" . $DB->getField("etude_key") . "\" title=\"Pour modifier\">Modifier</a></td>\n".
			"	</tr>\n";
			$compteur++;
		}
		if ($nb==0) {
			$html .= "<p>Aucune &eacute;tude ne correspond &agrave; votre requ&ecirc;te...</p>";
		}
		else if ($nb>0) {
			$html .=
				"</table>";			
		}
	
		return $html;
	}
?>
