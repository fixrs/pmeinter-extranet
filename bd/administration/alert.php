<?PHP
	global $DB;

	require "../conf/conf.inc.php";
	import("com.quiboweb.email.Email");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	
	ob_start();
	require_once("../ratios/excel.php");
   	$debug = ob_get_contents();
   	ob_end_clean();
	
	echo $debug;
	if (trim($debug) != "") {
		$email = new Email("info@pmeinter.com", "lvincent@notarius.net", "Nouveau fichier de ratios de gestion : " . date("Y-m-d"), $debug);
		$email->addHTMLContent(nl2br($debug));
		$email->build();
		$email->send();
	}

	$debug = "";

	$DB->close();

?>