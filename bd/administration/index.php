<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
		$Skin->assign("errors", $html);
		$Skin->display("administration.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
		$Skin->assign("errors", $html);
		$Skin->display("administration.tpl");
		exit;	
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);

	
	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$html = "";
 
	$html .= menuAdminString();

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
	$Skin->assign("page", "administration");
	$Skin->assign("title", "PME Inter : Administration");
	$Skin->assign("content", $html);
	$Skin->display("administration.tpl");
	
	$DB->close();


	// -----------------
	// LES SOUS-ROUTINES
	// -----------------

	function menuAdminString() { 
		global $BASEURL, $DB;
		$html =
			"<h1>Console d'Administration</h1>\n".
			"<h2>Veuillez choisir une cat&eacute;gorie de gestion</h2>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n";

			if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
				$html .= "<li><a href=\"" . $BASEURL . "utilisateurs/index.php\">Gérer les utilisateurs</a></li>\n";
			}

			if ($_SESSION['user_type'] == 1) {
#				$html .= "<li><a href=\"" . $BASEURL . "administration/backup_db.php\">Gérer la base de données</a></li>\n";
				$html .= "<li>Gérer la base de données</li>\n";
			}
			
			if ($_SESSION['user_type'] == 1) {
#				$html .= "<li><a href=\"" . $BASEURL . "administration/backup_docs.php\">Gérer les documents</a></li>\n";
				$html .= "<li>Gérer les documents</li>\n";
			}			
			
			if ($_SESSION['user_type'] == 1) {
				$html .= "<li><a href=\"" . $BASEURL . "administration/outlook.php\">Exporter une liste des employ&eacute;s pour Outlook</a></li>\n";
				$html .= "<li><a href=\"" . $BASEURL . "administration/outlook.php?droit=1\">Exporter une liste des notaires et avocats pour Outlook</a></li>\n";
				$html .= "<li><a href=\"" . $BASEURL . "administration/outlook.php?droit=0\">Exporter une liste des employ&eacute;s autre que notaires et avocats pour Outlook</a></li>\n";

				$DB->query("SELECT * FROM directions_de_travail");

				while($DB->next_record()) {
					$html .= "<li><a href=\"" . $BASEURL . "administration/outlook.php?comite=" . $DB->getField("key") . "\">Exporter une liste Outlook : " . $DB->getField("nom") . "</a></li>\n";				
				}

				
#				$html .= "<li>Exporter une liste des employeurs pour Outlook</li>\n";
			}

		$html .=
			"	</ul>\n".
			"</div>\n";

		return $html;
	}

?>
