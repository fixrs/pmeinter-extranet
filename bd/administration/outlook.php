<?PHP
	require "../conf/conf.inc.php";

	session_start();

	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
		$Skin->assign("errors", $html);
		$Skin->display("administration.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
		$Skin->assign("errors", $html);
		$Skin->display("administration.tpl");
		exit;	
	}

	
	$path = str_replace("outlook.php", "", __FILE__);
	
	/** PHPExcel */
	include '../ratios/PHPExcel.php';
	require_once '../ratios/PHPExcel/IOFactory.php';

	$objPHPExcel = new PHPExcel();

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);

	$sql =	
		"SELECT `prenom`, `nom`, `courriel`, `ddn_a`, `ddn_m`, `ddn_j`, `adresse`, `ville`, `province`, `code_postal`, `telephone1`, `telephone2`, `telecopieur`, `courriel_etudes_succursales`, " .
		"( SELECT `abbreviation_m` " .
			"FROM `titres` " .
			"WHERE `key`=" .
			"( SELECT `titres_key` " .
				"FROM `employes_titres` " .
				"WHERE `employes_key`=`employes`.`key` " .
				"LIMIT 1 " .
			") LIMIT 1 " .
		") \"titre\", " .
		"( SELECT `nom` " .
			"FROM `fonctions` " .
			"WHERE `key`=" .
			"( SELECT `fonctions_key` " .
				"FROM `employes_fonctions` " .
				"WHERE `employes_key`=`employes`.`key` " .
				"LIMIT 1 " .
			") LIMIT 1 " .
		") \"fonction\" " .
		"FROM `employes` INNER JOIN employes_etudes_succursales ON employes_etudes_succursales.employes_key = employes.key ".
		"INNER JOIN etudes_succursales ON employes_etudes_succursales.etudes_succursales_key = etudes_succursales.key";

	if ($_GET["comite"] != "") {
		$sql .=
			" ".
			"INNER JOIN `employes_directions_de_travail` edt ON edt.directions_de_travail_key = '" . $_GET["comite"] . "' AND edt.employes_key = employes.key ".
			"WHERE employes.actif = '1' ";
	} else {
		$sql .=
			" ".
			"WHERE employes.actif = '1' ";
	}









	header('Content-Type: application/vnd.ms-excel');
	header('Content-Disposition: attachment;filename="contacts.xls');
	header('Cache-Control: max-age=0');

	$objPHPExcel->setActiveSheetIndex(0);

	$objPHPExcel->getActiveSheet()->SetCellValue("A1", "Titre");
	$objPHPExcel->getActiveSheet()->SetCellValue("B1", "Prénom");
	$objPHPExcel->getActiveSheet()->SetCellValue("C1", "Deuxièmeprénom");
	$objPHPExcel->getActiveSheet()->SetCellValue("D1", "Nom");
	$objPHPExcel->getActiveSheet()->SetCellValue("E1", "Suffixe");
	$objPHPExcel->getActiveSheet()->SetCellValue("F1", "Société");
	$objPHPExcel->getActiveSheet()->SetCellValue("G1", "Service");
	$objPHPExcel->getActiveSheet()->SetCellValue("H1", "Titre1");
	$objPHPExcel->getActiveSheet()->SetCellValue("I1", "Ruebureau");
	$objPHPExcel->getActiveSheet()->SetCellValue("J1", "Ruebureau2");
	$objPHPExcel->getActiveSheet()->SetCellValue("K1", "Ruebureau3");
	$objPHPExcel->getActiveSheet()->SetCellValue("L1", "Villebureau");
	$objPHPExcel->getActiveSheet()->SetCellValue("M1", "DépRégionbureau");
	$objPHPExcel->getActiveSheet()->SetCellValue("N1", "Codepostalbureau");
	$objPHPExcel->getActiveSheet()->SetCellValue("O1", "PaysRégionbureau");
	$objPHPExcel->getActiveSheet()->SetCellValue("P1", "Ruedomicile");
	$objPHPExcel->getActiveSheet()->SetCellValue("Q1", "Ruedomicile2");
	$objPHPExcel->getActiveSheet()->SetCellValue("R1", "Ruedomicile3");
	$objPHPExcel->getActiveSheet()->SetCellValue("S1", "Villedomicile");
	$objPHPExcel->getActiveSheet()->SetCellValue("T1", "DépRégiondomicile");
	$objPHPExcel->getActiveSheet()->SetCellValue("U1", "Codepostaldomicile");
	$objPHPExcel->getActiveSheet()->SetCellValue("V1", "PaysRégiondomicile");
	$objPHPExcel->getActiveSheet()->SetCellValue("W1", "Rueautre");
	$objPHPExcel->getActiveSheet()->SetCellValue("X1", "Rueautre2");
	$objPHPExcel->getActiveSheet()->SetCellValue("Y1", "Rueautre3");
	$objPHPExcel->getActiveSheet()->SetCellValue("Z1", "Villeautre");
	$objPHPExcel->getActiveSheet()->SetCellValue("AA1", "DépRégionautre");
	$objPHPExcel->getActiveSheet()->SetCellValue("AB1", "Codepostalautre");
	$objPHPExcel->getActiveSheet()->SetCellValue("AC1", "PaysRégionautre");
	$objPHPExcel->getActiveSheet()->SetCellValue("AD1", "Téléphonedelassistante");
	$objPHPExcel->getActiveSheet()->SetCellValue("AE1", "Télécopiebureau");
	$objPHPExcel->getActiveSheet()->SetCellValue("AF1", "Téléphonebureau");
	$objPHPExcel->getActiveSheet()->SetCellValue("AG1", "Téléphone2bureau");
	$objPHPExcel->getActiveSheet()->SetCellValue("AH1", "Rappel");
	$objPHPExcel->getActiveSheet()->SetCellValue("AI1", "Téléphonevoiture");
	$objPHPExcel->getActiveSheet()->SetCellValue("AJ1", "Téléphonesociété");
	$objPHPExcel->getActiveSheet()->SetCellValue("AK1", "Télécopiedomicile");
	$objPHPExcel->getActiveSheet()->SetCellValue("AL1", "Téléphonedomicile");
	$objPHPExcel->getActiveSheet()->SetCellValue("AM1", "Téléphone2domicile");
	$objPHPExcel->getActiveSheet()->SetCellValue("AN1", "RNIS");
	$objPHPExcel->getActiveSheet()->SetCellValue("AO1", "Télmobile");
	$objPHPExcel->getActiveSheet()->SetCellValue("AP1", "Télécopieautre");
	$objPHPExcel->getActiveSheet()->SetCellValue("AQ1", "Téléphoneautre");
	$objPHPExcel->getActiveSheet()->SetCellValue("AR1", "Récepteurderadiomessagerie");
	$objPHPExcel->getActiveSheet()->SetCellValue("AS1", "Téléphoneprincipal");
	$objPHPExcel->getActiveSheet()->SetCellValue("AT1", "Radiotéléphone");
	$objPHPExcel->getActiveSheet()->SetCellValue("AU1", "TéléphoneTDDTTY");
	$objPHPExcel->getActiveSheet()->SetCellValue("AV1", "Télex");
	$objPHPExcel->getActiveSheet()->SetCellValue("AW1", "Adressedemessagerie");
	$objPHPExcel->getActiveSheet()->SetCellValue("AX1", "Typedemessagerie");
	$objPHPExcel->getActiveSheet()->SetCellValue("AY1", "Nomcompletdeladressedemessagerie");
	$objPHPExcel->getActiveSheet()->SetCellValue("AZ1", "Adressedemessagerie2");
	$objPHPExcel->getActiveSheet()->SetCellValue("BA1", "Typedemessagerie2");
	$objPHPExcel->getActiveSheet()->SetCellValue("BB1", "Nomcompletdeladressedemessagerie2");
	$objPHPExcel->getActiveSheet()->SetCellValue("BC1", "Adressedemessagerie3");
	$objPHPExcel->getActiveSheet()->SetCellValue("BD1", "Typedemessagerie3");
	$objPHPExcel->getActiveSheet()->SetCellValue("BE1", "Nomcompletdeladressedemessagerie3");
	$objPHPExcel->getActiveSheet()->SetCellValue("BF1", "Anniversaire");
	$objPHPExcel->getActiveSheet()->SetCellValue("BG1", "Anniversairedemariageoufête");
	$objPHPExcel->getActiveSheet()->SetCellValue("BH1", "Autreboîtepostale");
	$objPHPExcel->getActiveSheet()->SetCellValue("BI1", "BPprofessionnelle");
	$objPHPExcel->getActiveSheet()->SetCellValue("BJ1", "Boîtepostaledudomicile");
	$objPHPExcel->getActiveSheet()->SetCellValue("BK1", "Bureau");
	$objPHPExcel->getActiveSheet()->SetCellValue("BL1", "Catégories");
	$objPHPExcel->getActiveSheet()->SetCellValue("BM1", "Codegouvernement");
	$objPHPExcel->getActiveSheet()->SetCellValue("BN1", "Compte");
	$objPHPExcel->getActiveSheet()->SetCellValue("BO1", "Conjointe");
	$objPHPExcel->getActiveSheet()->SetCellValue("BP1", "Critèredediffusion");
	$objPHPExcel->getActiveSheet()->SetCellValue("BQ1", "DisponibilitéInternet");
	$objPHPExcel->getActiveSheet()->SetCellValue("BR1", "Emplacement");
	$objPHPExcel->getActiveSheet()->SetCellValue("BS1", "Enfants");
	$objPHPExcel->getActiveSheet()->SetCellValue("BT1", "Informationsfacturation");
	$objPHPExcel->getActiveSheet()->SetCellValue("BU1", "Initiales");
	$objPHPExcel->getActiveSheet()->SetCellValue("BV1", "Kilométrage");
	$objPHPExcel->getActiveSheet()->SetCellValue("BW1", "Langue");
	$objPHPExcel->getActiveSheet()->SetCellValue("BX1", "Motsclés");
	$objPHPExcel->getActiveSheet()->SetCellValue("BY1", "Nomdelassistante");
	$objPHPExcel->getActiveSheet()->SetCellValue("BZ1", "Notes");
	$objPHPExcel->getActiveSheet()->SetCellValue("CA1", "Numérodidentificationdelorganisation");
	$objPHPExcel->getActiveSheet()->SetCellValue("CB1", "PageWeb");
	$objPHPExcel->getActiveSheet()->SetCellValue("CC1", "Passetemps");
	$objPHPExcel->getActiveSheet()->SetCellValue("CD1", "Priorité");
	$objPHPExcel->getActiveSheet()->SetCellValue("CE1", "Privé");
	$objPHPExcel->getActiveSheet()->SetCellValue("CF1", "Profession");
	$objPHPExcel->getActiveSheet()->SetCellValue("CG1", "Recommandépar");
	$objPHPExcel->getActiveSheet()->SetCellValue("CH1", "Responsable");
	$objPHPExcel->getActiveSheet()->SetCellValue("CI1", "Serveurdannuaire");
	$objPHPExcel->getActiveSheet()->SetCellValue("CJ1", "Sexe");
	$objPHPExcel->getActiveSheet()->SetCellValue("CK1", "Utilisateur1");
	$objPHPExcel->getActiveSheet()->SetCellValue("CL1", "Utilisateur2");
	$objPHPExcel->getActiveSheet()->SetCellValue("CM1", "Utilisateur3");
	$objPHPExcel->getActiveSheet()->SetCellValue("CN1", "Utilisateur4");


	$DB->query( $sql );
	
	$iterator = 1;

	$emails = array();

	while( $DB->next_record( ) ) {
		
		if ($_GET["droit"] == 1) {
			if (!preg_match("/(notaire|avocat)/i", $DB->getField("fonction"))) {
				continue;
			}
		} else if (isset($_GET["droit"]) && $_GET["droit"] == 0) {
			if (preg_match("/(notaire|avocat)/i", $DB->getField("fonction"))) {
				continue;
			}
		}

		$titre = str_replace(",", " ", $DB->getField( "titre" ));
		$fonction = str_replace(",", " ", $DB->getField( "fonction" ));
		$prenom = str_replace(",", " ", $DB->getField( "prenom" ));
		$nom = str_replace(",", " ", $DB->getField( "nom" ));
		$courriel = str_replace(",", " ", $DB->getField( "courriel" ));

		if (isset($emails[$courriel])) {
			continue;
		} else {
			$emails[$courriel] = 1;
		}

		$fete = str_replace(",", " ", $DB->getField( "ddn_a" ) . "-" . $DB->getField( "ddn_m" ) . "-" . $DB->getField( "ddn_j" ));

		$adresse = str_replace(",", " ", $DB->getField( "adresse" ));
		$ville = str_replace(",", " ", $DB->getField( "ville" ));
		$province = str_replace(",", " ", $DB->getField( "province" ));
		$code_postal = str_replace(",", " ", $DB->getField( "code_postal" ));
		$telephone1 = str_replace(",", " ", $DB->getField( "telephone1" ));
		$telephone2 = str_replace(",", " ", $DB->getField( "telephone2" ));
		$telecopieur = str_replace(",", " ", $DB->getField( "telecopieur" ));
		$courriel_etudes_succursales = str_replace(",", " ", $DB->getField( "courriel_etudes_succursales" ));

		$iterator++;

		$objPHPExcel->getActiveSheet()->SetCellValue("A" . $iterator, $fonction);
		$objPHPExcel->getActiveSheet()->SetCellValue("B" . $iterator, $prenom);
		//$objPHPExcel->getActiveSheet()->SetCellValue("C" . $iterator, "Deuxièmeprénom");
		$objPHPExcel->getActiveSheet()->SetCellValue("D" . $iterator, $nom);
		$objPHPExcel->getActiveSheet()->SetCellValue("E" . $iterator, $titre);
		//$objPHPExcel->getActiveSheet()->SetCellValue("F" . $iterator, "Société");
		//$objPHPExcel->getActiveSheet()->SetCellValue("G" . $iterator, "Service");
		//$objPHPExcel->getActiveSheet()->SetCellValue("H" . $iterator, "Titre1");
		$objPHPExcel->getActiveSheet()->SetCellValue("I" . $iterator, $adresse);
		//$objPHPExcel->getActiveSheet()->SetCellValue("J" . $iterator, "Ruebureau2");
		//$objPHPExcel->getActiveSheet()->SetCellValue("K" . $iterator, "Ruebureau3");
		$objPHPExcel->getActiveSheet()->SetCellValue("L" . $iterator, $ville);
		$objPHPExcel->getActiveSheet()->SetCellValue("M" . $iterator, $province);
		$objPHPExcel->getActiveSheet()->SetCellValue("N" . $iterator, $code_postal);
		$objPHPExcel->getActiveSheet()->SetCellValue("O" . $iterator, "Canada");
		//$objPHPExcel->getActiveSheet()->SetCellValue("P" . $iterator, "Ruedomicile");
		//$objPHPExcel->getActiveSheet()->SetCellValue("Q" . $iterator, "Ruedomicile2");
		//$objPHPExcel->getActiveSheet()->SetCellValue("R" . $iterator, "Ruedomicile3");
		//$objPHPExcel->getActiveSheet()->SetCellValue("S" . $iterator, "Villedomicile");
		//$objPHPExcel->getActiveSheet()->SetCellValue("T" . $iterator, "DépRégiondomicile");
		//$objPHPExcel->getActiveSheet()->SetCellValue("U" . $iterator, "Codepostaldomicile");
		//$objPHPExcel->getActiveSheet()->SetCellValue("V" . $iterator, "PaysRégiondomicile");
		//$objPHPExcel->getActiveSheet()->SetCellValue("W" . $iterator, "Rueautre");
		//$objPHPExcel->getActiveSheet()->SetCellValue("X" . $iterator, "Rueautre2");
		//$objPHPExcel->getActiveSheet()->SetCellValue("Y" . $iterator, "Rueautre3");
		//$objPHPExcel->getActiveSheet()->SetCellValue("Z" . $iterator, "Villeautre");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AA" . $iterator, "DépRégionautre");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AB" . $iterator, "Codepostalautre");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AC" . $iterator, "PaysRégionautre");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AD" . $iterator, "Téléphonedelassistante");
		$objPHPExcel->getActiveSheet()->SetCellValue("AE" . $iterator, $telecopieur);
		$objPHPExcel->getActiveSheet()->SetCellValue("AF" . $iterator, $telephone1);
		$objPHPExcel->getActiveSheet()->SetCellValue("AG" . $iterator, $telephone2);
		//$objPHPExcel->getActiveSheet()->SetCellValue("AH" . $iterator, "Rappel");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AI" . $iterator, "Téléphonevoiture");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AJ" . $iterator, "Téléphonesociété");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AK" . $iterator, "Télécopiedomicile");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AL" . $iterator, "Téléphonedomicile");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AM" . $iterator, "Téléphone2domicile");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AN" . $iterator, "RNIS");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AO" . $iterator, "Télmobile");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AP" . $iterator, "Télécopieautre");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AQ" . $iterator, "Téléphoneautre");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AR" . $iterator, "Récepteurderadiomessagerie");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AS" . $iterator, "Téléphoneprincipal");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AT" . $iterator, "Radiotéléphone");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AU" . $iterator, "TéléphoneTDDTTY");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AV" . $iterator, "Télex");
		$objPHPExcel->getActiveSheet()->SetCellValue("AW" . $iterator, $courriel);
		$objPHPExcel->getActiveSheet()->SetCellValue("AX" . $iterator, "SMTP");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AY" . $iterator, "Nomcompletdeladressedemessagerie");
		//$objPHPExcel->getActiveSheet()->SetCellValue("AZ" . $iterator, "Adressedemessagerie2");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BA" . $iterator, "Typedemessagerie2");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BB" . $iterator, "Nomcompletdeladressedemessagerie2");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BC" . $iterator, "Adressedemessagerie3");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BD" . $iterator, "Typedemessagerie3");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BE" . $iterator, "Nomcompletdeladressedemessagerie3");
		$objPHPExcel->getActiveSheet()->SetCellValue("BF" . $iterator, $fete);
		//$objPHPExcel->getActiveSheet()->SetCellValue("BG" . $iterator, "Anniversairedemariageoufête");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BH" . $iterator, "Autreboîtepostale");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BI" . $iterator, "BPprofessionnelle");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BJ" . $iterator, "Boîtepostaledudomicile");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BK" . $iterator, "Bureau");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BL" . $iterator, "Catégories");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BM" . $iterator, "Codegouvernement");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BN" . $iterator, "Compte");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BO" . $iterator, "Conjointe");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BP" . $iterator, "Critèredediffusion");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BQ" . $iterator, "DisponibilitéInternet");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BR" . $iterator, "Emplacement");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BS" . $iterator, "Enfants");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BT" . $iterator, "Informationsfacturation");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BU" . $iterator, "Initiales");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BV" . $iterator, "Kilométrage");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BW" . $iterator, "Langue");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BX" . $iterator, "Motsclés");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BY" . $iterator, "Nomdelassistante");
		//$objPHPExcel->getActiveSheet()->SetCellValue("BZ" . $iterator, "Notes");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CA" . $iterator, "Numérodidentificationdelorganisation");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CB" . $iterator, "PageWeb");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CC" . $iterator, "Passetemps");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CD" . $iterator, "Priorité");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CE" . $iterator, "Privé");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CF" . $iterator, "Profession");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CG" . $iterator, "Recommandépar");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CH" . $iterator, "Responsable");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CI" . $iterator, "Serveurdannuaire");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CJ" . $iterator, "Sexe");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CK" . $iterator, "Utilisateur1");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CL" . $iterator, "Utilisateur2");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CM" . $iterator, "Utilisateur3");
		//$objPHPExcel->getActiveSheet()->SetCellValue("CN" . $iterator, "Utilisateur4");
		
	}

	$objPHPExcel->addNamedRange( 
	    new PHPExcel_NamedRange(
	        'Contacts', 
	        $objPHPExcel->getActiveSheet(), 
	        'A1:A' . $iterator . ',B1:B' . $iterator . ',C1:C' . $iterator . ',D1:D' . $iterator . ',E1:E' . $iterator . ',F1:F' . $iterator . ',G1:G' . $iterator . ',H1:H' . $iterator . ',I1:I' . $iterator . ',J1:J' . $iterator . ',K1:K' . $iterator . ',L1:L' . $iterator . ',M1:M' . $iterator . ',N1:N' . $iterator . ',O1:O' . $iterator . ',P1:P' . $iterator . ',Q1:Q' . $iterator . ',R1:R' . $iterator . ',S1:S' . $iterator . ',T1:T' . $iterator . ',U1:U' . $iterator . ',V1:V' . $iterator . ',W1:W' . $iterator . ',X1:X' . $iterator . ',Y1:Y' . $iterator . ',Z1:Z' . $iterator . ',AA1:AA' . $iterator . ',AB1:AB' . $iterator . ',AC1:AC' . $iterator . ',AD1:AD' . $iterator . ',AE1:AE' . $iterator . ',AF1:AF' . $iterator . ',AG1:AG' . $iterator . ',AH1:AH' . $iterator . ',AI1:AI' . $iterator . ',AJ1:AJ' . $iterator . ',AK1:AK' . $iterator . ',AL1:AL' . $iterator . ',AM1:AM' . $iterator . ',AN1:AN' . $iterator . ',AO1:AO' . $iterator . ',AP1:AP' . $iterator . ',AQ1:AQ' . $iterator . ',AR1:AR' . $iterator . ',AS1:AS' . $iterator . ',AT1:AT' . $iterator . ',AU1:AU' . $iterator . ',AV1:AV' . $iterator . ',AW1:AW' . $iterator . ',AX1:AX' . $iterator . ',AY1:AY' . $iterator . ',AZ1:AZ' . $iterator . ',BA1:BA' . $iterator . ',BB1:BB' . $iterator . ',BC1:BC' . $iterator . ',BD1:BD' . $iterator . ',BE1:BE' . $iterator . ',BF1:BF' . $iterator . ',BG1:BG' . $iterator . ',BH1:BH' . $iterator . ',BI1:BI' . $iterator . ',BJ1:BJ' . $iterator . ',BK1:BK' . $iterator . ',BL1:BL' . $iterator . ',BM1:BM' . $iterator . ',BN1:BN' . $iterator . ',BO1:BO' . $iterator . ',BP1:BP' . $iterator . ',BQ1:BQ' . $iterator . ',BR1:BR' . $iterator . ',BS1:BS' . $iterator . ',BT1:BT' . $iterator . ',BU1:BU' . $iterator . ',BV1:BV' . $iterator . ',BW1:BW' . $iterator . ',BX1:BX' . $iterator . ',BY1:BY' . $iterator . ',BZ1:BZ' . $iterator . ',CA1:CA' . $iterator . ',CB1:CB' . $iterator . ',CC1:CC' . $iterator . ',CD1:CD' . $iterator . ',CE1:CE' . $iterator . ',CF1:CF' . $iterator . ',CG1:CG' . $iterator . ',CH1:CH' . $iterator . ',CI1:CI' . $iterator . ',CJ1:CJ' . $iterator . ',CK1:CK' . $iterator . ',CL1:CL' . $iterator . ',CM1:CM' . $iterator . ',CN1:CN' . $iterator
	    ) 
	);


	//echo 'A1:A' . $iterator . ',B1:B' . $iterator . ',C1:C' . $iterator . ',D1:D' . $iterator . ',E1:E' . $iterator . ',F1:F' . $iterator . ',G1:G' . $iterator . ',H1:H' . $iterator . ',I1:I' . $iterator . ',J1:J' . $iterator . ',K1:K' . $iterator . ',L1:L' . $iterator . ',M1:M' . $iterator . ',N1:N' . $iterator . ',O1:O' . $iterator . ',P1:P' . $iterator . ',Q1:Q' . $iterator . ',R1:R' . $iterator . ',S1:S' . $iterator . ',T1:T' . $iterator . ',U1:U' . $iterator . ',V1:V' . $iterator . ',W1:W' . $iterator . ',X1:X' . $iterator . ',Y1:Y' . $iterator . ',Z1:Z' . $iterator . ',AA1:AA' . $iterator . ',AB1:AB' . $iterator . ',AC1:AC' . $iterator . ',AD1:AD' . $iterator . ',AE1:AE' . $iterator . ',AF1:AF' . $iterator . ',AG1:AG' . $iterator . ',AH1:AH' . $iterator . ',AI1:AI' . $iterator . ',AJ1:AJ' . $iterator . ',AK1:AK' . $iterator . ',AL1:AL' . $iterator . ',AM1:AM' . $iterator . ',AN1:AN' . $iterator . ',AO1:AO' . $iterator . ',AP1:AP' . $iterator . ',AQ1:AQ' . $iterator . ',AR1:AR' . $iterator . ',AS1:AS' . $iterator . ',AT1:AT' . $iterator . ',AU1:AU' . $iterator . ',AV1:AV' . $iterator . ',AW1:AW' . $iterator . ',AX1:AX' . $iterator . ',AY1:AY' . $iterator . ',AZ1:AZ' . $iterator . ',BA1:BA' . $iterator . ',BB1:BB' . $iterator . ',BC1:BC' . $iterator . ',BD1:BD' . $iterator . ',BE1:BE' . $iterator . ',BF1:BF' . $iterator . ',BG1:BG' . $iterator . ',BH1:BH' . $iterator . ',BI1:BI' . $iterator . ',BJ1:BJ' . $iterator . ',BK1:BK' . $iterator . ',BL1:BL' . $iterator . ',BM1:BM' . $iterator . ',BN1:BN' . $iterator . ',BO1:BO' . $iterator . ',BP1:BP' . $iterator . ',BQ1:BQ' . $iterator . ',BR1:BR' . $iterator . ',BS1:BS' . $iterator . ',BT1:BT' . $iterator . ',BU1:BU' . $iterator . ',BV1:BV' . $iterator . ',BW1:BW' . $iterator . ',BX1:BX' . $iterator . ',BY1:BY' . $iterator . ',BZ1:BZ' . $iterator . ',CA1:CA' . $iterator . ',CB1:CB' . $iterator . ',CC1:CC' . $iterator . ',CD1:CD' . $iterator . ',CE1:CE' . $iterator . ',CF1:CF' . $iterator . ',CG1:CG' . $iterator . ',CH1:CH' . $iterator . ',CI1:CI' . $iterator . ',CJ1:CJ' . $iterator . ',CK1:CK' . $iterator . ',CL1:CL' . $iterator . ',CM1:CM' . $iterator . ',CN1:CN' . $iterator;

	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter->save('php://output');

	$DB->close();

	
?>