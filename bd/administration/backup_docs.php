<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
		$Skin->assign("errors", $html);
		$Skin->display("backup_docs.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
		$Skin->assign("errors", $html);
		$Skin->display("backup_docs.tpl");
		exit;	
	}

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);

	
	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$action = getorpost("action");

	$content = "";
	$feedback = "";

	switch ($action) {
		case "backup":
			$backupZipFile = backupDb();
			if ($backupZipFile != "") {
				$feedback .= _success("La base de donn&eacute;es actuelle a &eacute;t&eacute; sauvegard&eacute;e avec succ&egrave;s.");
			} else {
				$feedback .= _error("Une erreur s'est produite lors de copie de sauvegarde.");
			}
			
			break;
		
		case "restore":
			if ($backupZipFile != "") {
				if (restoreDb($backupZipFile)) {
					$feedback .= _success("La copie de sauvegarde (" . $backupZipFile . ") a &eacute;t&eacute; restor&eacute;e avec succ&egrave;s.");
				} else {
					$feedback .= _error("Une erreur s'est produite lors de la restoration de la copie de sauvegarde (" . $backupZipFile . ").");
					$backupZipFile = "";
				}	
			} else {
				$feedback .= _error("Veuillez choisir une copie de sauvegarde &agrave; restorer.");
			}
			break;
		
		case "download":
			if ($backupZipFile != "") {
				sendBackup($backupZipFile);
				$backupZipFile = "";
			} else {
				$feedback .= _error("Veuillez choisir une copie de sauvegarde &agrave; t&eacute;l&eacute;charger.");
			}
			break;
	}

	if ($feedback != "") {
		$feedback .= "<br />\n";
	}

	$backupZipFilesArray = getBackupZipFilesArray();

	$content = getContentString($backupZipFilesArray, $backupZipFile);

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
	$Skin->assign("page", "backup_docs");
	$Skin->assign("title", "PME Inter : Administration");
	$Skin->assign("content", $content);
	$Skin->assign("feedback", $feedback);
	$Skin->display("backup_docs.tpl");


	// -----------------
	// LES SOUS-ROUTINES
	// -----------------

	function getContentString($backupZipFilesArray = "", $backupZipFile = "") { 
		global $BASEURL;

		$html =
			getJavaScriptString();

		if (is_array($backupZipFilesArray) && count($backupZipFilesArray) > 0) {
			$html .=
				getLatestBackupDateString($backupZipFilesArray);
		} else {
			$html .=
				"Aucune copie de sauvegarde n'a &eacute;t&eacute; effectu&eacute;e.<br /><br />\n";
		}

		$html .=
			"<form name=\"backup_docs\" action=\"backup_docs.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" id=\"action\" value=\"\">\n".
			"	Cr&eacute;er une copie de sauvegarde maintenant&nbsp;: <input name=\"backup\" id=\"backup\" type=\"button\" value=\"Sauvegarder\" onclick=\"javascript: return submitAction('backup')\" /><br />\n".
			"	<br />\n";

		if (is_array($backupZipFilesArray) && count($backupZipFilesArray) > 0) {
			$html .=
				getBackupZipFilesString($backupZipFilesArray, $backupZipFile);
		}

		if (is_array($backupZipFilesArray) && count($backupZipFilesArray) > 0) {
			$html .=
				"	<br />\n".
				"	<input name=\"restore\" id=\"restore\" type=\"button\" value=\"Restaurer\" onclick=\"javascript: return submitAction('restore');\" />\n".
				"	&nbsp;&nbsp;|&nbsp;&nbsp;\n".
				"	<input name=\"download\" id=\"download\" type=\"button\" value=\"T&eacute;l&eacute;charger\" onclick=\"javascript: return submitAction('download');\" />\n";
		}

		$html .=
			"</form>\n";

		return $html;
	}

	function getBackupZipFilesString($backupZipFilesArray = "", $latestBackupZipFile = "") { 
		global $BASEURL, $BASEPATH;

		$html =
			"<table class=\"backup_list\">\n";

		if (is_array($backupZipFilesArray) && count($backupZipFilesArray) > 0) {
			foreach ($backupZipFilesArray as $file => $info) {
				$name = $info['name'];
				$date = $info['date'];
				$size = round($info['size'] / 1048576, 2) . " MB";

				if ($info['name'] == $latestBackupZipFile) {
					$html .=
						"<tr class=\"latest\">\n";
				} else {
					$html .=
						"<tr>\n";
				}

				$html .=
					"	<td>" . $name . "</td>\n".
					"	<td>" . $size . "</td>\n".
					"	<td>" . $date . "</td>\n".
					"	<td style=\"text-align:center;\"><input class=\"radio\" name=\"backupZipFile\" id=\"backupZipFile_" . $name . "\" type=\"radio\" \" value=\"" . $name . "\"></td>\n".
					"</tr>\n";
			}
		}

		$html .=
			"</table>\n";

		return $html;
	}

	function getBackupZipFilesArray() {
		global $BASEPATH;

		$dirPath = $BASEPATH . "backup";
		$dirHandle = @opendir($dirPath) or die("Impossible d'ouvrir <em>" . $dirPath . "</em>");
		
		$backupZipFilesArray = array();
		while ($fileName = readdir($dirHandle)) {
			if (substr($fileName, 0, 10) == "db_backup_" && $fileName != "." && $fileName != ".." && substr($fileName, -4) == ".zip") {
				$backupZipFilesArray[$fileName] = array();
				$backupZipFilesArray[$fileName]['name'] = $fileName;
				$backupZipFilesArray[$fileName]['path'] = $BASEPATH . "backup/" . $fileName;
				$backupZipFilesArray[$fileName]['size'] = filesize($backupZipFilesArray[$fileName]['path']);
				$backupZipFilesArray[$fileName]['date'] = date("Y-m-d H:i:s", mktime(substr($fileName, 16, 2), substr($fileName, 18, 2), substr($fileName, 20, 2), substr($fileName, 12, 2), substr($fileName, 14, 2), substr($fileName, 10, 2)));
			}
		}

		closedir($dirHandle);

		return $backupZipFilesArray;
	}

	function backupDb() {
		global $DB, $DB_TABLES, $BASEPATH;
	
		$date = date("ymdHis");

		if (is_array($DB_TABLES) && count($DB_TABLES) > 0) {
			$backupSqlFilesArray = array();
			foreach ($DB_TABLES as $table) {
				$backupSqlFile = $BASEPATH . "backup/db_backup_" . $date . "_" . $table . ".sql";
				$backupSqlFilesArray[] = $backupSqlFile;
				$DB->query(
					"SELECT * ".
					"INTO OUTFILE '" . $backupSqlFile . "' ".
					"FROM `" . $table . "`;"
				);
			}
		}
		$DB->close();

		$backupZipFile = $BASEPATH . "backup/db_backup_" . $date . ".zip";

		$zip = new ZipArchive();

		$zip->open($backupZipFile, ZIPARCHIVE::CREATE);
		
		if (is_array($backupSqlFilesArray) && count($backupSqlFilesArray) > 0) {
			foreach ($backupSqlFilesArray as $backupSqlFile) {

				$fileNameInZip = substr(getFileNameFromPath($backupSqlFile), 23);
				$zip->addFile($backupSqlFile, $fileNameInZip);
			}
		}
		$zip->close();

		if (is_array($backupSqlFilesArray) && count($backupSqlFilesArray) > 0) {
			foreach ($backupSqlFilesArray as $backupSqlFile) {
				unlink($backupSqlFile);
			}
		}

		return getFileNameFromPath($backupZipFile);
	}

	function restoreDb($backupZipFile = "") {
		global $DB, $DB_TABLES, $BASEPATH;

		if ($backupZipFile != "") {
			$zip = new ZipArchive;
			if ($zip->open($BASEPATH . "backup/" . $backupZipFile) === TRUE) {
				$zip->extractTo($BASEPATH . "backup/" . rtrim($backupZipFile, ".zip"));
				$zip->close();
			} else {
				return FALSE;
			}
		}

		if (is_array($DB_TABLES) && count($DB_TABLES) > 0) {
			foreach ($DB_TABLES as $table) {
				$backupSqlFile = $BASEPATH . "backup/" . rtrim($backupZipFile, ".zip") . "/" . $table . ".sql";
				$DB->query(
					"DELETE ".
					"FROM `" . $table . "`;"
				);
				$DB->query(
					"LOAD DATA ".
					"INFILE '" . $backupSqlFile . "' ".
					"INTO TABLE " . $table . ";"
				);
				unlink($backupSqlFile);
			}
		}		
		$DB->close();

		rmdir($BASEPATH . "backup/" . rtrim($backupZipFile, ".zip"));

		return TRUE;
	}

	function sendBackup($backupZipFile = "") {
		global $BASEURL;
		if ($backupZipFile != "") {
			header("Location: " . $BASEURL . "backup/" . $backupZipFile);
		}
	}

	function getJavaScriptString() {
		$html =
			"<script language=\"javascript\">\n".
			"	function submitAction(action) {\n".
			"		if (action != 'download' && checkForOpenEvals()) {\n".
			"			alert('Certaine(s) évaluation(s) sont en cours de modification et/ou de consultation:\\nVous devez les fermer avant de poursuivre.');\n".
			"			return false;\n".
			"		} else {\n".
			"			if (action == 'restore') {\n".
			"				if (confirm('Êtes-vous bien certain de vouloir restorer cette copie de sauvegarde ?\\nToutes les données enregistrées après la date de cette copie de sauvegarde seront perdues.\\n\\nIl est recommandé d\'effectuer une copie de sauvegarde de la base de donnée actuelle avant d\'effectuer une restoration.')) {;\n".
			"					document.getElementById('action').value = 'restore';\n".
			"					document.backup_db.submit()\n".
			"				} else {\n".
			"			 		return;\n".
			"				}\n".
			"			} else {\n".
			"				document.getElementById('action').value = action;\n".
			"				document.backup_db.submit()\n".
			"			}\n".
			"		}\n".
			"	}\n".

			"	function checkForOpenEvals() {\n".
			"		var evals_info = new Array();\n";

		if (is_array($_SESSION['evaluations']) && count($_SESSION['evaluations']) > 0) {
			$n = 0;
			foreach ($_SESSION['evaluations'] as $sid_n => $detail) {
				if (is_array($detail['compare'])) {
					$html .=
						"evals_info[" . $n . "] = '- Comparatif';";
				} else {
					$html .=
						"evals_info[" . $n . "] = '- " . addslashes($detail['etudes_nom'] . " (" . $detail['s0_date_visite'] . ")") . " sauvegardée le " . addslashes(substr($detail['updatedOn'], 0, 10) . " à " . substr($detail['updatedOn'], 11)) . ".';\n";
				}
				$n++;
			}
		}

		$html .=
			"		if (evals_info.length > 0) {\n".
			"			return false;\n".
			"		}\n".
			"		return true;\n".
			"	}\n".
			"</script>\n";

		return $html;
	}

	function getLatestBackupDateString($filesArray = "") {

		$html = "";

		if (is_array($filesArray) && count($filesArray) > 0) { 
			$fulldate = 0;
			foreach ($filesArray as $file => $info) {
				$last_fulldate = $fulldate;
				$fulldate = substr($info['name'], 10, 12);
				if ($fulldate > $last_fulldate) {
					$newest_fulldate = $fulldate;
				} else {
					$newest_fulldate = $last_fulldate;
				}
			}

			$mktime = mktime(substr($newest_fulldate, 6, 2), substr($newest_fulldate, 8, 2), substr($newest_fulldate, 10, 2), substr($newest_fulldate, 2, 2), substr($newest_fulldate, 4, 2), substr($newest_fulldate, 0, 2));
			$date =	date("Y-m-d", $mktime);
			$time =	date("H:i:s", $mktime);
	
			$html .=
				"<em>La plus r&eacute;cente copie de sauvegarde a &eacute;t&eacute; effectu&eacute;e le " . $date . " &agrave; " . $time . "</em><br /><br />\n";
		}
		
		return $html;
	}

	function backupDocs() {
		
	}

?>
