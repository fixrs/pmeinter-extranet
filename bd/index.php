<?PHP
	require "conf/conf.inc.php";

	session_start();


	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "accueil");


	$html = "<h1>Base de donn&eacute;es de PME INTER Notaires</h1>\n";

	/***********************************/
	/*********       EMAIL      ********/
	/***********************************/
	if (isset($_GET["i"])) {

		$Skin->assign("bodyparams", " class=\"event-inscription\" ");

		$json = json_decode(base64_decode($_GET["i"]), true);

		$json_user_email = $json["courriel"];
		$json_table = $json["table"];
		$json_id = $json["id"];

		$DB->query(
			"SELECT e.key ".
			"FROM `employes` e ".
			"WHERE e.courriel = '" . $json_user_email . "'"
		);

		while ($DB->next_record()) {
			$employe_key = $DB->getField("key");
		}

		if ($employe_key != "") {


			$Skin->assign("EVENTJS",
				//"<script type=\"text/javascript\" src=\"" . $BASEURL . "skin/js/jquery-1.12.4.js\"></script>\n".
				"<script type=\"text/javascript\" src=\"" . $BASEURL . "skin/js/event.js\"></script>"
			);

			switch ($json_table) {

				case "direction_de_travail":

					$DB->query(
						"SELECT dte.*, dt.nom, dtes.status ".
						"FROM `directions_de_travail_event` dte ".
						"INNER JOIN `employes_directions_de_travail` edt ON edt.directions_de_travail_key = dte.directions_de_travail_key AND edt.employes_key = '" . $employe_key . "' ".
						"INNER JOIN `directions_de_travail` dt ON dt.key = dte.directions_de_travail_key ".
						"LEFT JOIN `directions_de_travail_event_assists` dtes ON dtes.utilisateurs_key = '" . $employe_key . "' AND dtes.directions_de_travail_event_id = '" . $json_id . "' ".
						"WHERE dte.id = '" . $json_id . "'"
					);

					while ($DB->next_record()) {
						$note = $DB->getField("note");
						$description = preg_replace("/\[LIEU\]/", $DB->getField("lieu"), $DB->getField("description"));
						$description = preg_replace("/\[COMITE\]/", $DB->getField("nom"), $description);
						$description = preg_replace("/\[DATE\]/", $DB->getField("date"), $description);
						$description = preg_replace("/\[INSCRIPTIONS\]/", "", $description);

						$description = preg_replace("/\[DATE_INSCRIPTION\]/is", $DB->getField("date_inscription"), $description);
						$description = preg_replace("/\[HEURE_DEBUT\]/is", $DB->getField("heure_debut"), $description);
						$description = preg_replace("/\[HEURE_FIN\]/is", $DB->getField("heure_fin"), $description);
						$description = preg_replace("/\[CONFERENCIER\]/is", $DB->getField("conferencier"), $description);
						$description = preg_replace("/\[SUJET\]/is", $DB->getField("sujet"), $description);
						$description = preg_replace("/\[NOTE\]/is", "", $description);

						$html .= "<h2 style=\"font-size: 1.4em; color: #253767; margin-top: 2px; margin-bottom: 22px; text-transform: uppercase; text-decoration: none; text-align: center;padding: 20px;\">" . $note . "</h2>\n";

						$fichiers = "";
						for ($i = 1; $i < 6; $i++) {
							if (trim($DB->getField("fichier" . $i)) != "" && file_exists($BASEPATH . "docs/evenements/" . $DB->getField("fichier" . $i))) {
								$fichiers .= "<li><a href=\"http://bd.pmeinter.ca/docs/evenements/" . $DB->getField("fichier" . $i) . "\" target=\"_blank\">Pi&egrave;ce jointe #" . $i . "</a></li>\n";
							}
						}

						if (trim($fichiers) != "") {
							$fichiers = "<ul>" . $fichiers . "</ul>";
						}

						$description = preg_replace("/\[FICHIERS\]/", $fichiers, $description);

						if ($DB->getField("status") == 1) {
							$html .=
								"<div class=\"direction_travail_event\" style=\"border: solid 1px #DDD; padding: 30px;\">" .
								" <h2 style=\"cursor: pointer; font-size: 14px; text-transform: none; color: #000; text-decoration: none;\"><!--" . $DB->getField("date") . " - " . $DB->getField("nom") . " - " . $DB->getField("lieu") . " : --> Votre statut :</h2>".
								"	<div>\n".
								"<ul style=\"margin-left: 40px;\" id=\"present\">\n".
								"	<li class=\"present\"><strong>Pr&eacute;sent(e) &agrave; la r&eacute;union AM et PM</strong></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=2&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=3&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union PM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=4&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) via Go to Meeting</a></li>\n".
								"	<li class=\"absent\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=0&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Absent(e) &agrave; la r&eacute;union</a></li>\n".
								"</ul><br /><br />\n".
								$description .

								"	</div>\n".
								"</div>";
						} else if ($DB->getField("status") == 2) {
							$html .=
								"<div class=\"direction_travail_event\" style=\"border: solid 1px #DDD; padding: 30px;\">" .
								" <h2 style=\"cursor: pointer; font-size: 14px; text-transform: none; color: #000; text-decoration: none; \"><!--" . $DB->getField("date") . " - " . $DB->getField("nom") . " - " . $DB->getField("lieu") . " : -->Votre statut :</h2>".
								"	<div>\n".
								"<ul style=\"margin-left: 40px;\" id=\"absent\">\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=1&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM et PM</a></li>\n".
								"	<li class=\"present\"><strong>Pr&eacute;sent(e) &agrave; la r&eacute;union AM</strong></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=3&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union PM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=4&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) via Go to Meeting</a></li>\n".
								"	<li class=\"absent\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=0&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Absent(e) &agrave; la r&eacute;union</a></li>\n".
								"</ul><br /><br />\n".
								$description .

								"	</div>\n".
								"</div>";
						} else if ($DB->getField("status") == 3) {
							$html .=
								"<div class=\"direction_travail_event\" style=\"border: solid 1px #DDD; padding: 30px;\">" .
								" <h2 style=\"cursor: pointer; font-size: 14px; text-transform: none; color: #000; text-decoration: none; \"><!--" . $DB->getField("date") . " - " . $DB->getField("nom") . " - " . $DB->getField("lieu") . " : -->Votre statut :</h2>".
								"	<div>\n".
								"<ul style=\"margin-left: 40px;\" id=\"absent\">\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=1&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM et PM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=2&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM</a></li>\n".
								"	<li class=\"present\"><strong>Pr&eacute;sent(e) &agrave; la r&eacute;union PM</strong></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=4&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) via Go to Meeting</a></li>\n".
								"	<li class=\"absent\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=0&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Absent(e) &agrave; la r&eacute;union</a></li>\n".
								"</ul><br /><br />\n".
								$description .

								"	</div>\n".
								"</div>";

						} else if ($DB->getField("status") == 4) {
							$html .=
								"<div class=\"direction_travail_event\" style=\"border: solid 1px #DDD; padding: 30px;\">" .
								" <h2 style=\"cursor: pointer; font-size: 14px; text-transform: none; color: #000; text-decoration: none; \"><!--" . $DB->getField("date") . " - " . $DB->getField("nom") . " - " . $DB->getField("lieu") . " : -->Votre statut :</h2>".
								"	<div>\n".
								"<ul style=\"margin-left: 40px;\" id=\"absent\">\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=1&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM et PM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=2&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=3&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union PM</a></li>\n".
								"	<li class=\"present\"><strong>Pr&eacute;sent(e) via Go to Meeting</strong></li>\n".
								"	<li class=\"absent\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=0&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Absent(e) &agrave; la r&eacute;union</a></li>\n".
								"</ul><br /><br />\n".
								$description .

								"	</div>\n".
								"</div>";

						} else if ($DB->getField("status") == "0") {
							$html .=
								"<div class=\"direction_travail_event\" style=\"border: solid 1px #DDD; padding: 30px;\">" .
								" <h2 style=\"cursor: pointer; font-size: 14px; text-transform: none; color: #000; text-decoration: none; \"><!--" . $DB->getField("date") . " - " . $DB->getField("nom") . " - " . $DB->getField("lieu") . " : -->Votre statut :</h2>".
								"	<div>\n".
								"<ul style=\"margin-left: 40px;\" id=\"absent\">\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=1&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM et PM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=2&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=3&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union PM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=4&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) via Go to Meeting</a></li>\n".
								"	<li class=\"absent\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=0&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Absent(e) &agrave; la r&eacute;union</a></li>\n".



								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=1&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union</a></li>\n".
								"	<li><strong>Absent(e) &agrave; la r&eacute;union</strong></li>\n".
								"</ul><br /><br />\n".
								$description .

								"	</div>\n".
								"</div>";
						} else {

							$html .=
								"<div class=\"direction_travail_event\" style=\"border: solid 1px #DDD; padding: 30px;\">" .
								" <h2 style=\"cursor: pointer; display: none; font-size: 14px; text-transform: none; color: #000; text-decoration: none; \"><!--" . $DB->getField("date") . " - " . $DB->getField("nom") . " - " . $DB->getField("lieu") . " :-->Votre statut :</h2>".
								"	<div>\n".
								"<ul style=\"margin-left: 40px;\" class=\"present\">\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=1&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM et PM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=2&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union AM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=3&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) &agrave; la r&eacute;union PM</a></li>\n".
								"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=4&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Pr&eacute;sent(e) via Go to Meeting</a></li>\n".
								"	<li class=\"absent\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=0&ek=" . $employe_key . "&i=" . $_GET["i"] . "\">Absent(e) &agrave; la r&eacute;union</a></li>\n".
								"</ul><br /><br />\n".
								$description .

								"</div>\n".
								"</div>";

						}


					}








					break;

			}


			/*

			$etude_key = "";
			$DB->query("SELECT u.etudes_key FROM `utilisateurs` u WHERE `key` = '" . $user_key . "'");
			while ($DB->next_record()) {
				$etude_key = $DB->getField("etude_key");
			}

			$DB->query(
				"SELECT DISTINCT e.key, e.nom, e.prenom, f.nom AS fonction ".
				"FROM `employes` e INNER JOIN `employes_fonctions` ef ON ef.employes_key = e.key ".
				"INNER JOIN `fonctions` f ON f.key = ef.fonctions_key ".
				($_SESSION['user_key'] == 0 ? "" :
					"INNER JOIN `etudes_succursales` es ON es.etudes_key = '" . $etude_key . "' ".
					"INNER JOIN `employes_etudes_succursales` ees ON ees.employes_key = e.key AND ees.etudes_succursales_key = es.key ") .
				"ORDER BY e.nom, e.prenom"
			);

			$inscription_jfe_string =
				"<div id=\"jfe_participants\">\n".
				"	<br />\n".
				"	<input type=\"hidden\" id=\"jfe_table\" value=\"journee_formation_event\" />\n".
				"	<input type=\"hidden\" id=\"jfe_event\" value=\"[JFE_EVENT]\" />\n".
				"	<ul>\n";

			$inscription_cag_string =
				"<div id=\"cag_participants\">\n".
				"	<br />\n".
				"	<input type=\"hidden\" id=\"cag_table\" value=\"convocation_ag_event\" />\n".
				"	<input type=\"hidden\" id=\"cag_event\" value=\"[CAG_EVENT]\" />\n".
				"	<ul>\n";

			$inscription_caga_string =
				"<div id=\"caga_participants\">\n".
				"	<br />\n".
				"	<input type=\"hidden\" id=\"caga_table\" value=\"convocation_aga_event\" />\n".
				"	<input type=\"hidden\" id=\"caga_event\" value=\"[CAGA_EVENT]\" />\n".
				"	<ul>\n";

			while ($DB->next_record()) {
				$inscription_jfe_string .= "		<li><input type=\"checkbox\" class=\"user\" id=\"jfe_user" . $DB->getField("key") . "\" value=\"1\" /> <label for=\"jfe_user" . $DB->getField("key") . "\">" . $DB->getField("nom") . " " . $DB->getField("prenom") . ", " . $DB->getField("fonction") . "</label></li>";

				$inscription_cag_string .= "		<li><input type=\"checkbox\" class=\"user\" id=\"cag_user" . $DB->getField("key") . "\" value=\"1\" /> <label for=\"cag_user" . $DB->getField("key") . "\">" . $DB->getField("nom") . " " . $DB->getField("prenom") . ", " . $DB->getField("fonction") . "</label></li>";

				$inscription_caga_string .= "		<li><input type=\"checkbox\" class=\"user\" id=\"caga_user" . $DB->getField("key") . "\" value=\"1\" /> <label for=\"caga_user" . $DB->getField("key") . "\">" . $DB->getField("nom") . " " . $DB->getField("prenom") . ", " . $DB->getField("fonction") . "</label></li>";
			}

			$inscription_string =
				"	</ul>\n".
				"	<br clear=\"all\" /><br /><br />\n".
				"</div>\n";

			$inscription_jfe_string .= $inscription_string;
			$inscription_cag_string .= $inscription_string;
			$inscription_caga_string .= $inscription_string;


			$DB->query("SELECT * FROM `journee_formation_event` WHERE `date` >= '" . date('Y-m-d') . "' ORDER BY `date` ASC");
			while ($DB->next_record()) {
				$description = preg_replace("/\[LIEU\]/", $DB->getField("lieu"), $DB->getField("description"));
				$description = preg_replace("/\[DATE\]/", translateDate($DB->getField("date")), $description);
				$description = preg_replace("/\[COUT\]/", $DB->getField("cout"), $description);
				$description = preg_replace("/\[SUJET\]/", $DB->getField("sujet"), $description);
				$description = preg_replace("/\[DEPARTEMENT\]/", $DB->getField("departement"), $description);
				$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($DB->getField("date_limite")), $description);
				$description = preg_replace("/\[NOTE\]/", $DB->getField("note"), $description);
				$id = $DB->getField("id");

				$DB->query(
					"SELECT * ".
					"FROM `inscriptions` ".
					"WHERE `table` = 'journee_formation_event' ".
					"AND `event_key` = '" . $id . "' ".
					"AND `actif` = '1'"
				);

				while ($DB->next_record()) {
					$inscription_jfe_string = preg_replace("/(id=\"jfe_user" . $DB->getField("employes_key") . "\")/s", "$1 checked=\"checked\" ", $inscription_jfe_string);
				}

				$inscription_jfe_string = preg_replace("/\[JFE_EVENT\]/", $id, $inscription_jfe_string);

				$description = preg_replace("/\[INSCRIPTIONS\]/", $inscription_jfe_string, $description);

				$html .=
					"<div class=\"journee_formation_event\" style=\"margin-left: -20px;	\">" .
					$description .
					"</div>";
			}


			$DB->query("SELECT * FROM `convocation_aga_event` WHERE `date` >= '" . date('Y-m-d') . "' ORDER BY `date` ASC");
			while ($DB->next_record()) {
				$description = preg_replace("/\[LIEU\]/", $DB->getField("lieu"), $DB->getField("description"));
				$description = preg_replace("/\[DATE\]/", translateDate($DB->getField("date")), $description);
				$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($DB->getField("date_limite")), $description);
				$id = $DB->getField("id");

				$DB->query(
					"SELECT * ".
					"FROM `inscriptions` ".
					"WHERE `table` = 'convocation_aga_event' ".
					"AND `event_key` = '" . $id . "' ".
					"AND `actif` = '1'"
				);

				while ($DB->next_record()) {
					$inscription_caga_string = preg_replace("/(id=\"caga_user" . $DB->getField("employes_key") . "\")/s", "$1 checked=\"checked\" ", $inscription_caga_string);
				}

				$inscription_caga_string = preg_replace("/\[CAGA_EVENT\]/", $id, $inscription_caga_string);

				$description = preg_replace("/\[INSCRIPTIONS\]/", $inscription_caga_string, $description);

				$html .=
					"<div class=\"convocation_aga_event\" style=\"margin-left: -20px;	\">" .
					$description .
					"</div>";
			}

			$DB->query("SELECT * FROM `convocation_ag_event` WHERE `date` >= '" . date('Y-m-d') . "' ORDER BY `date` ASC");
			while ($DB->next_record()) {
				$description = preg_replace("/\[LIEU\]/", $DB->getField("lieu"), $DB->getField("description"));
				$description = preg_replace("/\[DATE\]/", translateDate($DB->getField("date")), $description);
				$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($DB->getField("date_limite")), $description);
				$id = $DB->getField("id");

				$DB->query(
					"SELECT * ".
					"FROM `inscriptions` ".
					"WHERE `table` = 'convocation_ag_event' ".
					"AND `event_key` = '" . $id . "' ".
					"AND `actif` = '1'"
				);

				while ($DB->next_record()) {
					$inscription_cag_string = preg_replace("/(id=\"cag_user" . $DB->getField("employes_key") . "\")/s", "$1 checked=\"checked\" ", $inscription_cag_string);
				}

				$inscription_cag_string = preg_replace("/\[CAG_EVENT\]/", $id, $inscription_cag_string);

				$description = preg_replace("/\[INSCRIPTIONS\]/", $inscription_cag_string, $description);

				$html .=
					"<div class=\"convocation_ag_event\" style=\"margin-left: -20px;	\">" .
					$description .
					"</div>";
			}
			*/






		}







		/***********************************/
		/*********       LOGUE      ********/
		/***********************************/


	} else if ($_SESSION["user_key"]) {



		$Skin->assign("EVENTJS",
			//"<script type=\"text/javascript\" src=\"" . $BASEURL . "skin/js/jquery-1.12.4.js\"></script>\n".
			"<script type=\"text/javascript\" src=\"" . $BASEURL . "skin/js/event.js\"></script>"
		);

		$DB->query(
			"SELECT u.employes_key ".
			"FROM `utilisateurs` u ".
			"WHERE u.key = '" . $_SESSION['user_key'] . "' "
		);

		while ($DB->next_record()) {
			$employe_key = $DB->getField("employes_key");
		}


		$DB->query(
			"SELECT dte.*, dt.nom, dtes.status ".
			"FROM `directions_de_travail_event` dte ".
			"INNER JOIN `employes_directions_de_travail` edt ON edt.directions_de_travail_key = dte.directions_de_travail_key AND edt.employes_key = '" . $employe_key . "' ".
			"INNER JOIN `directions_de_travail` dt ON dt.key = dte.directions_de_travail_key ".
			"LEFT JOIN `directions_de_travail_event_assists` dtes ON dtes.utilisateurs_key = '" . $employe_key . "' AND dtes.directions_de_travail_event_id = dte.id ".
			"WHERE dte.date >= '" . date("Y-m-d") . "'"
		);


		while ($DB->next_record()) {

			$description = preg_replace("/\[LIEU\]/", $DB->getField("lieu"), $DB->getField("description"));
			$description = preg_replace("/\[COMITE\]/", $DB->getField("nom"), $description);
			$description = preg_replace("/\[DATE\]/", $DB->getField("date"), $description);
			$description = preg_replace("/\[INSCRIPTIONS\]/", "", $description);

			$fichiers = "";
			for ($i = 1; $i < 6; $i++) {
				if (trim($DB->getField("fichier" . $i)) != "" && file_exists($BASEPATH . "docs/evenements/" . $DB->getField("fichier" . $i))) {
					$fichiers .= "<li><a href=\"http://bd.pmeinter.ca/docs/evenements/" . $DB->getField("fichier" . $i) . "\" target=\"_blank\">Pi&egrave;ce jointe #" . $i . "</a></li>\n";
				}
			}

			if (trim($fichiers) != "") {
				$fichiers = "<ul>" . $fichiers . "</ul>";
			}

			$description = preg_replace("/\[FICHIERS\]/", $fichiers, $description);

			if ($DB->getField("status") == 1) {
				$html .=
					"<div class=\"direction_travail_event\" style=\"border: solid 1px #DDD; padding: 30px;\">" .
					" <h2 style=\"cursor: pointer;  font-size: 14px; text-transform: none; color: #000; text-decoration: none; \"><!--" . $DB->getField("date") . " - " . $DB->getField("nom") . " - " . $DB->getField("lieu") . " : --> Votre statut : <strong class=\"absent\" style=\"display: none;\">Absent(e)</strong> <strong class=\"present\">Pr&eacute;sent(e)</strong></h2>".
					"	<div style=\"display: none;\">\n".
					$description .
					"<ul style=\"margin-left: 40px;\" id=\"present\">\n".
					//"	<li><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=1\">Je serai pr&eacute;sent(e)</a></li>\n".
					"	<li class=\"absent\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=0&ek=" . $employe_key . "\">Je ne pourrai finalement pas y assister</a></li>\n".
					"</ul>\n".
					"	</div>\n".
					"</div>";
			} else if ($DB->getField("status") == "0") {
				$html .=
					"<div class=\"direction_travail_event\" style=\"border: solid 1px #DDD; padding: 30px;\">" .
					" <h2 style=\"cursor: pointer;  font-size: 14px; text-transform: none; color: #000; text-decoration: none; \"><!--" . $DB->getField("date") . " - " . $DB->getField("nom") . " - " . $DB->getField("lieu") . " : --> Votre statut : <strong class=\"absent\">Absent(e)</strong> <strong class=\"present\" style=\"display: none;\">Pr&eacute;sent(e)</strong></h2>".
					"	<div style=\"display: none;\">\n".
					$description .
					"<ul style=\"margin-left: 40px;\" id=\"absent\">\n".
					"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=1&ek=" . $employe_key . "\">Je serai finalement pr&eacute;sent(e)</a></li>\n".
					//"	<li><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=0\">Je ne pourrai finalement pas y assister</a></li>\n".
					"</ul>\n".
					"	</div>\n".
					"</div>";
			} else {

				$html .=
					"<div class=\"direction_travail_event\" style=\"border: solid 1px #DDD; padding: 30px;\">" .
					" <h2 style=\"cursor: pointer; display: none; font-size: 14px; text-transform: none; color: #000; text-decoration: none; \"><!--" . $DB->getField("date") . " - " . $DB->getField("nom") . " - " . $DB->getField("lieu") . " :--> Votre statut : <strong class=\"absent\" style=\"display: none;\">Absent(e)</strong> <strong class=\"present\" style=\"display: none;\">Pr&eacute;sent(e)</strong></h2>".
					"	<div>\n".
					$description .
					"<ul style=\"margin-left: 40px;\" class=\"present\">\n".
					"	<li class=\"present\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=1&ek=" . $employe_key . "\">Je serai pr&eacute;sent(e)</a></li>\n".
					"	<li class=\"abesent\"><a href=\"/evenements/directions_de_travail.php?e=" . $DB->getField("id") . "&s=0&ek=" . $employe_key . "\">Je ne pourrai pas y assister</a></li>\n".
					"</ul>\n".
					"</div>\n".
					"</div>";
			}
		}


		$etude_key = "";
		$DB->query("SELECT u.etudes_key FROM `utilisateurs` u WHERE `key` = '" . $_SESSION['user_key'] . "'");
		while ($DB->next_record()) {
			$etude_key = $DB->getField("etude_key");
		}

		$DB->query(
			"SELECT DISTINCT e.key, e.nom, e.prenom, f.nom AS fonction ".
			"FROM `employes` e INNER JOIN `employes_fonctions` ef ON ef.employes_key = e.key ".
			"INNER JOIN `fonctions` f ON f.key = ef.fonctions_key ".
			($_SESSION['user_key'] == 0 ? "" :
				"INNER JOIN `etudes_succursales` es ON es.etudes_key = '" . $etude_key . "' ".
				"INNER JOIN `employes_etudes_succursales` ees ON ees.employes_key = e.key AND ees.etudes_succursales_key = es.key ") .
			"ORDER BY e.nom, e.prenom"
		);

		$inscription_jfe_string =
			"<div id=\"jfe_participants\">\n".
			"	<br />\n".
			"	<input type=\"hidden\" id=\"jfe_table\" value=\"journee_formation_event\" />\n".
			"	<input type=\"hidden\" id=\"jfe_event\" value=\"[JFE_EVENT]\" />\n".
			"	<ul>\n";

		$inscription_cag_string =
			"<div id=\"cag_participants\">\n".
			"	<br />\n".
			"	<input type=\"hidden\" id=\"cag_table\" value=\"convocation_ag_event\" />\n".
			"	<input type=\"hidden\" id=\"cag_event\" value=\"[CAG_EVENT]\" />\n".
			"	<ul>\n";

		$inscription_caga_string =
			"<div id=\"caga_participants\">\n".
			"	<br />\n".
			"	<input type=\"hidden\" id=\"caga_table\" value=\"convocation_aga_event\" />\n".
			"	<input type=\"hidden\" id=\"caga_event\" value=\"[CAGA_EVENT]\" />\n".
			"	<ul>\n";

		while ($DB->next_record()) {
			$inscription_jfe_string .= "		<li><input type=\"checkbox\" class=\"user\" id=\"jfe_user" . $DB->getField("key") . "\" value=\"1\" /> <label for=\"jfe_user" . $DB->getField("key") . "\">" . $DB->getField("nom") . " " . $DB->getField("prenom") . ", " . $DB->getField("fonction") . "</label></li>";

			$inscription_cag_string .= "		<li><input type=\"checkbox\" class=\"user\" id=\"cag_user" . $DB->getField("key") . "\" value=\"1\" /> <label for=\"cag_user" . $DB->getField("key") . "\">" . $DB->getField("nom") . " " . $DB->getField("prenom") . ", " . $DB->getField("fonction") . "</label></li>";

			$inscription_caga_string .= "		<li><input type=\"checkbox\" class=\"user\" id=\"caga_user" . $DB->getField("key") . "\" value=\"1\" /> <label for=\"caga_user" . $DB->getField("key") . "\">" . $DB->getField("nom") . " " . $DB->getField("prenom") . ", " . $DB->getField("fonction") . "</label></li>";
		}

		$inscription_string =
			"	</ul>\n".
			"	<br clear=\"all\" /><br /><br />\n".
			"</div>\n";

		$inscription_jfe_string .= $inscription_string;
		$inscription_cag_string .= $inscription_string;
		$inscription_caga_string .= $inscription_string;


		$DB->query("SELECT * FROM `journee_formation_event` WHERE `date` >= '" . date('Y-m-d') . "' ORDER BY `date` ASC");
		while ($DB->next_record()) {
			$description = preg_replace("/\[LIEU\]/", $DB->getField("lieu"), $DB->getField("description"));
			$description = preg_replace("/\[DATE\]/", translateDate($DB->getField("date")), $description);
			$description = preg_replace("/\[COUT\]/", $DB->getField("cout"), $description);
			$description = preg_replace("/\[SUJET\]/", $DB->getField("sujet"), $description);
			$description = preg_replace("/\[DEPARTEMENT\]/", $DB->getField("departement"), $description);
			$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($DB->getField("date_limite")), $description);
			$description = preg_replace("/\[NOTE\]/", $DB->getField("note"), $description);
			$id = $DB->getField("id");

			$DB->query(
				"SELECT * ".
				"FROM `inscriptions` ".
				"WHERE `table` = 'journee_formation_event' ".
				"AND `event_key` = '" . $id . "' ".
				"AND `actif` = '1'"
			);

			while ($DB->next_record()) {
				$inscription_jfe_string = preg_replace("/(id=\"jfe_user" . $DB->getField("employes_key") . "\")/s", "$1 checked=\"checked\" ", $inscription_jfe_string);
			}

			$inscription_jfe_string = preg_replace("/\[JFE_EVENT\]/", $id, $inscription_jfe_string);

			$description = preg_replace("/\[INSCRIPTIONS\]/", $inscription_jfe_string, $description);

			$html .=
				"<div class=\"journee_formation_event\" style=\"margin-left: -20px;	\">" .
				$description .
				"</div>";
		}


		$DB->query("SELECT * FROM `convocation_aga_event` WHERE `date` >= '" . date('Y-m-d') . "' ORDER BY `date` ASC");
		while ($DB->next_record()) {
			$description = preg_replace("/\[LIEU\]/", $DB->getField("lieu"), $DB->getField("description"));
			$description = preg_replace("/\[DATE\]/", translateDate($DB->getField("date")), $description);
			$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($DB->getField("date_limite")), $description);
			$id = $DB->getField("id");

			$DB->query(
				"SELECT * ".
				"FROM `inscriptions` ".
				"WHERE `table` = 'convocation_aga_event' ".
				"AND `event_key` = '" . $id . "' ".
				"AND `actif` = '1'"
			);

			while ($DB->next_record()) {
				$inscription_caga_string = preg_replace("/(id=\"caga_user" . $DB->getField("employes_key") . "\")/s", "$1 checked=\"checked\" ", $inscription_caga_string);
			}

			$inscription_caga_string = preg_replace("/\[CAGA_EVENT\]/", $id, $inscription_caga_string);

			$description = preg_replace("/\[INSCRIPTIONS\]/", $inscription_caga_string, $description);

			$html .=
				"<div class=\"convocation_aga_event\" style=\"margin-left: -20px;	\">" .
				$description .
				"</div>";
		}

		$DB->query("SELECT * FROM `convocation_ag_event` WHERE `date` >= '" . date('Y-m-d') . "' ORDER BY `date` ASC");
		while ($DB->next_record()) {
			$description = preg_replace("/\[LIEU\]/", $DB->getField("lieu"), $DB->getField("description"));
			$description = preg_replace("/\[DATE\]/", translateDate($DB->getField("date")), $description);
			$description = preg_replace("/\[DATE_LIMITE\]/", translateDate($DB->getField("date_limite")), $description);
			$id = $DB->getField("id");

			$DB->query(
				"SELECT * ".
				"FROM `inscriptions` ".
				"WHERE `table` = 'convocation_ag_event' ".
				"AND `event_key` = '" . $id . "' ".
				"AND `actif` = '1'"
			);

			while ($DB->next_record()) {
				$inscription_cag_string = preg_replace("/(id=\"cag_user" . $DB->getField("employes_key") . "\")/s", "$1 checked=\"checked\" ", $inscription_cag_string);
			}

			$inscription_cag_string = preg_replace("/\[CAG_EVENT\]/", $id, $inscription_cag_string);

			$description = preg_replace("/\[INSCRIPTIONS\]/", $inscription_cag_string, $description);

			$html .=
				"<div class=\"convocation_ag_event\" style=\"margin-left: -20px;	\">" .
				$description .
				"</div>";
		}

		$Skin->assign("bodyparams", " class=\"home blog logged-in admin-bar no-customize-support page-header-fixed group-blog masthead-fixed list-view full-width grid\" ");

	} else {

		$Skin->assign("bodyparams", " class=\"home blog anonymous admin-bar no-customize-support page-header-fixed group-blog masthead-fixed list-view full-width grid\" ");


	}




	$Skin->assign("BASEURL", $BASEURL);
	$Skin->assign("page", "index");
	$Skin->assign("title", "Accueil");
	$Skin->assign("content", $html);
	$Skin->display("index.tpl");

	$DB->close();

?>
