<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
		$Skin->assign("errors", $html);
		$Skin->display("formations.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
		$Skin->assign("errors", $html);
		$Skin->display("formations.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Formation");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$successString = "";
	$errorString = "";
	$formString = "";

	// ÉTAPE #1
	// Traitement des GET provenant de la sélection d'une action dans le PREMIER menu $menuString
	switch ($_GET['action']) {
		case "search":
			$formString .= validateJSString().searchString();
			break;
		case "results":
			$formString .= validateJSString().searchResultsString();
			break;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
	$Skin->assign("page", "formations");
	$Skin->assign("title", pageTitleString(getorpost('action')));
#	$Skin->assign("menu", menuFormationsString());
	$Skin->assign("menu_title", menuTitleString(getorpost('action')));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("formations.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "search":
				$title = "Recherche de formations";
				break;
			case "results":
				$title = "R&eacute;sultats de recherche de formations";
				break;
		}
		return $title;
	}

	// Fonction qui retourne le html pour le titre du sous-menu
	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "search":
				$html .= "<h1>Rechercher des formations</h1>\n";
				break;
			case "results":
				$html .= "<h1>Rechercher des formations</h1>\n";
				break;
		}
		return $html;
	}

	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function searchString() { 
		$motCle = "";
		$dateDebut = "";
		$beginDate = "";
		$endDate = "";
		if (isset($_GET['search_formations'])&& trim($_GET['search_formations'])!=""){
			$motCle = trim($_GET['search_formations']);
		}
		if (isset($_GET['date_debut']) && trim($_GET['date_debut'])!=""){
			$dateDebut = trim($_GET['date_debut']);
		}
		if (isset($_GET['begin_date']) && trim($_GET['begin_date'])!=""){
			$beginDate = trim($_GET['begin_date']);
		}
		if (isset($_GET['end_date']) && trim($_GET['end_date'])!=""){
			$endDate = trim($_GET['end_date']);
		}
		$html =
			"<div class=\"form\">\n".
//			"	<form id=\"formations_recherche\" action=\"search.php?action=results\" method=\"get\" onsubmit=\"return validateForm('search_formations');\">\n" .
			"	<form id=\"formations_recherche\" action=\"search.php?action=results\" method=\"get\">\n" .
			"		<input type=\"hidden\" id=\"action\" name=\"action\" value=\"results\" />".
			"		<table class=\"searchTable\">\n" .
			"			<tr>\n".
			"				<td>Mot(s)-cl&eacute;(s) :</td>\n" .
			"				<td><input type=\"text\" id=\"search_formations\" name=\"search_formations\" value=\"".$motCle."\" tabindex=\"20\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Date de la formation (facultatif) :</td>\n " .
			"				<td><input type=\"text\" maxlength=\"10\" size=\"10\" id=\"date_debut\" name=\"date_debut\" value=\"".$dateDebut."\" tabindex=\"22\" onChange=\"validateDate('date_debut');\" /> (format AAAA-MM-JJ)</td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td colspan=\"2\">Recherche par p&eacute;riode (facultatif) : </td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>&Agrave; partir du :</td>\n" .
			"				<td><input type=\"text\" maxlength=\"10\" size=\"10\" id=\"begin_date\" name=\"begin_date\" value=\"".$beginDate."\" tabindex=\"24\" onChange=\"validateDate('begin_date');\" /> </td>\n" .
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Jusqu'au :</td>\n" .
			"				<td><input type=\"text\" maxlength=\"10\" size=\"10\" id=\"end_date\" name=\"end_date\" value=\"".$endDate."\" tabindex=\"26\" onChange=\"validateDate('end_date');\" /></td>\n".
			"			</tr>\n".
			"		</table>\n".
			"		<br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" tabindex=\"28\" />\n".
			"	</form>\n".
			"</div>".
			"<br /><br />\n";
		return $html;
	}

	function searchResultsString() {
		global $DB;
		$html = searchString();
		$html .= "<h2>R&eacute;sultats de recherche de formations</h2>\n";
		$compteur = 0;
		$nb = 0;
		$query = "";
		$sqlDateEvent = "";
		$sqlPeriod = "";
			
		if (trim($_GET["search_formations"]) != "") {
			$terms = split(" ", trim($_GET["search_formations"]));
			$query = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$query .= "AND CONCAT_WS(' ', `nom`, if( `date_precision` IS NULL , NULL , `date_precision` ), if( `heure` IS NULL , NULL , `heure` ), if( `coordonnees` IS NULL , NULL , `coordonnees` ), if( `salle` IS NULL , NULL , `salle` ), if( `description` IS NULL , NULL , `description` ), if( `conferencier` IS NULL , NULL , `conferencier` ), if( `nom_responsable` IS NULL , NULL , `nom_responsable` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}
		if (trim($_GET['begin_date'])!='' || trim($_GET['end_date'])!=''){
			if(trim($_GET['begin_date'])!='' && trim($_GET['end_date'])==''){
				$sqlPeriod = "AND `date_debut` >= '".trim($_GET['begin_date'])."' ";
			}
			else if (trim($_GET['begin_date'])=='' && trim($_GET['end_date'])!=''){
				$sqlPeriod = "AND `date_debut` <='".trim($_GET['begin_date'])."' ";
			}
			else {
				$sqlPeriod = "AND `date_debut` BETWEEN '".trim($_GET['begin_date'])."' AND '".trim($_GET['end_date']) . "' ";
			}	
		}
		if (trim($_GET['date_debut'])!=''){
			$sqlDateEvent = "AND `date_debut` LIKE '".trim($_GET['begin_date'])."' ";	
		}
		$DB->query(
				"SELECT * ".
				"FROM `formations` ".
				"WHERE `actif` = '1' ".
				$sqlPeriod .
				$sqlDateEvent .
				(trim($_GET["search_formations"]) != "" ? $query : "").
				"ORDER BY `nom` ASC "
			);
		$nb = $DB->getNumRows();

		if ($nb>0){
			$html .=
				"<table class=\"resultsTable\">\n" .
				"	<tr>\n".
				"	<th>Nom de la formation</th><th>Date de la formation</th><th>&nbsp;</th>\n".
				"	</tr>\n";
		}
			
		while ($DB->next_record()) {
			$key = $DB->getField("key");
			$color = "";
			if ($compteur%2==0) {
				$color = " class=\"altTr\"";			
			}
			$html .=
			"    <tr". $color .">\n<td><a href=\"formations.php?action=read&begin=1&forma_key=" . $DB->getField("key") . "\" title=\"Pour visualiser\">".$DB->getField("nom"). "</a></td> <td>". $DB->getField("date_debut")."</td><td><a href=\"formations.php?action=read&begin=1&forma_key=" . $DB->getField("key") . "\" title=\"Pour visualiser\">Visualiser</a> | <a href=\"formations.php?action=modify&begin=1&forma_key=" . $DB->getField("key") . "\" title=\"Pour modifier\">Modifier</a></td>\n".
			"	</tr>\n";
			$compteur++;
		}
		if ($nb==0) {
			$html .= "<p>Aucune formation ne correspond &agrave; votre requ&ecirc;te...</p>";
		}
		else if ($nb>0) {
			$html .=
				"</table>";			
		}
	
		return $html;
	}
	
	
	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function validateJSString() { 
		$html =
			"<script type=\"text/javascript\" language=\"javascript\">\n" .
			"	String.prototype.trim = function(){ \n" .
			"		return this.replace(/^\s+|\s+$/g,'');\n" .
			"		}\n".
			
			"	function validateForm(nomChamp) {\n".
			"		var champ = document.getElementById(nomChamp);\n".
			"		var champValue = champ.value;\n".
			"		var dateDebut = document.getElementById('date_debut').value;\n".
			"		var beginDate = document.getElementById('begin_date').value;\n".
			"		var endDate = document.getElementById('end_date').value;\n".
			"		var blnOk = false;\n".
			"		if (champValue.trim()!=''){\n".
			"			blnOk = true;\n".
			"		} \n" .
			"		else { \n" .
			"			if (dateDebut.trim()=='' && beginDate.trim()=='' && endDate.trim()==''){\n" .
			"				alert('Vous devez soit entré un mot-clé, \\nsoit choisir la date de la formation, \\nsoit choisir une période.'); \n" .
			"				champ.focus();\n" .
			"			} \n" .
			"			else if (dateDebut.trim()!='') { \n" .
			"				blnOk = true;\n".
			"			} \n" .
			"			else if (beginDate.trim()!='' || endDate.trim()!='') { \n" .
			"				if (beginDate.trim()!='' && endDate.trim()!='') { \n" .
			"					blnOk = true;\n".
			"				} \n" .
			"				else { \n" .
			"					alert('La recherche par période doit avoir la date de début de la recherche \\nainsi que la date de fin de la recherche.'); \n" .
			"					document.getElementById('begin_date').focus();\n" .
			"				} \n" .
			"			} \n" .
			"		} \n" .
			"		return blnOk;\n" .

			"	}\n".
			"	function validateDate(champ) {\n".
			"		var blnOk = false;\n".
			"		var champDate = document.getElementById(champ);\n".
			"		var champValue = champDate.value;\n".
			"		if (champValue.length==10){\n".
			"			var arrDate = champValue.split('-');\n".
			"			var day = parseInt(arrDate[2]);\n".
			"			var year = parseInt(arrDate[0]);\n".
			"			var month = parseInt(arrDate[1]);\n".

			"			if(month<13 && month>1 && year>1970 && day<32 && day>0){\n".
			"				var newDate = date(month, day, year);\n".
			"				alert('La date entrée '+champValue);\n".
			"				if (newDate == champValue){\n".
			"					blnOk = true;\n".
			"				} \n" .
			"			} \n" .
			"		} \n" .
			"		else if (champValue.length==0 || champValue.trim()==''){\n".
			"				blnOk = true;\n".
			"		} \n" .
			"		if (!blnOk) { \n" .
			"			alert('La date entrée est invalide');\n".
			"			champDate.value = '';\n".
			"			champDate.focus();\n".
			"		} \n" .
			"		return blnOk;\n" .
			"	}\n".
			"</script>\n" .
			"\n";
		return $html;
	}
	
	function verifyDate($date){
		$blnOk = true;
		if (strlen($date)==10){

			if (strpos($date, "-")=== false){
				$blnOk = false;
			}
			else {
				$arrDate = explode( "-", $date);
				if ($date != date("Y-m-d", mktime(0, 0, 0, $arrDate[1], $arrDate[2], $arrDate[0]))){
					
					$blnOk = false;
				}
			}
		}
		else {
			$blnOk = false;
		}
		return $blnOk;
	}
	
?>
