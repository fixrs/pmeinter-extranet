<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
		$Skin->assign("errors", $html);
		$Skin->display("formations.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
		$Skin->assign("errors", $html);
		$Skin->display("formations.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Formation");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	

	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$successString = "";
	$errorString = "";
	$formString = "";

	// ÉTAPE #1
	// Traitement des GET provenant de la sélection d'une action dans le PREMIER menu $menuString
	switch ($_GET['action']) {
		case "list":
			$formString .= javascriptString().listInscriptionsString(getorpost('action'));
			break;

		case "present"://setPresence($key, $action, $value)
			if (isset($_GET['mod_key']) && $_GET['mod_key']!="" && isset($_GET['change_to']) && $_GET['change_to']!=""){
				setPresence($_GET['mod_key'], $_GET['action'], $_GET['change_to']);
				$successString = _success("<br/>Modification compl&eacute;t&eacute;e avec succ&egrave;s");
			}
			$formString .= javascriptString().listInscriptionsString(getorpost('action'));
			break;

		case "non_present":
			if (isset($_GET['mod_key']) && $_GET['mod_key']!="" && isset($_GET['change_to']) && $_GET['change_to']!=""){
				setPresence($_GET['mod_key'], $_GET['action'], $_GET['change_to']);
				$successString = _success("<br/>Modification compl&eacute;t&eacute;e avec succ&egrave;s");
			}
			$formString .= javascriptString().listInscriptionsString(getorpost('action'));
			break;
		case "not":
			if (isset($_GET['mod_key']) && $_GET['mod_key']!="" && isset($_GET['change_to']) && $_GET['change_to']!=""){
				setPresence($_GET['mod_key'], $_GET['action'], $_GET['change_to']);
				$successString = _success("<br/>Modification compl&eacute;t&eacute;e avec succ&egrave;s");
			}
			$formString .= javascriptString().listInscriptionsString(getorpost('action'));
			break;

		case "del_ins":
			if (isset($_GET['ins_key']) && $_GET['ins_key']!="" && $_GET['ins_key']!="0" && is_numeric($_GET['ins_key'])){
				deleteInscriptionsFromDB($_GET['ins_key']);
				$formations_key = "";
				if (isset($_GET['formations_key'])){
					$formations_key = $_GET['formations_key'];
				}
				header('Location: participation.php?action=del_ins&ins_key_yes=&formations_key='.$formations_key);
				exit;
			}
			else if(isset($_GET['ins_key_yes']) && $_GET['ins_key_yes']==""){
				$successString = _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s");
				$formString .= javascriptString().getResultInscriptionsString('del_ins');
			}
			else {
				$formString .= javascriptString().getResultInscriptionsString('del_ins');
				
			}
			break;
		case "mod_ins":
			$formString .= javascriptString().getResultInscriptionsString('mod_ins');
			break;
		case "read_ins":
			$formString .= javascriptString().getResultInscriptionsString('read_ins');
			break;

	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
	$Skin->assign("page", "formations");
	$Skin->assign("title", pageTitleString(getorpost('action')));
	$Skin->assign("menu_title", menuInscriptionsString(getorpost('action')));
	$Skin->assign("menu", menuTitleString(getorpost('action')));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("formations.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "list":
				$title = "Liste d'inscriptions &agrave; une formation";
				break;
			case "present":
				$title = "Liste des participants pr&eacute;sents aux formations";
				break;
			case "non_present":
				$title = "Liste des participants absents aux formations";
				break;
			case "not":
				$title = "Liste des participants dont la pr&eacute;sence n'est pas connue";
				break;
			case "del_ins":
				$title = "Suppression d'inscriptions &agrave; une formation";
				break;
			case "mod_ins":
				$title = "Modification d'inscriptions &agrave; une formation";
				break;
			case "read_ins":
				$title = "Consultation d'inscriptions &agrave; une formation";
				break;
			default:
				$title = "Liste d'inscriptions &agrave; une formation";
		}
		return $title;
	}

	// Fonction qui retourne le html pour le menu d'actions des formations
	function menuInscriptionsString($action) { 
		$key = "";
		if (isset($_GET['formations_key'])){
			$key = $_GET['formations_key'];	
		}
		switch ($action) {
			case "list":
				$html =
					"<p><span class=\"answer\">Listes de : </span><a href=\"participation.php?action=list&formations_key=".$key."\">Inscriptions</a> | <a href=\"participation.php?action=present&formations_key=".$key."\">Pr&eacute;sences</a> | <a href=\"participation.php?action=non_present&formations_key=".$key."\">Absences</a> | <a href=\"participation.php?action=not&formations_key=".$key."\">Pr&eacute;sence inconnue</a></p>\n";
				break;
			case "present":
				$html =
					"<p><span class=\"answer\">Listes de : </span><a href=\"participation.php?action=list&formations_key=".$key."\">Inscriptions</a> | <a href=\"participation.php?action=present&formations_key=".$key."\">Pr&eacute;sences</a> | <a href=\"participation.php?action=non_present&formations_key=".$key."\">Absences</a> | <a href=\"participation.php?action=not&formations_key=".$key."\">Pr&eacute;sence inconnue</a></p>\n";
				break;
			case "non_present":
				$html =
					"<p><span class=\"answer\">Listes de : </span><a href=\"participation.php?action=list&formations_key=".$key."\">Inscriptions</a> | <a href=\"participation.php?action=present&formations_key=".$key."\">Pr&eacute;sences</a> | <a href=\"participation.php?action=non_present&formations_key=".$key."\">Absences</a> | <a href=\"participation.php?action=not&formations_key=".$key."\">Pr&eacute;sence inconnue</a></p>\n";
				break;
			case "not":
				$html =
					"<p><span class=\"answer\">Listes de : </span><a href=\"participation.php?action=list&formations_key=".$key."\">Inscriptions</a> | <a href=\"participation.php?action=present&formations_key=".$key."\">Pr&eacute;sences</a> | <a href=\"participation.php?action=non_present&formations_key=".$key."\">Absences</a> | <a href=\"participation.php?action=not&formations_key=".$key."\">Pr&eacute;sence inconnue</a></p>\n";
				break;
			case "del_ins":
				$html =
					"<p><span class=\"answer\">Inscriptions : </span><a href=\"index.php?action=add_ins#part\">Ajouter</a> | <a href=\"index.php?action=mod_ins#part\">Modifier</a> | <a href=\"index.php?action=del_ins#part\">Supprimer</a> | <a href=\"index.php?action=read_ins#part\">Consulter</a></p>\n";
				break;
			case "mod_ins":
				$html =
					"<p><span class=\"answer\">Inscriptions : </span><a href=\"index.php?action=add_ins#part\">Ajouter</a> | <a href=\"index.php?action=mod_ins#part\">Modifier</a> | <a href=\"index.php?action=del_ins#part\">Supprimer</a> | <a href=\"index.php?action=read_ins#part\">Consulter</a></p>\n";
				break;
			case "read_ins":
				$html =
					"<p><span class=\"answer\">Inscriptions : </span><a href=\"index.php?action=add_ins#part\">Ajouter</a> | <a href=\"index.php?action=mod_ins#part\">Modifier</a> | <a href=\"index.php?action=del_ins#part\">Supprimer</a> | <a href=\"index.php?action=read_ins#part\">Consulter</a></p>\n";
				break;
			default:
				$html = "Liste d'inscriptions &agrave; une formation";
		}
		return $html;
	}

	// Fonction qui retourne le html pour le menu d'actions des formations
	function menuListesString($action) { 
		global $DB;
		$key = "";
		$blnIsSet = false;
		if (isset($_GET['formations_key']) && $_GET['formations_key']!=""){
			$key = $_GET['formations_key'];
			$blnIsSet = true;
		}		
		$html =
			"<div class=\"form\">\n".
			"	<form id=\"participations\" name=\"participations\" action=\"participation.php?action=".$action."\" method=\"get\">\n" .
			"		<p>Pour avoir la liste pour une formation seulement, veuillez la choisir ci-dessous.</p>".
			"		<input type=\"hidden\" name=\"action\" value=\"".$action."\" />\n".
			"		<select name=\"formations_key\" onchange=\"javascript:document.participations.submit();\">\n";

		if ($blnIsSet){
			$html .=
				"			<option value=\"0\" class=\"default\">- Toutes les formations -</option>\n";
		}
		else {
			$html .=
				"			<option value=\"-1\" class=\"default\">- Veuillez choisir une formation ou toutes les formations -</option>\n".
				"			<option value=\"0\">Toutes les formations</option>\n";
		}
		$html .= getFormationsOptionsString($key).
			"		</select>\n".
			"	</form>\n".
			"</div><br/>\n";
		return $html;
	}


	// Fonction qui retourne le html pour le titre du sous-menu
	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "list":
				$html = "<h2>Consulter la liste d'inscriptions aux formations</h2>";
				break;
			case "present":
				$html = "<h2>Consulter la liste des pr&eacute;sences aux formations</h2>";
				break;
			case "non_present":
				$html = "<h2>Consulter la liste des absences aux formations</h2>";
				break;
			case "not":
				$html = "<h2>Consulter la liste des pr&eacute;sences inconnues</h2>";
				break;
			case "del_ins":
				$html = "<h2>Supprimer une inscription &agrave; une formation</h2>\n";
				break;
			case "mod_ins":
				$html = "<h2>Modifier une inscription &agrave; une formation</h2>\n";
				break;
			case "read_ins":
				$html = "<h2>Consulter une inscription &agrave; une formation</h2>\n";
				break;
		}
		return $html;
	}

	function getFormationsOptionsString($key = ""){
		global $DB;
		$html = "";
		$DB->query(
			"SELECT `key`, `nom`, `date_debut` ".
			"FROM `formations` ".
			"WHERE `actif`='1' ".
			"ORDER BY `date_debut` DESC"
		);
		while($DB->next_record()) {
			$selected = "";		
			if ($key!="" && $key!="0" && $key==$DB->getField("key")){
				$selected = "selected=\"selected\" ";		
			}
				$html .= "<option ".$selected."value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . " (" . $DB->getField("date_debut") . ")</option>\n";
		}

		return $html;
	}

	function getFormationsKeyMenuString($action){
		$html = "";
		$formations_key = "";
		$html .= 
			"<div class=\"form\">\n".
			"	<form id=\"delete_inscription\" name=\"delete_inscription\" action=\"participation.php\" method=\"get\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"".$action."\" />\n".
			"		<select name=\"formations_key\" onchange=\"javascript:document.delete_inscription.submit();\">\n";
		if (!isset($_GET['formations_key'])){
			$html .=
				"			<option value=\"-1\" class=\"default\">- Veuillez choisir une formation ou toutes les formations -</option>\n";
			$html .=
				"			<option value=\"0\">Toutes les formations</option>\n";
		}
		else {
			$formations_key = $_GET['formations_key'];
			$html .=
				"			<option value=\"0\" class=\"default\">- Toutes les formations -</option>\n";
		}
		$html .= getFormationsOptionsString($formations_key).
			"		</select>\n".
			"		<br/><br/>\n";

		$html .= "	</form>\n".
			"</div>\n";
		
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à supprimer
	function getResultInscriptionsString($action) {
		global $DB;
		$orderBy = "`formations_participation`.`nom` ";
		$way = "ASC";
		$formations_key = "";

		$html = getFormationsKeyMenuString($action);
		
		if (isset($_GET['formations_key'])){
			$sql_forma_key = "";
			if ($_GET['formations_key']!="" && $_GET['formations_key']!="0"){
				$sql_forma_key = "AND `formations`.`key`='".$_GET['formations_key']. "' ";
			}
			if (isset($_GET['order_by']) && $_GET['order_by']!=""){
				switch($_GET['order_by']){
					case "nom":
						$orderBy = "`formations_participation`.`nom` ";
						$way = "ASC";
					break;
					case "etude":
						$orderBy = "`etudes`.`nom` ";
						$way = "ASC";
					break;
					case "formation":
						$orderBy = "`formations`.`nom` ";
						$way = "ASC";
					break;
					case "date":
						$orderBy = "`formations`.`date_debut` ";
						$way = "DESC";
					break;
					default:
						$orderBy = "`formations_participation`.`nom` ";
						$way = "ASC";
					break;
				}
			}
			$DB->query(
				"SELECT `formations`.`nom` AS `formations_nom`, `formations`.`key` AS `formations_key`, `formations`.`date_debut`, `formations_participation`.`nom`, " .
				"`formations_participation`.`prenom`, `formations_participation`.`key`, " .
				"`etudes`.`nom` AS `etudes_nom` ".
				"FROM `formations`, `formations_participation`, `etudes` ".
				"WHERE `formations`.`key`=`formations_participation`.`formations_key` " .
				$sql_forma_key.
				"AND `formations_participation`.`etudes_key`=`etudes`.`key` ".
				"GROUP BY `formations_participation`.`key` ".
				"ORDER BY ". $orderBy. $way
			);
			$nb = $DB->getNumRows();
	
			if ($nb>0){
				$html .=
					"<table class=\"resultsTable\">\n" .
					"	<tr>\n".
					"		<th><a href=\"participation.php?action=".$action."&order_by=nom&formations_key=".$_GET['formations_key']."#part\">Nom, pr&eacute;nom du participant</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=etude&formations_key=".$_GET['formations_key']."#part\">Nom de l'&eacute;tude</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=formation&formations_key=".$_GET['formations_key']."#part\">Nom de la formation</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=date&formations_key=".$_GET['formations_key']."#part\">Date de la formation</a></th>\n" .
					"		<th>&nbsp;</th>\n".
					"	</tr>\n";
			}
			$compteur = 0;
			
			while($DB->next_record()) {
				$key = $DB->getField("key");
				$color = "";
				if ($compteur%2==0) {
					$color = " class=\"altTr\"";			
				}
				$html .=
				"    <tr". $color .">\n" .
				"		<td>".$DB->getField("nom"). ", " .$DB->getField("prenom") ."</td>\n " .
				"		<td>". $DB->getField("etudes_nom")."</td>\n" .
				"		<td>". $DB->getField("formations_nom")."</td>\n" .
				"		<td>". $DB->getField("date_debut")."</td>\n" ;
	
				if ($action=="del_ins"){
					$html .=
						"		<td><a href=\"participation.php?action=".$action."&ins_key=".$key."&formations_key=".$_GET['formations_key']."#part\" title=\"Pour supprimer\" onclick=\"return areUSureUWantToDelete('".$DB->getField("nom"). ", " .$DB->getField("prenom") ."', ".$DB->getField("key") .");\">Supprimer</a></td>\n";
				}
				else if ($action=='read_ins'){
					$html .=
						"		<td><a href=\"inscriptions.php?action=read&ins_key=".$key."\" title=\"Pour visualiser\">Visualiser</a></td>\n";
				}
				else {
					$html .=
						"		<td><a href=\"inscriptions.php?action=modify&begin=1&ins_key=".$key."\" title=\"Pour modifier\">Modifier</a></td>\n";
				}
				$html .=
				"	</tr>\n";
				$compteur++;
			}
	
			if ($nb==0) {
				$html .= "<p>Aucune inscription.</p>";
			}
			else if ($nb>0) {
				$html .=
					"</table>";			
			}
		}
		return $html;

	}
	
	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à supprimer
	function listInscriptionsString($action) {
		global $DB;
		$orderBy = "`formations_participation`.`nom` ";
		$way = "ASC";
		$forma_key = "";
		$sqlforma_key = "";
		$sqlaction = "";

		$html = menuListesString($action); 
			
		if (isset($_GET['formations_key']) && $_GET['formations_key']!=""){

			if ($_GET['formations_key']!="0"){
				$forma_key = $_GET['formations_key'];
				$sqlforma_key = "AND `formations`.`key`= '" .$forma_key. "' ";
			}
		
			if (isset($_GET['order_by']) && $_GET['order_by']!=""){
				switch($_GET['order_by']){
					case "nom":
						$orderBy = "`formations_participation`.`nom` ";
						$way = "ASC";
					break;
					case "etude":
						$orderBy = "`etudes`.`nom` ";
						$way = "ASC";
					break;
					case "formation":
						$orderBy = "`formations`.`nom` ";
						$way = "ASC";
					break;
					case "date":
						$orderBy = "`formations`.`date_debut` ";
						$way = "DESC";
					break;
					case "date_ins":
						$orderBy = "`formations_participation`.`date_inscription` ";
						$way = "DESC";
					break;
					case "present":
						$orderBy = "`formations_participation`.`present` ";
						$way = "DESC";
					break;
					default:
						$orderBy = "`formations_participation`.`nom` ";
						$way = "ASC";
					break;
				}
			}
			if ($action=="non_present"){
				$sqlaction = "AND `formations_participation`.`present`='0' ";// .
							//"AND `formations`.`date_debut` <= CURRENT_DATE()";
				
			}
			else if ($action=="present"){
				$sqlaction = "AND `formations_participation`.`present`='1' ";// .
							//"AND `formations`.`date_debut` <= CURRENT_DATE()";
			}
			else if ($action=="not"){
				$sqlaction = "AND `formations_participation`.`present` IS NULL ";
				
			}
			
			$query = 
				"SELECT `formations`.`nom` AS `formations_nom`, `formations`.`key` AS `formations_key`, `formations`.`date_debut`, `formations_participation`.`nom`, " .
				"`formations_participation`.`prenom`, `formations_participation`.`key`, " .
				"`formations_participation`.`telephone`, `formations_participation`.`present`, `formations_participation`.`date_inscription`, " .
				"`etudes`.`nom` AS `etudes_nom` ".
				"FROM `formations`, `formations_participation`, `etudes` ".
				"WHERE `formations`.`key`=`formations_participation`.`formations_key` " .
				"AND `formations_participation`.`etudes_key`=`etudes`.`key` ".
				$sqlforma_key.
				$sqlaction.
				"GROUP BY `formations_participation`.`key` ".
				"ORDER BY ". $orderBy. $way;
			
			$DB->query($query);
	
			$nb = $DB->getNumRows();
	
			if ($nb>0){
				$html .=
					"<table class=\"resultsTable\">\n" .
					"	<tr>\n".
					"		<th><a href=\"participation.php?action=".$action."&order_by=nom&formations_key=".$_GET['formations_key']."\">Nom, pr&eacute;nom du participant</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=etude&formations_key=".$_GET['formations_key']."\">&Eacute;tude</a></th>\n" .
					"		<th><a href=\"javascript:;\">T&eacute;l&eacute;phone</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=date_ins&formations_key=".$_GET['formations_key']."\">Date d'inscription</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=formation&formations_key=".$_GET['formations_key']."\">Formation</a> <a href=\"participation.php?action=".$action."&order_by=date&formations_key=".$_GET['formations_key']."\">(date)</a></th>\n" .
					"		<th><a href=\"participation.php?action=".$action."&order_by=presen&formations_key=".$_GET['formations_key']."t\">Pr&eacute;sence</a></th>\n" .
					"		<th>&nbsp;</th>\n".
					"	</tr>\n";
			}
			$compteur = 0;
			
			while($DB->next_record()) {
				$key = $DB->getField("key");
				$color = "";
				$date_debut = $DB->getField("date_debut");
				if ($compteur%2==0) {
					$color = " class=\"altTr\"";			
				}
				$html .=
				"    <tr". $color .">\n" .
				"		<td><a href=\"inscriptions.php?action=read&ins_key=".$key."\" title=\"Pour visualiser\">".$DB->getField("nom"). ", " .$DB->getField("prenom") ."</a></td>\n " .
				"		<td>". $DB->getField("etudes_nom")."</td>\n" .
				"		<td>". $DB->getField("telephone")."</td>\n" .
				"		<td>". $DB->getField("date_inscription")."</td>\n" .
				"		<td>". $DB->getField("formations_nom")." (".$date_debut.")</td>\n";
				if ($action=='present' || $action=='non_present' || $action=='not' ){
					
					$presence = $DB->getField("present");
					$reponse = "";
					if (verifyDateEqualsOrOlderThanToday($date_debut)){
						// l'événement est déjà commencé
						if ($presence=="1"){
							$reponse =	"<b>Oui</b> | <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=0&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme absent\">Non</a>";
						}
						else if ($presence=="0"){
							$reponse =	"<a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=1&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme pr&eacute;sent\">Oui</a> |" .
										" <b>Non</b>";
						}
						else {
							$reponse =	"<a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=1&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme pr&eacute;sent\">Oui</a> |" .
										" <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=0&evenements_key=".$_GET['evenements_key']."\" title=\"Pour marquer comme absent\">Non</a>\n";
						}
					}
					else {
						// l'événement n'est pas encore commencé
						if ($presence=="1"){
							$reponse =	"<b>Oui</b> | <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=0&evenements_key=".$_GET['formations_key']."\" title=\"Pour marquer comme absent\" " .
										"onclick=\"return window.confirm('Cet &eacute;v&eacute;nement ne s\'est pas encore d&eacute;roul&eacute;. &Ecirc;tes-vous s&ucirc;r de vouloir marquer cette personne comme absente?');\">Non</a> |" .
										" <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=NULL&evenements_key=".$_GET['formations_key']."\" title=\"Pour marquer comme inconnu\">?</a>";
						}
						else if ($presence=="0"){
							$reponse =	"<a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=1&evenements_key=".$_GET['formations_key']."\" title=\"Pour marquer comme pr&eacute;sent\" " .
										"onclick=\"return window.confirm('Cet &eacute;v&eacute;nement ne s\'est pas encore d&eacute;roul&eacute;. &Ecirc;tes-vous s&ucirc;r de vouloir marquer cette personne comme pr&eacute;sente?');\">Oui</a> |" .
										" <b>Non</b> |" .
										" <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=NULL&evenements_key=".$_GET['formations_key']."\" title=\"Pour marquer comme inconnu\">?</a>";
						}
						else {
							$reponse =	"<a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=1&evenements_key=".$_GET['formations_key']."\" title=\"Pour marquer comme pr&eacute;sent\" " .
										"onclick=\"return window.confirm('Cet &eacute;v&eacute;nement ne s\'est pas encore d&eacute;roul&eacute;. &Ecirc;tes-vous s&ucirc;r de vouloir marquer cette personne comme pr&eacute;sente?');\">Oui</a> |" .
										" <a href=\"participation.php?action=".$action."&mod_key=".$key."&change_to=0&evenements_key=".$_GET['formations_key']."\" title=\"Pour marquer comme absent\" " .
										"onclick=\"return window.confirm('Cet &eacute;v&eacute;nement ne s\'est pas encore d&eacute;roul&eacute;. &Ecirc;tes-vous s&ucirc;r de vouloir marquer cette personne comme absente?');\">Non</a>\n";
						}
					}
					$html .=
					"		<td>". $reponse ."</td>\n" ;
				}
				else {
					
					$presence = $DB->getField("present");
					$reponse = "";
					if ($presence=="1"){
						$reponse = "Oui";
					}
					else if ($presence=="0"){
						$reponse = "Non";
					}
					else {
						$reponse = "Inconnue";
					}
					$html .=
					"		<td>". $reponse ."</td>\n" ;
				}
				$html .=
				"	</tr>\n";
				$compteur++;
			}
	
			if ($nb==0) {
				$html .= "<p>Aucune inscription.</p>";
			}
			else if ($nb>0) {
				$html .=
					"</table>";			
			}
		}
		$html .= "\n";
		
		return $html;

	}

	function verifyDateEqualsOrOlderThanToday($date){
		$blnOk = false;
		$arrDate = explode( "-", $date);
		if (date("Y-m-d", mktime(0, 0, 0, $arrDate[1], $arrDate[2], $arrDate[0]))<=date("Y-m-d")){
			
			$blnOk = true;
		}
		return $blnOk;
	}
	
	function setPresence($key, $action, $value){
		global $DB;
		$Participation = new Formation($DB, "", 1);
		$Participation->setTable("formations_participation");				
		$Participation->scanfields();
		$Participation->setQKey("key", $key);
		if( $value == "NULL" ) {
			$Participation->set("present", $value, 1 );
		}
		else {
			$Participation->set("present", $value);
		}
		
	}
	
	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à supprimer
	function deleteInscriptionsFromDB($participation_key) {
		global $DB;
		$html = "";
		
		$DB->query(
			"DELETE FROM `formations_participation` " .
			"WHERE `formations_participation`.`key`= '".$participation_key."'"
		);

		return $html;

	}

	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function javascriptString() { 
		$html =
			"<script type=\"text/javascript\" language=\"javascript\">\n" .
			"	String.prototype.trim = function(){ \n" .
			"		return this.replace(/^\s+|\s+$/g,'');\n" .
			"		}\n".
			
			"	function areUSureUWantToDelete(strName, key) {\n".
			"		var answer = confirm('Vous êtes sure de vouloir supprimer \\n'+ strName);\n".
			"		if (answer) {\n".
			"			return true;\n".
			"		}\n".
			"		else {\n".
			"			return false;\n".
			"		} \n" .
			"	}\n".
			"</script>\n" .
			"\n";
		return $html;
	}
	

?>
