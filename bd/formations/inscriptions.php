<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
		$Skin->assign("errors", $html);
		$Skin->display("formations.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
		$Skin->assign("errors", $html);
		$Skin->display("formations.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Formation");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	

	// ------------------
	// LE TRAITEMENT HTML
	// ------------------
	
	$formString = "";
	$successString = "";
	$errorString = "";
	$titreString = "";
	$begin = getorpost('begin');
	$action = getorpost('action');
	$key = getorpost('ins_key');


	switch ($action) {
		case "add" :
			$titreString = "<h1>Ajouter une inscription &agrave; une formation</h1>\n";
			if ($begin) {
				importForm("formations_inscription");
				$FormObject = new Form("", "", getForm($action));
				$FormObject->setValue("formations_key", getorpost('formations_key'));			
				$formString = $FormObject->returnForm();

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "formations/index.php?action=add_ins#part\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("formations_inscription");
					$FormObject = new Form("", getParametersFromPOST("add"), getForm($action));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "modify" :
			$titreString = "<h1>Modifier  une inscription &agrave; une formation</h1>\n";
			if ($begin) {
				importForm("formations_inscription");
				$FormObject = new Form("", "", getForm($action));
				$FormObject->setValue("ins_key", $key);			
				$FormObject->setValue("action", $action);
				$FormObject->setParameters(getAllFieldsFormationParticipation($key));
				$formString = $FormObject->returnFilledForm();

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Modifications compl&eacute;t&eacute;es avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "formations/index.php?action=mod_ins#part\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("formations_inscription");
					$parameters = getParametersFromPOST("modify");
					$FormObject = new Form("", $parameters, getForm($action));
					$FormObject->setValue("ins_key", $key);
					$FormObject->setValue("action", $action);
					$FormObject->setParameters($parameters);
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "read" :
			$titreString = "<h1>Consulter une inscription</h1>\n";
			importForm("formations_inscription");
			$FormObject = new Form("", "", getForm($action));
			$parameters = getAllFieldsFormationParticipation($key, "read");
			$FormObject->setParameters($parameters);
			$FormObject->setAllHidden();
			$formString = $FormObject->returnFilledForm();			
			break;

	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
	$Skin->assign("page", "formations");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("titre", $titreString);
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("formations.tpl");

	$DB->close();
	

	// -------------
	// LES FONCTIONS
	// -------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "PME Inter : Administration : Inscriptions aux formations : Ajout";
				break;
			case "modify":
				$title = "PME Inter : Administration : Inscriptions aux formations : Modification";
				break;
			default:
				$title = "PME Inter : Administration : Inscriptions aux formations";
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "modify":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "read":
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "modify":
				break;

			case "modify":
				break;

			case "read":
				break;
		}
		return $html;
	}

	// Fonction qui filtre les POST provenant d'un formulaire d'évaluation
	// Retourne une string html d'erreurs OU retourne "" si aucune n'est détectée
	function validateFormValues($action) {
		$errorString = "";
		global $DB, $EMAIL_FORMAT_PATTERN;
		switch($action){
			case "add":
				// le nom de l'événement doit être entré
				if ($_POST['etudes_key']=="0"){
					$errorString .= _error("<a href=\"javascript: showErrorField('etudes_key');\">Veuillez choisir une &eacute;tude</a><br />");
				}
				if (trim($_POST['prenom'])==""){
					$errorString .= _error("<a href=\"javascript: showErrorField('prenom');\">SVP entrez le pr&eacute;nom du participant</a><br />");
				}
				if (trim($_POST['nom'])==""){
					$errorString .= _error("<a href=\"#nom_v\" onclick=\"javascript: showErrorField('nom');\">SVP entrez le nom du participant</a><br />");
				}
				if (trim($_POST['courriel'])!=""){
					if (eregi('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$', trim($_POST['courriel']))){
					}
					else {
						$errorString .= _error("<a href=\"#courriel_v\" onclick=\"javascript: showErrorField('courriel');\">SVP entrez une adresse courriel valide</a><br />");
					}
				}
			break;
			
			case "modify":
				// le nom de l'événement doit être entré
				if ($_POST['etudes_key']=="0"){
					$errorString .= _error("<a href=\"javascript: showErrorField('etudes_key');\">Veuillez choisir une &eacute;tude</a><br />");
				}
				if (trim($_POST['prenom'])==""){
					$errorString .= _error("<a href=\"javascript: showErrorField('prenom');\">SVP entrez le pr&eacute;nom du participant</a><br />");
				}
				if (trim($_POST['nom'])==""){
					$errorString .= _error("<a href=\"#nom_v\" onclick=\"javascript: showErrorField('nom');\">SVP entrez le nom du participant</a><br />");
				}
				if (trim($_POST['courriel'])!=""){
					if (eregi('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$', trim($_POST['courriel']))){
					}
					else {
						$errorString .= _error("<a href=\"#courriel_v\" onclick=\"javascript: showErrorField('courriel');\">SVP entrez une adresse courriel valide</a><br />");
					}
				}
			break;
		}
		return $errorString;
	}
	
	// Fonction qui met à jour les valeurs dela DB à partir des valeurs de POST provenant du formulaire
	// Retourne une string html d'erreurs OU retourne "" si aucune n'est détectée
	function updateDbValues($action) {
		global $DB;
		$errorString = "";

		switch ($action) {
			case "add":
				$Participation  = new Formation($DB, "", "");
				$Participation->setTable("formations_participation");				
				$Participation->scanfields();
				$Participation->insertmode();

				$Participation->set("nom", trim($_POST['nom']));
				$Participation->set("prenom", trim($_POST['prenom']));
				$Participation->set("etudes_key", trim($_POST['etudes_key']));
				$Participation->set("formations_key", trim($_POST['formations_key']));
				$Participation->set("telephone", trim($_POST['telephone']));
				$Participation->set("courriel", trim($_POST['courriel']));
				$Participation->set("date_inscription", date('Y-m-d'));

				if (!$Participation->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion.");
				}
			break;

			case "modify":
				$Participation  = new Formation($DB, "", "");
				$Participation->setTable("formations_participation");				
				$Participation->scanfields();
				$Participation->setQKey("key", $_POST['ins_key']);			

				$Participation->set("nom", trim($_POST['nom']));
				$Participation->set("prenom", trim($_POST['prenom']));
				$Participation->set("etudes_key", trim($_POST['etudes_key']));
				$Participation->set("telephone", trim($_POST['telephone']));
				$Participation->set("courriel", trim($_POST['courriel']));

				if (!$Participation->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de la modification.");
				}
			break;
		}
				
		return $errorString;		
	}

	
	function getAllFieldsFormationParticipation($key, $read=""){
		global $DB;

		$Participation = new Formation($DB, "", 0);
		$Participation->setTable("formations_participation");				
		$Participation->scanfields();
		$Participation->setQKey("key", $key);
		$Participation->loadAll();
		if ($read=="read"){
			$forma_key = $Participation->get('formations_key');
			$Participation->set('formations_key', getFormationsName($read, $forma_key));
			$etudes_key = $Participation->get('etudes_key');
			$Participation->set('etudes_key', getEtudesName($etudes_key));
		}

		return $Participation->getAll();
	}
	
	function getParametersFromPOST($action) {
		$parameters = array();
		switch ($action) {
			case "add":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;			
			case "modify":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;	
		}
		return $parameters;
	}

	function getFormationsName($action, $forma_key){
		global $DB;
		if ($forma_key!= "" && $forma_key!= "0"){
			$Formation = new Formation($DB, "", 0);
			$Formation->scanfields();
			$Formation->setQKey("key", $forma_key);
			$Formation->loadAll();
			$actif = "";
			if ($Formation->get("actif")==0){
				$actif = " - Supprim&eacute;e";
			}
			$nomDate = $Formation->get("nom") . " (". $Formation->get("date_debut").")".$actif;

			return $nomDate;

		}
		else {
			return "";
		}
	}
	
	function getFormationsKey($ins_key){
		global $DB;
		$html = "";

		$DB->query(
			"SELECT `formations_key` ".
			"FROM `formations_participation` ".
			"WHERE `key`='".$ins_key."' "
		);
		while($DB->next_record()) {
			$html = $DB->getField("formations_key");
		}
		return $html;
	}

	function getEtudesName($etudes_key){
		global $DB;
		if ($etudes_key!= "" && $etudes_key!= "0"){
			$Etude = new Formation($DB, "", 0);
			$Etude->setTable("etudes");				
			$Etude->scanfields();
			$Etude->setQKey("key", $etudes_key);
			$Etude->load('nom');
			return $Etude->get('nom');
		}
		else {
			return "";
		}
	}
	
?>
