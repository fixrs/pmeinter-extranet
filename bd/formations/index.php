<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
		$Skin->assign("errors", $html);
		$Skin->display("formations.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
		$Skin->assign("errors", $html);
		$Skin->display("formations.tpl");
		exit;	
	}	
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Formation");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$successString = "";
	$errorString = "";
	$formString = "";

	// ÉTAPE #1
	// Traitement des GET provenant de la sélection d'une action dans le PREMIER menu $menuString
	switch ($_GET['action']) {
		case "add":
			// Création d'une évalutation dans la DB
			//$forma_key = createFormationsIntoDB();
			// Redirection vers la page du formulaire pour ajouter une formation
			header("Location: formations.php?action=add&begin=1&forma_key=" . $forma_key);
			exit;

		case "modify":
			$formString .= modifyFormationsString();
			break;

		case "delete":
			$formString .= deleteFormationsString();
			break;

		case "read":
			$formString .= readFormationsString();
			break;

		case "del_ins":
			$formString .= javascriptString().getFormationsKeyMenuString($_GET['action']);
			break;
			
		case "mod_ins":
			$formString .= javascriptString().getFormationsKeyMenuString($_GET['action']);
			break;
			
		case "add_ins":
			$formString .= javascriptString().addInscriptionsString();
			break;
			
		case "read_ins":
			$formString .= javascriptString().getFormationsKeyMenuString($_GET['action']);
			break;
	}

	// ÉTAPE #2
	// Traitement des POST provenant de la sélection d'un item sélectionné dans un dropdown APRES le menu $menuString
	switch ($_POST['action']) {
		case "modify":
			// Si aucune formation sélectionnée, Alors on réaffiche
			if ($_POST['forma_key'] == "0") {
				$formString .= modifyFormationsString();
				$errorString .= _error("Veuillez choisir une formation dans la liste");
			// Autrement on redirige vers formations.php
			} else {
				header("Location: formations.php?action=modify&begin=1&forma_key=" . $_POST['forma_key'] . "");
				exit;
			}
			break;

		case "delete":
			// Si aucune formation sélectionnée, Alors on réaffiche
			if ($_POST['forma_key'] == "0") {
				$formString .= deleteFormationsString();
				$errorString .= _error("Veuillez choisir une formation dans la liste");
			// Autrement on procède à la suppression
			} else {
				if (isset($_POST['confirmation'])){
					
					deleteFormationsFromDB($_POST['forma_key']);
					$formString .= deleteFormationsString();
					$successString = _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s");
				}
				else {
					$formString .= deleteFormationsString();
					$errorString .= _error("Pour supprimer, veuillez cocher la case de confirmation");
				}
			}
			break;

		case "read":
			// Si aucune formation sélectionnée, Alors on réaffiche
			if ($_POST['forma_key'] == "0") {
				$formString .= modifyFormationsString();
				$errorString .= _error("Veuillez choisir une formation dans la liste");
			// Autrement on redirige vers formations.php
			} else {
				header("Location: formations.php?action=read&begin=1&forma_key=" . $_POST['forma_key'] . "");
				exit;
			}
			break;

		case "add_ins":
			// Si aucune formation sélectionnée, Alors on réaffiche
			if ($_POST['forma_key'] == "0") {
				$formString .= javascriptString().addInscriptionsString();
				$errorString .= _error("Veuillez choisir une formation dans la liste");
			// Autrement on redirige vers formations.php
			} else {
				header("Location: inscriptions.php?action=add&begin=1&formations_key=" . $_POST['forma_key'] . "");
				exit;
			}
			break;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
	$Skin->assign("page", "formations");
	$Skin->assign("title", pageTitleString(getorpost('action')));
	$Skin->assign("menu", menuFormationsString());
	$Skin->assign("menu_title", menuTitleString(getorpost('action')));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("formations.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "Ajout de formations";
				break;
			case "modify":
				$title = "Modification de formations";
				break;
			case "delete":
				$title = "Suppression de formations";
				break;
			case "read":
				$title = "Consultation de formations";
				break;
			case "del_ins":
				$title = "Suppression d'inscriptions";
				break;
			case "add_ins":
				$title = "Ajout d'inscriptions &agrave; une formation";
				break;
			case "mod_ins":
				$title = "Modification d'inscriptions &agrave; une formation";
				break;
			case "read_ins":
				$title = "Consultation d'inscriptions &agrave; une formation";
				break;
			default:
				$title = "Gestion de formations";
		}
		return $title;
	}

	// Fonction qui retourne le html pour le menu d'actions des formations
	function menuFormationsString() { 
		$html =
			"<h1>Gestions des formations</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"index.php?action=add\">Ajouter une formation</a></li>\n".
			"		<li><a href=\"index.php?action=modify\">Modifier une formation</a></li>\n".
			"		<li><a href=\"index.php?action=delete\">Supprimer une formation</a></li>\n".
			"		<li><a href=\"index.php?action=read\">Consulter une formation</a></li>\n".
			"		<li><a href=\"search.php?action=search\">Rechercher des formations</a></li>\n".
			"	</ul>\n".
			"</div>\n".
			"<a name=\"part\"></a><h1>Participation aux formations</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n".
			"		<li><a href=\"index.php?action=add_ins#part\">Ajouter une inscription &agrave; une formation</a></li>\n".
			"		<li><a href=\"index.php?action=mod_ins#part\">Modifier une inscription &agrave; une formation</a></li>\n".
			"		<li><a href=\"index.php?action=del_ins#part\">Supprimer une inscription &agrave; une formation</a></li>\n".
			"		<li><a href=\"index.php?action=read_ins#part\">Consulter une inscription &agrave; une formation</a></li>\n".
			"		<li><a href=\"participation.php?action=list\">Liste des inscriptions, absences, pr&eacute;sences</a></li>\n";
		$html .=
			"	</ul>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le titre du sous-menu
	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "modify":
				$html .= "<h2>Modifier une formation</h2>\n";
				break;
			case "delete":
				$html .= "<h2>Supprimer une formation</h2>\n";
				break;
			case "read":
				$html .= "<h2>Consulter une formation</h2>\n";
				break;
			case "del_ins":
				$title = "<h2>Supprimer une inscription</h2>\n";
				break;
			case "add_ins":
				$title = "<h2>Ajouter une inscription &agrave; une formation</h2>\n";
				break;
			case "mod_ins":
				$title = "<h2>Modifier une inscription &agrave; une formation</h2>\n";
				break;
			case "read_ins":
				$title = "<h2>Consulter une inscription &agrave; une formation</h2>\n";
				break;
		}
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à modifier
	function modifyFormationsString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"		<select name=\"forma_key\">\n".
			"			<option value=\"0\" class=\"default\">- Choisir une formation -</option>\n";
		$html .= getFormationsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à consulter
	function readFormationsString() { 
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"		<select name=\"forma_key\">\n".
			"			<option value=\"0\" class=\"default\">- Choisir une formation -</option>\n";
		$html .= getFormationsOptionsString().
			"		</select>\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à supprimer
	function deleteFormationsString() {
		global $DB;
		$html =
			"<div class=\"form\">\n".
			"	<form action=\"index.php\" method=\"post\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"		<select name=\"forma_key\">\n".
			"			<option value=\"0\" class=\"default\">- Choisir une formation -</option>\n";
		
		$html .= getFormationsOptionsString().
			"		</select> \n".
			"		<input type=\"checkbox\" name=\"confirmation\" value=\"confirmation\" /> Confirmation<br /><br />\n".
			"		<input type=\"submit\" value=\"Supprimer\" class=\"submit\" />\n".
			"	</form>\n".
			"</div>\n";
		return $html;
	}

	function getFormationsOptionsString($key=""){
		global $DB;
		$html = "";

		$DB->query(
			"SELECT `key`, `nom`, `date_debut` ".
			"FROM `formations` ".
			"WHERE `actif`='1' ".
			"ORDER BY `date_debut` DESC"
		);
		while($DB->next_record()) {
			$selected = "";		
			if ($key!="" && $key==$DB->getField("key")){
				$selected = "selected=\"selected\" ";		
			}
			$html .= "<option ".$selected."value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . " (" . $DB->getField("date_debut") . ")</option>\n";
		}

		return $html;
	}

	// Fonction qui supprime une formation de la DB
	function deleteFormationsFromDB($key) {
		global $DB;
		
		$Formation = new Formation($DB, "", 1);
		$Formation->setQKey("key", $key);
		$Formation->set('actif', '0');		
	}

	function getFormationsKeyMenuString($action){
		$html = "";
		$formations_key = "";
		switch ($action){
			case "del_ins":
				$html .= "<h2>Supprimer une inscription</h2>";
			break;
	
			case "read_ins":
				$html .= "<h2>Consulter une inscription</h2>";
			break;
	
			default:
				$html .= "<h2>Modifier une inscription</h2>";
			break;
			
		}
		$html .= 
			"<div class=\"form\">\n".
			"	<form id=\"delete_inscription\" name=\"delete_inscription\" action=\"participation.php\" method=\"get\">\n".
			"		<input type=\"hidden\" name=\"action\" value=\"".$action."\" />\n".
			"		<select name=\"formations_key\" onchange=\"javascript:document.delete_inscription.submit();\">\n";
		if (!isset($_GET['formations_key'])){
			$html .=
				"			<option value=\"-1\" class=\"default\">- Veuillez choisir une formation ou toutes les formations -</option>\n";
			$html .=
				"			<option value=\"0\">Toutes les formations</option>\n";
		}
		else {
			$formations_key = $_GET['formations_key'];
			$html .=
				"			<option value=\"0\" class=\"default\">- Toutes les formations -</option>\n";
		}
		$html .= getFormationsOptionsString($formations_key).
			"		</select>\n".
			"		<br/><br/>\n";

		$html .= "	</form>\n".
			"</div>\n";
		
		return $html;
	}
	
	// Fonction qui retourne le html pour le dropdown de la sélection d'une formation à consulter
	function addInscriptionsString() { 
		global $DB;
		$DB->query(
			"SELECT `key`, `nom`, `date_debut`, `actif` ".
			"FROM `formations` ".
			"WHERE `actif`='1' ".
#			"AND `date_fin_inscription`>=CURRENT_DATE() ".
			"ORDER BY `date_debut` DESC"
		);
		$nb = $DB->getNumRows();
		$html =
			"<h2>Ajout d'une inscription &agrave; une formation</h2>\n";
		if ($nb>0){
			$html .=
				"<div class=\"form\">\n".
				"	<form action=\"index.php\" method=\"post\">\n".
				"		<input type=\"hidden\" name=\"action\" value=\"add_ins\" />\n".
				"		<select name=\"forma_key\">\n".
				"			<option value=\"0\" class=\"default\">- Choisir la formation pour laquelle vous voulez faire une inscription -</option>\n";
			while($DB->next_record()) {
				$actif = "";
/*				if ($DB->getField("actif")==0){
					$actif = " - Supprim&eacute;e";
				}*/
				$html .= "<option value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . " (" . $DB->getField("date_debut") . ")".$actif."</option>\n";
			}
	
			$html .= 
				"		</select>\n".
				"		<br /><br />\n".
				"		<input type=\"submit\" value=\"Afficher\" class=\"submit\" />\n".
				"	</form>\n".
				"</div>\n";
		}
		else {
			$html .= "Aucune formation n'accepte de nouvelles inscriptions";
		}
		return $html;
	}

	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function javascriptString() { 
		$html =
			"<script type=\"text/javascript\" language=\"javascript\">\n" .
			"	String.prototype.trim = function(){ \n" .
			"		return this.replace(/^\s+|\s+$/g,'');\n" .
			"		}\n".
			
			"	function areUSureUWantToDelete(strName, key) {\n".
			"		var answer = confirm('Vous êtes sure de vouloir supprimer \\n'+ strName);\n".
			"		if (answer) {\n".
			"			return true;\n".
			"		}\n".
			"		else {\n".
			"			return false;\n".
			"		} \n" .
			"	}\n".
			"</script>\n" .
			"\n";
		return $html;
	}
	
?>
