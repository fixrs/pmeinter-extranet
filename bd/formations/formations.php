<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
		$Skin->assign("errors", $html);
		$Skin->display("formations.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");
		$Skin->assign("errors", $html);
		$Skin->display("formations.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");
	import("com.pmeinter.Formation");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	

	// ------------------
	// LE TRAITEMENT HTML
	// ------------------
	
	$formString = "";
	$successString = "";
	$errorString = "";
	$titreString = "";
	$begin = getorpost('begin');
	$action = getorpost('action');
	$key = getorpost('forma_key');


	switch ($action) {
		case "add" :
			$titreString = "<h2>Ajouter une formation</h2>\n";
			if ($begin) {
				importForm("formations");
				$FormObject = new Form("", "", getForm($action));
				$formString = $FormObject->returnForm();

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "formations/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("formations");
					$FormObject = new Form("", getParametersFromPOST($action), getForm($action));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "modify" :
			$titreString = "<h2>Modifier une formation</h2>\n";
			if ($begin) {
				importForm("formations");
				$FormObject = new Form("", "", getForm($action));
				$FormObject->setValue("forma_key", $key);			
				$FormObject->setValue("action", $action);
				$FormObject->setParameters(getAllFieldsFormation($key));
				$formString = $FormObject->returnFilledForm();

			} else {
				$errorString = validateFormValues($action);
				if ($errorString == "") {
					$errorString = updateDbValues($action);
					if ($errorString == "") {
						$successString = _success("Modifications compl&eacute;t&eacute;es avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "formations/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}
				} else {
					importForm("formations");
					$parameters = getParametersFromPOST($action);
					$FormObject = new Form("", $parameters, getForm($action));
					$FormObject->setValue("forma_key", $key);
					$FormObject->setValue("action", $action);
					$FormObject->setParameters($parameters);
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "read" :
			$titreString = "<h2>Consulter une formation</h2>\n";
			importForm("formations");
			$FormObject = new Form("", "", getForm($action));
			$parameters = getAllFieldsFormation($key, "read");
			$FormObject->setParameters($parameters);
			$FormObject->setAllHidden();
			$formString = $FormObject->returnFilledForm();			
			break;

	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "formations");	
	$Skin->assign("page", "formations");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("titre", $titreString);
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("formations.tpl");

	$DB->close();
	

	// -------------
	// LES FONCTIONS
	// -------------

	// Fonction qui retourne le titre pour la page
	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "PME Inter : Administration : Formations : Ajout";
				break;
			case "modify":
				$title = "PME Inter : Administration : Formations : Modification";
				break;
			case "delete":
				$title = "PME Inter : Administration : Formations : Suppression";
				break;
			case "read":
				$title = "PME Inter : Administration : Formations : Consultation";
				break;
			case "search":
				$title = "PME Inter : Administration : Formations : Recherche";
				break;
			default:
				$title = "PME Inter : Administration : Formations";
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "modify":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>";
				break;

			case "read":
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "modify":
				break;

			case "modify":
				break;

			case "read":
				break;
		}
		return $html;
	}

	// Fonction qui filtre les POST provenant d'un formulaire d'évaluation
	// Retourne une string html d'erreurs OU retourne "" si aucune n'est détectée
	function validateFormValues( $action) {
		$errorString = "";
		global $DB;
		$blnPremiereDate = true;
		$blnNomManquant = false;
		switch($action){
			case "add":
				// le nom de l'événement doit être entré
				if (trim($_POST['nom'])==""){
					$errorString = _error("<a href=\"javascript: showErrorField('nom');\">SVP entrez le nom de la formation</a><br />");
					$blnNomManquant = true;
				}
				if (trim($_POST['date_fin_inscription'])==""){
					$errorString .= _error("<a href=\"javascript: showErrorField('date_fin_inscription');\">SVP entrez la date de fin d'inscription &agrave; cet &eacute;v&eacute;nement</a><br />");
				}
				else {
					//on vérifie si la date est une vraie date
					if (!verifyDate(trim($_POST['date_fin_inscription']))){
						$errorString .= _error("<a href=\"javascript: showErrorField('date_fin_inscription');\">Le format de la date limite d'inscription doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						$blnPremiereDate = false;
					}
				}
				//la date de début doit être entrée
				if (trim($_POST['date_debut'])==""){
					$errorString .= _error("<a href=\"#date_debut_v\" onclick=\"javascript: showErrorField('date_debut');\">SVP entrez la date de la formation</a><br />");
				}
				else {
					//on vérifie si la date est une vraie date
					if (!verifyDate(trim($_POST['date_debut']))){
						$errorString .= _error("<a href=\"#date_debut_v\" onclick=\"javascript: showErrorField('date_debut');\">Le format de la date doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						$blnPremiereDate = false;
					}
				}
				
				if ( !$blnNomManquant && $action=="add" && verifyIfFormationNameAndDateExists (trim($_POST['nom']), trim($_POST['date_debut']) ) ){
					$errorString .= _error("<a href=\"javascript: showErrorField('nom');\">Le nom de la formation ainsi que sa date de d&eacute;but sont identiques &agrave; une formation d&eacute;j&agrave; sauvegard&eacute;e</a><br />");			
				}
				
				
				//si la date de fin a été entrée, on vérifie le format
				if (trim($_POST['date_fin'])!=""){
					//s'il n'y a pas de message de format de date de la première date
					if ($blnPremiereDate) {
						//on vérifie si la date est une vraie date
						if (!verifyDate(trim($_POST['date_fin']))){
							$errorString .= _error("<a href=\"#date_fin_v\" onclick=\"javascript: showErrorField('date_fin');\">Le format de la date doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						}
					}
				}
		
				//si la date de fin a été entrée, on vérifie le format
				if (trim($_POST['nombre_jours'])!=""){
					if (numero_is_numeric(trim($_POST['nombre_jours']))!=1){
						$errorString .= _error("<a href=\"#nombre_jours_v\" onclick=\"javascript: showErrorField('nombre_jours');\">Le nombre de jours doit &ecirc;tre un chiffre</a><br />");			
					}
				}
		
				//si la date de fin a été entrée, on vérifie le format
				if (trim($_POST['prix'])!=""){
					if (numero_is_numeric(trim($_POST['prix']))!=1){
						$errorString .= _error("<a href=\"#prix_v\" onclick=\"javascript: showErrorField('prix');\">Le prix doit &ecirc;tre un nombre</a><br />");			
					}			
				}

			break;
			
			case "modify":
				// le nom de l'événement doit être entré
				if (trim($_POST['nom'])==""){
					$errorString = _error("<a href=\"javascript: showErrorField('nom');\">SVP entrez le nom de la formation</a><br />");
					$blnNomManquant = true;
				}
				if (trim($_POST['date_fin_inscription'])==""){
					$errorString .= _error("<a href=\"javascript: showErrorField('date_fin_inscription');\">SVP entrez la date de fin d'inscription &agrave; cet &eacute;v&eacute;nement</a><br />");
				}
				else {
					//on vérifie si la date est une vraie date
					if (!verifyDate(trim($_POST['date_fin_inscription']))){
						$errorString .= _error("<a href=\"javascript: showErrorField('date_fin_inscription');\">Le format de la date limite d'inscription doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						$blnPremiereDate = false;
					}
				}
				//la date de début doit être entrée
				if (trim($_POST['date_debut'])==""){
					$errorString .= _error("<a href=\"#date_debut_v\" onclick=\"javascript: showErrorField('date_debut');\">SVP entrez la date de la formation</a><br />");
				}
				else {
					//on vérifie si la date est une vraie date
					if (!verifyDate(trim($_POST['date_debut']))){
						$errorString .= _error("<a href=\"#date_debut_v\" onclick=\"javascript: showErrorField('date_debut');\">Le format de la date doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						$blnPremiereDate = false;
					}
				}
				
				if ( !$blnNomManquant && $action=="add" && verifyIfFormationNameAndDateExists (trim($_POST['nom']), trim($_POST['date_debut']) ) ){
					$errorString .= _error("<a href=\"javascript: showErrorField('nom');\">Le nom de la formation ainsi que sa date de d&eacute;but sont identiques &agrave; une formation d&eacute;j&agrave; sauvegard&eacute;e</a><br />");			
				}
				
				
				//si la date de fin a été entrée, on vérifie le format
				if (trim($_POST['date_fin'])!=""){
					//s'il n'y a pas de message de format de date de la première date
					if ($blnPremiereDate) {
						//on vérifie si la date est une vraie date
						if (!verifyDate(trim($_POST['date_fin']))){
							$errorString .= _error("<a href=\"#date_fin_v\" onclick=\"javascript: showErrorField('date_fin');\">Le format de la date doit &ecirc;tre <b>AAAA-MM-JJ</b></a><br />");
						}
					}
				}
		
				//si la date de fin a été entrée, on vérifie le format
				if (trim($_POST['nombre_jours'])!=""){
					if (numero_is_numeric(trim($_POST['nombre_jours']))!=1){
						$errorString .= _error("<a href=\"#nombre_jours_v\" onclick=\"javascript: showErrorField('nombre_jours');\">Le nombre de jours doit &ecirc;tre un chiffre</a><br />");			
					}
				}
		
				//si la date de fin a été entrée, on vérifie le format
				if (trim($_POST['prix'])!=""){
					if (numero_is_numeric(trim($_POST['prix']))!=1){
						$errorString .= _error("<a href=\"#prix_v\" onclick=\"javascript: showErrorField('prix');\">Le prix doit &ecirc;tre un nombre</a><br />");			
					}			
				}
			break;
		}
		return $errorString;
	}
	
	// Fonction qui met à jour les valeurs dela DB à partir des valeurs de POST provenant du formulaire
	// Retourne une string html d'erreurs OU retourne "" si aucune n'est détectée
	function updateDbValues($action) {
		global $DB;
		$errorString = "";

		switch ($action) {
			case "add":
				$Formation = new Formation($DB, "", "");
				
				$Formation->scanfields();
				$Formation->insertmode();

				$Formation->set("key", trim($_POST['key']));
				$Formation->set("nom", trim($_POST['nom']));
				$Formation->set("date_fin_inscription", trim($_POST['date_fin_inscription']));		
				$Formation->set("date_debut", trim($_POST['date_debut']));		
		
				//si le champs de la date de fin n'est pas vide 
				if (trim($_POST['date_fin'])!="") {
					$Formation->set("date_fin", trim($_POST['date_fin']));			
				}
				$Formation->set("date_precision", trim($_POST['date_precision']));
				$Formation->set("heure", trim($_POST['heure']));
				$Formation->set("nombre_jours", trim($_POST['nombre_jours']));
				$Formation->set("coordonnees", trim($_POST['coordonnees']));
				$Formation->set("salle", trim($_POST['salle']));
				$Formation->set("nom_responsable", trim($_POST['nom_responsable']));		
				$Formation->set("telephone_responsable", trim($_POST['telephone_responsable']));
				$Formation->set("description", trim($_POST['description']));
				$Formation->set("conferencier", trim($_POST['conferencier']));
				$Formation->set("prix", trim($_POST['prix']));
				$Formation->set("notes", trim($_POST['notes']));
				$Formation->set("domaines_daffaires_key", trim($_POST['domaines_daffaires_key']));
				$Formation->set("actif", '1');

				if (!$Formation->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion.");
				}
			break;

			case "modify":
				$Formation = new Formation($DB, "", "");
				
				$Formation->scanfields();
				$Formation->setQKey("key", $_POST['forma_key']);			

				$Formation->set("key", trim($_POST['key']));
				$Formation->set("nom", trim($_POST['nom']));
				$Formation->set("date_fin_inscription", trim($_POST['date_fin_inscription']));		
				$Formation->set("date_debut", trim($_POST['date_debut']));		
		
				//si le champs de la date de fin n'est pas vide 
				if (trim($_POST['date_fin'])!="") {
					$Formation->set("date_fin", trim($_POST['date_fin']));			
				}
				$Formation->set("date_precision", trim($_POST['date_precision']));
				$Formation->set("heure", trim($_POST['heure']));
				$Formation->set("nombre_jours", trim($_POST['nombre_jours']));
				$Formation->set("coordonnees", trim($_POST['coordonnees']));
				$Formation->set("salle", trim($_POST['salle']));
				$Formation->set("nom_responsable", trim($_POST['nom_responsable']));		
				$Formation->set("telephone_responsable", trim($_POST['telephone_responsable']));
				$Formation->set("description", trim($_POST['description']));
				$Formation->set("conferencier", trim($_POST['conferencier']));
				$Formation->set("prix", trim($_POST['prix']));
				$Formation->set("notes", trim($_POST['notes']));
				$Formation->set("domaines_daffaires_key", trim($_POST['domaines_daffaires_key']));
				$Formation->set("actif", '1');

				if (!$Formation->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de la modification.");
				}
			break;
		}
				
		return $errorString;		
	}

	
	/**
	* function
	*
	* Description: to verify if the date is a real date
	*	
	* @access public
	* @param $date the date entered in the form (has to be YYYY-MM-DD)
	* 
	* @return true or false
	*/		
	
	function verifyDate($date){
		$blnOk = true;
		if (strlen($date)==10){

			if (strpos($date, "-")=== false){
				$blnOk = false;
			}
			else {
				$arrDate = explode( "-", $date);
				if ($date != date("Y-m-d", mktime(0, 0, 0, $arrDate[1], $arrDate[2], $arrDate[0]))){
					
					$blnOk = false;
				}
			}
		}
		else {
			$blnOk = false;
		}
		return $blnOk;
	}
	
	/**
	* function
	*
	* @access public
	* @param primary key value
	* 
	* @return void
	*/		
	
	function getAllFieldsFormation($forma_key, $read=""){
		global $DB;

		$Formation = new Formation($DB, "", 0);
		$Formation->scanfields();
		$Formation->setQKey("key", $forma_key);
		$Formation->loadAll();
		if ($Formation->get("nombre_jours")=="0"){
			$Formation->set("nombre_jours", "");
		}
		if ($Formation->get("date_fin")=="0000-00-00"){
			$Formation->set("date_fin", "");
		}
		if ($Formation->get("prix")=="0.00"){
			$Formation->set("prix", "");			
		}
		if ($read=="read"){
			
			if ($Formation->get("prix")!=""){
				$prix = $Formation->get("prix");
				$Formation->set("prix", $prix." $");			
			}
			if ($Formation->get("domaines_daffaires_key")!="0"){
				$domaineKey = $Formation->get("domaines_daffaires_key");
				switch($domaineKey){
					case "1" :
						$Formation->set("domaines_daffaires_key", "Droit des affaires");
					break;
					case "2" :
						$Formation->set("domaines_daffaires_key", "Droit des personnes");
					break;
					case "3" :
						$Formation->set("domaines_daffaires_key", "Droit immobilier");
					break;
				}
			}
			else {
				$Formation->set("domaines_daffaires_key", "");
			}
		}
		return $Formation->getAll();
	}
	
		/**
	* function
	* 
	* @access public
	* @param number value
	* 
	* @return 1 or empty
	*/		
	
	function numero_is_numeric($value)
	{
		return (preg_match ("/^(-){0,1}([0-9]+)(,[0-9][0-9][0-9])*([.][0-9]){0,1}([0-9]*)$/", $value) == 1);
	}
	
	function getAllDomainesDAffaires(){
		global $DB;
		$html =	
			"		<select name=\"domaines_daffaires_key\">\n".
			"			<option id=\"domaines_daffaires_key\" value=\"0\">- Choisir un domaine d'affaires, si applicable -</option>\n";
		$DB->query(
			"SELECT `key`, `nom` ".
			"FROM `domaines_daffaires`".
			"ORDER BY `key` ASC"
		);
		while($DB->next_record()) {
			$html .= "<option id=\"domaines_daffaires_key\" value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . "</option>\n";
		}
		$html .=
			"		</select>\n".
			"		<br /><br />\n";
			
		return $html;
		
	}

	/**
	* function
	* 
	* @access public
	* @param name of the training and starting date of the event 
	* 
	* @return true (if both exists) or false
	*/		
	
	function verifyIfFormationNameAndDateExists($name, $event_date)
	{
		global $DB;
		$blnBothExists = false;
		
		$Formation= new Formation($DB, "", "");
		$Formation->scanfields();
		$Formation->setQKey("nom", $name);
		$Formation->loadAll();
		$date_debut = $Formation->get("date_debut");
		
		if ($date_debut==$event_date &&  $Formation->get("actif")=='1'){
			$blnBothExists = true;			
		}
		
		return $blnBothExists;
	}

	function getParametersFromPOST($action) {
		$parameters = array();
		switch ($action) {
			case "add":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;			
			case "modify":
				foreach ($_POST as $field => $value) {
					if (is_array($value)) {
						$parameters[$field] = $value;
					} else {
						$parameters[$field] = trim($value);
					}
				}
				break;	
		}
		return $parameters;
	}
	
?>
