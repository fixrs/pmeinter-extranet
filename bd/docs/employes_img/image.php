<?php

$format = $_GET["fm"];
$file = $_GET["fl"];

if (!file_exists('./' . $file)) {
    header("HTTP/1.0 404 Not Found");
    exit;
}

switch($format) {
    case 'jpg':
        $image = imagecreatefromjpeg('./' . $file);
        break;
    case 'png':
        $image = imagecreatefrompng('./' . $file);
        break;
    case 'gif':
        $image = imagecreatefromgif('./' . $file);
        break;
}


$size = getimagesize('./' . $file);

//var_dump($size);

$width = '400';
$height = floor(($width * $size[1]) / $size[0]);

$imgOut = imagecreatetruecolor($width, $height);
imagecopyresampled($imgOut, $image, 0, 0, 0, 0, $width, $height, $size[0], $size[1]);


switch($format) {
    case 'jpg':
        header("Content-type: image/jpg");
        imagejpeg($imgOut);
        break;
    case 'png':
        header("Content-type: image/png");
        imagepng($imgOut);
        break;
    case 'gif':
        header("Content-type: image/gif");
        imagegif($imgOut);
        break;
}

// free memory
imagedestroy($image);
imagedestroy($imgOut);
