<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "employes");
		$Skin->assign("errors", $html);
		$Skin->display("employes.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "employes");
		$Skin->assign("errors", $html);
		$Skin->display("employes.tpl");
		exit;	
	}

	import("com.pmeinter.Employe");
	import("com.quiboweb.form.Form");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);

	if (isset($_GET["test"])) {

		$previous_nom = "";
		$previous_prenom = "";
		$previous_courriel = "";
		$previous_key = "";
		$previous_ddn_a = "";
		$previous_ddn_m = "";
		$previous_ddn_j = "";
		$sql = "";

		$employesH = array();

		// $in = "690,827,967,775,879,977,768,688,1021,944,941,962,702,701,939,942,857,754,692,766,824,769,877,880,772,822,705,831,910,911,1009,856,858,659,832,833,716,1015,921,821,971,883,927,776,753,894,823,887,777,773,774,974,700,830,704,966,976,838,1007,826,972,707,778,955,715,712,963,876,706,888,970,928,866,886,714,878,946,765,923,902,691,828,837,891,689,957,959,893,973,906,904";OR e.key IN (" . $in . ") 

		$anniversaires = preg_split("/\n/is", "atremblay@ptfl.ca,09,23,
cforest@ptfl.ca,10,29,1989
flavoie@ptfl.ca,12,2,
mtremblay@ptfl.ca,06,3,
mlapointe@ptfl.ca,12,24,
mlemay@ptfl.ca,02,22,
ptremblay@ptfl.ca,04,6,
gdery@ptfl.ca,07,9,
hgauthier@ptfl.ca,08,30,
hperreault@ptfl.ca,12,26,
llavoie@ptfl.ca,12,16,
mouellet@ptfl.ca,05,8,
mmartel@ptfl.ca,09,12,
ngirard@ptfl.ca,01,17,
svezina@ptfl.ca,06,18,
alefranois@notairesabitibi.com,02,28,
cpomerleau@notairesabitibi.com,03,22,1989
cberube@notairesabitibi.com,11,6,1983
gmorin@notairesabitibi.com,10,18,
mcorriveau@notairesabitibi.com,04,17,
mlantagne@notairesabitibi.com,07,8,1966
ptrottier@notairesabitibi.com,02,24,
sbanville@notairesabitibi.com,05,6,
vstgelais@notairesabitibi.com,03,24,1981
yrichard@notairesabitibi.com,06,28,1955
aboisvert@notairesabitibi.com,01,9,
cdelisle@notairesabitibi.com,11,19,
jperron@notairesabitibi.com,03,2,1974
jmorin@notairesabitibi.com,05,13,
mlambert@notairesabitibi.com,02,5,
mpetit@notairesabitibi.com,07,5,
mmainville@notairesabitibi.com,06,26,
fhotin@notairesabitibi.com,05,21,1967
nlabrecque@notairesabitibi.com,04 ,6,
prichard@notairesabitibi.com,01,7,1993
sgauthier@notairesabitibi.com,11,7,
sborchu@notairesabitibi.com,01,27,1994
slavoie@notairesabitibi.com,09,15,
daisy.imbeault@notarius.net,06,6,
gbrassard@notarius.net,03,17,1982
gjoly@notarius.net,07,26,1963
hugo.bussieres@notarius.net,11,25,1984
nharvey@notarius.net,03,31,1969
sbussiere@notarius.net,12,14,1959
a.chamberland.pmenotaires@cgocable.ca,11,25,
gjoly@notarius.net,03,27,
d.lemonnier.pmenotaire@cgocable.ca,03,1,
f.quinn.pmenotaires@cgocable.ca,03,27,
pmebc@notarius.net,02,14,
gbrassard@notarius.net,02,21,
pmeinterbc@cgocable.ca,12,5,
m.croteau.pmenotaires@cgocable.ca,03,27,
m.lefrancois.pmenotaires@cgocable.ca,09,1,
sbussiere@notarius.net,05,28,
m.gagnon.pmenotaires@cgocable.ca,06,13,
pmebc@notarius.net,07,22,
v.dupuy.pmenotaires@cgocable.ca,11,24,
karen.oborne@notarius.net,02,3,1984
mylene.paquette@notarius.net,09,22,1983
rmenard@notarius.net,07,27,1953
deniseraymond@axion.ca,04,9,
johannelabrecque@axion.ca,10,17,
marysejodoin@axion.ca,03,31,
rmenard@notarius.net,06,3,
mp.mongeau@notarius.net,03,9,1988
madly.laporte@notarius.net,05,10,1971
francinefalker@hotmail.com,03,16,1983
benoit.prudhomme@pfdnotaires.com,10,15,1966
catherine.dufour@pfdnotaires.com,02,13,1982
danielle.beausoleil@pfdnotaires.com,10,24,1957
david.dolan@pfdnotaires.com,05,27,1961
eric.lavoie@pfdnotaires.com,08,30,1983
jf.monette@pfdnotaires.com,10,28,1978
kevin.houle@pfdnotaires.com,02,15,1985
louis.vincent@pfdnotaires.com,06,11,1959
marc.legault@pfdnotaires.com,06,19,1973
melanie.guignard@pfdnotaires.com,06,16,
michael.dsouza@pfdnotaires.com,03,20,1982
robert.williamson@pfdnotaires.com,10,14,1968
audree.roy@pfdnotaires.com,11,12,
am.parisien@pfdnotaires.com,01,30,
caroline.lecompte@pfdnotaires.com,03,17,
mlegault@notarius.net,01,16,
clheng@notarius.net,04,2,
chantal.desrosiers@pfdnotaires.com,06,13,
jjprudhomme@notarius.net,06,14,
colombe.pageau@pfdnotaires.com,02,2,
emilie.st-pierre@pfdnotaires.com,11,30,
esther.dolan@pfdnotaires.com,11,29,
fanny.deraspe@pfdnotaires.com,08,30,
francine.larouche@pfdnotaires.com,06,28,
lvincent@notarius.net,07,18,
genevieve.mathieu@pfdnotaires.com,07,19,
jjprudhomme@notarius.net,10,24,
julie.truchon@pfdnotaires.com,04,9,
karine.gemme@pfdnotaires.com,02,12,
karolyne.montreuil@pfdnotaires.com,12,20,
lydie.oura@pfdnotaires.com,05,17,
me.porlier@pfdnotaires.com,12,28,
mj.fiset@pfdnotaires.com,05,14,
mj.isabelle@pfdnotaires.com,08,21,
admin.pfd@notarius.net,10,5,
melanie.fournier@pfdnotaires.com,03,26,
michele.grenier@pfdnotaires.com,02,25,
mducharme@notarius.net,12,24,
nancy.emond@pfdnotaires.com,11,3,
lvincent@notarius.net,05,10,
nathalie.seguin@pfdnotaires.com,05,15,
roxanne.tailleur@pfdnotaires.com,04,21,
sonia.viens@pfdnotaires.com,08,4,
stephanie.laprise@pfdnotaires.com,11,2,
sylvie.veilleux@pfdnotaires.com,08,16,
valerie.moscato@pfdnotaires.com,10,23,
virginie.fournier@pfdnotaires.com,02,2,
amjulien@ddjnotaires.com,03,15,1983
adurocher@ddjnotaires.com,01,5,1970
jdauphinais@ddjnotaires.com,06,28,1981
mmcyr@ddjnotaires.com,10,3,1986
elavoie@ddjnotaires.com,05,22,
elemire@ddjnotaires.com,08,12,
kfilion@ddjnotaires.com,12,20,
ktrottier@ddjnotaires.com,12,1,
lpaquette@ddjnotaires.com,01,30,
ldauphinais@ddjnotaires.com,02,9,
mserli@ddjnotaires.com,06,30,
smichaud@ddjnotaires.com,10,25,
claude.abdelnour@abdelrosiers.com,02,19,
isabelle.simard@abdelrosiers.com ,01,1,1986
line.desrosiers@abdelrosiers.com ,01,3,1959
mariechristine.cote@abdelrosiers.com,03,22,1988
abdelrosiers@notarius.net,11,28,1959
carole.matte@abdelrosiers.com,12,4,
jennifer.scott@abdelrosiers.com,11,28,1980
julie.despres@abdelrosiers.com,11,8,1985
nadine.jean@abdelrosiers.com,03,27,1966
aik@gipl.qc.ca,11,9,
aphilippe@pmegatineau.ca,05,1,
cberube@pmegatineau.ca,11,24,1998
clarocque@pmegatineau.ca,06,6,
gdubreuil@pmegatineau.ca,05,12,
glaflamme@pmegatineau.ca,12,8,
ipelletier@pmegatineau.ca,01,12,
jacques-mauffette@gipl.qc.ca,12,19,
jfvp@pmegatineau.ca,01,20,
jfg@pmegatineau.ca,03,7,1972
jpl@pmegatineau.ca,05,21,
jlapierre@pmegatineau.ca,03,9,1972
jgorman@pmegatineau.ca,05,18,
llavallee@pmegatineau.ca,07,28,1957
mbellfoy@pmegatineau.ca,04,21,1987
mpjt@pmegatineau.ca,08,18,
mpatry@pmegatineau.ca,02,28,
mturpin@pmegatineau.ca,05,28,
mlafontaine@pmegatineau.ca,01,25,
malary@pmegatineau.ca,07,2,1967
pb@pmegatineau.ca,10,15,1976
smercier@pmegatineau.ca,11,19,
vlafontaine@pmegatineau.ca,06,10,
aimee-desrochers@gipl.qc.ca,08,20,
adesjardins@pmegatineau.ca,01,8,
anne-lamothe@gipl.qc.ca,01,16,
anne-tessier@gipl.qc.ca,03,17,
anne-marie-seguin@gipl.qc.ca,02,19,
annie-laroche@gipl.qc.ca,07,5,
bernadette-bertrand@gipl.qc.ca,10,12,
carole-denomme@gipl.qc.ca,08,15,
carole-pepin@gipl.qc.ca,12,15,
catheryne-cadieux@gipl.qc.ca,01,5,
cchenard@pmegatineau.ca,12,24,
callard@pmegatineau.ca,10,3,
france-hamann@gipl.qc.ca,05,22,
francine-roy@gipl.qc.ca,07,1,
jacinthe-gauthier@gipl.qc.ca,11,16,
jacqueline-potvin@gipl.qc.ca,04,12,
jennifer-gratton@gipl.qc.ca,03,30,
jessica-presseault@gipl.qc.ca,09,3,
johanne-gauthier@gipl.qc.ca,06,1,
johanne-gregoire@gipl.qc.ca,02,14,
josee-mercier@gipl.qc.ca,05,19,
josiane-brousseau@gipl.qc.ca,04 ,12,
kim-antila@gipl.qc.ca,08,24,
marie-helene-marois@gipl.qc.ca,07,26,
martine-brunet@gipl.qc.ca,05,9,
melanie-chenevert@gipl.qc.ca,08,11,
nathalie-gingras@gipl.qc.ca,09,10,
nathalie-mercier@gipl.qc.ca,08,10,
sacha-gauvreau@gipl.qc.ca,04,5,
spodrez@pmegatineau.ca ,08,1,
jmouaikel@pmegatineau.ca ,03,14,
gsaucier@pmegatineau.ca ,06,5,
koconnor@pmegatineau.ca ,12,19,
nlaframboise@pmegatineau.ca ,09,5,
cmariat@pmegatineau.ca ,08,18,
mvilleneuve@pmegatineau.ca ,04,2,
lparent@pmegatineau.ca ,11,26,
sylvie-boivin@gipl.qc.ca,08,4,
sylvie-morin@gipl.qc.ca,08,24,
tammy-wolfe@gipl.qc.ca,10,2,
vthibodeau@pmegatineau.ca,10,14,
acantin@gclnotaire.com,09,18,
ghebert@gclnotaires.com,09,3,
mgagnon@gclnotaires.com,02,9,
mriopel@gclnotaires.com,10,15,1955
psasseville@gclnotaires.com,10,26,
rgagnon@gclnotaires.com,07,21,1950
vlachapelle@gclnotaires.com,08,8,
alachapelle@gclnotaires.com,08,14,
amlachapelle@gclnotaires.com,04,20,
epellerin@gclnotaires.com,07,9,
jdrichard@gclnotaires.com,02,25,
llord@gclnotaires.com,03,17,
mjmanegre@gclnotaires.com,08,20,
mbegin@gclnotaires.com,11,9,
cpichette@gclnotaires.com,12,15,1975
clagace@gclnotaires.com,11,13,
cnantais@gclnotaires.com,08,13,
dbarrette@gclnotaires.com,06,11,
fst-georges@gclnotaires.com,03,15,
glachapelle@gclnotaires.com,08,1,
ileclere@gclnotaires.com,06,7,
jouellet@gclnotaires.com,06,25,
jgareau@gclnotaires.com,02,22,
jgravel@gclnotaires.com,03,25,
jgregoire@gclnotaires.com,12,31,
jlachance@gclnotaires.com,03,14,
lgauvin@gclnotaires.com,07,19,
lperreault@gclnotaires.com,08,27,
ncouture@gclnotaires.com,01,25,1982
ndurand@gclnotaires.com ,06,20,1969
ncaron@gclnotaires.com,08,19,1970
nlaporte@gclnotaires.com,07,23,
sdeneault@gclnotaires.com,05,8,
stremblay@gclnotires.com,06,19,
sfournier@gclnotaires.com,03,25,
clogagnon@notarius.net,06,22,1957
dstpierre@notarius.net ,03,29,1964
lgaron@notarius.net,11,21,1953
ylevesque@notarius.net,02,5,
hgaudreault@notarius.net,11,16,
ealexandre@notarius.net,11,3,
glauzier@notarius.net,09,9,
lise.dumais@notarius.net,03,12,
roylouise@notarius.net,03,16,
madeleineleblanc@notarius.net,11,25,
cguy@notarius.net,09,18,
sylvie.lapierre@notarius.net,07,16,1963
sylvie.lebel@notarius.net,10,7,
mslapoc@hotmail.com,03,29,
cpouliot@notarius.net,09,13,1970
nlafontaine@notaires-etchemins.com,12,3,1981
pnadeau@notarius.net,04,24,1964
aboutin@notaires-etchemins.com,04,14,
ccayouette@notaires-etchemins.com,05,24,
aboutin@notaires-etchemins.com,03,5,
lracine@notaires-etchemins.com,07,7,
vcarter@notaires-etchemins.com,02,24,
agerin@notarius.net,09,7,1977
apomerleau@notarius.net,01,22,1973
agirard@notarius.net,10,3,1987
pcrepeau@notarius.net,09,22,1973
vleroux@notarius.net,08,17,1986
ygerin@notarius.net,04,11,1946
bbolduc@gpcnotaires.com,07,29,
fgrenier@gpcnotaires.com,01,5,
gtheberge@gpcnotaires.com,08,30,
mlapointe@gpcnotaires.com,04 ,20,
jleblanc@gpcnotaires.com,02,21,
lyockell@@pcnotaires.com ,03,14,
mcpepin@pcnotaires.com ,06,15,
bbolduc@gpcnotaires.com,01,16,
scouture@@pcnotaires.com ,08,18,
ygerin@notarius.net,01,4,
ndube@notarius.net,02,5,
etremblay@notarius.net,01,31,
gsamson@notarius.net,05,13,
hpotvin@notarius.net,06,4,
cynthia.bedard@globetrotter.net,02,3,
jogendron@globetrotter.net,05,25,
bddnot@notarius.net,04,16,
v.samson@globetrotter.net,12,12,
poulin.karine@globetrotter.net,01,14,
vanessa.pelletier@globetrotter.net,11,15,
ja.richard@globetrotter.net,12,12,
bddadministration@globetrotter.net,06,25,
melaberge@globetrotter.net,03,22,
aouellet@notairesmontreal.pro,05,18,1987
dferland@notairesmontreal.pro,09,3,1984
jlroy@notairesmontreal.pro,06,23,1955
jbenitez@notairesmontreal.pro,07,3,1987
jgauthier@notairesmontreal.pro,11,21,1955
mneveu@notairesmontreal.pro,02,8,
schikhi@notairesmontreal.pro,07,8,
tfrechette@notairesmontreal.pro,11,4,1989
clafleche@notairesmontreal.pro,09,13,1985
dbrisebois@notaireslaval.proÊÊ ,07,19,1980
fneveu@notaireslaval.pro,04,9,
ghurtado@notairesmontreal.pro,09,10,
jfarkas@notairesmontreal.pro,05,5,
kelidrissi@notairemontreal.pro,08,31,
kdesjardins@notairesmontreal.pro,09,13,
mgmorisseau@notairesmontreal.pro,06,10,
sprecourt@notairesmontreal.pro,04,14,1975
admin@notairesmontreal.pro,03,6,1946
jlambert@notarius.net,03,22,
g.perronoddo@notarius.net,06,12,
c.levac@notarius.net,07,23,
ncheaib@notarius.net,08,13,
apelonis@notarius.net,03,15,
mauricebrisebois@videotron.ca,07,31,
lepineclaire@yahoo.ca,08,9,
esmb@esmb.ca ,02,23,
c.spenard@notarius.net,06,16,
gmarinier@notarius.net ,11,18,
lvilleneuve@notarius.net,11,13,1986
sophie.lasalle@notarius.net,06,11,
a.recine@notarius.net,07,11,
lasallevilleneuve@hotmail.com,10,28,
francine.boyer@notarius.net ,11,1,
mperillo@notarius.net ,11,27,
mm.labrosse@notarius.net ,05,5,
m.canuel@notarius.net,01,19,
gaeltremblay@notarius.net,10,25,1983
gtrembl2@notarius.net,11,26,1948
sbarriau@notarius.net,09,6,1958
gtrembl2@notarius.net,12,10,
gtrembl2@notarius.net,08,30,
gtrembl2@notarius.net,04 ,10,
gaeltremblay@notarius.net,12,18,
s.bujold@notarius.net,03,25,
jcgarant@notarius.net,05,19,1949
j.cardoso@notarius.net,01,8,1986
sgregoire@notarius.net,10,23,1972
v.branchaud@notarius.net,11,26,1987
lapointedenise@hotmail.com,04,11,
a.gangnito@notarius.net,10,19,
mclabry@hotmail.com,04,16,
agariep1@notarius.net,08,7,1960
aaubert@notarius.net,01,24,1957
jdionne@notarius.net,02,7,
jgauthie@notarius.net,11,11,
laurie.cote@notarius.net,10,23,1985
nconstan@notarius.net,10,14,
chantal.lortie@notarius.net,07,11,
christine.fleury@notarius.net,03,25,
elaine.pratte@notarius.net,06,23,
kgodin@notarius.net,08,31,
linda.goulet@notarius.net ,01,5,
m.deroy@notarius.net,12,22,
p.chapados@notarius.net,12,27,
mireilletiernan@gmail.com,07,1,
sandra.lagace@notarius.net,12,31,
ahebert@hebnotaires.com,08,19,
sberlatie@hebnotaires.com,02,2,
yroy@hebnotaires.com ,04,17,1956
info@hebnotaires.com,01,11,1982
dracette@hebnotaires.com,11,12,
mphilibert@hebnotaires.com,12,23,
sbourdages@hebnotaires.com,09,1,1968
frseguin@notarius.net,02,11,
rachelle.paquin@notarius.net,07,27,
anny.morissette@notarius.net,04,14,
paulineroy@notarius.net,03,28,
asimard@notarius.net,06,9,1966
emilie.langlois@notarius.net,11,28,
ksavoie@notarius.net,04,24,1971
melanie.jacques@notarius.net,06,11,
m.pilotte@notarius.net,05,23,
corbeilpa@notarius.net,11,7,1952
andreecsm@hotmail.com,10,5,
anick.filion@notarius.net,03,10,
dunn.c@notarius.net,12,18,
d.labarre@notarius.net,09,17,
guylainecsm@hotmail.com,02,19,
lamarche.l@notarius.net,06,8,
vanessa.poulin@notarius.net,11,21,
claudiaracine@dechamplaingirardnotaires.com ,04,10,1989
gdechamplain@dechamplaingirardnotaires.com ,10,29,
josee.girard@dechamplaingirardnotaires.com ,05,31,
michael.tremblay@dechamplaingirardnotaires.com ,02,14,1985
chantale.ratte@dechamplaingirardnotaires.com,02,12,1960
contact@dechamplaingirardnotaires.com,01,12,
gina.cote@dechamplaingirardnotaires.com ,07,23,1960
josee.girard@dechamplaingirardnotaires.com ,11,28,
josee.pellerin@@dechamplaingirardnotaires.com ,03,6,
mh.boily@dechamplaingirardnotaires.com,03,27,1985
maryse.cote@dechamplaingirardnotaires.com ,08,6,1964
MBegin@lesnotaires.net,06,6,1990
cpoulin@lesnotaires.net,01,11,1986
jbreton@lesnotaires.net,02,14,1974
javachon@lesnotaires.net,06,18,1956
sbisson@lesnotaires.net,07,5,
jcloutier@lesnotaires.net,04,8,1982
mesamson@lesnotaires.net,10,1,
mcloutier@lesnotaires.net,06,20,
msylvain@lesnotaires.net,01,26,
svachon@lesnotaires.net,11,16,
sjacques@lesnotaires.net,04,17,
cmathieu@notarius.net,12,23,1950
crodrigue@notarius.net,07,27,1963
i.beaulieu@notarius.net,09,25,1983
trnotair@notarius.net,09,10,1965
coralie.rodrigue@globetrotter.net,07,25,
erika.landry@globetrotter.net,11,13,
helenelari@globetrotter.net,03,5,
lquirion@notarius.net,10,3,
mdeblois@notarius.net,01,6,
rollande.patry@notarius.net,11,29,
soniaroy.tr@globetrotter.net,09,17,
sophie.boutin@globetrotter.net,03,18,
 droy@gdln.ca ,08,13,1989
 jdorais@gdln.ca ,09,30,1956
 klaprise@gdln.ca ,01,2,
 mcmessier@gdln.ca ,12,6,1984
 mgagnon@gdln.ca ,02,5,
 mbrais@gdln.ca ,06,19,1960
cdenicourt@gdln.ca,08,22,
cgagne@gdln.ca,08,4,
cwarren@gdln.ca,05,10,
hverville@gdln.ca,03,6,
icaron@gdln.ca,01,9,
ipettigrew@gdln.ca,03,28,
jvien@gdln.ca,10,2,
jst-denis@gdln.ca,02,4,1980
mcadieux@gdln.ca,05,16,
vbourgeois@gdln.ca ,08,1,
vnadeau@gdln.ca,09,27,
aleonard@lrvnotaires.com,04,7,
cbeland@lrvnotaires.com,02,24,1983
dlafond@lrvnotaires.com,08,27,
dbourgeois@lrvnotaires.com,05,18,
jfpiche@lrvnotaires.com,10,13,1977
jhebert@lrvnotaires.com,09,6,1977
lmenard@lrvnotaires.com,06,12,
mlamarre@lrvnotaires.com,02,14,
mcbergeron@ lrvnotaires.com,10,25,
mclavoie@lrvnotaires.com,04,13,
Mmartinchantal@lrvnotaires.com,08,27,1985
sleveille@lrvnotaires.com,07,9,
tbdavidson@lrvnotaires.com,06,23,
dlacasse@pmeinter.com,05,18,1959
cmenard@lrvnotaires.com,11,22,
amarchand@lrvnotaires.com,09,23,
aouimet@lrvnotaires.com,01,19,
athibert@lrvnotaires.com,11,29,
cgoyer@lrvnotaires.com,10,1,
chemondducharme@lrvnotaires.com,04 ,27,
csarrazin@lrvnotaires.com,08,2,
ccrispin@lrvnotaires.com,07,7,
galarie@lrvnotaires.com,04 ,20,
ylevesque@notarius.net,09,9,
jtranchemontagne@lrvnotaires.com,03,9,
kperrault@lrvnotaires.com,07,28,
louellette@lrvnotaires.com,11,9,
ltheroux@lrvnotaires.com,03,4,
mgagnon@lrvnotaires.com,05,27,
mfalardeau@lrvnotaires.com,10,11,
nroy@lrvnotaires.com,04 ,19,
nlavigne@lrvnotaires.com,08,15,
ndion@lrvnotaires.com,07,19,
phamelin@lrvnotaires.com,07,17,
smillette@lrvnotaires.com,09,7,
vleonard@lrvnotaires.com,03,6,
clefebvre@notarius.net,07,5,1962
diana.bonneau@notarius.net,03,15,1985
fmalenfant@notarius.net,05,12,
rgelinas@notarius.net,06,23,1957
thiffault.linda@cgocable.ca,12,14,
aleclerc@notarius.net ,07,7,1990
ac.brochu@notarius.net ,11,11,1988
btanguay@notarius.net,02,27,1952
datousignant@notarius.net,02,9,1952
pt.not@videotron.ca ,07,4,1951
iouellet@notarius.net ,12,6,1973
kfrancoeur@notarius.net ,09,27,1978
lise.marquis@notarius.net,08,24,1978
mrainville@notarius.net,08,21,1972
mtanguay@paretanguay.net,09,2,1983
mpare@notarius.net ,02,5,1979
shardouin@notarius.net ,12,9,1973
annie.grenier@paretanguay.net,04,27,
cathleen.hall@paretanguay.net,08,20,1970
celine.labrie@paretanguay.net,10,8,
chantal.corriveau@paretanguay.net,06,13,
edith.champigny@paretanguay.net,01,17,
reception@paretanguay.net,07,20,
guillaume.roy@paretanguay.net,11,13,1986
martine.morin@paretanguay.net,02,26,
melanie.bisaillon@paretanguay.net,03,8,
nancy.gagnon@paretanguay.net,02,5,
barthey@videotron.ca,02,11,
lgravel@pmeinter.com,03,14,
gdeblois@pmeinter.com,10,30,");


		echo "<pre>";
		print_r($anniversaires);
		echo "</pre>";
		$sql = "";
		for ($i = 0; $i < count($anniversaires); $i++) {
			$details = explode(",", $anniversaires[$i]);

			echo "<pre>";
			print_r($details);
			echo "</pre>";

			$email = trim(strtolower($details[0]));
			$mois = trim($details[1]);
			$jour = trim($details[2]);
			$annee = trim($details[3]);	

			$DB->query("SELECT * FROM `employes` e WHERE e.actif = 1 AND courriel = '" . $email . "'");


			while ($DB->next_record()) {
				$sql .= "UPDATE `employes` SET ddn_j = '" . $jour . "', ddn_m = '" . $mois . "', ddn_a = '" . $annee . "' WHERE `key` = '" . $DB->getField("key") . "';<br />";
			}
		}
		echo $sql;
		// $DB->query("SELECT * FROM `employes` e WHERE e.actif = 1 ORDER BY e.nom, e.prenom ASC");
		// while ($DB->next_record()) {
			
		// 	if (trim(strtolower($DB->getField("nom"))) == trim(strtolower($previous_nom))) {

		// 		if (trim(strtolower($DB->getField("prenom"))) == trim(strtolower($previous_prenom))) {

		// 			if (trim(str_replace(" ", '', $DB->getField("courriel"))) == trim($previous_courriel)) {					
						
		// 				if ($previous_key > $DB->getField("key")) {
		// 					$sql .=
		// 						"UPDATE employes SET actif = 0 WHERE `key` = '" . $previous_key . "' AND actif = 1;<br />";
		// 				} else {
		// 					$sql .=
		// 						"UPDATE employes SET actif = 0 WHERE `key` = '" . $DB->getField("key") . "' AND actif = 1;<br />";	
		// 				}

		// 				if ($DB->getField("ddn_a") . "-" . $DB->getField("ddn_m") . "-" . $DB->getField("ddn_j") != $previous_ddn_a . "-" . $previous_ddn_m . "-" . $previous_ddn_j) {
		// 					echo $DB->getField("key") . " = " . $DB->getField("ddn_a") . "-" . $DB->getField("ddn_m") . "-" . $DB->getField("ddn_j") . "<br />\n";
		// 					echo $previous_key . " = " . $previous_ddn_a . "-" . $previous_ddn_m . "-" . $previous_ddn_j . "<br />\n";
		// 					echo "UPDATE employes SET ddn_a = '" . $DB->getField("ddn_a") . "', ddn_m = '" . $DB->getField("ddn_m") . "', ddn_j = '" . $DB->getField("ddn_j") . "' WHERE `key` = " . $previous_key . ";<br /><br />";
		// 				}
						

						// $employesH["e" . $DB->getField("key")] = array(
						// 	"nom" => $DB->getField("nom"),
						// 	"prenom" => $DB->getField("prenom"),
						// 	"courriel" => $DB->getField("courriel"),
						// 	"previouskey" => $previous_key
						// );

						//echo $DB->getField("prenom") . " " . $DB->getField("nom") . "<br />";
						
						// DELETE FROM `employes_directions_de_travail` WHERE employes_key = 664 AND directions_de_travail_key = 7;
						// INSERT INTO `employes_directions_de_travail` (employes_key, directions_de_travail_key) VALUES (385, 1);
						// UPDATE `employes_directions_de_travail` SET directions_de_travail_key = 1 WHERE employes_key = 130;
						// INSERT INTO `employes_directions_de_travail` (employes_key, directions_de_travail_key) VALUES (579, 7);


						// $inkeys = $DB->getField("key") . "," . $previous_key;
						// $DB->query("SELECT * FROM `employes_directions_de_travail` WHERE employes_key IN (" . $inkeys . ") ORDER BY employes_key");
						
						// echo "Employe keys = " . $inkeys . "<br />\n";
						// while ($DB->next_record()) {
						// 	echo $DB->getField("employes_key") . " -> " . $DB->getField("directions_de_travail_key") . "<br />\n";
						// }

		// 			}
		// 		}

		// 	}

		// 	$previous_nom = $DB->getField("nom");
		// 	$previous_prenom = $DB->getField("prenom");
		// 	$previous_courriel = str_replace(" ", '', $DB->getField("courriel"));
		// 	$previous_key = $DB->getField("key");
		// 	$previous_ddn_a = $DB->getField("ddn_a");
		// 	$previous_ddn_m = $DB->getField("ddn_m");
		// 	$previous_ddn_j = $DB->getField("ddn_j");
		// }


		// echo "<pre>";
		// print_r($employesH);
		// echo "</pre>";


		//echo $sql;


		$DB->close();
		exit;


	}


	if (isset($_GET["import"])) { 
		require "./import.php";
		global $import2016;

		$etudes = array();
		$employes = array();
		$DB->query("SELECT e.key, e.nom, e.prenom, e.courriel, et.etudes_succursales_key AS etude_key FROM `employes` e INNER JOIN employes_etudes_succursales et ON et.employes_key = e.key WHERE e.actif = 1 ORDER BY e.key");
		while ($DB->next_record()) {
			$employes[] = array("key" => $DB->getField("key"), "nom" => $DB->getField("nom"), "courriel" => $DB->getField("courriel"), "prenom" => $DB->getField("prenom"), "etude_key" => $DB->getField("etude_key"));
		}

		$DB->query("SELECT e.nom, e.courriel, es.key, es.adresse, es.code_postal FROM `etudes` e INNER JOIN `etudes_succursales` es ON e.key = es.etudes_key WHERE es.actif = 1 ORDER BY e.key");
		while ($DB->next_record()) {
			$etudes[] = array("key" => $DB->getField("key"), "nom" => preg_replace("/(inc\.|S\.E\.N\.C\.R\.L\.)/i", "", $DB->getField("nom")), "courriel" => $DB->getField("courriel"), "adresse" => $DB->getField("adresse"), "code_postal" => $DB->getField("code_postal"));
		}

		$import2016 = preg_replace_callback("/\"([^\"]+)\"/is", function ($matches) { return str_replace(',', '#', $matches[1]); }, $import2016);
		$contacts = preg_split("/\n/is", $import2016);

		$next_employe_key = 0;
		$DB->query("SELECT max(e.key) AS next_key FROM `employes` e");
		while ($DB->next_record()) {
			$next_employe_key = $DB->getField("next_key");
		}
		$next_employe_key++;

		function searchEtude($string) {
			global $etudes;

			$string = str_replace("#", ",", $string);
			$string = preg_replace("/(inc\.|S\.E\.N\.C\.R\.L\.)/i", "", $string);

			foreach ($etudes as $etude) {
				if (preg_replace("/\W/", "", trim(strtolower($etude["nom"]))) == preg_replace("/\W/", "", trim(strtolower($string)))) {
					return $etude["key"];
				} 
			}
		}

		function searchEtudePC($string, $code_postal) {
			global $etudes;

			$string = str_replace("#", ",", $string);
			$string = preg_replace("/(inc\.|S\.E\.N\.C\.R\.L\.)/i", "", $string);

			foreach ($etudes as $etude) {
				if (preg_replace("/\W/", "", trim(strtolower($etude["nom"]))) == preg_replace("/\W/", "", trim(strtolower($string)))) {
					if (trim(strtolower($etude["code_postal"])) == trim(strtolower($code_postal))) {
						return $etude["key"];
					}
				} 
			}
		}

		function searchEmploye($string) {
			global $employes;

			foreach ($employes as $employe) {
				$nom = preg_replace("/\W/", "", trim(strtolower($employe["nom"])));
				$prenom = preg_replace("/\W/", "", trim(strtolower($employe["prenom"])));
				$point = 0;

				if (preg_match("/" . $nom . "/i", trim(strtolower($string)))) {
					$point++;
				} 

				if (preg_match("/" . $prenom . "/i", trim(strtolower($string)))) {
					$point++;
				} 

				if ($point == 2) {
					return $employe;
				}
			}
		}

		/*
		0 - Ville,
		1 - Société,
		2 - Adresse,
		3 - Bureau,
		4 - Ville,
		5 - Code postal,
		6 - Téléphone Bureau,
		7 - Télécopieur,
		8 - Site internet,
		9 - Courriel général de l'étude,
		10 - Ville,
		11 - Succursale attitrée,
			12 - Titre,
			13 - Personnel,
			14 - Fonction,
			15 - Courriel,
		16 - Poste téléphonique,
17 - Responsable de la facturation,
			18 - Associé,
		19 - Agent de liaison,
		20 - Agent de liaison COLLABORATRICE,
			21 - Anniversaire - mois,
			22 - Anniversaire-jour,
			23 - Anniversaire-année (si disponible),
			24 - Anniversaire de profession,
			25 - Comité Droit de la persone,
			26 - Comité Droit Immobilier,
			27 - Comité Droit des affaires,
			28 - Comité Droit agricole,
			29 - Comité gestion de bureaux,
			30 - Comité Action des collabroatrices,
31 - NE PAS TOUCHER CETTE COLONNE,
32 - Spécialisation - Droit des affaires,
33 - Spécialisation - Droit Immobilier,
34 - Spécialisation - Droit de la personne,
35 - Spécialisation - Droit agricole,
36 - Spécialisation - Expertises sectorielles,
37 - BD Extranet,
38 - Nom validé,
39 - Assurance collective,
40 - Compagnie
		*/
	
		if ($next_employe_key > 2) {
			//echo $next_employe_key;
			$sql = "";

			foreach ($contacts as $contact) {
				$infos = explode(',', $contact);

				$etude_key = searchEtude($infos[1]);

				$employe_key = searchEmploye($infos[13]);

				if (preg_match("/^[0-9]+$/", $employe_key["key"])) {
					if (strtolower(trim($infos[15])) != strtolower(trim($employe_key["courriel"]))) {
						$sql .= 
							"UPDATE employes ".
							"SET courriel = '" . mysql_real_escape_string(strtolower(trim($infos[15]))) . "' ".
							"WHERE `key` = '" . $employe_key["key"] . "';\n";
					}
				} else {
					
					//NEW PEOPLE
					$sql .=
						"INSERT INTO `employes` (
							`key`,
							`prenom`,
							`nom`,
							`courriel`,
							`annee_debut_pratique`,
							`ddn_a`,
							`ddn_m`,
							`ddn_j`,
							`associe`,
							`actif`
						) VALUES (
							'" . $next_employe_key . "',
							'" . mysql_real_escape_string(preg_replace("/^(.*?)\s.*$/is", "$1", $infos[13])) . "',
							'" . mysql_real_escape_string(preg_replace("/^.*?\s(.*)$/is", "$1", $infos[13])) . "',
							'" . mysql_real_escape_string($infos[15]) . "',
							'" . mysql_real_escape_string(strtolower(trim($infos[15]))) . "',
							'" . mysql_real_escape_string(strtolower(trim($infos[23]))) . "',
							'" . mysql_real_escape_string(strtolower(trim($infos[21]))) . "',
							'" . mysql_real_escape_string(strtolower(trim($infos[22]))) . "',
							'" . (trim(strtolower($infos[18])) == 'x' ? '1' : '0') . "',
							'1'
						);\n";
					
					
					if (trim(strtolower($infos[25])) == 'x') {
						$sql .= "INSERT INTO `employes_directions_de_travail` (employes_key, directions_de_travail_key) VALUES ('" . $next_employe_key . "', '2');\n";
					}	

					if (trim(strtolower($infos[26])) == 'x') {
						$sql .= "INSERT INTO `employes_directions_de_travail` (employes_key, directions_de_travail_key) VALUES ('" . $next_employe_key . "', '4');\n";
					}	

					if (trim(strtolower($infos[27])) == 'x') {
						$sql .= "INSERT INTO `employes_directions_de_travail` (employes_key, directions_de_travail_key) VALUES ('" . $next_employe_key . "', '1');\n";
					}	

					if (trim(strtolower($infos[28])) == 'x') {
						$sql .= "INSERT INTO `employes_directions_de_travail` (employes_key, directions_de_travail_key) VALUES ('" . $next_employe_key . "', '5');\n";
					}	

					if (trim(strtolower($infos[29])) == 'x') {
						$sql .= "INSERT INTO `employes_directions_de_travail` (employes_key, directions_de_travail_key) VALUES ('" . $next_employe_key . "', '3');\n";
					}	

					if (trim(strtolower($infos[30])) == 'x') {
						$sql .= "INSERT INTO `employes_directions_de_travail` (employes_key, directions_de_travail_key) VALUES ('" . $next_employe_key . "', '7');\n";
					}			
					
					switch (trim(strtolower($infos[14]))) {
						case 'notaire':
						case 'notaire associé':
						case 'notaire honorifique':
							$sql .= "INSERT INTO `employes_fonctions` (employes_key, fonctions_key) VALUES ('" . $next_employe_key . "', '8');\n";

							if (trim(strtolower($infos[12])) == 'me') {
								$sql .= "INSERT INTO `employes_titres` (employes_key, titres_key) VALUES ('" . $next_employe_key . "', '1');\n";
								$sql .= "INSERT INTO `employes_titres` (employes_key, titres_key) VALUES ('" . $next_employe_key . "', '5');\n";
							}
							break;

						case 'collaborateur/trice et/ou technicien/ne juridique':
							$sql .= "INSERT INTO `employes_fonctions` (employes_key, fonctions_key) VALUES ('" . $next_employe_key . "', '1');\n";
							break;

						case 'comptable':
							$sql .= "INSERT INTO `employes_fonctions` (employes_key, fonctions_key) VALUES ('" . $next_employe_key . "', '4');\n";
							$sql .= "INSERT INTO `employes_titres` (employes_key, titres_key) VALUES ('" . $next_employe_key . "', '3');\n";
							break;

						case 'dir. développement des affaires':
							$sql .= "INSERT INTO `employes_fonctions` (employes_key, fonctions_key) VALUES ('" . $next_employe_key . "', '13');\n";
							break;

						case 'directrice générale':
							$sql .= "INSERT INTO `employes_fonctions` (employes_key, fonctions_key) VALUES ('" . $next_employe_key . "', '12');\n";
							break;

					}
					
					$etude_key = searchEtudePC($infos[1], $infos[11]);
						if ($etude_key != "") {
					$sql .= "INSERT INTO `employes_etudes_succursales` (employes_key, etudes_succursales_key) VALUES ('" . $next_employe_key . "', '" . $etude_key . "');\n";
					}

					$next_employe_key++;
				}

				

			
			
	/*

		1	droit des affaires 	27 - Comité Droit des affaires,
		2	droit de la Personne 	25 - Comité Droit de la persone,
		3	gestion de bureaux	29 - Comité gestion de bureaux,
		4	droit Immobilier 	26 - Comité Droit Immobilier,
		5	droit agricole 	28 - Comité Droit agricole,
		6	développement des affaires
		7	action des collabroatrices 	30 - Comité Action des collabroatrices,

	*/

	/*

		1	Collaborateur / Technicien juridique
		4	Comptable
		15	Avocat
		16	Agent de liaison collaboratrice
		8	Notaire
		10	Autre
		11	Agent de liaison
		12	Directeur général
		13	Directeur du développement des affaires

		Notaire
		Notaire associé
		Collaborateur/trice et/ou technicien/ne juridique
		Notaire honorifique
		Comptable
		Dir. Développement des affaires
		Directrice générale

	*/

	// 1	key	int(11)			No	None	AUTO_INCREMENT	Change Change	Drop Drop	
	// 2	lastmod	timestamp		on update CURRENT_TIMESTAMP	No	0000-00-00 00:00:00	ON UPDATE CURRENT_TIMESTAMP	Change Change	Drop Drop	
	// 3	prenom	varchar(150)	utf8_general_ci		No			Change Change	Drop Drop	
	// 4	nom	varchar(150)	utf8_general_ci		No			Change Change	Drop Drop	
	// 5	courriel	varchar(100)	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 6	annee_debut_pratique	int(1)			Yes	NULL		Change Change	Drop Drop	
	// 7	annee_debut_pratique_avocat	int(1)			Yes	NULL		Change Change	Drop Drop	
	// 8	ddn_a	int(1)			Yes	NULL		Change Change	Drop Drop	
	// 9	ddn_m	int(1)			Yes	NULL		Change Change	Drop Drop	
	// 10	ddn_j	int(1)			Yes	NULL		Change Change	Drop Drop	
	// 11	associe	int(1)			Yes	NULL		Change Change	Drop Drop	
	// 12	description	text	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 13	expertises_droit_affaires	text	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 14	expertises_droit_personne	text	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 15	expertises_droit_immobilier	text	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 16	expertises_sectorielles	text	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 17	droit_des_affaires_desc	text	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 18	droit_des_personnes_desc	text	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 19	droit_immobilier_desc	text	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 20	expertises_sectorielles_desc	text	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 21	image	varchar(255)	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 22	image_description	text	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 23	succursale	varchar(255)	utf8_general_ci		Yes	NULL		Change Change	Drop Drop	
	// 24	actif	tinyint(

			}
		}

		echo nl2br($sql);
		
		$DB->close();
		exit;

	}

	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$successString = "";
	$errorString = "";
	$formString = "";
	
	$action = getorpost('action');
	$key = getorpost('key');

	switch ($_GET['action']) {
		case "add":
			header("Location: employes.php?action=add&begin=1");
			exit;

		case "modify":
			$formString .= modifyEmployeString();
			break;

		case "delete":
			$formString .= deleteEmployeString();
			break;

		case "read":
			$formString .= readEmployeString();
			break;

	}

	switch ($_POST['action']) {
		case "modify":
			if ($key == "-1") {
				$formString .= modifyEmployeString();
				$errorString .= _error("Veuillez choisir un employ&eacute; dans la liste");
			} else {
				header("Location: employes.php?action=modify&begin=1&key=" . $key . "");
				exit;
			}
			break;

		case "delete":
			if ($key == "-1") {
				$formString .= deleteEmployeString();
				$errorString .= _error("Veuillez choisir un employ&eacute; dans la liste");
			} else {
				if ($_POST['confirmation']) {
					$errorString .= deleteEmployeFromDB($key);
					if ($errorString == "") {
						$successString .= _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s");
					} else {
						$formString .= deleteEmployeString();
					}
				} else {
					$formString .= deleteEmployeString();
					$errorString .= _error("Veuillez confirmer la suppression");
				}
			}
			break;

		case "read":
			if ($key == "-1") {
				$formString .= readEmployeString();
				$errorString .= _error("Veuillez choisir un employ&eacute; dans la liste");
			} else {
				header("Location: employes.php?action=read&key=" . $key . "");
				exit;
			}
			break;

	}

	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "employes");
	$Skin->assign("page", "employes");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("menu", menuActionString());
	$Skin->assign("menu_title", menuTitleString($action));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("employes.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "Ajout d'employ&eacute;s";
				break;
			case "modify":
				$title = "Modification d'employ&eacute;s";
				break;
			case "delete":
				$title = "Suppression d'employ&eacute;s";
				break;
			case "read":
				$title = "Consultation d'employ&eacute;s";
				break;
			case "search":
				$title = "Recherche d'employ&eacute;s";
				break;
			default:
				$title = "Gestion des employ&eacute;s";
		}
		return $title;
	}

	function getEmployesOptionList(){
		global $DB;
		$html = "";

		if ($_SESSION['user_type'] == 1) {		
			$DB->query(
				"SELECT `key`, `prenom`, `nom` ".
				"FROM `employes` ".
				"WHERE `actif` = '1' ".
				"ORDER BY `nom` ASC"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$fullname = $DB->getField("nom") . ", " . $DB->getField("prenom");
				$html .= "<option value=\"" . $key . "\">" . $fullname . "</option>\n";
			}

		} elseif (($_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3) && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT DISTINCT e.key AS `key`, e.prenom AS `prenom`, e.nom AS `nom` ".
				"FROM `employes` AS e, `employes_etudes_succursales` AS ees, etudes_succursales AS es ".
				"WHERE e.actif = '1' AND ees.employes_key = e.key AND ees.etudes_succursales_key = es.key AND es.etudes_key = '" . $_SESSION['user_etudes_key'] . "' ".
				"ORDER BY `nom` ASC"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$fullname = $DB->getField("nom") . ", " . $DB->getField("prenom");
				$html .= "<option value=\"" . $key . "\">" . $fullname . "</option>\n";
			}			
		}
		
		return $html;
	}
	function menuActionString() { 
		$html =
			"<h1>Gestion des Employ&eacute;s</h1>\n".
			"<div class=\"menuIn\">\n".
			"	<ul>\n";

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=add\">Ajouter un employ&eacute;</a></li>\n";
		}

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=modify\">Modifier un employ&eacute;</a></li>\n";
		}		

		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
			$html .= "<li><a href=\"index.php?action=delete\">Supprimer un employ&eacute;</a></li>\n";
		}
		
		if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3) {
			$html .= "<li><a href=\"index.php?action=read\">Consulter un employ&eacute;</a></li>\n";
		}
				
		if ($_SESSION['user_type'] == 1) {
			$html .= "<li><a href=\"search.php?action=search\">Rechercher des employ&eacute;s</a></li>\n";
		}
			
		$html .=
			"	</ul>\n".
			"</div>\n";
		return $html;
	}

	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "modify":
				$html .= "<h2>Modifier un employ&eacute;</h2>\n";
				break;
			case "delete":
				$html .= "<h2>Supprimer un employ&eacute;</h2>\n";
				break;
			case "read":
				$html .= "<h2>Consulter un employ&eacute;</h2>\n";
				break;
		}
		return $html;
	}

	function modifyEmployeString() { 
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir un employ&eacute; -</option>\n";

		$html .=
			getEmployesOptionList().
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function readEmployeString() { 
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir un employ&eacute; -</option>\n";

		$html .=
			getEmployesOptionList().
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function deleteEmployeString() {
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir un employ&eacute; -</option>\n";

		$html .=
			getEmployesOptionList().
			"		</select>\n".
			"		&nbsp;&nbsp;Confirmer&nbsp: <input type=\"checkbox\" class=\"checkbox\" id=\"confirmation\" name=\"confirmation\">\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Supprimer\" />\n".
			"</form>\n";

		return $html;
	}

	function deleteEmployeFromDB($key) {
		if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
			exit;
			break;
		}

		global $DB, $BASEPATH;
		$errorString = "";
		$Employe = new Employe($DB, "", 1);
		$Employe->setQKey("key", $key);
		if (!$Employe->set("actif", "0")) {
			$errorString .= _error("Une erreur s'est produite lors de la suppression des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
		}

		$valeurs = "Etude : Voir la fiche (http://bd.pmeinter.ca/etudes/succursales.php?action=modify&begin=1&key=" . $Employe->get("etudes_succursales_key") . ")\n";
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

		// Additional headers
		$headers .= 'From: PME Inter DB <info@pmeinter.com>' . "\r\n";
		$headers .= 'Cc: info@pmeinter.com' . "\r\n";
		//$headers .= 'Bcc: viensf@gmail.com' . "\r\n";
		mail("gdeblois@pmeinter.com", "Suppression du profil de " . $Employe->get("prenom") . " " . $Employe->get("nom"), nl2br($valeurs), $headers);

		return $errorString;
	}


?>
