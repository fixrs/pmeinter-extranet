<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "employes");
		$Skin->assign("errors", $html);
		$Skin->display("employes.tpl");
		exit;
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "employes");
		$Skin->assign("errors", $html);
		$Skin->display("employes.tpl");
		exit;
	}

	import("com.pmeinter.Employe");
	import("com.quiboweb.form.Form");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$formString = "";
	$successString = "";
	$errorString = "";

	$begin = getorpost('begin');
	$action = getorpost('action');
	$key = getorpost('key');

	if (getorpost('test')) {
		print_r($_POST);
	}

	switch ($action) {
		case "add" :
			if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
				exit;
				break;
			}

			if ($begin) {
				importForm("employes");
				$FormObject = new Form("", "", getForm("add"));
				$formString = $FormObject->returnForm();

			} else {
				$errorString = validateFormValues("add", getParametersFromPOST());
				if ($errorString == "") {
					$errorString = updateDBValues("add", getParametersFromPOST());
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "employes/index.php\">Retourner au menu pr&eacute;c&eacute;dent.</a>";

						sendComparison(0);
					}

				} else {
					importForm("employes");
					$FormObject = new Form("", getParametersFromPOST(), getForm("add"));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "modify" :
			if ($_SESSION['user_type'] != 1 && $_SESSION['user_type'] != 2) {
				exit;
				break;
			}

			if ($begin) {
				importForm("employes");
				$FormObject = new Form("", getParametersFromDB("modify", $key), getForm("modify"));
				$formString = $FormObject->returnFilledForm();

			} else {
				$errorString = validateFormValues("modify", getParametersFromPOST());
				if ($errorString == "") {

					sendComparison();

					$errorString = updateDBValues("modify", getParametersFromPOST());
					if ($errorString == "") {
						$successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "employes/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
					}

				} else {
					importForm("employes");
					$FormObject = new Form("", getParametersFromPOST(), getForm("modify"));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "read" :
			importForm("employes");
			$FormObject = new Form("", getParametersFromDB("read", $key), getForm("read"));
			$FormObject->setAllHidden();
			$formString = $FormObject->returnFilledForm();
			break;

	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "employes");
	$Skin->assign("page", "employes");
	$Skin->assign("title", pageTitleString($action, $key));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("form_title", formTitleString($action, $key));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->assign("index", $indexString);
	$Skin->display("employes.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function pageTitleString($action, $key) {
		switch ($action) {
			case "add":
				$title = "Ajout d'un employ&eacute;";
				break;
			case "modify":
				$title = "Modification d'un employ&eacute;";
				break;
			case "read":
				$title = "Consultation d'un employ&eacute;";
				break;
			default:
				$title = "Gestion des employ&eacute;s";
		}
		return $title;
	}

	function formTitleString($action, $key) {
		switch ($action) {
			case "add":
				$title = "<h1>Ajout d'un employ&eacute;</h1>";
				break;
			case "modify":
				$title = "<h1>Modification d'un employ&eacute;</h1>";
				break;
			case "read":
				$title = "<h1>Consultation d'un employ&eacute;</h1>";
				break;
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/jquery-1.12.4.js\" type=\"text/javascript\"></script>\n".
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/hta/hta.js\" type=\"text/javascript\"></script>\n".
					"<script type=\"text/javascript\" src=\"" . $JS_URL . "/jHtmlArea/scripts/jHtmlArea-0.8.min.js\"></script>\n".
    				"<link rel=\"Stylesheet\" type=\"text/css\" href=\"" . $JS_URL . "/jHtmlArea/style/jHtmlArea.css\" />\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/employes.js\" type=\"text/javascript\"></script>\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>\n";
				break;
			case "modify":
				$html .=
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/jquery-1.12.4.js\" type=\"text/javascript\"></script>\n".
					//"<script language=\"JavaScript\" src=\"" . $JS_URL . "/hta/hta.js\" type=\"text/javascript\"></script>\n".
					"<script type=\"text/javascript\" src=\"" . $JS_URL . "/jHtmlArea/scripts/jHtmlArea-0.8.min.js\"></script>\n".
    				"<link rel=\"Stylesheet\" type=\"text/css\" href=\"" . $JS_URL . "/jHtmlArea/style/jHtmlArea.css\" />\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/employes.js\" type=\"text/javascript\"></script>\n".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>\n";
				break;
			case "read":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/employes.js\" type=\"text/javascript\"></script>\n";
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "add":
				$html .= "onload=\"javascript: showNotaireFields('add'); initShowChrCount();\"";
				break;
			case "modify":
				$html .= "onload=\"javascript: showNotaireFields('modify'); initShowChrCount();\"";
				break;
			case "read":
				$html .= "onload=\"javascript: showNotaireFields('read');\"";
				break;
		}
		return $html;
	}

	function validateFormValues($action, $parameters) {
		global $DB, $EMPLOYES_IMG_MAX_SIZE, $EMAIL_FORMAT_PATTERN;
		$errorString = "";

		$isNotaire = 0;
		// if (is_array($parameters['fonctions_key']) && count($parameters['fonctions_key']) > 0) {
		// 	foreach ($parameters['fonctions_key'] as $count => $value) {
		// 		if ($value == "8" || $value == "15") $isNotaire = 1;
		// 	}
		// }
		$aFonction = 0;
		for ($n = 0; $n < 60; $n++) {
			if (isset($parameters['fonctions_key_' . $n]) && $parameters['fonctions_key_' . $n] != "") {
				if ($n == 8 || $n == 15) { $isNotaire = 1; }
				$aFonction = 1;
			}
		}

		switch ($action) {
			case "add":
				if ($parameters['nom'] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('nom');\">Veuillez indiquer le nom de l'employ&eacute;.</a>");
				}

				if ($parameters['prenom'] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('prenom');\">Veuillez indiquer le pr&eacute;nom de l'employ&eacute;.</a>");
				}

				if ($parameters['etudes_succursales_key'][0] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('etudes_succursales_key');\">Veuillez choisir au minimum une succursale.</a>");
				}

				if ($parameters['courriel'] != "" && !preg_match($EMAIL_FORMAT_PATTERN, $parameters['courriel'])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('courriel');\">L'adresse courriel fournie n'est pas valide.</a>");

				} elseif ($parameters['courriel'] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('courriel');\">Veuillez fournir une adresse courriel.</a>");
				}

				if ($parameters['ddn_a'] == "" || $parameters['ddn_a'] < 1930 || $parameters['ddn_a'] > date("Y")) {
					$errorString .= _error("<a href=\"javascript: showErrorField('ddn_a');\">Veuillez fournir la date de naissance.</a>");
				}

				if ($parameters['ddn_m'] == "" || $parameters['ddn_m'] > 12 || $parameters['ddn_m'] < 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('ddn_m');\">Veuillez fournir la date de naissance.</a>");
				}

				if ($parameters['ddn_j'] == "" || $parameters['ddn_j'] > 31 || $parameters['ddn_j'] < 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('ddn_j');\">Veuillez fournir la date de naissance.</a>");
				}


				//if (count($parameters['fonctions_key']) == 0) {
				if (!$aFonction) {
					$errorString .= _error("<a href=\"javascript: showErrorField('fonctions_key');\">Veuillez choisir au minimum une fonction &agrave; l'employ&eacute;.</a>");

				} elseif ($isNotaire) {
					if ($parameters['associe'] != "0" && $parameters['associe'] != "1") {
						$errorString .= _error("<a href=\"javascript: showErrorField('associe');\">Veuillez indiquer si le notaire/avocat est un associ&eacute;.</a>");
					}

					//if ($parameters['annee_debut_pratique'] == "" || $parameters['annee_debut_pratique'] < 1900 || $parameters['annee_debut_pratique'] > date("Y")) {
					//	$errorString .= _error("<a href=\"javascript: showErrorField('annee_debut_pratique');\">Veuillez fournir l'ann&eacute;e de d&eacute;but de pratique du notaire.</a>");
					//}

					//if ($parameters['annee_debut_pratique_avocat'] == "" || $parameters['annee_debut_pratique_avocat'] < 1900 || $parameters['annee_debut_pratique_avocat'] > date("Y")) {
					//	$errorString .= _error("<a href=\"javascript: showErrorField('annee_debut_pratique_avocat');\">Veuillez fournir l'ann&eacute;e de d&eacute;but de pratique de l'avocat.</a>");
					//}



					/*if ($parameters['description'] == "") {
						$errorString .= _error("<a href=\"javascript: showErrorField('description');\">Veuillez fournir la description du notaire.</a>");

					} elseif (strlen(html_entity_decode(strip_tags($parameters['description']))) > 1800) {
						$errorString .= _error("<a href=\"javascript: showErrorField('description');\">La description du notaire est trop longue.</a>");
					}*/

					if (strlen(stripslashes(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), html_entity_decode(strip_tags($parameters['description']))))) > 1800) {
						$errorString .= _error("<a href=\"javascript: showErrorField('description');\">Le texte des notes biographiques est trop long. (1750 caract&egrave;res maximum)</a>");
					}

					if (strlen(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), $parameters['expertises_droit_affaires'])) > 420) {
						$errorString .= _error("<a href=\"javascript: showErrorField('expertises_droit_affaires');\">Le texte des expertises du notaire/avocat en droit d'affaires est trop long.</a>");
					}
					if (strlen(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), $parameters['expertises_droit_personne'])) > 420) {
						$errorString .= _error("<a href=\"javascript: showErrorField('expertises_droit_personne');\">Le texte des expertises du notaire/avocat en droit de la personne est trop long.</a>");
					}
					if (strlen(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), $parameters['expertises_droit_immobilier'])) > 420) {
						$errorString .= _error("<a href=\"javascript: showErrorField('expertises_droit_immobilier');\">Le texte des expertises du notaire/avocat en droit immobilier est trop long.</a>");
					}
					if (strlen(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), $parameters['expertises_sectorielles'])) > 420) {
						$errorString .= _error("<a href=\"javascript: showErrorField('expertises_sectorielles');\">Le texte des expertises sectorielles du notaire/avocat est trop long.</a>");
					}

					if ($_FILES['image_up']['name'] != "") {
						$file_error = $_FILES['image_up']['error'];
						$file_type = $_FILES['image_up']['type'];
						$file_size = $_FILES['image_up']['size'];
						if ($file_error) {
							$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">Une erreur s'est produite avec l'image de l'employ&eacute;.</a>");
						}
						if (!preg_match("/(gif|png|jpg|jpeg)/", $file_type)) {
							$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">Le format de l'image de l'employ&eacute; n'est pas valide.</a>");
						} elseif ($file_size == 0 || $file_size > $EMPLOYES_IMG_MAX_SIZE) {
							$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">La grosseur de l'image de l'employ&eacute; est trop grande (maximum&nbsp;: " . format_size($EMPLOYES_IMG_MAX_SIZE) . ").</a>");
						}
					}
				}
				break;

			case "modify":
				if ($parameters['nom'] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('nom');\">Veuillez indiquer le nom de l'employ&eacute;.</a>");
				}

				if ($parameters['prenom'] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('prenom');\">Veuillez indiquer le pr&eacute;nom de l'employ&eacute;.</a>");
				}

				if ($parameters['etudes_succursales_key'][0] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('etudes_succursales_key');\">Veuillez choisir au minimum une succursale.</a>");
				}

				if ($parameters['courriel'] != "" && !preg_match($EMAIL_FORMAT_PATTERN, $parameters['courriel'])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('courriel');\">L'adresse courriel fournie n'est pas valide.</a>");

				} elseif ($parameters['courriel'] == "") {
					$errorString .= _error("<a href=\"javascript: showErrorField('courriel');\">Veuillez fournir une adresse courriel.</a>");
				}

				if ($parameters['ddn_a'] == "" || $parameters['ddn_a'] < 1930 || $parameters['ddn_a'] > date("Y")) {
					$errorString .= _error("<a href=\"javascript: showErrorField('ddn_a');\">Veuillez fournir la date de naissance.</a>");
				}

				if ($parameters['ddn_m'] == "" || $parameters['ddn_m'] > 12 || $parameters['ddn_m'] < 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('ddn_m');\">Veuillez fournir la date de naissance.</a>");
				}

				if ($parameters['ddn_j'] == "" || $parameters['ddn_j'] > 31 || $parameters['ddn_j'] < 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('ddn_j');\">Veuillez fournir la date de naissance.</a>");
				}

				//if (count($parameters['fonctions_key']) == 0) {
				if (!$aFonction) {
					$errorString .= _error("<a href=\"javascript: showErrorField('fonctions_key');\">Veuillez choisir au minimum une fonction &agrave; l'employ&eacute;.</a>");

				} elseif ($isNotaire) {
					if ($parameters['associe'] != "0" && $parameters['associe'] != "1") {
						$errorString .= _error("<a href=\"javascript: showErrorField('associe');\">Veuillez indiquer si le notaire/avocat est un associ&eacute;.</a>");
					}

					//if ($parameters['annee_debut_pratique'] == "" || $parameters['annee_debut_pratique'] < 1900 || $parameters['annee_debut_pratique'] > date("Y")) {
					//	$errorString .= _error("<a href=\"javascript: showErrorField('annee_debut_pratique');\">Veuillez fournir l'ann&eacute;e de d&eacute;but de pratique du notaire.</a>");
					//}



					/*if ($parameters['description'] == "") {
						$errorString .= _error("<a href=\"javascript: showErrorField('description');\">Veuillez fournir la description du notaire.</a>");

					} elseif (strlen(html_entity_decode(strip_tags($parameters['description']))) > 1800) {
						$errorString .= _error("<a href=\"javascript: showErrorField('description');\">La description du notaire est trop longue.</a>");
					}*/



					if (strlen(stripslashes(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), html_entity_decode(strip_tags($parameters['description']))))) > 1800) {
						$errorString .= _error("<a href=\"javascript: showErrorField('description');\">Le texte des notes biographiques est trop long. (1750 caract&egrave;res maximum)</a>" . strlen(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), html_entity_decode(strip_tags($parameters['description'])))));
					}

					if (strlen(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), $parameters['expertises_droit_affaires'])) > 420) {
						$errorString .= _error("<a href=\"javascript: showErrorField('expertises_droit_affaires');\">Le texte des expertises du notaire/avocat en droit d'affaires est trop long.</a>");
					}
					if (strlen(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), $parameters['expertises_droit_personne'])) > 420) {
						$errorString .= _error("<a href=\"javascript: showErrorField('expertises_droit_personne');\">Le texte des expertises du notaire/avocat en droit de la personne est trop long.</a>");
					}
					if (strlen(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), $parameters['expertises_droit_immobilier'])) > 420) {
						$errorString .= _error("<a href=\"javascript: showErrorField('expertises_droit_immobilier');\">Le texte des expertises du notaire/avocat en droit immobilier est trop long.</a>");
					}
					if (strlen(str_replace(array("\n", "\r", "\t"), array(" ", " ", " "), $parameters['expertises_sectorielles'])) > 420) {
						$errorString .= _error("<a href=\"javascript: showErrorField('expertises_sectorielles');\">Le texte des expertises sectorielles du notaire/avocat est trop long.</a>");
					}

					if ($_FILES['image_up']['name'] != "") {
						$file_error = $_FILES['image_up']['error'];
						$file_type = $_FILES['image_up']['type'];
						$file_size = $_FILES['image_up']['size'];
						if ($file_error) {
							$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">Une erreur s'est produite avec l'image de l'employ&eacute;.</a>");
						}
						if (!preg_match("/(gif|png|jpg|jpeg)/", $file_type)) {
							$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">Le format de l'image de l'employ&eacute; n'est pas valide.</a>");
						} elseif ($file_size == 0 || $file_size > $EMPLOYES_IMG_MAX_SIZE) {
							$errorString .= _error("<a href=\"javascript: showErrorField('image_up');\">La grosseur de l'image de l'employ&eacute; est trop grande (maximum&nbsp;: " . format_size($EMPLOYES_IMG_MAX_SIZE) . ").</a>");
						}
					}
				}
				break;
		}
		return $errorString;
	}

	function getParametersFromPOST() {
		$parameters = array();
		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$parameters[$field] = $value;
				} else {
					$parameters[$field] = trim($value);
				}
			}
		}

		if (!isset($_POST["secteur1"])) { $parameters["secteur1"] = ''; }
		if (!isset($_POST["secteur2"])) { $parameters["secteur2"] = ''; }
		if (!isset($_POST["secteur3"])) { $parameters["secteur3"] = ''; }
		if (!isset($_POST["secteur4"])) { $parameters["secteur4"] = ''; }
		if (!isset($_POST["secteur5"])) { $parameters["secteur5"] = ''; }
		if (!isset($_POST["secteuro"])) { $parameters["secteuro"] = ''; }

		return $parameters;
	}

	function getParametersFromDB($action, $key) {
		global $DB, $BASEURL;
		$parameters = array();

		switch ($action) {
			case "modify":
				$Employe = new Employe($DB, "", 0);
				$Employe->setQKey("key", $key);
				$Employe->scanfields();
				$Employe->loadAll();
				$parameters = $Employe->getAll();

				$DB->query(
					"SELECT `etudes_succursales_key` ".
					"FROM `employes_etudes_succursales` ".
					"WHERE `employes_key` = '" . $Employe->get("key") . "';"
				);
				$i = 0;
				while ($DB->next_record()) {
					$parameters['etudes_succursales_key'][$i] = $DB->getField("etudes_succursales_key");
					$i++;
				}

				$DB->query(
					"SELECT `fonctions_key` ".
					"FROM `employes_fonctions` ".
					"WHERE `employes_key` = '" . $Employe->get("key") . "'"
				);
				$i = 0;
				while ($DB->next_record()) {
					//$parameters['fonctions_key'][$i] = $DB->getField("fonctions_key");
					$parameters['fonctions_key_' . $DB->getField("fonctions_key")] = $DB->getField("fonctions_key");
					$i++;
				}

				/*$DB->query(
					"SELECT `titres_key` ".
					"FROM `employes_titres` ".
					"WHERE `employes_key` = '" . $Employe->get("key") . "'"
				);
				$i = 0;
				while ($DB->next_record()) {
					$parameters['titres_key'][$i] = $DB->getField("titres_key");
					$i++;
				}*/

				$DB->query(
					"SELECT `directions_de_travail_key` ".
					"FROM `employes_directions_de_travail` ".
					"WHERE `employes_key` = '" . $Employe->get("key") . "'"
				);
				$i = 0;
				while ($DB->next_record()) {
					$parameters['directions_de_travail_key'][$i] = $DB->getField("directions_de_travail_key");
					$i++;
				}



				/*$DB->query(
					"SELECT ee.expertises_key AS expertises_key, e.domaines_daffaires_key AS domaines_daffaires_key ".
					"FROM `employes_expertises` AS ee, expertises AS e ".
					"WHERE `employes_key` = '" . $Employe->get("key") . "'"
				);
				$i = array(0, 0, 0, 0);
				while ($DB->next_record()) {
					switch ($DB->getField("domaines_daffaires_key")) {
						 case "1":
						 	$k = $i[0];
						 	$parameters['expertises_droit_des_affaires'][$k] = $DB->getField("expertises_key");
						 	$i[0]++;
						 	break;
						 case "2":
						 	$k = $i[1];
						 	$parameters['expertises_droit_des_personnes'][$k] = $DB->getField("expertises_key");
						 	$i[1]++;
						 	break;
						 case "3":
						 	$k = $i[2];
						 	$parameters['expertises_droit_immobilier'][$k] = $DB->getField("expertises_key");
						 	$i[2]++;
						 	break;
						 case "4":
						 	$k = $i[3];
						 	$parameters['expertises_autre'][$k] = $DB->getField("expertises_key");
						 	$i[3]++;
						 	break;
					}
				}*/
				break;

			case "read":
				$Employe = new Employe($DB, "", 0);
				$Employe->setQKey("key", $key);
				$Employe->scanfields();
				$Employe->loadAll();
				$parameters = $Employe->getAll();

				if ($parameters['associe'] == "1") {
					$parameters['associe'] = "Oui";
				} else {
					$parameters['associe'] = "Non";
				}

				$parameters['courriel'] = "<a href=\"mailto:" . $parameters['courriel'] . "\" title=\"Envoyer un courriel\">" . $parameters['courriel'] . "</a>";

				$DB->query(
					"SELECT e.nom AS etude_nom, e.actif AS active, es.key AS etudes_succursales_key, es.siege_social AS siege_social, es.adresse AS adresse, es.ville AS ville ".
					"FROM `etudes` AS e, `etudes_succursales` AS es, `employes_etudes_succursales` AS ees ".
					"WHERE e.key = es.etudes_key AND ees.employes_key = '" . $key . "' AND ees.etudes_succursales_key = es.key;"
				);
				$parameters['etudes_succursales_nom'] = "<ul class=\"answer\">\n";
				while ($DB->next_record()) {
					$etudes_succursales_nom = $DB->getField("etude_nom") . " (" . $DB->getField("adresse") . ", " . $DB->getField("ville") . ") ";
					if ($DB->getField("siege_social") == 1) {
						$etudes_succursales_nom .= "[SS] ";
					}
					if ($DB->getField("active") == 1) {
						$parameters['etudes_succursales_nom'] .= "<li><a href=\"" . $BASEURL . "etudes/succursales.php?action=read&begin=1&key=" . $DB->getField("etudes_succursales_key") . "\" title=\"Consulter\">" . $etudes_succursales_nom . "</a></li>\n";
					} else {
						$parameters['etudes_succursales_nom'] .= "<li>" . $etudes_succursales_nom . " * NON ACTIVE *</li>\n";
					}
				}
				$parameters['etudes_succursales_nom'] .= "</ul>\n";
				if ($etudes_succursales_nom == "") {
					$parameters['etudes_succursales_nom'] = "<span class=\"answer\">(aucune)</span>";
				}

				$DB->query(
					"SELECT f.nom AS fonction_nom, f.key AS fonction_key ".
					"FROM `fonctions` AS f, `employes_fonctions` AS ef ".
					"WHERE ef.employes_key = '" . $key . "' AND ef.fonctions_key = f.key"
				);
				$parameters['fonctions_nom'] = "<ul class=\"answer\">\n";
				$is_notaire = 0;
				while ($DB->next_record()) {
					if ($DB->getField('fonction_key') == 8 || $DB->getField("fonction_key") == 15) {
						$is_notaire = 1;
					}
					$parameters['fonctions_nom'] .= "<li>" . $DB->getField("fonction_nom") . "</li>\n";
				}
				$parameters['fonctions_nom'] .= "</ul>\n";
				if ($parameters['fonctions_nom'] == "<ul class=\"answer\">\n</ul>\n") {
					$parameters['fonctions_nom'] = "<span class=\"answer\">(aucune)</span>";
				}

				if ($is_notaire) {
					$parameters['fonctions_nom'] .= "<span id=\"is_notaire\" style=\"display:none;\">1</span>";
				} else {
					$parameters['fonctions_nom'] .= "<span id=\"is_notaire\" style=\"display:none;\">0</span>";
				}

				if ($parameters['ddn_a'] == "") {
					$parameters['ddn_a'] = "????";
				}
				if (strlen($parameters['ddn_m']) == 1) {
					$parameters['ddn_m'] = "0" . $parameters['ddn_m'];
				}
				if (strlen($parameters['ddn_j']) == 1) {
					$parameters['ddn_j'] = "0" . $parameters['ddn_j'];
				}

				/*$DB->query(
					"SELECT t.nom AS titre_nom ".
					"FROM `titres` AS t, `employes_titres` AS et ".
					"WHERE et.employes_key = '" . $key . "' AND et.titres_key = t.key"
				);
				$parameters['titres_nom'] = "<ul class=\"answer\">\n";
				while ($DB->next_record()) {
					$parameters['titres_nom'] .= "<li>" . $DB->getField("titre_nom") . "</li>\n";
				}
				$parameters['titres_nom'] .= "</ul>\n";
				if ($parameters['titres_nom'] == "<ul class=\"answer\">\n</ul>\n") {
					$parameters['titres_nom'] = "<span class=\"answer\">(aucun)</span>";
				}*/

				$DB->query(
					"SELECT d.nom AS direction_nom ".
					"FROM `directions_de_travail` AS d, `employes_directions_de_travail` AS ed ".
					"WHERE ed.employes_key = '" . $key . "' AND ed.directions_de_travail_key = d.key"
				);
				$parameters['direction_nom'] = "<ul class=\"answer\">\n";
				while ($DB->next_record()) {
					$parameters['direction_nom'] .= "<li>" . $DB->getField("direction_nom") . "</li>\n";
				}
				$parameters['direction_nom'] .= "</ul>\n";
				if ($parameters['direction_nom'] == "<ul class=\"answer\">\n</ul>\n") {
					$parameters['direction_nom'] = "<span class=\"answer\">(aucun)</span>";
				}


				break;
		}

		return $parameters;
	}

	function updateDBValues($action, $parameters) {
		global $DB, $BASEURL, $BASEPATH;
		$errorString = "";

		$parameters = addslashesToValues($parameters);

		$isNotaire = 0;
		for ($n = 0; $n < 60; $n++) {

			if (isset($parameters['fonctions_key_' . $n]) && $parameters['fonctions_key_' . $n] != "") {
				if ($n == "8" || $n == "15") $isNotaire = 1;
			}

			// if (is_array($parameters['fonctions_key']) && count($parameters['fonctions_key']) > 0) {
			// 	foreach ($parameters['fonctions_key'] as $count => $value) {
			// 		if ($value == "8" || $value == "15") $isNotaire = 1;
			// 	}
			// }
		}

		switch ($action) {
			case "add":
				$Employe = new Employe($DB, "", 0);
				$Employe->scanfields();
				$Employe->insertmode();
				$Employe->set("actif", "1");
				$Employe->set("lastmod", "CURRENT_TIMESTAMP", 1);
				$Employe->setAll($parameters);

				if (!$Employe->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
					break;
				}

				$employes_key = $Employe->getInsertId();
				$Employe->setQKey("key", $employes_key);
				$Employe->setInsertmode(0);

				if (is_array($parameters['etudes_succursales_key']) && count($parameters['etudes_succursales_key']) > 0) {
					foreach ($parameters['etudes_succursales_key'] as $count => $value) {
						$query_etudes_succursales =
							"INSERT INTO `employes_etudes_succursales` ".
							"(`employes_key`, `etudes_succursales_key`) ".
							"VALUES ('" . $employes_key . "', '" . $value . "')";
						$DB->query($query_etudes_succursales);
					}
				}


				for ($n = 0; $n < 60; $n++) {

					if (isset($parameters['fonctions_key_' . $n]) && $parameters['fonctions_key_' . $n] != "") {
						$query_fonctions =
							"INSERT INTO `employes_fonctions` ".
							"(`employes_key`, `fonctions_key`) ".
							"VALUES ('" . $employes_key . "', '" . $n . "')";
						$DB->query($query_fonctions);
					}
				}


				// if (is_array($parameters['fonctions_key']) && count($parameters['fonctions_key']) > 0) {
				// 	foreach ($parameters['fonctions_key'] as $count => $value) {
				// 		$query_fonctions =
				// 			"INSERT INTO `employes_fonctions` ".
				// 			"(`employes_key`, `fonctions_key`) ".
				// 			"VALUES ('" . $employes_key . "', '" . $value . "')";
				// 		$DB->query($query_fonctions);
				// 	}
				// }

				/*if (is_array($parameters['titres_key']) && count($parameters['titres_key']) > 0) {
					foreach ($parameters['titres_key'] as $count => $value) {
						$query_titres =
							"INSERT INTO `employes_titres` ".
							"(`employes_key`, `titres_key`) ".
							"VALUES ('" . $employes_key . "', '" . $value . "')";
						$DB->query($query_titres);
					}
				}*/

				if (is_array($parameters['directions_de_travail_key']) && count($parameters['directions_de_travail_key']) > 0) {
					foreach ($parameters['directions_de_travail_key'] as $count => $value) {
						$query_directions_de_travail =
							"INSERT INTO `employes_directions_de_travail` ".
							"(`employes_key`, `directions_de_travail_key`) ".
							"VALUES ('" . $employes_key . "', '" . $value . "')";
						$DB->query($query_directions_de_travail);
					}
				}


				/*if (is_array($parameters['expertises_droit_des_affaires']) && count($parameters['expertises_droit_des_affaires']) > 0) {
					foreach ($parameters['expertises_droit_des_affaires'] as $count => $value) {
						$query_expertises_droit_des_affaires =
							"INSERT INTO `employes_expertises` ".
							"(`employes_key`, `expertises_key`) ".
							"VALUES ('" . $employes_key . "', '" . $value . "')";
						$DB->query($query_expertises_droit_des_affaires);
					}
				}

				if (is_array($parameters['expertises_droit_des_personnes']) && count($parameters['expertises_droit_des_personnes']) > 0) {
					foreach ($parameters['expertises_droit_des_personnes'] as $count => $value) {
						$query_expertises_droit_des_personnes =
							"INSERT INTO `employes_expertises` ".
							"(`employes_key`, `expertises_key`) ".
							"VALUES ('" . $employes_key . "', '" . $value . "')";
						$DB->query($query_expertises_droit_des_personnes);
					}
				}

				if (is_array($parameters['expertises_droit_immobilier']) && count($parameters['expertises_droit_immobilier']) > 0) {
					foreach ($parameters['expertises_droit_immobilier'] as $count => $value) {
						$query_expertises_droit_immobilier =
							"INSERT INTO `employes_expertises` ".
							"(`employes_key`, `expertises_key`) ".
							"VALUES ('" . $employes_key . "', '" . $value . "')";
						$DB->query($query_expertises_droit_immobilier);
					}
				}

				if (is_array($parameters['expertises_autre']) && count($parameters['expertises_autre']) > 0) {
					foreach ($parameters['expertises_autre'] as $count => $value) {
						$query_expertises_autre =
							"INSERT INTO `employes_expertises` ".
							"(`employes_key`, `expertises_key`) ".
							"VALUES ('" . $employes_key . "', '" . $value . "')";
						$DB->query($query_expertises_autre);
					}
				}*/

				if ($isNotaire) {
					if ($_FILES["image_up"]["name"] != "") {
						$tmp_name = $_FILES["image_up"]["tmp_name"];
						$name = $_FILES["image_up"]["name"];
						$extension = substr($name, strrpos($name, "."));
						$newname = $employes_key . $extension;
						$move = move_uploaded_file($tmp_name, $BASEPATH . "docs/employes_img/" . $newname);
						$Employe->set("image", $newname);
						if (!$Employe->save("image")) {
							$errorString .= _error("Une erreur s'est produite lors de l'insertion de l'image sur le serveur.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
							break;
						}
					}
				}
				break;

			case "modify":
				$Employe = new Employe($DB, "", 0);
				$Employe->scanfields();
				$Employe->setQKey("key", $parameters['key']);
				$Employe->setAll($parameters);
				$Employe->set("lastmod", "CURRENT_TIMESTAMP", 1);

				$DB->query(
					"DELETE ".
					"FROM `employes_etudes_succursales` ".
					"WHERE `employes_key` = '" . $parameters['key'] . "'"
				);
				if (is_array($parameters['etudes_succursales_key']) && count($parameters['etudes_succursales_key']) > 0) {
					foreach ($parameters['etudes_succursales_key'] as $count => $value) {
						$query_etudes_succursales =
							"INSERT INTO `employes_etudes_succursales` ".
							"(`employes_key`, `etudes_succursales_key`) ".
							"VALUES ('" . $parameters['key'] . "', '" . $value . "')";
						$DB->query($query_etudes_succursales);
					}
				}

				$DB->query(
					"DELETE ".
					"FROM `employes_fonctions` ".
					"WHERE `employes_key` = '" . $parameters['key'] . "'"
				);

				for ($n = 0; $n < 60; $n++) {
					if (isset($parameters['fonctions_key_' . $n]) && $parameters['fonctions_key_' . $n] != "") {
						$query_fonctions =
							"INSERT INTO `employes_fonctions` ".
							"(`employes_key`, `fonctions_key`) ".
							"VALUES ('" . $parameters['key'] . "', '" . $n . "')";
						$DB->query($query_fonctions);
					}
				}

				// if (is_array($parameters['fonctions_key']) && count($parameters['fonctions_key']) > 0) {
				// 	foreach ($parameters['fonctions_key'] as $count => $value) {
				// 		$query_fonctions =
				// 			"INSERT INTO `employes_fonctions` ".
				// 			"(`employes_key`, `fonctions_key`) ".
				// 			"VALUES ('" . $parameters['key'] . "', '" . $value . "')";
				// 		$DB->query($query_fonctions);
				// 	}
				// }

				/*$DB->query(
					"DELETE ".
					"FROM `employes_titres` ".
					"WHERE `employes_key` = '" . $parameters['key'] . "'"
				);
				if (is_array($parameters['titres_key']) && count($parameters['titres_key']) > 0) {
					foreach ($parameters['titres_key'] as $count => $value) {
						$query_titres =
							"INSERT INTO `employes_titres` ".
							"(`employes_key`, `titres_key`) ".
							"VALUES ('" . $parameters['key'] . "', '" . $value . "')";
						$DB->query($query_titres);
					}
				}*/

				$DB->query(
					"DELETE ".
					"FROM `employes_directions_de_travail` ".
					"WHERE `employes_key` = '" . $parameters['key'] . "'"
				);
				if (is_array($parameters['directions_de_travail_key']) && count($parameters['directions_de_travail_key']) > 0) {
					foreach ($parameters['directions_de_travail_key'] as $count => $value) {
						$query_directions_de_travail =
							"INSERT INTO `employes_directions_de_travail` ".
							"(`employes_key`, `directions_de_travail_key`) ".
							"VALUES ('" . $parameters['key'] . "', '" . $value . "')";
						$DB->query($query_directions_de_travail);
					}
				}



				/*$DB->query(
					"DELETE ".
					"FROM `employes_expertises` ".
					"WHERE `employes_key` = '" . $parameters['key'] . "'"
				);
				if (is_array($parameters['expertises_droit_des_affaires']) && count($parameters['expertises_droit_des_affaires']) > 0) {
					foreach ($parameters['expertises_droit_des_affaires'] as $count => $value) {
						$query_expertises_droit_des_affaires =
							"INSERT INTO `employes_expertises` ".
							"(`employes_key`, `expertises_key`) ".
							"VALUES ('" . $parameters['key'] . "', '" . $value . "')";
						$DB->query($query_expertises_droit_des_affaires);
					}
				}
				if (is_array($parameters['expertises_droit_des_personnes']) && count($parameters['expertises_droit_des_personnes']) > 0) {
					foreach ($parameters['expertises_droit_des_personnes'] as $count => $value) {
						$query_expertises_droit_des_personnes =
							"INSERT INTO `employes_expertises` ".
							"(`employes_key`, `expertises_key`) ".
							"VALUES ('" . $parameters['key'] . "', '" . $value . "')";
						$DB->query($query_expertises_droit_des_personnes);
					}
				}
				if (is_array($parameters['expertises_droit_immobilier']) && count($parameters['expertises_droit_immobilier']) > 0) {
					foreach ($parameters['expertises_droit_immobilier'] as $count => $value) {
						$query_expertises_droit_immobilier =
							"INSERT INTO `employes_expertises` ".
							"(`employes_key`, `expertises_key`) ".
							"VALUES ('" . $parameters['key'] . "', '" . $value . "')";
						$DB->query($query_expertises_droit_immobilier);
					}
				}
				if (is_array($parameters['expertises_autre']) && count($parameters['expertises_autre']) > 0) {
					foreach ($parameters['expertises_autre'] as $count => $value) {
						$query_expertises_autre =
							"INSERT INTO `employes_expertises` ".
							"(`employes_key`, `expertises_key`) ".
							"VALUES ('" . $parameters['key'] . "', '" . $value . "')";
						$DB->query($query_expertises_autre);
					}
				}*/

				if ($isNotaire) {
					if ($_FILES["image_up"]["name"] != "") {
						$tmp_name = $_FILES["image_up"]["tmp_name"];
						$name = $_FILES["image_up"]["name"];
						$extension = substr($name, strrpos($name, "."));
						$newname = $parameters['key'] . $extension;
						$move = move_uploaded_file($tmp_name, $BASEPATH . "docs/employes_img/" . $newname);
						$Employe->set("image", $newname);
						if (!$Employe->save("image")) {
							$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
							break;
						}

					} else {
						if ($parameters['image_del'] == "1") {
							$image = $parameters['image'];
							$image_path = $BASEPATH . "docs/employes_img/" . $image;
							if (file_exists($image_path)) {
								unlink($image_path);
							}
							$Employe->set("image", "NULL", 1);
						} else {
							$Employe->excludefield("image");
						}
					}

				} else {

					$Employe->set("annee_debut_pratique", "NULL", 1);
					$Employe->set("annee_debut_pratique_avocat", "NULL", 1);
					$Employe->set("description", "NULL", 1);
					$Employe->set("image", "NULL", 1);
					//$Employe->set("droit_des_affaires_desc", "NULL", 1);
					//$Employe->set("droit_des_personnes_desc", "NULL", 1);
					//$Employe->set("droit_immobilier_desc", "NULL", 1);
					//$Employe->set("expertises_sectorielles_desc", "NULL", 1);
				}

				if (!$Employe->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion de l'image sur le serveur.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
				}

				break;
		}
		return $errorString;
	}

	function addslashesToValues($parameters = "") {
		if (is_array($parameters) && count($parameters) > 0) {
			foreach ($parameters as $field => $value) {
				if (is_array($value) && count($value) > 0) {
					foreach ($value as $field2 => $value2) {
						if (is_array($value2) && count($value2) > 0) {
							foreach ($value2 as $field3 => $value3) {
								$parameters[$field][$field2][$field3] = addslashes($value3);
							}
						} else {
							$parameters[$field][$field2] = addslashes($value2);
						}
					}
				} else {
					$parameters[$field] = addslashes($value);
				}
			}
		} else {
			$parameters = addslashes($parameters);
		}
		return $parameters;
	}

	function sendComparison($modify = 1) {
		global $_POST;

		$oldvalues = array();

		if ($modify) {
			$oldvalues = getParametersFromDB("modify", $_POST["key"]);
		}

		$valeurs =
			"Prenom : " . $_POST["prenom"] . (isset($oldvalues["prenom"]) ? " <em>(" . $oldvalues["prenom"] . ")</em>\n" : "\n").
			"Nom : " . $_POST["nom"] . (isset($oldvalues["nom"]) ? " <em>(" . $oldvalues["nom"] . ")</em>\n" : "\n").
			"Courriel : " . $_POST["courriel"] . (isset($oldvalues["courriel"]) ? " <em>(" . $oldvalues["courriel"] . ")</em>\n" : "\n").
			"Date de naissance : " . $_POST["ddn_a"] . "-" . $_POST["ddn_m"] . "-" . $_POST["ddn_j"] . (isset($oldvalues["ddn_a"]) && isset($oldvalues["ddn_m"]) && isset($oldvalues["ddn_j"]) ? " <em>(" . $oldvalues["ddn_a"] . "-" . $oldvalues["ddn_m"] . "-" . $oldvalues["ddn_j"] . ")</em>\n" : "\n");

		if (is_array($_POST["etudes_succursales_key"])) {
			for ($i = 0; $i < count($_POST["etudes_succursales_key"]); $i++) {
				$valeurs .=
					"Etude : Voir la fiche (http://bd.pmeinter.ca/etudes/succursales.php?action=modify&begin=1&key=" . $_POST["etudes_succursales_key"][$i] . ")\n";
			}
		} else {
			$valeurs .=
				"Etude : Voir la fiche (http://bd.pmeinter.ca/etudes/succursales.php?action=modify&begin=1&key=" . $_POST["etudes_succursales_key"] . ")\n";
		}

		if (isset($oldvalues["etudes_succursales_key"])) {
			if (is_array($oldvalues["etudes_succursales_key"])) {
				for ($i = 0; $i < count($oldvalues["etudes_succursales_key"]); $i++) {
					$valeurs .=
						"<em>Etude : Voir la fiche (http://bd.pmeinter.ca/etudes/succursales.php?action=modify&begin=1&key=" . $oldvalues["etudes_succursales_key"][$i] . ")</em>\n";
				}
			} else {
				$valeurs .=
					"<em>Etude : Voir la fiche (http://bd.pmeinter.ca/etudes/succursales.php?action=modify&begin=1&key=" . $oldvalues["etudes_succursales_key"] . ")</em>\n";
			}
		}

		$fonctions = array();
		$fonctions[1] = "Collaborateur / Technicien juridique";
		$fonctions[4] =	"Comptable";
		$fonctions[15] =	"Avocat";
		$fonctions[16] =	"Agent de liaison collaboratrice";
		$fonctions[18] =	"Notaire retraité";
		$fonctions[8] =	"Notaire";
		$fonctions[10] =	"Autre";
		$fonctions[11] =	"Agent de liaison";
		$fonctions[12] =	"Directeur général";
		$fonctions[13] =	"Directeur du développement des affaires";

		if (is_array($_POST["fonctions_key"])) {
			for ($i = 0; $i < count($_POST["fonctions_key"]); $i++) {
				$valeurs .=
					"Fonction : " . $fonctions[$_POST["fonctions_key"][$i]] . "\n";
			}
		} else {
			$valeurs .=
				"Fonction : " . $fonctions[$_POST["fonctions_key"]] . "\n";
		}

		if (isset($oldvalues["fonctions_key"])) {
			if (is_array($oldvalues["fonctions_key"])) {
				for ($i = 0; $i < count($oldvalues["fonctions_key"]); $i++) {
					$valeurs .=
						"<em>Fonction : " . $fonctions[$oldvalues["fonctions_key"][$i]] . "</em>\n";
				}
			} else {
				$valeurs .=
					"<em>Fonction : " . $fonctions[$oldvalues["fonctions_key"]] . "</em>\n";
			}
		}


		$directions = array();
		$directions[1] = "droit des affaires";
		$directions[2] = "droit de la personne";
		$directions[3] = "gestion de bureaux";
		$directions[4] = "droit immobilier";
		$directions[5] = "droit agricole";
		$directions[6] = "développement des affaires";
		$directions[7] = "action des collaboratrices";

		if (is_array($_POST["directions_de_travail_key"])) {
			for ($i = 0; $i < count($_POST["directions_de_travail_key"]); $i++) {
				$valeurs .=
					"Direction de travail : " . $directions[$_POST["directions_de_travail_key"][$i]] . "\n";
			}
		} else {
			$valeurs .=
				"Direction de travail : " . $directions[$_POST["directions_de_travail_key"]] . "\n";
		}

		if (isset($oldvalues["directions_de_travail_key"])) {
			if (is_array($oldvalues["directions_de_travail_key"])) {
				for ($i = 0; $i < count($oldvalues["directions_de_travail_key"]); $i++) {
					$valeurs .=
						"<em>Direction de travail : " . $directions[$oldvalues["directions_de_travail_key"][$i]] . "</em>\n";
				}
			} else {
				$valeurs .=
					"<em>Direction de travail : " . $directions[$oldvalues["directions_de_travail_key"]] . "</em>\n";
			}
		}


		if ($_POST["associe"] == "0" || $_POST["associe"] == "") {
			$valeurs .=
				"Associe : Non\n";
		} else {
			$valeurs .=
				"Associe : Oui\n";
		}

		if (isset($oldvalues["associe"])) {
			if ($oldvalues["associe"] == "0" || $oldvalues["associe"] == "") {
				$valeurs .=
					"<em>Associe : Non</em>\n";
			} else {
				$valeurs .=
					"<em>Associe : Oui</em>\n";
			}
		}

		$valeurs .=
				"Annee debut pratique : " . $_POST["annee_debut_pratique"] . (isset($oldvalues["annee_debut_pratique"]) ? " <em>(" . $oldvalues["annee_debut_pratique"] . ")</em>\n" : "\n").
				"Annee debut pratique avocat : " . $_POST["annee_debut_pratique_avocat"] . (isset($oldvalues["annee_debut_pratique_avocat"]) ? " <em>(" . $oldvalues["annee_debut_pratique_avocat"] . ")</em>\n" : "\n").
				"Description : " . $_POST["description"] . (isset($oldvalues["description"]) ? " <em>(" . $oldvalues["description"] . ")</em>\n" : "\n").
				"Secteurs : " . $_POST["secteur1"] . " " . $_POST["secteur2"] . " " . $_POST["secteur3"] . " " . $_POST["secteur4"] . " " . $_POST["secteur5"] . " " . $_POST["secteuro"] . (isset($oldvalues["secteur1"]) && isset($oldvalues["secteur2"]) && isset($oldvalues["secteur3"]) && isset($oldvalues["secteur4"]) && isset($oldvalues["secteur5"]) && isset($oldvalues["secteuro"]) ? " <em>(" . $oldvalues["secteur1"] . " " . $oldvalues["secteur2"] . " " . $oldvalues["secteur3"] . " " . $oldvalues["secteur4"] . " " . $oldvalues["secteur5"] . " " . $oldvalues["secteuro"] . ")</em>\n" : "\n");


		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

		// Additional headers
		$headers .= 'From: PME Inter DB <info@pmeinter.com>' . "\r\n";
		$headers .= 'Cc: info@pmeinter.com' . "\r\n";
		//$headers .= 'Bcc: viensf@gmail.com' . "\r\n";

		if ($modify == 1) {
			mail("gdeblois@pmeinter.com", "Changement du profil de " . $_POST["prenom"] . " " . $_POST["nom"], nl2br($valeurs), $headers);
		} else {
			mail("gdeblois@pmeinter.com", "Ajout du profil de " . $_POST["prenom"] . " " . $_POST["nom"], nl2br($valeurs), $headers);
		}

	}

?>
