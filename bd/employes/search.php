<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "employes");
		$Skin->assign("errors", $html);
		$Skin->display("employes.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "employes");
		$Skin->assign("errors", $html);
		$Skin->display("employes.tpl");
		exit;	
	}
	
	import("com.pmeinter.Employe");
	import("com.quiboweb.form.Form");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$successString = "";
	$errorString = "";
	$formString = "";
	$expertisesArray = array('droit_des_affaires_desc'=> 'Droit des affaires', 'droit_des_personnes_desc'=>'Droit des personnes', 'droit_immobilier_desc'=>'Droit immobilier', 'expertises_sectorielles_desc'=> 'Expertises sectorielles');
	
	$action = getorpost('action');
	$key = getorpost('key');

	switch ($_GET['action']) {
		case "liste":
			$DB->query(
				"SELECT em.*, f.nom AS fonction_nom, et.nom AS etude_nom, ets.adresse, ets.ville ".
				"FROM `etudes` et ".
				"INNER JOIN etudes_succursales ets ON ets.etudes_key = et.key AND et.actif = 1 AND ets.actif=1 ".
				"INNER JOIN employes_etudes_succursales ees ON ees.etudes_succursales_key = ets.key ".
				"INNER JOIN employes em ON ees.employes_key = em.key AND em.actif = 1 ".
				"LEFT JOIN employes_fonctions ef ON ef.employes_key = em.key ".
				"LEFT JOIN fonctions f ON ef.fonctions_key = f.key ".
				"WHERE 1 ".
				"ORDER BY et.nom, ets.ville, em.nom"
			);

			$last_etude = "";
			$last_succ = "";
			$labels = 
				"	<tr>\n".
				"		<td>Pr&eacute;nom</td>\n".
				"		<td>Nom</td>\n".
				"		<td>Courriel</td>\n".
				"		<td>Notaire depuis</td>\n".
				"		<td>Avocat depuis</td>\n".
				"		<td>Date de naissance</td>\n".
				"		<td>Est associ&eacute;</td>\n".
				"		<td>Description</td>\n".
				"		<td>Droit des affaires</td>\n".
				"		<td>Droit de la personne</td>\n".
				"		<td>Droit immobilier</td>\n".
				"		<td>Expertises sectorielles</td>\n".
				"		<td>Photo</td>\n".
				"		<td>Fonction</td>\n".
				"	</tr>\n";

			$formString .= "<table border=\"1\">\n";
			while ($DB->next_record()) {
				if ($last_etude != $DB->getField("etude_nom")) {
					$formString .= 
						"</table>\n".
						"<h2>" . $DB->getField("etude_nom") . "</h2>\n".
						"<table border=\"1\" style=\"background-color: #FFF;\">\n".
						$labels;

					$last_etude = $DB->getField("etude_nom");
				}

				if ($last_succ != $DB->getField("adresse") . ", " . $DB->getField("ville")) {
					$formString .= 
						"	<tr>\n".
						"		<td colspan=\"14\" style=\"text-align: center;\">" . $DB->getField("adresse") . ", " . $DB->getField("ville") . "</td>\n".
						"	</tr>\n";

					$last_succ = $DB->getField("adresse") . ", " . $DB->getField("ville");
				}

				$formString .= 
					"	<tr>\n".
					"		<td>" . $DB->getField("prenom") . "</td>\n".
					"		<td>" . $DB->getField("nom") . "</td>\n".
					"		<td>" . $DB->getField("courriel") . "</td>\n".
					"		<td>" . $DB->getField("annee_debut_pratique") . "</td>\n".
					"		<td>" . $DB->getField("annee_debut_pratique_avocat") . "</td>\n".
					"		<td>" . $DB->getField("ddn_a") . "-" . $DB->getField("ddn_m") . "-" . $DB->getField("ddn_j") . "</td>\n".
					"		<td>" . ($DB->getField("associe") ? "Yes" : "No") . "</td>\n".
					"		<td>" . preg_replace("/<\/?(font|span)[^>]*?>/is", "", preg_replace("/(<\/?p)[^>]*?>/is", "$1>", $DB->getField("description"))) . "</td>\n".
					"		<td>" . $DB->getField("expertises_droit_affaires") . "</td>\n".
					"		<td>" . $DB->getField("expertises_droit_personne") . "</td>\n".
					"		<td>" . $DB->getField("expertises_droit_immobilier") . "</td>\n".
					"		<td>" . $DB->getField("expertises_sectorielles") . "</td>\n".
					"		<td>" . ($DB->getField("image") ? "<img src=\"http://bd.pmeinter.ca/docs/employes_img/" . $DB->getField("image") . "\" style=\"width: 100px; height: auto;\" />" : "") . "</td>\n".
					"		<td>" . $DB->getField("fonction_nom") . "</td>\n".
					"	</tr>\n";
			}
			$formString .= "</table>\n";
			
			break;
		case "search":
			$formString .= validateJSString().searchString();
			break;
		case "results":
			$formString .= validateJSString().searchResultsString();
			break;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "employes");
	$Skin->assign("page", "employes");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("menu_title", menuTitleString($action));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("employes.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function pageTitleString($action) { 
		switch ($action) {
			case "search":
				$title = "Recherche d'employ&eacute;s";
				break;
			case "results":
				$title = "R&eacute;sultats de recherche d'employ&eacute;s";
				break;
			default:
				$title = "Recherche d'employ&eacute;s";
		}
		return $title;
	}

	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "search":
				$html .= "<h1>Rechercher des employ&eacute;s</h1>\n";
				break;
			case "results":
				$html .= "<h1>Rechercher des employ&eacute;s</h1>\n";
				break;
		}
		return $html;
	}

	function getEtudesList($key=""){
		global $DB;
		$html = 
			"	<select id=\"etudes_key\" name=\"etudes_key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir une &eacute;tude (facultatif) -</option>\n";

		$DB->query(
			"SELECT `key`, `nom` ".
			"FROM `etudes` " .
			"WHERE `actif`='1' ".
			"ORDER BY `nom` ASC"
		);

		while($DB->next_record()) {
			$selected = "";
			if ($key!="" && $key!="-1" && $key==$DB->getField("key")){
				$selected = " selected=\"selected\" ";
			}
			$html .= "<option ".$selected."value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . "</option>\n";
		}
		
		$html .= 
			"		</select>\n";

		return $html;
	}

	function getFonctionsList($key=""){
		global $DB;
		$html = 
			"	<select id=\"fonctions_key\" name=\"fonctions_key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir une fonction (facultatif) -</option>\n";

		$DB->query(
			"SELECT `key`, `nom` ".
			"FROM `fonctions` ".
			"ORDER BY `nom` ASC"
		);

		while($DB->next_record()) {
			$selected = "";
			if ($key!="" && $key!="-1" && $key==$DB->getField("key")){
				$selected = " selected=\"selected\" ";
			}
			$html .= "<option ".$selected."value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . "</option>\n";
		}

		$html .= 
			"		</select>\n";

		return $html;
	}

	function getTitresList($key=""){
		global $DB;
		$html = 
			"	<select id=\"titres_key\" name=\"titres_key\" onchange=\"showAdvancedSearchNotaire();\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir un titre (facultatif) -</option>\n";
		
		$DB->query(
			"SELECT `key`, `nom` ".
			"FROM `titres` ".
			"ORDER BY `nom` ASC"
		);

		while($DB->next_record()) {
			$selected = "";
			if ($key!="" && $key!="-1" && $key==$DB->getField("key")){
				$selected = " selected=\"selected\" ";
			}
			$html .= "<option ".$selected."value=\"" . $DB->getField("key") . "\">" . $DB->getField("nom") . "</option>\n";
		}
		
		$html .= 
			"		</select>\n";

		return $html;
	}

	function getExpertisesList($expertise_key=""){
		global $expertisesArray;
		$html = 
			"	<select id=\"domaines_list\" name=\"domaines_list\" onchange=\"showAdvancedSearchNotaire();\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir un domaine d'expertise (facultatif) -</option>\n";

		foreach( $expertisesArray as $key => $value){
			$selected = "";
			if ($expertise_key==$key){
				$selected = " selected=\"selected\" ";
			}
			$html .= "<option ".$selected."value=\"" . $key. "\">" . $value. "</option>\n";
		}		
		
		$html .= 
			"		</select>\n";

		return $html;
	}


	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function searchString() { 
		$nom = "";
		$etudes_key = "";
		$fonctions_key = "";
		$titres_key = "";
		$motCle = "";
		$expertisesSectorielles = "";
		$divStyle = " style=\"display: none; position: relative;\" ";
		if (isset($_GET['search_employes']) && trim($_GET['search_employes'])!=""){
			$nom = trim($_GET['search_employes']);
		}
		if (isset($_GET['etudes_key']) && trim($_GET['etudes_key'])!="-1"){
			$etudes_key = trim($_GET['etudes_key']);
		}
		if (isset($_GET['fonctions_key']) && trim($_GET['fonctions_key'])!="-1"){
			$fonctions_key = trim($_GET['fonctions_key']);
		}
		if (isset($_GET['titres_key']) && trim($_GET['titres_key'])!="-1"){
			$titres_key = trim($_GET['titres_key']);
			if ($titres_key=='1'){
				$divStyle = "";
			}
			else {
				$divStyle = " style=\"display: none; position: relative;\" ";
			}
		}
		if (isset($_GET['key_word']) && trim($_GET['key_word'])!=""){
			$motCle = trim($_GET['key_word']);
		}
		if (isset($_GET['domaines_list']) && trim($_GET['domaines_list'])!="-1"){
			$expertisesSectorielles = trim($_GET['domaines_list']);
		}
		$html =
			"<div class=\"form\">\n".
			"	<form id=\"formations_recherche\" action=\"search.php?action=results\" method=\"get\">\n" .
			"		<input type=\"hidden\" id=\"action\" name=\"action\" value=\"results\" />".
			"		<table class=\"searchTable\">\n" .
			"			<tr>\n".
			"				<td>Nom :</td> " .
			"				<td><input type=\"text\" id=\"search_employes\" name=\"search_employes\" value=\"".$nom."\" tabindex=\"20\" /></td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Nom de l'&eacute;tude&nbsp;:</td> " .
			"				<td>".getEtudesList($etudes_key)."</td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Fonction&nbsp;:</td> " .
			"				<td>".getFonctionsList($fonctions_key)."</td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td>Titre&nbsp;:</td> " .
			"				<td>".getTitresList($titres_key)."</td>\n".
			"			</tr>\n".
			"			<tr>\n".
			"				<td colspan=\"2\">" .
			"					<div id=\"div_search_notaires\"".$divStyle."> " .
			"						<table>\n" .
			"							<tr>\n" .
			"								<th colspan=\"2\">Recherche dans la description du notaire ainsi que les descriptions de ses expertises&nbsp;:</th> " .
			"							</tr>\n" .
			"							<tr>\n" .
			"								<td>Mot(s)-cl&eacute;(s)&nbsp;:</td> " .
			"								<td><input type=\"text\" id=\"key_word\" name=\"key_word\" value=\"".$motCle."\" /></td></td>\n".
			"							</tr>\n" .
			"							<tr>\n".
			"								<td>Pr&eacute;cision de la recherche de notaire&nbsp;:</td> " .
			"								<td>".getExpertisesList($expertisesSectorielles)."</td>\n".
			"							</tr>\n".
			"						</table>\n" .
			"					</div>\n " .
			"				</td>\n" .
			"			</tr>\n".
			"		</table>\n".
			"		<br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"bouton\" tabindex=\"28\" />\n".
			"	</form>\n".
			"</div>".
			"<br /><br />\n";
		return $html;
	}

	function searchResultsString() {
		global $DB;
		$html = searchString();
		$html .= "<h2>R&eacute;sultats de recherche d'employ&eacute;s</h2>\n";
		$compteur = 0;
		$nb = 0;
		$sql = "";
		$queryNom = "";
		$queryMotCle = "";

		if (trim($_GET["search_employes"]) != "") {
			$terms = split(" ", trim($_GET["search_employes"]));
			$queryNom = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$queryNom .= "AND CONCAT_WS(' ', `employes`.`nom`, `employes`.`prenom` ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}
		if (trim($_GET['key_word'])!=''){
			$terms = split(" ", trim($_GET["key_word"]));
			$queryMotCle = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "" && strlen($terms[$i]) > 2) {
					$queryMotCle .= "AND CONCAT_WS(' ',  if( `employes`.`description` IS NULL , NULL , `employes`.`description` ),  if( `employes`.`droit_des_affaires_desc` IS NULL , NULL , `employes`.`droit_des_affaires_desc` ),  if( `employes`.`droit_des_personnes_desc` IS NULL , NULL , `employes`.`droit_des_personnes_desc` ),  if( `employes`.`droit_immobilier_desc` IS NULL , NULL , `employes`.`droit_immobilier_desc` ),  if( `employes`.`expertises_sectorielles_desc` IS NULL , NULL , `employes`.`expertises_sectorielles_desc` ) ) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}

		if (isset($_GET['etudes_key']) && trim($_GET['etudes_key'])!='-1'){
			$sql = "AND `etudes_succursales`.`etudes_key`='".trim($_GET['etudes_key'])."' ";
		}
		if (isset($_GET['fonctions_key'])&& trim($_GET['fonctions_key'])!="-1"){
			$sql .= "AND `employes_fonctions`.`fonctions_key`='".trim($_GET['fonctions_key'])."' ";
		}
		if (isset($_GET['titres_key'])&& trim($_GET['titres_key'])!="-1"){
			$sql .= "AND `employes_titres`.`titres_key`='".trim($_GET['titres_key'])."' ";
		}
		if (isset($_GET['domaines_list'])&& trim($_GET['domaines_list'])!="-1"){
			$sql .= "AND `".trim($_GET['domaines_list'])."` IS NOT NULL ";
		}
		
		$DB->query(
				"SELECT `employes`.`key`, `employes`.`nom`, `employes`.`prenom`, `etudes`.`nom` AS etude_nom ".
				"FROM `employes` " .
				"LEFT JOIN `employes_fonctions` ON `employes`.`key`=`employes_fonctions`.`employes_key` " .
				"LEFT JOIN `employes_titres` ON `employes`.`key`=`employes_titres`.`employes_key` " .
				"LEFT JOIN `employes_etudes_succursales` ON `employes`.`key`=`employes_etudes_succursales`.`employes_key` " .
				"LEFT JOIN `etudes_succursales` ON `employes_etudes_succursales`.`etudes_succursales_key`=`etudes_succursales`.`key`".
				"LEFT JOIN `etudes` ON `etudes_succursales`.`etudes_key`=`etudes`.`key`".
				"WHERE `employes`.`actif` = '1' ".
				(trim($_GET["search_employes"]) != "" ? $queryNom : "").
				(trim($_GET["key_word"]) != "" ? $queryMotCle : "").
				$sql.
				"GROUP BY `employes`.`key` ".
				"ORDER BY `employes`.`nom` ASC "
			);
		$nb = $DB->getNumRows();

		if ($nb>0){
			$html .=
				"<table class=\"resultsTable\">\n" .
				"	<tr>\n".
				"	<th>Nom, Pr&eacute;nom </th><th>Nom de l'&eacute;tude</th><th>&nbsp;</th>\n".
				"	</tr>\n";
		}
			
		while ($DB->next_record()) {
			$key = $DB->getField("key");
			$color = "";
			if ($compteur%2==0) {
				$color = " class=\"altTr\"";			
			}
			$html .=
			"    <tr". $color .">\n<td><a href=\"employes.php?action=read&begin=1&key=" . $DB->getField("key") . "\" title=\"Pour visualiser\">".$DB->getField("nom"). ", ".$DB->getField("prenom")."</a></td>\n" .
					"<td>".$DB->getField("etude_nom")."&nbsp;</td>\n" .
					"<td><a href=\"employes.php?action=read&begin=1&key=" . $DB->getField("key") . "\" title=\"Pour visualiser\">Visualiser</a> | <a href=\"employes.php?action=modify&begin=1&key=" . $DB->getField("key") . "\" title=\"Pour modifier\">Modifier</a></td>\n".
			"	</tr>\n";
			$compteur++;
		}
		if ($nb==0) {
			$html .= "<p>Aucun employ&eacute; ne correspond &agrave; votre requ&ecirc;te...</p>";
		}
		else if ($nb>0) {
			$html .=
				"</table>";			
		}
	
		return $html;
	}

	function validateJSString() { 
		$html =
			"<script type=\"text/javascript\" language=\"javascript\">\n" .
			"	String.prototype.trim = function(){ \n" .
			"		return this.replace(/^\s+|\s+$/g,'');\n" .
			"		}\n".
			
			"	function showAdvancedSearchNotaire() {\n".
//			"			alert('montrer');\n" .
			"		var champ = document.getElementById('titres_key').value;\n".
			"		var div_element = document.getElementById('div_search_notaires');\n".
			"		var expertisesList = document.getElementById('domaines_list');\n".
			"		var motCle = document.getElementById('key_word');\n".
			"		if (champ=='1'){\n".
			"			div_element.style.display = 'block';\n".
			"		} \n" .
			"		else { \n" .
			"			div_element.style.display = 'none';\n".
			"			expertisesList.selectedIndex = 0;\n".
			"			motCle.value = '';\n".
			"		} \n" .

			"	}\n".
			"</script>\n" .
			"\n";
		return $html;
	}
	
?>
