<?PHP
	require "./conf/conf.inc.php";
	
	global $DEV;

	$key = trim($_POST['loginkey']);
	$password = trim($_POST['loginpass']);

	if ($password != "" && $key != "") {

		$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
		$User = new User($DB, "", "");
		$User->scanfields();
		$User->setQKey("key", $key);
		$User->loadAll();
	
		if ($User->get("password") != "" && $User->get("password") == md5($password) && $User->isActif()) {
			$_SESSION = array();
			session_start();
			
			$_SESSION['user_key'] = $User->get("key");
			$_SESSION['user_type'] = $User->get("type");
			$_SESSION['user_fullname'] = $User->get("prenom") . " " . $User->get("nom");
			$_SESSION['user_lastlogon'] = $User->get("lastLogon");
			$_SESSION['user_etudes_key'] = $User->get('etudes_key');

			if ($DEV) {
				$_SESSION['user_dev'] = 1;
			}			
			
			$User->set("lastLogon", "CURRENT_TIMESTAMP", 1);
			$User->save("lastLogon");
		}
	}

	if ($_SERVER['HTTP_REFERER'] == "" || (substr($_SERVER['HTTP_REFERER'], -10, 10) == "logout.php" && $key != "")) {
		header("Location: " . $BASEURL . "index.php");
	} else {
		header("Location: " . $_SERVER['HTTP_REFERER']);
	}


?>