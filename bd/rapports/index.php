<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "rapports");
		$Skin->assign("errors", $html);
		$Skin->display("rapports.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2 || $_SESSION['user_type'] == 3)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "rapports");
		$Skin->assign("errors", $html);
		$Skin->display("rapports.tpl");
		exit;	
	}	

#	import("com.quiboweb.form.Form");

#	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "rapports");


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$html = "Section non disponible pour le moment.";

	$Skin->assign("page", "rapports");
	$Skin->assign("title", "Rapports");
	$Skin->assign("html", $html);
	$Skin->display("rapports.tpl");

#	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	
?>
