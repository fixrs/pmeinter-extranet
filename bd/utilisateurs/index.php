<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
		$Skin->assign("errors", $html);
		$Skin->display("utilisateurs.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
		$Skin->assign("errors", $html);
		$Skin->display("utilisateurs.tpl");
		exit;	
	}		
	
	import("com.quiboweb.form.Form");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$successString = "";
	$errorString = "";
	$formString = "";

	$action = getorpost('action');
	$key = getorpost('key');

	switch ($_GET['action']) {
		case "add":
			header("Location: utilisateurs.php?action=add&begin=1");
			exit;

		case "modify":
			$formString .= modifyUtilisateurString();
			break;

		case "delete":
			$formString .= deleteUtilisateurString();
			break;

		case "read":
			$formString .= readUtilisateurString();
			break;

		case "search":
			$formString .= validateJSString() . searchString();
			break;

		case "results":
			$formString .= validateJSString() . searchResultsString();
			break;
	}

	switch ($_POST['action']) {
		case "modify":
			if ($key == "-1") {
				$formString .= modifyUtilisateurString();
				$errorString .= _error("Veuillez choisir un utilisateur dans la liste");
			} else {
				header("Location: utilisateurs.php?action=modify&begin=1&key=" . $key . "");
				exit;
			}
			break;

		case "delete":
			if ($key == "-1") {
				$formString .= deleteUtilisateurString();
				$errorString .= _error("Veuillez choisir un utilisateur dans la liste");
			} else {
				if ($_POST['confirmation']) {
					$errorString .= deleteUtilisateurFromDB($key);
					if ($errorString == "") {
						$successString .= _success("Suppression compl&eacute;t&eacute;e avec succ&egrave;s");
					} else {
						$formString .= deleteUtilisateurString();
					}
				} else {
					$formString .= deleteUtilisateurString();
					$errorString .= _error("Veuillez confirmer la suppression");
				}
			}
			break;

		case "read":
			if ($key == "-1") {
				$formString .= readUtilisateurString();
				$errorString .= _error("Veuillez choisir un utilisateur dans la liste");
			} else {
				header("Location: utilisateurs.php?action=read&key=" . $key . "");
				exit;
			}
			break;

		case "search":
			// ******** À COMPLÉTER ********
			break;
	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
	$Skin->assign("page", "utilisateurs");
	$Skin->assign("title", pageTitleString($action));
	$Skin->assign("menu", menuActionString($action));
	$Skin->assign("menu_title", menuTitleString($action));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->display("utilisateurs.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function pageTitleString($action) { 
		switch ($action) {
			case "add":
				$title = "Ajout d'utilisateur";
				break;
			case "modify":
				$title = "Modification d'utilisateurs";
				break;
			case "delete":
				$title = "Suppression d'utilisateurs";
				break;
			case "read":
				$title = "Consultation d'utilisateurs";
				break;
			case "search":
				$title = "Recherche d'utilisateurs";
				break;
			case "results":
				$title = "R&eacute;sultats de recherche d'utilisateurs";
				break;
			default:
				$title = "Gestion des utilisateurs";
		}
		return $title;
	}

	function menuActionString($action) { 
		$html = "";
		if ($action != "results"){
			$html .=
				"<h1>Gestion des Utilisateurs</h1>\n".
				"<div class=\"menuIn\">\n".
				"	<ul>\n";

			if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
				$html .= "<li><a href=\"index.php?action=add\">Ajouter un utilisateur</a></li>\n";
			}
						
			if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
				$html .= "<li><a href=\"index.php?action=modify\">Modifier un utilisateur</a></li>\n";
			}

			if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
				$html .= "<li><a href=\"index.php?action=delete\">Supprimer un utilisateur</a></li>\n";
			}
			
			if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
				$html .= "<li><a href=\"index.php?action=read\">Consulter un utilisateur</a></li>\n";
			}
			
			if ($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2) {
				$html .= "<li><a href=\"index.php?action=search\">Rechercher des utilisateurs</a></li>\n";
			}
			
			$html .=
				"	</ul>\n".
				"</div>\n";
		}
		else {
			$html =
				"<h1>Recherche des Utilisateurs</h1>\n";
		}
		return $html;
	}

	function menuTitleString($action) { 
		$html = "";
		switch ($action) {
			case "modify":
				$html .= "<h2>Modifier un utilisateur</h2>\n";
				break;
			case "delete":
				$html .= "<h2>Supprimer un utilisateur</h2>\n";
				break;
			case "read":
				$html .= "<h2>Consulter un utilisateur</h2>\n";
				break;
			case "search":
				$html .= "<h2>Rechercher des utilisateurs</h2>\n";
				break;
			case "search":
				$html .= "<h2>R&eacute;sultats de recherche des utilisateurs</h2>\n";
				break;
		}
		return $html;
	}

	function getUtilisateursOptionsString() {
		global $DB;
		$html = "";
		if ($_SESSION['user_type'] == 1) {
			$DB->query(
				"SELECT `key`, `prenom`, `nom` ".
				"FROM `utilisateurs` ".
				"ORDER BY `key` ASC;"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$fullname = $DB->getField("prenom") . " " . $DB->getField("nom");
				$html .= "<option value=\"" . $key . "\">" . $key . " (" . $fullname . ")</option>\n";
			}
		} elseif ($_SESSION['user_type'] == 2 && $_SESSION['user_etudes_key'] != "") {
			$DB->query(
				"SELECT `key`, `prenom`, `nom` ".
				"FROM `utilisateurs` ".
				"WHERE (`type` = '2' OR `type` = '3') AND `etudes_key` = '" . $_SESSION['user_etudes_key'] . "' ".
				"ORDER BY `key` ASC;"
			);
			while ($DB->next_record()) {
				$key = $DB->getField("key");
				$fullname = $DB->getField("prenom") . " " . $DB->getField("nom");
				$html .= "<option value=\"" . $key . "\">" . $key . " (" . $fullname . ")</option>\n";
			}			
		}

		return $html;
	}

	function modifyUtilisateurString() { 
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"modify\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir un utilisateur -</option>\n";

		$html .=
			getUtilisateursOptionsString().
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function readUtilisateurString() { 
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"read\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir un utilisateur -</option>\n";

		$html .=
			getUtilisateursOptionsString().
			"		</select>\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Afficher\" />\n".
			"</form>\n";

		return $html;
	}

	function deleteUtilisateurString() {
		global $DB;
		$html =
			"<form action=\"index.php\" method=\"post\">\n".
			"	<input type=\"hidden\" name=\"action\" value=\"delete\" />\n".
			"	<div class=\"entry\">\n".
			"		<select id=\"key\" name=\"key\">\n".
			"			<option class=\"default\" value=\"-1\">- Choisir un utilisateur -</option>\n";

		$html .=
			getUtilisateursOptionsString().
			"		</select>\n".
			"		&nbsp;&nbsp;Confirmer&nbsp: <input type=\"checkbox\" class=\"checkbox\" id=\"confirmation\" name=\"confirmation\">\n".
			"	</div>\n".
			"	<input type=\"submit\" class=\"submit\" value=\"Supprimer\" />\n".
			"</form>\n";
		return $html;
	}

	function deleteUtilisateurFromDB($key) {
		global $DB;
		$errorString = "";
		$User = new User($DB, "", 0);
		$User->scanfields();
		$User->setQKey("key", $key);
		if (!$User->delete()) {
			$errorString .= _error("Une erreur s'est produite lors de la suppression des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
		}
		return $errorString;
	}

	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function searchString() { 
		$motCle = "";
		if (isset($_GET['search_utilisateurs']) && trim($_GET['search_utilisateurs'])!=""){
			$motCle = trim($_GET['search_utilisateurs']);
		}
		$html =
			"<div class=\"form\">\n".
			"	<form id=\"utilisateurs_recherche\" action=\"index.php\" method=\"get\">\n" .
			"		<input type=\"hidden\" id=\"action\" name=\"action\" value=\"results\" />".
			"		Mot(s)-cl&eacute;(s) : <input type=\"text\" id=\"search_utilisateurs\" name=\"search_utilisateurs\" value=\"" . $motCle . "\" />\n".
			"		<br /><br />\n".
			"		<input type=\"submit\" value=\"Afficher\" class=\"bouton\" />\n".
			"	</form>\n".
			"</div>".
			"<br /><br />\n";
		return $html;
	}

	function searchResultsString() {
		global $DB;
		$html = searchString();
		$nb = 0;
		$query = "";
		
		if (trim($_GET["search_utilisateurs"]) != "") {
			$terms = split(" ", trim($_GET["search_utilisateurs"]));
			$query = "";
			for ($i = 0; $i < count($terms); $i++) {
				if (trim($terms[$i]) != "") {
					$query .= "AND CONCAT_WS(' ', `nom`, `prenom`, if( `courriel` IS NULL , NULL , `courriel` )) LIKE '%" . $terms[$i] . "%' ";
				}
			}
		}
		$compteur = 0;

		$DB->query(
				"SELECT * ".
				"FROM `utilisateurs` ".
				"WHERE `actif` = '1' ".
				(trim($_GET["search_utilisateurs"]) != "" ? $query : "").
				"ORDER BY `nom` ASC "
			);
		$nb = $DB->getNumRows();

		$html .=
			"<h2>R&eacute;sultats de la recherche</h2>\n";

		if ($nb>0){
			$html .=
				"<table class=\"resultsTable\">\n" .
				"	<tr>\n".
				"	<th>Nom de l'utilisateur</th><th>&nbsp;</th>\n".
				"	</tr>\n";
		}

		while ($DB->next_record()) {
			$key = $DB->getField("key");
			$color = "";
			if ($compteur%2==0) {
				$color = " class=\"altTr\"";			
			}
			$html .=
				"    <tr". $color .">\n<td><a href=\"utilisateurs.php?action=read&key=" . $DB->getField("key") . "\" title=\"Pour visualiser\">".$DB->getField("prenom"). " ". $DB->getField("nom")."</a></td> <td><a href=\"utilisateurs.php?action=read&key=" . $DB->getField("key") . "\" title=\"Pour visualiser\">Visualiser</a> | <a href=\"utilisateurs.php?action=modify&begin=1&key=" . $DB->getField("key") . "\" title=\"Pour modifier\">Modifier</a></td>\n".
				"	</tr>\n";
			$compteur++;
			
		}
		if ($nb==0) {
			$html .= "<p>Aucun utilisateur ne correspond &agrave; votre requ&ecirc;te...</p>";
		}
		else if ($nb>0) {
			$html .=
				"</table>";			
		}
	
		return $html;
	}
	
	// Fonction qui retourne le html pour le menu de la recherche d'un produit
	function validateJSString() { 
		$html =
			"<script type=\"text/javascript\" language=\"javascript\">\n" .
			"	String.prototype.trim = function(){ \n" .
			"		return this.replace(/^\s+|\s+$/g,'');\n" .
			"		}\n".
			
			"	function validateForm(nomChamp) {\n".
			"		var champ = document.getElementById(nomChamp);\n".
			"		var champValue = champ.value;\n".
			"		var blnOk = true;\n".
			"		if (champValue.trim()!=''){\n".
			"			blnOk = true;\n".
			"		} \n" .
			"		else { \n" .
			"			alert('Vous devez entrer un mot clé');\n" .
			"			champ.focus();\n" .
			"		} \n" .
			"		return blnOk;\n" .

			"	}\n".
			"</script>\n" .
			"\n";
		return $html;
	}
?>
