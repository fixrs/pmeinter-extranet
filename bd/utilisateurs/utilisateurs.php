<?PHP
	require "../conf/conf.inc.php";

	session_start();

	// --------
	// SECURITÉ
	// --------
	if (!isAuthenticated()) {
		$html = _error("Vous devez d'abord vous connecter pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
		$Skin->assign("errors", $html);
		$Skin->display("utilisateurs.tpl");
		exit;	
	}

	if (!($_SESSION['user_type'] == 1 || $_SESSION['user_type'] == 2)) {
		$html = _error("Vous ne disposez pas des droits requis pour acc&eacute;der &agrave; cette section.");
		$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
		$Skin->assign("errors", $html);
		$Skin->display("utilisateurs.tpl");
		exit;	
	}
	
	import("com.quiboweb.form.Form");

	$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);


	// ------------------
	// LE TRAITEMENT HTML
	// ------------------

	$formString = "";
	$successString = "";
	$errorString = "";

	$begin = getorpost('begin');
	$action = getorpost('action');
	$key = getorpost('key');

	switch ($action) {
		case "add" :
			if ($begin) {
				importForm("utilisateurs");
				$FormObject = new Form("", "", getForm("add"));
				$formString = $FormObject->returnForm();

			} else {
				$errorString = validateFormValues("add", getParametersFromPOST());
				if ($errorString == "") {
					$errorString = updateDBValues("add", getParametersFromPOST());
					if ($errorString == "") {
						$successString = _success("Ajout compl&eacute;t&eacute; avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "utilisateurs/index.php\">Retourner au menu pr&eacute;c&eacute;dent.</a>";
						break;
					} else {
						importForm("utilisateurs");
						$FormObject = new Form("", getParametersFromPOST(), getForm("add"));
						$formString = $FormObject->returnFilledForm();
					}

				} else {
					importForm("utilisateurs");
					$FormObject = new Form("", getParametersFromPOST(), getForm("add"));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "modify" :
			if ($begin) {
				importForm("utilisateurs");
				$FormObject = new Form("", getParametersFromDB("modify", $key), getForm("modify"));
				$formString = $FormObject->returnFilledForm();

			} else {
				$errorString = validateFormValues("modify", getParametersFromPOST());
				if ($errorString == "") {
					$errorString = updateDBValues("modify", getParametersFromPOST());
					if ($errorString == "") {
						$successString = _success("Modification compl&eacute;t&eacute;e avec succ&egrave;s");
						$formString = "<a href=\"" . $BASEURL . "utilisateurs/index.php\">Retourner au menu pr&eacute;c&eacute;dent</a>";
						break;
					} else {
						importForm("utilisateurs");
						$FormObject = new Form("", getParametersFromPOST(), getForm("modify"));
						$formString = $FormObject->returnFilledForm();
					}

				} else {
					importForm("utilisateurs");
					$FormObject = new Form("", getParametersFromPOST(), getForm("modify"));
					$formString = $FormObject->returnFilledForm();
				}
			}
			break;

		case "read" :
			importForm("utilisateurs");
			$FormObject = new Form("", getParametersFromDB("read", $key), getForm("read"));
			$FormObject->setAllHidden();
			$formString = $FormObject->returnFilledForm();
			break;

	}


	$Skin = new Skin($SKIN_PATH, $SKIN_URL, "administration");
	$Skin->assign("page", "utilisateurs");
	$Skin->assign("title", pageTitleString($action, $key));
	$Skin->assign("headcontent", headString($action));
	$Skin->assign("bodyparams", bodyString($action));
	$Skin->assign("form_title", formTitleString($action, $key));
	$Skin->assign("success", $successString);
	$Skin->assign("errors", $errorString);
	$Skin->assign("form", $formString);
	$Skin->assign("index", $indexString);
	$Skin->display("utilisateurs.tpl");

	$DB->close();


	// -------------
	// LES FONCTIONS
	// -------------

	function pageTitleString($action, $key) { 
		switch ($action) {
			case "add":
				$title = "Ajout d'un utilisateur";
				break;
			case "modify":
				$title = "Modification de l'utilisateur " . $key;
				break;
			case "read":
				$title = "Consultation de l'utilisateur " . $key;
				break;
			default:
				$title = "Gestion des utilisateurs";
		}
		return $title;
	}

	function headString($action) {
		global $JS_URL;
		$html = "";
		switch ($action) {
			case "add":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/utilisateurs.js\" type=\"text/javascript\"></script>";
				break;

			case "modify":
				$html .=
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/feedback.js\" type=\"text/javascript\"></script>".
					"<script language=\"JavaScript\" src=\"" . $JS_URL . "/utilisateurs.js\" type=\"text/javascript\"></script>";
				break;

			case "read":
				break;
		}
		return $html;
	}

	function bodyString($action) {
		$html = "";
		switch ($action) {
			case "add":
				if ($_SESSION['user_type'] == 2) {
					$html .= "onload=\"javascript: showEtudesField(); limitTypes('2');\"";
				} else {
					$html .= "onload=\"javascript: showEtudesField();\"";
				}
				break;

			case "modify":
				if ($_SESSION['user_type'] == 2) {
					$html .= "onload=\"javascript: showEtudesField(); limitTypes('2');\"";
				} else {
					$html .= "onload=\"javascript: showEtudesField();\"";
				}
				break;

			case "read":
				break;
		}
		return $html;
	}

	function formTitleString($action, $key) { 
		switch ($action) {
			case "add":
				$title = "<h1>Ajout d'un utilisateur</h1>";
				break;
			case "modify":
				$title = "<h1>Modification d'un utilisateur</h1>";
				break;
			case "read":
				$title = "<h1>Consultation d'un utilisateur</h1>";
				break;
		}
		return $title;
	}	

	function validateFormValues($action, $parameters) {
		global $DB, $EMAIL_FORMAT_PATTERN;
		$errorString = "";
		switch ($action) {
			case "add":
				if ($_SESSION['user_type'] == 2) {
					if ($parameters['type'] != 3) {
						$errorString .= _error("<a href=\"javascript: showErrorField('type');\">Vous ne poss&eacute;ss&eacute;dez pas les droits requis pour cr&eacute;er ce type de compte.</a>");
					}
				}
				if (strlen($parameters['key']) < 3 || strlen($parameters['key']) > 11) {
					$errorString .= _error("<a href=\"javascript: showErrorField('key');\">Le nom d'usager doit contenir un minimum de 3 charact&egrave;res et un maximum de 11.</a>");
				} else {
					$User = new User($DB, "", "");
					$User->setQKey("key", $parameters['key']);
					if ($User->entryExists()) {
						$errorString .= _error("<a href=\"javascript: showErrorField('key');\">Ce nom d'usager existe d&eacute;j&agrave;, veuillez en choisir un autre.</a>");
					}
				}
				if ($parameters['courriel'] != "" && !preg_match($EMAIL_FORMAT_PATTERN, $parameters['courriel'])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('courriel');\">L'adresse courriel fournie n'est pas valide.</a>");
				}
				if ($parameters['actif'] != "0" && $parameters['actif'] != "1") {
					$errorString .= _error("<a href=\"javascript: showErrorField('actif');\">Veuillez indiquer si le compte est actif ou non.</a>");
				}
				if (strlen($parameters['password']) < 6 || strlen($parameters['password']) > 255) {
					$errorString .= _error("<a href=\"javascript: showErrorField('password');\">Le mot de passe doit contenir un minimum de 6 charact&egrave;res et un maximum de 255.</a>");
				} else {
					if ($parameters['password'] != $parameters['password_confirmation']) {
						$errorString .= _error("<a href=\"javascript: showErrorField('password_confirmation');\">La confirmation du mot de passe est incorrecte.</a>");
					}
				}
				if (($parameters['type'] == 2 || $parameters['type'] == 3) && $parameters['etudes_key'] < 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('etudess_key');\">Veuillez indiquez l'étude du représentant.</a>");
				}
				break;

			case "modify":
				if ($_SESSION['user_type'] == 2) {
					if ($parameters['type'] != 3) {
						$errorString .= _error("<a href=\"javascript: showErrorField('type');\">Vous ne poss&eacute;ss&eacute;dez pas les droits requis pour cr&eacute;er ce type de compte.</a>");
					}
				}
				if (strlen($parameters['newkey']) < 3 || strlen($parameters['newkey']) > 11) {
					$errorString .= _error("<a href=\"javascript: showErrorField('newkey');\">Le nom d'usager doit contenir un minimum de 3 charact&egrave;res et un maximum de 11.</a>");
				} else {
					if ($parameters['newkey'] != $parameters['key']) {
						$User = new User($DB, "", "");
						$User->setQKey("key", $parameters['newkey']);
						if ($User->entryExists()) {
							$errorString .= _error("<a href=\"javascript: showErrorField('newkey');\">Ce nom d'usager existe d&eacute;j&agrave;, veuillez en choisir un autre.</a>");
						}
					}
				}
				if ($parameters['courriel'] != "" && !preg_match($EMAIL_FORMAT_PATTERN, $parameters['courriel'])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('courriel');\">L'adresse courriel fournie n'est pas valide.</a>");
				}
				if ($parameters['actif'] != "0" && $parameters['actif'] != "1") {
					$errorString .= _error("<a href=\"javascript: showErrorField('actif');\">Veuillez indiquer si le compte est actif ou non.</a>");
				}
				if ($parameters['password'] != "") {
					if (strlen($parameters['password']) < 6 || strlen($parameters['password']) > 255) {
						$errorString .= _error("<a href=\"javascript: showErrorField('password');\">Le mot de passe doit contenir un minimum de 6 charact&egrave;res et un maximum de 255.</a>");
					} else {
						if ($parameters['password'] != $parameters['password_confirmation']) {
							$errorString .= _error("<a href=\"javascript: showErrorField('password_confirmation');\">La confirmation du mot de passe est incorrecte.</a>");
						}
					}
				}
				if (($parameters['type'] == 2 || $parameters['type'] == 3) && $parameters['etudes_key'] < 1) {
					$errorString .= _error("<a href=\"javascript: showErrorField('etudes_key');\">Veuillez indiquez l'étude du représentant.</a>");
				}
				break;
		}
		return $errorString;
	}

	function getParametersFromPOST() {
		$parameters = array();
		if (is_array($_POST) && count($_POST) > 0) {
			foreach ($_POST as $field => $value) {
				if (is_array($value)) {
					$parameters[$field] = $value;
				} else {
					$parameters[$field] = trim($value);
				}
			}
		}
		return $parameters;
	}

	function getParametersFromDB($action, $key) {
		global $DB, $BASEURL;
		$parameters = array();
		switch ($action) {
			case "modify":
				$User = new User($DB, "", "");
				$User->setQKey("key", $key);
				$User->scanfields();
				$User->loadAll();
				$parameters = $User->getAll();
				$parameters['newkey'] = $parameters['key'];
				$parameters['password'] = "";
				$parameters['password_confirmation'] = "";
				
/*				if ($parameters['type'] == 2 || $parameters['type'] == 3) {
					$DB->query(
						"SELECT e.key AS etudes_key ".
						"FROM `etudes` AS e, `utilisateurs_etudes` AS ue ".
						"WHERE ue.utilisateurs_key = '" . $parameters['key'] . "' AND e.key = ue.etudes_key;"
					);
					$i = 0;
					while ($DB->next_record()) {
						$parameters['etudes_key'][$i] = $DB->getField("etudes_key");
						$i++;
					}
				}*/
				break;

			case "read":
				$User = new User($DB, "", "");
				$User->setQKey("key", $key);
				$User->scanfields();
				$User->loadAll();
				$parameters = $User->getAll();

				$parameters['password'] = "";
				$parameters['password_confirmation'] = "";

				if ($parameters['actif'] == "1") {
					$parameters['actif'] = "Oui";
				} else {
					$parameters['actif'] = "Non";
				}

/*				if ($parameters['type'] == 2 || $parameters['type'] == 3) {
					$parameters['etudes_nom'] = "<ul class=\"answer\">\n";
					$DB->query(
						"SELECT e.key AS etudes_key, e.nom AS etudes_nom ".
						"FROM `etudes` AS e, `utilisateurs_etudes` AS ue ".
						"WHERE ue.utilisateurs_key = '" . $parameters['key'] . "' AND e.key = ue.etudes_key;"
					);
					while ($DB->next_record()) {
						$etudes_nom = $DB->getField("etudes_nom");
						$parameters['etudes_nom'] .= "<li><a href=\"" . $BASEURL . "etudes/etudes.php?action=read&begin=1&key=" . $DB->getField("etudes_key") . "\" title=\"Consulter\">" . $etudes_nom . "</a></li>\n";
					}
					$parameters['etudes_nom'] .= "</ul>\n";
					if ($etudes_nom == "") {
						$parameters['etudes_nom'] = "<span class=\"answer\">(aucune)</span>";
					}
				}*/
				
				$DB->query(
					"SELECT e.key AS etudes_key, e.nom AS etudes_nom ".
					"FROM `etudes` AS e, `utilisateurs` AS u ".
					"WHERE u.key = '" . $parameters['key'] . "' AND e.key = u.etudes_key;"
				);
				while ($DB->next_record()) {
					$etudes_nom = $DB->getField("etudes_nom");
					$etudes_key = $DB->getField("etudes_key"); 
					$parameters['etudes_nom'] .= "<a href=\"" . $BASEURL . "etudes/etudes.php?action=read&begin=1&key=" . $etudes_key . "\" title=\"Consulter\">" . $etudes_nom . "</a>\n";
				}
				if ($etudes_nom == "") {
					$parameters['etudes_nom'] = "<span class=\"answer\">(aucune)</span>";
				}


				$DB->query(
					"SELECT e.key, e.nom, e.prenom, e.courriel ".
					"FROM `employes` e ".
					"WHERE e.key = '" . $parameters['key'] . "' ".
					"ORDER BY e.nom, e.prenom ASC;"
				);
				while ($DB->next_record()) {
					$employe_nom = $DB->getField("nom") . " " . $DB->getField("prenom");
					$employes_key = $DB->getField("key"); 
					$parameters['employe_nom'] .= "<a href=\"" . $BASEURL . "employes/employes.php?action=read&begin=1&key=" . $employes_key . "\" title=\"Consulter\">" . $employe_nom . "</a>\n";
				}
				if ($employe_nom == "") {
					$parameters['employe_nom'] = "<span class=\"answer\">(aucun)</span>";
				}


				
				switch ($parameters['type']) {
					case '1':
						$parameters['type'] = "Super administrateur (Contr&ocirc;le total)";
						break;
					case '2':
						$parameters['type'] = "Repr&eacute;sentant d'&eacute;tude (&Eacute;criture + Lecture associ&eacute;e &agrave; une &eacute;tude)";
						break;
					case '3':
						$parameters['type'] = "Repr&eacute;sentant d'&eacute;tude (Lecture associ&eacute;e &agrave; une &eacute;tude)";
						break;
					case '4':
						$parameters['type'] = "Repr&eacute;sentant d'&eacute;tude (Responsable des ratios de gestion)";
						break;
				}				
				break;
		}

		return $parameters;
	}
	
	function updateDBValues($action, $parameters) {
		global $DB;
		$errorString = "";

		$parameters = addslashesToValues($parameters);

		switch ($action) {
			case "add":
				$User = new User($DB, "", "");
				$User->scanfields();
				$User->insertmode();
								
				if ($User->entryExists("key", $parameters['key'])) {
					$errorString .= _error("<a href=\"javascript: showErrorField('key');\">Le nom d'usager existe d&eacute;j&agrave;.</a>");
					break;
				}

				$User->set("nom", $parameters['nom']);
				$User->set("prenom", $parameters['prenom']);
				$User->set("key", $parameters['key']);
				$User->set("password", md5($parameters['password']));
				$User->set("courriel", $parameters['courriel']);
				$User->set("actif", $parameters['actif']);
				$User->set("type", $parameters['type']);
				$User->set("etudes_key", $parameters['etudes_key']);
				$User->set("employes_key", $parameters['employes_key']);
				$User->set("createdOn", "CURRENT_TIMESTAMP", 1);
					
				if (!$User->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
					break;
				}
				
/*				$DB->query( 
					"DELETE FROM `utilisateurs_etudes` ".
					"WHERE `utilisateurs_key` = '" . $parameters['key'] . "';"
				);

				if ($parameters['type'] == 2 || $parameters['type'] == 3) {
					if (is_array($parameters['etudes_key']) && count($parameters['etudes_key']) > 0) {
						foreach ($parameters['etudes_key'] as $i => $etudes_key) {
							$DB->query(
								"INSERT INTO `utilisateurs_etudes` ".
								"(`utilisateurs_key`, `etudes_key`) ".
								"VALUES ('" . $parameters['key'] . "', '" . $etudes_key . "');"
							);
						}
					}
				}*/
				
				break;
			
			case "modify":
				$User = new User($DB, "", "");
				$User->scanfields();
				$User->setQKey("key", $parameters['key']);

				if ($parameters['key'] == $parameters['newkey']) {
					$User->excludefield("key");
				}
				
				if ($parameters['password'] == "") {
					$User->excludefield("password");
				}
								
				if ($User->entryExists() && !$User->isFieldExcluded("key")) {
					$errorString .= _error("<a href=\"javascript: showErrorField('key');\">Le nom d'usager existe d&eacute;j&agrave;.</a>");
					break;
				}

				$User->set("nom", $parameters['nom']);
				$User->set("prenom", $parameters['prenom']);
				$User->set("key", $parameters['newkey']);
				$User->set("password", md5($parameters['password']));
				$User->set("courriel", $parameters['courriel']);
				$User->set("actif", $parameters['actif']);
				$User->set("type", $parameters['type']);
				$User->set("etudes_key", $parameters['etudes_key']);
				$User->set("employes_key", $parameters['employes_key']);
					
				if (!$User->saveAll()) {
					$errorString .= _error("Une erreur s'est produite lors de l'insertion des valeurs dans la base de donn&eacute;es.<br />Si le probl&egrave;me persiste, veuillez contacter votre administrateur.");
					break;
				}

/*				$DB->query( 
					"DELETE FROM `utilisateurs_etudes` ".
					"WHERE `utilisateurs_key` = '" . $parameters['key'] . "';"
				);
				
				if ($parameters['type'] == 2 || $parameters['type'] == 3) {
					if (is_array($parameters['etudes_key']) && count($parameters['etudes_key']) > 0) {
						foreach ($parameters['etudes_key'] as $i => $etudes_key) {
							$DB->query(
								"INSERT INTO `utilisateurs_etudes` ".
								"(`utilisateurs_key`, `etudes_key`) ".
								"VALUES ('" . $parameters['key'] . "', '" . $etudes_key . "');"
							);
						}
					}
				}*/
				break;				
		}
		return $errorString;
	}

	function addslashesToValues($parameters = "") {
		if (is_array($parameters) && count($parameters) > 0) {
			foreach ($parameters as $field => $value) {
				if (is_array($value) && count($value) > 0) {
					foreach ($value as $field2 => $value2) {
						if (is_array($value2) && count($value2) > 0) {
							foreach ($value2 as $field3 => $value3) {
								$parameters[$field][$field2][$field3] = addslashes($value3);
							}
						} else {
							$parameters[$field][$field2] = addslashes($value2);
						}
					}
				} else {
					$parameters[$field] = addslashes($value);
				}
			}
		} else {
			$parameters = addslashes($parameters);
		}
		return $parameters;
	}

?>
