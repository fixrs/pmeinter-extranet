<?php
    error_reporting(E_ALL);
    ini_set("display_errors", 1);

    if (!function_exists("sanitize")) {
        function sanitize( $string ) {
            $string = normalize( $string );
            $string = preg_replace( '#[^a-z0-9\(\)\-]+#i', '_', $string );
            return mb_strtolower( $string, 'utf-8' );
        }
    }

    if (!function_exists("normalize")) {
        function normalize( $string ) {
            static $table = array(
                'Š'=>'S', 'š'=>'s', 'Đ'=>'Dj', 'đ'=>'dj', 'Ž'=>'Z', 'ž'=>'z', 'Č'=>'C', 'č'=>'c', 'Ć'=>'C', 'ć'=>'c',
                'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
                'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O',
                'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss',
                'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e',
                'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o',
                'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b',
                'ÿ'=>'y', 'Ŕ'=>'R', 'ŕ'=>'r',
            );
            return strtr($string, $table);
        }
    }

    if (isset($_FILES["file"])) {
        if (is_array($_FILES["file"])) {
            foreach ($_FILES["file"]["name"] as $j => $file) {

                $errValid = array();

                if ( preg_match("/(\.doc|.docx|\.pdf|\.txt|\.ppt|\.pptx|\.xls|\.xlsx|\.jpg|\.jpeg|\.png|\.gif)$/is", $_FILES["file"]["name"][$j]) ) {

                    if ($_FILES["file"]["error"][$j] > 0) {
                        $errValid['docs_filename'] = "Erreur lors du transfère du fichier";
                    } else {

                        $pathFileTmp = '';  //fichier temporaire lors de la création d'une fiche
                        $fileExt = '';
                        $fileName = '';
                        $fileNameUploaded = '';

                        $fileNameUploaded = $_FILES["file"]["tmp_name"][$j];
                        $fileExt = pathinfo(strtolower($_FILES["file"]["name"][$j]), PATHINFO_EXTENSION);
                        $fileName = sanitize(pathinfo(strtolower($_FILES["file"]["name"][$j]), PATHINFO_FILENAME));

                        $basedir = dirname(__DIR__) . '/d-docs/';
                        //on bâtit le nom du nouveau fichier temp
                        if (!file_exists($basedir)) {
                            mkdir($basedir);
                        }

                        $file_name = uniqid() . $fileName . '.' . $fileExt;
                        $pathFileTmp = $basedir . $file_name;

                        if (file_exists($fileNameUploaded)) {
                            move_uploaded_file($fileNameUploaded, $pathFileTmp);

                            if ($j != 0) { echo ","; }
                            echo $file_name;
                        }

                    }
                } else {
                    $errValid['fileDocUserEnf'] = "Le fichier ne peut pas être téléversé sur le serveur.";
                }
            }
        }
    } else if (isset($_GET["f"])) {

        $file_extension = preg_replace("/.*?\.([a-z]+)$/is", "$1", $_GET["f"]);
        $atype = "";
        switch ($file_extension) {
            case "pdf":
                $ctype="application/pdf";
                $atype = "attachment; filename=\"" . $_GET["f"] . "\"";
                break;
            case "doc":
            case "docx":
            case "xls":
            case "xlsx":
            case "ppt":
            case "pptx":
                $ctype="application/octet-stream";
                $atype = "attachment; filename=\"" . $_GET["f"] . "\"";
                break;

            case "gif": $ctype="image/gif"; break;
            case "png": $ctype="image/png"; break;
            case "jpe":
            case "jpeg":
            case "jpg": $ctype="image/jpg"; break;

            default: $ctype="application/octet-stream";
        }
        $basedir = dirname(__DIR__) . '/d-docs/';
        $pathFileTmp = $basedir;

        $filename = $_GET["f"];

        if (!file_exists($pathFileTmp . $filename)) {
            //die("NO FILE HERE");
        }

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header("Content-Type: $ctype");

        if ($atype) { header("Content-Disposition: " . $atype . ";"); header("Content-Transfer-Encoding: binary"); }

        header("Content-Length: ".@filesize($pathFileTmp . $filename));
        set_time_limit(0);
        @readfile($pathFileTmp . $filename) or die("File not found.");

        exit;

    }

