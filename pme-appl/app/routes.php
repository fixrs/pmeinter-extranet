<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

/* test import orignal DB */
Route::any("form",'Homecontroller@showForm');

//Route::any("importemployee",'importController@importempolyee');
//Route::any("importevent",'importController@importevent');
Route::any("geteventbyidcats",'eventController@geteventbyidcats');
Route::any("getlistmemberby/{idkey}",'eventController@getlistmemberby');

Route::any("syncsystem",'importController@syscall');