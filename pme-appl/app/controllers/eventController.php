<?php

class EventController extends BaseController
{

    /*
    |--------------------------------------------------------------------------
    | Default Home Controller
    |--------------------------------------------------------------------------
    |
    | You may wish to use controllers instead of, or in addition to, Closure
    | based routes. That's great! Here is an example controller method to
    | get you started. To route to this controller, just add the route:
    |
    |	Route::get('/', 'HomeController@showWelcome');
    |
    */


    public function geteventbyidcats()
    {
        $start=Input::get('start');
        $end=Input::get('end');
        $idcats=Input::get("idcats");

       
        if ($idcats == "") {
            $direction = DB::table("directions_de_travail_event as dte")
                ->join("directions_de_travail as dt", "dt.key", "=", "dte.directions_de_travail_key")
                ->whereBetween('dte.date', array(date("Y-m-d", strtotime($start)), date("Y-m-d", strtotime($end))))
                ->select("date as start", "date as end", "description as description", "lieu", "directions_de_travail_key", "nom")
                ->get();

        }
        else{

            $arr_idcats=explode(",",$idcats);
            $direction = DB::table("directions_de_travail_event as dte")
                ->join("directions_de_travail as dt", "dt.key", "=", "dte.directions_de_travail_key")
                ->whereBetween('dte.date', array(date("Y-m-d", strtotime($start)), date("Y-m-d", strtotime($end))))
                ->whereIn("dte.directions_de_travail_key",$arr_idcats)
                ->select("date as start", "date as end", "description as description", "lieu", "directions_de_travail_key", "nom")
                ->get();


        }
        $direction_arr_tempo = array();
        foreach ($direction as $ra) {

            $direction_arr_tempo[] = array("start" => $ra->start, "end" => $ra->end, "title" => strtoupper("DIRECTION DE TRAVAIL-" . $ra->nom), "lieu" => $ra->lieu);
        }
        $direction_arr["direction"]=$direction_arr_tempo;
        $general=$this->geteventgeneral($start,$end);
        $return_arr=array_merge ($direction_arr,$general);
        echo json_encode($return_arr);


    }

    public function geteventgeneral($start, $end)
    {

        $return_arr = array();

// assemeblee annuelle
        $query_assemblee_annuelle = DB::table("convocation_aga_event")
            ->whereBetween("date", array(date("Y-m-d", strtotime($start)), date("Y-m-d", strtotime($end))))
            ->select("date as start", "date as end", "description as description", "lieu")->get();

        $return_arr_assemeblee_annuelle = array();
        foreach ($query_assemblee_annuelle as $ra) {

            $return_arr_assemeblee_annuelle[] = array("start" => $ra->start, "end" => $ra->end, "title" => "AVIS CONVOCATION ASSSEMBLÉE GÉNÉRALE ANNUELLE", "lieu" => $ra->lieu);
        }

        $return_arr["assemblee_annuelle"] = $return_arr_assemeblee_annuelle;


// assemeblee
        $return_arr_assemeblee=array();
        $query_assemblee = DB::table("convocation_aga_event")
            ->whereBetween("date", array(date("Y-m-d", strtotime($start)), date("Y-m-d", strtotime($end))))
            ->select("date as start", "date as end", "description as description", "lieu")->get();
        $return_arr_assemeblee = array();
        foreach ($query_assemblee as $ra) {

            $return_arr_assemeblee[] = array("start" => $ra->start, "end" => $ra->end, "title" => "ASSSEMBLÉE GÉNÉRALE", "lieu" => $ra->lieu);
        }

        $return_arr["assemblee"] = $return_arr_assemeblee;


// congres
        $return_arr_congres=array();
        $query_congres = DB::table("congres_event")
            ->whereBetween("date", array(date("Y-m-d", strtotime($start)), date("Y-m-d", strtotime($end))))
            ->select("date as start", "date as end", "description as description", "lieu")->get();
        $return_arr_assemeblee_annuelle = array();
        foreach ($query_assemblee as $ra) {

            $return_arr_congres[] = array("start" => $ra->start, "end" => $ra->end, "title" => "ASSSEMBLÉE GÉNÉRALE", "lieu" => $ra->lieu);
        }

        $return_arr["congres"] = $return_arr_congres;

//JOURNEE DE FORMATION

        $query_formation = DB::table("journee_formation_event")
            ->whereBetween("date", array(date("Y-m-d", strtotime($start)), date("Y-m-d", strtotime($end))))
            ->select("date as start", "date as end", "sujet", "note", "cout", "description as description", "lieu")->get();

        $return_arr_formation = array();
        foreach ($query_formation as $ra) {

            $return_arr_formation[] = array("start" => $ra->start, "end" => $ra->end, "title" => "ASSSEMBLÉE GÉNÉRALE", "lieu" => $ra->lieu);
        }


        $return_arr["formation"] = $return_arr_formation;


        return $return_arr;


    }


public  function getlistmemberby($idkey)
{
    $query=DB::table("employes as e")
        ->join("employes_directions_de_travail as ed","ed.employes_key","=","e.key")
        ->join("directions_de_travail as dt","dt.key","=","ed.directions_de_travail_key")
        ->join("employes_etudes_succursales as ees","ees.employes_key","=","e.key")
        ->join("etudes_succursales as es ","es.key","=","ees.etudes_succursales_key")
        ->join("etudes as etu","etu.key","=","es.etudes_key")
        ->where("dt.key","=",$idkey)
        ->select("e.key","e.prenom","e.nom","e.courriel","etu.nom as etude","es.adresse","es.telephone1 as tel")
        ->get();
    $reslt_arr=array();
    foreach($query as $q)
    {
        $rows=array();
        foreach ($q as $k=>$v )
        {
            $rows[$k]=$v;
        }
        $reslt_arr[]=$rows;

    }

    echo json_encode($reslt_arr);
}

}