<?php

class ImportController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function getemployee_orignal($colone,$table)
	{
       $array_employes_orignal=array();
        $db_orginal=DB::connection('orginal');
        $employes_orginal=$db_orginal->select("select * from $table");
       // $employes=DB::table("employes")->get();
       // print_r($employes_orginal);
        //exit();
        $colon_get = array();

        if($colone=="all")
        {

            foreach ($employes_orginal as $em)
            {
                foreach($em as $k=>$v)
                {
                    $colon_get[$k] = $v;
                }


                $array_employes_orignal[] = $colon_get;
            }

        }else {
            foreach ($employes_orginal  as $em)
            {



                foreach ($colone as $c)
                {
                    $colon_get[$c] = $em->$c;

                }


                $array_employes_orignal[] = $colon_get;
            }
        }

        DB::disconnect('orginal');

        return $array_employes_orignal;
    }
    public  function importempolyee()
    {
        /*table employee*/
        $colone_employee=array("key","prenom","nom","courriel","associe", "succursale","actif");
        $arr_get=$this->getemployee_orignal($colone_employee,"employes");
        DB::table('employes')->truncate();
        DB::table('employes')->insert($arr_get);


        /*table directions_de_travail */
       $arr_get_leeroy_directions_de_travail=$this->getemployee_orignal("all","directions_de_travail");
       DB::table('directions_de_travail')->truncate();
       DB::table('directions_de_travail')->insert($arr_get_leeroy_directions_de_travail);


        /*table employes_directions_de_travail */
        $employes_directions_de_travail=$this->getemployee_orignal("all","employes_directions_de_travail");
        DB::table('employes_directions_de_travail')->truncate();
        DB::table('employes_directions_de_travail')->insert($employes_directions_de_travail);


        /*table etudes */
        $data_etude=array("key","nom","actif");
        $etudes=$this->getemployee_orignal($data_etude,"etudes");
        DB::table('etudes')->truncate();
        DB::table('etudes')->insert($etudes);

        /*table employes_etudes_succursales */
        $data_employes_etudes_succursales=array("key","employes_key","etudes_succursales_key");
        $employes_etudes_succursales=$this->getemployee_orignal($data_employes_etudes_succursales,"employes_etudes_succursales");
        DB::table('employes_etudes_succursales')->truncate();
        DB::table('employes_etudes_succursales')->insert($employes_etudes_succursales);


        /*table etudes_succursales */
        $data_etudes_succursales=array("key","etudes_key","siege_social","adresse","ville","province","code_postal","telephone1","telephone2",
            "courriel_etudes_succursales","actif");
        $etudes_succursales=$this->getemployee_orignal($data_etudes_succursales,"etudes_succursales");
        DB::table('etudes_succursales')->truncate();
        DB::table('etudes_succursales')->insert($etudes_succursales);



    }



    public  function importevent()
    {
        /*table directions_de_travail_event */
        $data_directions_de_travail_event=array("id","date","directions_de_travail_key","lieu","description","actif" );
        $directions_de_travail_event=$this->getemployee_orignal($data_directions_de_travail_event,"directions_de_travail_event");
        DB::table('directions_de_travail_event')->truncate();
        DB::table('directions_de_travail_event')->insert($directions_de_travail_event);


        /*table convocation_aga_event */
        $data_convocation_aga_event=array("id","date","date_limite","lieu","description","actif" );
        $convocation_aga_event=$this->getemployee_orignal($data_convocation_aga_event,"convocation_aga_event");
        DB::table('convocation_aga_event')->truncate();
        DB::table('convocation_aga_event')->insert($convocation_aga_event);


        /*table convocation_ag_event */
        $data_convocation_ag_event=array("id","date","date_limite","lieu","description","actif" );
        $convocation_ag_event=$this->getemployee_orignal($data_convocation_ag_event,"convocation_ag_event");
        DB::table('convocation_ag_event')->truncate();
        DB::table('convocation_ag_event')->insert($convocation_ag_event);

        /*table congres_event */
        $data_congres_event=array("id","date","lieu","actif" );
        $congres_event=$this->getemployee_orignal($data_congres_event,"congres_event");
        DB::table('congres_event')->truncate();
        DB::table('congres_event')->insert($congres_event);

        /*table journee_formation_event */
        $data_journee_formation_event=array("id","date","departement","sujet","date_limite","lieu","cout","note","description","actif" );
        $journee_formation_event=$this->getemployee_orignal($data_journee_formation_event,"journee_formation_event");
        DB::table('journee_formation_event')->truncate();
        DB::table('journee_formation_event')->insert($journee_formation_event);



    }

    public function syscall()
    {
        $this->importempolyee();
        $this->importevent();
        echo "ok";
    }

}
