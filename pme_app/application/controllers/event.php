<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller 
{
	
	
	public function geteventall()
	{
		$this->load->database();
		$start=$this->input->get_post("start");
		$end=$this->input->get_post("end");
		
		$query=$this->db->query("SELECT date as start,date as end,description as description ,lieu   FROM leeroy_directions_de_travail_event
				where date between ? and ? ", array($start,$end));
		$return_arr=array();
		$reslt_arr=$query->result_array();
		foreach($reslt_arr as $ra)
		{
			preg_match( "/<h1 .*>.*<\/h1>/i", $ra["description"], $matches );
			if(count($matches)>=1)
			{
				$titre=$matches[0];
				$patterns=array();
				$patterns[0]="/^<h1 style=\"text-align: center; color: red;\">/";
				$patterns[1]="/<\/h1>/";
				
				
				$titre_final=preg_replace($patterns,array("",""),$titre);
				
			}else
			{
				$titre_final=$ra["lieu"];
			}
			$return_arr[]=array("start"=>$ra["start"],"end"=>$ra["end"],"title"=>$titre_final,"description"=>$ra["description"]);
		}
		//echo $titre;
		
		
		
		echo json_encode($return_arr);
		
	}
	
	

	public function geteventbyidcats()
	{
		$this->load->database();
		$start=$this->input->get_post("start");
		$end=$this->input->get_post("end");
		$idcats=$this->input->get_post("idcats");
		if($idcats=="")
			{
				$query_direction=$this->db->query("SELECT date as start,date as end,description as description ,lieu ,directions_de_travail_key ,nom FROM leeroy_directions_de_travail_event as dte
											INNER JOIN leeroy_directions_de_travail as dt on dt.key=dte.directions_de_travail_key
											where  date between ? and ?  ", array($start,$end));
			}else
			{
				
				$query_direction=$this->db->query("SELECT date as start,date as end,description as description ,lieu ,directions_de_travail_key ,nom FROM leeroy_directions_de_travail_event as dte
											INNER JOIN leeroy_directions_de_travail as dt on dt.key=dte.directions_de_travail_key
											where dte.directions_de_travail_key in(".$idcats.") and  date between ? and ?  ", array($start,$end));
			}
		
				
		$return_arr=array();
		$reslt_arr=$query_direction->result_array();
		$direction_arr["direction"]=array();
		$direction_arr_tempo=array();
		foreach($reslt_arr as $ra)
		{
			
			$direction_arr_tempo[]=array("start"=>$ra["start"],"end"=>$ra["end"],"title"=>strtoupper("DIRECTION DE TRAVAIL-".$ra["nom"]),"lieu"=>$ra["lieu"]);
		}
		
		//echo $titre;
		$direction_arr["direction"]=$direction_arr_tempo;
		$general=$this->geteventgeneral($start,$end);
		$return_arr=array_merge ($direction_arr,$general);
		//print_r($return_arr);
		echo json_encode($return_arr);
	
	}
	
	
	
	
	
	
	
	public function geteventgeneral($start,$end)
	{
		$this->load->database();
		//$start=$this->input->get_post("start");
		//$end=$this->input->get_post("end");
		$return_arr=array();
		
		
// assemeblee annuelle
		$query_assemblee_annuelle=$this->db->query("SELECT date as start,date as end,description as description ,lieu   FROM leeroy_convocation_aga_event
				where date between ? and ? ", array($start,$end));
		$return_arr_assemeblee_annuelle=array();
		$arr_assemeblee_annuelle=$query_assemblee_annuelle->result_array();
		foreach($arr_assemeblee_annuelle as $ra)
		{
	
			$return_arr_assemeblee_annuelle[]=array("start"=>$ra["start"],"end"=>$ra["end"],"title"=>"AVIS CONVOCATION ASSSEMBLÉE GÉNÉRALE ANNUELLE","lieu"=>$ra["lieu"]);
		}
		
		$return_arr["assemblee_annuelle"]=$return_arr_assemeblee_annuelle;
	
		
// assemeblee
		$query_assemblee=$this->db->query("SELECT date as start,date as end,description as description ,lieu   FROM leeroy_convocation_ag_event
				where date between ? and ? ", array($start,$end));
		$return_arr_assemeblee=array();
		$arr_assemeblee=$query_assemblee->result_array();
		foreach($arr_assemeblee as $ra)
		{
		
			$return_arr_assemeblee[]=array("start"=>$ra["start"],"end"=>$ra["end"],"title"=>"AVIS CONVOCATION ASSSEMBLÉE GÉNÉRALE","lieu"=>$ra["lieu"]);
		}
		
		$return_arr["assemblee"]=$return_arr_assemeblee;
		

// congres		
		
		$query_congres=$this->db->query("SELECT date as start,date as end,lieu FROM leeroy_congres_event
				where date between ? and ? ", array($start,$end));
		$return_arr_congres=array();
		$arr_congres=$query_congres->result_array();
		foreach($arr_congres as $ra)
		{
		
			$return_arr_congres[]=array("start"=>$ra["start"],"end"=>$ra["end"],"title"=>"CONGRÈS","sujet1"=>$ra["sujet1"],"sujet2"=>$ra["sujet2"],
										"sujet3"=>$ra["sujet3"],"sujet4"=>$ra["sujet4"],"lieu"=>$ra["lieu"]);
		}
		
		$return_arr["congres"]=$return_arr_congres;
		

//JOURNEE DE FORMATION

		$query_formation=$this->db->query("SELECT date as start,date as end,departement ,sujet, note,cout,lieu FROM leeroy_journee_formation_event
				where date between ? and ?  and actif=1", array($start,$end));
		$return_arr_formation=array();
		$arr_formation=$query_formation->result_array();
		foreach($arr_formation as $ra)
		{
		
			$return_arr_formation[]=array("start"=>$ra["start"],"end"=>$ra["end"],"title"=>"JOURÉE FORMATION","sujet"=>$ra["sujet"],"note"=>$ra["note"],
					"departement"=>$ra["departement"],"cout"=>$ra["cout"],"lieu"=>$ra["lieu"]);
		}
		
		$return_arr["formation"]=$return_arr_formation;
		
		
	
		
	
		return $return_arr;
	
	}
	
	
	
	public function getlistmemberby($idkey)
	{
		$this->load->database();
		$query=$this->db->query("SELECT e.key,e.prenom,e.nom,e.courriel,etu.nom as etude,es.adresse,es.telephone1 as tel FROM  leeroy_employes as e
										inner join leeroy_employes_directions_de_travail  as ed
										on ed.employes_key=e.key
										inner join leeroy_directions_de_travail as dt
										on dt.key =ed.directions_de_travail_key
inner join leeroy_employes_etudes_succursales as ees on ees.employes_key=e.key
inner join leeroy_etudes_succursales as es on es.key=ees.etudes_succursales_key
inner join leeroy_etudes as etu on etu.key=es.etudes_key
 where dt.key=? ", array($idkey));
		
		$reslt_arr=$query->result_array();
		
		echo json_encode($reslt_arr);
	}	
	
	/*
	public function test()
	{
		$this->load->database();
		$query=$this->db->query("SELECT date as start,date as end,description as description ,lieu   FROM directions_de_travail_event", array($start,$end));
		$return_arr=array();
		$reslt_arr=$query->result_array();
		foreach($reslt_arr as $ra)
		{
			preg_match( "/<h1 .*>.*<\/h1>/i", $ra["description"], $matches );
			if(count($matches)>=1)
			{
				$titre=$matches[0];
				$patterns=array();
				$patterns[0]="/^<h1 style=\"text-align: center; color: red;\">/";
				$patterns[1]="/<\/h1>/";
	
	
				$titre_final=preg_replace($patterns,array("",""),$titre);
	
			}else
			{
				$titre_final=$ra["lieu"];
			}
			$return_arr[]=array("start"=>$ra["start"],"end"=>$ra["end"],"title"=>$titre_final,"description"=>$ra["description"]);
		}
		//echo $titre;
		print_r($return_arr);
	}
	*/
	
	
	
	
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */