<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Import extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->library('csvreader');
        $this->load->database();
    }


    function getimport_to_cvs($table)
    {
        /*local*/
        /*
                $host = 'localhost'; // MYSQL database host adress
                $db = 'pmeinter_bd'; // MYSQL database name
                $user = 'root'; // Mysql Datbase user
                $pass = 'root'; // Mysql Datbase password
        */


                $host = '104.131.253.35'; // MYSQL database host adress
                $db = 'pmeinter_bd'; // MYSQL database name
                $user = 'pmeinter_extrane'; // Mysql Datbase user
                $pass = 'd{2^L;$}Wq-e'; // Mysql Datbase password

// Connect to the database
        $link = mysqli_connect($host, $user, $pass,$db);
        $link->set_charset("utf8");
        //$table="employes"; // this is the tablename that you want to export to csv from mysql.

        $this->exportMysqlToCsv($link,$table,$table);
        mysqli_close($link);
    }


    private function exportMysqlToCsv($conect,$table,$filename = 'export.csv')
    {
        $csv_terminated = "\n";
        $csv_separator = ",";
        $csv_enclosed = '"';
        $csv_escaped = "\\";
        $sql_query = "select * from $table";

        // Gets the data from the database
        $result = mysqli_query($conect, $sql_query);
        $fields_cnt = mysqli_num_fields($result);


        $schema_insert = '';
        while ($finfo = mysqli_fetch_field($result))
            {
               // print_r($finfo);
                $l = $csv_enclosed . str_replace($csv_enclosed, $csv_escaped . $csv_enclosed,
                        stripslashes(trim($finfo->name))) . $csv_enclosed;
                $schema_insert .= $l;
                $schema_insert .= $csv_separator;
            } // end for

            $out = trim(substr($schema_insert, 0, -1));
            $out .= $csv_terminated;

            // Format the data
            while ($row = mysqli_fetch_array($result)) {
                $schema_insert = '';
                for ($j = 0; $j < $fields_cnt; $j++) {
                    if ($row[$j] == '0' || $row[$j] != '') {

                        if ($csv_enclosed == '') {

                            $schema_insert .=   str_replace(array("\r", "\n",","),array("","","||"),$row[$j]);
                        } else {
                            $row_replace=str_replace(array("\r", "\n",","),array("","","||"),$row[$j]);
                            $schema_insert .= $csv_enclosed .
                                str_replace($csv_enclosed, $csv_escaped . $csv_enclosed, $row_replace) . $csv_enclosed;
                        }
                    } else {
                        $schema_insert .= '';
                    }

                    if ($j < $fields_cnt - 1) {
                        $schema_insert .= $csv_separator;
                    }
                } // end for

                $out .= $schema_insert;
                $out .= $csv_terminated;
            } // end while

            // header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            //header("Content-Length: " . strlen($out));
            // Output to browser with appropriate mime type, you choose ;)
            // header("Content-type: text/x-csv");
            //header("Content-type: text/csv");
            //header("Content-type: application/csv");
            // header("Content-Disposition: attachment; filename=$filename");
        //file_put_contents("./import/".$filename.".cvs", "\xEF\xBB\xBF" . $out);
        file_put_contents("./import/".$filename.".cvs", $out);
            //exit;

        }


    function import_employes()
    {
        $this->getimport_to_cvs("employes");
        $this->getimport_to_cvs("directions_de_travail");
        $this->getimport_to_cvs("employes_directions_de_travail");
        $this->getimport_to_cvs("etudes");
        $this->getimport_to_cvs("employes_etudes_succursales");
        $this->getimport_to_cvs("etudes_succursales");
        $result["employes"] =   $this->csvreader->parse_file('./import/employes.cvs');
        $result["directions_de_travail"] =   $this->csvreader->parse_file('./import/directions_de_travail.cvs');
        $result["employes_directions_de_travail"] =   $this->csvreader->parse_file('./import/employes_directions_de_travail.cvs');

        $result["etudes"] =   $this->csvreader->parse_file('./import/etudes.cvs');
        $result["employes_etudes_succursales"] =   $this->csvreader->parse_file('./import/employes_etudes_succursales.cvs');
        $result["etudes_succursales"] =   $this->csvreader->parse_file('./import/etudes_succursales.cvs');
        //print_r($result["employes_directions_de_travail"]);

        $this->db->truncate('leeroy_employes');

        foreach($result["employes"] as $r)
        {
            $data=array(
                "key"=>$r["key"],
                "prenom"=>$r["prenom"],
                "nom"=>$r["nom"],
                "courriel"=>$r["courriel"],
                "associe"=>$r["associe"],
                "succursale"=>$r["succursale"],
                "actif"=>$r["actif"]
            );
            $this->db->insert('leeroy_employes', $data);
        }


        $this->db->truncate('leeroy_directions_de_travail');

        foreach($result["directions_de_travail"] as $dt)
        {
            $this->db->insert('leeroy_directions_de_travail', $dt);
        }


        $this->db->truncate('leeroy_employes_directions_de_travail');

        foreach($result["employes_directions_de_travail"] as $edt)
        {
            $this->db->insert('leeroy_employes_directions_de_travail', $edt);
        }



        $this->db->truncate('leeroy_etudes');
        foreach($result["etudes"] as $r)
        {
            $data=array(
                "key"=>$r["key"],
                "nom"=>$r["nom"],
                "nom"=>$r["nom"],
                "actif"=>$r["actif"],
                );
            $this->db->insert('leeroy_etudes', $data);
        }



        $this->db->truncate('leeroy_employes_etudes_succursales');
        foreach($result["employes_etudes_succursales"] as $r)
        {
            $data=array(
                "key"=>$r["key"],
                "employes_key"=>$r["employes_key"],
                "etudes_succursales_key"=>$r["etudes_succursales_key"],

            );
            $this->db->insert('leeroy_employes_etudes_succursales', $data);
        }


        $this->db->truncate('leeroy_etudes_succursales');
        foreach($result["etudes_succursales"] as $r)
        {
            $data=array(
                "key"=>$r["key"],
                "etudes_key"=>$r["etudes_key"],
                "siege_social"=>$r["siege_social"],
                "adresse"=>$r["adresse"],
                "ville"=>$r["ville"],
                "province"=>$r["province"],
                "code_postal"=>$r["code_postal"],
                "telephone1"=>$r["telephone1"],
                "telephone2"=>$r["telephone2"],
                "courriel_etudes_succursales"=>$r["courriel_etudes_succursales"],
                "actif"=>$r["actif"],
            );
            $this->db->insert('leeroy_etudes_succursales', $data);
        }




    }

/** importer event */


    function import_event()
    {

        $this->getimport_to_cvs("directions_de_travail_event");
        $this->getimport_to_cvs("convocation_aga_event");
        $this->getimport_to_cvs("convocation_ag_event");
        $this->getimport_to_cvs("congres_event");
        $this->getimport_to_cvs("journee_formation_event");

        $result["directions_de_travail_event"] =   $this->csvreader->parse_file('./import/directions_de_travail_event.cvs');

        $result["convocation_aga_event"] =   $this->csvreader->parse_file('./import/convocation_aga_event.cvs');
        $result["convocation_ag_event"] =   $this->csvreader->parse_file('./import/convocation_ag_event.cvs');
        $result["congres_event"] =   $this->csvreader->parse_file('./import/congres_event.cvs');
        $result["journee_formation_event"] =   $this->csvreader->parse_file('./import/journee_formation_event.cvs');
        print_r($result["directions_de_travail_event"] );

        $this->db->truncate('leeroy_directions_de_travail_event');

        foreach($result["directions_de_travail_event"] as $r)
        {
            $data=array(
                "id"=>$r["id"],
                "date"=>$r["date"],
                "directions_de_travail_key"=>$r["directions_de_travail_key"],
                "lieu"=>$r["lieu"],
                "description"=>$r["description"],
                "actif"=>$r["actif"]

            );
            $this->db->insert('leeroy_directions_de_travail_event', $data);
        }



        $this->db->truncate('leeroy_convocation_aga_event');

        foreach($result["convocation_aga_event"] as $r)
        {
            $data=array(
                "id"=>$r["id"],
                "date"=>$r["date"],
                "date_limite"=>$r["date_limite"],
                "lieu"=>$r["lieu"],
                "description"=>$r["description"],
                "actif"=>$r["actif"]

            );
            $this->db->insert('leeroy_convocation_aga_event', $data);
        }



        $this->db->truncate('leeroy_convocation_ag_event');

        foreach($result["convocation_ag_event"] as $r)
        {
            $data=array(
                "id"=>$r["id"],
                "date"=>$r["date"],
                "date_limite"=>$r["date_limite"],
                "lieu"=>$r["lieu"],
                "description"=>$r["description"],
                "actif"=>$r["actif"]

            );
            $this->db->insert('leeroy_convocation_ag_event', $data);
        }



        $this->db->truncate('leeroy_congres_event');

        foreach($result["congres_event"] as $r)
        {
            $data=array(
                "id"=>$r["id"],
                "date"=>$r["date"],
                "date_limite"=>$r["date_limite"],
                "lieu"=>$r["lieu"],
                "actif"=>$r["actif"]

            );
            $this->db->insert('leeroy_congres_event', $data);
        }



        $this->db->truncate('leeroy_journee_formation_event');

        foreach($result["journee_formation_event"] as $r)
        {
            $data=array(
                "id"=>$r["id"],
                "date"=>$r["date"],
                "departement"=>$r["departement"],
                "sujet"=>$r["sujet"],
                "date_limite"=>$r["date_limite"],
                "lieu"=>$r["lieu"],
                "cout"=>$r["cout"],
                "note"=>$r["note"],
                "description"=>$r["description"],
                "actif"=>$r["actif"]

            );
            $this->db->insert('leeroy_journee_formation_event', $data);
        }


    }
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */