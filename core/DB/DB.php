<?php

	/*
		Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/

	$str['eng']['error_query']		= "The query has failed : ";
	$str['eng']['error_connect']	= "Error connecting to the database : ";
	$str['eng']['error_select_db']  = "Unable to select specified database : ";
	$str['eng']['error_close']		= "Error closing database link : ";

	/**
     *
     * @version 1.0
     * @author Jean-Philippe Ricard <ricardjp@quiboweb.ca>
	 * @package	Quibo
     */
	class DB {

		var $str;		// The array used to display messages
		var $db;		// The DB link
		var $result = array();	// The result set returned by the last query
		var $row = array();		// The actual working row of the result set

		function DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS = "MySQL") {
			global $str;
			$this->str = $str['eng'];
			//import("quibo.DB.DBMS.".$DBMS);

			require_once("DBMS/" . $DBMS . ".php");
			$this->db = new $DBMS($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME);
		}

		function getField($field) {
			$last_result = count($this->db->result)-1;
			if ($last_result < 0) { $last_result = 0; }

			if (isset($this->db->row[$last_result])) {
				$last_row = count($this->db->row[$last_result])-1;
				if ($last_row < 0) { $last_row = 0; }
			} else {
				$last_row = 0;
			}

			if (isset($this->db->row[$last_result][$last_row][$field])) {
				return $this->db->row[$last_result][$last_row][$field];
			}

			return '';
			//return addslashes($this->db->row[$last_result][$last_row][$field]);
		}

		function reset() {
			if (isset($this->result) && strtolower(gettype($this->result)) == "array") {
				array_pop($this->result);
			}

			if (isset($this->row) && strtolower(gettype($this->row)) == "array") {
				array_pop($this->row);
			}

			if (isset($this->db->result) && strtolower(gettype($this->db->result)) == "array") {
				array_pop($this->db->result);
			}

			if (isset($this->db->row) && strtolower(gettype($this->db->row)) == "array") {
				array_pop($this->db->row);
			}
		}

		/* These functions MUST be overrided by children class */

		function query($query) {
			return $this->db->query($query);
		}

		function next_record() {
			return $this->db->next_record();
		}

		function getRow() {
			$last_result = count($this->db->result)-1;
			if ($last_result < 0) { $last_result = 0; }
			$last_row = count($this->db->row[$last_result])-1;
			if ($last_row < 0) { $last_row = 0; }

			return $this->db->row[$last_result][$last_row];
		}

		function num_rows() {
			return $this->db->num_rows();
		}

		function getInsertId() {
			return $this->db->getInsertId();
		}

		function getQueryInfo() {
			return $this->db->getQueryInfo();
		}

		function close() {
			return $this->db->close();
		}
	}
?>
