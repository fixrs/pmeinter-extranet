<?php

	/*
		Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/

	$str['eng']['error_query']		= "The query has failed : ";
	$str['eng']['error_connect']	= "Error connecting to the database : ";
	$str['eng']['error_select_db']	= "Unable to select specified database : ";
	$str['eng']['error_close']		= "Error closing database link : ";




	class MySQL extends DB {

        function MySQL($dbhost, $dbuser, $dbpass, $dbname) {
            $this->str = $GLOBALS['str']['eng'];

            $this->db = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname) or die($this->str['error_select_db'] . mysqli_error($this->db));
            mysqli_set_charset($this->db, DB_CHARSET);
            return $this->db;
        }

        function query($query) {
            @$this->result[count($this->result)] = mysqli_query($this->db, $query);

            if (!$this->result[count($this->result)-1]) {
                @die($this->str["error_query"] . mysqli_error($this->db));
            }

            return $this->result[count($this->result)-1];
        }

        function close() {
            parent::reset();
            if (!mysqli_close($this->db)) {
                die($this->str['error_close'] . mysqli_error($this->db));
            }
        }

        function next_record() {
            $last_result = count($this->result)-1;

            if ($last_result < 0) { parent::reset(); return FALSE; }

            $last_row = -1;
            if (isset($this->row[$last_result])) {
                $last_row += count($this->row[$last_result]);
            }

            $this->row[$last_result][$last_row + 1] = mysqli_fetch_array($this->result[$last_result]);

            if (!$this->row[$last_result][$last_row + 1]) {
                parent::reset();
                return FALSE;
            } else {
                return $this->row[$last_result][$last_row + 1];
            }
        }

        function get_num_rows() {
            return mysqli_num_rows($this->result[count($this->result)-1]);
        }

        function num_rows() {
            return mysqli_num_rows($this->result[count($this->result)-1]);
        }

        function get_num_fields() {
            return mysqli_num_fields($this->result[count($this->result)-1]);
        }

        function get_insert_id() {
            return mysqli_insert_id($this->db);
        }

        function getInsertId() {
            return mysqli_insert_id($this->db);
        }

        function get_query_info() {
            return mysqli_info($this->db);
        }

        function getQueryInfo() {
            return mysqli_info($this->db);
        }

        function get_field_name($i) {
            $properties = mysqli_fetch_field_direct($this->result[count($this->result)-1], $i);
            return is_object($properties) ? $properties->name : null;
        }

        function get_field_desc($field) {
            return mysqli_query($this->db, "DESCRIBE `" . $field . "`");
        }

        function get_affected_rows() {
            return mysqli_affected_rows($this->db);
        }

        function get_fields_list($table) {
            return mysqli_query($this->db, "SHOW COLUMNS FROM `" . $table . "`");
        }

        function get_table_list($like) {
            return mysqli_query($this->db, "SHOW TABLES FROM `" . $this->dbname . "` LIKE " . $like);
        }

        function escape($value) {
            return mysqli_real_escape_string($this->db, $value);
        }

        function toInt($value) {
            if (is_null($value)) {
                return null;
            }
            return intval($value);
        }

        function fromInt($value) {
            if (is_null($value)) {
                return 'NULL';
            }
            return "'" . $this->escape(intval($value)) . "'";
        }

        function toFloat($value) {
            if (is_null($value)) {
                return null;
            }
            return floatval($value);
        }

        function fromFloat($value) {
            if (is_null($value)) {
                return 'NULL';
            }
            return "'" . $this->escape(floatval($value)) . "'";
        }

        function toString($value) {
            if (is_null($value)) {
                return null;
            }
            return $value;
        }

        function fromString($value) {
            if (is_null($value)) {
                return 'NULL';
            }
            return "'" . $this->escape($value) . "'";
        }

        function toTimestamp($value) {
            if (is_null($value)) {
                return null;
            }
            return strtotime($value);
        }

        function fromTimestamp($value) {
            if (is_null($value)) {
                return 'NULL';
            }
            return "'" . $this->escape(date('Y-m-d H:i:s', $value)) . "'";
        }

        function toBoolean($value) {
            if (is_null($value)) {
                return null;
            }
            settype($value, 'boolean');
            return $value;
        }

        function fromBoolean($value) {
            if (is_null($value)) {
                return 'NULL';
            }
            return ($value) ? 'TRUE' : 'FALSE';
        }
    }



?>
