<?PHP
	/*
		Copyright (C) 2004 Francois Viens, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/


	/**
	 *
	 *
	 * @version 1.0
	 * @author Francois Viens <viensf@quiboweb.ca>
	 * @package	Quibo
	 */

	class I18N {

		var $TEXT;
		var $I18N_FILES_LIST;
		var $LANGUAGES;
		var $PO = array();
		var $LANG;
		var $BASEPATH;

		function I18N($I18N_FILES_LIST, $LANGUAGES, $BASEPATH = "") {

			$this->LANGUAGES = explode(" ", $LANGUAGES);
			$this->LANG = $this->LANGUAGES[0];
			$this->I18N_FILES_LIST = $I18N_FILES_LIST;
			$this->BASEPATH = $BASEPATH;

			$this->load();

		}

		function load() {
			for ($i = 0; $i < count($this->LANGUAGES); $i++) {
				$content = "";

				if (file_exists($this->BASEPATH . $this->LANGUAGES[$i] . ".po")) {

					$handle = fopen(
						$this->BASEPATH . $this->LANGUAGES[$i] . ".po",
						"r"
					);

					if (filesize($this->BASEPATH . $this->LANGUAGES[$i] . ".po") > 0) {

						$content = fread(
							$handle,
							filesize($this->BASEPATH . $this->LANGUAGES[$i] . ".po")
						);

					}

					fclose($handle);

					$array = "";

					preg_match_all(
						"/msgid \"(.*?)\"\s*\nmsgstr \"(.*?)\"\n\n/si",
						$content,
						$array
					);

					for ($j = 0; $j < count($array[0]); $j++) {
						//if ($this->PO[$array[1][$j]][$this->LANGUAGES[$i]] == "") {
						if (empty($this->PO[$array[1][$j]][$this->LANGUAGES[$i]])) {
							$this->PO[$array[1][$j]][$this->LANGUAGES[$i]][0] = $array[2][$j];
						}
					}
				}
			}
		}

		function createPO() {
			// $file_array = file($this->I18N_FILES_LIST);

			// for ($i = 0; $i < count($file_array); $i++) {

			// 	if (file_exists(trim($file_array[$i]))) {
			// 		$handle = fopen(trim($file_array[$i]), "r");
			// 		$content = fread($handle, filesize(trim($file_array[$i])));
			// 		fclose($handle);
			// 		$this->addPO($content, trim($file_array[$i]));
			// 	}
			// }

			// $this->writePO();
		}

		function writePO() {
			// for ($i = 0; $i < count($this->LANGUAGES); $i++) {
			// 	$content = "";

			// 	if ($this->PO != "") {
			// 		foreach ($this->PO as $key => $value) {
			// 			if ($key != "" && $this->PO[$key][$this->LANGUAGES[$i]][0] == "~void~") {
			// 				$content .= $this->PO[$key][$this->LANGUAGES[$i]][2];
			// 				$content .= "msgid \"" . stripslashes($key) . "\"\n";
			// 				$content .= "msgstr \"" . $this->PO[$key][$this->LANGUAGES[$i]][1] . "\"\n\n";
			// 			}
			// 		}

			// 		$handle = fopen(
			// 			$this->BASEPATH . $this->LANGUAGES[$i] . ".po",
			// 			"w"
			// 		);

			// 		fwrite($handle, $content);
			// 		fclose($handle);
			// 	}
			// }
		}

		function addPO($content, $filename = "") {

			// $array = "";

			// preg_match_all("~__\(\"(.*?)\"\)~si", $content, $array);

			// for ($i = 0; $i < count($array[0]); $i++) {
			// 	for ($j = 0; $j < count($this->LANGUAGES); $j++) {
			// 		if ($this->PO[$array[1][$i]][$this->LANGUAGES[$j]][0] != "" && $this->PO[$array[1][$i]][$this->LANGUAGES[$j]][0] != "~void~") {

			// 			$this->PO[$array[1][$i]][$this->LANGUAGES[$j]][1] = $this->PO[$array[1][$i]][$this->LANGUAGES[$j]][0];

			// 		} else {

			// 			if ($this->PO[$array[1][$i]][$this->LANGUAGES[$j]][0] != "~void~") {
			// 				$this->PO[$array[1][$i]][$this->LANGUAGES[$j]][1] = "";
			// 			}

			// 		}

			// 		$this->PO[$array[1][$i]][$this->LANGUAGES[$j]][0] = "~void~";
			// 		$this->PO[$array[1][$i]][$this->LANGUAGES[$j]][2] .= "#" . $filename . "\n";
			// 	}
			// }

			// preg_match_all("~__x\(\"(.*?)\"\,~si", $content, $array);

			// for ($i = 0; $i < count($array[0]); $i++) {
			// 	for ($j = 0; $j < count($this->LANGUAGES); $j++) {
			// 		if ($this->PO[$array[1][$i]][$this->LANGUAGES[$j]][0] != "" && $this->PO[$array[1][$i]][$this->LANGUAGES[$j]][0] != "~void~") {

			// 			$this->PO[$array[1][$i]][$this->LANGUAGES[$j]][1] = $this->PO[$array[1][$i]][$this->LANGUAGES[$j]][0];

			// 		} else {
			// 			if ($this->PO[$array[1][$i]][$this->LANGUAGES[$j]][0] != "~void~") {
			// 				$this->PO[$array[1][$i]][$this->LANGUAGES[$j]][1] = "";
			// 			}
			// 		}

			// 		$this->PO[$array[1][$i]][$this->LANGUAGES[$j]][0] = "~void~";

			// 		$this->PO[$array[1][$i]][$this->LANGUAGES[$j]][2] .= "#" . $filename . "\n";
			// 	}
			// }
		}

		function setLang($lang) {
			$this->LANG = $lang;
		}

		function __($text, $position) {
			$text = $this->entity2ansi($text);

			//if (trim($this->PO[$text][$this->LANG][0]) == "") {
			if (empty($this->PO[$text][$this->LANG][0])) {
				return $this->ansi2entity($text);
			} else {
				return $this->ansi2entity($this->PO[$text][$this->LANG][0]);
			}
		}

		function __x($text, $parametres = "") {
			if (trim($this->PO[$text][$this->LANG][0]) != "") {
				$text = $this->PO[$text][$this->LANG][0];
			}

			foreach ($parametres as $key => $value) {
				$text = preg_replace("/\{" . $key . "\}/i", $value, $text);
			}
			return $this->ansi2entity($text);
		}

		function ansi2entity($string) {

			// $string = htmlentities($string, ENT_NOQUOTES, "UTF-8");
			// $string = preg_replace("/&lt;/i", "<", $string);
			// $string = preg_replace("/&gt;/i", ">", $string);
			// $string = preg_replace("/&amp;/i", "&", $string);
			// $string = preg_replace("/&quot;/i", "\"", $string);
			return $string;
		}

		function entity2ansi($string, $utf8 = 1) {
			//$string = utf8_decode($string);
			//if ($utf8) {
			//	$string = html_entity_decode($string, ENT_NOQUOTES);//, "UTF-8");
			//} else {
			//	$string = html_entity_decode($string, ENT_NOQUOTES);
			//}
			//$string = html_entity_decode($string);
			#$string = preg_replace("/</i", "&lt;", $string);
			#$string = preg_replace("/>/i", "&gt;", $string);
			#$string = preg_replace("/\&/i", "&amp;", $string);
			#$string = preg_replace("/\"/i", "&quot;", $string);
			return $string;
		}

	}
