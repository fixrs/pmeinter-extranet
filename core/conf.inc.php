<?PHP


ini_set("session.gc_maxlifetime", "18000");
global $PACKAGES_PATH, $DBMS, $DB_NAME, $DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB;

date_default_timezone_set('America/Montreal');

function import($package) {
	global $PACKAGES_PATH;
	$packages = preg_split("/\./", $package);
	$path = $PACKAGES_PATH.DIRECTORY_SEPARATOR;

	for ($i = 0; $i < count($packages); $i++) {
		$path .= DIRECTORY_SEPARATOR.$packages[$i];
	}

	$path .= ".php";
	require_once("$path");
}

function addslashesParam() {

	foreach ($_POST as $key => $value) {
		if ($_POST[$key] != "" && !is_array($value)) {
			$_POST[$key] = addslashes($value);
		}
	}

	foreach ($_GET as $key => $value) {
		if ($_GET[$key] != "" && !is_array($value)) {
			$_GET[$key] = addslashes($value);
		}
	}

	foreach ($_REQUEST as $key => $value) {
		if ($_REQUEST[$key] != "" && !is_array($value)) {
			$_REQUEST[$key] = addslashes($value);
		}
	}

}



$BASEPATH =	dirname(__FILE__);
$BASEURL =	"/";
$PACKAGES_PATH = $BASEPATH;


//$DB_HOSTNAME = $DB_HOST;
$DB_HOSTNAME = "104.131.253.35";
$DB_USER = "pmeinter_user";
$DB_PASSWORD = "S0467k3F3e85y4K";
$DB_NAME = "pmeinter_bd";

$DBMS = "MySQL";

$LANGUAGE		=	"fr";
$LANGUAGES 		= 	array("fr" => "Fran&ccedil;ais");//, "en" => "English"

import("Date");
import("Calendar");
import("DB.DB");

$DB = new DB($DB_HOSTNAME, $DB_USER, $DB_PASSWORD, $DB_NAME, $DBMS);
$DB2 = new DB(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME, $DBMS);


define('EXTRANET_DOCS_ETUDES_IMG_URL', "https://bd.pmeinter.ca/docs/etudes_img/");
define('EXTRANET_DOCS_ETUDES_NOUVELLES_URL', "https://bd.pmeinter.ca/docs/etudes_nouvelles/");
define('EXTRANET_DOCS_EMPLOYES_IMG_URL', "https://bd.pmeinter.ca/docs/employes_img/");


function asciionly($string) {
	$string = htmlentities($string, ENT_QUOTES, "UTF-8");
	$string = preg_replace("/ &amp; /", " et ", $string);
	$string = preg_replace("/&([a-z])[^\;]+\;/", "\$1", strtolower(trim($string)));
	$string = preg_replace("/[^a-z0-9]/", "-", $string);
	$string = preg_replace("/-+/", "-", $string);
	$string = preg_replace("/-$/", "", $string);
	$string = preg_replace("/-039/", "", $string);
	return $string;
}

function toHtml($str) {
	return htmlentities(trim(stripslashes($str)), ENT_QUOTES, "UTF-8");
}

function url_exists($url) {
    $file_headers = @get_headers($url);

    if(preg_match("/404 /", $file_headers[0])){
          return false;
    } else if (preg_match("/302 /", $file_headers[0]) && preg_match("/404 /", $file_headers[7])) {
        return false;
    } else {
        return true;
    }
}

function getDirectionsTravail($layout, $filters = "all") {
	global $DB;

	$DB->query("SELECT * FROM `directions_de_travail` ORDER BY nom");

	$html = "";
	$array = array();
	while ($DB->next_record()) {
		switch ($layout) {
			case 'li-checkbox':
				$html .= "<li class=\"direction".$DB->getField("key")   ."\"><input type=\"checkbox\" class=\"secteur\" " . ((is_array($filters) && in_array($DB->getField("key"), $filters)) || $filters == "all" ? "checked" : "") . " name=\"secteur\" value=\"" . $DB->getField("key") . "\">" . ucfirst($DB->getField("nom")) . "</li>\n";
				break;

			case 'li':
				$html .= "<li>" . ucfirst($DB->getField("nom")) . "</li>\n";
				break;

			case 'array':
				$array[$DB->getField("key")] = ucfirst($DB->getField("nom"));
				break;

			default:
				$html .= ", " . ucfirst($DB->getField("nom")) . "\n";
				break;
		}
	}

	if ($layout == "array") {
		return $array;
	}

	echo preg_replace("/^\, /is", "", $html);
}



function getMembresDirectionTravail($id) {
	global $DB;

	$DB->query(
		"SELECT DISTINCT e.*, et.nom AS etude_nom, et.key AS etude_key, es.telephone1 ".
		"FROM employes e INNER JOIN `employes_directions_de_travail` edt ON e.key = edt.employes_key AND edt.directions_de_travail_key = '" . $id . "' ".
		"INNER JOIN employes_etudes_succursales ees ON ees.employes_key = e.key ".
		"INNER JOIN etudes_succursales es ON ees.etudes_succursales_key = es.key AND es.actif = 1 ".
		"INNER JOIN etudes et ON es.etudes_key = et.key WHERE e.actif = 1 ORDER BY nom, prenom "
	);

	$html = "";
	$array = array();
	$keys = array();
	while ($DB->next_record()) {

		if (!isset($keys[trim($DB->getField("nom")) . trim($DB->getField("prenom")) . trim($DB->getField("etude_nom"))])) {
			$array[] = array("nom" => $DB->getField("nom"), "key" => $DB->getField("key"), "prenom" => $DB->getField("prenom"), "etude_key" => $DB->getField("etude_key"), "etude" => $DB->getField("etude_nom"), "telephone" => $DB->getField("telephone1"), "courriel" => $DB->getField("courriel"), "linkedin" => $DB->getField("linkedin"));
				$keys[trim($DB->getField("nom")) . trim($DB->getField("prenom")) . trim($DB->getField("etude_nom"))] = 1;
		}
	}

	return $array;
}


if (!function_exists("current_time")) {
	function current_time($format) {
		return date($format);
	}
}


if (isset($_GET["ajax"]) && isset($_GET["date"]) && isset($_GET["layout"])) {
	if (!function_exists("__")) {
		function __($string) {
			return $string;
		}
	}
}

function getCalendar() {
	global $DB;
	$layout = "month";
	$date = current_time("Y-m-d");
	$filters = "";


	if (isset($_GET["date"]) && trim($_GET["date"]) != "") { $date = $_GET["date"]; }
	if (isset($_GET["layout"]) && trim($_GET["layout"]) != "") { $layout = $_GET["layout"]; }
	if (isset($_GET["filters"])) {

		if (is_array($_GET["filters"])) {
			$filters = $_GET["filters"];
		} else if (trim(urldecode($_GET["filters"])) != "" && trim(urldecode($_GET["filters"])) != "null") {
			$filters = json_decode(stripslashes(trim(urldecode($_GET["filters"]))), true);

		} else {
			$filters = "";
		}
	}

	$calendar = new Calendar($date, $layout, $filters);

	echo $calendar->toString($DB);
}

if (isset($_GET["ajax"]) && isset($_GET["date"]) && isset($_GET["layout"])) { getCalendar(); exit; }



function getMemberProfile($employe_key) {
    global $DB;

    $DB->query(
        "SELECT DISTINCT et.nom AS etude, em.prenom AS prenom, em.nom AS nom, em.courriel AS courriel, em.description AS description, em.image AS image, em.annee_debut_pratique AS annee_debut_pratique, em.expertises_droit_affaires AS expertises_droit_affaires, em.expertises_droit_personne AS expertises_droit_personne, em.expertises_droit_immobilier AS expertises_droit_immobilier, em.expertises_sectorielles AS expertises_sectorielles, em.succursale AS succursale, fo.abbreviation_m AS fonction ".
        "FROM `employes` AS em INNER JOIN `employes_etudes_succursales` AS em_et_su ON em.key = em_et_su.employes_key AND em.key = '" . $employe_key . "' ".
        "INNER JOIN `etudes_succursales` AS et_su ON em_et_su.etudes_succursales_key = et_su.key ".
        "INNER JOIN `etudes` AS et ON et_su.etudes_key = et.key ".
        "INNER JOIN `employes_fonctions` AS em_fo ON em.key = em_fo.employes_key ".
        "INNER JOIN `fonctions` AS fo ON em_fo.fonctions_key = fo.key ".
        "WHERE et.actif = '1' AND et_su.actif = '1' AND em.actif = '1' ;"
    );


    $employes = array();

    $html = "";

    while ($DB->next_record()) {

        $etude = toHtml($DB->getField('etude'));
        $prenom = toHtml($DB->getField('prenom'));
        $nom = toHtml($DB->getField('nom'));
        $courriel = toHtml($DB->getField('courriel'));
        $notes_biographiques = stripslashes($DB->getField('description'));
        $annee_debut_pratique = toHtml($DB->getField('annee_debut_pratique'));
        $annee_debut_pratique_avocat = toHtml($DB->getField('annee_debut_pratique_avocat'));
        $expertises_droit_affaires = stripslashes($DB->getField('expertises_droit_affaires'));
        $expertises_droit_personne = stripslashes($DB->getField('expertises_droit_personne'));
        $expertises_droit_immobilier = stripslashes($DB->getField('expertises_droit_immobilier'));
        $expertises_sectorielles = stripslashes($DB->getField('expertises_sectorielles'));
        $succursale = toHtml($DB->getField('succursale'));
        $image_file = toHtml($DB->getField('image'));
        $fonction = toHtml($DB->getField('fonction'));

        $title =  $etude;

        $employes[$employe_key]['id'] = "employe_" . $employe_key;
        $employes[$employe_key]['nom'] = $fonction . " " . $prenom . " " . $nom;
        $employes[$employe_key]['courriel'] = $courriel;
        $employes[$employe_key]['notes_biographiques'] = $notes_biographiques;
        $employes[$employe_key]['image_url'] = EXTRANET_DOCS_EMPLOYES_IMG_URL . $image_file;

        if (!url_exists($employes[$employe_key]['image_url'])) {
            $employes[$employe_key]['image_url'] = preg_replace("/\.jpg/", ".JPG", $employes[$employe_key]['image_url']);
        }

        $employes[$employe_key]['description'] = "";

        if ($courriel != "") {
            $employes[$employe_key]['email'] .= "<div class=\"email\"><a href=\"mailto:" . $courriel . "\" alt=\"Courriel de l'employ&eacute;\">" . $courriel . "</a></div>";
        }

        if ($succursale != "") {
            $employes[$employe_key]['description'] .= "<p><strong>Succursale attitr&eacute;e&nbsp;:</strong> " . $succursale . "</p>";
        }

        $DB->query(
            "SELECT fonctions_key ".
            "FROM `employes_fonctions` ".
            "WHERE employes_key = '" . $employe_key . "' "
        );
        $fonctions = array();
        while ($DB->next_record()) {
            $fonctions[$DB->getField("fonctions_key")] = 1;
        }

        if ($annee_debut_pratique != "" && $fonctions[8] == 1) {
            $employes[$employe_key]['debut_pratique'] .= "<p class=\"debut-pratique\"><strong>Notaire depuis&nbsp;:</strong> " . $annee_debut_pratique . "</p>";
        }



        if ($annee_debut_pratique_avocat != "" && $fonctions[15] == 1) {
            $employes[$employe_key]['debut_pratique'] .= "<p class=\"debut-pratique\"><strong>Avocat(e), conseiller(e) juridique depuis&nbsp;:</strong> " . $annee_debut_pratique_avocat . "</p>";
        } else if ($fonctions[15] == 1) {
            $employes[$employe_key]['debut_pratique'] .= "<p class=\"debut-pratique\"><strong>Avocat(e), conseiller(e) juridique</strong> </p>";
        }

        if ($expertises_droit_affaires != "") {
            $employes[$employe_key]['affaires'] .= "<p class=\"affaires\"><strong>Droit des affaires&nbsp;:</strong><br />" . $expertises_droit_affaires . "</p>";
        }
        if ($expertises_droit_personne != "") {
            $employes[$employe_key]['personnes'] .= "<p class=\"personnes\"><strong>Droit de la personne&nbsp;:</strong><br />" . $expertises_droit_personne . "</p>";
        }
        if ($expertises_droit_immobilier != "") {
            $employes[$employe_key]['immobilier'] .= "<p class=\"immobilier\"><strong>Droit immobilier&nbsp;:</strong><br />" . $expertises_droit_immobilier . "</p>";
        }
        if ($expertises_sectorielles != "") {
            $employes[$employe_key]['sectoriel'] .= "<p class=\"sectoriel\"><strong>Expertises sectorielles&nbsp;:</strong><br />" . $expertises_sectorielles . "</p>";
        }

    }

    $html .=
        "<div id=\"" . $employes[$employe_key]['id'] . "\" class=\"equipe01\">\n".
        (preg_match("/\.[a-z]+$/i", $employes[$employe_key]['image_url']) ? " <div class=\"imgEquipe\"><img width=\"126\" height=\"180\" src=\"" . $employes[$employe_key]['image_url'] . "\" alt=\"" . $employes[$employe_key]['nom'] . "\"></div>\n" : "").
        "   <h3><a href=\"mailto:" . $courriel . "\">" . $employes[$employe_key]['nom'] . "</a></h3>\n".
        "   ". $employes[$employe_key]['debut_pratique'] . "\n".
        "   ". $employes[$employe_key]['affaires'] . "\n".
        "   ". $employes[$employe_key]['personnes'] . "\n".
        "   ". $employes[$employe_key]['immobilier'] . "\n".
        "   ". $employes[$employe_key]['sectoriel'] . "\n";

    if (strlen($employes[$employe_key]['notes_biographiques']) > 16) {

        if (strpos($employes[$employe_key]['notes_biographiques'], '<p>') === false) {
            $employes[$employe_key]['notes_biographiques'] = '<p>' . $employes[$employe_key]['notes_biographiques'] . '</p>';
        }

        $html .=
            "       <h4>Notes biographiques</h4>".
            "       <div class=\"bio\">" . $employes[$employe_key]['notes_biographiques'] . "</div>\n";
    }

    $html .=
        "<br clear=\"all\" /></div>";

    return $html;
}






## Magic Quote Must BE ON on the php.ini config file
// if (!get_magic_quotes_gpc()) { addslashesParam(); }
// if (isset($_GET["post"]) && $_GET["post"] == "get" && count($_POST) == 0) { $_POST = $_GET; }

