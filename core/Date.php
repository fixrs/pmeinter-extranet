<?php
	
	/*
		Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/

	/**
     *
     * @version 1.0
     * @author Jean-Philippe Ricard <ricardjp@quiboweb.ca>
	 * @package	Quibo
     */
	class Date {

		var $days;
		var $months;
		var $datestr;

		/* create a new Date NOW */
		function Date() {
			$this->setLabels();
			$datestr = $this->days[date("w")];
			$datestr .= " " . __("le") . " ";
			$datestr .= date("d");
			$datestr .= " " . $this->months[(date("m") - 1)];
			$datestr .= " " . date("Y");
			$this->datestr = $datestr;
		}
		
		function setLabels() {
			$this->months = array(
				__("Janvier"), 
				__("Février"), 
				__("Mars"), 
				__("Avril"),
				__("Mai"), 
				__("Juin"), 
				__("Juillet"), 
				__("Août"),
				__("Septembre"), 
				__("Octobre"), 
				__("Novembre"), 
				__("Décembre")
			);

			$this->days = array(
				__("Dimanche"), 
				__("Lundi"), 
				__("Mardi"), 
				__("Mercredi"),
				__("Jeudi"), 
				__("Vendredi"), 
				__("Samedi")
			);	
		}

		function toString() {
			return $this->datestr;
		}

		function getDateIso($dateStr, $hour = 1) {
			if ($hour != 1 && preg_match("/^\d{4}-\d{2}-\d{2}$/", $dateStr)) {
				return $dateStr; 
			}
			
			if ($dateStr == "" && $hour != 1) { 
				return date("Y")."-".date("m")."-".date("d"); 
			}
			
			if ($dateStr == "" && $hour == 1) { 
				return date("Y")."-".date("m")."-".date("d")." ".date("H")."h".date("i"); 
			}

			$dateStr = preg_replace("/[-:\s]/", "", $dateStr);
			
			$year = substr($dateStr, 0, 4);
			$month = substr($dateStr, 4, 2);
			$day = substr($dateStr, 6, 2);
			$hour = substr($dateStr, 8, 2);
			$minute = substr($dateStr, 10, 2);
			$second = substr($dateStr, 12, 2);
			
			if ($year == "" || $month == "" || $day == "") { return ""; }

			if ($hour == 1) {
				return $year."-".$month."-".$day." ".$hour.":".$minute.":".$second;
			} else {
				return $year."-".$month."-".$day;
			}
		}
		
		function getDayOfTheWeek($date) {
			$return = date("w", strtotime($date));
			return $this->days[$return];	
		}

	}

?>