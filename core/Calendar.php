<?php
	/*
		Copyright (C) 2004 Jean-Philippe Ricard, QuiboWeb Inc.
		WWW.QUIBOWEB.CA

		This program is free software; you can redistribute it and/or
		modify it under the terms of the GNU General Public License
		as published by the Free Software Foundation; either version 2
		of the License, or (at your option) any later version.

		This program is distributed in the hope that it will be useful,
		but WITHOUT ANY WARRANTY; without even the implied warranty of
		MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
		GNU General Public License for more details.

		You should have received a copy of the GNU General Public License
		along with this program; if not, write to the Free Software
		Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
	*/

	/**
     *
     * @version 1.0
     * @author Jean-Philippe Ricard <ricardjp@quiboweb.ca>
	 * @package	Quibo
     */
	class Calendar {

		var $months;
		var $days;
		var $year;
		var $month;
		var $today;
		var $day;
		var $layout;
		var $filters;
		var $date;
		var $content;


		/* year is a 4 digits int, month is a 2 digits int */
		function Calendar($date, $layout = 'month', $filters) {
			$this->setLabels();
			$this->year = date("Y", strtotime($date));
			$this->month = date("n", strtotime($date));
			$this->day = date("j", strtotime($date));
			$this->date = $date;
			$this->layout = $layout;
			$this->filters = $filters;
		}

		function setLabels() {
			$this->months = array(
				__("Janvier"),
				__("Février"),
				__("Mars"),
				__("Avril"),
				__("Mai"),
				__("Juin"),
				__("Juillet"),
				__("Août"),
				__("Septembre"),
				__("Octobre"),
				__("Novembre"),
				__("Décembre")
			);

			$this->days = array(
				__("Dimanche"),
				__("Lundi"),
				__("Mardi"),
				__("Mercredi"),
				__("Jeudi"),
				__("Vendredi"),
				__("Samedi")
			);
		}

		function isLeapYear($year) {
			$date = strtotime($year . "-01-01");
			if (date("L", $date) == 1) {
				return true;
			}
			return false;
		}

		function getNumberOfDaysInMonth($year, $month) {
			if ($this->isLeapYear($year)) {
				$feb = 29;
			} else {
				$feb = 28;
			}
			$days = array(31, $feb, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
			$month = $month - 1;
			return $days[$month];
		}

		function getFirstDayOfMonth($year, $month) {
			$date = strtotime($year . "-" . $month . "-01");
			$day = date("w", $date);
			return ++$day;
		}

		function getNumberOfRows($year, $month) {
			$date = strtotime($year . "-" . $month . "-01");
			if (date("w", $date) > 5) {
				return 6;
			} else {
				return 5;
			}
		}

		function setToday($day) {
			$this->today = $day;
		}

		function getContent($DB) {
			$this->content = array();

			for ($j = 1; $j < 32; $j++) {
				$this->content["d" . $j] = array("descr" => "", "class" => " empty ");
				for ($i = 0; $i < 24; $i++) {
					$this->content["d" . $j . "-h" . $i] = array("descr" => "", "class" => " empty ");
					$this->content["d" . $j . "-h" . $i . ".5"] = array("descr" => "", "class" => " empty ");
				}
			}


			$DB->query(
				"SELECT * ".
				"FROM calendar_content ".
				"WHERE (date_debut >= '" . $this->year . "-" . substr("0".$this->month,-2) . "-01' AND date_debut <= '" . $this->year . "-" . substr("0".$this->month,-2) . "-31') ".
				"OR event_type = 'employes' ".
				"ORDER BY date_debut ASC"
			);


			$second = date("j", strtotime('second friday of ' . date("F", strtotime($this->year . "-" . substr("0".$this->month,-2) . "-01"))));

			$this->content["d" . $second] =  array("descr" => "<small style=\"font-size: 8px; color: red; text-align: center; padding: 2px; display: block;\">Date limite pour remettre vos textes à être publiés le mois suivant.</small>", "class" => "alert");

			while ($DB->next_record()) {

				$date_debut = $DB->getField("date_debut");
				$date_fin = $DB->getField("date_fin");

				$heure_debut = $DB->getField("heure_debut");
				$heure_fin = $DB->getField("heure_fin");

				$description = $DB->getField("description");
				$class = "";


				if (preg_match("/Rencontre:\s*[0-9]/is", $description)) {
					$directions = getDirectionsTravail("array");
					$direction_id = preg_replace("/Rencontre:\s*/is", "", trim($description));
					$description = preg_replace("/\s" . $direction_id . "$/is", $directions[$direction_id], trim($description));

					$class = "direction" . $direction_id;

					if (is_array($this->filters) && !isset($this->filters[$direction_id])) {
						continue;
					}
				}

				if ($DB->getField("event_type") == "employes") {
					$class = "anniversaire";
					$heure_debut = 0;



					if (!preg_match("/^[0-9]{4}-[0-9]+-[0-9]+$/", $date_debut)) {

						if (preg_match("/^0-([0-9]+-[0-9]+)$/", $date_debut)) {

							$date_debut = date("Y-m-d", strtotime(preg_replace("/^0-([0-9]+-[0-9]+)$/", $this->year . "-$1", $date_debut)));

						} else{

							$date_debut = date("Y-m-d", strtotime(preg_replace("/^([0-9]+-[0-9]+)$/", $this->year . "-$1", $date_debut)));
						}

					} else {
						$date_debut = date("Y-m-d", strtotime(preg_replace("/[0-9]{4}/", $this->year, $date_debut)));
					}

					if (date("m", strtotime($date_debut)) != date("m", strtotime("2016-" . $this->month . "-01"))) {
						continue;
					}

					$date_fin = "";

					if (is_array($this->filters) && !isset($this->filters[999])) {
						continue;
					} else if ($this->filters == "") {
						continue;
					}
				}



				if ($date_fin != "" && $date_debut != $date_fin) {

					$numberofdays = floor((strtotime($date_fin) - strtotime($date_debut)) / (60 * 60 * 24));

					for ($j = 0; $j < $numberofdays; $j++) {

						$day = date("j", strtotime($date_debut . " +" . $j . " day"));

						if ("-".$heure_debut != "-all") {
							$thedescription = " <span class=\"" . $class . "\">" . $description . "</span> " . $this->content["d" . $day . "-h" . $heure_debut]["descr"];
							$this->content["d" . $day . "-h" . $heure_debut] = array("descr" => $thedescription, "class" => $class);
						} else {
							for ($i = 0; $i < 24; $i++) {
								$thedescription = " <span class=\"" . $class . "\">" . $description . "</span> " . $this->content["d" . $day . "-h" . $i]["descr"];
								$this->content["d" . $day . "-h" . $i] = array("descr" => $thedescription, "class" => $class);
								$thedescription = " <span class=\"" . $class . "\">" . $description . "</span> " . $this->content["d" . $day . "-h" . $i . ".5"]["descr"];
								$this->content["d" . $day . "-h" . $i . ".5"] = array("descr" => $thedescription, "class" => $class);
							}
						}

						$thedescription = " <span class=\"" . $class . "\">" . $description . "</span> " . $this->content["d" . $day]["descr"];
						$this->content["d" . $day] =  array("descr" => $thedescription, "class" => $class);

					}

				} else {

					$day = date("j", strtotime($date_debut));

					if ("-".$heure_debut != "-all") {
						$thedescription = " <span class=\"" . $class . "\">" . $description . "</span> " . $this->content["d" . $day . "-h" . $heure_debut]["descr"];
						$this->content["d" . $day . "-h" . $heure_debut] = array("descr" => $thedescription, "class" => $class);

					} else {
						for ($i = 0; $i < 24; $i++) {
							$thedescription = " <span class=\"" . $class . "\">" . $description . "</span> " . $this->content["d" . $day . "-h" . $i]["descr"];
							$this->content["d" . $day . "-h" . $i] = array("descr" => $thedescription, "class" => $class);
							$thedescription = " <span class=\"" . $class . "\">" . $description . "</span> " . $this->content["d" . $day . "-h" . $i . ".5"]["descr"];
							$this->content["d" . $day . "-h" . $i . ".5"] = array("descr" => $thedescription, "class" => $class);

							// $this->content["d" . $day . "-h" . $i] = array("descr" => $description, "class" => $class);
							// $this->content["d" . $day . "-h" . $i . ".5"] = array("descr" => $description, "class" => $class);
						}
					}

					$thedescription = " <span class=\"" . $class . "\">" . $description . "</span> " . $this->content["d" . $day]["descr"];
					$this->content["d" . $day] =  array("descr" => $thedescription, "class" => $class);
				}

			}

		}

		function toString($DB, $short = FALSE) {

			$this->getContent($DB);

			switch ($this->layout) {
				case 'day':
					return $this->printDay($short);
					break;

				case 'week':
					return $this->printWeek($short);
					break;

				default:
					return $this->printMonth($short);
					break;
			}
		}

		function printDay($short = FALSE) {

			$currentDay = date("w", strtotime($this->date)) + 1;

			$day_class_segment = "";
			$day_class_id = "";

			$day = 1;

			switch ($currentDay) {
				case '1':
					$day_class_segment = " fc-sun fc-first ";
					break;
				case '2':
					$day_class_segment = " fc-mon ";
					break;
				case '3':
					$day_class_segment = " fc-tue ";
					break;
				case '4':
					$day_class_segment = " fc-wed ";
					break;
				case '5':
					$day_class_segment = " fc-thu ";
					break;
				case '6':
					$day_class_segment = " fc-fri ";
					break;
				case '7':
					$day_class_segment = " fc-sat fc-last ";
					break;
			}

			$calendarStr = "";
			$calendarStr .=
				"<div class=\"quibo-calendar-view\">\n".
				"<div class=\"calendar-wrapper\" data-filters='" . json_encode($this->filters) . "' data-date=\"" . $this->date . "\"  data-layout=\"" . $this->layout . "\" data-prev-date=\"" . date("Y-m-d", strtotime($this->date . " -1 day")) . "\" data-next-date=\"" . date("Y-m-d", strtotime($this->date . " +1 day")) . "\">".
				"<table class=\"fc-header\">\n".
				"	<tbody>\n".
				"		<tr>\n".
				"			<td class=\"fc-header-left\"><span class=\"fc-header-title\"><h2>" . $this->day . " " . $this->months[$this->month - 1] . " " . $this->year . "</h2></span></td>\n".
				"			<td class=\"fc-header-center\"></td>\n".
				"			<td class=\"fc-header-right\">\n".
				"				<span class=\"fc-button fc-button-month ui-state-default ui-corner-left ui-state-active\">Mois</span>\n".
				"				<span class=\"fc-button fc-button-agendaWeek ui-state-default\">Semaine</span>\n".
				"				<span class=\"fc-button fc-button-agendaDay ui-state-default\">Jour</span>\n".
				"				<span class=\"fc-button fc-button-prev ui-state-default\"><span class=\"fc-icon-wrap\"><span class=\"ui-icon ui-icon-circle-triangle-w\"></span></span></span>\n".
				"				<span class=\"fc-button fc-button-next ui-state-default ui-corner-right\"><span class=\"fc-icon-wrap\"><span class=\"ui-icon ui-icon-circle-triangle-e\"></span></span></span>\n".
				"			</td>\n".
				"		</tr>\n".
				"	</tbody>\n".
				"</table>\n".
				"<table class=\"calendarTable calendarDay fc-border-separate fc-agenda-slots\" style=\"width: 100%;\">\n".
				"	<thead>\n".
				"		<tr class=\"fc-first fc-last\">\n".
				"			<th class=\"calendarHeader fc-day-header ui-widget-header fc-first\"></th>\n".
				"			<th class=\"calendarHeader fc-day-header " . $day_class_segment . " ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[$currentDay - 1]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[$currentDay - 1], 0, 3)) . "</span> " . date("m/d", strtotime($this->date)) . "</th>\n".
				"		</tr>\n".
				"	</thead>\n".
				"	<tbody>\n";


			for ($i = 0; $i < 24; $i++) {

				$row1 = "";
				$row2 = "";

				$day_class = "fc-day ui-widget-content " . $day_class_segment;


				$row1 .= 	"			<td class=\"ui-widget-content " . $day_class . "\">\n".
							"				<div class=\"fc-hour-content\">" . $this->content["d" . $this->day . "-h" . $i]["descr"] . "</div>\n".
							"			</td>\n";

				$row2 .= 	"			<td class=\"ui-widget-content " . $day_class . "\">\n".
							"				<div class=\"fc-hour-content\">" . $this->content["d" . $this->day . "-h" . $i . ".5"]["descr"] . "</div>\n".
							"			</td>\n";

				$calendarStr .=
					"		<tr class=\"fc-slot" . $i . "\">\n".
					"			<th class=\"fc-agenda-axis ui-widget-header\">" . $i . "h</th>\n".
					"			" . $row1 . "\n".
					"		</tr>\n".
					"		<tr class=\"fc-slot" . $i . "-point5 fc-minor\">\n".
					"			<th class=\"fc-agenda-axis ui-widget-header\">&nbsp;</th>\n".
					"			" . $row2 . "\n".
					"		</tr>\n";

			}

			$calendarStr .=
				"	</tbody>\n".
				"</table>\n".
				"</div>\n".
				"</div>\n";


			return $calendarStr;

		}

		function printWeek($short = FALSE) {

			$numberOfDays = $this->getNumberOfDaysInMonth(
				$this->year,
				$this->month
			);

			$currentDay = date("w", strtotime($this->date)) + 1;

			$started = 0;
			$day = 1;

			$firstDay = $this->getFirstDayOfMonth($this->year, $this->month);


			switch ($currentDay) {
				case '1':
					$daysun = date("m/d", strtotime($this->date));
					$daymon = date("m/d", strtotime($this->date . " +1 day"));
					$daytue = date("m/d", strtotime($this->date . " +2 days"));
					$daywed = date("m/d", strtotime($this->date . " +3 days"));
					$daythu = date("m/d", strtotime($this->date . " +4 days"));
					$dayfri = date("m/d", strtotime($this->date . " +5 days"));
					$daysat = date("m/d", strtotime($this->date . " +6 days"));

					break;
				case '2':
					$daysun = date("m/d", strtotime($this->date . " -1 day"));
					$daymon = date("m/d", strtotime($this->date));
					$daytue = date("m/d", strtotime($this->date . " +1 day"));
					$daywed = date("m/d", strtotime($this->date . " +2 days"));
					$daythu = date("m/d", strtotime($this->date . " +3 days"));
					$dayfri = date("m/d", strtotime($this->date . " +4 days"));
					$daysat = date("m/d", strtotime($this->date . " +5 days"));
					break;
				case '3':
					$daysun = date("m/d", strtotime($this->date . " -2 days"));
					$daymon = date("m/d", strtotime($this->date . " -1 day"));
					$daytue = date("m/d", strtotime($this->date));
					$daywed = date("m/d", strtotime($this->date . " +1 day"));
					$daythu = date("m/d", strtotime($this->date . " +2 days"));
					$dayfri = date("m/d", strtotime($this->date . " +3 days"));
					$daysat = date("m/d", strtotime($this->date . " +4 days"));
					break;
				case '4':
					$daysun = date("m/d", strtotime($this->date . " -3 days"));
					$daymon = date("m/d", strtotime($this->date . " -2 days"));
					$daytue = date("m/d", strtotime($this->date . " -1 day"));
					$daywed = date("m/d", strtotime($this->date));
					$daythu = date("m/d", strtotime($this->date . " +1 day"));
					$dayfri = date("m/d", strtotime($this->date . " +2 days"));
					$daysat = date("m/d", strtotime($this->date . " +3 days"));
					break;
				case '5':
					$daysun = date("m/d", strtotime($this->date . " -4 days"));
					$daymon = date("m/d", strtotime($this->date . " -3 days"));
					$daytue = date("m/d", strtotime($this->date . " -2 days"));
					$daywed = date("m/d", strtotime($this->date . " -1 day"));
					$daythu = date("m/d", strtotime($this->date));
					$dayfri = date("m/d", strtotime($this->date . " +1 day"));
					$daysat = date("m/d", strtotime($this->date . " +2 days"));
					break;
				case '6':
					$daysun = date("m/d", strtotime($this->date . " -5 days"));
					$daymon = date("m/d", strtotime($this->date . " -4 days"));
					$daytue = date("m/d", strtotime($this->date . " -3 days"));
					$daywed = date("m/d", strtotime($this->date . " -2 days"));
					$daythu = date("m/d", strtotime($this->date . " -1 day"));
					$dayfri = date("m/d", strtotime($this->date));
					$daysat = date("m/d", strtotime($this->date . " +1 days"));
					break;
				case '7':
					$daysun = date("m/d", strtotime($this->date . " -6 days"));
					$daymon = date("m/d", strtotime($this->date . " -5 days"));
					$daytue = date("m/d", strtotime($this->date . " -4 days"));
					$daywed = date("m/d", strtotime($this->date . " -3 days"));
					$daythu = date("m/d", strtotime($this->date . " -2 days"));
					$dayfri = date("m/d", strtotime($this->date . " -1 day"));
					$daysat = date("m/d", strtotime($this->date));
					break;
			}

			$calendarStr = "";

			$calendarStr .=
				"<div class=\"quibo-calendar-view\">\n".
				"<div class=\"calendar-wrapper\" data-filters='" . json_encode($this->filters) . "' data-date=\"" . $this->date . "\"  data-layout=\"" . $this->layout . "\" data-prev-date=\"" . date("Y-m-d", strtotime($this->date . " -1 week")) . "\" data-next-date=\"" . date("Y-m-d", strtotime($this->date . " +1 week")) . "\">".
				"<table class=\"fc-header\">\n".
				"	<tbody>\n".
				"		<tr>\n".
				"			<td class=\"fc-header-left\"><span class=\"fc-header-title\"><h2>" . $this->months[$this->month - 1] . " " . $this->year . "</h2></span></td>\n".
				"			<td class=\"fc-header-center\"></td>\n".
				"			<td class=\"fc-header-right\">\n".
				"				<span class=\"fc-button fc-button-month ui-state-default ui-corner-left ui-state-active\">Mois</span>\n".
				"				<span class=\"fc-button fc-button-agendaWeek ui-state-default\">Semaine</span>\n".
				"				<span class=\"fc-button fc-button-agendaDay ui-state-default\">Jour</span>\n".
				"				<span class=\"fc-button fc-button-prev ui-state-default\"><span class=\"fc-icon-wrap\"><span class=\"ui-icon ui-icon-circle-triangle-w\"></span></span></span>\n".
				"				<span class=\"fc-button fc-button-next ui-state-default ui-corner-right\"><span class=\"fc-icon-wrap\"><span class=\"ui-icon ui-icon-circle-triangle-e\"></span></span></span>\n".
				"			</td>\n".
				"		</tr>\n".
				"	</tbody>\n".
				"</table>\n".
				"<table class=\"calendarTable calendarWeek fc-border-separate fc-agenda-slots\" style=\"width: 100%;\">\n".
				"	<thead>\n".
				"		<tr class=\"fc-first fc-last\">\n".
				"			<th class=\"calendarHeader fc-day-header ui-widget-header fc-first\"></th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-sun ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[0]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[0], 0, 3)) . "</span> " . $daysun . "</th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-mon ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[1]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[1], 0, 3)) . "</span> " . $daymon . "</th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-tue ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[2]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[2], 0, 3)) . "</span> " . $daytue . "</th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-wed ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[3]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[3], 0, 3)) . "</span> " . $daywed . "</th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-thu ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[4]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[4], 0, 3)) . "</span> " . $daythu . "</th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-fri ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[5]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[5], 0, 3)) . "</span> " . $dayfri . "</th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-sat ui-widget-header fc-last\"><span class=\"long\">" . strtoupper($this->days[6]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[6], 0, 3)) . "</span> " . $daysat . "</th>\n".
				"		</tr>\n".
				"	</thead>\n".
				"	<tbody>\n";


			for ($i = 0; $i < 24; $i++) {

				$row1 = "";
				$row2 = "";

				for ($j = 1; $j <= 7; $j++) {
					$day_class = "fc-day ui-widget-content";
					switch ($j) {
						case '1': $day_class .= " fc-sun fc-first"; $day = preg_replace("/.*?\/0?/is", "", $daysun); break;
						case '2': $day_class .= " fc-mon"; $day = preg_replace("/.*?\/0?/is", "", $daymon); break;
						case '3': $day_class .= " fc-tue"; $day = preg_replace("/.*?\/0?/is", "", $daytue); break;
						case '4': $day_class .= " fc-wed"; $day = preg_replace("/.*?\/0?/is", "", $daywed); break;
						case '5': $day_class .= " fc-thu"; $day = preg_replace("/.*?\/0?/is", "", $daythu); break;
						case '6': $day_class .= " fc-fri"; $day = preg_replace("/.*?\/0?/is", "", $dayfri); break;
						case '7': $day_class .= " fc-sat fc-last"; $day = preg_replace("/.*?\/0?/is", "", $daysat); break;
					}

					if ($currentDay == $j && $this->date == current_time("Y-m-d")) {
						$day_class .= " fc-today ui-state-highlight";
					}

					if ($started) {
						if ($day <= $numberOfDays) {

							$row1 .= 	"			<td class=\"ui-widget-content " . $day_class . "\">\n".
										"				<div class=\"fc-hour-content\">" . $this->content["d" . $day . "-h" . $i]["descr"] . "</div>\n".
										"			</td>\n";

							$row2 .= 	"			<td class=\"ui-widget-content " . $day_class . "\">\n".
										"				<div class=\"fc-hour-content\">" . $this->content["d" . $day . "-h" . $i . ".5"]['descr'] . "</div>\n".
										"			</td>\n";

							//$day++;
						} else {
							$row1 .= 	"			<td class=\"ui-widget-content " . $day_class . "\">\n".
										"				<div class=\"fc-hour-content\"></div>\n".
										"			</td>\n";

							$row2 .= 	"			<td class=\"ui-widget-content " . $day_class . "\">\n".
										"				<div class=\"fc-hour-content\"></div>\n".
										"			</td>\n";
						}
					} else {
						if ($j < $firstDay) {
							$row1 .= 	"			<td class=\"ui-widget-content " . $day_class . "\">\n".
										"				<div class=\"fc-hour-content\"></div>\n".
										"			</td>\n";

							$row2 .= 	"			<td class=\"ui-widget-content " . $day_class . "\">\n".
										"				<div class=\"fc-hour-content\"></div>\n".
										"			</td>\n";
						} else {
							$row1 .= 	"			<td class=\"ui-widget-content " . $day_class . "\">\n".
										"				<div class=\"fc-hour-content\">" . $this->content["d" . $day . "-h" . $i]["descr"] . "</div>\n".
										"			</td>\n";

							$row2 .= 	"			<td class=\"ui-widget-content " . $day_class . "\">\n".
										"				<div class=\"fc-hour-content\">" . $this->content["d" . $day . "-h" . $i . ".5"]["descr"] . "</div>\n".
										"			</td>\n";

							//$day++;
							$started = 1;
						}
					}
				}

				$calendarStr .=
					"		<tr class=\"fc-slot" . $i . "\">\n".
					"			<th class=\"fc-agenda-axis ui-widget-header\">" . $i . "h</th>\n".
					"			" . $row1 . "\n".
					"		</tr>\n".
					"		<tr class=\"fc-slot" . $i . "-point5 fc-minor\">\n".
					"			<th class=\"fc-agenda-axis ui-widget-header\">&nbsp;</th>\n".
					"			" . $row2 . "\n".
					"		</tr>\n";

			}

			$calendarStr .=
				"	</tbody>\n".
				"</table>\n".
				"</div>\n".
				"</div>\n";


			return $calendarStr;
		}


		function printMonth($short = FALSE) {
			$numberOfDays = $this->getNumberOfDaysInMonth(
				$this->year,
				$this->month
			);

			$firstDay = $this->getFirstDayOfMonth($this->year, $this->month);

			$started = 0;
			$day = 1;

			$calendarStr = "";

			$calendarStr .=
				"<div class=\"quibo-calendar-view\">\n".
				"<div class=\"calendar-wrapper\" data-filters='" . json_encode($this->filters) . "' data-date=\"" . $this->date . "\"  data-layout=\"" . $this->layout . "\" data-prev-date=\"" . date("Y-m-d", strtotime($this->date . " -1 month")) . "\" data-next-date=\"" . date("Y-m-d", strtotime($this->date . " +1 month")) . "\">".
				"<table class=\"fc-header\">\n".
				"	<tbody>\n".
				"		<tr>\n".
				"			<td class=\"fc-header-left\"><span class=\"fc-header-title\"><h2>" . $this->months[$this->month - 1] . " " . $this->year . "</h2></span></td>\n".
				"			<td class=\"fc-header-center\"></td>\n".
				"			<td class=\"fc-header-right\">\n".
				"				<span class=\"fc-button fc-button-month ui-state-default ui-corner-left ui-state-active\">Mois</span>\n".
				"				<span class=\"fc-button fc-button-agendaWeek ui-state-default\">Semaine</span>\n".
				"				<span class=\"fc-button fc-button-agendaDay ui-state-default\">Jour</span>\n".
				"				<span class=\"fc-button fc-button-prev ui-state-default\"><span class=\"fc-icon-wrap\"><span class=\"ui-icon ui-icon-circle-triangle-w\"></span></span></span>\n".
				"				<span class=\"fc-button fc-button-next ui-state-default ui-corner-right\"><span class=\"fc-icon-wrap\"><span class=\"ui-icon ui-icon-circle-triangle-e\"></span></span></span>\n".
				"			</td>\n".
				"		</tr>\n".
				"	</tbody>\n".
				"</table>\n".
				"<table class=\"calendarTable calendarMonth fc-border-separate\" style=\"width: 100%;\">\n".
				"	<thead>\n".
				"		<tr class=\"fc-first fc-last\">\n".
				"			<th class=\"calendarHeader fc-day-header fc-sun ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[0]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[0], 0, 3)) . "</span></th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-mon ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[1]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[1], 0, 3)) . "</span></th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-tue ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[2]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[2], 0, 3)) . "</span></th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-wed ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[3]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[3], 0, 3)) . "</span></th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-thu ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[4]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[4], 0, 3)) . "</span></th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-fri ui-widget-header\"><span class=\"long\">" . strtoupper($this->days[5]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[5], 0, 3)) . "</span></th>\n".
				"			<th class=\"calendarHeader fc-day-header fc-sat ui-widget-header fc-last\"><span class=\"long\">" . strtoupper($this->days[6]) . "</span> <span class=\"short\">" . strtoupper(substr($this->days[6], 0, 3)) . "</span></th>\n".
				"		</tr>\n".
				"	</thead>\n".
				"	<tbody>\n";

			$rows = $this->getNumberOfRows($this->year, $this->month);
			for ($i = 0; $i < $rows; $i++) {
				$class = "fc-week ";
				if ($i == 0) { $class = "fc-week fc-first"; }
				if ($i == ($rows -1)) { $class = "fc-week fc-last"; }

				$calendarStr .= "<tr class=\"" . $class . "\">\n";


				for ($j = 1; $j <= 7; $j++) {
					$day_class = "fc-day ui-widget-content";
					switch ($j) {
						case '1': $day_class .= " fc-sun fc-first"; break;
						case '2': $day_class .= " fc-mon"; break;
						case '3': $day_class .= " fc-tue"; break;
						case '4': $day_class .= " fc-wed"; break;
						case '5': $day_class .= " fc-thu"; break;
						case '6': $day_class .= " fc-fri"; break;
						case '7': $day_class .= " fc-sat fc-last"; break;
					}

					if ($this->year . "-" . substr("0" . $this->month, -2) . "-" . $day == current_time("Y-m-d")) {
						$day_class .= " fc-today ui-state-highlight";
					}



					if ($started) {
						if ($day <= $numberOfDays) {
							if ($day == $this->today) {
								$calendarStr .= "<td class=\"calendarActiveCell " . $day_class . "\"><div class=\"fc-day-number\">" . $day . "</div>
					<div class=\"fc-day-content\">" . $this->content["d" . $day]["descr"] . "</div></td>\n";
							}
							else {
								$calendarStr .= "<td class=\"calendarCell " . $day_class . "\"><div class=\"fc-day-number\">" . $day . "</div>
					<div class=\"fc-day-content\">" . $this->content["d" . $day]["descr"] . "</div></td>\n";
							}
							$day++;
						}
						else {
							$calendarStr .= "<td class=\"calendarCell " . $day_class . "\">&nbsp;</td>\n";
						}
					}
					else {
						if ($j < $firstDay) {
							$calendarStr .= "<td class=\"calendarCell " . $day_class . "\">&nbsp;</td>\n";
						}
						else {
							if ($day == $this->today) {
								$calendarStr .= "<td class=\"calendarActiveCell " . $day_class . "\"><div class=\"fc-day-number\">" . $day . "</div>
					<div class=\"fc-day-content\">" . $this->content["d" . $day]["descr"] . "</div></td>\n";
							}
							else {
								$calendarStr .= "<td class=\"calendarCell " . $day_class . "\"><div class=\"fc-day-number\">" . $day . "</div>
					<div class=\"fc-day-content\">" . $this->content["d" . $day]["descr"] . "</div></td>\n";
							}
							$day++;
							$started = 1;
						}
					}
				}
				$calendarStr .= "</tr>\n";
			}

			$calendarStr .=
				"	</tbody>\n".
				"</table>\n".
				"</div>\n".
				"</div>\n";


			return $calendarStr;
		}

	}
